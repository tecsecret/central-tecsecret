<?php
/**
 * Força re-sincronização de pedidos com boletos emitidos (desde que o status não seja cancelado ou pago).
 * @author     Henrique Cruz | henriquecruz.com.br
 * @copyright  Copyright (c) 2019 https://henriquecruz.com.br
 */

require_once("init.php");
$whmcs->load_function("gateway");
$whmcs->load_function("invoice");

// Initialise gateway configuration
$gatewayConfig = getGatewayVariables("paghiper");

$unpaid_transactions = 0;
$paid_transactions = 0;


echo '<pre>'; 

if(!empty($gatewayConfig)) {

	// Define the gateway auth
    $account_token = trim($gatewayConfig['token']);
    $account_api_key = trim($gatewayConfig['api_key']);

	// Pegamos todos os boletos com status reservado na tabela de boletos
	//$transactions = mysql_query("SELECT transaction_id, status FROM mod_paghiper WHERE status IN ('reserved', 'paid')");
	$transactions = mysql_query("SELECT mod_paghiper.* FROM mod_paghiper RIGHT JOIN tblinvoices ON tblinvoices.id = mod_paghiper.order_id WHERE tblinvoices.status IN ('Unpaid', 'Payment Pending') AND mod_paghiper.status IN ('pending', 'reserved', 'paid', 'cancel_failed');");
	while($transaction = mysql_fetch_array($transactions)) {

		// Define data for our API transaction
		$paghiper_data = array(
			'apiKey'			=> $account_api_key,
			'token'				=> $account_token,
			'transaction_id'	=> $transaction['transaction_id'] 
		);

		// Agora vamos buscar o status da transação diretamente na PagHiper, usando a API.
		$url = "https://api.paghiper.com/transaction/status/";
		$data_post = json_encode( $paghiper_data );
		$mediaType = "application/json"; // formato da requisição
		$charset = "UTF-8";
		$headers = array();
		$headers[] = "Accept: ".$mediaType;
		$headers[] = "Accept-Charset: ".$charset;
		$headers[] = "Accept-Encoding: ".$mediaType;
		$headers[] = "Content-Type: ".$mediaType.";charset=".$charset;
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_post);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

		$result = curl_exec($ch);

		// captura o http code
		$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);

		// Agora processamos a notificação, que recebemos em formato JSON
		$request = json_decode($result, true);

		$is_paid = (array_key_exists('status', $request['status_request'])) ? ( in_array($request['status_request']['status'], array('paid', 'completed')) ) : false;
		$invoice_id 		= $request['status_request']['order_id'];
		$transaction_id 	= $transaction['transaction_id'];
		$ammount_paid       = to_monetary($request['status_request']['value_cents_paid'] / 100);
		$transaction_fee    = to_monetary($request['status_request']['value_fee_cents'] / 100);

		if($is_paid) {

			// Checamos se a transação ja foi criada no banco
			$result = select_query("tblaccounts", "id", array( "transid" => $transaction_id ));
			$numRows = mysql_num_rows($result);
			$transaction_is_added = ($numRows) ? true : false;

			if(!$transaction_is_added) {

				// Calcula a taxa cobrada pela PagHiper de maneira dinâmica e registra para uso no painel.
				$fee = $transaction_fee;

				// Logamos a transação no log de Gateways do WHMCS.
				logTransaction('paghiper',$request,"Transação Concluída");

				// Logamos status no banco
				log_status_to_db($status, $transaction_id);

				// Se estiver tudo certo, checamos se o valor pago é diferente do configurado na fatura
				if($results['balance'] !== $ammount_paid) {

					// Subtraimos valor de balanço do valor pago. Funciona tanto para desconto como acréscimo.
					// Ex. 1: Valor pago | R$ 18 - R$ 20 (Balanço) = -R$ 2 [Desconto]
					// Ex. 2: Valor pago | R$ 21 - R$ 20 (Balanço) = +R$ 1 [Multa]
					$value = $ammount_paid - $results['balance'];

					if($results['balance'] > $ammount_paid) {

						// Conciliação: Desconto por antecipação (Valor de balanço da Invoice - Valor total pago)
						$desc = 'Desconto por pagamento antecipado';
						add_to_invoice($invoice_id, $desc, $value, $whmcsAdmin);

					} else {

						// Conciliação: Juros e Multas = (Valor total pago - Valor contido na Invoice)
						$desc = 'Juros e multa por atraso';
						add_to_invoice($invoice_id, $desc, $value, $whmcsAdmin);

					}
				}

				// Registramos o pagamento e damos baixa na fatura
				addInvoicePayment($invoice_id,$transaction_id,$ammount_paid,$fee,'paghiper');

				$unpaid_transactions++;

				echo "Transação #{$transaction_id} marcou a fatura #{$invoice_id} como paga.<br>";

			} else {

				echo "Transação #{$transaction_id} ja foi adicionada a fatura #{$invoice_id}.<br>";

				try {

					$result = select_query("tblinvoices", "userid,total,status", array( "id" => $invoice_id ));
					$data = mysql_fetch_array($result);
					$userid = $data["userid"];
					$total = $data["total"];
					$status = $data["status"];
	
					$result = select_query("tblaccounts", "SUM(amountin)-SUM(amountout)", array( "invoiceid" => $invoice_id ));
					$data = mysql_fetch_array($result);
					$amountpaid = $data[0];
					$balance = $total - $amountpaid;
	
					$balance = format_as_currency($balance);
					$amount = format_as_currency($amount);
					$balance -= $amount;

					logActivity("Added Invoice Payment - Invoice ID: " . $invoice_id, $userid);
					run_hook("AddInvoicePayment", array( "invoiceid" => $invoice_id ));
	
					if( $balance <= 0 && in_array($status, array( "Unpaid", "Payment Pending" )) ) {
						processPaidInvoice($invoice_id, $noemail, $date);
					} else {
						if( !$noemail ) {
							sendMessage("Invoice Payment Confirmation", $invoice_id);
						}
					}

				} catch(Exception $e) {
					echo "Não foi possível processar a fatura {$invoice_id}. Erro: ", $e->getMessage();
				}

				$paid_transactions ++;

			}
		}


	}
}

echo "{$unpaid_transactions} transações foram criadas.<br>";
echo "{$paid_transactions} faturas foram marcadas como pagas.<br>";
echo '</pre>';