{if $ttc && $ttc.tasks}
    <div class="panel panel-default panel-accent-green" menuitemname="TTC Open Tasks">
        <div class="panel-heading " >
            <div  style="float:left;">
                <h3 class="panel-title"><i class="glyphicon glyphicon-tasks"></i>
                    {$TTCLANG.home_opentasks} 
                    <span class="badge badge-success">{$ttc.numopentasks}
                    </span>
                </h3>
            </div>
            <div align="right" style="margin-right: 2%;">
                <label  >
                    {if $ttcBalance < 0 }<font color="red">{else}<font color="green">{/if}{$TTCLANG.home_balance} {$ttcBalance} </font>
                </label>
            </div>


        </div>
        <div class="panel-body" style="max-height: 300px; overflow: scroll; overflow-x: hidden;">
            <table class="table table-striped table-framed table-centered" style="font-size: 0.9em;">
                <thead>
                    <tr>
                        <th>{$TTCLANG.home_taskname}</th>
                        <th>{$TTCLANG.home_taskstatus}</th>
                        <th>{$TTCLANG.home_taskhoursworked}</th>
                        <th>{$TTCLANG.home_taskrate}</th>

                    </tr>
                </thead>
                <tbody>
                    {foreach from=$ttc.tasks item=task}
                        <tr>
                            <td>{$task.name}</td>
                            <td>{$task.status}</td>
                            <td><a class="balloon" href="javascript:" title="
                                   {if $task.entries}
                                       {if $task.entries.o}
                                           <strong>{$TTCLANG.NormalRate}:</strong><br />
                                           {foreach from=$task.entries.o item=entry}
                                               <strong>{$entry.hours}</strong> on <strong>{$entry.date}</strong> ({$entry.description}) - {$task.hourlyrate} {$ttc.defaultcurrency.code}/h <br />
                                           {/foreach} 
                                       {/if}
                                       {if $task.entries.e}                    
                                           <strong>{$TTCLANG.ExtraRate}:</strong><br />
                                           {foreach from=$task.entries.e item=entry}
                                               <strong>{$entry.hours}</strong> on <strong>{$entry.date}</strong> ({$entry.description}) - {$entry.extrarate} {$ttc.defaultcurrency.code}/h <br />
                                           {/foreach}                                    
                                       {/if}
                                   {else}{$TTCLANG.noRecords}<br />{/if}  ">{$task.hoursworked}</a></td>
                            {if $task.curr.code}
                                <td>{$task.hourlyrate} {$task.curr.code}/h{if $task.hourlyrate1}<br />{$task.hourlyrate1} {$task.curr.code}/h{/if}</td>
                            {else}
                                <td>{$task.hourlyrate} {$ttc.defaultcurrency.code}/h{if $task.hourlyrate1}<br />{$task.hourlyrate1} {$ttc.defaultcurrency.code}/h{/if}</td>
                            {/if}
                        </tr>
                    {foreachelse}
                        <tr>
                            <td colspan="4" class="textcenter">{$TTCLANG.home_noneopentasks}</td>
                        </tr>
                    {/foreach}
                    <tr>
                        <td></td>
                        <td class="textright"><strong>{$TTCLANG.home_prepaidhours}</strong></td>
                        <td class="textleft">{foreach from=$ttc.currentprepaid key=fieldname item=hours}<strong>{$hours}</strong> ({$fieldname})<br />{/foreach}</td>
                        <td class="textright"><a class="btn btn-success" href="{if $ttc.prepaidproductpath}{$ttc.prepaidproductpath}{else}cart.php?a=add&pid={$ttc.prepaidproductid}{/if}"><i class="icon-ok-circle icon-white"></i> {$TTCLANG.home_button_buyhours}</a></td>
                    </tr>
                    {if $ttc.currentincidents}
                        <tr>
                            <td></td>
                            <td class="textright"><strong>{$TTCLANG.home_prepaidincidents}</strong></td>
                            <td class="textleft">{foreach from=$ttc.currentincidents key=fieldname item=incidents}<strong>{$incidents}</strong> ({$fieldname})<br />{/foreach}</td>
                            <td class="textright"></td>
                        </tr>
                    {/if}
                </tbody>
            </table>
        </div>
        <div class="panel-footer"></div>
    </div>

    {literal}
        <script type="text/javascript" src="modules/addons/TimeTaskManager/core/assets/js/jquery.balloon.min.js"></script>
        <script type="text/javascript">
            jQuery(function () {
                jQuery(".balloon").balloon({position: "right"});
            });
        </script>
    {/literal}
{/if}