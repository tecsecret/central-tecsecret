{if $ttcClosed && $ttcClosed.tasks}
    <div class="panel panel-default panel-accent-asbestos" menuitemname="TTC Closed Tasks">
        <div class="panel-heading">
            <h3 class="panel-title"><i class="glyphicon glyphicon-tasks"></i> {$TTCLANG.home_closedTasks} <span class="badge badge-info">{$ttcClosed.numopentasks}</span></h3>
        </div>
        <div class="panel-body" style="max-height: 300px; overflow: scroll; overflow-x: hidden;">
            <table class="table table-striped table-framed table-centered" style="font-size: 0.9em;">
                <thead>
                    <tr>
                        <th>{$TTCLANG.home_taskname}</th>
                        <th>{$TTCLANG.home_taskstatus}</th>
                        <th>{$TTCLANG.home_taskhoursworked}</th>
                        <th>{$TTCLANG.home_taskrate}</th>
                    </tr>
                </thead>
                <tbody>
                    {foreach from=$ttcClosed.tasks item=task}
                        <tr>
                            <td>{$task.name}</td>
                            <td>{$task.status}</td>
                            <td><a class="balloon" href="javascript:" title="
                                   {if $task.entries}
                                       {if $task.entries.o}
                                           <strong>{$TTCLANG.NormalRate}:</strong><br />
                                           {foreach from=$task.entries.o item=entry}
                                               <strong>{$entry.hours}</strong> on <strong>{$entry.date}</strong> ({$entry.description}) - {$task.hourlyrate} {$ttc.defaultcurrency.code}/h <br />
                                           {/foreach} 
                                       {/if}
                                       {if $task.entries.e}                    
                                           <strong>{$TTCLANG.ExtraRate}:</strong><br />
                                           {foreach from=$task.entries.e item=entry}
                                               <strong>{$entry.hours}</strong> on <strong>{$entry.date}</strong> ({$entry.description}) - {$entry.extrarate} {$ttc.defaultcurrency.code}/h <br />
                                           {/foreach}                                    
                                       {/if}
                                   {else}{$TTCLANG.noRecords}<br />{/if}  ">{$task.hoursworked}</a></td>
                            {if $task.curr.code}
                                <td>{$task.hourlyrate} {$task.curr.code}/h{if $task.hourlyrate1}<br />{$task.hourlyrate1} {$task.curr.code}/h{/if}</td>
                            {else}
                                <td>{$task.hourlyrate} {$ttc.defaultcurrency.code}/h{if $task.hourlyrate1}<br />{$task.hourlyrate1} {$ttc.defaultcurrency.code}/h{/if}</td>
                            {/if}
                        </tr>
                    {foreachelse}
                        <tr>
                            <td colspan="4" class="textcenter">{$TTCLANG.home_noclosedtasks}</td>
                        </tr>
                    {/foreach}

                </tbody>
            </table>
        </div>
        <div class="panel-footer"></div>
    </div>
{/if}