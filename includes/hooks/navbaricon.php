<?php

use WHMCS\View\Menu\Item as MenuItem;
 
add_hook('ClientAreaPrimaryNavbar', 1, function(MenuItem $primaryNavbar)
{
    if (!is_null($primaryNavbar->getChild('Home'))) {
              $primaryNavbar->getChild('Home')
                            ->setIcon('fas fa-home');
    }
    
    if (!is_null($primaryNavbar->getChild('Services'))) {
              $primaryNavbar->getChild('Services')
                            ->setIcon('fas fa-bars');
    }

    if (!is_null($primaryNavbar->getChild('Domains'))) {
              $primaryNavbar->getChild('Domains')
                            ->setIcon('fas fa-globe');
    }    
    
    if (!is_null($primaryNavbar->getChild('Store'))) {
              $primaryNavbar->getChild('Store')
                            ->setIcon('fas fa-store');

    }

    if (!is_null($primaryNavbar->getChild('Billing'))) {
              $primaryNavbar->getChild('Billing')
                            ->setIcon('fas fa-file-invoice-dollar');
    }    
    
    if (!is_null($primaryNavbar->getChild('Support'))) {
              $primaryNavbar->getChild('Support')
                            ->setIcon('far fa-question-circle');
    }    
    
     if (!is_null($primaryNavbar->getChild('Open Ticket'))) {
              $primaryNavbar->getChild('Open Ticket')
                            ->setIcon('fas fa-concierge-bell');
    }  

if (!is_null($primaryNavbar->getChild('Announcements'))) {
              $primaryNavbar->getChild('Announcements')
                            ->setIcon('fas fa-newspaper');
    }  


if (!is_null($primaryNavbar->getChild('Contact Us'))) {
              $primaryNavbar->getChild('Contact Us')
                            ->setIcon('fas fa-phone');
    }
 
if (!is_null($primaryNavbar->getChild('Network Status'))) {
              $primaryNavbar->getChild('Network Status')
                            ->setIcon('fas fa-plug');
    } 

if (!is_null($primaryNavbar->getChild('Knowledgebase'))) {
              $primaryNavbar->getChild('Knowledgebase')
                            ->setIcon('fas fa-bug');
    } 
    
if (!is_null($primaryNavbar->getChild('Chat Online'))) {
              $primaryNavbar->getChild('Chat Online')
                            ->setIcon('far fa-comment');
    } 

if (!is_null($primaryNavbar->getChild('Website Security'))) {
              $primaryNavbar->getChild('Website Security')
                            ->setIcon('fas fa-lock');
    } 
    
if (!is_null($primaryNavbar->getChild('Affiliates'))) {
              $primaryNavbar->getChild('Affiliates')
                            ->setIcon('fas fa-handshake');
    }

if (!is_null($primaryNavbar->getChild('pm-addon-overview'))) {
              $primaryNavbar->getChild('pm-addon-overview')
                            ->setIcon('fas fa-project-diagram');
    }
    
	if (!is_null($primaryNavbar->getChild('Reseller Area'))) {
              $primaryNavbar->getChild('Reseller Area')
                            ->setIcon('fas fa-project-diagram');
    }
	
if (!is_null($primaryNavbar->getChild('uniqueMenuItemName'))) {
              $primaryNavbar->getChild('uniqueMenuItemName')
                            ->setIcon('far fa-building');
} 

});