<?php
use WHMCS\Database\Capsule;

add_hook("AdminAreaFooterOutput", 1, function($vars){
	$pdo = Capsule::connection()->getPdo();
	$sql = $pdo->query("SELECT value FROM tblconfiguration WHERE setting = 'Version' LIMIT 1");
    if(explode(".", $sql->fetch()[0]["value"])[0] == "8" && $vars["filename"] == "clientsprofile"){
		$lang_password = AdminLang::trans("fields.password");
		$lang_change_password = AdminLang::trans("fields.entertochange");
		return <<<HTML
<script type="text/javascript">
	$('table tr:eq(4) > td:eq(0)').attr('class', 'fieldlabel').html('$lang_password');
	$('table tr:eq(4) > td:eq(1)').attr('class', 'fieldarea').html('<input type="password" class="form-control input-300" name="password" placeholder="$lang_change_password" tabindex="4"/>');
</script>
HTML;
	}
});

add_hook("ClientDetailsValidation", 1, function($vars){
	$pdo = Capsule::connection()->getPdo();
	$sql = $pdo->query("SELECT value FROM tblconfiguration WHERE setting = 'Version' LIMIT 1");
	if(explode(".", $sql->fetch()[0]["value"])[0] == "8"){ // Verify version WHMCS is 8
		$currentUser = new \WHMCS\Authentication\CurrentUser;
		if(
			$vars["password"] != "" && // Verify password is not empty
			$currentUser->isAuthenticatedAdmin() && // Verify is user logged is administrator
			!$currentUser->isMasqueradingAdmin() // Verify is client logged is not a adminstrator
		){
			$clientid = $vars["userid"];
			$GetClientsDetails = localAPI("GetClientsDetails", array(
				"clientid" => $vars["userid"]
			));
			$GetUsers = localAPI("GetUsers", array(
				"search" => $GetClientsDetails["client"]["email"]
			));
			$userid = $GetUsers["users"][0]["id"];
			
			$hasher = new WHMCS\Security\Hash\Password();
			$password = $hasher->hash(WHMCS\Input\Sanitize::decode($vars["password"]));
			
			$pdo->beginTransaction();
			
			try{
				$sql = $pdo->prepare("UPDATE tblusers SET password = :password WHERE id = :id");
				$sql->execute(array(
					"password" => $password,
					"id" => $userid
				));
				$pdo->commit();
			}catch(Exception $e){
				$pdo->rollBack();
				return array(
					"SQL Error! Verify MySQL and Hook to Change Password in WHMCS 8 located in: ".__FILE__
				);
			}
		}
	}
});
?>