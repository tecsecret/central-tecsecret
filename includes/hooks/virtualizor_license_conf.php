<?php
//////////////////////////////////////////////////////////////
//===========================================================
// universal.php
//===========================================================
// VIRTUALIZOR 
// Version : 1.1
// Inspired by the DESIRE to be the BEST OF ALL
// ----------------------------------------------------------
// Started by: Alons
// Date:       30th Jan 2017
// Time:       21:00 hrs
// Site:       http://www.virtualizor.com/ (VIRTUALIZOR)
// ----------------------------------------------------------
// Please Read the Terms of use at http://www.virtualizor.com
// ----------------------------------------------------------
//===========================================================
// (c)Softaculous Inc.
//===========================================================
//////////////////////////////////////////////////////////////


global $conf_virtualizor;

$conf_virtualizor['noc_username'] = 'example'; //Virtualizor NOC username
$conf_virtualizor['noc_password'] = 'example'; //Virtualizor NOC user password
$conf_virtualizor['whmcs_admin'] = 'whmcs_admin'; //WHMCS admin username

//$conf_virtualizor['product_id'] = array(); // List of product ids you want to license Virtualizor for

// Choose the license type, If the below is empty Dedicated Server license will be issued by default
$conf_virtualizor['vps_pid'] = array(); // List of product ids you want to issue Virtualizor VPS license for
$conf_virtualizor['dedicated_pid'] = array(); // List of product ids you want to issue Virtualizor Dedicated server license for

// If you would like to choose a different name for the Configurable options. Default name for Config option is Auto Installer
//$GLOBALS['conf_virtualizor']['fields']['VPS Control Panel'] = 'Virtualizor';

$conf_virtualizor['debug'] = 0; // Set this to 1 if you want to DEBUG

?>