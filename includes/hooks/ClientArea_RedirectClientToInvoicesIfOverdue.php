<?php
/**
 * Prevent Client Access to Client Area, Except to My Invoices page if they have Overdue invoices
 *
 * @author     SENTQ <support@whmcms.com>
 * @copyright  Copyright (c) SENTQ 2016
 * @link       http://www.whmcms.com/
 */

if (!defined("WHMCS")){
    die("This file cannot be accessed directly");
}

use Illuminate\Database\Capsule\Manager as Capsule;
use WHMCS\View\Menu\Item as MenuItem;

add_hook("ClientAreaPage", 1, function($vars){
    
    /**
     * Overdue Invoices Limit
     *
     * If client has overdue invoices more than this number a restriction will be applied
     * Default is 0, mean if client has 1 or more overdue invoices
     */
    $overdueInvoicesLimit = 1;
    
    /**
     * Allow Access To Client Area Homepage
     *
     * True  = Allowed
     * False = Redirect To My Invoices
     */
    $allowAccessToCAHomePage = false;
    
    # Admin Logged-in as Client
    if (isset($_SESSION['adminid'])){
        return;
    }
    
    # ~
    $client = Menu::context('client');
    
    # Client Logged in
    if (is_null($client)===false){
        
        # Get Overdue Invoices
        $getInvoices = Capsule::table("tblinvoices")
        ->where("status", "UnPaid")
        ->where("userid", $client->id)
        ->where("duedate", "<", date("Y-m-d"))
        ->count();
        
        # Client Has 1 or more Overdue Invoices
        if ($getInvoices>intval($overdueInvoicesLimit)){
            
            $redirect = true;
            
            # Pages Where The Redirect Action Will Not Applied
            if (in_array($vars['filename'], array("viewinvoice", "creditcard"))){
                $redirect = false;
            }
            if ($vars['filename']=="clientarea" && in_array($_REQUEST['action'], array("invoices", "addfunds", "masspay", "supporttickets.php", "submitticket.php"))){
                $redirect = false;
            }
            
            if ($vars['filename']=="clientarea" && empty($_REQUEST['action']) && $allowAccessToCAHomePage===true){
                $redirect = false;
            }
            
            # Now Redirect Client to My Invoices
            if ($redirect===true){
                
                header("Location: clientarea.php?action=invoices");
                
                exit;
                
            }
            
        }
        
    }
    
});