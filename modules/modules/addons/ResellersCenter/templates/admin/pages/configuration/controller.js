{literal}
var RC_Configuration = 
{
    init: function()
    {
        $("#configurationForm .select2").select2();
        $("#configurationForm .checkbox-switch").bootstrapSwitch();
        
        this.relatedOptionsHandler();
    },
    
    relatedOptionsHandler: function()
    {
        //Reseller Invoice and Invoice Branding
        $("[name='settings[resellerInvoice]']").on("switchChange.bootstrapSwitch", function(event, state)
        {
            if(state)
            {
                $("[name='settings[gateways][]']").attr("disabled", true);
                $("[name='settings[invoiceBranding]']").bootstrapSwitch('state', true);
            }
            else
            {
                $("[name='settings[gateways][]']").attr("disabled", false);
            }
        });
        
        $("[name='settings[invoiceBranding]']").on("switchChange.bootstrapSwitch", function(event, state)
        {
            if(!state)
            {
                $("[name='settings[resellerInvoice]']").bootstrapSwitch('state', false);
            }
        });
    },
    
    submitConfigurationForm: function()
    {
        //We cannot send "/" as array key
        $("#configurationForm [name^='settings[emailTemplates]']").each(function(index, element)
        {
            var name = $(element).attr("name");
            if(name.indexOf("/")) 
            {
                name = name.replace("/", "-slash-");
                $(element).attr("name", name);
            }
        });

        var form = $("#configurationForm").serialize();
        JSONParser.request("saveConfiguration|configuration", form, function(){});
    }
}
RC_Configuration.init();
{/literal}