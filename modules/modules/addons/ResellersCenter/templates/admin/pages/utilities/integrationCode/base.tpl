{**********************************************************************
* ResellersCenter product developed. (2016-07-21)
*
*
*  CREATED BY MODULESGARDEN       ->       http://modulesgarden.com
*  CONTACT                        ->       contact@modulesgarden.com
*
*
* This software is furnished under a license and may be used and copied
* only  in  accordance  with  the  terms  of such  license and with the
* inclusion of the above copyright notice.  This software  or any other
* copies thereof may not be provided or otherwise made available to any
* other person.  No title to and  ownership of the  software is  hereby
* transferred.
*
*
**********************************************************************}

{**
* @author Paweł Złamaniec <pawel.zl@modulesgarden.com>
*}

<div id="integration" class="box light">
    <div class="box-title">
        <div class="caption">
            <i class="fa fa-code font-red-thunderbird"></i>
            <span class="caption-subject bold font-red-thunderbird uppercase">
                {$MGLANG->T('integrationCode','title')}
            </span>
        </div>
    </div>
    <div class="box-body">
        <h4><strong>{$MGLANG->T('integrationCodeStoreLogoHeader')}</strong></h4>
        {$MGLANG->T('integrationCodeOriginStoreLogo')}<br />

{literal}
<pre>
{if $assetLogoPath}
    &lt;a href="{$WEB_ROOT}/index.php" class="logo"&gt;&lt;img src="{$assetLogoPath}" alt="{$companyname}"&gt;&lt;/a&gt;
{else}
    &lt;a href="{$WEB_ROOT}/index.php" class="logo logo-text"&gt;{$companyname}&lt;/a&gt;
{/if}
</pre>
{/literal}
        {$MGLANG->T('integrationCodeReplacementStoreLogo')}<br />
{literal}
<pre>
{if $RCLogo}
    &lt;a href="{$WEB_ROOT}/index.php"&gt;&lt;img src="{$WEB_ROOT}/{$RCLogo}" alt="{$companyname}" &gt;&lt;/a&gt;
{else}
    {if $logo}
        &#x3C;p&#x3E;&#x3C;img src=&#x22;{$logo}&#x22; title=&#x22;{$companyname}&#x22; /&#x3E;&#x3C;/p&#x3E;
    {else}
        &#x3C;h2&#x3E;{$companyname}&#x3C;/h2&#x3E;
    {/if}
{/if}
</pre>
{/literal}
        {$MGLANG->T('integrationCodeOriginInvoiceLogo')}<br />
{literal}
<pre>
{if $logo}
    &#x3C;p&#x3E;&#x3C;img src=&#x22;{$logo}&#x22; title=&#x22;{$companyname}&#x22; /&#x3E;&#x3C;/p&#x3E;
{else}
    &#x3C;h2&#x3E;{$companyname}&#x3C;/h2&#x3E;
{/if}
</pre>
{/literal}
        {$MGLANG->T('integrationCodeReplacementInvoiceLogo')}<br />
{literal}
<pre>
{if $RCLogo}
    &lt;a href="{$WEB_ROOT}/index.php"&gt;&lt;img src="{$WEB_ROOT}/{$RCLogo}" alt="{$companyname}" &gt;&lt;/a&gt;
{else}
    {if $logo}
        &#x3C;p&#x3E;&#x3C;img src=&#x22;{$logo}&#x22; title=&#x22;{$companyname}&#x22; /&#x3E;&#x3C;/p&#x3E;
    {else}
        &#x3C;h2&#x3E;{$companyname}&#x3C;/h2&#x3E;
    {/if}
{/if}
</pre>
{/literal}
        
        {$MGLANG->T('logo4')}
            <pre>{literal}&lt;WHMCS_PATH&gt;/templates/six/invoicepdf_rename.tpl{/literal}</pre>
        
        {$MGLANG->T('logo5')}
        <hr>
        
        <h4><strong>{$MGLANG->T('resellerdepts1')}</strong></h4>
        {$MGLANG->T('resellerdepts2')}<br>
        {$MGLANG->T('resellerdepts3')}
        <pre>{literal}&lt;li&gt;
    &lt;a id=&quot;btnGetSupport&quot; href=&quot;submitticket.php&quot;&gt;
        &lt;i class=&quot;fa fa-envelope-o&quot;&gt;&lt;/i&gt;
        &lt;p&gt;
            {$LANG.getsupport} &lt;span&gt;&amp;raquo;&lt;/span&gt;
        &lt;/p&gt;
    &lt;/a&gt;
&lt;/li&gt;
{/literal}</pre>
        {$MGLANG->T('resellerdepts4')}
        <pre>{literal}{assign var=openTicketVar value='Open Ticket'}
{if $primaryNavbar.$openTicketVar}
    &lt;li&gt;
        &lt;a id=&quot;btnGetSupport&quot; href=&quot;submitticket.php&quot;&gt;
            &lt;i class=&quot;fa fa-envelope-o&quot;&gt;&lt;/i&gt;
            &lt;p&gt;
                {$LANG.getsupport} &lt;span&gt;&amp;raquo;&lt;/span&gt;
            &lt;/p&gt;
        &lt;/a&gt;
    &lt;/li&gt;
{/if}{/literal}</pre>
        <h4><strong>{$MGLANG->T('resellerinvoice1')}</strong></h4>
        {$MGLANG->T('resellerinvoice2')}
            <pre>{literal}&lt;WHMCS_PATH&gt;/templates/six/rcviewinvoice.tpl
&lt;WHMCS_PATH&gt;/templates/six/rccreditcard.tpl{/literal}</pre>
    </div>
</div>