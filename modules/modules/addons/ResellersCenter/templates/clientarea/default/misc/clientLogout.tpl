<script type='text/javascript'>
    $(document).ready(function ()
    {
        var menu = $("#top-nav");
        if(! menu.length) {
            menu = $(".top-nav");
        }
        
        menu.append('<div class="alert alert-info admin-masquerade-notice">{$MGLANG->T('addonCA', 'loggedasclient')}<br><a href="index.php?m=ResellersCenter&mg-page=clients&mg-action=returnToRc&json=1" class="alert-link">{$MGLANG->T('addonCA','returntorc')}</a></div>');
    });
</script>

