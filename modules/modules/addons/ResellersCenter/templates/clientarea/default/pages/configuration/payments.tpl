<div class="box light">
    <div class="box-title tabbable-line">

        <ul class="nav nav-tabs nav-left">
            <li class="show-draggable">
                <a><i class="glyphicon glyphicon-move"></i></a>
            </li>
            
            {foreach from=$gateways key=index item=gateway name=gatewayTabsName}
                <li class="{if $smarty.foreach.gatewayTabsName.index eq 0}active{/if}">
                    <a href="#RCPayments{$index}" data-toggle="tab" data-gateway="{$gateway->name}">
                        {$gateway->adminName}
                    </a>
                </li>
            {/foreach}
        </ul>
        
    </div>
    <div class="box-body" style='min-height: 320px;'>
                
        <div class="tab-content">
            {foreach from=$gateways key=index item=gateway name=gatewayTabsContent}
                <div id="RCPayments{$index}" class="tab-pane {if $smarty.foreach.gatewayTabsContent.index eq 0}active{/if}" >
                    <div class="scroller">
                        <form class="paymentGatewayForm">
                            {$gateway->config(['gateways', $gateway->name])}
                        </form>
                    </div>
                </div>
            {/foreach}
        </div>
        
    </div>
</div>