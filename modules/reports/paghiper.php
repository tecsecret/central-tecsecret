<?php

/**
 * PagHiper Balance and Monthly Report
 * @author: Álvaro Meireles
 * @version 1.0.0
 * @copyright 2019 Álvaro Meireles (whatsapp p/ outros serviços: 71991242047)
 */

if (!defined("WHMCS"))
{
	die("This file cannot be accessed directly");
}

use WHMCS\Database\Capsule;

$reportdata["monthspagination"] = true;

$datefilter = $year.'-'.$month.'%';
$html = '<table class="table table-striped">
			<thead>
			<tr>
			<td>Nome do Cliente</td>
			<td>E-mail</td>
			<td>CPF/CNPJ</td>
			<td>Fatura</td>
			<td>Valor Bruto</td>
			<td>Liquido Recebido</td>
			<td>Tarifa</td>
			<td>Pagamento Confirmado</td>
		</tr>
	</thead>
<tbody>
';

function connectApi($year, $month)
{
    foreach(Capsule::table('tblpaymentgateways')->WHERE('gateway', '=', 'paghiper')->get() as $paghiper)
    {
        if($paghiper->setting === 'api_key')   $api  = $paghiper->value;
        if($paghiper->setting === 'token')    $token = $paghiper->value;
        if($paghiper->setting === 'cpf_cnpj') $cpfid = $paghiper->value;
    }

    $isday = date("t", mktime(0, 0, 0, $month, '01' , $year));

    if(isset($api) && isset($token))
    {
        $data_post = json_encode(
            array(
                   'apiKey'       => $api,
                   'token'        => $token,
                   'status'       => 'paid',
                   'initial_date' => $year . '-'. $month .'-01',
                   'final_date'   => $year . '-'. $month .'-' . $isday,
                   'filter_date'  => 'paid_date'
            )
        );

        $url = "https://api.paghiper.com/transaction/list/";
        $mediaType = "application/json";
        $charSet   = "UTF-8";
        $headers   = array();
        $headers[] = "Accept: " . $mediaType;
        $headers[] = "Accept-Charset: " . $charSet;
        $headers[] = "Accept-Encoding: " . $mediaType;
        $headers[] = "Content-Type: " . $mediaType . ";charset=" . $charSet;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_post);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($ch);
        $json = json_decode($result, true);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $return = json_decode($result, true);
        return array(
            'httpCode' => $httpCode,
            'api'      => $return,
            'cpfid'    => $cpfid
        );
    }
    return FALSE;
}

function _format($int)
{
	return number_format($int, 2, ',', '.');
}

function _getclientid($cpf, $fieldid)
{
	foreach(Capsule::table('tblcustomfieldsvalues')->WHERE('fieldid', $fieldid)->get() as $field)
	{
		$field->value = str_replace('-', '', $field->value);
		$field->value = str_replace('.', '', $field->value);
		if($cpf === $field->value)
		{
			return $field->relid;
		}
	}
}

function _formatCnpjCpf($value)
{
  $cnpj_cpf = preg_replace("/\D/", '', $value);
  if(strlen($cnpj_cpf) === 11)
  {
    return preg_replace("/(\d{3})(\d{3})(\d{3})(\d{2})/", "\$1.\$2.\$3-\$4", $cnpj_cpf);
  } 
  return preg_replace("/(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/", "\$1.\$2.\$3/\$4-\$5", $cnpj_cpf);
}

$return = connectApi($year, $month);
if($return !== FALSE && !isset($return['transaction_list_request']['response_message']))
{
	$all = 0;
	$fee = 0;
	if($return['httpCode'] == 201)
	{
		foreach($return['api']['transaction_list_request']['transaction_list'] as $k => $v)
		{
			$clientid = _getclientid($return['api']['transaction_list_request']['transaction_list'][$k]['payer_cpf_cnpj'], $return['cpfid']);
			$html .= "<tr style=\"cursor:pointer;\">";
			$html .= '<th><a href="clientssummary.php?userid='.$clientid.'">'. $return['api']['transaction_list_request']['transaction_list'][$k]['payer_name'] .'</a></th>';
			$html .= '<th>'. $return['api']['transaction_list_request']['transaction_list'][$k]['payer_email'] .'</th>';
			$html .= '<th>'. _formatCnpjCpf($return['api']['transaction_list_request']['transaction_list'][$k]['payer_cpf_cnpj']) .'</th>';
			$html .= '<th>'. $return['api']['transaction_list_request']['transaction_list'][$k]['items'][0]['item_id'] .'</th>';
			$html .= '<th>R$ '. _format($return['api']['transaction_list_request']['transaction_list'][$k]['value_cents'] / 100) .'</th>';
			$html .= '<th>R$ '. _format((($return['api']['transaction_list_request']['transaction_list'][$k]['value_cents'] - $return['api']['transaction_list_request']['transaction_list'][$k]['value_fee_cents']) / 100)) .'</th>';
			$html .= '<th>R$ '. _format($return['api']['transaction_list_request']['transaction_list'][$k]['value_fee_cents'] / 100) .'</th>';
			$html .= '<th>'. date('d/m/Y H:i:s', strtotime($return['api']['transaction_list_request']['transaction_list'][$k]['paid_date'])) .'</th>';
			$html .= '</tr>';
			$all += (($return['api']['transaction_list_request']['transaction_list'][$k]['value_cents'] - $return['api']['transaction_list_request']['transaction_list'][$k]['value_fee_cents']) / 100);
			$fee += ($return['api']['transaction_list_request']['transaction_list'][$k]['value_fee_cents'] / 100);
		}
	}

	$html .= '</tbody></table>';
	$html .= '
	<script>
	$(document).ready(function(){
		$("table").DataTable();
	});
	</script>
	';
	echo '<div class="alert alert-success text-center">Valor Total: R$ '. _format($all) .'<br />Tarifas: R$ '. _format($fee) .'</div>';
	echo $html;
}
else
{
	if(isset($return['transaction_list_request']['response_message']))
	{
		echo '<div class="alert alert-danger text-center">'. $return['transaction_list_request']['response_message'] .'</div>';
		return;
	}
	echo '<div class="alert alert-danger text-center">API ou Token do PagHiper não configurados na tabela: `tblpaymentgateways` do seu WHMCS<br /><small>o sistema procura pelos valores `api_key` e `token` na tabela mencionada acima</div>';
}