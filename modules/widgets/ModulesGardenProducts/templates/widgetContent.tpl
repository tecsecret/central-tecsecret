<link rel="stylesheet" type="text/css"  href="../modules/widgets/ModulesGardenProducts/style.css" />
<div style="padding: 5px; text-align: center; "><img style="width: 75%;" src="../modules/widgets/ModulesGardenProducts/logo.png" alt="ModulesGarden Logo" /></div>

{foreach from=$promotionNotes key=pKey item=pNote}
    <div class="mgPromoBox">{$pNote|html_entity_decode}</div>
{/foreach}

{if $showClientModules}
    <div style="overflow: auto; max-height: {$productListHeight}px;">
        <table cellspacing="0" cellpadding="0" class="table-widget">
            <thead>
                <tr>
                    <th>Module Name</th>
                    <th>Version</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                {foreach from=$clientModules key=modKey item=$modDetails}
                    <tr>
                        <td>{$modDetails.name}</td>
                        <td>{$modDetails.localVersion}</td>
                        {if $modDetails.newVersionAvailable}
                            <td style="text-align: right;"><a target="_blank" class="check-it" href="{$modDetails.siteUrl}">Update Available</a></td>
                        {else}
                            <td style="text-align: right;">Up to date</td>
                        {/if}
                    </tr>
                {/foreach}
            </tbody>
        </table>
    </div>
{/if}

<a style='clear: both' data-tweet-limit='1' data-chrome='nofooter noheader' height='350px'class="twitter-timeline" href="https://twitter.com/ModulesGarden?ref_src=twsrc%5Etfw"></a>

<div class="footer">
    <a href="javascript:;" {if !$showTwitter} style="display: none"{/if} class="show-twitter">Show Twitter</a>
    <a href="javascript:;" {if $showTwitter} style="display: none"{/if} class="hide-twitter">Hide Twitter</a>
</div>

{literal}
    <script>
        function loadMGTwitter()
        {
            !function (d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0], p = /^http:/.test(d.location) ? 'http' : 'https';
                if (!d.getElementById(id)) {
                    js = d.createElement(s);
                    js.id = id;
                    js.src = 'https://platform.twitter.com/widgets.js';
                    js.setAttribute('onload', "twttr.events.bind('rendered',function(e) {  $('.home-widgets-container').masonry().masonry('reloadItems'); })");
                    fjs.parentNode.insertBefore(js, fjs);
                }
            }(document, 'script', 'twitter-wjs');
        }
    </script>
{/literal}
<script type="text/javascript">
    $(function () {
        $(".hide-twitter").click(function () {
            $.post(document.location.toString(), "aaa=1&hidemgtwitter=1&ajax=1", function () {
                $("#twitter-widget-0").hide();
                $(".show-twitter").show();
                $(".hide-twitter").hide();
                $('.home-widgets-container').masonry().masonry('reloadItems');
            });
        });

        $(".show-twitter").click(function () {
            $.post(document.location.toString(), "aaa=1&hidemgtwitter=0&ajax=1", function () {
                loadMGTwitter();
                $("#twitter-widget-0").show();
                $(".show-twitter").hide();
                $(".hide-twitter").show();
                $('.home-widgets-container').masonry().masonry('reloadItems');
            });
        });
    });
{if $autoloadTwitter}
    loadMGTwitter(); 
    $("#twitter-widget-0").show();
    $(".show-twitter").hide();
    $(".hide-twitter").show();
{/if}    
</script>

<style type="text/css">
    #twitter-widget-0 
    {   
        margin-top: 15px!important;
        width:100%!important;
    }
    .show-twitter,
    .hide-twitter
    {
        float: right;
        text-decoration: none;
        font-size: 12px;
        color: #202f60;
        padding: 0 6px 6px;
    }
    .show-twitter {
        padding-top: 5px;
    }    
</style>
