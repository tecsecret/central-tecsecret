<script type="text/javascript">
    function ModulesGardenProductsWidget() {
        $("#ModulesGardenProductsWidget").html("<p style=\"text-align: center\"><img src=\"images/loading.gif\"/></p>");
        jQuery.post(document.location.toString(), "GetModulesGardenProducts=1&ajax=1", function (data) {
            var MGLoadWidget = new Promise(function(resolve, reject) {
                $("#ModulesGardenProductsWidget").html(data);
                resolve(1);
            });
            MGLoadWidget.then(function(value) {
                setTimeout(function(){ 
                    $('.home-widgets-container').masonry().masonry('reloadItems');
                }, 100);
            });
        });
    }
    jQuery(function () {
        ModulesGardenProductsWidget();
    });
</script>
<style type="text/css">
    .mg-promo
    {
        padding: 5px;
        border: 3px #178DBE solid;
        border-radius: 5px;
    }
    #ModulesGardenProductsWidget {
        min-height: 300px;
    }
</style>
<div id="ModulesGardenProductsWidget"></div>
