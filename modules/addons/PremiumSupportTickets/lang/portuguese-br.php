<?php

/**
 * Default language file for module
 * @author Piotr Sarzyński <piotr.sa@modulesgarden.com>
 */


/****************************************************************************************
 *                        Idioma Português-BR 
 ****************************************************************************************/

// CLIENT AREA MESSAGES
$PremiumSupportTicketsLanguage['clientarea_message_on_can_create_in_dept']  = 'Diposnível para criar Ticket';
$PremiumSupportTicketsLanguage['clientarea_message_on_cant_create_in_dept'] = 'Não é possível criar ticket';
$PremiumSupportTicketsLanguage['clientarea_message_on_missingpoints']       = 'Pontos de crédito de falta de suporte: ';
$PremiumSupportTicketsLanguage['clientarea_message_on_requiredpoints']      = 'Pontos de crédito de suporte necessários: ';
$PremiumSupportTicketsLanguage['clientarea_message_on_havepoints']          = 'Seus Pontos de Crédito para Suporte: ';
$PremiumSupportTicketsLanguage['clientarea_message_on_headeroncreate']      = 'Pontos de Crédito de Apoio necessários para abrir este ticket: ';
$PremiumSupportTicketsLanguage['clientarea_message_on_create_havepoints']   = 'Pontos de Crédito de Suporte (<i>SCP</i>): ';

$PremiumSupportTicketsLanguage['clientarea_message_cantcreateinthisdept']   = 'Você não tem SCP suficiente para criar um tricket neste Departamento.';
$PremiumSupportTicketsLanguage['clientarea_message_somethingiswrong']       = 'Problem occurred for this Ticket Department. Please, contact administrator.';
$PremiumSupportTicketsLanguage['clientarea_message_notenoughpoints']        = 'Not enough SCP to create a ticket with a selected priority!';
$PremiumSupportTicketsLanguage['clientarea_message_departmentnotconfigured']= 'This Ticket Department is not configured. Please, contact administrator.';




// rules - list
$PremiumSupportTicketsLanguage['rules_list_top_description']     = 'List of defined rules. Each rule represents a single relation that will be checked upon client action. All rules that match client action will be applied (client will receive Support Credit Points for each matching rule).';
$PremiumSupportTicketsLanguage['rules_list_tbl_th_name']         = 'Name';
$PremiumSupportTicketsLanguage['rules_list_tbl_th_type']         = 'Type';
$PremiumSupportTicketsLanguage['rules_list_tbl_th_apply']        = 'Apply To';
$PremiumSupportTicketsLanguage['rules_list_tbl_th_depts']        = 'Ticket Departments';
$PremiumSupportTicketsLanguage['rules_list_tbl_th_credits']      = 'SC Points';
$PremiumSupportTicketsLanguage['rules_list_tbl_th_status']       = 'Status';
$PremiumSupportTicketsLanguage['rules_list_tbl_th_actions']      = 'Actions';
$PremiumSupportTicketsLanguage['rules_list_points_full']         = 'Support Credit Points';
$PremiumSupportTicketsLanguage['rules_list_delete_cornfirm']     = 'Are you sure you want to delete the rule?';

$PremiumSupportTicketsLanguage['rules_list_tbl_undefined']       = 'Undefined Rule';
$PremiumSupportTicketsLanguage['table_empty']                    = 'Nothing to display';
$PremiumSupportTicketsLanguage['rules_list_add_rule']            = 'Add Rule';

$PremiumSupportTicketsLanguage['rules_list_product_type']        = 'Product';
$PremiumSupportTicketsLanguage['rules_list_product_all']         = 'Every Product';
$PremiumSupportTicketsLanguage['rules_list_product_multiple']    = 'Products';
$PremiumSupportTicketsLanguage['rules_list_productgrp_type']     = 'Product Group';
$PremiumSupportTicketsLanguage['rules_list_productgrp_all']      = 'Every Group';
$PremiumSupportTicketsLanguage['rules_list_productgrp_multiple'] = 'Groups';
$PremiumSupportTicketsLanguage['rules_list_addon_type']          = 'Product Addon';
$PremiumSupportTicketsLanguage['rules_list_addon_all']           = 'Every Addon';
$PremiumSupportTicketsLanguage['rules_list_addon_multiple']      = 'Addons';

$PremiumSupportTicketsLanguage['rules_list_depts_all']           = 'Every Ticket Department';
$PremiumSupportTicketsLanguage['rules_list_depts_multiple']      = 'Departments';

$PremiumSupportTicketsLanguage['rules_list_tbl_disable']         = 'Disable';
$PremiumSupportTicketsLanguage['rules_list_tbl_enable']          = 'Enable';
$PremiumSupportTicketsLanguage['rules_list_tbl_edit']            = 'Edit';


// rules - add
$PremiumSupportTicketsLanguage['rules_add_add']                   = 'Add New Rule';
$PremiumSupportTicketsLanguage['rules_add_configtype']            = 'Rule Type';
$PremiumSupportTicketsLanguage['rules_add_apply']                 = 'Required Items';
$PremiumSupportTicketsLanguage['rules_add_rules']                 = 'Departments, Points & Billing Settings';
$PremiumSupportTicketsLanguage['rules_add_step']                  = 'Step';
$PremiumSupportTicketsLanguage['rules_add_type_required']         = 'Configuration Type is required!';

$PremiumSupportTicketsLanguage['rules_add_cfg_select']            = 'Select';
$PremiumSupportTicketsLanguage['rules_add_cfg_name']              = 'Configuration Name';
$PremiumSupportTicketsLanguage['rules_add_cfg_name_friendly']     = 'Type in the name of configuration to manage your rules easily, this field is required';
$PremiumSupportTicketsLanguage['rules_add_cfg_products']          = 'For Selected Products';
$PremiumSupportTicketsLanguage['rules_add_cfg_products_descr']    = 'In this rule type, you can determine the list of <b>products</b>, which, when bought by a Client, can provide them Support Credit Points.<br /><br />';
$PremiumSupportTicketsLanguage['rules_add_cfg_productsgrp_descr'] = 'In this rule type, you can determine the list of <b>product groups</b>. Client may buy products belonging to that group and then Support Credit Points can be added according to this rule configuration.<br /><br />';
$PremiumSupportTicketsLanguage['rules_add_cfg_addons_descr']      = 'In this rule type, you can determine the list of <b>product addons</b> that a client can buy in order to be given Support Credit Points, accordingly to the rule.<br /><br />';
$PremiumSupportTicketsLanguage['rules_add_cfg_productsgrp']       = 'For Selected Product Groups';
$PremiumSupportTicketsLanguage['rules_add_cfg_addons']            = 'For Selected Product Addons';

// step2
$PremiumSupportTicketsLanguage['rules_add_selectall']                    = 'Select All';
$PremiumSupportTicketsLanguage['rules_add_step2_product_descr']          = 'Select products that will be required to apply the rule. The rule will be applied and points granted when a client purchases one of selected here products and order with this product is accepted.';
$PremiumSupportTicketsLanguage['rules_add_canselectmore']                = 'You can select more than just one';
$PremiumSupportTicketsLanguage['rules_add_step2_product']                = 'Products';
$PremiumSupportTicketsLanguage['rules_add_step2_product_holder']         = 'Select required products';
$PremiumSupportTicketsLanguage['rules_add_step2_product_moreinstead']    = 'Apply the rule to all your products';
$PremiumSupportTicketsLanguage['rules_add_step2_productgrp_moreinstead'] = 'Instead of selecting a single group, apply the rule to all of them';
$PremiumSupportTicketsLanguage['rules_add_step2_productgrp_descr']       = 'Here you can configure which products groups will be required to apply the rule.  The rule will be applied when a client purchases a product that belongs to a selected group.';
$PremiumSupportTicketsLanguage['rules_add_step2_productgrp']             = 'Groups';
$PremiumSupportTicketsLanguage['rules_add_step2_productgrp_holder']      = 'Apply the rule to all your groups';
$PremiumSupportTicketsLanguage['rules_add_step2_addons_descr']           = 'Here you can configure required addons to apply the rule. The rule will be applied when a client purchases one of selected product addons and order with this product addon is accepted.';
$PremiumSupportTicketsLanguage['rules_add_step2_addons']                 = 'Addons';
$PremiumSupportTicketsLanguage['rules_add_step2_addons_holder']          = 'Apply the rule to all of the product addons';
$PremiumSupportTicketsLanguage['rules_add_step2_addons_moreinstead']     = 'Apply the rule to all of the product addons';

// step3
$PremiumSupportTicketsLanguage['rules_add_step3_ticketdepts']           = 'Ticket Departments';
$PremiumSupportTicketsLanguage['rules_add_step3_ticketdepts_holder']    = 'Department to apply Support Credit Points';
$PremiumSupportTicketsLanguage['rules_add_step3_ticketdepts_instead']   = 'Apply the rule to all Ticket Departments';
$PremiumSupportTicketsLanguage['rules_add_step3_whenadd']               = 'Support Credit Points Frequency';
$PremiumSupportTicketsLanguage['rules_add_step3_whenadd_holder']        = 'Support Credit Points will be added once or in recurring mode';
$PremiumSupportTicketsLanguage['rules_add_step3_onetime']               = 'One Time';
$PremiumSupportTicketsLanguage['rules_add_step3_recuring']              = 'Recurring';
$PremiumSupportTicketsLanguage['rules_add_step3_recuring_select']       = 'Select one from list above';
$PremiumSupportTicketsLanguage['rules_add_step3_howmanycredits']        = 'Support Credit Points';
$PremiumSupportTicketsLanguage['rules_add_step3_howmanycredits_holder'] = 'Number of Support Credit Points';
$PremiumSupportTicketsLanguage['rules_add_step3_howmanycredits_descr']  = 'Number of credits that will be added to the client account (removed, if a negative value is inserted)';
$PremiumSupportTicketsLanguage['rules_add_step3_active']                = 'Active';
$PremiumSupportTicketsLanguage['rules_add_step3_active_descr']          = 'Enable rule by default';



// clients - list
$PremiumSupportTicketsLanguage['clients_tbl_th_client']             = 'Cliente';
$PremiumSupportTicketsLanguage['clients_tbl_th_details']            = 'Detalhes';
$PremiumSupportTicketsLanguage['clients_tbl_th_total']              = 'Total';
$PremiumSupportTicketsLanguage['clients_tbl_th_actions']            = 'Ações';
$PremiumSupportTicketsLanguage['clients_departments']               = 'Departamento do Ticket';
$PremiumSupportTicketsLanguage['clients_departments_many']          = 'Ticket Departamentos';
$PremiumSupportTicketsLanguage['clients_detailed_show']             = 'Show Detailed Information';
$PremiumSupportTicketsLanguage['clients_detailed_hide']             = 'Hide Detailed Information';
$PremiumSupportTicketsLanguage['clients_havepoints']                = 'Support Credit Points in';
$PremiumSupportTicketsLanguage['clients_no_points']                 = 'No Support Credit Points';
$PremiumSupportTicketsLanguage['clients_th_dept']                   = 'Ticket Department Name';
$PremiumSupportTicketsLanguage['clients_th_points']                 = 'Creditos de Solicitações de Suporte';
$PremiumSupportTicketsLanguage['clients_modify']                    = 'Modificar Créditos de Suporte';

$PremiumSupportTicketsLanguage['clients_search']                    = 'Buscar';
$PremiumSupportTicketsLanguage['clients_search_client']             = 'Filter By Client';
$PremiumSupportTicketsLanguage['clients_search_clientdescr']        = 'Type in client ID or name.';
$PremiumSupportTicketsLanguage['clients_search_search_filter']      = 'Buscar';
$PremiumSupportTicketsLanguage['clients_search_search_filterreset'] = 'Reset';
$PremiumSupportTicketsLanguage['clients_filter_holder']             = 'Type in client ID or name.';
$PremiumSupportTicketsLanguage['pagination_showing']                = 'Showing';
$PremiumSupportTicketsLanguage['pagination_showing_to']             = 'to';
$PremiumSupportTicketsLanguage['pagination_showing_of']             = 'of';
$PremiumSupportTicketsLanguage['pagination_showing_results']        = 'result\'s';

// client - modify
$PremiumSupportTicketsLanguage['clients_modify_h1']                 = 'Add/Remove Support Credit Points For';
$PremiumSupportTicketsLanguage['clients_modify_descr']              = '<span class="label label-important">Notice!</span> You can remove Support Credit Points by inserting negative values in the fields above';
$PremiumSupportTicketsLanguage['clients_modify_save']               = 'Save';

// departments
$PremiumSupportTicketsLanguage['departments_tbl_th_name']           = 'Nome do Departamento de Suporte';
$PremiumSupportTicketsLanguage['departments_tbl_th_details']        = 'Actions';
$PremiumSupportTicketsLanguage['departments_no_configured']         = 'This Ticket Department is not Configured yet!';
$PremiumSupportTicketsLanguage['departments_detailed_show']         = 'Motrar Detalhes';
$PremiumSupportTicketsLanguage['departments_detailed_hide']         = 'Esconder Detalhes';
$PremiumSupportTicketsLanguage['departments_descr']                 = 'Here you can configure the value of tickets in points for every of your support Ticket Departments. Enter the number of required points to open a ticket and set up additional price per status.';
$PremiumSupportTicketsLanguage['departments_required_points']       = 'Pontos de Crédito';
$PremiumSupportTicketsLanguage['departments_required_points_descr'] = 'Minimum number of Support Credit Points required to open a ticket in the department. Once created, points will be removed from the client\'s account.';
$PremiumSupportTicketsLanguage['departments_statuses_points']       = 'Points Per Status';
$PremiumSupportTicketsLanguage['departments_status_low']            = 'Low';
$PremiumSupportTicketsLanguage['departments_status_normal']         = 'Normal';
$PremiumSupportTicketsLanguage['departments_status_high']           = 'High';
$PremiumSupportTicketsLanguage['departments_zero']                  = 'You have no support ticket departments defined. There is nothing to configure here.';
$PremiumSupportTicketsLanguage['departments_save']                  = 'Salvar';

// integration code's
$PremiumSupportTicketsLanguage['integration_cde_h1']          = 'Support Ticket Departments List';
$PremiumSupportTicketsLanguage['integration_cde_h1']          = 'Ticket Creation Form';
$PremiumSupportTicketsLanguage['integration_cde_infile']      = 'In File:';
$PremiumSupportTicketsLanguage['integration_cde_replace']     = 'Replace:';
$PremiumSupportTicketsLanguage['integration_cde_replacewith'] = 'With:';
$PremiumSupportTicketsLanguage['integration_cde_after']       = 'After code:';
$PremiumSupportTicketsLanguage['integration_cde_add']         = 'Add:';
$PremiumSupportTicketsLanguage['integration_cde_andreplace']  = 'And replace:';
$PremiumSupportTicketsLanguage['integration_errorhandler']    = 'Adicione o fragmento de código abaixo no início do arquivo para exibir erros:';

$PremiumSupportTicketsLanguage['integrationV5']    = 'Template Five';
$PremiumSupportTicketsLanguage['integrationDefault']    = 'Template Default';
$PremiumSupportTicketsLanguage['integrationV6']    = 'Tema Six';

$PremiumSupportTicketsLanguage['rules_edit_edit']                   = 'Edit Rule';

$PremiumSupportTicketsLanguage['userNegativeBalance'] = 'This user has negative balance of Support Credit Points';
$PremiumSupportTicketsLanguage['in'] = 'in';
$PremiumSupportTicketsLanguage['department'] = 'departamento';
