<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="{$charset}" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>{$companyname} - {$pagetitle}</title>
  <!-- Bootstrap -->
  <link href="{$BASE_PATH_CSS}/bootstrap.min.css" rel="stylesheet">
 <link href="{$WEB_ROOT}/assets/css/fontawesome-all.min.css" rel="stylesheet">

  <script src="{$WEB_ROOT}/templates/{$template}/js/scripts.min.js?v={$versionHash}"></script>
  <script src="{$WEB_ROOT}/modules/addons/clean_html_invoice/assets/js/JsBarcode.js"></script>
  <script src="{$WEB_ROOT}/modules/addons/clean_html_invoice/assets/js/CODE128.js"></script>

  {if $barcode_output!="Disabled"}
   {literal}
    <script>
    jQuery(document).ready(function(){
      jQuery("#barcode").JsBarcode("{/literal}{$invoiceid}{literal}",{
        {/literal}
        {if $barcode_output=="Code 128 Accent Color"}
          lineColor: "{$primary_color}",
        {/if}
        {literal}
        width:3,
        height:20,
        format:"CODE128",
      });
    });
    </script>
  {/literal}
  {/if}

  {if $LANG.locale == 'ar_AR' || $LANG.locale == 'he_IL'}
  <link href="{$WEB_ROOT}/modules/addons/clean_html_invoice/assets/css/bootstrap-rtl.min.css" rel="stylesheet">
  {/if}
  {if isset($fonts_html)}
  {$fonts_html}
  {/if}

  <style>
  {literal}
  body { background-color: #f5f5f7; }
  h1, h2 { font-weight: 100; }
  h5 { text-transform: uppercase; letter-spacing: 1.5px; }
  .table>thead:first-child>tr:first-child>td { text-transform: uppercase; letter-spacing: 1.5px; font-weight: 400; padding: 15px 45px; }
  .table>tbody>tr>td { padding: 5px 45px; border-top: 0; }
  .table>tbody>tr.active>td { background-color: #fafafa; }
  input[type=submit] { display: inline-block; margin-bottom: 0; font-size: 14px; font-weight: 400; line-height: 1.42857143; text-align: center; white-space: nowrap; vertical-align: middle; -ms-touch-action: manipulation; touch-action: manipulation; cursor: pointer; -webkit-user-select: none; -moz-user-select: none; -ms-user-select: none; user-select: none; background-image: none; border: 1px solid transparent; border-radius: 4px; border-color: #ccc; padding: 6px 12px; }
  .draft { color: #999; border: 1px solid #999; }
  .draft, .unpaid { padding: 5px 15px; }
  .unpaid { color: #c00; border: 1px solid #c00; }
  .paid { color: #779500; border: 2px solid #779500; }
  .paid, .refunded { padding: 5px 15px; }
  .refunded { color: #248; border: 1px solid #248; }
  .cancelled { color: #888; border: 1px solid #888; padding: 5px 15px; }
  .collections { color: #fc0; border: 1px solid #fc0; padding: 5px 15px; }
  .invoice-container .payment-btn-container { padding-top: 15px; }
  .invoice-container .payment-btn-container table { float: right; }
  .invoice-status { text-transform: uppercase; letter-spacing: 1.5px; font-size: 18px; font-weight: 600; margin-top: 24px; }
  .table-invoice { width: 100%; max-width: 100%; margin-bottom: 60px; }
  .table-invoice-header>tbody>tr.primary>td { font-size: 30px; }
  .table-invoice-items>thead>tr>td, .table-transactions>thead>tr>td { padding: 5px 30px; }
  .table__cell { padding: 15px 30px; }
  .barcode__cell>td { padding-right: 20px; }
  .table-balance .table__cell, .table-invoice-totals .table__cell { padding: 7px 30px 7px 15px; }
  .table-transactions { margin-bottom: 0; }
  .table-balance { margin-bottom: 15px; }
  .table-transactions .table__cell { padding: 7px 30px; }
  .logo img { max-height: 100px; max-width: 150px; }
  .vertical-view-line { margin: -20px 0 0 40px; border: 2px solid #d0dbe1; height: 20px; width: 0; }
  .logo { margin-top: 18px; margin-bottom: 18px; }
  .invoice-container { background: #fff; overflow: hidden; border: 1px solid #e3e3e3; padding: 20px 0; }
  .well-notes { background-color: #fff; }
  .btn-link { color: #333; }
  @media (min-width:768px) {
    .container { width: 770px !important; }
    .invoice-container, .invoice-container>div { width: 740px; }
  }
  @media (min-width:992px) {
    .container { width: 870px !important; }
    .invoice-container, .invoice-container>div { width: 840px; min-height:1030px; }
  }
  @media (min-width:1200px) {
    .container { width: 870px !important; }
    .invoice-container>div { width: 840px; min-height:1130px;}
  }
  @media only screen and (max-width:480px) {
    .table-invoice-totals { margin-left: -15px; }
    .well .pull-right, .well .pull-left { text-align: center; float:none!important; } 
  }

 @media print {
 .col-sm-1, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-sm-10, .col-sm-11, .col-sm-12 { float: left; }
 .col-sm-12 { width: 100%; }
 .col-sm-11 { width: 91.66666667%; }
 .col-sm-10 { width: 83.33333333%; }
 .col-sm-9 { width: 75%; }
 .col-sm-8 { width: 66.66666667%; }
 .col-sm-7 { width: 58.33333333%; }
 .col-sm-6 { width: 50%; }
 .col-sm-5 { width: 41.66666667%; }
 .col-sm-4 { width: 33.33333333%; }
 .col-sm-3 { width: 25%; }
 .col-sm-2 { width: 16.66666667%; }
 .col-sm-1 { width: 8.33333333%; }
 .visible-xs { display: none !important; }
 .hidden-xs { display: block !important; }
 table.hidden-xs { display: table; }
 tr.hidden-xs { display: table-row !important; }
 td.hidden-xs, th.hidden-xs { display: table-cell !important; }
 .hidden-sm, .hidden-xs.hidden-print { display: none !important; }
 .visible-sm { display: block !important; }
 table.visible-sm { display: table; }
 tr.visible-sm { display: table-row !important; }
 td.visible-sm, th.visible-sm { display: table-cell !important; }
 .box_footer {min-height: initial !important;}
 }

.row.is-flex { display: flex; flex-wrap: wrap; }
.row.is-flex>[class*=col-] { display: flex; flex-direction: column; }
.row.is-flex { -ms-flex-wrap: wrap; flex-wrap: wrap; }
.row.is-flex, .row.is-flex>[class*=col-] { display: -webkit-box; display: -ms-flexbox; display: flex; }
.row.is-flex>[class*=col-] { -webkit-box-orient: vertical; -webkit-box-direction: normal; -ms-flex-direction: column; flex-direction: column; }
.align-items-end { -webkit-box-align: end!important; -ms-flex-align: end!important; align-items: flex-end!important; }
.row.is-flex:after, .row.is-flex:before { display:none; }

#fullpage-overlay{display:table;position:fixed;z-index:1000;top:0;left:0;width:100%;height:100%;background-color:rgba(0,0,0,0.8);color:#fff;}
#fullpage-overlay .outer-wrapper{position:relative;height:100%;}
#fullpage-overlay .inner-wrapper{position:absolute;top:50%;left:50%;height:30%;width:50%;text-align:center;margin:-3% 0 0 -25%;}
#fullpage-overlay .msg{display:inline-block;max-width:400px;padding:20px;}

  {/literal}
  body { font-family: {$body_font}; }
  h1,h2,h3,h4,h5,h6 { font-family: {$header_font}; }
  strong { font-family: {$header_font}; }
  .btn-credit { color: #fff!important; background-color: {$primary_color}!important; border-color: {$primary_color}!important; }
  .table-invoice>tbody>tr.primary>td { background-color: {$primary_color}; color: #fff; font-weight: 100; }
  .table-invoice>tbody>tr.secondary>td { background-color: {$secondary_color}; }
  {if $page_decorations=="1"}
    .invoice-container > div { box-shadow: 0 0 0 10px {$secondary_color}, 0 0 0 20px {$primary_color}; }
  {/if}
  .well-credit { background-color:#fff; border-top: 4px solid {$primary_color} }
  .table-invoice-items>tbody>tr:nth-child(even) { background-color: {$table_color_even}; }
  .table-invoice-items>tbody>tr:nth-child(odd) { background-color: {$table_color}; }

  {if $LANG.locale == 'ar_AR' || $LANG.locale == 'he_IL'}
    .box_head { margin-top:60px;margin-bottom:30px; margin-right: 15px; }
    .box_total, .box_head_mob, .box_footer { margin-right: 15px; }
    .box_footer { min-height:340px; padding: 0 0px; }
  {else}
    .box_head { margin-top:60px;margin-left:30px;margin-bottom:30px; }
    .box_total { margin-left:15px; }
    .box_footer { min-height:340px; padding:15px; }
  {/if}

  {if $header_style == 'regular'}
    .table-invoice>tbody>tr.primary>td,
    .table-invoice-items>thead>tr>td,
    .table-transactions>thead>tr>td,
    .box_total h5 { text-transform: none; letter-spacing: 1px; }
  {else}
      {if $header_style == 'uppercase'}
        .table-invoice>tbody>tr.primary>td,
        .table-invoice-items>thead>tr>td,
        .table-transactions>thead>tr>td,
        .box_total h5  {  text-transform: uppercase; letter-spacing: 1px; }
      {else}
        .table-invoice>tbody>tr.primary>td,
        .table-invoice-items>thead>tr>td,
        .table-transactions>thead>tr>td,
        .box_total h5 {  text-transform: uppercase; letter-spacing: 1.5px; }
      {/if}
  {/if}

  </style>
</head>
<body>
  {$clean_html_invoice_settmpvar}

  <div class="contrainer-fluid hidden-print" style="background-color:#fff">
    <div class="container">
      <div class="row">
        <div class="col-xs-8">
          <h2>{$pagetitle}</h2>
        </div>
        <div class="col-xs-4">
          <div class="pull-right flip">
            <div class="invoice-status">
              {if $status eq "Draft"}
              <span class="draft">{$LANG.invoicesdraft}</span>
              {elseif $status eq "Unpaid"}
              <span class="unpaid">{$LANG.invoicesunpaid}</span>
              {elseif $status eq "Paid"}
              <span class="paid">{$LANG.invoicespaid}</span>
              {elseif $status eq "Refunded"}
              <span class="refunded">{$LANG.invoicesrefunded}</span>
              {elseif $status eq "Cancelled"}
              <span class="cancelled">{$LANG.invoicescancelled}</span>
              {elseif $status eq "Collections"}
              <span class="collections">{$LANG.invoicescollections}</span>
              {elseif $status eq "Payment Pending"}
              <span class="paid">{$LANG.invoicesPaymentPending}</span>
              {/if}
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="container">
    <div class="row" style="margin-top:15px;">
      <div class="col-sm-12">
        {if $paymentSuccessAwaitingNotification}
            {include file="$template/includes/panel.tpl" type="success" headerTitle=$LANG.success bodyContent=$LANG.invoicePaymentSuccessAwaitingNotify bodyTextCenter=true}
        {elseif $paymentSuccess}
            {include file="$template/includes/panel.tpl" type="success" headerTitle=$LANG.success bodyContent=$LANG.invoicepaymentsuccessconfirmation bodyTextCenter=true}
        {elseif $paymentInititated}
            {include file="$template/includes/panel.tpl" type="info" headerTitle=$LANG.success bodyContent=$LANG.invoicePaymentInitiated bodyTextCenter=true}
        {elseif $pendingReview}
            {include file="$template/includes/panel.tpl" type="info" headerTitle=$LANG.success bodyContent=$LANG.invoicepaymentpendingreview bodyTextCenter=true}
        {elseif $paymentFailed}
            {include file="$template/includes/panel.tpl" type="danger" headerTitle=$LANG.error bodyContent=$LANG.invoicepaymentfailedconfirmation bodyTextCenter=true}
        {elseif $offlineReview}
            {include file="$template/includes/panel.tpl" type="info" headerTitle=$LANG.success bodyContent=$LANG.invoiceofflinepaid bodyTextCenter=true}
        {/if}
      </div>
    </div>
  </div>

  <div class="container hidden-print">
    <div class="row" style="margin-top:15px;">
      {if $invalidInvoiceIdRequested}
      {include file="$template/includes/panel.tpl" type="danger" headerTitle=$LANG.error bodyContent=$LANG.invoiceserror bodyTextCenter=true}
      {else}
      <div class="col-md-12">
        {if $status eq "Unpaid"}
        <div class="well clearfix" style="background-color:#fff;">
          <h5>{$LANG.paymentmethod}</h5>
          <hr>
          <div class="pull-left flip">
            <div class="form-group">
            {if $allowchangegateway}
            <form method="post" action="{$smarty.server.PHP_SELF}?id={$invoiceid}" class="form-inline">
              {$gatewaydropdown}
            </form>
            {else}
             {$paymentmethod}{if $paymethoddisplayname} ({$paymethoddisplayname}){/if}
            {/if}
          </div>
          </div>
          <div class="pull-right flip">
            <div class="payment-btn-container">
              {$paymentbutton}
            </div>
          </div>
        </div>
        {/if}
      </div>
    </div>
  </div>
  {if $manualapplycredit}
  <div class="container hidden-print">
    <div class="row">
      <div class="col-md-12">
        <div class="vertical-view-line"></div>
        <div class="well well-credit clearfix">
          <form method="post" action="{$smarty.server.PHP_SELF}?id={$invoiceid}" class="form-inline">
            <input type="hidden" name="applycredit" value="true" />
            <h5>{$LANG.invoiceaddcreditapply}<span class="pull-right flip">{$totalcredit}</span></h5>
            <hr>
            <div class="pull-left flip"><p class="form-control-static">{$LANG.invoiceaddcreditamount}</p></div>
            <div class="pull-right flip">
              <div class="input-group">
                <input type="text" name="creditamount" value="{$creditamount}" class="form-control" />
                <span class="input-group-btn">
                  <input type="submit" value="{$LANG.invoiceaddcreditapply}" class="btn btn-credit" />
                </span>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  {/if}

  {if $notes}
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="vertical-view-line"></div>
        <div class="well well-notes clearfix">
          <h5>{$LANG.invoicesnotes}</h5>
          <hr>
          <p>{$notes}</p>
        </div>
      </div>
    </div>
  </div>
  {/if}


  <div class="container hidden-print">
    <div class="row">
      <div class="col-sm-6">
        <a href="javascript:window.print()" class="btn btn-link"><i class="fal fa-print"></i> {$LANG.print}</a>
        <a href="dl.php?type=i&amp;id={$invoiceid}" class="btn btn-link"><i class="fal fa-file-pdf" aria-hidden="true"></i> PDF</a>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="invoice-container">
      <div>
        <div class="row">
          <div class="col-md-12">
            <div class="row box_head">
              <div class="col-xs-12 col-sm-5">
                {if $logo}
                <div class="logo"><img src="{if $custom_logo=="on"}{$WEB_ROOT}/assets/img/{$custom_logo_filename}{else}{$logo}{/if}" alt="{$companyname}" title="{$companyname}" /></div>
                {else}
                <h2>{$companyname}</h2>
                {/if}
                <address>
                  {$payto}
                  {if $taxCode}<br>{$taxIdLabel}: {$taxCode}{/if}
                </address>
              </div>
              <div class="col-sm-7 hidden-xs">
                <table class="table-invoice table-invoice-header">
                  <tbody>
                    <tr class="primary">
                      <td colspan="2" class="table__cell">{$pagetitle}</td>
                    </tr>
                    <tr class="secondary">
                      <td class="table__cell">
                        <strong><i class="fal fa-calendar" aria-hidden="true"></i> {$LANG.invoicesdatecreated}</strong>
                      </td>
                      <td class="table__cell">{$date}</td>
                    </tr>
                    {if $status eq "Unpaid" || $status eq "Draft"}
                    <tr class="secondary">
                      <td class="table__cell">
                        <strong><i class="fal fa-calendar" aria-hidden="true"></i> {$LANG.invoicesdatedue}</strong>
                      </td>
                      <td class="table__cell">{$datedue}</td>
                    </tr>
                    {/if}
                    <tr class="barcode__cell">
                      <td class="text-right" colspan="2">
                        {if $barcode_output!="Disabled"}
                        <img id="barcode"/>
                        {/if}
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>

            <div class="container visible-xs">
              <div class="row box_head_mob">
                <div class="col-xs-4">
                  <dl class="dl-horizontal">
                    <dt><strong>{$LANG.invoicestitle}</strong></dt>
                    <dd>{$invoiceid}</dd>
                  </dl>
                </div>
                <div class="col-xs-4">
                  <dl class="dl-horizontal">
                    <dt><strong>{$LANG.invoicesdatecreated}</strong></dt>
                    <dd>{$date}</dd>
                  </dl>
                </div>
                <div class="col-xs-4">
                  <dl class="dl-horizontal">
                    <dt><strong>{$LANG.invoicesdatedue}</strong></dt>
                    <dd>{$datedue}</dd>
                  </dl>
                </div>
              </div>
            </div>

            <table class="table-invoice table-invoice-items">
              <thead>
                <tr>
                  <td><strong>{$LANG.invoicesdescription}</strong></td>
                  <td class="text-right"><strong>{$LANG.invoicesamount}</strong></td>
                </tr>
              </thead>
              <tbody>
                {foreach from=$invoiceitems item=item}
                <tr>
                  <td class="table__cell">{$item.description}{if $item.taxed eq "true"} <small><i class="fal fa-asterisk" style="color:#777;" aria-hidden="true"></i></small>{/if}</td>
                  <td class="text-right table__cell">{$item.amount}</td>
                </tr>
                {/foreach}
              </tbody>
            </table>
            <div class="row box_total">
              <div class="col-md-7">
                <h5><strong>{$LANG.invoicesinvoicedto}</strong></h5>
                <address>
                  {if $clientsdetails.companyname}{$clientsdetails.companyname}<br>{/if}
                  {$clientsdetails.firstname} {$clientsdetails.lastname}<br>
                  {$clientsdetails.address1}, {$clientsdetails.address2}<br>
                  {$clientsdetails.city}, {$clientsdetails.state}, {$clientsdetails.postcode}<br>
                  {$clientsdetails.country}
                  {if $clientsdetails.tax_id}
                  <br>{$taxIdLabel}: {$clientsdetails.tax_id}
                  {/if}
                  {if $customfields}
                  <br><br>
                  {foreach from=$customfields item=customfield}
                  {$customfield.fieldname}: {$customfield.value}<br>
                  {/foreach}
                  {/if}
                </address>

              </div>
              <div class="col-md-5">
                <table class="table-invoice table-invoice-totals">
                  <tbody>
                    <tr class="secondary">
                      <td class="table__cell"><strong>{$LANG.invoicessubtotal}</strong></td>
                      <td class="text-right table__cell"><strong>{$subtotal}</strong></td>
                    </tr>
                   {if $taxname}
                    <tr class="secondary">
                      <td class="table__cell"><strong>{$taxrate}% {$taxname}</strong></td>
                      <td class="text-right table__cell"><strong>{$tax}</strong></td>
                    </tr>
                    {/if}
                    {if $taxname2}
                    <tr class="secondary">
                      <td class="table__cell"><strong>{$taxrate2}% {$taxname2}</strong></td>
                      <td class="text-right table__cell"><strong>{$tax2}</strong></td>
                    </tr>
                    {/if}
                    <tr class="secondary">
                      <td class="table__cell"><strong>{$LANG.invoicescredit}</strong></td>
                      <td class="text-right table__cell"><strong>{$credit}</strong></td>
                    </tr>
                    <tr class="primary">
                      <td class="table__cell"><h4>{$LANG.invoicestotal}</h4></td>
                      <td class="text-right table__cell"><h4>{$total}</h4></td>
                    </tr>
                    {if $taxrate}
                    <tr class="secondary">
                      <td colspan="2" style="padding:6px 12px;"><small><i class="fal fa-fw fa-asterisk" style="color:#777;" aria-hidden="true"></i> {$LANG.invoicestaxindicator}</small></td>
                    </tr>
                    {/if}
                  </tbody>
                </table>
              </div>
            </div>
            {if $clean_html_show_transactions=='on' && ($clean_html_transactions_hide_empty_table=='' || $clean_html_transactions_hide_empty_table=='on' && count($transactions))}
            <div class="transactions-container">
              <div class="table-responsive">
                <table class="table-invoice table-transactions">
                  <thead>
                    <tr>
                      <td><strong>{$LANG.invoicestransdate}</strong></td>
                      <td><strong>{$LANG.invoicestransgateway}</strong></td>
                      <td><strong>{$LANG.invoicestransid}</strong></td>
                      <td class="text-right"><strong>{$LANG.invoicestransamount}</strong></td>
                    </tr>
                  </thead>
                  <tbody>
                    {foreach from=$transactions item=transaction}
                    <tr class="secondary">
                      <td class="table__cell">{$transaction.date}</td>
                      <td class="table__cell">{$transaction.gateway}</td>
                      <td class="table__cell">{$transaction.transid}</td>
                      <td class="text-right table__cell">{$transaction.amount}</td>
                    </tr>
                    {foreachelse}
                    <tr>
                      <td class="table__cell text-center" colspan="4">{$LANG.invoicestransnonefound}</td>
                    </tr>
                    {/foreach}
                  </tbody>
                </table>
              </div>
              <div class="row">
                <div class="col-md-7">
                </div>
                <div class="col-md-5">
                  <table class="table-invoice table-balance">
                    <tbody>
                    <tr class="primary">
                      <td class="table__cell"><strong>{$LANG.invoicesbalance}</strong></td>
                      <td class="text-right table__cell"><strong>{$balance}</strong></td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          {/if}
          {/if}
        </div>
      </div>

          {if $show_footer_areas}
            <div class="row is-flex align-items-end box_footer">
                {foreach from=$footer_column_items_array item=item}
                  <div class="col-md-{$footer_column_size}">
                      {html_entity_decode($item.text)}
                  </div>
                {/foreach}
            </div>
          {/if}

    </div>
  </div>
  <div class="row" style="margin-top:30px;">
    <div class="col-sm-12 text-center hidden-print">
      <a class="btn btn-link" href="clientarea.php">{$LANG.invoicesbacktoclientarea}</a>
    </div>
  </div>
</div>

    <div id="fullpage-overlay" class="hidden">
        <div class="outer-wrapper">
            <div class="inner-wrapper">
                <img src="{$WEB_ROOT}/assets/img/overlay-spinner.svg">
                <br>
                <span class="msg"></span>
            </div>
        </div>
    </div>

</body>
</html>
