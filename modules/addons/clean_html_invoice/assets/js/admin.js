$(function(){
	var invoice_footer_column_items = $('[name=footer_column_items]').val();
	var quote_footer_column_items = $('[name=quote_footer_column_items]').val();

	if(invoice_footer_column_items)
	if(invoice_footer_column_items.length>0){
		var items = $.parseJSON(invoice_footer_column_items);
		var i;
	  for (var j = 0; j<items.length; j++){
				invoice_addmore_footer_column_gid();
		}
		i = 0;
		$( "#savesetting [name^=invoice_footer_column]" ).each(function( index ) {
			$(this).val(items[i].text);
			i++;
		});
	}
	var c = $("#invoice_footer_column_holder .invoice_footer_column_row").length;
	if (c>=4) { $('.invoice_add_more_btn').hide();};

	if(quote_footer_column_items)
	if(quote_footer_column_items.length>0){
		var items = $.parseJSON(quote_footer_column_items);
		var i;
	  for (var j = 0; j<items.length; j++){
				quote_addmore_footer_column_gid();
		}
		i = 0;
		$( "#savesetting [name^=quote_footer_column]" ).each(function( index ) {
			$(this).val(items[i].text);
			i++;
		});
	}
	var c = $("#quote_footer_column_holder .quote_footer_column_row").length;
	if (c>=4) { $('.quote_add_more_btn').hide();};

});


function invoice_addmore_footer_column_gid(){
	var c = $(".invoice_footer_column_row").length;

	if (c>=5) { $('.invoice_add_more_btn').hide(); return; };

	$('#invoice_footer_column_holder').append($('#f_template').html());
	if (c+1>=5) { $('.invoice_add_more_btn').hide(); };

	rename_updatestr(".invoice_footer_column_row");
}

function quote_addmore_footer_column_gid(){
	var c = $(".quote_footer_column_row").length;

	if (c>=5) { $('.quote_add_more_btn').hide(); return; };

	$('#quote_footer_column_holder').append($('#quote_f_template').html());
	if (c+1>=5) { $('.quote_add_more_btn').hide(); };

	rename_updatestr(".quote_footer_column_row");
}

function rename_updatestr(parent){
	var i = 1;
	$( parent ).each(function( index ) {
			$(this).find( "[data-template]" ).each(function( index ) {
					var html = $(this).attr("data-template");
					html = html.replace("{X}", i);
					$(this).html( html );
			});
	  	i++;
	});
}

function invoice_remove_footer_column_gid(e){
	$(e).parents('.invoice_footer_column_row').remove();

	var c = $(".invoice_footer_column_row").length;
	if (c<5) { $('.invoice_add_more_btn').show(); return; };

	rename_updatestr(".invoice_footer_column_row");
}

function quote_remove_footer_column_gid(e){
	$(e).parents('.quote_footer_column_row').remove();

	var c = $(".quote_footer_column_row").length;
	if (c<5) { $('.quote_add_more_btn').show(); return; };

	rename_updatestr('.quote_footer_column_row');
}


function vasltojson(){
	var invoice_items = new Array();
	var quote_items = new Array();

	$( "#savesetting [name^=invoice_footer_column]" ).each(function( index ) {
		var o = new Object;
		o.text = $(this).val();
		invoice_items.push(o);
	});
	$('[name=footer_column_items]').val(JSON.stringify(invoice_items));

	$( "#savesetting [name^=quote_footer_column]" ).not('[name^=quote_footer_column_items]').each(function( index ) {
		var o = new Object;
		o.text = $(this).val();
		quote_items.push(o);
	});
	$('[name=quote_footer_column_items]').val(JSON.stringify(quote_items));


}