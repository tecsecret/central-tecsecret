{**********************************************************************
* DiscountCenter product developed. (2015-11-18)
* *
*
*  CREATED BY MODULESGARDEN       ->       http://modulesgarden.com
*  CONTACT                        ->       contact@modulesgarden.com
*
*
* This software is furnished under a license and may be used and copied
* only  in  accordance  with  the  terms  of such  license and with the
* inclusion of the above copyright notice.  This software  or any other
* copies thereof may not be provided or otherwise made available to any
* other person.  No title to and  ownership of the  software is  hereby
* transferred.
*
*
**********************************************************************}
{**
* @author Paweł Kopeć <pawelk@modulesgarden.com>
*}
<div class="col-lg-12" id="mg-discount" >
    <legend>{if $id} {$MGLANG->T('header','Edit Early Payment')} {else}{$MGLANG->T('header','Add Early Payment')}{/if}</legend>
    <ul class="nav nav-tabs" id="mg-discount-nav-tab">
        <li class="active"><a href="#mg-tab-general" id="mg-nav-general">{$MGLANG->T('General')}</a></li>
        <li><a href="#mg-tab-products" id="mg-nav-product" {if !$isEdit} class="disabled-link"{/if}>{$MGLANG->T('Discounts')}</a></li>
        <li><a href="#mg-tab-rules"  id="mg-nav-rules"  class="disabled-link">{$MGLANG->T('Email Notifications')}</a></li>
    </ul>
    <form data-toggle="validator" role="form" id="mg-form-general">
        <input type="hidden" name="id" value="{$id}" />
        <div class="tab-content">
            {* General *}
            <div class="tab-pane active" id="mg-tab-general" >

                {$form.general}
                <div class="pull-right" {if $isEdit} style="display:none;" {/if}>
                    <a href="#" id="mg-button-next-general" class="btn btn-primary btn-inverse">{$MGLANG->T('next')} &rarr; </a>
                </div>
            </div> 
            {* Products Discounts *}
            <div class="tab-pane" id="mg-tab-products">
                <div id="mg-tab-products-content"></div>
                <div class="pull-left pager-x" {if $isEdit} style="display:none;" {/if}>
                    <a href="#mg-nav-general" class="btn btn-primary btn-inverse">&larr; {$MGLANG->T('Back')} </a>
                </div>
                <div class="pull-right pager-x" {if $isEdit} style="display:none;" {/if}>
                    <a href="#mg-nav-rules" class="btn btn-primary btn-inverse next-x disabled-link">{$MGLANG->T('next')} &rarr; </a>
                </div>
            </div>
            {* Email Notifications *}
            <div class="tab-pane" id="mg-tab-rules">
                {$form.emailNotifications}
                <div class="form-horizontal">
                    <div class="form-group">
                        <div class="col-sm-12">
                            <legend  data-toggle="tooltip" >{$MGLANG->T('Early Discount Cancellation Reminders')}</legend>
                        </div>     
                    </div>
                    {section name=k start=0 loop=$loopNotifications step=1}
                        {assign var="i" value=$smarty.section.k.index}
                        <div class="form-group">
                            <label class="col-sm-2 control-label"></label>
                            <input type="hidden" name="notifications[id][]" value="{$notifications.$i.id}" />
                            <div class="col-sm-2">
                                <select class="form-control" name="notifications[template_id][]">
                                    {foreach $emailTemplates item=t key=k}
                                        <option value="{$k}" {if $notifications.$i.template_id==$k}selected{/if}>{$t}</option>
                                    {/foreach}
                                </select>
                                <div  class="help-block with-errors" style="display:none;"></div>
                            </div>
                            <div class="col-sm-2  control-label" style="text-align: left; padding-left: 0px; padding-top: 0px;  "> 
                                {$MGLANG->T('Select the emial for clients  or lave blank to disable the feature')}
                            </div>
                            <div class="col-sm-2" style="width:11%;">
                                <input type="number" min="0" max="10000" data-error="{$MGLANG->T('Field is required')}" placeholder="1" class="form-control mg-input-quantity" value="{$notifications.$i.period}" name="notifications[period][]" requried>
                                <div  class="help-block with-errors mg-error-quantity" style="display:none;"></div>
                            </div>
                            <div class="col-sm-2  control-label" style="text-align: left; padding-left: 0px; padding-top: 0px; "> 
                                {$MGLANG->T('Enter the number of days before discount cancellation')}
                            </div>
                            <button  type="button" class="btn btn-primary mg-clone-add" style="" title="{$MGLANG->T('Add')}">               
                                <i class="glyphicon glyphicon-plus"></i>
                            </button>
                            <button  type="button" class="btn btn-danger mg-clone-remove" {if $loopNotifications==1}style="display:none;"{/if} title="{$MGLANG->T('remove')}">               
                                <i class="glyphicon glyphicon-remove"></i>
                            </button>
                        </div>                        
                    {/section}
                </div>
                
                <div class="pull-left pager-x" {if $isEdit} style="display:none;" {/if}>
                    <a href="#mg-nav-product" class="btn btn-primary btn-inverse">&larr; {$MGLANG->T('Back')} </a>
                </div>
                <div class="pull-right pager-x">
                    <a href="#" class="btn btn-success btn-inverse next-x" id="mg-button-nav-save">{$MGLANG->T('Save Changes')} </a>
                </div>
            </div>
        </div>
    </form>
</div>
{literal}
    <style type="text/css">
        .mg-row-price-strike{
            text-decoration: line-through;
            color: red;
        }
        .mg-row-price-promotion{
            color: green;
        }
        .mg-promotion-form-td{
            width: 15% !important;
        }
        .mg-promotion-form-dom-td{
            width: 10% !important;
        }
        #mg-discount-nav-tab .disabled-link{
            color: #98aeb5 !important;
        }
        .mg-promotion {
            padding-top:16px !important;
        }
        .select2-results__group {
            cursor: pointer !important;
        }
    </style>
    <script type="text/javascript">
        jQuery(document).ready(function () {
            var isEdit = "{/literal}{$isEdit}{literal}";
            var runValidator = true;
            function mgValidateForm(attrId) {
                var result = true;
                $(attrId + ' .form-group').each(function () {
                    if ($(this).hasClass('has-error')) {
                        result = false;
                    }
                });
                return result;
            }
            function mgTypeChange(){
                if(runValidator){
                    $("#mg-form-general").validator('validate');
                } 
                
                $( ".mg-discount-type" ).each(function( index ) {
                var promotionValue = $(this).closest('td').find('.mg-discount-value');
                var priceTd = $(this).closest('tr').prev('tr').find(promotionValue.attr('data-target'));

                if ($(this).val() == "0") { //Hide promotion and stop the script
                    $(priceTd).find('.mg-row-price-regular').show();
                    $(priceTd).find('.mg-row-price-strike').hide();
                    $(priceTd).find('.mg-row-price-promotion').text('');
                    promotionValue.val('');
                    if($(".mg-discount-value").val()==""){
                        $("#mg-nav-rules").addClass('disabled-link');
                    }
                    return;
                }
                var price = parseFloat(promotionValue.attr('data-price'));
                var value = parseFloat(promotionValue.val());

                //Validate 
                if (isNaN(value))
                    return;

                //Calculate priotion price
                $(priceTd).find('.mg-row-price-regular').hide();

                var promotionPrice = 0;
                if ($(this).val() == "percentage") { //Add percentage promotion
                    var regularPrice = (100 - value);
                    promotionPrice = (price / 100) * regularPrice;

                }
                else if ($(this).val() == "free_amount") { //Add free amount
                    promotionPrice = price - value;
                }
                promotionPrice = parseFloat(promotionPrice).toFixed(2);
                $(priceTd).find('.mg-row-price-strike').show();
                $(priceTd).find('.mg-row-price-promotion').text(promotionPrice + " " + promotionValue.attr('data-currency'));
                
                if($(this).val() != "0" && mgValidateForm("#mg-form-general") === true){
                     if(isEdit=="1")
                        $("#mg-nav-rules").removeClass('disabled-link');
                     $("#mg-tab-products .next-x").removeClass('disabled-link');
                }
                });
                
            }
            var validatorOptions = {
                custom: {
                    mgvaluevalidation: function (el) {
                        var value = parseFloat($(el).val());
                        var promotionType = $(el).closest('td').find('.mg-discount-type');
                        var price = parseFloat($(el).attr('data-price'));

                        switch (promotionType.val()) {
                            case "percentage":
                                if (value > 100)
                                    return false;
                                break;
                            case "free_amount":
                                if (value > price)
                                    return false;
                                break;
                            default:
                                return true;
                        }

                        return true;
                    }
                },
                errors: {
                    mgvaluevalidation: "Value is to high"
                }
            };
            //Nav back next
            $('.tab-pane .pager-x a').click(function (e) {
                if ($(this).hasClass('disabled-link'))
                    return;
                e.preventDefault();
                var href = $(this).attr("href");
                if (href == "#mg-nav-rules")
                    $("#mg-nav-rules").removeClass('disabled-link');
                $(href).tab('show');

            });
            //# Show Tab rules 
            $("#mg-nav-rules").click(function (e) {
                if ($(this).hasClass('disabled-link'))
                    return;
                $("#mg-nav-rules").tab('show');
            });
            //# Show Tab general
            $('#mg-nav-general').click(function (e) {
                if ($(this).hasClass('disabled-link'))
                    return;
                e.preventDefault();
                $(this).tab('show');
            });
            var mgLastActivedCollapse = null;
            // Next Step
            if (isEdit && ($("#mg-discount-applies-products").val() != null || $("#mg-discount-applies-addons").val() != null || $("#mg-discount-applies-domains").val() != null)) {
                JSONParser.request(
                        'discounts'
                        , $("#mg-form-general").serialize()
                        , function (data) {
                            if (data.html) {
                                runValidator = false;
                                if ($("#mg-nav-product").hasClass('disabled-link'))
                                    $("#mg-nav-product").removeClass('disabled-link');

                                $('#mg-tab-products-content').html('').html(data.html);
                                $("#mg-form-general").validator('destroy');
                                $('#mg-form-general').validator(validatorOptions);
                                $("#mg-form-general").validator('validate');
                                mgTypeChange();
                                runValidator = true;
                            }
                        });
            }
            $("#mg-button-next-general, #mg-nav-product").click(function (e) {
                $("#mg-form-general").validator('validate');
                if (mgValidateForm("#mg-form-general") !== true)
                    return false;
                if ($(this).hasClass('disabled-link'))
                    return;
                JSONParser.request(
                        'discounts'
                        , $("#mg-form-general").serialize()
                        , function (data) {
                            if (data.html) {
                                if ($("#mg-nav-product").hasClass('disabled-link'))
                                    $("#mg-nav-product").removeClass('disabled-link');

                                $('#mg-tab-products-content').html('').html(data.html);
                                $("#mg-nav-product").tab('show');
                                $("#mg-form-general").validator('destroy');
                                $('#mg-form-general').validator(validatorOptions);
                                $("#mg-form-general").validator('validate');
                                mgTypeChange();
                            }
                        });
            });
            // #2 keep active collapse 
            $('#mg-discount').delegate('.panel-collapse', 'show.bs.collapse', function (e) {
                $(this).prev().find("span i").removeClass('glyphicon-plus').addClass('glyphicon-minus');
                mgLastActivedCollapse = "#" + $(this).attr('id');
            });
            $('#mg-discount').delegate('.panel-collapse', 'hide.bs.collapse', function (e) {
                $(this).prev().find("span i").removeClass('glyphicon-minus').addClass('glyphicon-plus');
            });
            $("#mg-button-next-rules, #mg-nav-rules").click(function (e) {
                if ($(this).hasClass('disabled-link'))
                    return;
                $("#mg-nav-rules").tab('show');
            });
            // #2 Discount-type change
            $('#mg-discount').delegate('.mg-discount-type', 'change', function (e) {
                e.preventDefault();
                if(runValidator){
                    $("#mg-form-general").validator('validate');
                }
                var promotionValue = $(this).closest('td').find('.mg-discount-value');
                var priceTd = $(this).closest('tr').prev('tr').find(promotionValue.attr('data-target'));

                if ($(this).val() == "0") { //Hide promotion and stop the script
                    $(priceTd).find('.mg-row-price-regular').show();
                    $(priceTd).find('.mg-row-price-strike').hide();
                    $(priceTd).find('.mg-row-price-promotion').text('');
                    promotionValue.val('');
                    if ($(".mg-discount-value").val() == "") {
                        $("#mg-nav-rules").addClass('disabled-link');
                    }
                    return;
                }
                var price = parseFloat(promotionValue.attr('data-price'));
                var value = parseFloat(promotionValue.val());

                //Validate 
                if (isNaN(value))
                    return;

                //Calculate priotion price
                $(priceTd).find('.mg-row-price-regular').hide();

                var promotionPrice = 0;
                if ($(this).val() == "percentage") { //Add percentage promotion
                    var regularPrice = (100 - value);
                    promotionPrice = (price / 100) * regularPrice;

                } else if ($(this).val() == "free_amount") { //Add free amount
                    promotionPrice = price - value;
                }
                promotionPrice = parseFloat(promotionPrice).toFixed(2);
                $(priceTd).find('.mg-row-price-strike').show();
                $(priceTd).find('.mg-row-price-promotion').text(promotionPrice + " " + promotionValue.attr('data-currency'));

                if ($(this).val() != "0" && mgValidateForm("#mg-form-general") === true) {
                    if (isEdit == "1")
                        $("#mg-nav-rules").removeClass('disabled-link');
                    $("#mg-tab-products .next-x").removeClass('disabled-link');
                }

            });
            // Discount-value change
            $('#mg-discount').delegate('.mg-discount-value', 'keyup', function (e) {
                var value = parseFloat($(this).val());
                var priceTd = $(this).closest('tr').prev('tr').find($(this).attr('data-target'));
                if (isNaN(value)) {
                    $(priceTd).find('.mg-row-price-regular').show();
                    $(priceTd).find('.mg-row-price-strike').hide();
                    $(priceTd).find('.mg-row-price-promotion').text('');
                    return;
                }

                var promotionType = $(this).closest('td').find('.mg-discount-type');
                if (promotionType.val() == "0")
                    return;

                var price = parseFloat($(this).attr('data-price'));
                //Calculate priotion price

                $(priceTd).find('.mg-row-price-regular').hide();

                var promotionPrice = 0;
                if (promotionType.val() == "percentage") { //Add percentage promotion
                    var regularPrice = (100 - value);
                    promotionPrice = (price / 100) * regularPrice;

                } else if (promotionType.val() == "free_amount") { //Add free amount
                    promotionPrice = price - value;
                }
                promotionPrice = parseFloat(promotionPrice).toFixed(2);
                $(priceTd).find('.mg-row-price-strike').show();
                $(priceTd).find('.mg-row-price-promotion').text(promotionPrice + " " + $(this).attr('data-currency'));
                if ($(this).val() != "0" && mgValidateForm("#mg-form-general") === true) {
                    if (isEdit == "1")
                        $("#mg-nav-rules").removeClass('disabled-link');
                    $("#mg-tab-products .next-x").removeClass('disabled-link');
                }
            });
            // add disable link 
            $('#mg-discount').delegate('.mg-discount-value', 'keypress', function (e) {
                var chCode = (e.which) ? e.which : e.keyCode;
                return !(chCode > 31 && (chCode < 48 || chCode > 57)) || (chCode === 46 || chCode === 110 || chCode === 190);
            });
            $('#mg-discount').delegate('.mg-discount-value', 'keyup', function (e) {
                disableDiscountLinks(this);
            });
            function disableDiscountLinks(elem) {
                setTimeout(function () {
                    var value = parseFloat($(elem).val());
                    if (mgValidateForm("#mg-form-general") === true && !isNaN(value)) {
                        if (isEdit == "1") {
                            $("#mg-nav-rules").removeClass('disabled-link');
                            $("#mg-nav-general").removeClass('disabled-link');
                            //$("#mg-tab-products .next-x").removeClass('disabled-link');
                            $("#mg-tab-products .pager-x").children().removeClass('disabled-link');
                        }
                    } else {
                        $("#mg-nav-rules").addClass('disabled-link');
                        $("#mg-nav-general").addClass('disabled-link');
                        //$("#mg-tab-products .next-x").addClass('disabled-link');
                        $("#mg-tab-products .pager-x").children().addClass('disabled-link');
                    }
                }, 500);
            }
            // Save
            $("#mg-button-nav-save").click(function (e) {
                e.preventDefault();
                $(this).prop("disabled", true);
                JSONParser.request(
                        'save'
                        , $("#mg-form-general").serialize()
                        , function (data) {
                            $(this).prop("disabled", false);
                            if (data.success)//$('#mg-form-general input[name="id"]').val()=="" &&
                                window.location.href = "?module=DiscountCenter&mg-page=earlyPayment";
                        });

            });
            //Select2 All
            $(".mg-wrapper").on("select2:select", ".select2-hidden-accessible", function (e) {
                if (e.params.data.id !== "-1")
                    return;
                $(this).find('option').each(function (index) {
                    $(this).prop('selected', !$(this).is(':selected'));
                });
                $(this).trigger("change");
            });
            //copy
            $('#mg-tab-products-content').on('click', '.mg-discount-copy', function (e) {
                e.preventDefault();
                runValidator = false;
                var col = $(this).parent('td').index();
                if ($(this).closest('tr').find("td.active").size())
                    col--;
                var td = $(this).closest('tr').next('tr').children('td:eq( ' + col + ' )');
                var type = td.find('.mg-discount-type');
                var value = td.find('.mg-discount-value');
                var priceType = value.attr('data-pricetype');

                $(this).closest('tbody').find('tr').find('td:eq( ' + col + ' )')
                        .find('input[data-pricetype="' + priceType + '"]').each(function (i) {
                    var toValue = $(this);
                    var toType = $(toValue).closest('td').find('.mg-discount-type');
                    toValue.val(value.val());
                    toType.val(type.val());
                    toType.trigger("change");
                });
                runValidator = true;
            });
            //#cloning add
            $("#mg-discount").delegate(".mg-clone-add", "click", function (e) {
                e.preventDefault();
                var content = $(this).closest('.form-group').clone();
                content.find('input[name="notifications[id][]"]').val('');
                $(this).closest('.form-group').after(content);
                $(".mg-clone-remove").show();

                $("#mg-form-add").validator('destroy');
                $("#mg-form-add").validator('validate');
            });
            //#cloning remove
            $("#mg-discount").delegate(".mg-clone-remove", "click", function (e) {
                e.preventDefault();
                $(this).closest('.form-group').remove();
                if ($(".mg-clone-remove").size() == 1)
                    $(".mg-clone-remove").hide();
            });
        });
    </script>
{/literal}