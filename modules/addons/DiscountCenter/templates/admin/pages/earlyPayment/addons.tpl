{**********************************************************************
* DiscountCenter product developed. (2015-11-25)
* *
*
*  CREATED BY MODULESGARDEN       ->       http://modulesgarden.com
*  CONTACT                        ->       contact@modulesgarden.com
*
*
* This software is furnished under a license and may be used and copied
* only  in  accordance  with  the  terms  of such  license and with the
* inclusion of the above copyright notice.  This software  or any other
* copies thereof may not be provided or otherwise made available to any
* other person.  No title to and  ownership of the  software is  hereby
* transferred.
*
*
**********************************************************************}

{**
* @author Paweł Kopeć <pawelk@modulesgarden.com>
*}
<legend>{$MGLANG->T('Product Addons')}</legend>
<div class="panel-group" id="mg-accordion-addon" role="tablist" aria-multiselectable="true">
    {assign var="k" value=0}
    {foreach from=$addons item=item}
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="mg-headingOne-addons" data-toggle="collapse" href="#mg-collapse-addo-{$item.addon.id}" data-parent="#mg-accordion-prod" aria-expanded="true" aria-controls="mg-collapse-addo-{$item.addon.id}">
                <h4 class="panel-title">
                    <a href="configaddons.php?action=manage&id={$item.addon.id}" target="blank">#{$item.addon.id}</a>
                    <a role="button" data-toggle="collapse" data-parent="#mg-accordion-prod" href="#mg-collapse-addo-{$item.addon.id}" aria-expanded="true" aria-controls="mg-collapse-addo-{$item.addon.id}">
                        {$item.addon.name}
                    </a>
                    <span class="pull-right"><i class="glyphicon glyphicon-plus"></i></span>
                </h4>
            </div>
            <div id="mg-collapse-addo-{$item.addon.id}" class="panel-collapse collapse {*if $i==1} in{/if*}" role="tabpanel" aria-labelledby="mg-headingOne-addons">
                <div class="panel-body">
                    <table class="table mg-promotion-table">
                        <tbody>
                            <tr class="active">
                                <td style="text-align:center;">{$MGLANG->T('Currency')}</td>
                                <td></td>
                                <td>{$MGLANG->T('One Time/Monthly')}</td>
                                <td>{$MGLANG->T('Quarterly')}</td>
                                <td>{$MGLANG->T('Semi-Annually')}</td>
                                <td>{$MGLANG->T('Annually')}</td>
                                <td>{$MGLANG->T('Biennially')}</td>
                                <td>{$MGLANG->T('Triennially')}</td>
                            </tr>
                            {foreach from=$item.pricing item=itemPrince}
                                {*Setup Price*}
                                <tr>
                                    <td rowspan="3" class="active"  style="text-align:center;">{$itemPrince.currency.code}</td> {*Currency*}
                                </tr>
                                {*Register Price*}
                                <tr>
                                    <td>{$MGLANG->T('Price')}</td>
                                    <td class="mg-row-price_1">{*Monthly*} 
                                        {if $itemPrince.price.monthly == "-1" || $itemPrince.price.monthly == "0.00"} - {else}
                                            <span class="mg-row-price-regular"> {$itemPrince.price.monthly} {$itemPrince.currency.code}</span> 
                                            <span class="mg-row-price-strike" style="display: none;">{$itemPrince.price.monthly} {$itemPrince.currency.code}</span> <span class="mg-row-price-promotion"></span>
                                            {include file="copy.tpl"}
                                        {/if}
                                    </td>
                                    <td class="mg-row-price_2">{*Quarterl*}
                                        {if $itemPrince.price.quarterly=="-1" || $itemPrince.price.quarterly == "0.00"} - {else}
                                            <span class="mg-row-price-regular"> {$itemPrince.price.quarterly} {$itemPrince.currency.code}</span> 
                                            <span class="mg-row-price-strike" style="display: none;">{$itemPrince.price.quarterly} {$itemPrince.currency.code}</span> <span class="mg-row-price-promotion"></span>
                                            {include file="copy.tpl"}
                                        {/if} 
                                    </td>
                                    <td class="mg-row-price_3">{*Semi-Annually*}
                                        {if $itemPrince.price.semiAnnually=="-1" || $itemPrince.price.semiAnnually == "0.00"} - {else}
                                            <span class="mg-row-price-regular"> {$itemPrince.price.semiAnnually} {$itemPrince.currency.code} </span> 
                                            <span class="mg-row-price-strike" style="display: none;">{$itemPrince.price.semiAnnually} {$itemPrince.currency.code}</span> <span class="mg-row-price-promotion"></span>
                                            {include file="copy.tpl"}
                                        {/if} 
                                    </td>
                                    <td class="mg-row-price_4">{*Annually*}
                                        {if $itemPrince.price.annually=="-1" || $itemPrince.price.annually == "0.00"}- {else}
                                            <span class="mg-row-price-regular"> {$itemPrince.price.annually} {$itemPrince.currency.code}</span> 
                                            <span class="mg-row-price-strike" style="display: none;">{$itemPrince.price.annually} {$itemPrince.currency.code}</span> <span class="mg-row-price-promotion"></span>
                                            {include file="copy.tpl"}
                                        {/if} 
                                    </td>
                                    <td class="mg-row-price_5">{*Biennially*}
                                        {if $itemPrince.price.biennially=="-1" || $itemPrince.price.biennially == "0.00"} - {else}
                                            <span class="mg-row-price-regular"> {$itemPrince.price.biennially} {$itemPrince.currency.code} </span> 
                                            <span class="mg-row-price-strike" style="display: none;">{$itemPrince.price.biennially} {$itemPrince.currency.code}</span> <span class="mg-row-price-promotion"></span>
                                            {include file="copy.tpl"}
                                        {/if} 
                                    </td>
                                    <td class="mg-row-price_6">{*Triennially*}
                                        {if $itemPrince.price.triennially=="-1" || $itemPrince.price.triennially == "0.00"} - {else}
                                            <span class="mg-row-price-regular"> {$itemPrince.price.triennially} {$itemPrince.currency.code}</span> 
                                            <span class="mg-row-price-strike" style="display: none;">{$itemPrince.price.triennially} {$itemPrince.currency.code}</span> <span class="mg-row-price-promotion"></span>
                                            {include file="copy.tpl"}
                                        {/if} 
                                    </td>
                                </tr>
                                <tr>
                                    <td class="mg-promotion">{$MGLANG->T('Promotion')}</td>                                            
                                    <td class="mg-promotion-form-td">{*Monthly*}
                                        {if $itemPrince.price.monthly=="-1" || $itemPrince.price.monthly == "0.00"} 
                                            - 
                                        {else}
                                            {include file="promotion.tpl" itemName="addons"  period="monthly"  relationId=$item.addon.id
                                            price=$itemPrince.price.monthly
                                            priceType="register"
                                            discountType=$post.addons.$k.monthly.register.discountType
                                            discountValue=$post.addons.$k.monthly.register.value target=".mg-row-price_1"}
                                        {/if} 
                                    </td>
                                    <td class="mg-promotion-form-td">{*Quarterly*} 
                                        {if $itemPrince.price.quarterly=="-1" || $itemPrince.price.quarterly == "0.00"} 
                                            - 
                                        {else}
                                            {include file="promotion.tpl" itemName="addons"  period="quarterly" relationId=$item.addon.id
                                            price=$itemPrince.price.quarterly
                                            priceType="register"
                                            discountType=$post.addons.$k.quarterly.register.discountType
                                            discountValue=$post.addons.$k.quarterly.register.value target=".mg-row-price_2"}
                                        {/if} 
                                    </td>
                                    <td class="mg-promotion-form-td">{*Semi-Annually*}
                                        {if $itemPrince.price.semiAnnually=="-1" || $itemPrince.price.semiAnnually == "0.00"} 
                                            - 
                                        {else}
                                            {include file="promotion.tpl" itemName="addons"  period="semiAnnually" relationId=$item.addon.id
                                            price=$itemPrince.price.semiAnnually
                                            priceType="register"
                                            discountType=$post.addons.$k.semiAnnually.register.discountType
                                            discountValue=$post.addons.$k.semiAnnually.register.value target=".mg-row-price_3"}
                                        {/if} 
                                    </td>
                                    <td class="mg-promotion-form-td">{*Annually*}
                                        {if $itemPrince.price.annually =="-1" || $itemPrince.price.annually == "0.00"} 
                                            - 
                                        {else}
                                            {include file="promotion.tpl" itemName="addons"  period="annually" relationId=$item.addon.id
                                            price=$itemPrince.price.annually
                                            priceType="register"
                                            discountType=$post.addons.$k.annually.register.discountType
                                            discountValue=$post.addons.$k.annually.register.value target=".mg-row-price_4"}
                                        {/if} 
                                    </td>
                                    <td class="mg-promotion-form-td">{*Biennially*}
                                        {if $itemPrince.price.biennially=="-1" || $itemPrince.price.biennially == "0.00"} 
                                            - 
                                        {else}
                                            {include file="promotion.tpl" itemName="addons"  period="biennially" relationId=$item.addon.id
                                            price=$itemPrince.price.biennially
                                            priceType="register"
                                            discountType=$post.addons.$k.biennially.register.discountType
                                            discountValue=$post.addons.$k.biennially.register.value target=".mg-row-price_5"}
                                        {/if} 
                                    </td>
                                    <td class="mg-promotion-form-td">{*Triennially*}
                                        {if $itemPrince.price.triennially=="-1" || $itemPrince.price.triennially == "0.00"} 
                                            - 
                                        {else}
                                            {include file="promotion.tpl" itemName="addons"  period="triennially" relationId=$item.addon.id
                                            price=$itemPrince.price.triennially
                                            priceType="register"
                                            discountType=$post.addons.$k.triennially.register.discountType
                                            discountValue=$post.addons.$k.triennially.register.value target=".mg-row-price_6"}
                                        {/if} 
                                    </td>
                                </tr>
                                {assign var="k" value=$k+1}
                            {foreachelse}
                                <tr>
                                    <td colspan='8' style="text-align:center;">{$MGLANG->T('Nothing to display')}</td>
                                </tr>
                            {/foreach}
                        </tbody> 
                    </table>
                </div>
            </div>
        </div>
    {/foreach}
</div>
