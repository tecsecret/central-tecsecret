{**********************************************************************
 * DiscountCenter product developed. (2015-11-27)
 * *
 *
 *  CREATED BY MODULESGARDEN       ->       http://modulesgarden.com
 *  CONTACT                        ->       contact@modulesgarden.com
 *
 *
 * This software is furnished under a license and may be used and copied
 * only  in  accordance  with  the  terms  of such  license and with the
 * inclusion of the above copyright notice.  This software  or any other
 * copies thereof may not be provided or otherwise made available to any
 * other person.  No title to and  ownership of the  software is  hereby
 * transferred.
 *
 *
 **********************************************************************}

{**
* @author Paweł Kopeć <pawelk@modulesgarden.com>
*}
<span title="{$products} {$MGLANG->T("Products")}
{$addons} {$MGLANG->T("Addons")}
{$domains} {$MGLANG->T("Domains")}" >{$products} / {$addons} / {$domains}</span>