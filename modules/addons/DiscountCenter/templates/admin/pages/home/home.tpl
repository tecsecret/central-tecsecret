{**********************************************************************
* DiscountCenter product developed. (2015-11-16)
* *
*
*  CREATED BY MODULESGARDEN       ->       http://modulesgarden.com
*  CONTACT                        ->       contact@modulesgarden.com
*
*
* This software is furnished under a license and may be used and copied
* only  in  accordance  with  the  terms  of such  license and with the
* inclusion of the above copyright notice.  This software  or any other
* copies thereof may not be provided or otherwise made available to any
* other person.  No title to and  ownership of the  software is  hereby
* transferred.
*
*
**********************************************************************}
{if !$hideGuide}
    <div class="alert alert-info" role="alert">

        <p style="color:#31708F">{$MGLANG->T('steps')}</p>
        <ol>
            <li style="color:#31708F">{$MGLANG->T('step1')}   &rarr; {$MGLANG->T('Add New')}  </li>
            <li style="color:#31708F">{$MGLANG->T('step2')} &rarr; {$MGLANG->T('enter number of days')} &rarr; {$MGLANG->T('Save Changes')}</li>
            <li style="color:#31708F">{$MGLANG->T('step3')} </li>
        </ol>
            <p style="color:#31708F">{$MGLANG->T('step4')} <a href="http://www.docs.modulesgarden.com/Discount_Center_For_WHMCS" target="_blank">{$MGLANG->T('Wiki.')} </a></p>

        <br/>  
        <button data-dismiss="alert" class="btn btn-inverse btn-info" type="button">{$MGLANG->T('Hide Guide')}</button>
    </div>
{/if}

<div class="row">
                        
    <div class="col-md-6">

        <div class="box light bordered">
            <div class="box-title">
                <div class="caption font-black">
                    <i class="icon-badge font-black"></i>
                    <span class="caption-subject font-black bold uppercase">{$MGLANG->T('Summary')}</span>
                </div>

            </div>
            <div class="box-body">
                <div class="scroller" style="height:300px" data-handle-color="#a1b2bd">
                    <div class="pull-left">{$MGLANG->T('Discounts')}</div>
                    <div class="pull-right" style="margin-right: 20px;">{$summary.discounts}</div>
                    <div class="clearfix"></div>
                    <hr/>
                    
                    <div class="pull-left">{$MGLANG->T('Quantity Discounts')}</div>
                    <div class="pull-right" style="margin-right: 20px;">{$summary.quantity}</div>
                    <div class="clearfix"></div>
                    <hr/>
                    
                    <div class="pull-left">{$MGLANG->T('Most used Discount')}</div>
                    <div class="pull-right" style="margin-right: 20px;">{$summary.topDiscount}</div>
                    <div class="clearfix"></div>
                    <hr/>
                    <div class="pull-left">{$MGLANG->T('Most used Quantity Discount')}</div>
                    <div class="pull-right" style="margin-right: 20px;">{$summary.topQuantity}</div>
                    <div class="clearfix"></div>
                    <hr/>
                    
                    <div class="pull-left">{$MGLANG->T('The largest discount in single purchase')}</div>
                    <div class="pull-right" style="margin-right: 20px;">{$summary.topPurchase}</div>
                    <div class="clearfix"></div>
                    <hr/>
                </div>
            </div>
        </div>

    </div>
    <div class="col-md-6">
        <!-- BEGIN box box-->
        <div class="box light bordered">
            <div class="box-title">
                <div class="caption font-black">
                    <i class="icon-share font-black"></i>
                    <span class="caption-subject bold uppercase">{$MGLANG->T('Discounts Overview')}</span>
                </div>
                    <div class="pull-right" style="margin-right: 20px;  margin-top:8px;">{$MGLANG->T('Period')}
                        <select id="mg-chart-period" >
                                    <option selected="" value="week">{$MGLANG->T('Week')}</option>
                                    <option value="month">{$MGLANG->T('Month')}</option>
                                    <option value="year">{$MGLANG->T('Year')}</option>
                        </select>
                    </div>
            </div>
            <div class="box-body">
                    
                <div style="width:100%;height:300px;" id="mg-chart-1">
                    <div style="position: relative;">

                    </div>
                </div>
            </div>
        </div>
        <!-- END box box-->
    </div>
</div>
{literal}
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript">
        google.load("visualization", "1", {packages:["corechart"]});
        jQuery(document).ready(function () {
            
            $('#mg-chart-period').on('change',function(){
                    JSONParser.request(
                            'chart'
                            , {
                                period: $(this).val()
                            }
                    , function (data) {
                         var jsonData = new google.visualization.DataTable(data.jsonData);
                         var options = { legend: {position: "top"},hAxis: {title: data.horizontalTitle},vAxis: {title: data.verticalTitle},colors: ["#80D044","#CCCCCC"],chartArea: {left:80,top:40,width:"85%",height:"70%"} };
                         var chart = new google.visualization.AreaChart(document.getElementById("mg-chart-1"));
                         chart.draw(jsonData,options);
                    });
            });
            $('#mg-chart-period').change();
            
        });
        
    </script>
{/literal}