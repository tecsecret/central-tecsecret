{**********************************************************************
* DiscountCenter product developed. (2015-12-03)
* *
*
*  CREATED BY MODULESGARDEN       ->       http://modulesgarden.com
*  CONTACT                        ->       contact@modulesgarden.com
*
*
* This software is furnished under a license and may be used and copied
* only  in  accordance  with  the  terms  of such  license and with the
* inclusion of the above copyright notice.  This software  or any other
* copies thereof may not be provided or otherwise made available to any
* other person.  No title to and  ownership of the  software is  hereby
* transferred.
*
*
**********************************************************************}

{**
* @author Paweł Kopeć <pawelk@modulesgarden.com>
*}
<div class="col-lg-12" id="mg-discount" >
    <legend>{if $id} {$MGLANG->T('header','Edit Discount')} {else}{$MGLANG->T('header','Add Quantity Discount')}{/if}</legend>
    <ul class="nav nav-tabs" id="mg-discount-nav-tab">
        <li class="active"><a href="#mg-tab-general" id="mg-nav-general">{$MGLANG->T('General')}</a></li>
        <li><a href="#mg-tab-products" id="mg-nav-product" {if !$isEdit}  class="disabled-link"{/if}>{$MGLANG->T('quantity Discounts')}</a></li>
        <li><a href="#mg-tab-rules"  id="mg-nav-rules" {if !$isEdit} class="disabled-link"{/if} >{$MGLANG->T('Activation Rules')}</a></li>
    </ul>
    <form data-toggle="validator" role="form" id="mg-form-add">
        <input type="hidden" name="id" value="{$id}" />
        <div class="tab-content">

            {* General *}
            <div class="tab-pane active" id="mg-tab-general" >

                {$generalForm}
                <div class="pull-right" {if $isEdit} style="display:none;" {/if}>
                    <a href="#mg-nav-product" id="mg-button-next-1"  class="btn btn-primary btn-inverse next-x">{$MGLANG->T('next')} &rarr; </a>
                </div>
            </div>
            {* quantity Discounts *}
            <div class="tab-pane" id="mg-tab-products">
                <div class="form-horizontal">
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="mg-quantity-apply-type">{$MGLANG->T('Apply To')}</label>
                        <div class="col-sm-8">
                            <select name="quantity[applyType]" id="mg-quantity-apply-type">
                                <option value="all" {if $discount.applyType=="all"}selected{/if}>{$MGLANG->T('All Required Products')}</option>
                                <option value="more_expensive" {if $discount.applyType=="more_expensive"}selected{/if}>{$MGLANG->T('more_expensive')}</option>
                                <option value="less_expensive" {if $discount.applyType=="less_expensive"}selected{/if}>{$MGLANG->T('less_expensive')}</option>
                                <option value="products" {if $discount.applyType=="products"}selected{/if}>{$MGLANG->T('products')}</option>
                                <option value="active_services_quantity" {if $discount.applyType=="active_services_quantity"}selected{/if}>{$MGLANG->T('Active Services Quantity')}</option>
                            </select>
                            <span class="help-block error-block"></span>
                        </div>
                    </div>
                    <div class="form-group" id="mg-quantity-group-price" style="display: none;">
                        <label class="col-sm-2 control-label" for="mg-quantity-apply-price-limit">{$MGLANG->T('Price Limit')}</label>
                        <div class="col-sm-8">
                            <input type="text" data-error="{$MGLANG->T('Field is required')}" placeholder="{$MGLANG->T('PriceLimitPlaceholder')}" id="mg-quantity-apply-price-limit" class="form-control" value="{$discount.priceLimit}" name="quantity[priceLimit]">
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                    {$promotionalProductsForm}
                    <div class="form-group">
                        <div class="col-sm-12">
                            <legend id="newDiscount_formGroup_promotionalProductsLabel">{$MGLANG->T('Thresholds')}</legend>
                        </div>
                    </div>
                    {section name=k start=0 loop=$loopThresholds step=1}
                        {assign var="i" value=$smarty.section.k.index}
                        <div class="form-group">
                            <label class="col-sm-2 control-label"></label>
                            <input type="hidden" name="threshold[id][]" value="{$thresholds.$i.id}" />

                            <div class="col-sm-2" style="width:11%;">
                                <input type="number" min="1" max="10000" data-error="{$MGLANG->T('Field is required')}" placeholder="1" class="form-control mg-input-quantity" value="{$thresholds.$i.quantity}" name="threshold[quantity][]" requried>
                                <div  class="help-block with-errors mg-error-quantity" style="display:none;"></div>
                            </div>
                            <div class="col-sm-2  control-label" style="text-align: left; padding-left: 0px;"> 
                                {$MGLANG->T('quantity of required products in order')}
                            </div>

                            <div class="col-sm-2" style="width:11%;">
                                <input type="text" data-error="{$MGLANG->T('That value is invalid')}" pattern={literal}"^\d{0,2}(\.\d{0,2})?$"{/literal} placeholder="10.00" class="form-control mg-input-promotion" value="{$thresholds.$i.promotion}" name="threshold[promotion][]" requried>
                                <div  class="help-block with-errors mg-error-promotion" style="display:none;"></div>
                            </div>
                            <div class="col-sm-2  control-label" style="text-align: left; padding-left: 0px;"> 
                                {$MGLANG->T('discount')}
                            </div>
                            <button  type="button" class="btn btn-primary mg-clone-add" style="" title="{$MGLANG->T('Add Threshold')}">               
                                <i class="glyphicon glyphicon-plus"></i>
                            </button>
                            <button  type="button" class="btn btn-danger mg-clone-remove" {if $loopThresholds==1}style="display:none;"{/if} title="{$MGLANG->T('remove')}">               
                                <i class="glyphicon glyphicon-remove"></i>
                            </button>
                        </div> 
                    {/section}
                </div>
                <div class="pull-left pager-x" {if $isEdit} style="display:none;" {/if}>
                    <a href="#mg-nav-general" class="btn btn-primary btn-inverse">&larr; {$MGLANG->T('Back')} </a>
                </div>
                <div class="pull-right pager-x">
                    <a href="#mg-nav-rules" class="btn btn-primary btn-inverse next-x" {if $isEdit} style="display:none;" {/if}>{$MGLANG->T('next')} &rarr; </a>
                </div>
                
            </div>

            {* Activation Rules *}
            <div class="tab-pane" id="mg-tab-rules">
                {$activationRules}
                <div class="pull-left pager-x" {if $isEdit} style="display:none;" {/if}>
                    <a href="#mg-nav-product" class="btn btn-primary btn-inverse">&larr; {$MGLANG->T('Back')} </a>
                </div>
                <div class="pull-right pager-x">
                    <a href="#" class="btn btn-success btn-inverse next-x" id="mg-button-nav-save">{$MGLANG->T('Save Changes')}</a>
                </div>
            </div>
             <br/> <br/>
        </div>
    </form>
</div>
{literal}
    <style type="text/css">
        #mg-discount-nav-tab .disabled-link{
            color: #98aeb5 !important;
        }
    </style>
    <script type="text/javascript">
        jQuery(document).ready(function () {
            
            function mgValidateForm(attrId) {
                var result = true;
                $(attrId + ' .form-group').each(function () {
                    if ($(this).hasClass('has-error')) {
                        result = false;
                    }
                });
                return result;
            }
            
            //Nav back next
            $('.tab-pane .pager-x a').click(function (e) {
                if($(this).hasClass('disabled-link'))
                    return; 
                $("#mg-form-add").validator('validate');
                if (mgValidateForm("#mg-form-add") !== true)
                    return;
                
                e.preventDefault();
                var href = $(this).attr("href");
                if(href !="#"){
                    $(href).removeClass('disabled-link');
                    $(href).tab('show');
                }
            });

            //# Show Tabs
            $('#mg-discount-nav-tab a').click(function (e) {
                if($(this).hasClass('disabled-link'))
                    return; 
                
                $("#mg-form-add").validator('validate');
                if (mgValidateForm("#mg-form-add") !== true)
                    return;
                
                e.preventDefault();
                $(this).tab('show');
            });
            
            //# 1 must select one requried product
            $("#mg-button-next-1").click(function (e) {
                e.preventDefault();
                if($("#mg-quantity-required-products").val() || $("#mg-quantity-required-addons").val() ||  $("#mg-quantity-required-domains").val() ){
                    $("#mg-form-add").validator('validate');
                    if (mgValidateForm("#mg-form-add") !== true)
                        return;
                    var href = $(this).attr("href");
                    if(href !="#"){
                        $(href).removeClass('disabled-link');
                        $(href).tab('show');
                    }
                }else{
                    $("#mg-form-add").validator('validate');
                    $('#MGAlerts').alerts('error',"{/literal}{$MGLANG->T("Field 'Products' or 'Addons' or 'Domains' is required")}{literal}");
                }
            });
            
            //# 2
            $("#mg-quantity-apply-type").select2();
            
            //#2 cloning add
            $("#mg-discount").delegate(".mg-clone-add", "click", function (e) {
                e.preventDefault();
                var content = $(this).closest('.form-group').clone();
                content.find('input[name="threshold[id][]"]').val('');
                $(this).closest('.form-group').after(content);
                $(".mg-clone-remove").show();
                
                $("#mg-form-add").validator('destroy');
                $("#mg-form-add").validator('validate');
            });

            //#2 cloning remove
            $("#mg-discount").delegate(".mg-clone-remove", "click", function (e) {
                e.preventDefault();
                $(this).closest('.form-group').remove();
                if ($(".mg-clone-remove").size() == 1)
                    $(".mg-clone-remove").hide();
            });
            //#2 Apply Type
            $("#mg-quantity-apply-type").change(function(){
                switch($(this).val()){
                    case 'more_expensive':
                    case 'less_expensive':
                        $("#mg-quantity-group-price").show();
                        $(".mg-promotional-products").addClass("hidden");
                        $("#rules_modal_rules_recurring").parent('.form-group').show();
                        $('input[type=checkbox][name="rules[recurring][]"').trigger('change');
                        $("#mg-discount-unpaidInvoices").parent('.form-group').hide();
                        break;
                   case 'all':
                   case 'paid':
                       $("#mg-quantity-group-price").hide();
                       $(".mg-promotional-products").addClass("hidden");
                       $("#mg-discount-unpaidInvoices").parent('.form-group').hide();
                       break;
                   case 'active_services_quantity':
                       $("#mg-quantity-group-price").hide();
                       $(".mg-promotional-products").addClass("hidden");
                       $("#rules_modal_rules_recurring").parent('.form-group').hide();
                       $("#rules_recurringMonths").closest('.form-group').hide();
                       $("#mg-discount-unpaidInvoices").parent('.form-group').show();
                       break;
                   case 'products':
                       $("#mg-quantity-group-price").hide();
                       $(".mg-promotional-products").removeClass("hidden");
                       $("#rules_modal_rules_recurring").parent('.form-group').show();
                       $('input[type=checkbox][name="rules[recurring][]"').trigger('change');
                       $("#mg-discount-unpaidInvoices").parent('.form-group').hide();
                        break;
                }
            });
            $("#mg-quantity-apply-type").change();
            
            // # 2 Thresholds
            $("#mg-discount").delegate(".mg-input-quantity", "click", function (e) {
                $(this).closest('.form-group').find(".mg-error-quantity").html('').show();
                $(this).closest('.form-group').find(".mg-error-promotion").html('').hide();
            });
            $("#mg-discount").delegate(".mg-input-promotion", "click", function (e) {
                 $(this).closest('.form-group').find(".mg-error-promotion").html('').show();
                 $(this).closest('.form-group').find(".mg-error-quantity").html('').hide();
            });
            
            // # 3 Clients  checboxes
            $('#mg-quantity-clientTyps input[type=checkbox][name="rules[clientTyps][]"][value=all]').change(function(){
               if($(this).prop('checked')){
                   $('#mg-quantity-clientTyps input[type=checkbox][name="rules[clientTyps][]"][value=new]').prop("checked",false);
                   $('#mg-quantity-clientTyps input[type=checkbox][name="rules[clientTyps][]"][value=chosen]').prop("checked",false).change();
                   //Groups
                   $(this).closest('.form-group').next('.form-group').next('.form-group').hide();
               }else{
                   //Groups
                   $(this).closest('.form-group').next('.form-group').next('.form-group').show();
               }               
            });
            $('#mg-quantity-clientTyps input[type=checkbox][name="rules[clientTyps][]"][value=all]').change();
            
            // # 3 Chosen clients
            $('#mg-quantity-clientTyps input[type=checkbox][name="rules[clientTyps][]"][value=chosen]').change(function(){
                if($(this).prop('checked')){
                    $(this).closest('.form-group').next('.form-group').show();
                    $('#mg-quantity-clientTyps input[type=checkbox][name="rules[clientTyps][]"][value=all]').prop("checked",false).change();
                }else{
                    $(this).closest('.form-group').next('.form-group').hide();
                }
            });
            $('#mg-quantity-clientTyps input[type=checkbox][name="rules[clientTyps][]"][value=chosen]').change();
            //# 3 Clients new
            $('#mg-quantity-clientTyps input[type=checkbox][name="rules[clientTyps][]"][value=new]').change(function(){
                if($(this).prop('checked'))
                    $('#mg-quantity-clientTyps input[type=checkbox][name="rules[clientTyps][]"][value=all]').prop("checked",false).change();
            });
            
            //#3 Save changes
            $("#mg-button-nav-save").click(function(e){
                e.preventDefault();
                $(this).prop("disabled", true);
                JSONParser.request(
                        'saveQuantityDiscount'
                        , $("#mg-form-add").serialize()
                        , function (data) {
                            console.log(data);
                            $(this).prop("disabled", false);
                            if( data.success)  //$('#mg-form-add input[name="id"]').val()=="" &&
                                window.location.href = "addonmodules.php?module=DiscountCenter&mg-page=quantityDiscounts"; 
                        }
                );
            });
            
            $("#mg-quantity-clients, #mg-quantity-clients-edit").select2({
                ajax: {
                    url: JSONParser.url + "&mg-action=searchCustomer",
                    dataType: 'json',
                    delay: 250,
                    multiple: true,
                    data: function (params) {
                        return {
                            q: params.term, // search term
                            page: params.page
                        };
                    },
                    processResults: function (data, page) {
                        return {
                            results: data.items
                        };
                    },
                    cache: true
                },
                escapeMarkup: function (markup) {
                    return markup;
                }, // let our custom formatter work
                minimumInputLength: 1,
                templateResult: function (repo) {
                    return repo.name || repo.text;
                },
                templateSelection: function (repo) {
                    if (repo.loading)
                        return repo.text;
                    var markup = "";
                    if (repo.text)
                        return repo.text;
                    if (repo.name)
                        return  repo.name;
                    if (repo.description) {
                        markup += '<div>' + repo.description + '</div>';
                    }
                    return markup;
                }
            });
            
            //Select2 Groups
            $( "body" ).on( "click", ".select2-results__group", function(e) {
                var id = $(this).closest('ul').attr('id');
                id = "#"+id.replace('-results', '').replace('select2-','');
                var label  = $(this).parent().attr('aria-label');
                var select = $(id);
                select.find('optgroup').each(function( index ) {

                  if($(this).attr('label')==label){
                    $(this).find('option').each(function( index ) {
                      $(this).prop('selected', !$(this).is(':selected'));
                    });
                  }
                });
                select.trigger("change").select2("close").select2("open");

            });
            
            //Select2 All
            $( ".mg-wrapper" ).on( "select2:select", ".select2-hidden-accessible", function(e) {
                 if(e.params.data.id !=="-1")
                     return;
                 $(this).find('option').each(function( index ) {
                     $(this).prop('selected', !$(this).is(':selected'));
                 }); 
                 $(this).trigger("change");
            }); 
            
            $(".mg-datetime-picker").datetimepicker({ locale: 'en',  format: 'YYYY-MM-DD HH:mm'});
            
            //dates
            $('#mg-discount-includeDateTime input[type=checkbox][name="discount[includeDateTime][]').change(function(){
                if($(this).prop('checked')){
                    $(this).closest('.form-group').next('.form-group').show();
                    $(this).closest('.form-group').next('.form-group').next('.form-group').show();
                }else{
                    $(this).closest('.form-group').next('.form-group').hide();
                    $(this).closest('.form-group').next('.form-group').next('.form-group').hide();
                }
            });
            $('#mg-discount-includeDateTime input[type=checkbox][name="discount[includeDateTime][]').trigger('change');
            
        });
        
        //recurringMonths
        $('input[type=checkbox][name="rules[recurring][]"').change(function(){
            if($(this).prop('checked')){
                $(this).closest('.form-group').next('.form-group').show();
            }else{
                $(this).closest('.form-group').next('.form-group').hide();
            }
        });
        $('input[type=checkbox][name="rules[recurring][]"').trigger('change');    
        
    </script>
{/literal}