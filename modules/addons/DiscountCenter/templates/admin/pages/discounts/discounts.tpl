{**********************************************************************
* DiscountCenter product developed. (2015-11-16)
* *
*
*  CREATED BY MODULESGARDEN       ->       http://modulesgarden.com
*  CONTACT                        ->       contact@modulesgarden.com
*
*
* This software is furnished under a license and may be used and copied
* only  in  accordance  with  the  terms  of such  license and with the
* inclusion of the above copyright notice.  This software  or any other
* copies thereof may not be provided or otherwise made available to any
* other person.  No title to and  ownership of the  software is  hereby
* transferred.
*
*
**********************************************************************}

{**
* @author Paweł Kopeć <pawelk@modulesgarden.com>
*}
<div class="col-lg-12" id="mg-discounts" >
    <legend>{$MGLANG->T('header','Discounts')} 
        <button type="button" class="btn btn-default btn-xs" id="mg-button-toggle" style="float:right;">{$MGLANG->T('filters','toggle')}</button>
    </legend>
    <div id="mg-form-filters" style="display:none;" class="well well-sm" style="margin-bottom: 10px;">
        <form>
            <div class="row">
                <div class="col-md-3">                  
                    <div class="form-group">
                        <label for="mg-filters-product">{$MGLANG->T('Product')} </label>
                        <select name='productId' class="form-control"  id="mg-filters-product">
                            <option value="0">---</option>
                            {foreach from=$products item=item  key=key}
                                <option value="{$key}" >{$item}</option>
                            {/foreach}
                        </select>
                    </div>
                    <button type="button" class="btn btn-warning" id="mg-filters-button-reset">{$MGLANG->T('filters','Reset')}</button>
                    <button type="button" class="btn btn-primary" id="mg-filters-button-filter">{$MGLANG->T('filters','Filter')}</button>
                </div>
                <div class="col-md-3">                  
                    <div class="form-group">
                        <label for="mg-filters-addon">{$MGLANG->T('Addon')} </label>
                        <select name='addonId' class="form-control"  id="mg-filters-addon">
                            <option value="0">---</option>
                            {foreach from=$addons item=item  key=key}
                                <option value="{$key}" >{$item}</option>
                            {/foreach}
                        </select>
                    </div>
                </div>
                <div class="col-md-3">                  
                    <div class="form-group">
                        <label for="mg-filters-domain">{$MGLANG->T('Domain')} </label>
                        <select name='domainId' class="form-control"  id="mg-filters-domain">
                            <option value="0">---</option>
                            {foreach from=$domains item=item  key=key}
                                <option value="{$key}" >{$item}</option>
                            {/foreach}
                        </select>
                    </div>
                </div>
                <div class="col-md-3"> 
                    <div class="form-group">
                        <label class="col-sm-2 control-label" style="padding-left:0px;">{$MGLANG->T('status')} </label>
                        <br>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name='enable'> 
                                {$MGLANG->T('enable')}
                            </label>
                        </div>

                    </div> 
                
                </div>

                
            </div>
        </form> 
    </div>

    <table class="table table-hover" id="mg-data-list" >
        <thead>
            <tr>
                <th>{$MGLANG->T('tableHeader','ID')}</th>
                <th>{$MGLANG->T('tableHeader','Name')}</th>
                <th style="width: 100px;">{$MGLANG->T('tableHeader','Assigned')}</th>
                <th style="width: 100px;">{$MGLANG->T('tableHeader','Status')}</th>
                <th style="width: 100px;">{$MGLANG->T('tableHeader','Start Date')}</th>
                <th style="width: 100px;">{$MGLANG->T('tableHeader','Expiry Date')}</th>
                <th style="width: 100px; text-align: center;">{$MGLANG->T('tableHeader','Actions')}</th>
            </tr>
        </thead>
        <tbody>
        </tbody> 
    </table>

    <div class="well well-sm" style="margin-top:10px;">
        <a class="btn btn-success btn-inverse" href="addonmodules.php?module=DiscountCenter&mg-page=discounts&mg-action=addDiscount">               
            <i class="glyphicon glyphicon-plus"></i>
            {$MGLANG->T('button','Add New')}
        </a>
    </div>

    {*Modal delete*}
    <div class="modal fade bs-example-modal-lg" id="mg-modal-delete-discount" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title">{$MGLANG->T('modal','Delete Discount')} <strong></strong></h4>
                </div>
                <div class="modal-loader" style="display:none;"></div>

                <div class="modal-body">
                    <input type="hidden" name="id" data-target="id" value="">
                    <div class="modal-alerts">
                        <div style="display:none;" data-prototype="error">
                            <div class="note note-danger">
                                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only"></span></button>
                                <strong></strong>
                                <a style="display:none;" class="errorID" href=""></a>
                            </div>
                        </div>
                        <div style="display:none;" data-prototype="success">
                            <div class="note note-success">
                                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only"></span></button>
                                <strong></strong>
                            </div>
                        </div>
                    </div>
                    <div style="margin: 30px; text-align: center;">

                        <div>{$MGLANG->T('Are you sure you want to delete this entry from discounts list?')} </div>
                    </div>
                </div>


                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-modal-action="deleteDiscount" id="pm-modal-addip-button-add">{$MGLANG->T('modal','delete')}</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">{$MGLANG->T('modal','close')}</button>
                </div>
            </div>
        </div>
    </div>

</div>

{literal}
    <script type="text/javascript">
        var mgDataTable;

        jQuery(document).ready(function () {
            
            mgDataTable = $('#mg-data-list').dataTable({
                processing: false,
                searching: true,
                autoWidth: false,
                "serverSide": false,
                "order": [[0, "asc"]],
                ajax: function (data, callback, settings) {
                    JSONParser.request(
                            'list'
                            , {
                                filter: $("#mg-form-filters form").MGGetForms()
                                , limit: data.length
                                , offset: data.start
                                , order: data.order
                                , search: data.search
                            }
                    , function (data) {
                        callback(data);
                    }
                    );
                },
                'columns': [
                    , null
                    , null
                    , {orderable: false}
                    , {orderable: false}
                    , {orderable: false}
                    , {orderable: false}

                ],
                'aoColumnDefs': [{
                        'bSortable': false,
                        'aTargets': ['nosort']
                    }],
                language: {
                    "lengthMenu": "{/literal}{$MGLANG->absoluteT('Show _MENU_ entries')}{literal}",
                    "info": "{/literal}{$MGLANG->absoluteT('Showing _START_ to _END_ of _TOTAL_ entries')}{literal}",
                    "zeroRecords": "{/literal}{$MGLANG->T('Nothing to display')}{literal}",
                    "infoEmpty": "",
                    "search": "{/literal}{$MGLANG->T('search')}{literal}",
                    "paginate": {
                        "previous": "{/literal}{$MGLANG->T('previous')}{literal}"
                        , "next": "{/literal}{$MGLANG->T('next')}{literal}"
                    }
                }
            });

            jQuery('#mg-discounts').MGModalActions();
           
            // Filters
            $("#mg-button-toggle").click(function () {
                $("#mg-form-filters").toggle();
            });

            $("#mg-filters-button-filter").click(function(){
                var api = mgDataTable.api();
                    api.ajax.reload(function() {
                    }, false);
            });
            
            $("#mg-filters-button-reset").click(function(){
                $("#mg-form-filters").MGresetInputData();
                var api = mgDataTable.api();
                    api.ajax.reload(function() {
                    }, false);
            });
            
            
            $('#mg-modal-delete-discount').on('hidden.bs.modal', function () {
                var api = mgDataTable.api();
                  api.ajax.reload(function() {
                  }, false);
            });
            
            $('#mg-discounts').on('mgajax.saved', function (e) {
                var api = mgDataTable.api();
                    api.ajax.reload(function() {
                    }, false);
            });  

        });
    </script>
{/literal}