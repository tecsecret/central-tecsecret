{**********************************************************************
 * DiscountCenter product developed. (2016-02-16)
 * *
 *
 *  CREATED BY MODULESGARDEN       ->       http://modulesgarden.com
 *  CONTACT                        ->       contact@modulesgarden.com
 *
 *
 * This software is furnished under a license and may be used and copied
 * only  in  accordance  with  the  terms  of such  license and with the
 * inclusion of the above copyright notice.  This software  or any other
 * copies thereof may not be provided or otherwise made available to any
 * other person.  No title to and  ownership of the  software is  hereby
 * transferred.
 *
 *
 **********************************************************************}

{**
* @author Paweł Kopeć <pawelk@modulesgarden.com>
*}
<input type="hidden" name="{$itemName}[{$k}][{$period}][{$priceType}][currencyId]" value="{$itemPrince.currency.id}" />
<input type="hidden" name="{$itemName}[{$k}][{$period}][{$priceType}][relationId]" value="{$relationId}" />
<div class="form-group " style="padding-left:0px; margin:0px; padding-right: 0;">
    <select  class="form-control mg-discount-type" name="{$itemName}[{$k}][{$period}][{$priceType}][discountType]">
        <option value="0">{$MGLANG->T('Disable')}</option>
        <option value="percentage" {if $discountType=="percentage" || !$discountType} selected {/if}>{$MGLANG->T('Percentage')}</option>
        <option value="free_amount" {if $discountType=="free_amount"} selected {/if}>{$MGLANG->T('Fixed Amount')} </option>
    </select>
    <div class="help-block with-errors"></div>
</div>
<div class="clear"></div>
<div class="form-group " style="padding-left:0px; margin:0px; padding-right: 0;">
    <input type="text" name="{$itemName}[{$k}][{$period}][{$priceType}][value]" class="form-control mg-discount-value"
           placeholder="{$MGLANG->T("value","placeholder")}"
            value="{$discountValue}"
               data-price="{$price}" data-target="{$target}"  data-currency="{$itemPrince.currency.code}" data-pricetype="{$priceType}"
               data-mgvaluevalidation="mgvaluevalidation" data-mgvaluevalidation-error="{$MGLANG->T('That value is to high')}" 
               pattern={literal}"^\d{0,6}(\.\d{0,2})?$"{/literal} data-error="{$MGLANG->T('The value must be positive')}">
    <div class="help-block with-errors"></div>
</div>