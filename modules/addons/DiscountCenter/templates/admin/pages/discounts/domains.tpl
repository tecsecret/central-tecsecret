{**********************************************************************
 * DiscountCenter product developed. (2015-11-25)
 * *
 *
 *  CREATED BY MODULESGARDEN       ->       http://modulesgarden.com
 *  CONTACT                        ->       contact@modulesgarden.com
 *
 *
 * This software is furnished under a license and may be used and copied
 * only  in  accordance  with  the  terms  of such  license and with the
 * inclusion of the above copyright notice.  This software  or any other
 * copies thereof may not be provided or otherwise made available to any
 * other person.  No title to and  ownership of the  software is  hereby
 * transferred.
 *
 *
 **********************************************************************}

{**
* @author Paweł Kopeć <pawelk@modulesgarden.com>
*}
<legend>{$MGLANG->T('Domains')}</legend>
<div class="panel-group" id="mg-accordion-dom" role="tablist" aria-multiselectable="true">
    {assign var="k" value=0}
    {foreach from=$domains item=item}
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="mg-headingOne-domains" data-toggle="collapse" href="#mg-collapse-dom-{$item.domain.id}" data-parent="#mg-accordion-dom" aria-expanded="true" aria-controls="mg-collapse-dom-{$item.domain.id}">
                <h4 class="panel-title">
                    <a href="configdomains.php" target="blank">#{$item.domain.id}</a>
                    <a role="button" data-toggle="collapse" data-parent="#mg-accordion-dom" href="#mg-collapse-dom-{$item.domain.id}" aria-expanded="true" aria-controls="mg-collapse-dom-{$item.domain.id}">
                        {$item.domain.extension}
                    </a>
                    <span class="pull-right"><i class="glyphicon glyphicon-plus"></i></span>
                </h4>
            </div>
            <div id="mg-collapse-dom-{$item.domain.id}" class="panel-collapse collapse {*if $i==1} in{/if*}" role="tabpanel" aria-labelledby="mg-headingOne-domains">
                <div class="panel-body">
                    <table class="table mg-promotion-table">
                        <tbody>

                            <tr class="active">
                                <td style="text-align:center;">{$MGLANG->T('Currency')}</td>
                                <td></td>
                                <td>{$MGLANG->T('1 Year')}</td>
                                <td>{$MGLANG->T('2 Years')}</td>
                                <td>{$MGLANG->T('3 Years')}</td>
                                <td>{$MGLANG->T('4 Years')}</td>
                                <td>{$MGLANG->T('5 Years')}</td>
                                <td>{$MGLANG->T('6 Years')}</td>
                                <td>{$MGLANG->T('7 Years')}</td>
                                <td>{$MGLANG->T('8 Years')}</td>
                                <td>{$MGLANG->T('9 Years')}</td>
                                <td>{$MGLANG->T('10 Years')}</td>
                            </tr>
                            
                            {foreach from=$item.pricing item=itemPrince}
                                {assign var="priceType" value=$itemPrince.price.type}
                                <tr>
                                    <td rowspan="2" class="active"  style="text-align:center;">{$itemPrince.currency.code}</td> {*Currency*}
                                    <td>{$MGLANG->T($itemPrince.price.type, 'Price')}</td>
                                    <td class="mg-row-price_1">{*1 Year*} 
                                        {if in_array($itemPrince.price.monthlySetupFee,["-1","0.00"]) } - {else}
                                            <span class="mg-row-price-regular"> {$itemPrince.price.monthlySetupFee} {$itemPrince.currency.code}</span> 
                                            <span class="mg-row-price-strike" style="display: none;">{$itemPrince.price.monthlySetupFee} {$itemPrince.currency.code}</span> <span class="mg-row-price-promotion"></span>
                                           {include file="copy.tpl"}
                                        {/if}
                                    </td>
                                    <td class="mg-row-price_2">{*2 Years*}
                                        {if in_array($itemPrince.price.quarterlySetupFee,["-1","0.00"])} - {else}
                                            <span class="mg-row-price-regular"> {$itemPrince.price.quarterlySetupFee} {$itemPrince.currency.code}</span> 
                                            <span class="mg-row-price-strike" style="display: none;">{$itemPrince.price.quarterlySetupFee} {$itemPrince.currency.code}</span> <span class="mg-row-price-promotion"></span>
                                           {include file="copy.tpl"}
                                        {/if} 
                                        </td>
                                    <td class="mg-row-price_3">{*3 Years*}
                                        {if in_array($itemPrince.price.semiAnnuallySetupFee,["-1","0.00"])} - {else}
                                            <span class="mg-row-price-regular"> {$itemPrince.price.semiAnnuallySetupFee} {$itemPrince.currency.code} </span> 
                                            <span class="mg-row-price-strike" style="display: none;">{$itemPrince.price.semiAnnuallySetupFee} {$itemPrince.currency.code}</span> <span class="mg-row-price-promotion"></span>
                                            {include file="copy.tpl"}
                                        {/if}
                                    </td>
                                    <td class="mg-row-price_4">{*4 Years*}
                                        {if in_array($itemPrince.price.annuallySetupFee,["-1","0.00"])}- {else}
                                            <span class="mg-row-price-regular"> {$itemPrince.price.annuallySetupFee} {$itemPrince.currency.code}</span> 
                                            <span class="mg-row-price-strike" style="display: none;">{$itemPrince.price.annuallySetupFee} {$itemPrince.currency.code}</span> <span class="mg-row-price-promotion"></span>
                                           {include file="copy.tpl"}
                                        {/if} 
                                    </td>
                                    <td class="mg-row-price_5">{*5 Years*}
                                        {if in_array($itemPrince.price.bienniallySetupFee,["-1","0.00"])} - {else}
                                            <span class="mg-row-price-regular"> {$itemPrince.price.bienniallySetupFee} {$itemPrince.currency.code} </span> 
                                            <span class="mg-row-price-strike" style="display: none;">{$itemPrince.price.bienniallySetupFee} {$itemPrince.currency.code}</span> <span class="mg-row-price-promotion"></span>
                                            {include file="copy.tpl"}
                                        {/if}
                                    </td>
                                    <td class="mg-row-price_6">{*6 Years*}
                                        {if in_array($itemPrince.price.monthly,["-1","0.00"])} - {else}
                                            <span class="mg-row-price-regular"> {$itemPrince.price.monthly} {$itemPrince.currency.code}</span> 
                                            <span class="mg-row-price-strike" style="display: none;">{$itemPrince.price.monthly} {$itemPrince.currency.code}</span> <span class="mg-row-price-promotion"></span>
                                            {include file="copy.tpl"}
                                        {/if}
                                    </td>
                                    <td class="mg-row-price_7">{*7 Years*}
                                        {if in_array($itemPrince.price.quarterly,["-1","0.00"])} - {else}
                                            <span class="mg-row-price-regular"> {$itemPrince.price.quarterly} {$itemPrince.currency.code}</span> 
                                            <span class="mg-row-price-strike" style="display: none;">{$itemPrince.price.quarterly} {$itemPrince.currency.code}</span> <span class="mg-row-price-promotion"></span>
                                           {include file="copy.tpl"}
                                        {/if}
                                    </td>
                                    <td class="mg-row-price_8">{*8 Years*}
                                        {if in_array($itemPrince.price.semiAnnually,["-1","0.00"])} - {else}
                                            <span class="mg-row-price-regular"> {$itemPrince.price.semiAnnually} {$itemPrince.currency.code}</span> 
                                            <span class="mg-row-price-strike" style="display: none;">{$itemPrince.price.semiAnnually} {$itemPrince.currency.code}</span> <span class="mg-row-price-promotion"></span>
                                           {include file="copy.tpl"}
                                        {/if}
                                    </td>
                                    <td class="mg-row-price_9">{*9 Years*}
                                        {if in_array($itemPrince.price.annually,["-1","0.00"])} - {else}
                                            <span class="mg-row-price-regular"> {$itemPrince.price.annually} {$itemPrince.currency.code}</span> 
                                            <span class="mg-row-price-strike" style="display: none;">{$itemPrince.price.annually} {$itemPrince.currency.code}</span> <span class="mg-row-price-promotion"></span>
                                            {include file="copy.tpl"}
                                        {/if}
                                    </td>
                                    <td class="mg-row-price_10">{*10 Years*}
                                        {if in_array($itemPrince.price.biennially,["-1","0.00"])} - {else}
                                            <span class="mg-row-price-regular"> {$itemPrince.price.biennially} {$itemPrince.currency.code}</span> 
                                            <span class="mg-row-price-strike" style="display: none;">{$itemPrince.price.biennially} {$itemPrince.currency.code}</span> <span class="mg-row-price-promotion"></span>
                                            {include file="copy.tpl"}
                                        {/if}
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td class="mg-promotion">{$MGLANG->T('Promotion',$itemPrince.price.type,'Promotion')}</td>                                            
                                    <td class="mg-promotion-form-dom-td">{*1 Year*}
                                        {if in_array($itemPrince.price.monthlySetupFee,["-1","0.00"])} 
                                            - 
                                        {else}
                                            {include file="promotion.tpl" itemName="domains"  period="YEAR" relationId=$item.domain.id
                                            price=$itemPrince.price.monthlySetupFee
                                            priceType=$priceType
                                            discountType=$post.domains.$k.YEAR.$priceType.discountType
                                            discountValue=$post.domains.$k.YEAR.$priceType.value target=".mg-row-price_1"}
                                        {/if}
                                    </td>
                                    <td class="mg-promotion-form-dom-td">{*2 Years*}
                                        {if in_array($itemPrince.price.quarterlySetupFee,["-1","0.00"])} 
                                            - 
                                        {else}
                                            {include file="promotion.tpl" itemName="domains"  period="YEARS_2" relationId=$item.domain.id
                                            price=$itemPrince.price.quarterlySetupFee
                                            priceType=$priceType
                                            discountType=$post.domains.$k.YEARS_2.$priceType.discountType
                                            discountValue=$post.domains.$k.YEARS_2.$priceType.value target=".mg-row-price_2"}
                                        {/if}
                                    </td>
                                    <td class="mg-promotion-form-dom-td">{*3 Years*}
                                        {if in_array($itemPrince.price.semiAnnuallySetupFee,["-1","0.00"])} 
                                            - 
                                        {else}
                                            {include file="promotion.tpl" itemName="domains"  period="YEARS_3" relationId=$item.domain.id
                                            price=$itemPrince.price.semiAnnuallySetupFee
                                            priceType=$priceType
                                            discountType=$post.domains.$k.YEARS_3.$priceType.discountType
                                            discountValue=$post.domains.$k.YEARS_3.$priceType.value target=".mg-row-price_3"}
                                        {/if}
                                    </td>
                                    <td class="mg-promotion-form-dom-td">{*4 Years*}
                                        {if in_array($itemPrince.price.annuallySetupFee,["-1","0.00"])} 
                                            - 
                                        {else}
                                            {include file="promotion.tpl" itemName="domains"  period="YEARS_4" relationId=$item.domain.id
                                            price=$itemPrince.price.annuallySetupFee
                                            priceType=$priceType
                                            discountType=$post.domains.$k.YEARS_4.$priceType.discountType
                                            discountValue=$post.domains.$k.YEARS_4.$priceType.value target=".mg-row-price_4"}
                                        {/if}
                                    </td>
                                    <td class="mg-promotion-form-dom-td">{*5 Years*}
                                        {if in_array($itemPrince.price.bienniallySetupFee,["-1","0.00"])} 
                                            - 
                                        {else}
                                            {include file="promotion.tpl" itemName="domains"  period="YEARS_5" relationId=$item.domain.id
                                            price=$itemPrince.price.bienniallySetupFee
                                            priceType=$priceType
                                            discountType=$post.domains.$k.YEARS_5.$priceType.discountType
                                            discountValue=$post.domains.$k.YEARS_5.$priceType.value target=".mg-row-price_5"}
                                        {/if}
                                    </td>
                                    <td class="mg-promotion-form-dom-td">{*6 Years*}
                                        {if in_array($itemPrince.price.monthly,["-1","0.00"])} 
                                            - 
                                        {else}
                                            {include file="promotion.tpl" itemName="domains"  period="YEARS_6" relationId=$item.domain.id
                                            price=$itemPrince.price.monthly
                                            priceType=$priceType
                                            discountType=$post.domains.$k.YEARS_6.$priceType.discountType
                                            discountValue=$post.domains.$k.YEARS_6.$priceType.value target=".mg-row-price_6"}
                                        {/if}
                                    </td>
                                    <td class="mg-promotion-form-dom-td">{*7 Years*}
                                        {if in_array($itemPrince.price.quarterly,["-1","0.00"])} 
                                            - 
                                        {else}
                                            {include file="promotion.tpl" itemName="domains"  period="YEARS_7" relationId=$item.domain.id
                                            price=$itemPrince.price.quarterly
                                            priceType=$priceType
                                            discountType=$post.domains.$k.YEARS_7.$priceType.discountType
                                            discountValue=$post.domains.$k.YEARS_7.$priceType.value target=".mg-row-price_7"}
                                        {/if}
                                    </td>
                                    <td class="mg-promotion-form-dom-td">{*8 Years*}
                                        {if in_array($itemPrince.price.semiAnnually,["-1","0.00"])} 
                                            - 
                                        {else}
                                            {include file="promotion.tpl" itemName="domains"  period="YEARS_8" relationId=$item.domain.id
                                            price=$itemPrince.price.semiAnnually
                                            priceType=$priceType
                                            discountType=$post.domains.$k.YEARS_8.$priceType.discountType
                                            discountValue=$post.domains.$k.YEARS_8.$priceType.value target=".mg-row-price_8"}
                                        {/if}
                                    </td>
                                    <td class="mg-promotion-form-dom-td">{*9 Years*}
                                        {if in_array($itemPrince.price.annually,["-1","0.00"])} 
                                            - 
                                        {else}
                                            {include file="promotion.tpl" itemName="domains"  period="YEARS_9" relationId=$item.domain.id
                                            price=$itemPrince.price.annually
                                            priceType=$priceType
                                            discountType=$post.domains.$k.YEARS_9.$priceType.discountType
                                            discountValue=$post.domains.$k.YEARS_9.$priceType.value target=".mg-row-price_9"}
                                        {/if}
                                    </td>
                                    <td class="mg-promotion-form-dom-td">{*10 Years*}
                                        {if in_array($itemPrince.price.biennially,["-1","0.00"])} 
                                            - 
                                        {else}
                                            {include file="promotion.tpl" itemName="domains"  period="YEARS_10" relationId=$item.domain.id
                                            price=$itemPrince.price.biennially
                                            priceType=$priceType
                                            discountType=$post.domains.$k.YEARS_10.$priceType.discountType
                                            discountValue=$post.domains.$k.YEARS_10.$priceType.value target=".mg-row-price_10"}
                                        {/if}
                                    </td>
                                </tr>
                                {assign var="k" value=$k+1}
                            {foreachelse}
                                <tr>
                                    <td colspan='8' style="text-align:center;">{$MGLANG->T('Nothing to display')}</td>
                                </tr>
                            {/foreach}
                        </tbody> 
                    </table>
                </div>
            </div>
        </div>
    {/foreach}

</div>