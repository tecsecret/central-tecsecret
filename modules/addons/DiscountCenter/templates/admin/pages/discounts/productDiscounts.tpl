{**********************************************************************
* DiscountCenter product developed. (2015-11-18)
* *
*
*  CREATED BY MODULESGARDEN       ->       http://modulesgarden.com
*  CONTACT                        ->       contact@modulesgarden.com
*
*
* This software is furnished under a license and may be used and copied
* only  in  accordance  witd  tde  terms  of such  license and witd tde
* inclusion of tde above copyright notice.  This software  or any otder
* copies tdereof may not be provided or otderwise made available to any
* otder person.  No title to and  ownership of tde  software is  hereby
* transferred.
*
*
**********************************************************************}

{**
* @autdor Paweł Kopeć <pawelk@modulesgarden.com>
*}    
{* PRODUCTS *}
{if $products}
    {include file="products.tpl"}
{/if}

{*Addons*}
{if $addons}
    {include file="addons.tpl"}
{/if}

{*Domains*}
{if $domains}
    {include file="domains.tpl"}
    
{/if}