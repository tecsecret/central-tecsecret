{**********************************************************************
* DiscountCenter product developed. (2016-02-15)
* *
*
*  CREATED BY MODULESGARDEN       ->       http://modulesgarden.com
*  CONTACT                        ->       contact@modulesgarden.com
*
*
* This software is furnished under a license and may be used and copied
* only  in  accordance  with  the  terms  of such  license and with the
* inclusion of the above copyright notice.  This software  or any other
* copies thereof may not be provided or otherwise made available to any
* other person.  No title to and  ownership of the  software is  hereby
* transferred.
*
*
**********************************************************************}

{**
* @author Paweł Kopeć <pawelk@modulesgarden.com>
*}
<div class="col-lg-12" id="mg-setting-content" >

    <legend>{$MGLANG->T('Integration Code')}</legend>
    <div class="panel-group " id="mg-accordion" role="tablist" aria-multiselectable="true">
        <div class="panel panel-primary">
            <div class="panel-heading" role="tab" id="mg-headingOne">
                <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#mg-accordion1" href="#mg-collapseOne1" aria-expanded="true" aria-controls="mg-collapseOne1">
                       {$MGLANG->T('Available Variables')}
                    </a>
                </h4>
            </div>
            <div id="mg-collapseOne1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="mg-headingOne1">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-3">{literal}{$dc_product_price}{/literal}</div>
                        <div class="col-md-9">{$MGLANG->T('Variable displaying full product price.')}</div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">{literal}{$dc_recurring_price}{/literal}</div>
                        <div class="col-md-9">{$MGLANG->T('Variable displaying full product recurring price.')}</div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">{literal}{$dc_product_discount_price}{/literal}</div>
                        <div class="col-md-9">{$MGLANG->T('Variable displaying discount product price.')}</div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">{literal}{$dc_product_setup_price}{/literal}</div>
                        <div class="col-md-9">{$MGLANG->T('Variable displaying full setup price.')}</div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">{literal}{$dc_product_setup_discount_price}{/literal}</div>
                        <div class="col-md-9">{$MGLANG->T('Variable displaying discount setup price.')}</div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">{literal}{$dc_addon_prices[$addon.name]}{/literal}</div>
                        <div class="col-md-9">{$MGLANG->T('Variable displaying full addon price.')}</div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">{literal}{$dc_total_discount_price}{/literal}</div>
                        <div class="col-md-9">{$MGLANG->T('Variable displaying total discount product price.')}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="panel-group " id="mg-accordion" role="tablist" aria-multiselectable="true">
        <div class="panel panel-primary">
            <div class="panel-heading" role="tab" id="mg-headingOne">
                <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#mg-accordion" href="#mg-collapseOne" aria-expanded="true" aria-controls="mg-collapseOne">
                       {$MGLANG->T('Order Form Template: Modern')}
                    </a>
                </h4>
            </div>
            <div id="mg-collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="mg-headingOne">
                <div class="panel-body">
                    
                    {$MGLANG->T('Edit \'ordersummary.tpl\' file located in \'templates/orderforms/modern/ordersummary.tpl\'.')}<br/>
                    {$MGLANG->T('Example:')}  
                    <pre>{$modern}</pre>
                </div>
            </div>
        </div>
    </div>
    <div class="panel-group " id="mg-accordion2" role="tablist" aria-multiselectable="true">
        <div class="panel panel-primary">
            <div class="panel-heading" role="tab" id="mg-headingOne">
                <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#mg-accordion2" href="#mg-collapse2" aria-expanded="true" aria-controls="mg-collapseOne">
                       {$MGLANG->T('Order Form Template: Standard Cart')}
                    </a>
                </h4>
            </div>
            <div id="mg-collapse2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="mg-heading2">
                <div class="panel-body">
                    
                    {$MGLANG->T('Edit \'ordersummary.tpl\' file located in \'templates/orderforms/standard_cart/ordersummary.tpl\'.')}<br/>
                    {$MGLANG->T('Example:')}  
                    <pre>{$standard_cart}</pre>
                </div>
            </div>
        </div>
    </div>  
</div>
{literal}
<style>
    #mg-setting-content .panel-primary a:hover{
        color: white;
    }
</style>
{/literal}
