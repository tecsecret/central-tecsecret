<div class="summaryproduct">{$producttotals.productinfo.groupname} - <b>{$producttotals.productinfo.name}</b></div>
<table class="ordersummarytbl">
<tr><td>{$producttotals.productinfo.name}</td><td class="text-right"> {if $dc_product_price}<span style="text-decoration: line-through; ">{$dc_product_price}</span> {$dc_product_discount_price} {else}{$producttotals.pricing.baseprice}{/if}</td></tr>
{foreach from=$producttotals.configoptions item=configoption}{if $configoption}
<tr><td style="padding-left:10px;">&raquo; {$configoption.name}: {$configoption.optionname}</td><td class="text-right">{$configoption.recurring}{if $configoption.setup} + {$configoption.setup} {$LANG.ordersetupfee}{/if}</td></tr>
{/if}{/foreach}
{foreach from=$producttotals.addons item=addon}
<tr><td>+ {$addon.name}</td><td class="text-right"> {if $dc_addon_prices[$addon.name] && $dc_addon_prices[$addon.name] != $addon.recurring}<span style="text-decoration: line-through; ">{$dc_addon_prices[$addon.name]}</span>{/if} {$addon.recurring}</td></tr>
{/foreach}
</table>
{if $producttotals.pricing.setup || $producttotals.pricing.recurring || $producttotals.pricing.addons}
<div class="summaryproduct"></div>
<table width="100%">
{if $producttotals.pricing.setup}<tr><td>{$LANG.cartsetupfees}:</td><td class="text-right">{if $dc_product_setup_price}<span style="text-decoration: line-through;">{$dc_product_setup_price}</span>{/if} {$producttotals.pricing.setup}</td></tr>{/if}
{foreach from=$producttotals.pricing.recurringexcltax key=cycle item=recurring}
<tr><td>{$cycle}:</td><td class="text-right">{if $dc_recurring_price}{$dc_recurring_price}{else}{$recurring}{/if}</td></tr>
{/foreach}
{if $producttotals.pricing.tax1}<tr><td>{$carttotals.taxname} @ {$carttotals.taxrate}%:</td><td class="text-right">{$producttotals.pricing.tax1}</td></tr>{/if}
{if $producttotals.pricing.tax2}<tr><td>{$carttotals.taxname2} @ {$carttotals.taxrate2}%:</td><td class="text-right">{$producttotals.pricing.tax2}</td></tr>{/if}
</table>
{/if}
<div class="summaryproduct"></div>
<table width="100%">
<tr><td colspan="2" class="text-right">{$LANG.ordertotalduetoday}: <b>{$producttotals.pricing.totaltoday}</b></td></tr>
</table>