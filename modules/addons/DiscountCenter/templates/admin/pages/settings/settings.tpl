{**********************************************************************
* DiscountCenter product developed. (2015-11-17)
* *
*
*  CREATED BY MODULESGARDEN       ->       http://modulesgarden.com
*  CONTACT                        ->       contact@modulesgarden.com
*
*
* This software is furnished under a license and may be used and copied
* only  in  accordance  with  the  terms  of such  license and with the
* inclusion of the above copyright notice.  This software  or any other
* copies thereof may not be provided or otherwise made available to any
* other person.  No title to and  ownership of the  software is  hereby
* transferred.
*
*
**********************************************************************}

{**
* @author Paweł Kopeć <pawelk@modulesgarden.com>
*}
<div class="col-lg-12" id="mg-setting-content" >

    <legend>{$MGLANG->T('header','settings')}</legend>


    <form action="" method="post" id="mg-settings-form" style="min-height:300px;" data-toggle="validator" role="form">
        {$form}

        <div class="well well-sm" style="margin-top:300px;">
            <button class="btn btn-primary btn-inverse" type="submit"  id="pm-button-save"                
                    <i class="glyphicon glyphicon-plus"></i>
                {$MGLANG->T('button','Save Changes')}
            </button>
        </div>
    </form> 
</div>

{literal}
    <script type="text/javascript">
        jQuery(document).ready(function () {

            $('#pm-button-save').on('click', function (e) {
                if($(this).hasClass('disabled'))
                    return;
                    e.preventDefault();
                    JSONParser.request(
                        'saveSettings'
                        , $('#mg-settings-form').MGGetForms()
                        , function (data) {
                        }
                );

            });
        });
    </script>

{/literal}