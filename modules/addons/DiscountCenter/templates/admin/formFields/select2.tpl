{if $enableLabel}
    <label for="{$addIDs}_{$name}" class="col-sm-{$labelcolWidth} control-label">{$MGLANG->T('label')}</label>
{/if}
<div class="col-sm-{$colWidth}">
    <select {if $multiple}multiple="multiple"{/if} {if $id} id="{$id}" {elseif $addIDs}id="{$addIDs}_{$name}"{/if} name="{$nameAttr}{if $multiple}[]{/if}" {if $readonly||$disabled}disabled ="disabled"{/if} {foreach from=$dataAttr key=dataKey item=dataValue}data-{$dataKey}="{$dataValue}"{/foreach}>
        
        
        {foreach from=$options item=option key=opValue}
            <option value="{$opValue}" {if $multiple}{if in_array($opValue,$value)}selected{/if}{else}{if $opValue==$value}selected{/if}{/if}>
                    {$option}
            </option>
        {/foreach}
        
        {foreach from=$optgroups item=optgroup}
            <optgroup label="{$optgroup.label}">
                {foreach from=$optgroup.options item=option key=opValue}
                    <option value="{$opValue}" {if $multiple}{if in_array($opValue,$value)}selected{/if}{else}{if $opValue==$value}selected{/if}{/if}>
                            {$option}
                    </option>
                {/foreach}
            </optgroup>
        {/foreach}
        
    </select>
    {if $readonly}
        <input type="hidden" name="{$nameAttr}" value="{$value}" />
    {/if}
  {if $enableDescription }
    <span class="help-block">{$MGLANG->T('description')}</span>
  {/if}
    <span class="help-block error-block"{if !$error}style="display:none;"{/if}>{$error}</span>
  {literal}
    <script type="text/javascript">
        $(document).ready(function(){
            $("#{/literal}{if $id}{$id}{else}{$addIDs}_{$name}{/if}{literal}").select2().on('select2-selecting', function(e) {
                var $select = $(this);
                if (e.val == '') { // Assume only groups have an empty id
                    e.preventDefault();
                    $select.select2('data', $select.select2('data').concat(e.choice.children));
                    $select.select2('close');
                }
            });
        });
    </script>
    
  {/literal}
</div>


