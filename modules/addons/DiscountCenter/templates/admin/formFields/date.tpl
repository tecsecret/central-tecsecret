{if $enableLabel}
    <label for="{$formName}_{$name}" class="col-sm-{$labelcolWidth} control-label">{$MGLANG->T('label')}</label>
{/if}
<div class="col-sm-{$colWidth}">
    {if !$datetime}
        <input name="{$nameAttr}" data-datepicker type="text" value="{$value}"  class="form-control" {if $id} id="{$id}" {elseif $addIDs}id="{$addIDs}_{$name}"{/if}  placeholder="{if $enablePlaceholder}{$MGLANG->T('placeholder')}{/if}" {foreach from=$dataAttr key=dataKey item=dataValue}data-{$dataKey}="{$dataValue}"{/foreach}>
    {else}
        <div class='input-group date mg-datetime-picker'>
            <input name="{$nameAttr}" type="text" value="{$value}"  class="form-control mg-datetime-picker" {if $id} id="{$id}" {elseif $addIDs}id="{$addIDs}_{$name}"{/if}  placeholder="{if $enablePlaceholder}{$MGLANG->T('placeholder')}{/if}" {foreach from=$dataAttr key=dataKey item=dataValue}data-{$dataKey}="{$dataValue}"{/foreach}>
            <span class="input-group-addon">
                <span class="glyphicon glyphicon-calendar"></span>
            </span>
        </div>
    {/if}
    {if $enableDescription}
        <span class="help-block">{$MGLANG->T('description')}</span>
    {/if}
    <span class="help-block error-block"{if !$error}style="display:none;"{/if}>{$error}</span>
</div>
