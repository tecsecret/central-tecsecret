function isAllFilled() {
    if((jQuery("[name='firstname']").val() && jQuery("[name='lastname']").val() && jQuery("[name='email']").val() && jQuery("[name='address1']").val() && jQuery("[name='city']").val() &&
        jQuery("[name='state']").val() && jQuery("[name='postcode']").val() && jQuery("[name='country']").val() && jQuery("[name='phonenumber']").val()) || jQuery("#existingcust").hasClass("active") || $("#inputCustType").val() == 'existing')
    {
        jQuery("input[name^=contract]").removeAttr("disabled");
        jQuery(".contract-link").show();
    } else {
        jQuery("input[name^=contract]").attr("disabled", true);
        jQuery(".contract-link").hide();
    }
}

function rewriteValuesToForm() {
    jQuery("#view-contract-pdf").find('input:not([id])').remove();

    jQuery('form[name="orderfrm"]').find("input[type='text'], input[type='email'], input[type='tel'], input[type='checkbox'], select").each(function() {
        jQuery("#view-contract-pdf").append('<input type="hidden" name="' + jQuery(this).attr("name") + '" value="' + jQuery(this).val() + '">');
    });
}

function mgRcHideContractSectionIfDoesNotExist()
{
    if(jQuery("#contract-exist").length == 0) {
        jQuery("#recurring-biling-contracts").hide();
    }
}

function mgRcAppendFieldsOnCheckout()
{
    jQuery("body").append('\n\
        <form action="recurringbilling.php" method="post" target="_blank" id="view-contract-pdf">\n\
            <input type="hidden" name="pdf_pid" value="" id="pdf-pid">\n\
            <input type="hidden" name="pdf_bc" value="" id="pdf-bc">\n\
            <input type="hidden" name="pdf_currency" value="" id="pdf-currency">\n\
            <input type="hidden" name="pdf_length" value="" id="pdf-length">\n\
        </form>\n\
    ');
}

function mgRcDelegateAllFieldsFunction()
{
    isAllFilled();

    jQuery("input").keyup(isAllFilled);
    jQuery("input").focusout(isAllFilled);
    jQuery(document).delegate("select", "change", isAllFilled);
}

function mgRcLoadIcheckOnCheckout(){
    jQuery("[rc-contract]").iCheck({
        checkboxClass: "icheckbox_square-blue",
        radioClass: "iradio_square-blue",
        increaseArea: "20%" // optional
    });
};

jQuery(document).ready(function() {

    mgRcLoadIcheckOnCheckout();
    mgRcHideContractSectionIfDoesNotExist();
    mgRcAppendFieldsOnCheckout();
    mgRcDelegateAllFieldsFunction();
});