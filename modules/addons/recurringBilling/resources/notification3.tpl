Hello {$client_first_name},
<br /><br />
This is the third notification about contract going to be ended for product {$productgroup} - {$product} ({$domain}).
<br /><br />
You have signed this contract {$datestarted} and it ends {$dateend}.
<br /><br />
Direct link to your service assigned to the contract: {$servicelink}