Hello {$client_first_name},
<br /><br />
This is notification about contract going to be ended for product {$productgroup} - {$product} ({$domain}).
<br /><br />
You have signed this contract {$datestarted} and it ends {$dateend}.
<br /><br />
After end of the contract your service will be terminated.
