Hello {$client_first_name},
<br /><br />
This is notification about contract going to be ended for product {$productgroup} - {$product} ({$domain}).
<br /><br />
You have signed this contract {$datestarted} and it ends {$dateend}.
<br /><br />
You can renew this contract by clicking this link: {$renewallink}