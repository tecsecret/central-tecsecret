//ArVal v0.0.1 - kij wie czy będzie rozwijana

jQuery(document).ready(function() {
    jQuery('input[arval-button="1"], a[arval-button="1"], button[arval-button="1"]').click(function(){
        
        var is_error = false;
        
        jQuery(".arval-label").remove();
        
        jQuery('[arval="not-empty"]').each(function() {
            if(jQuery(this).val() === '') {
                jQuery(this).after('<p class="arval-label" style="color: red; font-weight: bold;">This field cannot be empty</p>');
                is_error = true;
            }
        });
        
        jQuery('[arval="int"]').each(function() {
            if(!jQuery.isNumeric(jQuery(this).val()) && jQuery(this).val() !== '') {
                jQuery(this).after('<p class="arval-label" style="color: red; font-weight: bold;">Please provide valid integer value</p>');
                is_error = true;
            }
        });

        jQuery('[arval-error="1"]').each(function(){
            is_error = true;
        });
        
        if(!is_error) {
            jQuery(this).closest('form').submit();
        }
    });
});