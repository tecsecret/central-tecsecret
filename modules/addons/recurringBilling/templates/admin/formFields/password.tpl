{if $enableLabel}
    <label for="{$formName}_{$name}" class="col-sm-2 control-label">{$MGLANG->T('label')}</label>
{/if}
<div class="col-sm-{$colWidth}">
    <input name="{$nameAttr}" {if $addIDs}id="{$addIDs}_{$name}"{/if} type="password" value="{$value}" class="form-control" {foreach from=$dataAttr key=dataKey item=dataValue}data-{$dataKey}="{$dataValue}"{/foreach} {if $addIDs}id="{$addIDs}_{$name}"{/if} placeholder="{if $enablePlaceholder}{$MGLANG->T('placeholder')}{/if}">
    {if $enableDescription }
      <span class="help-block">{$MGLANG->T('description')}</span>
    {/if}
    <span class="help-block error-block"{if !$error}style="display:none;"{/if}>{$error}</span>
</div>
