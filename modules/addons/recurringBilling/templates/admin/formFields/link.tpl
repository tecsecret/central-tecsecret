    <div class="text-center">
        <a name="{$nameAttr}" value="{$name}" class="btn btn-{$color}" {foreach from=$attr key=dataKey item=dataValue}  {$dataKey}="{$dataValue}" {/foreach} {foreach from=$dataAttr key=dataKey item=dataValue}data-{$dataKey}="{$dataValue}"{/foreach}>{if $icon}<i class="glyphicon glyphicon-{$icon}"></i> {/if}{if $enableContent}{$nameAttr}{/if}</a>
    </div>