<tr>
    <td>{$id}</td>
    <td>{$name}</td>
    <td>{$contentname}</td>
    <td>{$product}</td>
    <td><span class="label {if $allowcancellation}label-success{else}label-default{/if}">{if $allowcancellation}{$MGLANG->T('label', 'allowed')}{else}{$MGLANG->T('label', 'prohibited')}{/if}</span></td>
    <td>
        <a bstt="1" title="{$MGLANG->T('tooltip','edit')}" href="addonmodules.php?module=recurringBilling&mg-page=contractTemplates&mg-action=edit&id={$id}" class="btn btn-sm btn-warning btn-inverse btn-icon-only"><i class="glyphicon glyphicon-pencil"></i></a>
        <button bstt="1" title="{$MGLANG->T('tooltip','remove')}" type="button" data-modal-id="MGDeleteItem" data-modal-target="{$id}" class="btn btn-sm btn-danger btn-inverse btn-icon-only deleteItem" name="delete" value="{$id}"><i class="glyphicon glyphicon-remove"></i></button>
    </td>
</tr>