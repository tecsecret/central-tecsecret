<style>
    {literal}
        .erroreq {
            border-color: #A94442;
            border-style: solid;
            border-width: 1px;
            border-radius: 4px;
        }
    {/literal}
</style>
<!-- BEGIN PAGE BREADCRUMB -->
    <ul class="breadcrumb">
        <li>
            <a href="addonmodules.php?module=recurringBilling"><i class="fa fa-home"></i></a>
        </li>
        <li>
            <a href="addonmodules.php?module=recurringBilling&mg-page=contractTemplates">{$MGLANG->T('breadcrumb','contractTemplates')}</a>
        </li>
        <li>
            <a href="addonmodules.php?module=recurringBilling&mg-page=contractTemplates&mg-action=edit&id={$id}">{$MGLANG->T('breadcrumb','editContractTemplate')}</a>
        </li>
    </ul>
<form class="form-horizontal" action="addonmodules.php?module=recurringBilling&mg-page=contractTemplates" method="post">
    <input type="hidden" name="id" value="{$id}">
    <input type="hidden" name="mg-action" value="saveTemplate">
    <div class="col-md-6">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">{$MGLANG->T('title','template')}</h3>
            </div>
            <div class="panel-body">
                <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">{$MGLANG->T('label','name')}</label>
                    <div class="col-sm-10">
                      <input arval="not-empty" class="form-control" type="text" id="name" name="name" value="{$data.name}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="contentname" class="col-sm-2 control-label">{$MGLANG->T('label','contentname')}</label>
                    <div class="col-sm-10">
                        <select class="form-control" id="contentname" name="contentname">{$content_dropdown}</select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="product" class="col-sm-2 control-label">{$MGLANG->T('label','product')}</label>
                    <div class="col-sm-10">
                        <select {if $id > 0}disabled{/if} class="form-control" id="product" name="product">{if $product_dropdown}{$product_dropdown}{else}<option>{$MGLANG->T('option','no_results')}</option>{/if}</select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="promotype" class="col-sm-2 control-label">{$MGLANG->T('label','promotype')}</label>
                    <div class="col-sm-10">
                        <select class="form-control" id="promotype" name="promotype">
                            <option value="fixed" {if $settings.promotype eq "fixed"}selected{/if}>{$MGLANG->T('option','fixed')}</option>
                            <option value="percentage" {if $settings.promotype eq "percentage"}selected{/if}>{$MGLANG->T('option','percentage')}</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="signtype" class="col-sm-2 control-label">{$MGLANG->T('label','signtype')}</label>
                    <div class="col-sm-10">
                        <select class="form-control" id="signtype" name="signtype">     
                            {foreach from=$type_dropdown key=value_type item=type}
                                <option value="{$value_type}"{if $value_type == $settings.signtype}selected{/if}>{$type}</option> 
                            {/foreach}
                        </select> 
                    </div>
                </div>      
                <div class="form-group">
                    <label for="penaltytype" class="col-sm-2 control-label">{$MGLANG->T('label','penaltytype')}</label>
                    <div class="col-sm-10">
                        <select class="form-control" id="penaltytype" name="penaltytype">
                            <option value="fixed" {if $settings.penaltytype eq "fixed"}selected{/if}>{$MGLANG->T('option','fixed')}</option>
                            <option value="percentage" {if $settings.penaltytype eq "percentage"}selected{/if}>{$MGLANG->T('option','percentage')}</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="contractrenewal" class="col-sm-2 control-label">{$MGLANG->T('label','contractrenewal')}</label>
                    <div class="col-sm-10">
                        <select class="form-control" id="penaltytype" name="contractrenewal">
                            <option value="none" {if $settings.contractrenewal eq "none"}selected{/if}>{$MGLANG->T('option','none')}</option>
                            <option value="cancel" {if $settings.contractrenewal eq "cancel"}selected{/if}>{$MGLANG->T('option','cancel')}</option>
                            <option value="renew" {if $settings.contractrenewal eq "renew"}selected{/if}>{$MGLANG->T('option','renew')}</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="allowCancellation" class="col-sm-2 control-label">{$MGLANG->T('label','allowcancellation')}</label>
                    <div class="col-sm-10">
                        <div class="onoffswitch">
                            <input type="checkbox" name="allowCancellation" class="onoffswitch-checkbox" id="allowCancellation" {if $settings.allowcancellation}checked{/if}>
                            <label class="onoffswitch-label" for="allowCancellation">
                                <span class="onoffswitch-inner" data-before="{$MGLANG->T('label','allowed')}" data-after="{$MGLANG->T('label','prohibited')}"></span>
                                <span class="onoffswitch-switch"></span>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
                        
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">{$MGLANG->T('title','descriptions')}</h3>
            </div>
            <div class="panel-body">
                <p><strong>{$MGLANG->T('item','length')}</strong> - {$MGLANG->T('description','length')}</p>
                <p><strong>{$MGLANG->T('item','promoAmount')}</strong> - {$MGLANG->T('description','promoAmount')}</p>
                <p><strong>{$MGLANG->T('item','penaltyAmount')}</strong> - {$MGLANG->T('description','penaltyAmount')}</p>
                <p><strong>{$MGLANG->T('item','daysToCancel')}</strong> - {$MGLANG->T('description','daysToCancel')}</p>
                <p><strong>{$MGLANG->T('item','period')}</strong> - {$MGLANG->T('description','period')}</p>
                <p><strong>{$MGLANG->T('item','total')}</strong> - {$MGLANG->T('description','total')}</p>
                <p><strong>{$MGLANG->T('item','amount')}</strong> - {$MGLANG->T('description','amount')}</p>
                <p><strong>{$MGLANG->T('item','totalPenalty')}</strong> - {$MGLANG->T('description','totalPenalty')}</p>
                <p><strong>{$MGLANG->T('item','noticeTime')}</strong> - {$MGLANG->T('description','noticeTime')}</p>
                <br/>
                <p><strong>{$MGLANG->T('item','renewalTypes')}</strong>
                <p><strong>{$MGLANG->T('option','renew')}</strong> - {$MGLANG->T('description','renew')}</p>
                <p><strong>{$MGLANG->T('option','none')}</strong> - {$MGLANG->T('description','none')}</p>
                <p><strong>{$MGLANG->T('option','cancel')}</strong> - {$MGLANG->T('description','cancel')}</p>
            </div>
        </div>
    </div>

    {if $showDetails}
        <div class="col-md-6">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">{$MGLANG->T('title','cycles')}</h3>
                </div>
                <div class="panel-body">
                    <ul class="nav nav-tabs" id="config-tab">
                        {foreach from=$currencies item=currency}
                            <li role="presentation" show="currency-tab{$currency.id}"><a href="#">{$currency.code}</a></li>
                        {/foreach}
                    </ul>
                        {foreach from=$currencies item="currency"}
                            <div id="currency-tab{$currency.id}">
                                {assign var="curr" value=$currency.id}
                                {foreach from=$billingcycles.$curr.cycles key="billingcycle" item="cycleamount"}
                                    <div class="billingcycle-content" cycle="{$billingcycle}" currencyid="{$currency.id}" amount="{$cycleamount}">
                                        <div class="panel panel-default rozwijable" is-shown="false" style="cursor: pointer;">
                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="col-xs-4">
                                                        <strong>{$MGLANG->T('cycle', $billingcycle)}</strong><br>
                                                        {$MGLANG->T('label', 'amount')} {$currency.prefix}{$cycleamount}{$currency.suffix}
                                                    </div>
                                                    <div class="col-xs-6">
                                                        <div><span class="contract-amount"></span> {$MGLANG->T('label', 'contract')}<br></div>
                                                        <div><input {if $settings.$billingcycle.$curr eq "on"}checked{/if} type="checkbox" name="nocontract[{$currency.id}][{$billingcycle}]"> <span>{$MGLANG->T('checkbox', 'allowSkipContract')}</span></div>
                                                    </div>
                                                    <div class="col-xs-2" style="margin-top: 7px;">
                                                        <a bstt="1" title="{$MGLANG->T('tooltip', 'add')}" class="btn btn-sm btn-success btn-icon-only btn-inverse pull-right add-new-item"><i class="glyphicon glyphicon-plus"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="bc-data">
                                            {foreach from=$settings.lengths item=length}
                                                {if $length.currencyid eq $currency.id and $length.billingcycle eq $billingcycle}
                                                    {include file="ex-item-content.tpl"}
                                                {/if}
                                            {/foreach}
                                        </div>
                                    </div>
                                {/foreach}
                            </div>
                        {/foreach}
                </div>
            </div>
        </div>
    {/if}    
    <div class="col-md-12">
        <a arval-button="1" style="margin-right: 10px;" class="btn btn-success btn-inverse">{$MGLANG->T('button','save')}</a>
        <a href="addonmodules.php?module=recurringBilling&mg-page=contractTemplates" class="btn btn-default btn-inverse">{$MGLANG->T('button','back')}</a>
    </div>
</form>

{foreach from=$length_dropdown key=cyclekey item=onecycle}
    <div style="display: none" id="length-dropdown-{$cyclekey}">{$onecycle}</div>
{/foreach}

{include file="item-content.tpl"}
    
<script type="text/javascript">
    {literal}    
        var new_item_counter = 0;
        var currencies = {{/literal}{foreach from=$currencies item=item}{$item.id} : {ldelim}"prefix" : "{$item.prefix}", "suffix" : "{$item.suffix}", "rate" : "{$item.rate}"{rdelim},{/foreach}{literal}};
        
        function roundVal(val) {
            return Math.round(val * 100) / 100;
        }
        
        function refreshCurrencyBox() {
            jQuery("div[id^='currency-tab']").hide();
            var item = jQuery("#config-tab li.active").first().attr('show');
            jQuery("#" + item).show();
        }
        
        function parseContractHTML(html, currencyid, cycle, counter) {
            var matches = html.match(/:[a-z]+:/g);
            jQuery.each(matches, function(index, value){
                // wiem, że tak się nie robi :)
                if(value === ":lengthdropdown:") {
                    var dropdown = jQuery("#length-dropdown-" + cycle).html();
                    html = html.replace(value, dropdown);
                } else if(value === ":prefix:" || value === ":suffix:"){
                    var dotless = value.replace(/:/g, '');
                    html = html.replace(value, currencies[currencyid][dotless]);
                } else if(value === ":cid:") {
                    html = html.replace(value, currencyid);
                } else if(value === ":bc:") {
                    html = html.replace(value, cycle);
                } else {
                    var dotless = value.replace(/:/g, '');
                    html = html.replace(value ,'contracts[' + currencyid + '][' + cycle + '][new][' + counter + '][' + dotless + ']');
                }
            });
            
            return html;
        }
        
        function countContracts() {
            jQuery('.billingcycle-content').each(function() {
                var amount = jQuery(this).find(".blank-contract-container").length;
                jQuery(this).find(".contract-amount").first().html(amount);
            });
        }
        
        function validateAndCalculate() {
            var container = jQuery(this).closest('.blank-contract-container');

            if(!jQuery(this).val().match(/^\d+\.$/)) {
                var value = jQuery(this).val();
                jQuery(this).val(parseFloat(value));

                if(jQuery(this).val() === 'NaN') {
                    jQuery(this).val('');
                }
            }

            calculateContractValues(container);

            var price = container.find('[contract-val="totalprice"]').html();
            if(price < 0 && jQuery(this).attr("contract-val") === "promo") {
                jQuery(this).closest(".form-group").addClass('has-error');
                jQuery(this).closest(".form-group").attr('arval-error', '1');
                container.find('[contract-val="totalprice"]').closest(".form-group").addClass("erroreq");
            } else {
                jQuery(this).closest(".form-group").removeClass('has-error');
                jQuery(this).closest(".form-group").removeAttr('arval-error');
                container.find('[contract-val="totalprice"]').closest(".form-group").removeClass("erroreq");
            }

            calculateContractValues(container);
        }
            
        function calculateContractValues(obj) {
            var promotype   = jQuery('[name="promotype"]').val();
            var penaltytype = jQuery('[name="penaltytype"]').val();

            var amount      = obj.closest('.billingcycle-content').attr("amount");
            
            if(amount === undefined) {
                return false;
            }
            
            var length      = obj.find('[contract-val="length"]').val();
            var promo       = obj.find('[contract-val="promo"]').val();
            var penalty     = obj.find('[contract-val="penalty"]').val();

            if(promotype === "fixed") {
                var totalprice      = (amount - promo) * length;
                var promoamount     = amount - promo;
            } else {
                var totalprice      = (amount * (1 - promo / 100)) * length;
                var promoamount     = amount * (1 - promo / 100);
            }
            
            if(penaltytype === "fixed") {
                var totalpenalty    = penalty;
            } else {
                var totalpenalty    = totalprice * (penalty / 100);
            }

            obj.find('[contract-val="totalprice"]').html(roundVal(totalprice));
            obj.find('[contract-val="promoamount"]').html(roundVal(promoamount));
            obj.find('[contract-val="totalpenalty"]').html(roundVal(totalpenalty));
        }
        
        function calculateAllContracts() {
            jQuery('.blank-contract-container').each(function(){
                calculateContractValues(jQuery(this));
            });
        }
        
        jQuery(document).ready(function(){
            var curr_amount = 0;
            
            jQuery('[show^="currency-tab"]').each(function() {
                curr_amount++;
            });
            
            if(curr_amount < 2) {
                jQuery(".copyContract").remove();
            }
            
            jQuery('[bstt="1"]').bstooltip();
            
            jQuery('select[item-selected]').each(function(){
                jQuery(this).val(jQuery(this).attr('item-selected'));
            });
            jQuery("#config-tab").find("li:first").addClass("active");
            refreshCurrencyBox();
            countContracts();
            calculateAllContracts();
            
            jQuery('#config-tab li').click(function(e) {
                e.preventDefault();
                jQuery("#config-tab li.active").removeClass('active');
                jQuery(this).addClass('active');
                refreshCurrencyBox();
            });
            
            jQuery('.add-new-item').click(function(){
                var content = jQuery("#blank-contract").html();
                var currencyid = jQuery(this).closest(".billingcycle-content").attr("currencyid");
                var billingcycle = jQuery(this).closest(".billingcycle-content").attr("cycle");
                
                jQuery(this).closest('.billingcycle-content').find('.bc-data').first().append(parseContractHTML(content, currencyid, billingcycle, new_item_counter));
                new_item_counter++;
                jQuery('[bstt="1"]').bstooltip();
                return false;
            });
            
            jQuery(document).delegate('#penaltytype, #promotype', 'change', function(){
                jQuery(".calculateValuesTextBox").each(validateAndCalculate);
                calculateAllContracts();
            });
            
            jQuery(document).delegate('.calculateValuesTextBox', 'keyup', validateAndCalculate);
            
            jQuery(document).delegate('.calculateValuesDropdown', 'change', function(){
                var container = jQuery(this).closest('.blank-contract-container');
                calculateContractValues(container);
            });
            
            jQuery(document).delegate('.deleteItem', 'click', function(){
                jQuery(this).bstooltip('destroy');
                jQuery(this).closest('.blank-contract-container').remove();
            });
            
            jQuery(".prepareDelete").click(function() {
                var delete_id = jQuery(this).attr("contract-length-id");
                jQuery(this).closest("form").prepend('<input type="hidden" name="deletes[' + delete_id + ']">');
                jQuery(this).bstooltip('destroy');
                jQuery(this).closest('.blank-contract-container').remove();
            });
            
            jQuery(".rozwijable").click(function(){
                var is_shown = jQuery(this).attr('is-shown');
                if(is_shown === "true") {
                    jQuery(this).closest('.billingcycle-content').find('*[existing="true"]').slideToggle(100);
                    jQuery(this).attr('is-shown', 'false');
                } else {
                    jQuery(this).closest('.billingcycle-content').find('*[existing="true"]').slideToggle(100);
                    jQuery(this).attr('is-shown', 'true');
                }
            });
            
            jQuery('input[type="checkbox"]').click(function(event){
                event.stopPropagation();
            });
            
            jQuery(document).delegate(".copyContract", "click", function() {
                var content     = jQuery("#blank-contract").html();
                var cycle       = jQuery(this).closest('.billingcycle-content').attr('cycle');
                var global_curr = jQuery(this).closest('.billingcycle-content').attr('currencyid');
                var global_length = jQuery(this).closest('.blank-contract-container').attr("length");
                var item        = jQuery(this);

                jQuery("div[cycle='" + cycle + "']").each(function(){
                    var currencyid  = jQuery(this).closest('.billingcycle-content').attr('currencyid');

                    if(jQuery('[length="' + global_length + '"][bc="' + cycle + '"][currency="' + currencyid + '"]').length) {
                        var container = item.closest(".blank-contract-container");

                        var c_length    = container.find('[contract-val="length"]').val();
                        var c_promo     = container.find('[contract-val="promo"]').val();
                        var c_penalty   = container.find('[contract-val="penalty"]').val();
                        var c_cancel    = container.find('[contract-val="cancel"]').val();
                        var c_period    = container.find('[contract-val="period"]').val();
                        var c_notice    = container.find('[contract-val="notice"]').val();

                        var container_to = jQuery('[length="' + global_length + '"][bc="' + cycle + '"][currency="' + currencyid + '"]').closest(".blank-contract-container");
                        
                        if(jQuery('select[name="promotype"]').val() === "fixed") {
                            var t_promo = (currencies[currencyid]["rate"] / currencies[global_curr]["rate"]) * c_promo;
                        } else {
                            var t_promo = c_promo;
                        }

                        if(jQuery('select[name="penaltytype"]').val() === "fixed") {
                            var t_penalty = (currencies[currencyid]["rate"] / currencies[global_curr]["rate"]) * c_penalty;
                        } else {
                            var t_penalty = c_penalty;
                        }
                        container_to.find('[contract-val="length"]').val(c_length);
                        container_to.find('[contract-val="promo"]').val(roundVal(t_promo));
                        container_to.find('[contract-val="penalty"]').val(roundVal(t_penalty));
                        container_to.find('[contract-val="cancel"]').val(c_cancel);
                        container_to.find('[contract-val="period"]').val(c_period);
                        container_to.find('[contract-val="notice"]').val(c_notice);
                        
                        calculateContractValues(container_to);
                    } else {
                        var dom         = parseContractHTML(content, currencyid, cycle, new_item_counter);

                        var length      = item.closest('.blank-contract-container').find('select[name$="[length]"]').first().val();
                        var promo       = item.closest('.blank-contract-container').find('input[name$="[promo]"]').first().val();
                        var penalty     = item.closest('.blank-contract-container').find('input[name$="[penalty]"]').first().val();
                        var period      = item.closest('.blank-contract-container').find('input[name$="[period]"]').first().val();
                        var cancel      = item.closest('.blank-contract-container').find('input[name$="[cancel]"]').first().val();
                        var notice      = item.closest('.blank-contract-container').find('input[name$="[notice]"]').first().val();

                        var obj         = jQuery(this).find('.bc-data').first().append(dom).children().last();

                        if(jQuery('select[name="promotype"]').val() === "fixed") {
                            var t_promo = (currencies[currencyid]["rate"] / currencies[global_curr]["rate"]) * promo;
                        } else {
                            var t_promo = promo;
                        }

                        if(jQuery('select[name="penaltytype"]').val() === "fixed") {
                            var t_penalty = (currencies[currencyid]["rate"] / currencies[global_curr]["rate"]) * penalty;
                        } else {
                            var t_penalty = penalty;
                        }

                        obj.find('select[name$="[length]"]').val(length);
                        obj.find('input[name$="[promo]"]').val(roundVal(t_promo));
                        obj.find('input[name$="[penalty]"]').val(roundVal(t_penalty));
                        obj.find('input[name$="[period]"]').val(period);
                        obj.find('input[name$="[cancel]"]').val(cancel);
                        obj.find('input[name$="[notice]"]').val(notice);
                        
                        obj.attr("length", length);
                        obj.attr("bc", cycle);
                        obj.attr("currency", currencyid);

                        calculateContractValues(obj);
                        new_item_counter++;

                        jQuery(this).closest('.bc-data').first().find('.blank-contract-container').last().remove();
                    }
                });  
                
                jQuery('.blank-contract-container').each(function(){
                    calculateContractValues(jQuery(this));
                });
            });
            
            jQuery(document).delegate('[contract-val="length"]', "change", function() {
                jQuery(this).closest(".blank-contract-container").attr("length", jQuery(this).val());
            });
            
            jQuery(".rozwijable").mouseenter(function(){
                jQuery(this).css("background-color", "#EEE");
            });
            
            jQuery(".rozwijable").mouseleave(function(){
                jQuery(this).css("background-color", "#FFF");
            });

            jQuery("#allowCancellation").click(function(){
                if(jQuery("#allowCancellation").is(':checked')) {
                    jQuery(".daysToCancelDiv").show();
                }else{
                    jQuery(".daysToCancelDiv").hide();
                }
            });
            
            if(jQuery("#allowCancellation").is(':checked')) {
                jQuery(".daysToCancelDiv").show();
            }else{
                jQuery(".daysToCancelDiv").hide();
            }
            
        });
    {/literal}
</script>