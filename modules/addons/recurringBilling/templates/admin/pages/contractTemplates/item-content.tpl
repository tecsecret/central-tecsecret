<div id="blank-contract" style="display: none;">
    <div length="1" bc=":bc:" currency=":cid:" class="blank-contract-container row well" style="margin: 0px; padding-top: 8px; padding-bottom: 8px; margin-bottom: 20px;">
        <div class="col-sm-12">
            <a bstt="1" title="{$MGLANG->T('tooltip', 'delete')}" class="btn btn-sm btn-danger btn-icon-only btn-inverse deleteItem pull-right" style="margin-left: 5px; margin-right: -18px;margin-top: 10px;"><i class="glyphicon glyphicon-remove"></i></a>
            <a bstt="1" title="{$MGLANG->T("button","otherCurrencies")}" class="btn btn-sm btn-success btn-inverse btn-icon-only pull-right copyContract" style="margin-top: 10px;"><i class="glyphicon glyphicon-share"></i></a>
        </div>
        <div class="col-sm-12">
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="col-sm-6 control-label">{$MGLANG->T("label","length")}</label>
                    <div class="col-sm-6">
                        <select class="form-control calculateValuesDropdown" contract-val="length" name=":length:">:lengthdropdown:</select>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="col-sm-6 control-label">{$MGLANG->T("label","total")}</label>
                    <div class="col-sm-6" style="margin-top: 7px;">
                        <p style="font-weight: bold; color: #1A4D80;">:prefix:<span style="font-weight: bold; color: #1A4D80;" contract-val="totalprice"></span>:suffix:</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="col-sm-6 control-label">{$MGLANG->T("label","promo")}</label>
                    <div class="col-sm-6">
                      <input class="form-control calculateValuesTextBox" type="text" contract-val="promo" name=":promo:" value="">
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="col-sm-6 control-label">{$MGLANG->T("label","promoAmount")}</label>
                    <div class="col-sm-6" style="margin-top: 7px;">
                        <p style="font-weight: bold; color: #2D8B2B;">:prefix:<span style="font-weight: bold; color: #2D8B2B;" contract-val="promoamount"></span>:suffix:</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="col-sm-6 control-label">{$MGLANG->T("label","penalty")}</label>
                    <div class="col-sm-6">
                      <input class="form-control calculateValuesTextBox" type="text" id="name" contract-val="penalty" name=":penalty:" value="">
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="col-sm-6 control-label">{$MGLANG->T("label","totalPenalty")}</label>
                    <div class="col-sm-6" style="margin-top: 7px;">
                        <p style="font-weight: bold; color: #D9534F;">:prefix:<span style="font-weight: bold; color: #D9534F;" contract-val="totalpenalty"></span>:suffix:</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12 daysToCancelDiv">
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="col-sm-6 control-label">{$MGLANG->T("label","daysToCancel")}</label>
                    <div class="col-sm-6">
                        <input class="form-control" arval="int" type="text" contract-val="cancel" name=":cancel:" value="">
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="col-sm-6 control-label">{$MGLANG->T("label","trialPeriod")}</label>
                    <div class="col-sm-6">
                        <input class="form-control" arval="int" type="text" contract-val="period" name=":period:" value="">
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="col-sm-6 control-label">{$MGLANG->T("label","noticeTime")}</label>
                    <div class="col-sm-6">
                        <input class="form-control" arval="int" type="text" contract-val="notice" name=":notice:" value="">
                    </div>
                </div>
            </div>
        </div>
        <hr>
    </div>
</div>
    