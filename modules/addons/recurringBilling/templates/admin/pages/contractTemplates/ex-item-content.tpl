{assign var="cid" value=$length.currencyid}
<div length="{$length.length}" bc="{$billingcycle}" currency="{$cid}" class="blank-contract-container row well" style="margin: 0px; padding-top: 8px; padding-bottom: 8px; margin-bottom: 20px; display: none; background-color: #FFF;" existing="true">
    <div class="col-sm-12">
        <a bstt="1" title="{$MGLANG->T('tooltip', 'delete')}" class="btn btn-sm btn-danger btn-inverse btn-icon-only prepareDelete pull-right" contract-length-id="{$length.id}" style="margin-left: 5px; margin-right: -18px; margin-top: 10px;"><i class="glyphicon glyphicon-remove"></i></a>
        <a bstt="1" title="{$MGLANG->T("button","otherCurrencies")}" class="btn btn-sm btn-success btn-icon-only btn-inverse pull-right copyContract" style="margin-top: 10px;"><i class="glyphicon glyphicon-share"></i></a>
    </div>
    <div class="col-sm-12">
        <div class="col-sm-6">
            <div class="form-group">
                <label class="col-sm-6 control-label">{$MGLANG->T("label","length")}</label>
                <div class="col-sm-6">
                    <select class="form-control calculateValuesDropdown" item-selected="{$length.length}" contract-val="length" name="contracts[{$currency.id}][{$billingcycle}][existing][{$length.id}][length]">{$length_dropdown.$billingcycle}</select>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label class="col-sm-6 control-label">{$MGLANG->T("label","total")}</label>
                <div class="col-sm-6" style="margin-top: 7px;">
                    <p style="font-weight: bold; color: #1A4D80;">{$currencies.$cid.prefix}<span style="font-weight: bold; color: #1A4D80;" contract-val="totalprice"></span>{$currencies.$cid.suffix}</p>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-12">
        <div class="col-sm-6">
            <div class="form-group">
                <label class="col-sm-6 control-label">{$MGLANG->T("label","promo")}</label>
                <div class="col-sm-6">
                  <input class="form-control calculateValuesTextBox" type="text" contract-val="promo" name="contracts[{$currency.id}][{$billingcycle}][existing][{$length.id}][promo]" value="{$length.promo}">
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label class="col-sm-6 control-label">{$MGLANG->T("label","promoAmount")}</label>
                <div class="col-sm-6" style="margin-top: 7px;">
                    <p style="font-weight: bold; color: #2D8B2B;">{$currencies.$cid.prefix}<span style="font-weight: bold; color: #2D8B2B;" contract-val="promoamount"></span>{$currencies.$cid.suffix}</p>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-12">
        <div class="col-sm-6">
            <div class="form-group">
                <label class="col-sm-6 control-label">{$MGLANG->T("label","penalty")}</label>
                <div class="col-sm-6">
                  <input class="form-control calculateValuesTextBox" type="text" id="name" contract-val="penalty" name="contracts[{$currency.id}][{$billingcycle}][existing][{$length.id}][penalty]" value="{$length.penalty}">
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label class="col-sm-6 control-label">{$MGLANG->T("label","totalPenalty")}</label>
                <div class="col-sm-6" style="margin-top: 7px;">
                    <p style="font-weight: bold; color: #D9534F;">{$currencies.$cid.prefix}<span style="font-weight: bold; color: #D9534F;" contract-val="totalpenalty"></span>{$currencies.$cid.suffix}</p>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-12 daysToCancelDiv">
        <div class="col-sm-6">
            <div class="form-group">
                <label class="col-sm-6 control-label">{$MGLANG->T("label","daysToCancel")}</label>
                <div class="col-sm-6">
                     <input class="form-control calculateValuesTextBox" type="text" contract-val="cancel" name="contracts[{$currency.id}][{$billingcycle}][existing][{$length.id}][cancel]" value="{$length.cancel}">
                </div>
            </div>
        </div>
    </div>            
    <div class="col-sm-12">
        <div class="col-sm-6">
            <div class="form-group">
                <label class="col-sm-6 control-label">{$MGLANG->T("label","trialPeriod")}</label>
                <div class="col-sm-6">
                  <input class="form-control" arval="int" type="text" contract-val="period" name="contracts[{$currency.id}][{$billingcycle}][existing][{$length.id}][period]" value="{$length.period}">
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-12">
        <div class="col-sm-6">
            <div class="form-group">
                <label class="col-sm-6 control-label">{$MGLANG->T("label","noticeTime")}</label>
                <div class="col-sm-6">
                  <input class="form-control" arval="int" type="text" contract-val="notice" name="contracts[{$currency.id}][{$billingcycle}][existing][{$length.id}][notice]" value="{$length.notice}">
                </div>
            </div>
        </div>
    </div>
    <hr>
</div>
