    {literal}
        <style>
            .expandable {
                font-weight: bold;
                font-size: 16px;
                cursor: pointer;
            }
            .fields-group {
                background: #EEE!important;
            }
            .field-label {
                font-size: 12px;
                font-weight: bold;
                width: 30%; 
                display: table-cell;
            }
            .field-mergefield {
                font-size: 12px;
                width: 65%; 
                display: table-cell;
            }
            .field-row {
                width: 100%;
                display: table-row;
                cursor: pointer;
            }
            .dropdown-menu, .nav-tabs li{
                text-transform:capitalize;
            }
        </style>
    {/literal}

    <!-- BEGIN PAGE BREADCRUMB -->
    <ul class="breadcrumb">
        <li>
            <a href="addonmodules.php?module=recurringBilling"><i class="fa fa-home"></i></a>
        </li>
        <li>
            <a href="addonmodules.php?module=recurringBilling&mg-page=contractContent">{$MGLANG->T('breadcrumb','contractContent')}</a>
        </li>
        <li>
            <a href="addonmodules.php?module=recurringBilling&mg-page=contractContent&mg-action=edit&id={$id}">{$MGLANG->T('breadcrumb','editContent')}</a>
        </li>
    </ul>
        
    <div class="col-lg-12" id="MGItems">
        <div class="alert alert-info">{$MGLANG->T('description', 'tutorial')}</div>
        <form id="contentForm" method="post" action="addonmodules.php?module=recurringBilling&mg-page=contractContent" class="form">
            <input type="hidden" name="mg-action" value="saveContent">
            <input type="hidden" name="id" value="{$id}">
            <div class="row">
                <div class="col-lg-12">
                    <h4>{$MGLANG->T('label','name')}</h4>
                    <input type="text" name="content_name" arval="not-empty" class="form-control" value="{$content_name}" style="width: 20%;">
                </div>
            </div><br>
            <div class="row">
                <div class="col-lg-8">
                    <h4>{$MGLANG->T('label','content')}</h4>
                    <ul role="tablist" class="nav nav-tabs">
                        {if !$contract_contents}
                            <li role="presentation" class="active"><a aria-controls="english" data-toggle="tab" href="#contentenglish">English</a></li>
                        {else}
                            {foreach from=$contract_contents key=language item=contract_content}
                                <li role="presentation" {if $language eq 'english'}class='active'{/if}>
                                    <a aria-controls="content{$language}" data-toggle="tab" href="#content{$language}">{$language}
                                        {if $language neq 'english'}<button style="margin-left: 10px; line-height: 15px;" type="button" class="close">&times;</button>{/if}
                                    </a>
                                </li>
                            {/foreach}
                        {/if}
                        <li role="presentation" class="languageSelect dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-controls="drop" aria-haspopup="true" aria-expanded="false">
                              {$MGLANG->T('label', 'addLanguage')} <span class="caret"></span>
                            </a>
                            <ul id ="drop" class="dropdown-menu">
                                {foreach from=$languages item=language}
                                    {if $language neq 'english'}<li role="presentation"><a data-language="{$language}">{$language}</a></li>{/if}
                                {/foreach}
                            </ul>
                        </li>
                    </ul>
                    <div class="tab-content">
                        {if !$contract_contents}
                            <div role="tabpanel" class="tab-pane active" id="contentenglish">
                                <textarea name="contract_content[english]" style="height: 500px;">{$contract_content}</textarea>
                            </div>
                        {else}
                            {foreach from=$contract_contents key=language item=contract_content}
                                <div role="tabpanel" class="tab-pane {if $language eq 'english'}active{/if}" id="content{$language}" >
                                    <textarea name="contract_content[{$language}]" style="height: 500px;">{$contract_content}</textarea>
                                </div>
                            {/foreach}
                        {/if}
                    </div>
                </div>
                <div class="col-lg-4" style="margin-top: -1px;">
                    <h4>{$MGLANG->T('label','merge_fields')}</h4>
                    <ul class="list-group">
                        {foreach from=$merge_fields key=name item=fields}
                            {assign var=mfields value=$fields}
                            <li class="list-group-item fields-group expandable-list" expanded="{if $name eq 'client'}true{else}false{/if}" style="cursor: pointer;"><strong>{$MGLANG->T('group', $name)}</strong><p class="pull-right expandable">{if $name eq 'client'}-{else}+{/if}</p></li>
                            <li class="list-group-item fields-container" style="display: {if $name eq 'client'}block{else}none{/if};">
                                {foreach from=$mfields item=mfield}
                                    <span class="field-row"><div class="field-label">{$MGLANG->T('groupitem', $name, $mfield)}</div> <div class="field-mergefield">{ldelim}${$name}.{$mfield}{rdelim}</div></span>
                                {/foreach}
                            </li>
                        {/foreach}
                    </ul>
                </div>
            </div><br>
            <div class="row">
                <div class="col-lg-12">
                    <button arval-button="1" style="margin-right: 10px;" class="btn btn-success btn-inverse" {if $inuse}disabled=""{/if}>{$MGLANG->T('button','save')}</button>
                    <a href="addonmodules.php?module=recurringBilling&mg-page=contractContent" class="btn btn-default btn-inverse">{$MGLANG->T('button','back')}</a>
                </div>
            </div>
        </form>
    </div>
        
    <script src="../modules/addons/recurringBilling/templates/admin/assets/js/tinymce/tinymce.min.js"></script>
    
    {literal}
        <script type="text/javascript">
            function refresh()
            {                
                tinymce.init({
                    selector: "textarea",
                    theme: "modern",
                    plugins: [
                        "advlist autolink lists link image charmap preview hr",
                        "searchreplace wordcount visualblocks visualchars code fullscreen",
                        "insertdatetime nonbreaking save table contextmenu directionality",
                        "paste textcolor colorpicker textpattern imagetools"
                    ],
                    toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | preview | forecolor backcolor",
                    image_advtab: true,
                });
                
                jQuery(".close").unbind("click");
                jQuery(".dropdown-menu li").unbind('click');
                
                jQuery(".close").on("click", function()
                {
                    if(jQuery(this).parent().parent().hasClass("active"))
                    {
                        jQuery(".nav-tabs").find("li:first").addClass("active");
                        jQuery(".tab-content").find("div:first").addClass("active");
                    }
                    
                    var tabpane = jQuery(this).parent().attr('aria-controls');
                    jQuery("#"+tabpane).remove();
                    
                    var language = jQuery(this).parent().text();
                    language = language.substring(0, language.length - 1);
                    jQuery(".dropdown-menu").append('<li role="presentation"><a data-language="'+language+'">'+language+'</a></li>');
                    jQuery(this).parent().remove();
                    
                    refresh();
                });
                
                jQuery(".dropdown-menu li").on('click', function(e){
                    var language = jQuery(this).find('a').data('language');
                    jQuery(".languageSelect").before('<li style="text-transform:capitalize" role="presentation"><a aria-controls="content'+language+'" data-toggle="tab" href="#content'+language+'">'+language+'<button style="margin-left: 10px; line-height: 15px;" type="button" class="close">&times;</button></a></li>')
                    jQuery(".tab-content").append('<div role="tabpanel" class="tab-pane" id="content'+language+'"><textarea name="contract_content['+language+']" style="height: 500px;"></textarea></div>')
                    jQuery(this).remove();
                    refresh();
                });
            }
            
            jQuery(document).ready(function(){
                refresh();

                jQuery(".expandable-list").click(function(){
                    if(jQuery(this).attr("expanded") === "false") {
                        jQuery(this).find("p").html("-");
                        jQuery(this).next().slideDown(100);
                        jQuery(this).attr("expanded", "true");
                    }
                    else if(jQuery(this).attr("expanded") === "true")
                    {
                        jQuery(this).find("p").html("+");
                        jQuery(this).next().slideUp(100);
                        jQuery(this).attr("expanded", "false");
                    }
                }); 
                
                jQuery(".field-row").click(function(){
                    var val = jQuery(this).find(".field-mergefield").html();
                    tinymce.activeEditor.execCommand('mceInsertContent', false, val);
                });
                jQuery(".field-row").mouseover(function(){
                    jQuery(this).css('background-color', '#EEE');
                });
                jQuery(".field-row").mouseleave(function(){
                    jQuery(this).css('background-color', '');
                });
            });
        </script>
    {/literal}