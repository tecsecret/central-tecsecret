<tr>
    <td>{$id}</td>
    <td>{$name}</td>
    <td>{$edited}</td>
    <td>
        <a bstt="1" title="{$MGLANG->T('tooltip','edit')}" href="addonmodules.php?module=recurringBilling&mg-page=contractContent&mg-action=edit&id={$id}" class="btn btn-sm btn-warning btn-inverse btn-icon-only"><i class="glyphicon glyphicon-pencil"></i></a>
        <button bstt="1" title="{$MGLANG->T('tooltip','delete')}" type="button" data-modal-id="MGDeleteItem" data-modal-target="{$id}" class="btn btn-sm btn-danger btn-inverse btn-icon-only deleteItem" name="delete" value="{$id}"><i class="glyphicon glyphicon-remove"></i></button>
    </td>
</tr>