<style>
    {literal}
        td.dataTables_empty {
            text-align: center;
        }
    {/literal}
</style>
<!-- BEGIN PAGE BREADCRUMB -->
<ul class="breadcrumb">
    <li>
        <a href="addonmodules.php?module=recurringBilling"><i class="fa fa-home"></i></a>
    </li>
    <li>
        <a href="addonmodules.php?module=recurringBilling&mg-page=contractContent">{$MGLANG->T('breadcrumb','contractContent')}</a>
    </li>
</ul>

<div class="col-lg-12" id="MGItems">
    <form method="post" action="{$formLink}">
        <input type="hidden" name="mg-action" value="addRows">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th style="width: 5%;">{$MGLANG->T('item','id','label')}</th>
                    <th style="">{$MGLANG->T('item','name','label')}</th>
                    <th style="">{$MGLANG->T('item','lastedit','label')}</th>
                    <th style="width: 8%;">{$MGLANG->T('item','actions','label')}</th>
                </tr>
            </thead>
            <tbody>

            </tbody>
            <tfoot>
                <tr><td><a class="btn btn-sm btn-success btn-inverse" href="addonmodules.php?module=recurringBilling&mg-page=contractContent&mg-action=edit&id=0"><i class="glyphicon glyphicon-plus"></i>{$MGLANG->T('button','addNew')}</a></td><td colspan="3"></td></tr>
            </foot>
        </table>
    </form>
            
    <div class="modal fade bs-example-modal-lg" data-modal-load="getItem" id="MGDeleteItem" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
              <h4 class="modal-title" id="myModalLabel">{$MGLANG->T('modal','deleteLabel')} <strong data-modal-var="name"></strong></h4>
            </div>
            <div class="modal-loader"></div>
            <div class="modal-body">
                <input type="hidden" name="id" value="">
                <div class="alert alert-warning">{$MGLANG->T('modal','deleteDescription')}</div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-danger btn-inverse" data-modal-action="deleteItem">{$MGLANG->T('modal','delete')}</button>
              <button type="button" class="btn btn-default btn-inverse" data-dismiss="modal">{$MGLANG->T('modal','close')}</button>
            </div>
        </div>
      </div>
    </div>      

</div>      


{literal}
    <script type="text/javascript">
        jQuery(document).ready(function(){
            jQuery('#MGItems table').dataTable( {
                  processing: true,
                  serverSide: true,
                  searching: false,
                  autoWidth: false,
                  ajax: function(data, callback, settings){
                      var filter = {
                      };
                      JSONParser.request(
                                  'getList'
                                  , {
                                      filter: filter
                                      ,order: data.order[0]
                                      ,limit: data.length
                                      ,offset: data.start
                                  }
                                  , function(data) {
                                      callback(data);
                                      jQuery('[data-toggle="tooltip"]').tooltip();
                                      jQuery('a[bstt="1"], button[bstt="1"]').bstooltip();
                                  }
                      );
                  },
                  columns: [
                      null
                      ,null
                      ,null
                      ,{ orderable: false, targets: 0}
                  ],
                  pagingType: "simple_numbers",
                  aLengthMenu: [
                      [10, 25, 50, 75, 100],
                      [10, 25, 50, 75, 100]
                  ],
                  iDisplayLength: {/literal}{$limit}{literal},
                  sDom: '<"top"<"row"<"col-sm-6"l><"col-sm-6">>>t<"table-bottom"<"row"<"col-sm-6"i><"col-sm-6"Lp>>>',
                        "zeroRecords": "{/literal}{$MGLANG->absoluteT('addonAA','datatables','zeroRecords')}{literal}",
                        "infoEmpty": "{/literal}{$MGLANG->absoluteT('addonAA','datatables','zeroRecords')}{literal}",
                        "paginate": {
                          "previous": "{/literal}{$MGLANG->absoluteT('addonAA','datatables','previous')}{literal}"
                          ,"next": "{/literal}{$MGLANG->absoluteT('addonAA','datatables','next')}{literal}"
                   }
            });

            jQuery('#MGItems').MGModalActions();

            //var field_counter = 0;
            //var button_counter = 0;
            /*
            jQuery('#MGAddField').click(
                function(){
                    jQuery('#save-changes').show('400');
                    var html = '<tr>\n\
                        <td></td>\n\
                        <td><input class="form-control" type="text" name="new_names[' + (field_counter++) + ']"></td>\n\
                        <td></td>\n\
                        <td><a><a class="btn btn-sm btn-danger btn-icon-only deleteRow"><i class="glyphicon glyphicon-remove"></i></a></a></td>\n\
                    </tr>';
                    jQuery('#MGItems table tbody').append(html);
                    button_counter++;
                }
            );
                
            jQuery(document).delegate('.deleteRow', 'click', function(){
                button_counter--;
                $(this).closest('tr').remove();
                if(button_counter <= 0) {
                    jQuery('#save-changes').hide('400');
                }
            });
            */
            //jQuery(document).delegate(".deleteItem", "click", function(){
            //    var value = jQuery(this).val();
            //    jQuery("#deleteId").val(value);
            //});
        });
    </script>
{/literal}