<!-- BEGIN PAGE BREADCRUMB -->
<ul class="breadcrumb">
    <li>
        <a href="addonmodules.php?module=recurringBilling"><i class="fa fa-home"></i></a>
    </li>
    <li>
        <a href="addonmodules.php?module=recurringBilling&mg-page=signTypes">{$MGLANG->T('breadcrumb','signTypes')}</a>
    </li>
</ul>

<form class="form-horizontal" action="addonmodules.php?module=recurringBilling&mg-page=signTypes" method="post">
    <input type="hidden" name="mg-action" value="save"> 
    {foreach from=$configuration item="submodule"}
        <div class="col-md-6">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">{$MGLANG->T($submodule.name, 'title','template')}</h3>
                </div>
                <div class="panel-body" style="min-height:450px;">
                    {if $submodule.success } 
                        <div class="alert alert-success alert-dismissable fade in"><a href="addonmodules.php?module=recurringBilling&mg-page=signTypes" class="close" data-dismiss="alert" aria-label="close">&times;</a>{$submodule.success }</div>
                    {/if}
                    {$submodule.fields}
                </div>

            </div> 
        </div>
    {/foreach}
    <div class="col-md-12">
        <p><button class="btn btn-inverse btn-success">{$MGLANG->T('button','save')}</button></p>
    </div>
</form>
