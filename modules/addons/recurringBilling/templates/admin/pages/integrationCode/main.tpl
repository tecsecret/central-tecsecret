<!-- BEGIN PAGE BREADCRUMB -->
<ul class="breadcrumb">
    <li>
        <a href="addonmodules.php?module=recurringBilling"><i class="fa fa-home"></i></a>
    </li>
    <li>
        <a href="addonmodules.php?module=recurringBilling&mg-page=integrationCode">{$MGLANG->T('breadcrumb','integrationCode')}</a>
    </li>
</ul>
{if $WHMCS72}
   <div class="alert alert-info">
        {$MGLANG->T('integration','whmcs7', '11')}
    </div>
    
    <hr>
    
    <div class="alert alert-info">
        {$MGLANG->T('integration','whmcs7', '12')}<br>
        {$MGLANG->T('integration','whmcs7', '13a')}{$orderFormTemplate}{$MGLANG->T('integration','whmcs7', '13b')}<br>
        {$MGLANG->T('integration','whmcs7', '13c')}
    </div>
    <div class="alert alert-info">
        {$MGLANG->T('integration','whmcs7', '14')}
    </div>

    <pre>
    &lt;div class=&quot;sub-heading&quot;&gt;
        &lt;span&gt;{ldelim}$LANG.orderForm.paymentDetails{rdelim}&lt;/span&gt;
    &lt;/div&gt;</pre>

    <div class="alert alert-info">
        {$MGLANG->T('integration','whmcs7', '15')}
    </div>

    <pre>
    &lt;div class=&quot;sub-heading&quot; id=&quot;recurring-biling-contracts&quot;&gt;
        &lt;span&gt;{ldelim}$contractTitle{rdelim}&lt;/span&gt;
    &lt;/div&gt;
    {ldelim}$contractOutput{rdelim}</pre>
    
{elseif $WHMCS6}
    <div class="alert alert-info">
        {$MGLANG->T('integration','whmcs6', '11')}
    </div>
    
    <hr>
    
    <div class="alert alert-info">
        {$MGLANG->T('integration','whmcs6', '12')}<br>
        {$MGLANG->T('integration','whmcs6', '13a')}{$orderFormTemplate}{$MGLANG->T('integration','whmcs6', '13b')}<br>
        {$MGLANG->T('integration','whmcs6', '13c')}
    </div>
    <div class="alert alert-info">
        {$MGLANG->T('integration','whmcs6', '14')}
    </div>

    <pre>
    &lt;div class=&quot;sub-heading&quot;&gt;
        &lt;span&gt;{ldelim}$LANG.orderForm.paymentDetails{rdelim}&lt;/span&gt;
    &lt;/div&gt;</pre>

    <div class="alert alert-info">
        {$MGLANG->T('integration','whmcs6', '15')}
    </div>

    <pre>
    &lt;div class=&quot;sub-heading&quot; id=&quot;recurring-biling-contracts&quot;&gt;
        &lt;span&gt;{ldelim}$contractTitle{rdelim}&lt;/span&gt;
    &lt;/div&gt;
    {ldelim}$contractOutput{rdelim}</pre>
     
{else}
    <div class="alert alert-info">
        {$MGLANG->T('integration','whmcs5', '0')}
    </div>
    
    <hr>
    
    <div class="alert alert-info">
        {$MGLANG->T('integration','whmcs5', '11')}<br>
        {$MGLANG->T('integration','whmcs5', '12')}
    </div>
    <div class="alert alert-info">
        {$MGLANG->T('integration','whmcs5', '13')}
    </div>
    
    <pre>
    {ldelim}if $taxenabled && !$loggedin{rdelim}</pre>

    <div class="alert alert-info">
        {$MGLANG->T('integration','whmcs5', '14')}
    </div>

    <pre>
    &lt;div class=&quot;signupfields padded&quot;&gt;
        &lt;h2&gt;{ldelim}$contractTitle{rdelim}&lt;/h2&gt;
        {ldelim}$contractOutput{rdelim}
    &lt;/div&gt;</pre>
    
    <hr>
    
    <div class="alert alert-info">
        {$MGLANG->T('integration','whmcs5', '21')}<br>
        {$MGLANG->T('integration','whmcs5', '22')}
    </div>
    <div class="alert alert-info">
        {$MGLANG->T('integration','whmcs5', '23')}
    </div>
    
    <pre>
    &lt;li&gt;&lt;a id=&quot;Menu-Billing-My_Quotes&quot; href=&quot;clientarea.php?action=quotes&quot;&gt;{ldelim}$LANG.quotestitle{rdelim}&lt;/a&gt;&lt;/li&gt;</pre>
    
    <div class="alert alert-info">
        {$MGLANG->T('integration','whmcs5', '24')}
    </div>
    
    <pre>
    &lt;li&gt;&lt;a id=&quot;Menu-Billing-My_Contracts&quot; href=&quot;{ldelim}$contractLink{rdelim}&quot;&gt;{ldelim}$contractTitle{rdelim}&lt;/a&gt;&lt;/li&gt;</pre>
{/if}