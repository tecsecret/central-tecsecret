<!-- BEGIN PAGE BREADCRUMB -->
<ul class="breadcrumb">
    <li>
        <a href="addonmodules.php?module=recurringBilling"><i class="fa fa-home"></i></a>
    </li>
    <li>
        <a href="addonmodules.php?module=recurringBilling&mg-page=configuration">{$MGLANG->T('breadcrumb','configuration')}</a>
    </li>
</ul>

<div class="col-lg-12" id="MGItems">  
    <form method="post" action="addonmodules.php?module=recurringBilling&mg-page=configuration">
        <input type="hidden" name="mg-action" value="save">
        <div class="row">
            <div class="col-md-6">
                <div class="panel panel-primary">
                    <div class="panel-heading">{$MGLANG->T('title','notifications')}</div>
                    <div class="panel-body">

                        <div class="form-group row">
                            <div class="col-md-6" bstt="1" title="{$MGLANG->T('tooltip','notify1')}">
                                <div class="col-sm-8" style="text-align: right;">
                                    <label style="margin-top: 5px;">
                                        <b>
                                            {$MGLANG->T('checkbox','notify1')}
                                        </b>
                                    </label>
                                </div>
                                <div class="col-sm-4" style="float: right">
                                    <div class="checkbox-slider--b-flat" style="margin-top: 5px;">
                                        <label>
                                            <input name="settings[notify1]" {if $settings.notify1}checked{/if} type="checkbox">
                                            <span></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6" bstt="1" title="{$MGLANG->T('tooltip','notify1time')}">
                                <div class="col-sm-8" style="text-align: right;">
                                    <label style="margin-top: 5px;">
                                        <b>
                                            {$MGLANG->T('text','notify1time')}
                                        </b>
                                    </label>
                                </div>
                                <div class="col-sm-4" style="float: right">
                                    <input value="{$settings.notify1time}" class="form-control input-sm" name="settings[notify1time]" type="text" size="8" {if not $settings.notify1}disabled{/if}>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6" bstt="1" title="{$MGLANG->T('tooltip','notify2')}">
                                <div class="col-sm-8" style="text-align: right;">
                                    <label style="margin-top: 5px;">
                                        <b>
                                            {$MGLANG->T('checkbox','notify2')}
                                        </b>
                                    </label>
                                </div>
                                <div class="col-sm-4" style="float: right">
                                    <div class="checkbox-slider--b-flat" style="margin-top: 5px;">
                                        <label>
                                            <input name="settings[notify2]" {if $settings.notify2}checked{/if} type="checkbox">
                                            <span></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6" bstt="1" title="{$MGLANG->T('tooltip','notify2time')}">
                                <div class="col-sm-8" style="text-align: right;">
                                    <label style="margin-top: 5px;">
                                        <b>
                                            {$MGLANG->T('text','notify2time')}
                                        </b>
                                    </label>
                                </div>
                                <div class="col-sm-4" style="float: right">
                                    <input value="{$settings.notify2time}" class="form-control input-sm" name="settings[notify2time]" type="text" size="8" {if not $settings.notify3}disabled{/if}>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6" bstt="1" title="{$MGLANG->T('tooltip','notify3')}">
                                <div class="col-sm-8" style="text-align: right;">
                                    <label style="margin-top: 5px;">
                                        <b>
                                            {$MGLANG->T('checkbox','notify3')}
                                        </b>
                                    </label>
                                </div>
                                <div class="col-sm-4" style="float: right">
                                    <div class="checkbox-slider--b-flat" style="margin-top: 5px;">
                                        <label>
                                            <input name="settings[notify3]" {if $settings.notify3}checked{/if} type="checkbox">
                                            <span></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6" bstt="1" title="{$MGLANG->T('tooltip','notify3time')}">
                                <div class="col-sm-8" style="text-align: right;">
                                    <label style="margin-top: 5px;">
                                        <b>
                                            {$MGLANG->T('text','notify3time')}
                                        </b>
                                    </label>
                                </div>
                                <div class="col-sm-4" style="float: right">
                                    <input value="{$settings.notify3time}" class="form-control input-sm" name="settings[notify3time]" type="text" size="8" {if not $settings.notify3}disabled{/if}>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="panel panel-primary">
                    <div class="panel-heading">{$MGLANG->T('title','penalty')}</div>
                    <div class="panel-body">
                        
                        <div class="form-group row" >
                            <div class="col-md-6" bstt="1" title="{$MGLANG->T('tooltip','penalty')}">
                                <div class="col-md-8" style="text-align: right;">
                                    <label style="margin-top: 5px;">
                                        <b>
                                            {$MGLANG->T('text','penalty')}
                                        </b>
                                    </label>
                                </div>
                                <div class="col-md-4" style="float: right">
                                    <input value="{$settings.penalty}" class="form-control input-sm" name="settings[penalty]" type="text" style="width: 92.4883px;">
                                    <span></span>
                                </div>
                            </div>
                        </div>
                                    
                    </div>
                </div>
                <div class="panel panel-primary">
                    <div class="panel-heading">{$MGLANG->T('title','guide')}</div>
                    <div class="panel-body">
                        
                        <div class="form-group row">
                            <div class="col-md-6" bstt="1" title="{$MGLANG->T('tooltip','guide')}">
                                <div class="col-md-8" style="text-align: right;">
                                    <label style="margin-top: 5px;">
                                        <b>
                                            {$MGLANG->T('text','guide')}
                                        </b>
                                    </label>
                                </div>
                                <div class="col-md-4" style="float: right">
                                    <div class="checkbox-slider--b-flat" style="margin-top: 5px;">
                                        <label>
                                            <input name="settings[guide]" {if $settings.guide}checked{/if} type="checkbox">
                                            <span></span>
                                        </label>
                                    </div>
                                </div>
                            </div>   
                        </div>
                                    
                    </div>
                </div>
            </div>                   
        </div>
        <p><button class="btn btn-inverse btn-success">{$MGLANG->T('button','save')}</button></p>
    </form>
</div>      


{literal}
    <script type="text/javascript">
        jQuery(document).ready(function(){
            jQuery('input[type="checkbox"]').click(function() {
                if(jQuery(this).is(':checked')) {
                    jQuery(this).closest(".col-md-6").next().find(".col-sm-4").find("input").attr("disabled", false);
                } else {
                    jQuery(this).closest(".col-md-6").next().find(".col-sm-4").find("input").attr("disabled", true);
                }
            });
            
            jQuery('[bstt="1"]').bstooltip();
        });
    </script>
{/literal}