    {literal}
        <style>
            .expandable {
                font-weight: bold;
                font-size: 16px;
                cursor: pointer;
            }
            .fields-group {
                background: #EEE!important;
            }
            .field-label {
                font-size: 12px;
                font-weight: bold;
                width: 30%; 
                display: table-cell;
            }
            .field-mergefield {
                font-size: 12px;
                width: 65%; 
                display: table-cell;
            }
            .field-row {
                width: 100%;
                display: table-row;
                cursor: pointer;
            }
            .phat {
                font-weight: bold;
            }
            .formatowacz {
                font-size: 16px;
            }
        </style>
    {/literal}

    <!-- BEGIN PAGE BREADCRUMB -->
    <ul class="breadcrumb">
        <li>
            <a href="addonmodules.php?module=recurringBilling"><i class="fa fa-home"></i></a>
        </li>
        <li>
            <a href="addonmodules.php?module=recurringBilling&mg-page=signedContracts">{$MGLANG->T('breadcrumb','signedContracts')}</a>
        </li>
        <li>
            <a href="addonmodules.php?module=recurringBilling&mg-page=signedContracts&mg-action=edit&id={$id}">{$MGLANG->T('breadcrumb','editContract')}</a>
        </li>
    </ul>
        
    <div class="col-lg-12" id="MGItems">
        <div class="alert alert-info">{$MGLANG->T('notify','change')}</div>
        <form method="post" action="" class="form">
            <input type="hidden" name="mg-action" value="saveContent">
            <input type="hidden" name="id" value="{$id}">
            <div class="row">
                <div class="col-md-1">
                    <span class="phat formatowacz">{$MGLANG->T('label','client')}:</span>
                </div>
                <div class="col-md-5">
                    <span class="formatowacz"><a style="font-size: 16px;" target="_blank" href="clientssummary.php?userid={$contract->_client->id}">{$contract->_client->firstname} {$contract->_client->lastname}</a></span>
                </div>
                <div class="col-md-1">
                    <span class="phat formatowacz">{$MGLANG->T('label','service')}:</span>
                </div>
                <div class="col-md-5">
                    <span class="formatowacz"><a style="font-size: 16px;" target="_blank" href="clientsservices.php?userid={$contract->_client->id}&id={$contract->_service->id}">{$contract->_product->name}</a></span>
                </div>
            </div><br>
                
            <div class="row">
                <div class="col-md-1" style="margin-top: 3px;">
                    <span class="phat formatowacz">{$MGLANG->T('label','amount')}:</span>
                </div>
                <div class="col-md-5">
                    <span class="formatowacz">{$contract->_currency->prefix}<input style="width: 75px; display: inline;" class="form-control input-sm" type="text" name="amount" value="{$contract->_service->amount}">{$contract->_currency->suffix}</span>
                </div>
                <div class="col-md-1" style="margin-top: 3px;">
                    <span class="phat formatowacz">{$MGLANG->T('label','signDate')}:</span>
                </div>
                <div class="col-md-5">
                    <span class="formatowacz"><input style="width: 150px;" type="text" class="form-control input-sm" name="startdate" value="{$date}"></span>
                </div>
            </div><br>

            <div class="row">
                <div class="col-md-1" style="margin-top: 3px;">
                    <span class="phat formatowacz">{$MGLANG->T('label','length')}:</span>
                </div>
                <div class="col-md-5">
                    <span class="formatowacz"><select style="width: 100px;" class="form-control input-sm" name="length">{$length_dropdown}</select></span>
                </div>
                <div class="col-md-1">
                    <span class="phat formatowacz">{$MGLANG->T('label','endDate')}:</span>
                </div>
                <div class="col-md-5">
                    <span class="formatowacz">{$contract->enddate}</span>
                </div>
            </div><br>

            <div class="row">
                <div class="col-md-1">
                    <span class="phat formatowacz">{$MGLANG->T('label','status')}:</span>
                </div>
                <div class="col-md-5">
                    <span class="formatowacz">{$pstatus}</span>
                </div>
            </div>

            <hr style="margin: 5px 0px; border-color: #DDD;">
            
            <div class="row">
                <div class="col-lg-8">
                    <h4>{$MGLANG->T('label','content')}</h4>
                    <textarea name="contract_content" style="height: 500px;">{$content}</textarea>
                </div>
                <div class="col-lg-4" style="margin-top: -1px;">
                    <h4>{$MGLANG->T('label','history')}</h4>
                    {foreach from=$history item=entry}
                        <ul class="list-group" style="margin-bottom: 5px;">
                            <li class="list-group-item fields-item">
                                <div class="row">
                                    <div class="col-xs-6">
                                        {$entry->date}<br>
                                        {$MGLANG->T('label','admin')} <a target="_blank" href="configadmins.php?action=manage&id={$entry->_admin->id}">{$entry->_admin->firstname} {$entry->_admin->lastname}</a>
                                    </div>
                                    <div class="col-xs-6" style="margin-top: 5px;">
                                        <a target="_blank" href="../recurringbilling.php?entryid={$entry->id}" class="btn btn-default btn-inverse btn-info pull-right">{$MGLANG->T('button','viewPDF')}</a>
                                        {if $status == "active"}<button data-modal-target="{$entry->id}" data-modal-id="MGRestoreHistory" class="btn btn-default btn-inverse btn-orange pull-right" style="margin-right: 7px;">{$MGLANG->T('button','revert')}</button>{/if}
                                    </div>
                                </div>
                            </li>
                        </ul>
                    {foreachelse}
                        <h5><strong>{$MGLANG->T('text','noEditHistory')}</strong></h5>
                    {/foreach}
                </div>
            </div><br>
            <div class="row">
                <div class="col-lg-12">
                    {if $status == "active"}<button style="margin-right: 10px;" class="btn btn-success btn-inverse">{$MGLANG->T('button','save')}</button>{/if}
                    <a href="addonmodules.php?module=recurringBilling&mg-page=signedContracts" class="btn btn-default btn-inverse">{$MGLANG->T('button','back')}</a>
                </div>
            </div>
        </form>
            
        <div class="modal fade bs-example-modal-lg" data-modal-load="getItem" id="MGRestoreHistory" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
                <form action="" method="post">
                    <input type="hidden" name="mg-action" value="restoreItem">
                    <input type="hidden" name="cid" value="{$id}">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                      <h4 class="modal-title" id="myModalLabel">{$MGLANG->T('modal','restoreLabel')} <strong data-modal-var="name"></strong></h4>
                    </div>
                    <div class="modal-loader"></div>
                    <div class="modal-body">
                        <input type="hidden" name="id" value="">
                        <div class="alert alert-warning">{$MGLANG->T('modal','restoreDescription')}</div>
                    </div>
                    <div class="modal-footer">
                      <input type="submit" class="btn btn-orange btn-inverse" value="{$MGLANG->T('modal','restore')}">
                      <button type="button" class="btn btn-default" data-dismiss="modal">{$MGLANG->T('modal','close')}</button>
                    </div>
                </form>
            </div>
          </div>
        </div>  
    </div>
        
    <script src="../modules/addons/recurringBilling/templates/admin/assets/js/tinymce/tinymce.min.js"></script>
    
    {literal}
        <script type="text/javascript">
            tinymce.init({
                selector: "textarea",
                theme: "modern",
                plugins: [
                    "advlist autolink lists link image charmap preview hr",
                    "searchreplace wordcount visualblocks visualchars code fullscreen",
                    "insertdatetime nonbreaking save table contextmenu directionality",
                    "paste textcolor colorpicker textpattern imagetools"
                ],
                toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | preview | forecolor backcolor",
                image_advtab: true,
            });
            
            jQuery('#MGItems').MGModalActions();
        </script>
    {/literal}