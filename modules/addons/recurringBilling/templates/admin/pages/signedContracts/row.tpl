<tr>
    <td>{$id}</td>
    <td>{$client}</td>
    <td>{$service}</td>
    <td>{$amount}</td>
    <td>{$status}</td>
    <td>{$date}</td>
    <td>{$length}</td>
    <td>{$enddate}</td>
    <td>
        <a bstt="1" title="{$MGLANG->T('tooltip','edit')}" href="addonmodules.php?module=recurringBilling&mg-page=signedContracts&mg-action=edit&id={$id}" class="btn btn-sm btn-warning btn-inverse btn-icon-only"><i class="glyphicon glyphicon-pencil"></i></a>
        {if $status eq "active"}<button bstt="1" title="{$MGLANG->T('tooltip','cancel')}" type="button" data-modal-id="MGCancelContract" data-modal-target="{$id}" class="btn btn-sm btn-danger btn-inverse btn-icon-only hide-on-update" name="cancel" value="{$id}"><i class="glyphicon glyphicon-remove"></i></button>{/if}
    </td>
</tr>