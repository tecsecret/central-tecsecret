<style>
    {literal}
        td.dataTables_empty {
            text-align: center;
        }
    {/literal}
</style>
<!-- BEGIN PAGE BREADCRUMB -->
<ul class="breadcrumb">
    <li>
        <a href="addonmodules.php?module=recurringBilling"><i class="fa fa-home"></i></a>
    </li>
    <li>
        <a href="addonmodules.php?module=recurringBilling&mg-page=signedContracts">{$MGLANG->T('breadcrumb','signedContracts')}</a>
    </li>
</ul>

<div class="col-lg-12" id="MGItems">
    <table class="table table-hover">
        <thead>
            <tr>
                <th style="width: 5%">{$MGLANG->T('item','id','label')}</th>
                <th style="">{$MGLANG->T('item','client','label')}</th>
                <th style="">{$MGLANG->T('item','service','label')}</th>
                <th style="">{$MGLANG->T('item','amount','label')}</th>
                <th style="">{$MGLANG->T('item','signdate','label')}</th>
                <th style="">{$MGLANG->T('item','length','label')}</th>
                <th style="">{$MGLANG->T('item','enddate','label')}</th>
                <th style="">{$MGLANG->T('item','status','label')}</th>
                <th style="width: 8%;">{$MGLANG->T('item','actions','label')}</th>
            </tr>
        </thead>
        <tbody>
            <!-- LOADED DYNAMICALLY -->
        </tbody>
        <tfoot>
            <!-- EMPTY -->
        </foot>
    </table>
            
    <div class="modal fade bs-example-modal-lg" data-modal-load="getItem" id="MGCancelContract" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
              <h4 class="modal-title" id="myModalLabel">{$MGLANG->T('modal','cancelLabel')} <strong data-modal-var="name"></strong></h4>
            </div>
            <div class="modal-loader"></div>
            <div class="modal-body">
                <input type="hidden" name="id" value="">
                <div class="alert alert-warning">{$MGLANG->T('modal','cencelDescription')}</div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-danger btn-inverse" data-modal-action="cancelContract">{$MGLANG->T('modal','cancel')}</button>
              <button type="button" class="btn btn-default" data-dismiss="modal">{$MGLANG->T('modal','close')}</button>
            </div>
        </div>
      </div>
    </div> 
</div>
            
{literal}
    <script type="text/javascript">        
        jQuery(document).ready(function(){
            jQuery('#MGItems table').dataTable( {
                  processing: true,
                  serverSide: true,
                  searching: true,
                  autoWidth: false,
                  ajax: function(data, callback, settings){
                      var filter = {
                      };
                      JSONParser.request(
                                  'getList'
                                  , {
                                      filter: filter
                                      ,order: data.order[0]
                                      ,limit: data.length
                                      ,offset: data.start
                                  }
                                  , function(data) {
                                      callback(data);
                                      jQuery('[data-toggle="tooltip"]').tooltip();
                                      jQuery('[bstt="1"]').bstooltip();
                                  }
                      );
                  },
                  columns: [
                      null
                      ,null
                      ,{ orderable: false, targets: 0}
                      ,{ orderable: false, targets: 0}
                      ,null
                      ,{ orderable: false, targets: 0}
                      ,null
                      ,null
                      ,{ orderable: false, targets: 0}
                  ],
                  pagingType: "simple_numbers",
                  aLengthMenu: [
                      [10, 25, 50, 75, 100],
                      [10, 25, 50, 75, 100]
                  ],
                  iDisplayLength: {/literal}{$limit}{literal},
                  sDom: '<"top"<"row"<"col-sm-6"l><"col-sm-6">>>t<"table-bottom"<"row"<"col-sm-6"i><"col-sm-6"Lp>>>',
                        "zeroRecords": "{/literal}{$MGLANG->absoluteT('addonAA','datatables','zeroRecords')}{literal}",
                        "infoEmpty": "{/literal}{$MGLANG->absoluteT('addonAA','datatables','zeroRecords')}{literal}",
                        "paginate": {
                          "previous": "{/literal}{$MGLANG->absoluteT('addonAA','datatables','previous')}{literal}"
                          ,"next": "{/literal}{$MGLANG->absoluteT('addonAA','datatables','next')}{literal}"
                   }
            });

            jQuery('#MGItems').MGModalActions();

        });
    </script>
{/literal}