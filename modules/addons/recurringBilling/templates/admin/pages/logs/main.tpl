<style>
    {literal}
        td.dataTables_empty {
            text-align: center;
        }
    {/literal}
</style>
<!-- BEGIN PAGE BREADCRUMB -->
<ul class="breadcrumb">
    <li>
        <a href="addonmodules.php?module=recurringBilling"><i class="fa fa-home"></i></a>
    </li>
    <li>
        <a href="addonmodules.php?module=recurringBilling&mg-page=logs">{$MGLANG->T('breadcrumb','logs')}</a>
    </li>
</ul>

<div class="col-lg-12" id="MGItems">
    <table class="table table-hover">
        <thead>
            <tr>
                <th style="width: 5%">{$MGLANG->T('item','id','label')}</th>
                <th style="">{$MGLANG->T('item','contract','label')}</th>
                <th style="">{$MGLANG->T('item','user','label')}</th>
                <th style="">{$MGLANG->T('item','log','label')}</th>
                <th style="">{$MGLANG->T('item','date','label')}</th>
            </tr>
        </thead>
        <tbody>

        </tbody>
    </table>
</div>
            
{literal}
    <script type="text/javascript">        
        jQuery(document).ready(function(){
            jQuery('#MGItems table').dataTable( {
                  processing: true,
                  serverSide: true,
                  searching: true,
                  searchDelay: 1000,
                  autoWidth: false,
                  ajax: function(data, callback, settings){
                      var filter = {
                      };
                      JSONParser.request(
                                  'getList'
                                  , {
                                      filter: filter
                                      ,order: data.order[0]
                                      ,limit: data.length
                                      ,offset: data.start
                                      ,search: data.search.value
                                  }
                                  , function(data) {
                                      callback(data);
                                      jQuery('[data-toggle="tooltip"]').tooltip();
                                  }
                      );
                  },
                  columns: [
                      null
                      ,null
                      ,{ orderable: false, targets: 0}
                      ,null
                      ,null
                  ],
                  order: [[0, "DESC"]],
                  pagingType: "simple_numbers",
                  aLengthMenu: [
                      [10, 25, 50, 75, 100],
                      [10, 25, 50, 75, 100]
                  ],
                  iDisplayLength: {/literal}{$limit}{literal},
                  sDom: '<"top"<"row"<"col-sm-6"l><"col-sm-6"f>>>t<"table-bottom"<"row"<"col-sm-6"i><"col-sm-6"Lp>>>',
                        "zeroRecords": "{/literal}{$MGLANG->absoluteT('addonAA','datatables','zeroRecords')}{literal}",
                        "infoEmpty": "{/literal}{$MGLANG->absoluteT('addonAA','datatables','zeroRecords')}{literal}",
                        "paginate": {
                          "previous": "{/literal}{$MGLANG->absoluteT('addonAA','datatables','previous')}{literal}"
                          ,"next": "{/literal}{$MGLANG->absoluteT('addonAA','datatables','next')}{literal}"
                   }
            });

        });
    </script>
{/literal}