<style>
    {literal}
        .row-finished {
            background-color: #FFF;
        }

        .row-cancelled {
            background-color: #EEE;
        }

        .row-broken {
            background-color: #F2DEDE;
        }
        .income-label {
            width: 35%; 
            text-align: right; 
            padding-right: 20px!important;
        }
    {/literal}
</style>

<!-- BEGIN PAGE BREADCRUMB -->
<ul class="breadcrumb">
    <li>
        <a href="addonmodules.php?module=recurringBilling"><i class="fa fa-home"></i></a>
    </li>
    <li>
        <a href="addonmodules.php?module=recurringBilling&mg-page=dashboard">{$MGLANG->T('breadcrumb','dashboard')}</a>
    </li>
</ul>

<div class="col-lg-12" id="MGItems">

    {if $config.guide}
        <div class="alert alert-info" id="guide">
            {$MGLANG->T('guide','step1')}<br><br>
            {$MGLANG->T('guide','step2')}<br>
            {$MGLANG->T('guide','step3')}<br>
            <span style="padding-left:3em; color: #31708F;">{$MGLANG->T('guide','step4')}</span><br>
            <span style="padding-left:3em; color: #31708F;">{$MGLANG->T('guide','step5')}</span><br>
            {$MGLANG->T('guide','step6')}<br><br>
            {$MGLANG->T('guide','step7')} <a style="color: #0000FF;" href="http://www.docs.modulesgarden.com/Recurring_Billing_Extended_For_WHMCS">{$MGLANG->T('guide','wiki')}</a>.<br><br>
            <p><button onclick="{literal}JSONParser.request('disableGuides', {}, function(data) {jQuery('#guide').slideToggle('500');});{/literal}" class="btn btn-info btn-inverse">{$MGLANG->T('guide','hide')}</button></p>
        </div>
    {/if}
    
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-primary">
                <div class="panel-heading">{$MGLANG->T('title','contracts')}</div>
                <div class="panel-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>{$MGLANG->T('item','id','label')}</th>
                                <th>{$MGLANG->T('item','client','label')}</th>
                                <th>{$MGLANG->T('item','service','label')}</th>
                                <th>{$MGLANG->T('item','amount','label')}</th>
                                <th>{$MGLANG->T('item','signDate','label')}</th>
                            </tr>
                        </thead>
                        <tbody>
                            {foreach from=$active item=contract}
                                <tr>
                                    <td>{$contract.id}</td>
                                    <td>{$contract.client}</td>
                                    <td>{$contract.service}</td>
                                    <td>{$contract.amount}</td>
                                    <td>{$contract.date}</td>
                                </tr>
                            {foreachelse}
                                <tr><td colspan="5" style="text-align: center;">{$MGLANG->T('label','noContracts')}</td></tr>
                            {/foreach}
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="panel panel-primary">
                <div class="panel-heading">{$MGLANG->T('title','latestLogs')}</div>
                <div class="panel-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>{$MGLANG->T('item','id','label')}</th>
                                <th>{$MGLANG->T('item','contract','label')}</th>
                                <th>{$MGLANG->T('item','user','label')}</th>
                                <th>{$MGLANG->T('item','log','label')}</th>
                                <th>{$MGLANG->T('item','date','label')}</th>
                            </tr>
                        </thead>
                        <tbody>
                            {foreach from=$logs item=log}
                                <tr>
                                    <td>{$log.id}</td>
                                    <td>{$log.contract}</td>
                                    <td>{$log.user}</td>
                                    <td>{$log.log}</td>
                                    <td>{$log.date}</td>
                                </tr>
                            {foreachelse}
                                <tr><td colspan="5" style="text-align: center;">{$MGLANG->T('label','noLogs')}</td></tr>
                            {/foreach}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel panel-primary">
                <div class="panel-heading">{$MGLANG->T('title','endedContracts')}</div>
                <div class="panel-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>{$MGLANG->T('item','id','label')}</th>
                                <th>{$MGLANG->T('item','client','label')}</th>
                                <th>{$MGLANG->T('item','service','label')}</th>
                                <th>{$MGLANG->T('item','amount','label')}</th>
                                <th>{$MGLANG->T('item','endDate','label')}</th>
                            </tr>
                        </thead>
                        <tbody>
                            {foreach from=$ended item=contract}
                                <tr class="row-{$contract.status}">
                                    <td>{$contract.id}</td>
                                    <td>{$contract.client}</td>
                                    <td>{$contract.service}</td>
                                    <td>{$contract.amount}</td>
                                    <td>{$contract.enddate}</td>
                                </tr>
                            {foreachelse}
                                <tr><td colspan="5" style="text-align: center;">{$MGLANG->T('label','noEndedContracts')}</td></tr>
                            {/foreach}
                        </tbody>
                    </table>
                </div>
            </div>
                        
            <div class="panel panel-primary">
                <div class="panel-heading">{$MGLANG->T('title','contractIncome')}</div>
                <div class="panel-body">
                    <table style="width: 100%;">
                        <tr>
                            <td class="income-label"><strong>{$MGLANG->T('label','activeContracts')}</strong></td>
                            <td style="width: 15%;">{$activeContracts}</td>
                            <td class="income-label"><strong>{$MGLANG->T('label','finishedContracts')}</strong></td>
                            <td style="width: 15%;">{$finishedContracts}</td>
                        </tr>
                        <tr>
                            <td class="income-label"><strong>{$MGLANG->T('label','cancelledContracts')}</strong></td>
                            <td>{$cancelledContracts}</td>
                            <td class="income-label"><strong>{$MGLANG->T('label','brokenContracts')}</strong></td>
                            <td>{$brokenContracts}</td>
                        </tr>
                        
                        <tr valign="top">
                            <td class="income-label"><strong>{$MGLANG->T('label','monthlyRevenue')}</strong></td>
                            <td>
                                <table>
                                    {foreach from=$revenue.monthly item=rev}
                                        <tr>
                                            <td>{$rev}</td>
                                        </tr>
                                    {/foreach}
                                </table>
                            </td>
                            <td class="income-label"><strong>{$MGLANG->T('label','yearlyRevenue')}</strong></td>
                            <td>
                                <table>
                                    {foreach from=$revenue.yearly item=rev}
                                        <tr>
                                            <td>{$rev}</td>
                                        </tr>
                                    {/foreach}
                                </table>
                            </td>
                        </tr>
                        <tr valign="top">
                            <td class="income-label"><strong>{$MGLANG->T('label','estMonthlyRevenue')}</strong></td>
                            <td>
                                <table>
                                    {foreach from=$revenue.estMonthly item=rev}
                                        <tr>
                                            <td>{$rev}</td>
                                        </tr>
                                    {/foreach}
                                </table>
                            </td>
                            <td class="income-label"><strong>{$MGLANG->T('label','estYearlyRevenue')}</strong></td>
                            <td>
                                <table>
                                    {foreach from=$revenue.estYearly item=rev}
                                        <tr>
                                            <td>{$rev}</td>
                                        </tr>
                                    {/foreach}
                                </table>
                            </td>
                        </tr>
                        
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>