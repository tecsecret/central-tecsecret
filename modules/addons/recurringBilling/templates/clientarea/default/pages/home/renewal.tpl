{if !$error}
    <div class="alert alert-info">
    {if $renew}
        {$MGLANG->T('info', 'contracthasbeenrenew')} {$invoicelink}
    {elseif $cancel}
        {$MGLANG->T('info', 'contracthasbeencancel')}
    {/if}
    </div>
{/if}

<a class="btn btn-default" href="index.php?m=recurringBilling">{$MGLANG->T('button','back')}</a>

