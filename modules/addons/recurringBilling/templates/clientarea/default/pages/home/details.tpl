{literal}
<style>
    .help-inline {
        display: inline;
        font-size: 12px;
        color: #737373;
    }
    label {
        height: 15px; 
        line-height: 5px;
        margin-right: 15px;
        font-size: 12px !important;
    }
    #contractForm {
        padding: 10px !important;
    }
</style>
{/literal}

<div class="col-lg-12" id="MGContractView">
    <h3>{$MGLANG->T('title','details')}</h3><br>
    <div id="MGContractContent">
        {$content}
    </div><br>
    {if $isRenewal}
        <form id="contractForm" method="post" action="index.php?m=recurringBilling&mg-action=renewal" class="form well">
            <h4>{$MGLANG->T('title','renewContract')}</h4>
            <input hidden name="contractid" value="{$contractid}"/>
            <div class="col-lg-12">
                <div class="form-group">
                    <input type="checkbox" id="confirm" name="confirm">
                    <label for="confirm" class="control-label" style="">{$MGLANG->T('label','confirm')}</label>
                    <div class="validationError col-lg-12" style="display: none"><span class="text-danger">{$MGLANG->T('validation','confirm')}</span></div>
                </div>
            </div>

            <div class="text-center">
                <button name="renewal" value="accept" class="btn btn-sm btn-success">{$MGLANG->T('button','renewcontract')}</button>
                <button name="renewal" value="decline" class="btn btn-sm btn-danger">{$MGLANG->T('button','cancelcontract')}</button>
            </div>
        </form>
    {/if}
    <a class="btn btn-default" href="index.php?m=recurringBilling">{$MGLANG->T('button','back')}</a>
</div>

{literal}
<script type="text/javascript">
    function validate(event)
    {
        $(".validationError").hide();
        
        var confirm = jQuery("input[name='confirm']");
        if(! (confirm).is(":checked"))
        {
            event.preventDefault();
            $(".validationError").show();
        }
    }
    
    jQuery("button[value='accept']").on("click", function(e){
        validate(e);
    });
    
</script>
{/literal}