{literal}
    <style>
        .canBeRenew{
            background-color: #fae3e3;
        }
        .help-inline {
            display: inline;
            font-size: 12px;
            color: #737373;
        }
        .legend-renewal {
            background: #fae3e3;
            width: 20px;
            height: 20px;
            margin-right: 10px;
        }
    </style>
{/literal}

<div class="col-lg-12" id="MGItems">
    <h3>{$MGLANG->T('title','contracts')}</h3><br>
    <table class="table table-hover">
        <thead>
            <tr>
                <th>{$MGLANG->T('item','id','label')}</th>
                <th>{$MGLANG->T('item','service','label')}</th>
                <th>{$MGLANG->T('item','amount','label')}</th>
                <th>{$MGLANG->T('item','signdate','label')}</th>
                <th>{$MGLANG->T('item','length','label')}</th>
                <th>{$MGLANG->T('item','enddate','label')}</th>
                <th>{$MGLANG->T('item','status','label')}</th>
                <th>{$MGLANG->T('item','actions','label')}</th>
                <th></th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            {foreach from=$data item=entry}
                <tr {if $entry.canBeRenew}class="canBeRenew"{/if}>
                    <td>{$entry.id}</td>
                    <td>{$entry.service}</td>
                    <td>{$entry.amount}</td>
                    <td>{$entry.date}</td>
                    <td>{$entry.length}</td>
                    <td>{$entry.enddate}</td>
                    <td>{$entry.status}</td>
                    <td style="float: left"> <a class="btn btn-sm btn-primary" href="index.php?m=recurringBilling&mg-action=details&id={$entry.id}">{$MGLANG->T('button','details')}</a> </td>
                    {if $entry.documentLink}
                        <td style="float: left"> <a target="_blank" class="btn btn-sm btn-info" href="recurringbilling.php{$entry.documentLink}">{$MGLANG->T('button','viewDocument')}</a> </td>
                    {else}
                        <td style="float: left"> <a target="_blank" class="btn btn-sm btn-info" href="recurringbilling.php?contractid={$entry.id}">{$MGLANG->T('button','viewDocument')}</a> </td>
                    {/if}
                    <td style="float: left"> <a class="btn btn-sm btn-default" href="index.php?m=recurringBilling&mg-action=history&id={$entry.id}">{$MGLANG->T('button','history')}</a> </td>        
                </tr>
            {foreachelse}
                <tr><td colspan="8" style="text-align: center;">{$MGLANG->T('table','noResults')}</td></tr>
                {/foreach}
        </tbody>
    </table>

    <div class="legend-renewal pull-left"></div> - <span class="help-inline">{$MGLANG->T('table', 'legend', 'renewal')}</span>
</div>
