<div class="col-lg-12" id="MGItems">
    <h3>{$MGLANG->T('title','history')}</h3><br>
    <table class="table table-hover">
        <thead>
            <tr>
                <th style="">{$MGLANG->T('item','signdate','label')}</th>
                <th style="width: 10%">{$MGLANG->T('item','actions','label')}</th>
            </tr>
        </thead>
        <tbody>
            {foreach from=$data item=entry}
                <tr>
                    <td>{$entry->date}</td>
                    <td>
                        <a target="_blank" class="btn btn-info" href="recurringbilling.php?entryid={$entry->id}">{$MGLANG->T('button','viewPDF')}</a>
                    </td>
                </tr>
            {foreachelse}
                <tr><td colspan="2" style="text-align: center;">{$MGLANG->T('table','noHistory')}</td></tr>
            {/foreach}
        </tbody>
    </table>
    <a class="btn btn-default" href="index.php?m=recurringBilling">{$MGLANG->T('button','back')}</a>
</div>