<?php

$_LANG['token'] = ', Error Token:';
$_LANG['generalError'] = 'Something has gone wrong. Check the logs and contact the administrator';

$_LANG['addonAA']['datatables']['next'] = 'Next';
$_LANG['addonAA']['datatables']['previous'] = 'Next';
$_LANG['addonAA']['datatables']['zeroRecords'] = 'Nothing to display';

$_LANG['validationErrors']['emptyField'] = 'Field cannot be empty';

// Recurring Billing 1.0.0

// Pages
$_LANG['addonAA']['pagesLabels']['contractContent']['label'] = 'Contract Content';
$_LANG['addonAA']['pagesLabels']['dashboard']['label'] = 'Dashboard';
$_LANG['addonAA']['pagesLabels']['contractTemplates']['label'] = 'Contract Templates';
$_LANG['addonAA']['pagesLabels']['signedContracts']['label'] = 'Signed Contracts';
$_LANG['addonAA']['pagesLabels']['signTypes']['label'] = 'Sign Types';
$_LANG['addonAA']['pagesLabels']['logs']['label'] = 'Logs';
$_LANG['addonAA']['pagesLabels']['configuration']['label'] = 'Configuration';
$_LANG['addonAA']['pagesLabels']['utilities']['label'] = 'Utilities';
$_LANG['addonAA']['pagesLabels']['utilities']['configuration'] = 'Configuration';
$_LANG['addonAA']['pagesLabels']['utilities']['logs'] = 'Logs';
$_LANG['addonAA']['pagesLabels']['utilities']['integrationCode'] = 'Integration Code';
$_LANG['addonAA']['pagesLabels']['documentation']['label'] = 'Documentation';

//Dashboard
$_LANG['addonAA']['dashboard']['title']['contracts'] = 'Recently Signed Contracts';
$_LANG['addonAA']['dashboard']['item']['id']['label'] = '#';
$_LANG['addonAA']['dashboard']['item']['client']['label'] = 'Client';
$_LANG['addonAA']['dashboard']['item']['service']['label'] = 'Service';
$_LANG['addonAA']['dashboard']['item']['amount']['label'] = 'Amount';
$_LANG['addonAA']['dashboard']['item']['signDate']['label'] = 'Signing Date';
$_LANG['addonAA']['dashboard']['title']['endedContracts'] = 'Recently Ended Contracts';
$_LANG['addonAA']['dashboard']['item']['endDate']['label'] = 'Ending Date';
$_LANG['addonAA']['dashboard']['label']['noContracts'] = 'There Are No Contracts Yet';
$_LANG['addonAA']['dashboard']['label']['noEndedContracts'] = 'There Are No Ended Contracts Yet';
$_LANG['addonAA']['dashboard']['title']['latestLogs'] = 'Logs';
$_LANG['addonAA']['dashboard']['item']['contract']['label'] = 'Contract';
$_LANG['addonAA']['dashboard']['item']['user']['label'] = 'User';
$_LANG['addonAA']['dashboard']['item']['log']['label'] = 'Log';
$_LANG['addonAA']['dashboard']['item']['date']['label'] = 'Date';
$_LANG['addonAA']['dashboard']['label']['noLogs'] = 'No Logs';
$_LANG['addonAA']['dashboard']['breadcrumb']['dashboard'] = 'Dashboard';
$_LANG['addonAA']['dashboard']['title']['contractIncome'] = 'Contracts Summary';
$_LANG['addonAA']['dashboard']['label']['activeContracts'] = 'Active Contracts';
$_LANG['addonAA']['dashboard']['label']['finishedContracts'] = 'Finished Contracts';
$_LANG['addonAA']['dashboard']['label']['cancelledContracts'] = 'Cancelled Contracts';
$_LANG['addonAA']['dashboard']['label']['brokenContracts'] = 'Broken Contracts';
$_LANG['addonAA']['dashboard']['label']['monthlyRevenue'] = 'Current Monthly Revenue';
$_LANG['addonAA']['dashboard']['label']['yearlyRevenue'] = 'Current Yearly Revenue';
$_LANG['addonAA']['dashboard']['label']['estMonthlyRevenue'] = 'Estimated Monthly Revenue';
$_LANG['addonAA']['dashboard']['label']['estYearlyRevenue'] = 'Estimated Yearly Revenue';

$_LANG['addonAA']['dashboard']['guide']['step1'] = "The freshly activated module has to be configured, take the following steps to proceed:";
$_LANG['addonAA']['dashboard']['guide']['step2'] = "1.Set up a new contract by adding its content, it can be done under 'Contract Content' → 'Add New'.";
$_LANG['addonAA']['dashboard']['guide']['step3'] = "2.Create the template of a contract after moving to 'Contract Templates' → 'Add New'";
$_LANG['addonAA']['dashboard']['guide']['step4'] = "a. Fill out the required fields and press the 'Save' button.";
$_LANG['addonAA']['dashboard']['guide']['step5'] = "b. Specify contract details according to the product that you have chosen in the previous step.";
$_LANG['addonAA']['dashboard']['guide']['step6'] = "3. Set up the notifications and penalty for the contract breach, both can be found in the 'Configuration' section.";
$_LANG['addonAA']['dashboard']['guide']['step7'] = "For more info visit our";
$_LANG['addonAA']['dashboard']['guide']['wiki'] = "Wiki";
$_LANG['addonAA']['dashboard']['guide']['hide'] = "Hide Guide";

//Contract Content
$_LANG['addonAA']['contractContent']['item']['id']['label'] = 'ID';
$_LANG['addonAA']['contractContent']['item']['name']['label'] = 'Name';
$_LANG['addonAA']['contractContent']['item']['lastedit']['label'] = 'Edited On';
$_LANG['addonAA']['contractContent']['item']['actions']['label'] = 'Actions';
$_LANG['addonAA']['contractContent']['label']['name'] = 'Name';
$_LANG['addonAA']['contractContent']['label']['content'] = 'Content';
$_LANG['addonAA']['contractContent']['label']['merge_fields'] = 'Available Merge Fields';
$_LANG['addonAA']['contractContent']['modal']['deleteLabel'] = 'Delete Contract Content';
$_LANG['addonAA']['contractContent']['modal']['deleteDescription'] = 'Are you sure that you want to delete this item?';
$_LANG['addonAA']['contractContent']['modal']['delete'] = 'Delete';
$_LANG['addonAA']['contractContent']['modal']['close'] = 'Cancel';
$_LANG['addonAA']['contractContent']['button']['save'] = 'Save';
$_LANG['addonAA']['contractContent']['button']['back'] = 'Cancel';
$_LANG['addonAA']['contractContent']['button']['addNew'] = 'Add New';
$_LANG['addonAA']['contractContent']['success']['deleteItem'] = 'Item has been successfully deleted';
$_LANG['addonAA']['contractContent']['success']['addRows'] = 'Items has been successfully added';
$_LANG['addonAA']['contractContent']['success']['saveContent'] = 'Changes have been successfully saved';
$_LANG['addonAA']['contractContent']['label']['addLanguage'] = 'Add Language';
//Merge fields
$_LANG['addonAA']['contractContent']['group']['client'] = 'Client Fields';
$_LANG['addonAA']['contractContent']['group']['hosting'] = 'Hosting Fields';
$_LANG['addonAA']['contractContent']['group']['product'] = 'Product Fields';
$_LANG['addonAA']['contractContent']['group']['contract'] = 'Contract Fields';
$_LANG['addonAA']['contractContent']['groupitem']['client']['id'] = 'ID';
$_LANG['addonAA']['contractContent']['groupitem']['client']['firstname'] = 'First Name';
$_LANG['addonAA']['contractContent']['groupitem']['client']['lastname'] = 'Last Name';
$_LANG['addonAA']['contractContent']['groupitem']['client']['companyname'] = 'Company Name';
$_LANG['addonAA']['contractContent']['groupitem']['client']['email'] = 'Email';
$_LANG['addonAA']['contractContent']['groupitem']['client']['address1'] = 'Address Line 1';
$_LANG['addonAA']['contractContent']['groupitem']['client']['address2'] = 'Address Line 2';
$_LANG['addonAA']['contractContent']['groupitem']['client']['city'] = 'City';
$_LANG['addonAA']['contractContent']['groupitem']['client']['state'] = 'State';
$_LANG['addonAA']['contractContent']['groupitem']['client']['postcode'] = 'Postcode';
$_LANG['addonAA']['contractContent']['groupitem']['client']['country'] = 'Country';
$_LANG['addonAA']['contractContent']['groupitem']['client']['phonenumber'] = 'Phone Number';
$_LANG['addonAA']['contractContent']['groupitem']['client']['status'] = 'Status';
$_LANG['addonAA']['contractContent']['groupitem']['client']['language'] = 'Language';
$_LANG['addonAA']['contractContent']['groupitem']['client']['tax_id'] = 'Tax ID';
$_LANG['addonAA']['contractContent']['groupitem']['product']['id'] = 'ID';
$_LANG['addonAA']['contractContent']['groupitem']['product']['type'] = 'Type';
$_LANG['addonAA']['contractContent']['groupitem']['product']['name'] = 'Product Name';
$_LANG['addonAA']['contractContent']['groupitem']['product']['description'] = 'Description';
$_LANG['addonAA']['contractContent']['groupitem']['product']['paytype'] = 'Payment Type';
$_LANG['addonAA']['contractContent']['groupitem']['product']['servertype'] = 'Server Type';
$_LANG['addonAA']['contractContent']['groupitem']['hosting']['id'] = 'ID';
$_LANG['addonAA']['contractContent']['groupitem']['hosting']['userid'] = 'User ID';
$_LANG['addonAA']['contractContent']['groupitem']['hosting']['regdate'] = 'Registration Date';
$_LANG['addonAA']['contractContent']['groupitem']['hosting']['domain'] = 'Domain';
$_LANG['addonAA']['contractContent']['groupitem']['hosting']['paymentmethod'] = 'Payment Method';
$_LANG['addonAA']['contractContent']['groupitem']['hosting']['firstpaymentamount'] = 'First Payment Amount';
$_LANG['addonAA']['contractContent']['groupitem']['hosting']['amount'] = 'Amount';
$_LANG['addonAA']['contractContent']['groupitem']['hosting']['billingcycle'] = 'Billing Cycle';
$_LANG['addonAA']['contractContent']['groupitem']['hosting']['nextduedate'] = 'Next Due Date';
$_LANG['addonAA']['contractContent']['groupitem']['hosting']['nextinvoicedate'] = 'Next Invoice Date';
$_LANG['addonAA']['contractContent']['groupitem']['hosting']['domainstatus'] = 'Domain Status';
$_LANG['addonAA']['contractContent']['groupitem']['hosting']['notes'] = 'Notes';
$_LANG['addonAA']['contractContent']['groupitem']['hosting']['ns1'] = 'Nameserver 1';
$_LANG['addonAA']['contractContent']['groupitem']['hosting']['ns2'] = 'Nameserver 2';
$_LANG['addonAA']['contractContent']['groupitem']['hosting']['lastupdate'] = 'Last Update Date';
$_LANG['addonAA']['contractContent']['groupitem']['contract']['length_months_only'] = 'Contract Length (In Months)';
$_LANG['addonAA']['contractContent']['groupitem']['contract']['length_full'] = 'Contract Length';
$_LANG['addonAA']['contractContent']['groupitem']['contract']['promo'] = 'Promo';
$_LANG['addonAA']['contractContent']['groupitem']['contract']['penalty'] = 'Penalty';
$_LANG['addonAA']['contractContent']['groupitem']['contract']['promo_amount'] = 'Promo Price';
$_LANG['addonAA']['contractContent']['groupitem']['contract']['penalty_amount'] = 'Penalty Price';
$_LANG['addonAA']['contractContent']['groupitem']['contract']['days_to_cancel'] = 'Days To Cancel';
$_LANG['addonAA']['contractContent']['groupitem']['contract']['total_price'] = 'Total Contract Price';
$_LANG['addonAA']['contractContent']['groupitem']['contract']['payinterval'] = 'Payment Interval';
$_LANG['addonAA']['contractContent']['groupitem']['contract']['setup_fee'] = 'Product Setup Fee';
$_LANG['addonAA']['contractContent']['error']['deleteTemplates'] = 'Please remove or change all templates that use this content in order to delete it.';
$_LANG['addonAA']['contractContent']['error']['empty'] = "Name cannot be empty";
$_LANG['addonAA']['contractContent']['tooltip']['edit'] = 'Edit';
$_LANG['addonAA']['contractContent']['tooltip']['delete'] = 'Delete';
$_LANG['addonAA']['contractContent']['breadcrumb']['contractContent'] = 'Contract Content';
$_LANG['addonAA']['contractContent']['breadcrumb']['editContent'] = 'Edit Content';
$_LANG['addonAA']['contractContent']['description']['tutorial'] = "Compose a draft of your contract content. Below you can find an advanced edition tool which will help you prepare a suitable outline for your contract. Use available merge fields to automatically add data relevant to every client and product in your offer. Once you finish, move to the 'Contract Templates' section to assign the template to a product and set up pricing options. Such a ready-made contract will be later on available for your clients to accept.";

//Contract Templates
$_LANG['addonAA']['contractTemplates']['item']['id']['label'] = 'ID';
$_LANG['addonAA']['contractTemplates']['item']['name']['label'] = 'Contract Name';
$_LANG['addonAA']['contractTemplates']['item']['contentname']['label'] = 'Content Name';
$_LANG['addonAA']['contractTemplates']['item']['product']['label'] = 'Product Name';
$_LANG['addonAA']['contractTemplates']['item']['allowcancellation']['label'] = 'Cancellation';
$_LANG['addonAA']['contractTemplates']['item']['actions']['label'] = 'Actions';
$_LANG['addonAA']['contractTemplates']['button']['save'] = 'Save';
$_LANG['addonAA']['contractTemplates']['button']['addNew'] = 'Add New';
$_LANG['addonAA']['contractTemplates']['modal']['deleteLabel'] = 'Delete Contract Template';
$_LANG['addonAA']['contractTemplates']['modal']['deleteDescription'] = 'Are you sure that you want to delete this template?';
$_LANG['addonAA']['contractTemplates']['modal']['delete'] = 'Delete';
$_LANG['addonAA']['contractTemplates']['modal']['close'] = 'Cancel';
$_LANG['addonAA']['contractTemplates']['success']['addRows'] = 'Item has been successfully added';
$_LANG['addonAA']['contractTemplates']['success']['deleteItem'] = 'Template has been successfully deleted';
//edit
$_LANG['addonAA']['contractTemplates']['title']['template'] = 'Contract Template';
$_LANG['addonAA']['contractTemplates']['title']['cycles'] = 'Contract Details';
$_LANG['addonAA']['contractTemplates']['title']['descriptions'] = 'Explanations';
$_LANG['addonAA']['contractTemplates']['label']['name'] = 'Template Name';
$_LANG['addonAA']['contractTemplates']['label']['contentname'] = 'Content';
$_LANG['addonAA']['contractTemplates']['label']['product'] = 'Product/Service';
$_LANG['addonAA']['contractTemplates']['label']['promotype'] = 'Promo Type';
$_LANG['addonAA']['contractTemplates']['label']['penaltytype'] = 'Penalty Type';
$_LANG['addonAA']['contractTemplates']['label']['contractrenewal'] = 'Renewal Type';
$_LANG['addonAA']['contractTemplates']['label']['allowcancellation'] = 'Cancellation';
$_LANG['addonAA']['contractTemplates']['label']['allowed'] = 'Allowed';
$_LANG['addonAA']['contractTemplates']['label']['prohibited'] = 'Prohibited';
$_LANG['addonAA']['contractTemplates']['option']['none'] = 'Renew Without Contract';
$_LANG['addonAA']['contractTemplates']['option']['cancel'] = 'Cancel';
$_LANG['addonAA']['contractTemplates']['option']['renew'] = 'Renew With Contract';
$_LANG['addonAA']['contractTemplates']['option']['fixed'] = 'Fixed';
$_LANG['addonAA']['contractTemplates']['option']['percentage'] = 'Percentage';
$_LANG['addonAA']['contractTemplates']['option']['no_results'] = '--No results--';
$_LANG['addonAA']['contractTemplates']['success']['saveTemplate'] = 'Contract template has been successfully saved';
$_LANG['addonAA']['contractTemplates']['success']['saveTemplateSkipped'] = 'Contract template has been saved, but some settings were skipped due to existing duplicates.';
$_LANG['addonAA']['contractTemplates']['cycle']['monthly'] = 'Meses';
$_LANG['addonAA']['contractTemplates']['cycle']['quarterly'] = 'Trimestral';
$_LANG['addonAA']['contractTemplates']['cycle']['semiannually'] = 'Semestral';
$_LANG['addonAA']['contractTemplates']['cycle']['annually'] = 'Anual';
$_LANG['addonAA']['contractTemplates']['cycle']['biennially'] = 'Biennially';
$_LANG['addonAA']['contractTemplates']['cycle']['triennially'] = 'Triennially';
$_LANG['addonAA']['contractTemplates']['label']['type'] = 'Sign Type';
$_LANG['addonAA']['contractTemplates']['label']['length'] = 'Length';
$_LANG['addonAA']['contractTemplates']['label']['promo'] = 'Promo Amount';
$_LANG['addonAA']['contractTemplates']['label']['signtype'] = 'Sign Type';
$_LANG['addonAA']['contractTemplates']['label']['penalty'] = 'Penalty Amount';
$_LANG['addonAA']['contractTemplates']['label']['total'] = 'Total Contract Price';
$_LANG['addonAA']['contractTemplates']['label']['promoAmount'] = 'Price After Promo';
$_LANG['addonAA']['contractTemplates']['label']['totalPenalty'] = 'Total Penalty';
$_LANG['addonAA']['contractTemplates']['label']['contract'] = 'Contract(s)';
$_LANG['addonAA']['contractTemplates']['label']['amount'] = 'Amount: ';
$_LANG['addonAA']['contractTemplates']['customfield']['name'] = 'Contract Length';
$_LANG['addonAA']['contractTemplates']['checkbox']['allowSkipContract'] = 'Select to permit orders without a contract';
$_LANG['addonAA']['contractTemplates']['button']['back'] = 'Cancel';
$_LANG['addonAA']['contractTemplates']['button']['otherCurrencies'] = 'Copy To Other Currencies';
$_LANG['addonAA']['contractTemplates']['error']['empty'] = "Name cannot be empty";
$_LANG['addonAA']['contractTemplates']['error']['productAssigned'] = 'Product is already assigned to the configuration';
$_LANG['addonAA']['contractTemplates']['tooltip']['edit'] = 'Edit';
$_LANG['addonAA']['contractTemplates']['tooltip']['remove'] = 'Delete';
$_LANG['addonAA']['contractTemplates']['tooltip']['noProducts'] = 'All recurring products already have contracts assigned';
$_LANG['addonAA']['contractTemplates']['tooltip']['add'] = 'Add';
$_LANG['addonAA']['contractTemplates']['tooltip']['delete'] = 'Delete';
$_LANG['addonAA']['contractTemplates']['breadcrumb']['contractTemplates'] = 'Contract Templates';
$_LANG['addonAA']['contractTemplates']['breadcrumb']['editContractTemplate'] = 'Edit Contract Template';
$_LANG['addonAA']['contractTemplates']['label']['trialPeriod'] = 'Trial Period';
$_LANG['addonAA']['contractTemplates']['label']['daysToCancel'] = 'Days To Cancel';
$_LANG['addonAA']['contractTemplates']['label']['noticeTime'] = 'Notice Time';
$_LANG['addonAA']['contractTemplates']['item']['length'] = 'Length';
$_LANG['addonAA']['contractTemplates']['description']['length'] = 'a time period of contract validity.';
$_LANG['addonAA']['contractTemplates']['item']['promoAmount'] = 'Promo Amount';
$_LANG['addonAA']['contractTemplates']['description']['promoAmount'] = 'a discount value applicable to a product, dependent on the chosen Promo Type.';
$_LANG['addonAA']['contractTemplates']['item']['penaltyAmount'] = 'Penalty Amount';
$_LANG['addonAA']['contractTemplates']['description']['penaltyAmount'] = 'a penalty value applicable in case of a contract breach, dependent on the chosen Penalty Type.';
$_LANG['addonAA']['contractTemplates']['item']['daysToCancel'] = 'Days To Cancel';
$_LANG['addonAA']['contractTemplates']['description']['daysToCancel'] = 'a number of days before termination of a contract, in case the contract can be cancelled without the penalty.';
$_LANG['addonAA']['contractTemplates']['item']['period'] = 'Trial Period';
$_LANG['addonAA']['contractTemplates']['description']['period'] = 'a number of days when a client may terminate a contract without the penalty, starting from the contract confirmation.';
$_LANG['addonAA']['contractTemplates']['item']['total'] = 'Total Contract Price';
$_LANG['addonAA']['contractTemplates']['description']['total'] = 'a sum of all payments made throughout a full period of contract validity.';
$_LANG['addonAA']['contractTemplates']['item']['amount'] = 'Price After Promo';
$_LANG['addonAA']['contractTemplates']['description']['amount'] = 'a due price for a product/service after the discount application.';
$_LANG['addonAA']['contractTemplates']['item']['totalPenalty'] = 'Total Penalty';
$_LANG['addonAA']['contractTemplates']['description']['totalPenalty'] = 'a total charge imposed on a client upon a contract breach.';
$_LANG['addonAA']['contractTemplates']['item']['noticeTime'] = 'Notice Time';
$_LANG['addonAA']['contractTemplates']['description']['noticeTime'] = 'a number of days before the contract expiration, when an email notification of renewal options should be sent to a client.';

$_LANG['addonAA']['contractTemplates']['item']['renewalTypes'] = 'Renewal Types:';
$_LANG['addonAA']['contractTemplates']['item']['none'] = 'Renew Without Contract';
$_LANG['addonAA']['contractTemplates']['description']['none'] = 'a client maintains the access to a product/service after the contract expiration.';
$_LANG['addonAA']['contractTemplates']['item']['cancel'] = 'Cancel';
$_LANG['addonAA']['contractTemplates']['description']['cancel'] = 'a product/service is terminated upon the contract expiration.';
$_LANG['addonAA']['contractTemplates']['item']['renew'] = 'Renew With Contract';
$_LANG['addonAA']['contractTemplates']['description']['renew'] = 'a client has to confirm a contract to continue using a product/service. If not, a product/service is terminated upon the contract expiration.';

$_LANG['time']['month'] = 'Mês';
$_LANG['time']['year'] = 'Ano';
$_LANG['time']['pluralSuffixMonths'] = 's';
$_LANG['time']['pluralSuffixYears'] = 's';

//Signed Contracts
$_LANG['addonAA']['signedContracts']['item']['id']['label'] = 'ID';
$_LANG['addonAA']['signedContracts']['item']['client']['label'] = 'Client';
$_LANG['addonAA']['signedContracts']['item']['service']['label'] = 'Product/Service';
$_LANG['addonAA']['signedContracts']['item']['amount']['label'] = 'Amount';
$_LANG['addonAA']['signedContracts']['item']['signdate']['label'] = 'Signing Date';
$_LANG['addonAA']['signedContracts']['item']['length']['label'] = 'Length';
$_LANG['addonAA']['signedContracts']['item']['enddate']['label'] = 'Ending Date';
$_LANG['addonAA']['signedContracts']['item']['status']['label'] = 'Status';
$_LANG['addonAA']['signedContracts']['item']['actions']['label'] = 'Actions';
$_LANG['addonAA']['signedContracts']['modal']['cancelLabel'] = 'Cancel Contract';
$_LANG['addonAA']['signedContracts']['modal']['cencelDescription'] = 'Are you sure that you want to cancel this contract?';
$_LANG['addonAA']['signedContracts']['modal']['cancel'] = 'Proceed';
$_LANG['addonAA']['signedContracts']['modal']['close'] = 'Close';
$_LANG['addonAA']['signedContracts']['success']['cancelContract'] = 'Contract has been successfully cancelled';
$_LANG['addonAA']['signedContracts']['cycle']['Monthly'] = 'Monthly';
$_LANG['addonAA']['signedContracts']['cycle']['Quarterly'] = 'Quarterly';
$_LANG['addonAA']['signedContracts']['cycle']['Semi-Annually'] = 'Semi-Annually';
$_LANG['addonAA']['signedContracts']['cycle']['Annually'] = 'Annually';
$_LANG['addonAA']['signedContracts']['cycle']['Biennially'] = 'Biennially';
$_LANG['addonAA']['signedContracts']['cycle']['Triennially'] = 'Triennially';
//edit
$_LANG['addonAA']['signedContracts']['label']['content'] = 'Contract';
$_LANG['addonAA']['signedContracts']['label']['history'] = 'History';
$_LANG['addonAA']['signedContracts']['button']['back'] = 'Cancel';
$_LANG['addonAA']['signedContracts']['button']['save'] = 'Save';
$_LANG['addonAA']['signedContracts']['button']['viewPDF'] = 'View PDF';
$_LANG['addonAA']['signedContracts']['button']['revert'] = 'Restore';
$_LANG['addonAA']['signedContracts']['text']['noEditHistory'] = 'There is no edit history of this contract';
$_LANG['addonAA']['signedContracts']['success']['saveContent'] = 'Contract has been successfully saved';
$_LANG['addonAA']['signedContracts']['success']['restore'] = 'Contract has been successfully restored';
$_LANG['addonAA']['signedContracts']['label']['admin'] = 'Modified By:';
$_LANG['addonAA']['signedContracts']['modal']['restoreLabel'] = 'Restore From History';
$_LANG['addonAA']['signedContracts']['modal']['restoreDescription'] = 'Are you sure that you want to restore this content from history to the currently displayed contract? This will overwrite the current contract content.';
$_LANG['addonAA']['signedContracts']['modal']['restore'] = 'Restore';
$_LANG['addonAA']['signedContracts']['label']['client'] = 'Client';
$_LANG['addonAA']['signedContracts']['label']['service'] = 'Product';
$_LANG['addonAA']['signedContracts']['label']['amount'] = 'Amount';
$_LANG['addonAA']['signedContracts']['label']['signDate'] = 'Signing Date';
$_LANG['addonAA']['signedContracts']['label']['length'] = 'Length';
$_LANG['addonAA']['signedContracts']['label']['endDate'] = 'Ending Date';
$_LANG['addonAA']['signedContracts']['label']['status'] = 'Status';
$_LANG['addonAA']['signedContracts']['error']['wrongData'] = 'Please provide the correct data';
$_LANG['addonAA']['signedContracts']['notify']['change'] = 'If you decide to change the Amount, Length or Signing Date fields, please do not forget to introduce such changes in your contract content as well.';

$_LANG['addonAA']['signedContracts']['status']['active'] = 'Active';
$_LANG['addonAA']['signedContracts']['status']['cancelled'] = 'Cancelled';
$_LANG['addonAA']['signedContracts']['status']['broken'] = 'Broken';
$_LANG['addonAA']['signedContracts']['status']['finished'] = 'Finished';

$_LANG['addonAA']['signedContracts']['tooltip']['edit'] = 'Edit';
$_LANG['addonAA']['signedContracts']['tooltip']['cancel'] = 'Cancel Contract';

$_LANG['addonAA']['signedContracts']['breadcrumb']['signedContracts'] = 'Signed Contracts';
$_LANG['addonAA']['signedContracts']['breadcrumb']['editContract'] = 'Edit Contract';

//Sign Types
$_LANG['addonAA']['signTypes']['breadcrumb']['signTypes'] = 'Sign Types';
$_LANG['addonAA']['signTypes']['success']['save'] = 'Configuration has been saved';
$_LANG['addonAA']['signTypes']['success']['auth'] = 'Authorization completed successfully';
$_LANG['addonAA']['signTypes']['button']['save'] = 'Save';


$_LANG['addonAA']['signTypes']['DocuSign']['title']['template'] = "DocuSign";
$_LANG['addonAA']['signTypes']['DocuSign']['DocuSignIs Demo']['label'] = 'Demo';
$_LANG['addonAA']['signTypes']['DocuSign']['DocuSignIs Demo']['enabled'] = 'Enabled';
$_LANG['addonAA']['signTypes']['DocuSign']['DocuSignIs Demo']['disabled'] = 'Disabled';
$_LANG['addonAA']['signTypes']['DocuSign']['DocuSignClient ID']['Client ID'] = 'Client ID';
$_LANG['addonAA']['signTypes']['DocuSign']['DocuSignClient Secret']['Client Secret'] = 'Client Secret';
$_LANG['addonAA']['signTypes']['DocuSign']['DocuSignRedirect URL']['Redirect URL'] = 'Redirect URI';
$_LANG['addonAA']['signTypes']['DocuSign']['DocuSignClient Email']['Client Email'] = 'Client Email';
$_LANG['addonAA']['signTypes']['DocuSign']['Client Password']['label'] = 'Client Password'; 
$_LANG['addonAA']['signTypes']['Adobe']['title']['template'] = "Adobe Sign";
$_LANG['addonAA']['signTypes']['Adobe']['AdobeClient ID']['Client ID'] = 'Client ID';
$_LANG['addonAA']['signTypes']['Adobe']['AdobeClient Secret']['Client Secret'] = 'Client Secret';
$_LANG['addonAA']['signTypes']['Adobe']['AdobeRedirect URL']['Redirect URL'] = 'Redirect URI';
$_LANG['addonAA']['signTypes']['Adobe']['AdobeClient Email']['Client Email'] = 'Client Email';
$_LANG['addonAA']['signTypes']['File']['title']['template'] = "File Upload";
$_LANG['addonAA']['signTypes']['File']['FileMinimum file size']['Minimum file size'] = 'Minimum File Size';
$_LANG['addonAA']['signTypes']['File']['FileMaximum file size']['Maximum file size'] = 'Maximum File Size';
$_LANG['addonAA']['signTypes']['File']['FileMinimum file size']['description'] = "The units are given in bytes. If set to '0', validation will be disabled.";
$_LANG['addonAA']['signTypes']['File']['FileMaximum file size']['description'] = "The units are given in bytes. If set to '0', validation will be disabled.";

$_LANG['signTypes']['fileInfo'] = 'The customer signs the contract by uploading the properly signed file.';

$_LANG['signTypes']['select']['Adobe'] = "Adobe Sign";
$_LANG['signTypes']['select']['File'] = "File Upload";
$_LANG['signTypes']['select']['DocuSign'] = "DocuSign";
$_LANG['signTypes']['select']['Checkbox'] = "Checkbox";


//Logs
$_LANG['addonAA']['logs']['item']['id']['label'] = 'ID';
$_LANG['addonAA']['logs']['item']['contract']['label'] = 'Contract';
$_LANG['addonAA']['logs']['item']['user']['label'] = 'User';
$_LANG['addonAA']['logs']['item']['log']['label'] = 'Log';
$_LANG['addonAA']['logs']['item']['date']['label'] = 'Date';
$_LANG['addonAA']['logs']['breadcrumb']['logs'] = 'Logs';

//Configuration
$_LANG['addonAA']['configuration']['title']['notifications'] = 'Notifications';
$_LANG['addonAA']['configuration']['title']['penalty'] = 'Penalty';
$_LANG['addonAA']['configuration']['checkbox']['notify1'] = 'First Notification';
$_LANG['addonAA']['configuration']['checkbox']['notify2'] = 'Second Notification';
$_LANG['addonAA']['configuration']['checkbox']['notify3'] = 'Third Notification';
$_LANG['addonAA']['configuration']['text']['notify1time'] = 'Days To Contract End';
$_LANG['addonAA']['configuration']['text']['notify2time'] = 'Days To Contract End';
$_LANG['addonAA']['configuration']['text']['notify3time'] = 'Days To Contract End';
$_LANG['addonAA']['configuration']['text']['penalty'] = 'Penalty After (Days)';
$_LANG['addonAA']['configuration']['button']['save'] = 'Save';
$_LANG['addonAA']['configuration']['success']['save'] = 'Configuration has been saved';
$_LANG['addonAA']['configuration']['breadcrumb']['configuration'] = 'Configuration';
$_LANG['addonAA']['configuration']['title']['guide'] = 'Guide';
$_LANG['addonAA']['configuration']['text']['guide'] = 'Show Guide';
$_LANG['addonAA']['configuration']['tooltip']['notify1'] = 'Enable the first email notification';
$_LANG['addonAA']['configuration']['tooltip']['notify1time'] = 'Set how many days before a contract expiration date, the first notification to the customer should be sent.';
$_LANG['addonAA']['configuration']['tooltip']['notify2'] = 'Enable the second email notification';
$_LANG['addonAA']['configuration']['tooltip']['notify2time'] = 'Set how many days before a contract expiration date, the second notification to the customer should be sent.';
$_LANG['addonAA']['configuration']['tooltip']['notify3'] = 'Enable the third email notification';
$_LANG['addonAA']['configuration']['tooltip']['notify3time'] = 'Set how many days before a contract expiration date, the third notification to the customer should be sent.';
$_LANG['addonAA']['configuration']['tooltip']['penalty'] = 'Define how many days need to pass after an unpaid invoice has been created, to charge a user for the contract breach.';
$_LANG['addonAA']['configuration']['tooltip']['guide'] = "Guide will be visible every time an administrator enters the 'Dashboard' tab.";

//Integration Code
$_LANG['addonAA']['integrationCode']['breadcrumb']['integrationCode'] = 'Integration Code';
$_LANG['addonAA']['integrationCode']['integration']['whmcs7']['11'] = 'Module requires alterations to your order form template.';
$_LANG['addonAA']['integrationCode']['integration']['whmcs7']['12'] = 'Open your currently used order form and follow the instructions.';
$_LANG['addonAA']['integrationCode']['integration']['whmcs7']['13a'] = 'You will find it in: yourWHMCS/templates/orderforms/';
$_LANG['addonAA']['integrationCode']['integration']['whmcs7']['13b'] = '/checkout.tpl';
$_LANG['addonAA']['integrationCode']['integration']['whmcs7']['13c'] = 'Please note that if you are using a different order template, the location of the integration code might be different.';
$_LANG['addonAA']['integrationCode']['integration']['whmcs7']['14'] = 'Find the line:';
$_LANG['addonAA']['integrationCode']['integration']['whmcs7']['15'] = 'Insert the below code snippet above this line:';
$_LANG['addonAA']['integrationCode']['integration']['whmcs6']['11'] = 'Module requires alterations to your order form template.';
$_LANG['addonAA']['integrationCode']['integration']['whmcs6']['12'] = '1. Open your currently used order form and follow the instructions.';
$_LANG['addonAA']['integrationCode']['integration']['whmcs6']['13a'] = 'You will find it in: yourWHMCS/templates/orderforms/';
$_LANG['addonAA']['integrationCode']['integration']['whmcs6']['13b'] = '/checkout.tpl';
$_LANG['addonAA']['integrationCode']['integration']['whmcs6']['13c'] = 'Please note that if you are using different order template, the location of the integration code might be different.';
$_LANG['addonAA']['integrationCode']['integration']['whmcs6']['14'] = 'Find the line:';
$_LANG['addonAA']['integrationCode']['integration']['whmcs6']['15'] = 'Insert the below code snippet above this line:';
$_LANG['addonAA']['integrationCode']['integration']['whmcs6']['16'] = '2. Open your currently used template and follow the instructions.';
$_LANG['addonAA']['integrationCode']['integration']['whmcs6']['17'] = 'You will find it in: yourWHMCS/templates/yourTemplate/header.tpl';
$_LANG['addonAA']['integrationCode']['integration']['whmcs6']['18'] = 'Find the line:';
$_LANG['addonAA']['integrationCode']['integration']['whmcs6']['19'] = 'Insert the following code snippet below this line:';

$_LANG['addonAA']['integrationCode']['integration']['whmcs5']['0'] = 'Follow the below instruction to properly insert the required integration codes.';

$_LANG['addonAA']['integrationCode']['integration']['whmcs5']['11'] = '1. Open your currently used order form.';
$_LANG['addonAA']['integrationCode']['integration']['whmcs5']['12'] = 'You will find it in: yourWHMCS/templates/orderforms/yourOrderForm/viewcart.tpl';
$_LANG['addonAA']['integrationCode']['integration']['whmcs5']['13'] = 'Find the line:';
$_LANG['addonAA']['integrationCode']['integration']['whmcs5']['14'] = 'Insert the below code snippet above this line:';

$_LANG['addonAA']['integrationCode']['integration']['whmcs5']['21'] = '2. Open your currently used template and follow the instructions.';
$_LANG['addonAA']['integrationCode']['integration']['whmcs5']['22'] = 'You will find it in: yourWHMCS/templates/yourTemplate/header.tpl';
$_LANG['addonAA']['integrationCode']['integration']['whmcs5']['23'] = 'Find the line:';
$_LANG['addonAA']['integrationCode']['integration']['whmcs5']['24'] = 'Insert the following code snippet above this line:';

$_LANG['error']['productEdit']   = "Product is in use by Recurring Billing Extended and pay type has be set to recurring.";
$_LANG['error']['productEditRB'] = "Assigned product's pay type has be set to recurring. Please check your configuration.";
$_LANG['error']['error'] = "Error";
/*
 * 
 * CLIENT AREA
 * 
 */

//Contracts view
$_LANG['addonCA']['navbarLabel'] = 'Meus Contratos';
$_LANG['addonCA']['home']['title']['contracts'] = 'Contratos de Serviço';
$_LANG['addonCA']['home']['title']['history'] = 'Historico do contrato';
$_LANG['addonCA']['home']['title']['details'] = 'Detalhes do contrato';
$_LANG['addonCA']['home']['title']['renewContract'] = 'Renovar contrato';
$_LANG['addonCA']['home']['item']['id']['label'] = 'ID';
$_LANG['addonCA']['home']['item']['service']['label'] = 'Produto/Serviço';
$_LANG['addonCA']['home']['item']['amount']['label'] = 'Valor';
$_LANG['addonCA']['home']['item']['signdate']['label'] = 'Data de Assinatura';
$_LANG['addonCA']['home']['item']['length']['label'] = 'Duração';
$_LANG['addonCA']['home']['item']['enddate']['label'] = 'Data Final';
$_LANG['addonCA']['home']['item']['status']['label'] = 'Status';
$_LANG['addonCA']['home']['item']['actions']['label'] = 'Ações';
$_LANG['addonCA']['home']['button']['viewDocument'] = 'Ver documento';
$_LANG['addonCA']['home']['button']['history'] = 'Histórico';
$_LANG['addonCA']['home']['button']['details'] = 'Ver contrato';
$_LANG['addonCA']['home']['button']['renewcontract'] = 'Renovara';
$_LANG['addonCA']['home']['button']['cancelcontract'] = 'Cancelar';
$_LANG['addonCA']['home']['table']['noResults'] = '--Sem resultados--';
$_LANG['addonCA']['home']['table']['noHistory'] = '--Sem entradas de histórico--';
$_LANG['addonCA']['home']['table']['legend']['renewal'] = 'Os contratos marcados com esta cor estão prestes a terminar. Visualize a página de detalhes do contrato para renová-lo.';
$_LANG['addonCA']['home']['status']['active'] = 'Ativo';
$_LANG['addonCA']['home']['status']['cancelled'] = 'Cancelado';
$_LANG['addonCA']['home']['status']['broken'] = 'Interompido';
$_LANG['addonCA']['home']['status']['finished'] = 'Finalisado';
$_LANG['addonCA']['home']['button']['back'] = 'Voltar';
$_LANG['addonCA']['home']['label']['confirm'] = 'Aceite os Contratos e termos';
$_LANG['addonCA']['home']['description']['confirm'] = "Marque esta caixa para aceitar o contrato's e termos";
$_LANG['addonCA']['home']['validation']['confirm'] = "Você deve aceitar os Termos contrato(s) para prosseguir.";
$_LANG['addonCA']['home']['info']['contracthasbeenrenew'] = 'Obrigado por renovar o contrato. Os serviços prestados ao abrigo deste contrato serão prolongados pela duração do contrato. Você pode visualizar uma nova fatura clicando neste link:';
$_LANG['addonCA']['home']['info']['contracthasbeencancel'] = 'Você cancelou o contrato atual. Todos os serviços relacionados a este contrato serão desativados ao final do ciclo de faturamento.';

//Client Area Contract
$_LANG['optionname']['contract']      = 'Duração do Contrato';
$_LANG['contract']['noContracts']     = "--Ainda não há contratos--";
$_LANG['contract']['invoiceText']     = "Multa por quebra antecipada de contrato para: %group% - %name% (%domain%) - ID do serviço: %id%";
$_LANG['option']['doNotWantContract'] = "Não quero assinar contrato";
$_LANG['button']['viewContract']      = "Ver contrato";
$_LANG['button']['downloadContract']  = "Baixar contrato";
$_LANG['button']['acceptedContract']  = "Contrato Aceito";
$_LANG['button']['acceptContract']    = "Aceitar Contrato";
$_LANG['button']['upload']            = "Upload do contrato";
$_LANG['checkbox']['acceptContract']  = "Aceite os Termos de Uso";
$_LANG['error']['acceptContract']     = "Você deve aceitar os Termos de Uso do contrato para prosseguir.";
$_LANG['error']['valideFileContract'] = "Por favor carregue o documento assinado";
$_LANG['error']['acceptAllContracts'] = "Você deve aceitar os Termos de Uso do contrato para prosseguir.";
$_LANG['customfield']['signDate']     = 'Data de Assinatura do Contrato';
$_LANG['customfield']['length']       = 'Duração do Contrato';
$_LANG['fatalError']['unforgivable']  = "Alguns dados não são preenchidos";
$_LANG['customfield']['endDate']      = 'Contrato Termina';
$_LANG['sidebar']['contracts']        = 'Contratos de Serviço';
$_LANG['info']['break'] = 'Atenção! Este produto foi atribuído a um contrato assinado e a solicitação de cancelamento cobrará de você %price% como uma penalidade.';
$_LANG['pagetitle']['viewPDF'] = "Ver PDF";
$_LANG['menutitle']['contractTitle'] = "Meus Contratos";
$_LANG['description']['tos'] = "Aceite os Termos de Uso do contrato para:";
$_LANG['description']['viewPDF'] = "Ver contrato";
$_LANG['header']['contractTitle'] = "Contratos";
$_LANG['logs']['newContractSigned'] = "Novo contrato assinado";
$_LANG['logs']['contractBroken'] = "Contrato rompido, o serviço foi cancelado";
$_LANG['logs']['contractCancel'] = "Contrato cancelado no período experimental, o serviço foi cancelado";
$_LANG['logs']['contractFinish'] = "Contrato finalizado com sucesso";

//Notification templates
$_LANG['template']['notification1'] = "First Notification About Ending Contract";
$_LANG['template']['notification2'] = "Second Notification About Ending Contract";
$_LANG['template']['notification3'] = "Third Notification About Ending Contract";
$_LANG['template']['notificationRenew']     = "Renewal Notification About Ending Contract";
$_LANG['template']['notificationCancel']    = "Termination Notification About Ending Contract";
$_LANG['template']['notificationNone']      = "Notification About Ending Contract";

//Client Area Cancellation Request
$_LANG['description']['cancellationotallowed'] = "Devido ao contrato ativo, você não pode solicitar o cancelamento deste serviço";

//..............
$_LANG['message']['checkEmail']                = "Verifique sua caixa de correio.";
$_LANG["fileInfo"]["error"]["type"]            = "Tipo de arquivo inválido.";
$_LANG["fileInfo"]["error"]["maxSize"]         = "O tamanho máximo de um arquivo enviado.";
$_LANG["fileInfo"]["error"]["minSize"]         = "O tamanho máximo de um arquivo enviado.";
$_LANG["fileInfo"]["error"]["maxSizeHtmlForm"] = "O arquivo enviado excede o MAX_FILE_SIZE diretiva especificada no formulário HTML.";
$_LANG["fileInfo"]["error"]["partially"]       = "O arquivo foi carregado apenas parcialmente.";
$_LANG["fileInfo"]["error"]["lost"]            = "Nenhum arquivo foi enviado.";
$_LANG["fileInfo"]["error"]["missingFolder"]   = "Faltando uma pasta temporária. Introduzido em PHP 5.0.3.";
$_LANG["fileInfo"]["error"]["failedWrite"]     = "Falha ao salvar o arquivo no disco. Introduzido em PHP 5.1.0.";
$_LANG["fileInfo"]["error"]["stopUpload"]      = "Uma extensão PHP interrompeu o upload do arquivo. O PHP não fornece uma maneira de identificar qual extensão causou a interrupção do upload do arquivo; examinando a lista de extensões carregadas com phpinfo() pode ajudar. Introduzido em PHP 5.2.0.";

/**
 * Version 1.2.5
 */
$_LANG['time']['months'] = 'Meses';
$_LANG['time']['years'] = 'Anos';

/**
 * v 1.2.4
 */
$_LANG['pagetitle']['pdfname'] = "doc";
$_LANG['addonCA']['home']['button']['viewPDF'] = 'Ver PDF';