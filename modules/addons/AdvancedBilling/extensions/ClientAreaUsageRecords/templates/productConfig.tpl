<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">{$MGLANG->T('Pricing for usage records')}</h3>
    </div>
    <div class="panel-body">
        {foreach from=$resources item=resource}
        <div class="row">
            {if $resource.pricing.enabled eq 'on'}
                <div class="col-md-5 text-right">{$resource.friendlyName}</div>
                <div class="col-md-7">{$currency.prefix}{$resource.pricing.price|number_format:'10'|rtrim:'0'|rtrim:'.'} {$currency.suffix} {$resource.pricing.unit}</div>
            {/if}
        </div>
        {/foreach}
    </div>
</div>