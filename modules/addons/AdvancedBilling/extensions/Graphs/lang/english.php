<?php

$_LANG['addonAA']['pagesLabels']['label']['graphs'] = 'Graphs';
$_LANG['addonAA']['Graphs']['Line Chart For'] = 'Line Chart For';

$_LANG['addonAA']['Graphs']['Product'] = 'Product';
$_LANG['addonAA']['Graphs']['Hosting'] = 'Hosting';
$_LANG['addonAA']['Graphs']['Please select product'] = 'Please select the product';
$_LANG['addonAA']['Graphs']['Resources'] = 'Resources';
$_LANG['addonAA']['Graphs']['Date Range'] = 'Date Range';

$_LANG['addonAA']['Graphs']['Product Name'] = 'Product Name';
$_LANG['addonAA']['Graphs']['Client Name'] = 'Client Name';
$_LANG['addonAA']['Graphs']['Status'] = 'Status';
$_LANG['addonAA']['Graphs']['Actions'] = 'Actions';
$_LANG['addonAA']['Graphs']['Account List'] = 'Account List';
$_LANG['addonAA']['Graphs']['Graph extension allows you to view graphs of usage records for every configured services in Advanced Billing'] = 'The Graphs extension allows you to view graphs of usage records for every configured service in the Advanced Billing module';
$_LANG['addonAA']['pagesLabels']['graphs']['details'] = 'Details';

$_LANG['addonAA']['Graphs']['Date From'] = 'Date From';
$_LANG['addonAA']['Graphs']['Date To'] = 'Date To';

$_LANG['addonAA']['Graphs']['No data to show']  =  'No data to show';

$_LANG['ClientAreaMailerReminders']['Show'] = 'Show';

$_LANG['ClientAreaGraphs']['Resource Usage Chart'] = 'Resource Usage Chart';
$_LANG['ClientAreaGraphs']['Show'] = 'Show';
$_LANG['ClientAreaGraphs']['Hide'] = 'Hide';
$_LANG['ClientAreaGraphs']['From'] = 'From';
$_LANG['ClientAreaGraphs']['To'] = 'To';
$_LANG['ClientAreaGraphs']['Resources'] = 'Resources';
$_LANG['ClientAreaGraphs']['No data to show'] = 'No data to show';
