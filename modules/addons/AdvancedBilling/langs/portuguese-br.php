<?php
################################################################################
# Advanced Billing 3.0 Lang

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *  Main Menu
 */
$_LANG['addonAA']['pagesLabels']['items']['label']          = "Items";
$_LANG['addonAA']['pagesLabels']['invoices']['label']       = "Invoices";
$_LANG['addonAA']['pagesLabels']['settings']['label']       = "Settings";
$_LANG['addonAA']['pagesLabels']['settings']['integration'] = 'Integration';
$_LANG['addonAA']['pagesLabels']['settings']['extensions']  = 'Extensions';
$_LANG['addonAA']['pagesLabels']['settings']['logs']        = 'Logs';
$_LANG['addonAA']['configuration']['Pricing which will be billed after declared hours'] = 'Pricing billed after a declared number of hours has passed';

$_LANG['addonAA']['pagesLabels']['label']['configuration'] = 'Configuration';
$_LANG['addonAA']['pagesLabels']['label']['items'] = 'Items';
$_LANG['addonAA']['pagesLabels']['label']['invoices'] = 'Invoices';
$_LANG['addonAA']['pagesLabels']['label']['settings'] = 'Settings';
$_LANG['addonAA']['pagesLabels']['label']['credits'] = 'Credits';
$_LANG['addonAA']['settings']['Fixed Pricing'] = 'Fixed Pricing';

$_LANG['addonAA']['configuration']['Please Note'] = 'Please Note';
$_LANG['addonAA']['configuration']['In order to enable automatic synchronization, please setup a following command line in the cron (5 minutes suggested)'] = 'In order to enable the automatic synchronization, you need to set up the following cron command line';
$_LANG['addonAA']['configuration']['and set cron frequency for each product'] = 'and set the cron frequency for each product (every 5 minutes suggested)';
$_LANG['addonAA']['configuration']['Add Product'] = 'Add Product';
$_LANG['addonAA']['configuration']['Enable Advanced Billing For'] = 'Enable Advanced Billing For';
$_LANG['addonAA']['configuration']['To enable product in Advanced Billing module please select it in field below'] = 'Select products that should be enabled in the Advanced Billing module:';
$_LANG['addonAA']['configuration']['Product List'] = 'Products List';
$_LANG['addonAA']['configuration']['Product Name'] = 'Product Name';
$_LANG['addonAA']['configuration']['Submodule'] = 'Submodule';
$_LANG['addonAA']['configuration']['Enabled Extensions'] = 'Enabled Extensions';
$_LANG['addonAA']['configuration']['Cron Frequency'] = 'Cron Frequency';
$_LANG['addonAA']['configuration']['This field accepts numbers only'] = 'This field accepts numbers only';
$_LANG['addonAA']['configuration']['Value can\'t be below 180'] = 'The value cannot be below 180';
$_LANG['addonAA']['configuration']['seconds'] = 'seconds';
$_LANG['addonAA']['configuration']['Status'] = 'Status';
$_LANG['addonAA']['configuration']['Actions'] = 'Actions';
$_LANG['addonAA']['configuration']['Disabled'] = 'Disabled';
$_LANG['addonAA']['configuration']['Enabled'] = 'Enabled';
$_LANG['addonAA']['configuration']['Enable'] = 'Enable';
$_LANG['addonAA']['configuration']['Disable'] = 'Disable';
$_LANG['addonAA']['configuration']['Pricing'] = 'Pricing';
$_LANG['addonAA']['configuration']['Settings'] = 'Settings';
$_LANG['addonAA']['configuration']['View Items'] = 'View Items';
$_LANG['addonAA']['configuration']['View Invoices'] = 'View Invoices';

$_LANG['addonAA']['configuration']['Set Pricing For '] = 'Set Pricing For ';
$_LANG['addonAA']['configuration']['Usage Record'] = 'Usage Record';
$_LANG['addonAA']['configuration']['Free Limit'] = 'Free Limit';
$_LANG['addonAA']['configuration']['Price'] = 'Price';
$_LANG['addonAA']['configuration']['Display Unit'] = 'Display Unit';
$_LANG['addonAA']['configuration']['Type'] = 'Type';
$_LANG['addonAA']['configuration']['Configure'] = 'Configure';
$_LANG['addonAA']['configuration']['Save'] = 'Save';
$_LANG['addonAA']['configuration']['Cancel'] = 'Cancel';
$_LANG['addonAA']['configuration']['The email template that will be used to send a notification to a client'] = 'The email template that will be used to send a notification to a client';
$_LANG['addonAA']['configuration']['Email Template'] = 'Email Template';


$_LANG['addonAA']['configuration']['Module Settings'] = 'Module Settings';
$_LANG['addonAA']['configuration']['Recurring Billing and Credit Billing cannot be enabled simultaneously'] = 'Recurring Billing and Credit Billing extensions cannot be enabled simultaneously';
$_LANG['addonAA']['configuration']['Fixed Pricing, Recurring Billing, Credit Billing and Prepaid Billing cannot be enabled simultaneously'] = 'Fixed Pricing, Recurring Billing, Credit Billing and Prepaid Billing extensions cannot be enabled simultaneously';
$_LANG['addonAA']['configuration']['Settings for this module are not available'] = 'Settings for this module are not available';
$_LANG['addonAA']['configuration']['Settings for this extension are not available'] = 'Settings for this extension are not available';

$_LANG['addonAA']['configuration']['Advanced Billing Info'] = 'Advanced Billing Cycle Info';
$_LANG['addonAA']['configuration']['Show Billing Cycle Information from Advanced Billing Module'] = 'Show the billing cycle information from the Advanced Billing module';

$_LANG['addonAA']['configuration']['Apply Group Discount'] = 'Apply Group Discount';
$_LANG['addonAA']['configuration']['Apply the discount to Invoice, that is set in the Client Group'] = 'Apply the discount to the invoice set in the client group';
$_LANG['addonAA']['configuration']['Check this option if you want to enable notifications extension for this product'] = 'Check this option if you want to enable the Notifications extension for this product';


$_LANG['addonAA']['configuration']['Minimum Billing Amount'] = 'Minimum Billing Amount';
$_LANG['addonAA']['configuration']['The Minimum Amount, that should be billed in the new Invoice creation'] = 'Set the minimum amount of charge for invoices to be generated';
$_LANG['addonAA']['configuration']['Validation Error, please check your configuration'] = 'The validation error has occurred, please check your configuration';

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *  Items
 */
$_LANG['addonAA']['items']['Item list will show you current usage counted by the Advanced Billing module for accounts'] = 'The list of items includes products for which the current usage is counted by the Advanced Billing module';
$_LANG['addonAA']['pagesLabels']['items']['details'] = 'Details';
$_LANG['addonAA']['items']['Account List']  = 'Accounts List';
$_LANG['addonAA']['items']['Product Name']  = 'Product Name';
$_LANG['addonAA']['items']['Domain']    = 'Domain';
$_LANG['addonAA']['items']['Actions'] = 'Actions';
$_LANG['addonAA']['items']['View Details'] = 'View Details';
$_LANG['addonAA']['items']['Delete Records'] = 'Delete Records';
$_LANG['addonAA']['items']['Hosting'] = 'Hosting';
$_LANG['addonAA']['items']['Client Name'] = 'Client Name';
$_LANG['addonAA']['items']['Total Amount'] = 'Total Amount';
$_LANG['addonAA']['items']['Last Update'] = 'Last Update';
$_LANG['addonAA']['items']['Are you sure?'] = 'Are you sure that you want to proceed?';
$_LANG['addonAA']['items']['Nothing to display'] = 'Nothing to display';
$_LANG['addonAA']['items']['Delete'] = 'Delete';
$_LANG['addonAA']['items']['Cancel'] = 'Cancel';
$_LANG['addonAA']['items']['Generate Invoice'] = 'Generate Invoice';
$_LANG['addonAA']['items']['Total'] = 'Total';
$_LANG['addonAA']['items']['Date'] = 'Date';
$_LANG['addonAA']['items']['All currently generated records will be deleted. This action can NOT be undone!'] = 'All currently generated records will be deleted. This action cannot be undone.';
$_LANG['addonAA']['items']['For Product'] = 'For Product';
$_LANG['addonAA']['items']['Show All'] = 'Show All';

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *  Invoices
 */
$_LANG['addonAA']['invoices']['Awaiting Invoices means, that it will not be generated until you confirm that manually'] = 'Awaiting invoices will not be generated until you confirm this action manually';
$_LANG['addonAA']['invoices']['This is the best way to avoid any unwanted invoices for your clients and test the possibilities of Advanced Billing module. To enable this feature, please edit your Products in Configuration page and disable "Autogenerate Invoice" option.'] = 'This is the best way to avoid any unwanted invoices for your clients and test the possibilities of the Advanced Billing module.
            To enable this feature, please edit your Products in the Configuration page and disable the "Autogenerate Invoice" option.';
$_LANG['addonAA']['invoices']['Awaiting Invoices'] = 'Awaiting Invoices';
$_LANG['addonAA']['invoices']['Client Name'] = 'Client Name';
$_LANG['addonAA']['invoices']['Hosting'] = 'Hosting';
$_LANG['addonAA']['invoices']['Product Name'] = 'Product Name';
$_LANG['addonAA']['invoices']['Total'] = 'Total';
$_LANG['addonAA']['invoices']['Invoice Date'] = 'Invoice Date';
$_LANG['addonAA']['invoices']['Invoice Due Date'] = 'Invoice Due Date';
$_LANG['addonAA']['invoices']['Actions'] = 'Actions';
$_LANG['addonAA']['invoices']['Show Details'] = 'Show Details';
$_LANG['addonAA']['invoices']['Delete'] = 'Delete';
$_LANG['addonAA']['invoices']['Generate'] = 'Generate';
$_LANG['addonAA']['invoices']['Invoices History'] = 'Invoices History';
$_LANG['addonAA']['invoices']['Nothing to display'] = 'Nothing to display';

$_LANG['addonAA']['invoices']['Are you sure?'] = 'Are you sure that you want to proceed?';
$_LANG['addonAA']['invoices']['Selected Invoice will be deleted. This action can NOT be undone!'] = 'The selected invoice will be deleted. This action cannot be undone.';
$_LANG['addonAA']['invoices']['Cancel'] = 'Cancel';

$_LANG['addonAA']['invoices']['Invoice Details'] = 'Invoice Details';
$_LANG['addonAA']['invoices']['Description'] = 'Description';
$_LANG['addonAA']['invoices']['Amount'] = 'Amount';
$_LANG['addonAA']['invoices']['Taxed'] = 'Taxed';
$_LANG['addonAA']['invoices']['Save'] = 'Save';
$_LANG['addonAA']['invoices']['Date'] = 'Date';
$_LANG['addonAA']['invoices']['Due Date'] = 'Due Date';
$_LANG['addonAA']['invoices']['Client'] = 'Client';

$_LANG['invoices']['line'] = 'The customer is billed for the over usage of {name} - {usage} {unit} (Free Limit: {freelimit})  for {total}';

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *  Settings → Integration Code
 */
$_LANG['addonAA']['settings']['In order to enable you customers to monitor resource usage in product details and pricing in product configuration please do following instruction'] = 'In order to enable customers to monitor the resource usage in product details and the pricing in product configuration, please follow the below instructions';
$_LANG['addonAA']['settings']['Display product pricing in product configuration'] = 'Display Product Pricing During Order';
$_LANG['addonAA']['settings']['Display usage records'] = 'Display Usage Records';
$_LANG['addonAA']['settings']['In order to enable your customers to monitor resource usage, open the file'] = 'In order to enable your customers to monitor the resource usage, open the file';
$_LANG['addonAA']['settings']['Find the following line'] = 'Find the line';
$_LANG['addonAA']['settings']['Add this code Before the line'] = 'Add the below code above that line';
$_LANG['addonAA']['settings']['In order to enable usage record pricing on order form, open the file'] = 'In order to display the product pricing during the order placing, open the file';

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *  Settings → Extensions
 */
$_LANG['addonAA']['settings']['Each extension is configured per product, therefore you can use different sets of extensions according to your needs'] = 'Each extension is configured per product, therefore you can use different sets of extensions according to your needs';
$_LANG['addonAA']['settings']['Available Extensions'] = 'Available Extensions';
$_LANG['addonAA']['settings']['Extension'] = 'Extension';
$_LANG['addonAA']['settings']['Type'] = 'Type';
$_LANG['addonAA']['settings']['Version'] = 'Version';
$_LANG['addonAA']['settings']['Actions'] = 'Actions';
$_LANG['addonAA']['settings']['Enabled'] = 'Enabled';
$_LANG['addonAA']['settings']['Disabled'] = 'Disabled';
$_LANG['addonAA']['settings']['Configure'] = 'Configure';
$_LANG['addonAA']['settings']['Save'] = 'Save';
$_LANG['addonAA']['settings']['Cancel'] = 'Cancel';
$_LANG['addonAA']['settings']['Set Configuration For '] = 'Set Configuration For ';
$_LANG['addonAA']['settings']['License Key'] = 'License Key';

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *  Settings → Logs
 */
$_LANG['addonAA']['settings']['Logs Settings'] = 'Logs Settings';
$_LANG['addonAA']['settings']['Logs'] = 'Logs';
$_LANG['addonAA']['settings']['Logger Type'] = 'Log Type';
$_LANG['addonAA']['settings']['Database'] = 'Database';
$_LANG['addonAA']['settings']['File'] = 'File';
$_LANG['addonAA']['settings']['Date'] = 'Date';
$_LANG['addonAA']['settings']['Message'] = 'Message';
$_LANG['addonAA']['settings']['File Content'] = 'File Content';
$_LANG['addonAA']['settings']['Back'] = 'Back';
$_LANG['addonAA']['pagesLabels']['settings']['viewLogFile'] = 'View File';

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *  Extension
 */
$_LANG['addonAA']['configuration']['Set Settings For'] = 'Configure Settings For';
$_LANG['addonAA']['configuration']['Check this option if you want to enable usage records extension for this product'] = 'Check this option if you want to enable the Usage Records extension for this product';
$_LANG['addonAA']['configuration']['Check this option if you want to enable recurring billing extension for this product'] = 'Check this option if you want to enable the Recurring Billing extension for this product';
$_LANG['addonAA']['configuration']['Display Summary From'] = 'Display Summary From';
$_LANG['addonAA']['configuration']['Usage Records Precision'] = 'Usage Records Precision';
$_LANG['addonAA']['configuration']['Usage Records Per Page'] = 'Usage Records Per Page';
$_LANG['addonAA']['configuration']['Enable Records History'] = 'Records History';

$_LANG['addonAA']['configuration']['Bill on terminate'] = 'Bill On Termination';
$_LANG['addonAA']['configuration']['Bill your client after account is terminated'] = 'Bill your client after the account is terminated';
$_LANG['addonAA']['configuration']['Check this option if you want to bill hosting on terminate'] = 'Check this option if you want to bill the hosting upon termination';
$_LANG['addonAA']['configuration']['Billing Type'] =  'Billing Type';
$_LANG['addonAA']['configuration']['Billing Type Value'] = 'Billing Type Value';
$_LANG['addonAA']['configuration']['Due Date'] = 'Due Date';
$_LANG['addonAA']['configuration']['Autogenerate Invoice'] = 'Auto Generate Invoice';
$_LANG['addonAA']['configuration']['Check this option if you want module to generate invoice automatically'] = 'Check this option if you want the module to generate invoices automatically';
$_LANG['addonAA']['configuration']['Auto Apply Credits'] = 'Auto Apply Credits';
$_LANG['addonAA']['configuration']['When this is enabled invoices will be generated automatically'] = 'If enabled, the invoices will be generated automatically';
$_LANG['addonAA']['configuration']['Auto apply any available credit from clients credit balance'] = 'Automatically apply any credits available in the client\'s credit balance';

$_LANG['addonAA']['configuration']['Enable Credit Billing'] = 'Enable Credit Billing';
$_LANG['addonAA']['configuration']['Check this option if you want to enable Credit Billing for this product'] = 'Check this option if you want to enable the Credit Billing extension for this product';
$_LANG['addonAA']['configuration']['Create Invoice Each'] = 'Create Invoice Every';
$_LANG['addonAA']['configuration']['Days'] = 'Days';
$_LANG['addonAA']['configuration']['Minimum Credit'] = 'Minimum Credit';
$_LANG['addonAA']['configuration']["Minimum amount that will be charged from client's account"] = 'The minimum amount that will be charged from the client\'s account';
$_LANG['addonAA']['configuration']['Low Credit Notification'] = 'Low Credit Notification';
$_LANG['addonAA']['configuration']['Send email Notification about low credit amount on account']  = 'Send email notifications about the low credit amount on the account';
$_LANG['addonAA']['configuration']['Email Inverval'] = 'Email Interval';
$_LANG['addonAA']['configuration']['Set email interval for email notification'] = 'Set a number of days interval for email notifications';
$_LANG['addonAA']['configuration']['Autosuspend'] = 'Autosuspend';
$_LANG['addonAA']['configuration']['Autosuspend account when user does not have sufficient funds'] = 'Suspend the account automatically if the user does not have sufficient funds';
$_LANG['addonAA']['configuration']['Days'] = 'Days';

$_LANG['addonAA']['configuration']['Enable Prepaid Billing'] = 'Enable Prepaid Billing';
$_LANG['addonAA']['configuration']['Check this option if you want to enable Prepaid Billing for this product'] = 'Check this option if you want to enable the Prepaid Billing extension for this product';
$_LANG['addonAA']['configuration']['Create Summation Each'] = 'Create Summation Every';

$_LANG['addonAA']['configuration']['Autorefill'] = 'Autorefill';
$_LANG['addonAA']['configuration']['Enable to charge from client\'s credit card when client\'s balance reach 0'] = 'If enabled, the client\'s credit card will be charged when the client\'s balance reaches 0';
$_LANG['addonAA']['configuration']['Gateway'] = 'Gateway';
$_LANG['addonAA']['configuration']['Minimum Amount'] = 'Minimum Amount';
$_LANG['addonAA']['configuration']['Minimum amount to charge from client\'s credit card'] = 'The minimum amount that will be charged from the client\'s credit card';
$_LANG['addonAA']['configuration']['Maximum Amount'] = 'Maximum Amount';
$_LANG['addonAA']['configuration']['Maximum amount to charge from client\'s credit card'] = 'The maximum amount that will be charged from the client\'s credit card';

$_LANG['addonAA']['configuration']['Check this option if you want to enable Product Auto Upgrade Extension for this product'] = 'Check this option if you want to enable the Product Auto Upgrade Extension for this product';
$_LANG['addonAA']['configuration']['Check this option if you want to enable Graph Extension for this product'] = 'Check this option if you want to enable the Graphs extension for this product';

$_LANG['addonAA']['pagesLabels']['settings']['archive'] = 'Items Archive';
$_LANG['addonAA']['settings']['On this page you can find information regarding archived usage records'] = "This page contains information on the archived usage records";
$_LANG['addonAA']['settings']['Archive Information'] = "Archive Information";
$_LANG['addonAA']['settings']['Archived Items'] = "Archived Items";
$_LANG['addonAA']['settings']['Space On Disk'] = "Space On Disk";
$_LANG['addonAA']['settings']['First Item Date'] = "First Item Date";
$_LANG['addonAA']['settings']['Flush Now'] = "Flush Now";
$_LANG['addonAA']['settings']['Save Interval'] = "Save Interval";
$_LANG['addonAA']['settings']['Set Flush Archive Interval(Days)'] = "Set Flush Archive Interval(Days)";

$_LANG['addonAA']['settings']['Flush interval has been saved successfully']  = "The flush interval has been saved successfully";
$_LANG['addonAA']['settings']['Flush interval saving failed, numeric value are expected']  = "The flush interval saving has failed. A numeric value is expected";
$_LANG['addonAA']['settings']['Archive Flush Interval Saved']  = "The saving of the flush interval has failed";
$_LANG['addonAA']['settings']['Archive has been flushed']  = "The archive has been flushed";
$_LANG['addonAA']['settings']['Archive Remove Interval Saved'] = "The removal interval has been saved successfully";
$_LANG['addonCA']['Notifications']['Notifications']['remindersLimitReached'] = "The number of reminders has reached the limit.";
$_LANG['addonCA']['home']['Hourly Billing'] = 'Hourly Billing';

// ***********  EXTENSION ***************//

$_LANG['addonAA']['settings']['Usage Records']  = "Usage Records";
$_LANG['addonAA']['settings']['Extension used to view details of usage records and their history per product']  = "Extension used to view details of usage records and their history per product";

$_LANG['addonAA']['settings']['Credit Billing']  = "Credit Billing";
$_LANG['addonAA']['settings']['Credit Billing Extension used to charge clients for their products from their credit balance']  = "Extension that allows you to charge clients for their products from their credit balance as well as generate invoices";

$_LANG['addonAA']['settings']['Prepaid Billing']  = "Prepaid Billing";
$_LANG['addonAA']['settings']['Prepaid Billing Extension that allows you to charge clients for their products from their credit balance as well as create summations']  = "Extension that allows you to charge clients for their products from their credit balance as well as create summations";

$_LANG['addonAA']['settings']['Graphs']  = "Graphs";
$_LANG['addonAA']['settings']['Extension used to create resource usage graphs for services']  = "Extension used to create resource usage graphs for services";

$_LANG['addonAA']['settings']['Notifications']  = "Notifications";
$_LANG['addonAA']['settings']['Extension used to send email reminders to clients when specified resource limits are reached or exceeded']  = "Extension used to send email reminders to clients when specified resource limits are reached or exceeded";

$_LANG['addonAA']['settings']['Recurring Billing']  = "Recurring Billing";
$_LANG['addonAA']['settings']['Extension used to bill clients at specified time intervals']  = "Extension used to bill clients at specified time intervals";

$_LANG['addonAA']['settings']['Proxmox Cloud Autoscaling'] = 'Proxmox Cloud Autoscaling';
$_LANG['addonAA']['settings']['Extension used to automatically scale server cloud depend on its load'] = 'Extension used to automatically scale server cloud based on its load';

$_LANG['addonAA']['settings']['Product Auto Upgrade'] = 'Product Auto Upgrade';
$_LANG['addonAA']['settings']['Extension used to automatically upgrade products basing on resource usage'] = 'Extension used to automatically upgrade products based on resource usage';


// ************** 3.4.0 artur.pi ******************//


$_LANG['addonAA']['configuration']['Maximum Number Of Reminders'] = "Maximum Number Of Reminders";
$_LANG['addonAA']['configuration']['The number is used to limit reminders'] = "Set the limit on the number of reminders that your clients may create";


$_LANG['addonAA']['configuration']['Time to wait in hours until service will be billed at monthly rate'] = "The number of hours that must pass before the service is billed at a monthly rate";
$_LANG['addonAA']['configuration']['Hours Amount'] = "Hours";
$_LANG['addonAA']['configuration']['Check this option if you want to enable fixed pricing extension for this product'] = "Check this option if you want to enable the Fixed Pricing extension for this product";

$_LANG['addonAA']['configuration']['Enable Usage Records']    = "Enable Usage Records";
$_LANG['addonAA']['configuration']['Enable Fixed Pricing']    = 'Enable Fixed Pricing';
$_LANG['addonAA']['configuration']['Enable Graphs']           = 'Enable Graphs';
$_LANG['addonAA']['configuration']['Enable Notifications']    = 'Enable Notifications';
$_LANG['addonAA']['configuration']['Enable Recuring Billing'] = 'Enable Recurring Billing';

$_LANG['addonAA']['settings']['Extension used to bill clients with a fixed amount after active usage of the service in a specified time interval'] = 'Extension used to bill clients with a fixed amount after active usage of the service in a specified time interval';

$_LANG['addonAA']['configuration']['Append tax to created invoice'] = "Add tax to the created invoice";
$_LANG['addonAA']['configuration']['Tax'] = 'Tax';
$_LANG['addonAA']['credits']['Client credits has been refunded'] = "The client's credits have been refunded successfully";

//////*****cPanel*****//////
$_LANG['invoice']['resource']['Bandwidth'] = 'Bandwidth';
$_LANG['invoice']['resource']['Storage'] = 'Storage';
$_LANG['invoice']['resource']['Databases'] = 'Databases';
$_LANG['invoice']['resource']['Addon Domains'] = 'Addon Domains';
$_LANG['invoice']['resource']['Subdomains'] = 'Subdomains';
$_LANG['invoice']['resource']['Parked Domains'] = 'Parked Domains';
$_LANG['invoice']['resource']['Email Accounts'] = 'Email Accounts';
$_LANG['invoice']['resource']['Installed Applications'] = 'Installed Applications';
$_LANG['invoice']['resource']['FTP Accounts'] = 'FTP Accounts';
$_LANG['invoice']['resource']['Email Forwarders'] = 'Email Forwarders';
$_LANG['invoice']['resource']['Domain Forwarders'] = 'Domain Forwarders';
$_LANG['invoice']['resource']['Hourly'] = 'Hourly';

//////*****Proxmox VPS/Cloud*****//////
$_LANG['invoice']['resource']['Bandwidth IN'] = 'Bandwidth IN';
$_LANG['invoice']['resource']['Bandwidth OUT'] = 'Bandwidth OUT';
$_LANG['invoice']['resource']['Bandwidth TOTAL'] = 'Bandwidth TOTAL';
$_LANG['invoice']['resource']['Disk Usage'] = 'Disk Usage';
$_LANG['invoice']['resource']['Disk Size'] = 'Disk Size';
$_LANG['invoice']['resource']['Memory Usage'] = 'Memory Usage';
$_LANG['invoice']['resource']['Memory Size'] = 'Memory Size';
$_LANG['invoice']['resource']['Backups Usage'] = 'Backups Usage';
$_LANG['invoice']['resource']['CPU Number'] = 'CPU Number';
$_LANG['invoice']['resource']['CPU Usage'] = 'CPU Usage';
$_LANG['invoice']['resource']['IPv4'] = 'IPv4';
$_LANG['invoice']['resource']['IPv6'] = 'IPv6';

//////*****Zimbra*****//////
$_LANG['invoice']['resource']['Mailboxes'] = 'Mailboxes';
$_LANG['invoice']['resource']['Email Aliases'] = 'Email Aliases';
$_LANG['invoice']['resource']['Domain Aliases'] = 'Domain Aliases';

//////*****Virtuozzo*****//////
$_LANG['invoice']['resource']['Template'] = 'Template';
$_LANG['invoice']['resource']['CPU Cores'] = 'CPU Cores';
$_LANG['invoice']['resource']['CPU Limit'] = 'CPU Limit';
$_LANG['invoice']['resource']['Memory Limit'] = 'Memory Limit';
$_LANG['invoice']['resource']['Incoming Bandwidth'] = 'Incoming Bandwidth';
$_LANG['invoice']['resource']['Outgoing Bandwidth'] = 'Outgoing Bandwidth';
$_LANG['invoice']['resource']['Disk Used'] = 'Disk Used';
$_LANG['invoice']['resource']['Disk Limit'] = 'Disk Limit';
$_LANG['invoice']['resource']['Public IP'] = 'Public IP';
$_LANG['invoice']['resource']['Private IP'] = 'Private IP';
$_LANG['invoice']['resource']['Disk I/O Limit'] = 'Disk I/O Limit';
$_LANG['invoice']['resource']['Disk I/O ps Limit'] = 'Disk I/O ps Limit';
$_LANG['invoice']['resource']['Backup Space Used'] = 'Backup Space Used';

//////*****Virtualizor*****//////
$_LANG['invoice']['resource']['CPU Units'] = 'CPU Units';
$_LANG['invoice']['resource']['RAM Size'] = 'RAM Size';
$_LANG['invoice']['resource']['RAM Usage'] = 'RAM Usage';
$_LANG['invoice']['resource']['Network Speed IN'] = 'Network Speed IN';
$_LANG['invoice']['resource']['Network Speed OUT'] = 'Network Speed OUT';
$_LANG['invoice']['resource']['I/O Disk Read'] = 'I/O Disk Read';
$_LANG['invoice']['resource']['I/O Disk Write'] = 'I/O Disk Write';
$_LANG['invoice']['resource']['IPv4 Addresses'] = 'IPv4 Addresses';
$_LANG['invoice']['resource']['IPv6 Addresses'] = 'IPv6 Addresses';

//////*****Ticket Billing*****//////
$_LANG['invoice']['resource']['Opened Tickets'] = 'Opened Tickets';

//////*****Solus*****//////
$_LANG['invoice']['resource']['Available Disk Size'] = 'Available Disk Size';
$_LANG['invoice']['resource']['Available Memory'] = 'Available Memory';
$_LANG['invoice']['resource']['Available Bandwidth'] = 'Available Bandwidth';
$_LANG['invoice']['resource']['Bandwidth Used'] = 'Bandwidth Used';
$_LANG['invoice']['resource']['IP Addresses'] = 'IP Addresses';

//////*****RackspaceEmail*****//////
$_LANG['invoice']['resource']['Mailbox Storage'] = 'Mailbox Storage';
$_LANG['invoice']['resource']['Sync Licenses'] = 'Sync Licenses';
$_LANG['invoice']['resource']['Blackberry Licenses'] = 'Blackberry Licenses';
$_LANG['invoice']['resource']['Exchange Storage'] = 'Exchange Storage';
$_LANG['invoice']['resource']['Exchange Mailboxes'] = 'Exchange Mailboxes';

//////*****Plesk*****//////
$_LANG['invoice']['resource']['Disk Space'] = 'Disk Space';
$_LANG['invoice']['resource']['Email Boxes'] = 'Email Boxes';
$_LANG['invoice']['resource']['Redirects'] = 'Redirects';
$_LANG['invoice']['resource']['Mail Groups'] = 'Mail Groups';
$_LANG['invoice']['resource']['Autoresponders'] = 'Autoresponders';
$_LANG['invoice']['resource']['Mailing Lists'] = 'Mailing Lists';
$_LANG['invoice']['resource']['Users'] = 'Users';
$_LANG['invoice']['resource']['Webapps'] = 'Webapps';
$_LANG['invoice']['resource']['Traffic'] = 'Traffic';

//////*****OpenstackVPS*****//////
$_LANG['invoice']['resource']['VCPU Cores'] = 'VCPU Cores';
$_LANG['invoice']['resource']['CPU Utilization'] = 'CPU Utilization';
$_LANG['invoice']['resource']['Disk Root Used'] = 'Disk Root Used';
$_LANG['invoice']['resource']['Disk I/O Write'] = 'Disk I/O Write';
$_LANG['invoice']['resource']['Disk I/O Read'] = 'Disk I/O Read';
$_LANG['invoice']['resource']['Floating IP'] = 'Floating IP';
$_LANG['invoice']['resource']['Fixed IP'] = 'Fixed IP';
$_LANG['invoice']['resource']['Backups Number'] = 'Backups Number';

//////*****DirectAdmin*****//////
$_LANG['invoice']['resource']['Virtual Domains'] = 'Virtual Domains';
$_LANG['invoice']['resource']['Domain Pointers'] = 'Domain Pointers';
$_LANG['invoice']['resource']['MySQL Databases'] = 'MySQL Databases';
$_LANG['invoice']['resource']['Email Autoresponders'] = 'Email Autoresponders';
$_LANG['invoice']['resource']['POP Accounts'] = 'POP Accounts';

//////*****DigitalOcean*****//////
$_LANG['invoice']['resource']['Memory'] = 'Memory';
$_LANG['invoice']['resource']['Vcpus'] = 'Vcpus';
$_LANG['invoice']['resource']['Size'] = 'Size';
$_LANG['invoice']['resource']['Disk'] = 'Disk';
$_LANG['invoice']['resource']['Snapshots'] = 'Snapshots';
$_LANG['invoice']['resource']['Volumes'] = 'Volumes';

