<?php

namespace ModulesGarden\SmsCenter\Core\Models;

use \ModulesGarden\SmsCenter\Core\ModuleConstants;


/**
 * Description of ExtendedEloquentModel
 *
 */
class ExtendedEloquentModel extends \Illuminate\Database\Eloquent\Model
{
    public function __construct(array $attributes = [])
    {
        
        $this->table = ModuleConstants::getPrefixDataBase() . $this->table;
        
        parent::__construct($attributes);
    }

}
