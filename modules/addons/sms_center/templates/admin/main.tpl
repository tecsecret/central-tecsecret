<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
<link rel="stylesheet" href="{$assetsURL}/css/layers-ui.css">

<div id="layers">
    <div class="app">
        <div class="app-header app-header--responsive navbar">
            <a class="navbar__brand brand brand--product" href="{$mainURL}">
                <div class="brand__logo product-{$tagImageModule}-for-whmcs i-c-5x">
                    <img class="i-c-3x" src="{$assetsURL}/img/products/{$tagImageModule}.svg" alt="{$mainName}">
                </div>
                <div class="brand__text">
                    {$mainName}
                </div>
            </a>
            <button class="navbar__burger navbar-right btn" data-toggle="offCanvas" data-target=".app-navbar">
                <span class="btn__icon burger">
                    <span></span>
                    <span></span>
                    <span></span>
                </span>
            </button>
        </div>
        <div class="app-navbar navbar navbar--responsive off-canvas-responsive off-canvas-responsive--right">
            <div class="navbar__top">
                <a class="navbar__brand brand brand--product" href="{$mainURL}">
                    <div class="brand__logo product-{$tagImageModule}-for-whmcs i-c-6x">
                        <img class="i-c-4x" src="{$assetsURL}/img/products/{$tagImageModule}.svg" alt="{$mainName}">
                    </div>
                    <div class="brand__text">
                        {$mainName}
                    </div>
                </a>
                <a class="navbar__brand brand is-right" href="https://www.modulesgarden.com" target="_blank">
                    <div class="brand__logo">
                        <img src="{$assetsURL}/img/logo.png" alt="ModulesGarden" width="150">
                    </div>
                </a>
            </div>
            <div class="navbar__nav">
                <ul class="nav nav--h is-left">
                    {foreach from=$menu key=catName item=category}
                        {if $category.submenu}
                            <li class="nav__item has-dropdown {if $currentPageName|strtolower === $catName|strtolower}is-active{/if}">
                                <a class="nav__link" href="{$category.url}">
                                    {if $category.icon}
                                        <i class="{$category.icon}"></i>
                                    {/if}
                                    {if $category.label}
                                        {$subpage.label}
                                        <span class="nav__link-drop-arrow"></span>
                                    {else}
                                        <span class="nav__link-text">{$MGLANG->T('pagesLabels','label', $catName)}</span>
                                        <span class="nav__link-drop-arrow"></span>
                                    {/if}
                                </a>
                                <ul class="nav nav--sub">
                                    {foreach from=$category.submenu key=subCatName item=subCategory}
                                        {if $subCategory.externalUrl}
                                            <li class="nav__item">
                                                <a class="nav__link" href="{$subCategory.externalUrl}" target="_blank">
                                                    {if $subCategory.icon}<i class="{$subCategory.icon}"></i>{/if}
                                                    {if $subCategory.label}
                                                        {$subCategory.label}
                                                    {else}
                                                        <span class="nav__link-text">{$MGLANG->T('pagesLabels', $catName, $subCatName)}</span>
                                                    {/if}                                                        
                                                </a>
                                            </li>
                                        {else}
                                            <li class="nav__item">
                                                <a class="nav__link" href="{$subCategory.url}">
                                                    {if $subCategory.icon}<i class="{$subCategory.icon}"></i>{/if}
                                                    {if $subCategory.label}
                                                        {$subCategory.label}
                                                    {else}
                                                        <span class="nav__link-text">{$MGLANG->T('pagesLabels', $catName, $subCatName)}</span>
                                                    {/if}                                                        
                                                </a>
                                            </li>                                         
                                        {/if}
                                    {/foreach}
                                </ul>
                            </li>                            
                        {else}
                            <li class="nav__item {if $currentPageName|strtolower === $catName|strtolower}is-active{/if}">
                                <a class="nav__link" href="{if $category.externalUrl}{$category.externalUrl}{else}{$category.url}{/if}"
                                        {if $category.externalUrl} target="_blank"{/if}>
                                    {if $category.icon}
                                        <i class="{$category.icon}"></i>
                                    {/if}
                                    {if $category.label}
                                        {$subpage.label}
                                    {else}
                                        <span class="nav__link-text">{$MGLANG->T('pagesLabels','label', $catName)}</span>
                                    {/if}
                                    <span class="drop-arrow"></span>
                                </a>
                            </li>        
                        {/if}
                    {/foreach}
                </ul>
            </div>
        </div>
        <div class="app-main">
            <div class="app-main__body">
                <div class="app-main__top top">
                    <ul class="breadcrumb type-5">
                        {assign var="brKeys"  value=$breadcrumbs|array_keys}         
                        {foreach from=$breadcrumbs key=brKey item=brItem}
                            {if $brItem.name !== 'Index'}
                                <li class="breadcrumb__item is-active">{if $brItem.url}<a class="breadcrumb__link" href="{$brItem.url}">{if $brKeys[0] === $brKey}{$MGLANG->T('pagesLabels','label', $brItem.name)}{else}{$MGLANG->T('pagesLabels', $breadcrumbs[($brKey - 1)]['name'], $brItem.name)}{/if}</a>{else}<span class="breadcrumb__link">{if $brKeys[0] === $brKey}{$MGLANG->T('pagesLabels','label', $brItem.name)}{else}{$MGLANG->T('pagesLabels', $breadcrumbs[($brKey - 1)]['name'], $brItem.name)}{/if}</span>{/if}</li>
                            {/if}
                        {/foreach}
                    </ul>
                </div>
                {if $error}
                    <div class="alert alert--outline alert--icon alert--danger alert--bordered m-b-x alert--dismiss mg-message">
                        <div class="alert__body">
                            <b>{$error}</b>
                        </div>
                        <button type="button" class="btn btn--icon btn--link btn--close" data-dismiss="alert">
                            <i class="btn__icon zmdi zmdi-close"></i>
                        </button>
                    </div>                    
                {/if}
                {if $success}    
                    <div class="alert alert--outline alert--icon alert--success alert--bordered m-b-x alert--dismiss mg-message">
                        <div class="alert__body">
                            <b>{$success}</b>
                        </div>
                        <button type="button" class="btn btn--icon btn--link btn--close" data-dismiss="alert">
                            <i class="btn__icon zmdi zmdi-close"></i>
                        </button>
                    </div>                
                {/if}
                {if ($isDebug eq true AND (count($MGLANG->getMissingLangs()) != 0))}
                    <div class="lu-row">
                        <div class="widget">
                            <div class="widget__body">
                                <div class="widget__content">
                                    <div class="lu-row">
                                        {foreach from=$MGLANG->getMissingLangs() key=varible item=value}
                                            <div class="col-md-12"><b>{$varible}</b> = '{$value}';</div>
                                        {/foreach}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                {/if}
                {$content}
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="{$assetsURL}/js/vue.min.js"></script>
{*<script type="text/javascript" src="https://unpkg.com/vue"></script>*}
<script type="text/javascript" src="{$assetsURL}/js/mgComponents.js"></script>
<script type="text/javascript" src="{$assetsURL}/js/jscolor.min.js"></script>            
<script type="text/javascript" src="{$assetsURL}/js/layers-ui.js"></script>
<script type="text/javascript" src="{$assetsURL}/js/layers-ui-table.js"></script>            

<div class="clear"></div>
