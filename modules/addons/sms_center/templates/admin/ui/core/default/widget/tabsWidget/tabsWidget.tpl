<div class="lu-row">
    <div class="widget">
        <div class="widget__header">
            {if $rawObject->isRawTitle() || $rawObject->getTitle()}
                <div class="widget__top top">
                    <div class="top__title">
                        {if $rawObject->getIcon()}<i class="{$rawObject->getIcon()}"></i>{/if}
                        {if $rawObject->isRawTitle()}{$rawObject->getRawTitle()}{elseif $rawObject->getTitle()}{$MGLANG->T($rawObject->getTitle())}{/if}
                    </div>
                </div>
            {/if}
            <div class="widget__nav swiper-container swiper-container-horizontal swiper-container-false" data-content-slider="" style="visibility: visible;">
                {assign var="firstArrKey" value=$rawObject->getElements()|array_keys}
                {if $elements}
                    <ul class="nav nav--md nav--h nav--tabs nav--arrow">
                        {foreach from=$rawObject->getElements() key=tplKey item=tplValue}
                            <li {if $tplKey  === $firstArrKey[0]} class="nav__item is-active" {else} class="nav__item" {/if}>
                                <a class="nav__link"  data-toggle="lu-tab" href="#contTab{$tplValue->getId()}">
                                    <span class="nav__link-text">{if $tplValue->isRawTitle()}{$tplValue->getRawTitle()}{elseif $tplValue->getTitle()}{$MGLANG->T($tplValue->getTitle())}{/if}</span>
                                </a>
                            </li>
                        {/foreach}
                    </ul>
                {/if}
            </div>
        </div>
        <div class="widget__body">
            <div class="tab-content">
                {foreach from=$rawObject->getElements() key=tplKey item=tplValue}
                    <div id="contTab{$tplValue->getId()}" class="tab-pane {if $tplKey === $firstArrKey[0]} is-active {/if}">
                        {$tplValue->getHtml()}
                    </div>
                {/foreach}
            </div>
        </div>
    </div>
</div>
