{**********************************************************************
* SmsCenter product developed. (2017-10-30)
* *
*
*  CREATED BY MODULESGARDEN       ->       http://modulesgarden.com
*  CONTACT                        ->       contact@modulesgarden.com
*
*
* This software is furnished under a license and may be used and copied
* only  in  accordance  with  the  terms  of such  license and with the
* inclusion of the above copyright notice.  This software  or any other
* copies thereof may not be provided or otherwise made available to any
* other person.  No title to and  ownership of the  software is  hereby
* transferred.
*
*
**********************************************************************}

{**
* @author Sławomir Miśkowicz <slawomir@modulesgarden.com>
*}

<div class="form-group">
    <label class="form-label">
        {if $rawObject->isRawTitle()}{$rawObject->getRawTitle()}{elseif $rawObject->getTitle()}{$MGLANG->T($rawObject->getTitle())}{/if}
        {if $rawObject->getDescription()}
            <i data-title="{$MGLANG->T($rawObject->getDescription())}" data-toggle="tooltip" class="i-c-2x zmdi zmdi-help-outline form-tooltip-helper lu-tooltip drop-target drop-element-attached-bottom drop-element-attached-center drop-target-attached-top drop-target-attached-center"></i>
        {/if}
    </label>
    
    <mg-field-ajax 
        v-bind:renderList='{$rawObject->getRenderAgainList()}' 
        v-bind:isRenderAgain="{$rawObject->isRenderAgain()}" 
        v-bind:fieldId="'mg-field-ajax-{$rawObject->getName()}'" 
        @fieldrendernow='$children.find(child => { return child.$options.name === "mg-field-ajax" && $(child.$el).attr("fieldname") === "mg-field-ajax-{$rawObject->getName()}"; }).reloadSelect("mg-field-ajax-{$rawObject->getName()}", "{$rawObject->getId()}", "{$rawObject->getNamespace()}", "{$rawObject->getIndex()}")' inline-template>
        <select 
            fieldName='mg-field-ajax-{$rawObject->getName()}' 
            class='form-control ajax'
            isRenderSelect='true'
            name='{$rawObject->getName()}'
            {if $rawObject->isDisabled()}disabled='disabled'{/if}
            {if $rawObject->isMultiple()}data-options='removeButton:true; resotreOnBackspace:true; dragAndDrop:true; maxItems: null;' multiple='multiple'{/if}>
        </select>
    </mg-field-ajax>
    <div class="form-feedback form-feedback--icon" hidden="hidden">
    </div>    
</div>