{**********************************************************************
* SmsCenter product developed. (2017-08-24)
* *
*
*  CREATED BY MODULESGARDEN       ->       http://modulesgarden.com
*  CONTACT                        ->       contact@modulesgarden.com
*
*
* This software is furnished under a license and may be used and copied
* only  in  accordance  with  the  terms  of such  license and with the
* inclusion of the above copyright notice.  This software  or any other
* copies thereof may not be provided or otherwise made available to any
* other person.  No title to and  ownership of the  software is  hereby
* transferred.
*
*
**********************************************************************}

{**
* @author Sławomir Miśkowicz <slawomir@modulesgarden.com>
*}

<div id="mgVueMainContainer">
    {literal}  
        <mg-resp-handler>
            <template slot-scope="{ resps }">
                <div class="lu-row">
                    <div v-for="resp in resps" :class="'alert alert--outline alert--icon alert--' +  resp.type + ' alert--bordered m-b-x alert--dismiss mg-message'">
                        <div class="alert__body">
                            <b>{{ resp.message }}</b>
                        </div>
                        <button type="button" class="btn btn--icon btn--link btn--close" data-dismiss="alert">
                            <i class="btn__icon zmdi zmdi-close"></i>
                        </button>
                    </div>
                </div>
            </template>    
        </mg-resp-handler>    
    {/literal}
    <div class="lu-row"><i v-show="pageLoading" class="page_processing"></i></div>
    {foreach from=$elements key=nameElement item=dataElement}
        {$dataElement->getHtml()}
    {/foreach}
    {literal}
    
    <mg-modal v-if="showModal" @close="showModal = false" :bodydata="htmlContent"></mg-modal>
    
    <div id='allScript'></div>
    
    <div id="vueModalContainer"></div>
    <div class="preloader-container preloader-container--full-screen preloader-container--overlay" v-show="pagePreLoader">
        <div class="preloader preloader--lg"></div>
    </div>
</div>
{/literal}

<script type="text/javascript">
    $(function(){
        mgPageControler = new mgVuePageControler('mgVueMainContainer');
        mgPageControler.vinit();
    });
</script>

{if $scriptHtml}
    <script type="text/javascript">
        {$scriptHtml}
    </script>
{/if}

