var modulesgardenIsSend = false;

$(document).ready(function(){

    var $_GET = {};
    document.location.search.replace(/\??(?:([^=]+)=([^&]*)&?)/g, function () {
        function decode(s) {
            return decodeURIComponent(s.split("+").join(" "));
        }
        $_GET[decode(arguments[1])] = decode(arguments[2]);
    });
    var clientid = $_GET['userid'];
    $("div#clientsummarycontainer .clientssummarybox").first().parent().children().last().after(GetSMSTemplate(clientid));
    $("#mg-loading").hide();
});

function modulesGardenAddAlert(message, type)
{
    var alert = "<div id ='modulesgardenmessagesmscenter' class='" + type + "box'><strong><span class='title' style=\"text-transform: capitalize;\">" + type + "!</span></strong><br>" + message + "</div>";
    
    if ($('#modulesgardenmessagesmscenter').length > 0)
    {
        $('#modulesgardenmessagesmscenter').replaceWith(alert);
    }
    else
    {
        $('#profileContent').prepend(alert);
    }
}

function GetSMSTemplate(userid) {
    return "<div class=\"clientssummarybox\">"+
        "<div class=\"title\">Send SMS</div>"+
        "<form method=\"post\" id=\"modulesgarden-send-sms\">"+
        "<input id=\"send-sms-userid\" type=\"hidden\" value=\"" + userid + "\" name=\"userid\">"+
        "<input id=\"send-sms-userid\" type=\"hidden\" value=\"sms_center\" name=\"module\">"+
        "<input id=\"send-sms-userid\" type=\"hidden\" value=\"api\" name=\"mg-page\">"+
        "<input id=\"send-sms-userid\" type=\"hidden\" value=\"sendMessage\" name=\"mg-action\">"+
        "<input id=\"send-sms-userid\" type=\"hidden\" value=\"sendMessageFromProfile\" name=\"mg-function\">"+
        "<div align=\"center\">"+
        "<textarea name=\"message\" rows=\"6\" style=\"width:90%;\"></textarea><br>"+
        "<button type=\"button\" name=\"sendsms\" class=\"button btn btn-default\" onclick=\"modulesGardenSendSms();\">"+
        '<img id="mg-loading" src="../assets/img/spinner.gif" /> Send</button>' + 
        "</div>"+
        "</form>"+
        "</div>";
}


function modulesGardenSendSms()
{
    if (modulesgardenIsSend == false && $('textarea[name=message]').val().replace(/\s+/, "") != '') {
        modulesgardenIsSend = true;
        $("#mg-loading").show();
        
        var dir = $('#controllerAction').attr('value');
        
        $.ajax({
            type: "POST",
            url: dir,
            data: $("#modulesgarden-send-sms").serialize(),
            success: function(data)
            {
                $("#mg-loading").hide();
                modulesgardenIsSend = false;
                if(data.data.status == 'success')
                {
                    modulesGardenAddAlert(data.data.message, data.data.status);
                    $('textarea[name=message]').val('');
                }
                else if (data.data.status == 'error')
                {
                    modulesGardenAddAlert(data.data.message, data.data.status);
                }
                else
                {
                    modulesGardenAddAlert(data, 'error');
                }
            }
        });
    }
}

