function testConnection(vueObj, params, event)
{
    
    submoduleName = $(event.target).parents('tr').first().attr('actionid'); // wyszukanie 
    if (mgPageControler.vueLoader.isLoadSpiner(event) === false)
    {
        return;
    }
    mgPageControler.vueLoader.refreshUrl();
    mgPageControler.vueLoader.showSpinner(event);
    mgPageControler.vueLoader.refreshUrl();
    mgPageControler.vueLoader.addUrlComponent('loadData', 'testConnectionButton');
    mgPageControler.vueLoader.addUrlComponent('ajax', '1');
    mgPageControler.vueLoader.addUrlComponent('submodule',submoduleName);
    
    data = params;
    $.post(mgPageControler.vueLoader.targetUrl, data).done(function(data) {
                data = data.data;
                mgPageControler.vueLoader.hideSpinner(event);
                mgPageControler.vueLoader.addAlert(data.status, data.message);
                }
            );
}

function redirectStepData(targetId,nextPage, event)
{
    var self =  mgPageControler.vueLoader;
    var formTargetId = ($('#'+targetId)[0].tagName === 'FORM') ? targetId : $('#'+targetId).find('form').attr('id');
    if(formTargetId){
        if (self.isLoadSpiner(event) === false)
        {
            return;
        }
        self.showSpinner(event);
        var formCont = new mgFormControler(formTargetId);
        var formData = formCont.getFieldsData();
        formData = self.addMassActionsToData(formData);
        self.refreshUrl();
        self.addUrlComponent('loadData', formTargetId);
        self.addUrlComponent('namespace', getItemNamespace(formTargetId));
        self.addUrlComponent('index', getItemIndex(formTargetId));
        self.addUrlComponent('ajax', '1');
        self.addUrlComponent('mgForm', $('#'+formTargetId).attr('mgformaction'));
        self.addUrlComponent('stepPage', nextPage);
        $.post(self.targetUrl, formData)
        .done(function( data ) {
            data = data.data;
            self.hideSpinner(event);
            
            if(data.status === 'success'){
                self.showModal = false;
                self.runRefreshActions();
                self.cleanMassActions();
                self.addAlert(data.status, data.message);
                formCont.updateFieldsValidationMessages([]);
                var url = window.location.protocol+'//'+window.location.host+window.location.pathname+'?module=sms_center&mg-page=massSms&mg-action='+nextPage;
                window.location.href=url;
            }else if(data.rawData !== undefined && data.rawData.FormValidationErrors !== undefined) {
                formCont.updateFieldsValidationMessages(data.rawData.FormValidationErrors);
            }else if( data.status === 'error') {
                self.addAlert(data.status, data.message);
            }
        });
    }
    else{
        //todo error reporting
    }
    
}

function saveFilterStepData(targetId,nextPage, event)
{
    
    var self =  mgPageControler.vueLoader;
    var formTargetId = ($('#'+targetId)[0].tagName === 'FORM') ? targetId : $('#'+targetId).find('form').attr('id');
    if(formTargetId){
        if (self.isLoadSpiner(event) === false)
        {
            return;
        }
        self.showSpinner(event);
        var formCont = new mgFormControler(formTargetId);
        var formData = formCont.getFieldsData();
        formData = self.addMassActionsToData(formData);
        self.refreshUrl();
        self.addUrlComponent('loadData', formTargetId);
        self.addUrlComponent('namespace', getItemNamespace(formTargetId));
        self.addUrlComponent('index', getItemIndex(formTargetId));
        self.addUrlComponent('ajax', '1');
        self.addUrlComponent('mgForm', $('#'+formTargetId).attr('mgformaction'));
        self.addUrlComponent('saveConfiguration', 'true');
        $.post(self.targetUrl, formData)
        .done(function( data ) {
            data = data.data;
            self.hideSpinner(event);
            if(data.status === 'success'){
                self.showModal = false;
                self.runRefreshActions();
                self.cleanMassActions();
                self.addAlert(data.status, data.message);
                formCont.updateFieldsValidationMessages([]);
                var url = window.location.protocol+'//'+window.location.host+window.location.pathname+'?module=sms_center&mg-page=massSms&mg-action='+nextPage;
                //window.location.href=url;
            }else if(data.status === 'error'){
                self.addAlert(data.status, data.message);
            }else if(data.rawData.FormValidationErrors !== undefined) {
                formCont.updateFieldsValidationMessages(data.rawData.FormValidationErrors);
            }
        });
    }
    else{
        //todo error reporting
    }
    
}


function customSubmitForm(targetId, event) {
    var self =  mgPageControler.vueLoader;
    var formTargetId = ($('#'+targetId)[0].tagName === 'FORM') ? targetId : $('#'+targetId).find('form').attr('id');
    if(formTargetId){
        if (self.isLoadSpiner(event) === false)
        {
            return;
        }
        self.showSpinner(event);
        var formCont = new mgFormControler(formTargetId);
        var formData = formCont.getFieldsData();
        formData = self.addMassActionsToData(formData);
        self.refreshUrl();
        self.addUrlComponent('loadData', formTargetId);
         self.addUrlComponent('send', false);
        self.addUrlComponent('namespace', getItemNamespace(formTargetId));
        self.addUrlComponent('index', getItemIndex(formTargetId));
        self.addUrlComponent('ajax', '1');
        self.addUrlComponent('mgForm', $('#'+formTargetId).attr('mgformaction'));
        $.post(self.targetUrl, formData)
        .done(function( data ) {
            data = data.data;
            self.hideSpinner(event);
            self.$nextTick(function() {
                if (data.callBackFunction && typeof window[data.callBackFunction] === "function") {
                    window[data.callBackFunction](data, targetId, event);
                }
            });
            if(data.status === 'success'){
                self.showModal = false;
                self.runRefreshActions();
                self.cleanMassActions();
                self.addAlert(data.status, data.message);
                formCont.updateFieldsValidationMessages([]);
            }else if(data.rawData !== undefined && data.rawData.FormValidationErrors !== undefined) {
                formCont.updateFieldsValidationMessages(data.rawData.FormValidationErrors);
            }else if( data.status === 'error') {
                self.addAlert(data.status, data.message);
            }
        });
    }
    else{
        //todo error reporting
    }
}

$(document).on('ready', function() {
    countChars();
  });

function countChars()
{
    var val = $('[name=viewTemplate]').val().replace(/{[^}]*}/g, "");
    var count = val.length;
    if (val.length != $('[name=viewTemplate]').val().length){
        count = count.toString() + "+";
    }

    $('#charsCounter').text(count);
}