var getClientTokens = function (tabs)
{
    var dir = $('#controllerAction').attr('value');

    var result = null
    $.ajax({
        type: "POST",
        url: dir,
        async: false,
        data: {
            module: 'sms_center',
            'mg-page': 'api',
            'mg-action': 'actionCheckClientStatus',
            clients: tabs,
        },
        success: function (data)
        {
            if (data.data && data.data.tokens)
            {
                result = data.data.tokens;
               
            }else{
                console.log(data);
            }
        },
    });
    
    return result;
};

var getOrderTokens = function (tab)
{
    var dir = $('#controllerAction').attr('value');
    var result = null;
    $.ajax({
        type: "POST",
        url: dir,
        async: false,
        data: {
            module: 'sms_center',
            'mg-page': 'api',
            'mg-action': 'actionCheckOrderStatus',
            orderToken: 'true',
            tabId: tab
        },
        success: function (data)
        {
            result = data.data;
        },
    });
    return result;
};

var getInvoiceTokens = function (tab)
{
    var dir = $('#controllerAction').attr('value');
    var result = null;
    $.ajax({
        type: "POST",
        url: dir,
        async: false,
        data: {
            module: 'sms_center',
            'mg-page': 'api',
            'mg-action': 'actionCheckInvoicetatus',
            orderToken: 'true',
            tabId: tab
        },
        success: function (data)
        {
            result = data.data;
        },
    });
    return result;
};

var getToken = function (tid, type)
{
    var dir = $('#controllerAction').attr('value');
    var result = null;
    $.ajax({
        type: "POST",
        url: dir,
        async: false,
        data: {
            module: 'sms_center',
            'mg-page': 'api',
            'mg-action': 'getToken',
            id: tid,
            type: type,
        },
        success: function (data)
        {
            result = data;
        },
    });
    return result;
};
