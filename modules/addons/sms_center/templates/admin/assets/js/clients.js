$(document).ready(function () {
    var tabs = getClientId();
    var result = getClientTokens(tabs);
    setClientStatus(result);
    
});


var setClientStatus = function (statuses)
{
    var first = false;
    $("#sortabletbl0 > tbody > tr").each(function () {
        if (!first) {
            first = true;
            $(this).find('th:eq(7)').after('<th>SMS Activation Status</th>');
        } else {
            var clientid = $(this).find('td:eq(1) > a').html();
            var td = $(this);
            td.find('td:eq(7)').after('<td class="mg-loading"></td>');
            if (statuses[clientid] === true) {
                td.find('td:eq(8)').remove();
                td.find('td:eq(7)').after('<td style="color: #cc0000;">Unconfirmed</td>');
            } else {
                td.find('td:eq(8)').remove();
                td.find('td:eq(7)').after('<td style="color: #779500;">Confirmed</td>');
            }
        }
    });
};

var getClientId = function()
{
    var tab = [];
    var first = false;
    $("#sortabletbl0 > tbody > tr").each(function () {
        if (!first) {
            first = true;
        } else {
            var clientid = $(this).find('td:eq(1) > a').html();
            tab.push(clientid);

        }
    });
    return tab;
}