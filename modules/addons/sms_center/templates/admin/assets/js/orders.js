$(document).ready(function(){
    
    var tabs = getOrderIds();
    var res = getOrderTokens(tabs);
    if(res.tokens)
    {
        setOrderStatus(res.tokens);
    }
});

var setOrderStatus = function (statuses)
{
    var first = false;
    $("#sortabletbl0 > tbody > tr").each(function(){
        if(!first) {
            first = true;
            $(this).find('th:eq(7)').after('<th>SMS Activation Status</th>');
        } else {
            var orderid = $(this).find('td:eq(1) > a > b').html();
            var td = $(this);
            td.find('td:eq(7)').after('<td class="mg-loading"><img src="../modules/addons/sms_center/templates/admin/assets/img/ajax-loader.gif"></td>');
            
            if (statuses[orderid] === true) {
                td.find('td:eq(8)').remove();
                td.find('td:eq(7)').after('<td style="color: #cc0000;">Unconfirmed</td>');
            } else {
                td.find('td:eq(8)').remove();
                td.find('td:eq(7)').after('<td style="color: #779500;">Confirmed</td>');
            }
        }
    });
}

var getOrderIds = function ()
{
    var tab = [];
    var first = false;
    $("#sortabletbl0 > tbody > tr").each(function(){
        if(!first) {
            first = true;
        } else {
            var orderid = $(this).find('td:eq(1) > a > b').html();
            tab.push(orderid);
        }
    });
    return tab;
}