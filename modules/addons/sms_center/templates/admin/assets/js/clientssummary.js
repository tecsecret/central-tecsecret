$(document).ready(function(){
    var $_GET = {};
    document.location.search.replace(/\??(?:([^=]+)=([^&]*)&?)/g, function () {
        function decode(s) {
            return decodeURIComponent(s.split("+").join(" "));
        }
        $_GET[decode(arguments[1])] = decode(arguments[2]);
    });
    
    var dir = $('#controllerAction').attr('value');
    
    $.post(dir, {module: 'sms_center','mg-page': 'api', 'mg-action': 'actionStatus', clientid: $_GET['userid']})
        .done(function(data){
            if(data.data && data.data.status === true) {
                $('#clientsummarycontainer').children().first().after('SMS Verification Status: <strong style="color: #779500;">Confirmed</strong>');
            } else if (data.data && data.data.status === false) {
                $('#clientsummarycontainer').children().first().after('SMS Verification Status: <strong style="color: #cc0000;">Unconfirmed</strong>'+ 
                    '<p><form method=\"post\">'+          
                        '<input value=\"Verify\" name=\"verifyclient\" class=\"button\" type=\"submit\">'+
                    '</form></p>'
                    );
            }
        });
});