$(document).ready(function(){
    var $_GET = {};
    document.location.search.replace(/\??(?:([^=]+)=([^&]*)&?)/g, function () {
        function decode(s) {
            return decodeURIComponent(s.split("+").join(" "));
        }
        $_GET[decode(arguments[1])] = decode(arguments[2]);
    });
    var clientid = $_GET['userid'];
    checkCustomField(clientid);
});


function checkCustomField(uid)
{
    var dir = $('#controllerAction').attr('value');
    $.ajax({
            type: "POST",
            url: dir,
            data: {
                userid: uid,
                module: 'sms_center',
                'mg-page': 'api',
                'mg-action': 'actionCheckCF',
            },
            success: function(data)
            {
               if(data.data.status === false)
               {
                   $("#customfield"+data.data.cf).attr( 'checked', true );
               }
                       
            }
        });

}