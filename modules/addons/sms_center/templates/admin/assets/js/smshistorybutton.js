$(document).ready(function(){
   
   var userid = $('#userid').val();
   $('ul.client-tabs').append("<li class='tab'><a id='clientTab-14'>SMS History</a></li>");
   
   $('li').css('cursor', 'pointer');
   
   $('li > a#clientTab-14').click(function(){
       $('li').removeClass('active');
       $(this).parent().addClass('active');     
        addHistoryTable(userid); 
   }); 
   
   
});


function addHistoryTable(id){
    $.get("addonmodules.php?module=sms_center&mg-page=smsHistory&loadData=smsHistoryPage&namespace=ModulesGarden_SmsCenter_App_UI_SmsHistory_Pages_HistoryPage&index=smsHistoryPage&iDisplayLength=9999999&iDisplayStart=0&sSearch=&iSortCol_0=&sSortDir_0=&ajax=1", 
        {
            userid: id
        },
        function(data, status){
            var values = data.data.rawData.records;           
            addTable(values, id);    
        }
  );
}

function addTable(data, userId){
    var table = $("<table class='datatable'/>");
    table.append('<tr><th>Message</th><th>Date</th></tr>');
    table.addClass('table table--mob-collapsible dataTable no-footer dtr-column');

    if(filterData(data, userId).length != 0){
    $.each(filterData(data, userId), function(){
        table.append( '<tr><td>' + this.sms + '</td><td>' + this.date + '</td></tr>' );
    }); 
    } else {
        table.append( '<tr><td>No Records Found</td><td></td></tr>' );
    }
$('div.client-tabs').html(table);
}

function filterData(data, userId){
    var filteredData = [];
    $.each(data, function(){
       if(getUserId(this.client) === userId){
           filteredData.push(this);
       } 
    });
    return filteredData;
}

function getUserId(clientString){
    var table = clientString.split('#');
    table = table[1].split(' ');
    return table[0];
}

function showMore(str)
{
    $('#more'+str).show();
    $('#less'+str).hide();
}

function showLess(str)
{
    $('#less'+str).show();
    $('#more'+str).hide();
}