$(document).ready(function(){
    var $_GET = {};

    document.location.search.replace(/\??(?:([^=]+)=([^&]*)&?)/g, function () {
        function decode(s) {
            return decodeURIComponent(s.split("+").join(" "));
        }

        $_GET[decode(arguments[1])] = decode(arguments[2]);
    });
    
    var orderid = $_GET['id'];
    $('#ajaxchangeorderstatus').after(' <img id="mg-loading" src="../modules/addons/sms_center/templates/admin/assets/img/ajax-loader.gif">');
    
    var result = getToken(orderid, 'order');
    
    $("#mg-loading").remove();
    if(result.data.tokens !== true) {
        $('#ajaxchangeorderstatus').after(' SMS Verification Status: <span style="color: #779500;">Confirmed</span>');
    } else {
        $('#ajaxchangeorderstatus').after(' SMS Verification Status: <span style="color: #cc0000;">Unconfirmed</span>');
    }
});



