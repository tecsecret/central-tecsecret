$(document).ready(function(){
    
    var tab = getInvoiceId();
    var res = getInvoiceTokens(tab);
    if(res.tokens)
    {
        setInvoiceStatus(res.tokens);
    }
   
});

var setInvoiceStatus = function (statuses)
{
    var first = false;
    $("#sortabletbl0 > tbody > tr").each(function(){
        if(!first) {
            first = true;
            $(this).find('th:eq(6)').after('<th>SMS Activation Status</th>');
        } else {
            var invoiceid = $(this).find('td:eq(1) > a').html();
            var td = $(this);
            td.find('td:eq(6)').after('<td class="mg-loading"><img src="../modules/addons/sms_center/templates/admin/assets/img/ajax-loader.gif"></td>');
            
            if (statuses['invoice'][invoiceid] === true) {
                td.find('td:eq(7)').remove();
                td.find('td:eq(6)').after('<td style="color: #cc0000;">Unconfirmed</td>');
            } else {
                td.find('td:eq(7)').remove();
                td.find('td:eq(6)').after('<td style="color: #779500;">Confirmed</td>');
            }
        }
    });
}

var getInvoiceId = function()
{
    var tab = [];
    var first = false;
    $("#sortabletbl0 > tbody > tr").each(function(){
        if(!first) {
            first = true;
        } else {
            var invoiceid = $(this).find('td:eq(1) > a').html();
            tab.push(invoiceid);
        }
    });
    return tab;s
}