$(document).ready(function(){
    var $_GET = {};

    document.location.search.replace(/\??(?:([^=]+)=([^&]*)&?)/g, function () {
        function decode(s) {
            return decodeURIComponent(s.split("+").join(" "));
        }

        $_GET[decode(arguments[1])] = decode(arguments[2]);
    });
    
    var invoiceid = $_GET['id'];
    $('.textgreen, .textred, .textgrey, .textgold, .textblack, .textblue').after('<p id="mg-loading"><img src="../modules/addons/sms_center/templates/admin/assets/img/ajax-loader.gif"></p>');
    
    var result = getToken(invoiceid, 'invoice');
    $("#mg-loading").remove();

    if(result.data.tokens !== true) {
        $('.textgreen, .textred, .textgrey, .textgold, .textblack, .textblue').after('<br>SMS Verification Status: <strong style="color: #779500;">Confirmed</strong>');
    } else {
        $('.textgreen, .textred, .textgrey, .textgold, .textblack, .textblue').after('<br>SMS Verification Status: <strong style="color: #cc0000;">Unconfirmed</strong>');
    }

});