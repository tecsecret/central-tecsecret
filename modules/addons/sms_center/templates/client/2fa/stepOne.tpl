<h2>{$lang->absoluteT('SMS Center Two-Factor Authentication')}</h2>
    {if $invalid == 'true'}

        <div class="errorbox">
            <div class="alert alert-danger">
            <strong>
                {$lang->absoluteT('An Error Occurred. Please Try Again...')}
            </strong><br/>
        {if isset($errorMsg)}
            {$errorMsg}
            {else}
        {$lang->absoluteT('Your mobile number is incorrect.')}
        {/if}
            </div>
        </div>
    {/if} 

{if $result == 'error'}
    <div class="errorbox">
        <div class="alert alert-danger">
        <strong>{$lang->absoluteT('An Error Occurred. Please Try Again...')}
        </strong><br />
        {$lang->absoluteT('SMS message with activation code could not be sent. Please try again later.')}
        </div>
   </div>
{/if}

<p>{$lang->absoluteT('Please enter the code we sent you via SMS message.')}</p>

{if isset($timesent)}
    <p>{$lang->absoluteT('Please note that new activation code could be sent only once every')} {$first_sms_validity} {$lang->absoluteT('minutes.')} {$lang->absoluteT('Expiry date of the current code:')} {$timesent}</p>
{/if}
<form {if $usertype == 'admin'} onsubmit="dialogSubmit();return false" {else} method="post" {/if}>
<input type="hidden" name="module" value="smscenter" />
<table>
<tr><td width="70">{$lang->absoluteT('SMS Code')}</td><td><input type="text" name="sms_code" size="30" id="sms_code" /></td></tr>
</table>
    <input type="submit" style="margin-top: 10px" value="{$lang->absoluteT('Activate2FA')}&raquo;" class="btn btn-primary large" />
</form>
