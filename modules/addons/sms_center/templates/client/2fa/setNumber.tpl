<div >
    <h2>{$lang->T('SMS Center Two-Factor Authentication')}</h2>
    {if isset($invalid)}
{if $errorMsg}
    <div class="errorbox">
        <div class="alert alert-danger">
            {$errorMsg}
        </div>
    </div>
    {else}
    <div class="errorbox">
        <div class="alert alert-danger">
            {$lang->absoluteT('An Error Occurred. Please Try Again...')}<br/>
            {$lang->absoluteT('Your mobile number is incorrect.')}
        </div>
    </div>
{/if}

    {/if}
    <p>{$lang->absoluteT('To begin with SMS Center 2FA simply provide your mobile number.')}</p>
    <form {if $usertype == 'admin'} onsubmit="dialogSubmit();return false" {else} method="post" {/if}>
    <input type="hidden" name="module" value="smscenter" />
    <table>
    <tr><td width="100">Mobile Number</td><td>+ <input style="margin-right: 10px; width: 25px;" type="text" name="country_code" id="country_code" />
    <input type="text" name="sms_number" size="30" id="sms_number" /></td></tr>
    </table>
    <p align="center"><input type="submit" value="{$lang->absoluteT('Activate2FA')}&raquo;" class="btn btn-primary large" /></p>
    </form>

</div>


