{if isset($error)}
<h4 style='text-align: center;'>{$lang->absoluteT('SMS could not be sent. Please try again later.')}</h4>
{/if}
{if isset($timesent)}
<p>{$lang->absoluteT('Please note that new activation code could be sent only once every')} {$validity} {$lang->absoluteT('minutes.')} {$lang->absoluteT('Expiry date of the current code:')} {$timesent}</p>
{/if}
<br>
<form method="post" action="dologin.php">
    <div class="form-group">
        <label for="2fatoken">{$lang->absoluteT('Verification SMS Code')}</label>
        <input type="text" name="sms_code" class="form-control" id="2fatoken" aria-describedby="2faHelp" placeholder="Token">
    </div>
    <div class="form-group"  >
        <div class="col-lg-offset-5">
            <input type="submit" value="{$lang->absoluteT('Login2FA')}&raquo;" class="btn btn-primary"/>
        </div>
    </div>

</form>