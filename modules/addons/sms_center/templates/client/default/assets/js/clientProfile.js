$(document).ready(function(){
 
 checkCustomField();
 
});

function checkCustomField()
{
    $.ajax({
            type: "POST",
            url: "index.php",
            data: {
                m: 'sms_center',
                'mg-page': 'configuration',
                'mg-action': 'actionCheckCF',
            },
            success: function(data)
            {
               if(data.data.status === false)
               {
                   $("#customfield"+data.data.cf).attr( 'checked', true );
               }
                       
            }
        });
}