{**********************************************************************
* SmsCenter product developed. (2017-08-31)
* *
*
*  CREATED BY MODULESGARDEN       ->       http://modulesgarden.com
*  CONTACT                        ->       contact@modulesgarden.com
*
*
* This software is furnished under a license and may be used and copied
* only  in  accordance  with  the  terms  of such  license and with the
* inclusion of the above copyright notice.  This software  or any other
* copies thereof may not be provided or otherwise made available to any
* other person.  No title to and  ownership of the  software is  hereby
* transferred.
*
*
**********************************************************************}

{**
* This is default search form template for widget elements
*
* @author Sławomir Miśkowicz <slawomir@modulesgarden.com>
*}

{literal}
    <div data-toggler-options="toggleClass: is-open" class="input-group input-group--toggle">
        <input type="text" data-toggler-options="toggleClass: hidden" placeholder="{/literal}{$MGLANG->absoluteT('searchPlacecholder')}{literal}" class="form-control hidden" @keyup.enter="searchData" id="groupListFilter">
        <button data-toggler="selectors: .btn .btn__icon, .form-control; lu-container: .input-group;" type="button" class="input-group__btn btn btn--sm btn--icon btn--link">
            <i data-toggler-options="toggleClass: zmdi-search zmdi-close;" class="btn__icon zmdi zmdi-search"></i>
        </button>
    </div>
{/literal}
