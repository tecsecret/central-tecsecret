{**********************************************************************
* SmsCenter product developed. (2017-08-31)
* *
*
*  CREATED BY MODULESGARDEN       ->       http://modulesgarden.com
*  CONTACT                        ->       contact@modulesgarden.com
*
*
* This software is furnished under a license and may be used and copied
* only  in  accordance  with  the  terms  of such  license and with the
* inclusion of the above copyright notice.  This software  or any other
* copies thereof may not be provided or otherwise made available to any
* other person.  No title to and  ownership of the  software is  hereby
* transferred.
*
*
**********************************************************************}

{**
* This is default search form template for widget elements
*
* @author Sławomir Miśkowicz <slawomir@modulesgarden.com>
*}

{literal}
    <div class="top__search input-group">
        <span class="icon-sm input-group__icon">
            <i class="zmdi zmdi-search "></i>
        </span>
        <input id="groupListFilter" placeholder="{/literal}{$MGLANG->absoluteT('searchPlacecholder')}{literal}" value="" @keyup.enter="searchData" class="form-control input-group__form-control table-search">
    </div>
{/literal}
