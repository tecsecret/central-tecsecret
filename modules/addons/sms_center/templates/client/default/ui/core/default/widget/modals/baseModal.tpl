{**********************************************************************
* SmsCenter product developed. (2017-11-02)
* *
*
*  CREATED BY MODULESGARDEN       ->       http://modulesgarden.com
*  CONTACT                        ->       contact@modulesgarden.com
*
*
* This software is furnished under a license and may be used and copied
* only  in  accordance  with  the  terms  of such  license and with the
* inclusion of the above copyright notice.  This software  or any other
* copies thereof may not be provided or otherwise made available to any
* other person.  No title to and  ownership of the  software is  hereby
* transferred.
*
*
**********************************************************************}

{**
* @author Sławomir Miśkowicz <slawomir@modulesgarden.com>
*}

<div class="modal show modal--info modal--{$rawObject->getModalSize()} modal--zoomIn" id="confirmationModal">
    <div class="modal__dialog">
        <div class="modal__content" id="mgModalContainer">
            <div class="modal__top top">
                <div class="top__title type-4 text-danger">
                    <i class="zmdi zmdi-info-outline"></i>
                    {if $rawObject->isRawTitle()}{$rawObject->getRawTitle()}{elseif $rawObject->getTitle()}{$MGLANG->T('modal', $rawObject->getTitle())}{/if}
                </div>
                <div class="top__toolbar">
                    <button class="btn btn--xs btn--danger btn--icon btn--link btn--plain closeModal" data-dismiss="lu-modal" aria-label="Close">
                        <i class="btn__icon zmdi zmdi-close"></i>
                    </button>
                </div>
            </div>
            <div class="modal__body">
                {foreach from=$rawObject->getForms() item=form }
                    {$form->getHtml()}
                {/foreach}
            </div>
            <div class="modal__actions">
                {foreach from=$rawObject->getActionButtons() item=actionButton}
                    {$actionButton->getHtml()}
                {/foreach} 
            </div>
            <div class="modal__actions">
                {if ($isDebug eq true AND (count($MGLANG->getMissingLangs()) != 0))}{literal}
                    <div class="lu-row">
                        {/literal}{foreach from=$MGLANG->getMissingLangs() key=varible item=value}{literal}
                            <div class="lu-col-md-12"><b>{/literal}{$varible}{literal}</b> = '{/literal}{$value}{literal}';</div>
                        {/literal}{/foreach}{literal}
                    </div>
                {/literal}{/if}
            </div>
        </div>
    </div>
</div>
