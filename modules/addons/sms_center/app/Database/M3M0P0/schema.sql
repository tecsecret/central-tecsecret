--
-- `#prefix#MessageGroups`
--
CREATE TABLE IF NOT EXISTS `#prefix#MessageGroups` (
    `id`               int(10)      unsigned                NOT NULL AUTO_INCREMENT,
    `group_id`        VARCHAR(255) NOT NULL,
    `group`          VARCHAR(255) NOT NULL,
    `msg_name`            VARCHAR(255) NOT NULL,
    `enabled`            VARCHAR(255) NOT NULL DEFAULT 'off',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=#charset# DEFAULT COLLATE #collation#;
--
-- `#prefix#Templates`
--
CREATE TABLE IF NOT EXISTS `#prefix#Templates` (
    `id`               int(10)      unsigned                NOT NULL AUTO_INCREMENT,
    `group_id`        int(2) NOT NULL,
    `lang`          VARCHAR(255) NOT NULL,
    `message`            TEXT,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=#charset# DEFAULT COLLATE #collation#;
--
-- `#prefix#SubmoduleSettings`
--
CREATE TABLE IF NOT EXISTS `#prefix#SubmoduleSettings` (
    `id`               int(10)      unsigned                NOT NULL AUTO_INCREMENT,
    `setting`        VARCHAR(255) NOT NULL,
    `value`          VARCHAR(255) NOT NULL,
    `submodule`            VARCHAR(255) NOT NULL,
    `settingName`            VARCHAR(255) NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=#charset# DEFAULT COLLATE #collation#;
--
-- `#prefix#GeneralSettings`
--
CREATE TABLE IF NOT EXISTS `#prefix#GeneralSettings` (
    `id`               int(10)      unsigned                NOT NULL AUTO_INCREMENT,
    `setting`        VARCHAR(255),
    `value`          VARCHAR(255),
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=#charset# DEFAULT COLLATE #collation#;
--
-- `#prefix#SmsTasks`
--
CREATE TABLE IF NOT EXISTS `#prefix#SmsTasks` (
    `id`               int(10)      unsigned                NOT NULL AUTO_INCREMENT,
    `name`        VARCHAR(255) NOT NULL,
    `params`        TEXT NOT NULL,
    `parentid`        INT(11) NOT NULL,
    `repeats`        VARCHAR(255) NOT NULL,
    `status`        VARCHAR(255) NOT NULL,
    `date`        DATETIME,
    `lastrun`        DATETIME,
    `nextrun`          DATETIME,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=#charset# DEFAULT COLLATE #collation#;
--
-- `#prefix#AdminNotifications`
--
CREATE TABLE IF NOT EXISTS `#prefix#AdminNotifications` (
    `id`               int(10)      unsigned                NOT NULL AUTO_INCREMENT,
    `aid`        INT(11) NOT NULL,
    `enabled`        VARCHAR(255) ,
    `cc`        VARCHAR(255) ,
    `d_code`        INT(11) ,
    `mobile`        VARCHAR(255) ,
    `notifications`        TEXT,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=#charset# DEFAULT COLLATE #collation#;
--
-- `#prefix#AdminNotificationsRules`
--
CREATE TABLE IF NOT EXISTS `#prefix#AdminNotificationsRules` (
    `id`               int(10)      unsigned                NOT NULL AUTO_INCREMENT,
    `ruleId`        INT(11) ,
    `adminId`        INT(11) NOT NULL,
    `enabled`        VARCHAR(255) ,
    `method`        VARCHAR(255) ,
    `value`        TEXT ,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=#charset# DEFAULT COLLATE #collation#;
--
-- `#prefix#GatewayStatus`
--
CREATE TABLE IF NOT EXISTS `#prefix#GatewayStatus` (
    `id`               int(10)      unsigned                NOT NULL AUTO_INCREMENT,
    `submodule`        VARCHAR(255) NOT NULL,
    `status`        VARCHAR(255) ,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=#charset# DEFAULT COLLATE #collation#;
--
-- `#prefix#GatewayRules`
--
CREATE TABLE IF NOT EXISTS `#prefix#GatewayRules` (
    `id`               int(10)      unsigned                NOT NULL AUTO_INCREMENT,
    `countries`        TEXT,
    `submodule`        VARCHAR(255) ,
    `settingName`        VARCHAR(255) ,
    `status`        VARCHAR(255) ,
    `default`        VARCHAR(255) ,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=#charset# DEFAULT COLLATE #collation#;
--
-- `#prefix#Tokens`
--
CREATE TABLE IF NOT EXISTS `#prefix#Tokens` (
    `id`               int(10)      unsigned                NOT NULL AUTO_INCREMENT,
    `cid`       int(10),
    `token`        VARCHAR(255) ,
    `date`        DATETIME ,
    `type`        VARCHAR(255) ,
    `resend`        VARCHAR(255) ,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=#charset# DEFAULT COLLATE #collation#;
--
-- `#prefix#ClientConfiguration`
--
CREATE TABLE IF NOT EXISTS `#prefix#ClientConfiguration` (
    `id`               int(10)      unsigned                NOT NULL AUTO_INCREMENT,
    `cid`        int(10),
    `tpl_id`        int(10),
    `enabled`        VARCHAR(255) ,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=#charset# DEFAULT COLLATE #collation#;
--
-- `#prefix#MassMessageTemplates`
--
CREATE TABLE IF NOT EXISTS `#prefix#MassMessageTemplates` (
    `id`               int(10)      unsigned                NOT NULL AUTO_INCREMENT,
    `name`        VARCHAR(255) NOT NULL,
    `type`          VARCHAR(255),
    `message`            TEXT,
    `enabled`            VARCHAR(255),
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=#charset# DEFAULT COLLATE #collation#;
--
-- `#prefix#TwoFactoryAuth`
--
CREATE TABLE IF NOT EXISTS `#prefix#TwoFactoryAuth` (
    `id`               int(10)      unsigned                NOT NULL AUTO_INCREMENT,
    `clientid`        int(11) NOT NULL,
    `type`              VARCHAR(255),
    `code`             VARCHAR(255),
    `usertype`            VARCHAR(255),
    `expires`            DATETIME,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=#charset# DEFAULT COLLATE #collation#;
--
-- `#prefix#MassFilterSettings`
--
CREATE TABLE IF NOT EXISTS `#prefix#MassFilterSettings` (
    `id`               int(10)      unsigned                NOT NULL AUTO_INCREMENT,
    `setting`        VARCHAR(255),
    `value`              VARCHAR(255),
    `settingName`             VARCHAR(255),
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=#charset# DEFAULT COLLATE #collation#;
--
-- `#prefix#ClientLogginNotification`
--
CREATE TABLE IF NOT EXISTS `#prefix#ClientLoginNotification` (
    `id`                  int(10)      unsigned                NOT NULL AUTO_INCREMENT,
    `cid`                 int(10),
    `status`              VARCHAR(255),
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=#charset# DEFAULT COLLATE #collation#;
--
-- `#prefix#Logger`
--
CREATE TABLE IF NOT EXISTS `#prefix#Logger` (
    `id`            int(10) unsigned NOT NULL AUTO_INCREMENT,
    `id_ref`        int(10) NOT NULL,
    `id_type`       VARCHAR(255) NOT NULL,
    `type`          VARCHAR(255) NOT NULL,
    `level`         VARCHAR(255) NOT NULL,
    `date`          DATETIME DEFAULT null,
    `request`       TEXT NOT NULL,
    `response`      TEXT NOT NULL,
    `before_vars`   TEXT NOT NULL,
    `vars`          TEXT NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=#charset# DEFAULT COLLATE #collation#;