INSERT INTO `#prefix#MessageGroups` (`group_id`, `group`, `msg_name`, `enabled`) 
SELECT * FROM ( SELECT '0', 'admin', 'Admin Login Notification', 'off') AS tmp
WHERE NOT EXISTS (SELECT * FROM `#prefix#MessageGroups` WHERE `#prefix#MessageGroups`.`msg_name` LIKE 'Admin Login Notification') 
LIMIT 1 ;
INSERT INTO `#prefix#MessageGroups` (`group_id`, `group`, `msg_name`, `enabled`) 
SELECT * FROM ( SELECT '0', 'admin', 'Service Suspend Notification', 'off') AS tmp
WHERE NOT EXISTS (SELECT * FROM `#prefix#MessageGroups` WHERE `#prefix#MessageGroups`.`msg_name` LIKE 'Service Suspend Notification') 
LIMIT 1 ;
INSERT INTO `#prefix#MessageGroups` (`group_id`, `group`, `msg_name`, `enabled`) 
SELECT * FROM ( SELECT '0', 'admin', 'Order Paid Notification', 'off') AS tmp
WHERE NOT EXISTS (SELECT * FROM `#prefix#MessageGroups` WHERE `#prefix#MessageGroups`.`msg_name` LIKE 'Order Paid Notification') 
LIMIT 1 ;
INSERT INTO `#prefix#Templates` (`group_id`, `lang`, `message`)
SELECT * FROM ( SELECT (SELECT id FROM `#prefix#MessageGroups` WHERE `#prefix#MessageGroups`.`msg_name` LIKE 'Admin Login Notification'), 'default', 'Notification: {$admin_first_name} {$admin_last_name}  {$admin_username} admin logged in to the website.') AS tmp
WHERE NOT EXISTS (SELECT * FROM `#prefix#Templates` WHERE `#prefix#Templates`.`group_id` = (SELECT id FROM `#prefix#MessageGroups` WHERE `#prefix#MessageGroups`.`msg_name` LIKE 'Admin Login Notification'))
LIMIT 1 ;
INSERT INTO `#prefix#Templates` (`group_id`, `lang`, `message`)
SELECT * FROM ( SELECT (SELECT id FROM `#prefix#MessageGroups` WHERE `#prefix#MessageGroups`.`msg_name` LIKE 'Service Suspend Notification'), 'default', 'Notification: This product/service has been suspended. Product/Service: {$service_product_name} {if $service_domain}Domain: {$service_domain}{/if} Amount: {$service_first_payment_amount} Due Date: {$service_next_due_date} Suspension Reason: {$service_suspension_reason}') AS tmp
WHERE NOT EXISTS (SELECT * FROM `#prefix#Templates` WHERE `#prefix#Templates`.`group_id` = (SELECT id FROM `#prefix#MessageGroups` WHERE `#prefix#MessageGroups`.`msg_name` LIKE 'Service Suspend Notification'))
LIMIT 1 ;
INSERT INTO `#prefix#Templates` (`group_id`, `lang`, `message`)
SELECT * FROM ( SELECT (SELECT id FROM `#prefix#MessageGroups` WHERE `#prefix#MessageGroups`.`msg_name` LIKE 'Order Paid Notification'), 'default', 'The payment for order {$order_number} has been successfully processed.') AS tmp
WHERE NOT EXISTS (SELECT * FROM `#prefix#Templates` WHERE `#prefix#Templates`.`group_id` = (SELECT id FROM `#prefix#MessageGroups` WHERE `#prefix#MessageGroups`.`msg_name` LIKE 'Order Paid Notification'))
LIMIT 1 ;