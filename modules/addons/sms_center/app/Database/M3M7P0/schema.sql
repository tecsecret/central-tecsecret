--
-- #prefix#SmsHistory
--
CREATE TABLE IF NOT EXISTS `#prefix#SmsHistory` (
    `id`               int(10)      unsigned                NOT NULL AUTO_INCREMENT,
    `params`        TEXT NOT NULL,
    `client`        TEXT NOT NULL,
    `date`        DATETIME,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=#charset# DEFAULT COLLATE #collation#;
--
-- #prefix#ClientVerification
--
CREATE TABLE IF NOT EXISTS `#prefix#ClientVerification` (
    `id`                int(10)      unsigned                NOT NULL AUTO_INCREMENT,
    `cid`               int(10) NOT NULL,
    `date`              DATE NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=#charset# DEFAULT COLLATE #collation#;