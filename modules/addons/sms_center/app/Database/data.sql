INSERT INTO `#prefix#MessageGroups` (`group_id`, `group`, `msg_name`, `enabled`) 
SELECT * FROM ( SELECT '0', 'domain', 'Expired Domain Notice', 'off' ) AS tmp
WHERE NOT EXISTS (SELECT * FROM `#prefix#MessageGroups` WHERE `#prefix#MessageGroups`.`msg_name` LIKE 'Expired Domain Notice') 
LIMIT 1 ;
INSERT INTO `#prefix#MessageGroups` (`group_id`, `group`, `msg_name`, `enabled`) 
SELECT * FROM ( SELECT '0', 'domain', 'Domain Registration Confirmation', 'off') AS tmp
WHERE NOT EXISTS (SELECT * FROM `#prefix#MessageGroups` WHERE `#prefix#MessageGroups`.`msg_name` LIKE 'Domain Registration Confirmation') 
LIMIT 1 ;
INSERT INTO `#prefix#MessageGroups` (`group_id`, `group`, `msg_name`, `enabled`) 
SELECT * FROM ( SELECT '0', 'domain', 'Domain Renewal Confirmation', 'off') AS tmp
WHERE NOT EXISTS (SELECT * FROM `#prefix#MessageGroups` WHERE `#prefix#MessageGroups`.`msg_name` LIKE 'Domain Renewal Confirmation') 
LIMIT 1 ;
INSERT INTO `#prefix#MessageGroups` (`group_id`, `group`, `msg_name`, `enabled`) 
SELECT * FROM ( SELECT '0', 'domain', 'Domain Transfer Completed', 'off') AS tmp
WHERE NOT EXISTS (SELECT * FROM `#prefix#MessageGroups` WHERE `#prefix#MessageGroups`.`msg_name` LIKE 'Domain Transfer Completed') 
LIMIT 1 ;
INSERT INTO `#prefix#MessageGroups` (`group_id`, `group`, `msg_name`, `enabled`) 
SELECT * FROM ( SELECT '0', 'domain', 'Domain Transfer Initiated', 'off') AS tmp
WHERE NOT EXISTS (SELECT * FROM `#prefix#MessageGroups` WHERE `#prefix#MessageGroups`.`msg_name` LIKE 'Domain Transfer Initiated') 
LIMIT 1 ;
INSERT INTO `#prefix#MessageGroups` (`group_id`, `group`, `msg_name`, `enabled`) 
SELECT * FROM ( SELECT '0', 'domain', 'Upcoming Domain Renewal Notice', 'off') AS tmp
WHERE NOT EXISTS (SELECT * FROM `#prefix#MessageGroups` WHERE `#prefix#MessageGroups`.`msg_name` LIKE 'Upcoming Domain Renewal Notice') 
LIMIT 1 ;
INSERT INTO `#prefix#MessageGroups` (`group_id`, `group`, `msg_name`, `enabled`) 
SELECT * FROM ( SELECT '0', 'domain', 'Domain Transfer Failed', 'off') AS tmp
WHERE NOT EXISTS (SELECT * FROM `#prefix#MessageGroups` WHERE `#prefix#MessageGroups`.`msg_name` LIKE 'Domain Transfer Failed') 
LIMIT 1 ;
INSERT INTO `#prefix#MessageGroups` (`group_id`, `group`, `msg_name`, `enabled`) 
SELECT * FROM ( SELECT '0', 'affiliate', 'Affiliate Monthly Referrals Report', 'off') AS tmp
WHERE NOT EXISTS (SELECT * FROM `#prefix#MessageGroups` WHERE `#prefix#MessageGroups`.`msg_name` LIKE 'Affiliate Monthly Referrals Report') 
LIMIT 1 ;
INSERT INTO `#prefix#MessageGroups` (`group_id`, `group`, `msg_name`, `enabled`) 
SELECT * FROM ( SELECT '0', 'general', 'Credit Warning', 'off') AS tmp
WHERE NOT EXISTS (SELECT * FROM `#prefix#MessageGroups` WHERE `#prefix#MessageGroups`.`msg_name` LIKE 'Credit Warning') 
LIMIT 1 ;
INSERT INTO `#prefix#MessageGroups` (`group_id`, `group`, `msg_name`, `enabled`) 
SELECT * FROM ( SELECT '0', 'general', 'Access Details Reset', 'off') AS tmp
WHERE NOT EXISTS (SELECT * FROM `#prefix#MessageGroups` WHERE `#prefix#MessageGroups`.`msg_name` LIKE 'Access Details Reset') 
LIMIT 1 ;
INSERT INTO `#prefix#MessageGroups` (`group_id`, `group`, `msg_name`, `enabled`) 
SELECT * FROM ( SELECT '0', 'general', 'Client Signup Email', 'off') AS tmp
WHERE NOT EXISTS (SELECT * FROM `#prefix#MessageGroups` WHERE `#prefix#MessageGroups`.`msg_name` LIKE 'Client Signup Email') 
LIMIT 1 ;
INSERT INTO `#prefix#MessageGroups` (`group_id`, `group`, `msg_name`, `enabled`) 
SELECT * FROM ( SELECT '0', 'general', 'Automated Password Reset', 'off') AS tmp
WHERE NOT EXISTS (SELECT * FROM `#prefix#MessageGroups` WHERE `#prefix#MessageGroups`.`msg_name` LIKE 'Automated Password Reset') 
LIMIT 1 ;
INSERT INTO `#prefix#MessageGroups` (`group_id`, `group`, `msg_name`, `enabled`) 
SELECT * FROM ( SELECT '0', 'general', 'Credit Card Expiring Soon', 'off') AS tmp
WHERE NOT EXISTS (SELECT * FROM `#prefix#MessageGroups` WHERE `#prefix#MessageGroups`.`msg_name` LIKE 'Credit Card Expiring Soon') 
LIMIT 1 ;
INSERT INTO `#prefix#MessageGroups` (`group_id`, `group`, `msg_name`, `enabled`) 
SELECT * FROM ( SELECT '0', 'general', 'Order Confirmation', 'off') AS tmp
WHERE NOT EXISTS (SELECT * FROM `#prefix#MessageGroups` WHERE `#prefix#MessageGroups`.`msg_name` LIKE 'Order Confirmation') 
LIMIT 1 ;
INSERT INTO `#prefix#MessageGroups` (`group_id`, `group`, `msg_name`, `enabled`) 
SELECT * FROM ( SELECT '0', 'general', 'Password Reset Confirmation', 'off') AS tmp
WHERE NOT EXISTS (SELECT * FROM `#prefix#MessageGroups` WHERE `#prefix#MessageGroups`.`msg_name` LIKE 'Password Reset Confirmation') 
LIMIT 1 ;
INSERT INTO `#prefix#MessageGroups` (`group_id`, `group`, `msg_name`, `enabled`) 
SELECT * FROM ( SELECT '0', 'general', 'Password Reset Reminder', 'off') AS tmp
WHERE NOT EXISTS (SELECT * FROM `#prefix#MessageGroups` WHERE `#prefix#MessageGroups`.`msg_name` LIKE 'Password Reset Reminder') 
LIMIT 1 ;
INSERT INTO `#prefix#MessageGroups` (`group_id`, `group`, `msg_name`, `enabled`) 
SELECT * FROM ( SELECT '0', 'general', 'Password Reset Validation', 'off') AS tmp
WHERE NOT EXISTS (SELECT * FROM `#prefix#MessageGroups` WHERE `#prefix#MessageGroups`.`msg_name` LIKE 'Password Reset Validation') 
LIMIT 1 ;
INSERT INTO `#prefix#MessageGroups` (`group_id`, `group`, `msg_name`, `enabled`) 
SELECT * FROM ( SELECT '0', 'general', 'Quote Accepted', 'off') AS tmp
WHERE NOT EXISTS (SELECT * FROM `#prefix#MessageGroups` WHERE `#prefix#MessageGroups`.`msg_name` LIKE 'Quote Accepted') 
LIMIT 1 ;
INSERT INTO `#prefix#MessageGroups` (`group_id`, `group`, `msg_name`, `enabled`) 
SELECT * FROM ( SELECT '0', 'general', 'Quote Accepted Notification', 'off') AS tmp
WHERE NOT EXISTS (SELECT * FROM `#prefix#MessageGroups` WHERE `#prefix#MessageGroups`.`msg_name` LIKE 'Quote Accepted Notification') 
LIMIT 1 ;
INSERT INTO `#prefix#MessageGroups` (`group_id`, `group`, `msg_name`, `enabled`) 
SELECT * FROM ( SELECT '0', 'general', 'Customer Information', 'off') AS tmp
WHERE NOT EXISTS (SELECT * FROM `#prefix#MessageGroups` WHERE `#prefix#MessageGroups`.`msg_name` LIKE 'Customer Information') 
LIMIT 1 ;
INSERT INTO `#prefix#MessageGroups` (`group_id`, `group`, `msg_name`, `enabled`) 
SELECT * FROM ( SELECT '0', 'general', 'Quote Delivery with PDF', 'off') AS tmp
WHERE NOT EXISTS (SELECT * FROM `#prefix#MessageGroups` WHERE `#prefix#MessageGroups`.`msg_name` LIKE 'Quote Delivery with PDF') 
LIMIT 1 ;
INSERT INTO `#prefix#MessageGroups` (`group_id`, `group`, `msg_name`, `enabled`) 
SELECT * FROM ( SELECT '0', 'general', 'Unsubscribe Confirmation', 'off') AS tmp
WHERE NOT EXISTS (SELECT * FROM `#prefix#MessageGroups` WHERE `#prefix#MessageGroups`.`msg_name` LIKE 'Unsubscribe Confirmation') 
LIMIT 1 ;
INSERT INTO `#prefix#MessageGroups` (`group_id`, `group`, `msg_name`, `enabled`) 
SELECT * FROM ( SELECT '0', 'invoice', 'Credit Card Invoice Created', 'off') AS tmp
WHERE NOT EXISTS (SELECT * FROM `#prefix#MessageGroups` WHERE `#prefix#MessageGroups`.`msg_name` LIKE 'Credit Card Invoice Created') 
LIMIT 1 ;
INSERT INTO `#prefix#MessageGroups` (`group_id`, `group`, `msg_name`, `enabled`) 
SELECT * FROM ( SELECT '0', 'invoice', 'Credit Card Payment Confirmation', 'off') AS tmp
WHERE NOT EXISTS (SELECT * FROM `#prefix#MessageGroups` WHERE `#prefix#MessageGroups`.`msg_name` LIKE 'Credit Card Payment Confirmation') 
LIMIT 1 ;
INSERT INTO `#prefix#MessageGroups` (`group_id`, `group`, `msg_name`, `enabled`) 
SELECT * FROM ( SELECT '0', 'invoice', 'Credit Card Payment Due', 'off') AS tmp
WHERE NOT EXISTS (SELECT * FROM `#prefix#MessageGroups` WHERE `#prefix#MessageGroups`.`msg_name` LIKE 'Credit Card Payment Due') 
LIMIT 1 ;
INSERT INTO `#prefix#MessageGroups` (`group_id`, `group`, `msg_name`, `enabled`) 
SELECT * FROM ( SELECT '0', 'invoice', 'Credit Card Payment Failed', 'off') AS tmp
WHERE NOT EXISTS (SELECT * FROM `#prefix#MessageGroups` WHERE `#prefix#MessageGroups`.`msg_name` LIKE 'Credit Card Payment Failed') 
LIMIT 1 ;
INSERT INTO `#prefix#MessageGroups` (`group_id`, `group`, `msg_name`, `enabled`) 
SELECT * FROM ( SELECT '0', 'invoice', 'First Invoice Overdue Notice', 'off') AS tmp
WHERE NOT EXISTS (SELECT * FROM `#prefix#MessageGroups` WHERE `#prefix#MessageGroups`.`msg_name` LIKE 'First Invoice Overdue Notice') 
LIMIT 1 ;
INSERT INTO `#prefix#MessageGroups` (`group_id`, `group`, `msg_name`, `enabled`) 
SELECT * FROM ( SELECT '0', 'invoice', 'Invoice Created', 'off') AS tmp
WHERE NOT EXISTS (SELECT * FROM `#prefix#MessageGroups` WHERE `#prefix#MessageGroups`.`msg_name` LIKE 'Invoice Created') 
LIMIT 1 ;
INSERT INTO `#prefix#MessageGroups` (`group_id`, `group`, `msg_name`, `enabled`) 
SELECT * FROM ( SELECT '0', 'invoice', 'Invoice Payment Confirmation', 'off') AS tmp
WHERE NOT EXISTS (SELECT * FROM `#prefix#MessageGroups` WHERE `#prefix#MessageGroups`.`msg_name` LIKE 'Invoice Payment Confirmation') 
LIMIT 1 ;
INSERT INTO `#prefix#MessageGroups` (`group_id`, `group`, `msg_name`, `enabled`) 
SELECT * FROM ( SELECT '0', 'invoice', 'Invoice Payment Reminder', 'off') AS tmp
WHERE NOT EXISTS (SELECT * FROM `#prefix#MessageGroups` WHERE `#prefix#MessageGroups`.`msg_name` LIKE 'Invoice Payment Reminder') 
LIMIT 1 ;
INSERT INTO `#prefix#MessageGroups` (`group_id`, `group`, `msg_name`, `enabled`) 
SELECT * FROM ( SELECT '0', 'invoice', 'Invoice Refund Confirmation', 'off') AS tmp
WHERE NOT EXISTS (SELECT * FROM `#prefix#MessageGroups` WHERE `#prefix#MessageGroups`.`msg_name` LIKE 'Invoice Refund Confirmation') 
LIMIT 1 ;
INSERT INTO `#prefix#MessageGroups` (`group_id`, `group`, `msg_name`, `enabled`) 
SELECT * FROM ( SELECT '0', 'invoice', 'Second Invoice Overdue Notice', 'off') AS tmp
WHERE NOT EXISTS (SELECT * FROM `#prefix#MessageGroups` WHERE `#prefix#MessageGroups`.`msg_name` LIKE 'Second Invoice Overdue Notice') 
LIMIT 1 ;
INSERT INTO `#prefix#MessageGroups` (`group_id`, `group`, `msg_name`, `enabled`) 
SELECT * FROM ( SELECT '0', 'invoice', 'Third Invoice Overdue Notice', 'off') AS tmp
WHERE NOT EXISTS (SELECT * FROM `#prefix#MessageGroups` WHERE `#prefix#MessageGroups`.`msg_name` LIKE 'Third Invoice Overdue Notice') 
LIMIT 1 ;
INSERT INTO `#prefix#MessageGroups` (`group_id`, `group`, `msg_name`, `enabled`) 
SELECT * FROM ( SELECT '0', 'product', 'Service Unsuspension Notification', 'off') AS tmp
WHERE NOT EXISTS (SELECT * FROM `#prefix#MessageGroups` WHERE `#prefix#MessageGroups`.`msg_name` LIKE 'Service Unsuspension Notification') 
LIMIT 1 ;
INSERT INTO `#prefix#MessageGroups` (`group_id`, `group`, `msg_name`, `enabled`) 
SELECT * FROM ( SELECT '0', 'product', 'Cancellation Request Confirmation', 'off') AS tmp
WHERE NOT EXISTS (SELECT * FROM `#prefix#MessageGroups` WHERE `#prefix#MessageGroups`.`msg_name` LIKE 'Cancellation Request Confirmation') 
LIMIT 1 ;
INSERT INTO `#prefix#MessageGroups` (`group_id`, `group`, `msg_name`, `enabled`) 
SELECT * FROM ( SELECT '0', 'product', 'Dedicated/VPS Server Welcome Email', 'off') AS tmp
WHERE NOT EXISTS (SELECT * FROM `#prefix#MessageGroups` WHERE `#prefix#MessageGroups`.`msg_name` LIKE 'Dedicated/VPS Server Welcome Email') 
LIMIT 1 ;
INSERT INTO `#prefix#MessageGroups` (`group_id`, `group`, `msg_name`, `enabled`) 
SELECT * FROM ( SELECT '0', 'product', 'Hosting Account Welcome Email', 'off') AS tmp
WHERE NOT EXISTS (SELECT * FROM `#prefix#MessageGroups` WHERE `#prefix#MessageGroups`.`msg_name` LIKE 'Hosting Account Welcome Email') 
LIMIT 1 ;
INSERT INTO `#prefix#MessageGroups` (`group_id`, `group`, `msg_name`, `enabled`) 
SELECT * FROM ( SELECT '0', 'product', 'Other Product/Service Welcome Email', 'off') AS tmp
WHERE NOT EXISTS (SELECT * FROM `#prefix#MessageGroups` WHERE `#prefix#MessageGroups`.`msg_name` LIKE 'Other Product/Service Welcome Email') 
LIMIT 1 ;
INSERT INTO `#prefix#MessageGroups` (`group_id`, `group`, `msg_name`, `enabled`) 
SELECT * FROM ( SELECT '0', 'product', 'Reseller Account Welcome Email', 'off') AS tmp
WHERE NOT EXISTS (SELECT * FROM `#prefix#MessageGroups` WHERE `#prefix#MessageGroups`.`msg_name` LIKE 'Reseller Account Welcome Email') 
LIMIT 1 ;
INSERT INTO `#prefix#MessageGroups` (`group_id`, `group`, `msg_name`, `enabled`) 
SELECT * FROM ( SELECT '0', 'product', 'Service Suspension Notification', 'off') AS tmp
WHERE NOT EXISTS (SELECT * FROM `#prefix#MessageGroups` WHERE `#prefix#MessageGroups`.`msg_name` LIKE 'Service Suspension Notification') 
LIMIT 1 ;
INSERT INTO `#prefix#MessageGroups` (`group_id`, `group`, `msg_name`, `enabled`) 
SELECT * FROM ( SELECT '0', 'product', 'SHOUTcast Welcome Email', 'off') AS tmp
WHERE NOT EXISTS (SELECT * FROM `#prefix#MessageGroups` WHERE `#prefix#MessageGroups`.`msg_name` LIKE 'SHOUTcast Welcome Email') 
LIMIT 1 ;
INSERT INTO `#prefix#MessageGroups` (`group_id`, `group`, `msg_name`, `enabled`) 
SELECT * FROM ( SELECT '0', 'product', 'New Account Information', 'off') AS tmp
WHERE NOT EXISTS (SELECT * FROM `#prefix#MessageGroups` WHERE `#prefix#MessageGroups`.`msg_name` LIKE 'New Account Information') 
LIMIT 1 ;
INSERT INTO `#prefix#MessageGroups` (`group_id`, `group`, `msg_name`, `enabled`) 
SELECT * FROM ( SELECT '0', 'product', 'Server Information', 'off') AS tmp
WHERE NOT EXISTS (SELECT * FROM `#prefix#MessageGroups` WHERE `#prefix#MessageGroups`.`msg_name` LIKE 'Server Information') 
LIMIT 1 ;
INSERT INTO `#prefix#MessageGroups` (`group_id`, `group`, `msg_name`, `enabled`) 
SELECT * FROM ( SELECT '0', 'support', 'Clients Only Bounce Message', 'off') AS tmp
WHERE NOT EXISTS (SELECT * FROM `#prefix#MessageGroups` WHERE `#prefix#MessageGroups`.`msg_name` LIKE 'Clients Only Bounce Message') 
LIMIT 1 ;
INSERT INTO `#prefix#MessageGroups` (`group_id`, `group`, `msg_name`, `enabled`) 
SELECT * FROM ( SELECT '0', 'support', 'Replies Only Bounce Message', 'off') AS tmp
WHERE NOT EXISTS (SELECT * FROM `#prefix#MessageGroups` WHERE `#prefix#MessageGroups`.`msg_name` LIKE 'Replies Only Bounce Message') 
LIMIT 1 ;
INSERT INTO `#prefix#MessageGroups` (`group_id`, `group`, `msg_name`, `enabled`) 
SELECT * FROM ( SELECT '0', 'support', 'Support Ticket Auto Close Notification', 'off') AS tmp
WHERE NOT EXISTS (SELECT * FROM `#prefix#MessageGroups` WHERE `#prefix#MessageGroups`.`msg_name` LIKE 'Support Ticket Auto Close Notification') 
LIMIT 1 ;
INSERT INTO `#prefix#MessageGroups` (`group_id`, `group`, `msg_name`, `enabled`) 
SELECT * FROM ( SELECT '0', 'support', 'Closed Ticket Bounce Message', 'off') AS tmp
WHERE NOT EXISTS (SELECT * FROM `#prefix#MessageGroups` WHERE `#prefix#MessageGroups`.`msg_name` LIKE 'Closed Ticket Bounce Message') 
LIMIT 1 ;
INSERT INTO `#prefix#MessageGroups` (`group_id`, `group`, `msg_name`, `enabled`) 
SELECT * FROM ( SELECT '0', 'support', 'Support Ticket Feedback Request', 'off') AS tmp
WHERE NOT EXISTS (SELECT * FROM `#prefix#MessageGroups` WHERE `#prefix#MessageGroups`.`msg_name` LIKE 'Support Ticket Feedback Request') 
LIMIT 1 ;
INSERT INTO `#prefix#MessageGroups` (`group_id`, `group`, `msg_name`, `enabled`) 
SELECT * FROM ( SELECT '0', 'support', 'Support Ticket Opened', 'off') AS tmp
WHERE NOT EXISTS (SELECT * FROM `#prefix#MessageGroups` WHERE `#prefix#MessageGroups`.`msg_name` LIKE 'Support Ticket Opened') 
LIMIT 1 ;
INSERT INTO `#prefix#MessageGroups` (`group_id`, `group`, `msg_name`, `enabled`) 
SELECT * FROM ( SELECT '0', 'support', 'Support Ticket Opened by Admin', 'off') AS tmp
WHERE NOT EXISTS (SELECT * FROM `#prefix#MessageGroups` WHERE `#prefix#MessageGroups`.`msg_name` LIKE 'Support Ticket Opened by Admin') 
LIMIT 1 ;
INSERT INTO `#prefix#MessageGroups` (`group_id`, `group`, `msg_name`, `enabled`) 
SELECT * FROM ( SELECT '0', 'support', 'Support Ticket Reply', 'off') AS tmp
WHERE NOT EXISTS (SELECT * FROM `#prefix#MessageGroups` WHERE `#prefix#MessageGroups`.`msg_name` LIKE 'Support Ticket Reply') 
LIMIT 1 ;
INSERT INTO `#prefix#MessageGroups` (`group_id`, `group`, `msg_name`, `enabled`) 
SELECT * FROM ( SELECT '0', 'admin', 'Automatic Setup Failed', 'off') AS tmp
WHERE NOT EXISTS (SELECT * FROM `#prefix#MessageGroups` WHERE `#prefix#MessageGroups`.`msg_name` LIKE 'Automatic Setup Failed') 
LIMIT 1 ;
INSERT INTO `#prefix#MessageGroups` (`group_id`, `group`, `msg_name`, `enabled`) 
SELECT * FROM ( SELECT '0', 'admin', 'Automatic Setup Successful', 'off') AS tmp
WHERE NOT EXISTS (SELECT * FROM `#prefix#MessageGroups` WHERE `#prefix#MessageGroups`.`msg_name` LIKE 'Automatic Setup Successful') 
LIMIT 1 ;
INSERT INTO `#prefix#MessageGroups` (`group_id`, `group`, `msg_name`, `enabled`) 
SELECT * FROM ( SELECT '0', 'admin', 'Domain Renewal Failed', 'off') AS tmp
WHERE NOT EXISTS (SELECT * FROM `#prefix#MessageGroups` WHERE `#prefix#MessageGroups`.`msg_name` LIKE 'Domain Renewal Failed') 
LIMIT 1 ;
INSERT INTO `#prefix#MessageGroups` (`group_id`, `group`, `msg_name`, `enabled`) 
SELECT * FROM ( SELECT '0', 'admin', 'Domain Renewal Successful', 'off') AS tmp
WHERE NOT EXISTS (SELECT * FROM `#prefix#MessageGroups` WHERE `#prefix#MessageGroups`.`msg_name` LIKE 'Domain Renewal Successful') 
LIMIT 1 ;
INSERT INTO `#prefix#MessageGroups` (`group_id`, `group`, `msg_name`, `enabled`) 
SELECT * FROM ( SELECT '0', 'admin', 'New Cancellation Request', 'off') AS tmp
WHERE NOT EXISTS (SELECT * FROM `#prefix#MessageGroups` WHERE `#prefix#MessageGroups`.`msg_name` LIKE 'New Cancellation Request') 
LIMIT 1 ;
INSERT INTO `#prefix#MessageGroups` (`group_id`, `group`, `msg_name`, `enabled`) 
SELECT * FROM ( SELECT '0', 'admin', 'New Order Notification', 'off') AS tmp
WHERE NOT EXISTS (SELECT * FROM `#prefix#MessageGroups` WHERE `#prefix#MessageGroups`.`msg_name` LIKE 'New Order Notification') 
LIMIT 1 ;
INSERT INTO `#prefix#MessageGroups` (`group_id`, `group`, `msg_name`, `enabled`) 
SELECT * FROM ( SELECT '0', 'admin', 'Service Unsuspension Failed', 'off') AS tmp
WHERE NOT EXISTS (SELECT * FROM `#prefix#MessageGroups` WHERE `#prefix#MessageGroups`.`msg_name` LIKE 'Service Unsuspension Failed') 
LIMIT 1 ;
INSERT INTO `#prefix#MessageGroups` (`group_id`, `group`, `msg_name`, `enabled`) 
SELECT * FROM ( SELECT '0', 'admin', 'Service Unsuspension Successful', 'off') AS tmp
WHERE NOT EXISTS (SELECT * FROM `#prefix#MessageGroups` WHERE `#prefix#MessageGroups`.`msg_name` LIKE 'Service Unsuspension Successful') 
LIMIT 1 ;
INSERT INTO `#prefix#MessageGroups` (`group_id`, `group`, `msg_name`, `enabled`) 
SELECT * FROM ( SELECT '0', 'admin', 'Support Ticket Created', 'off') AS tmp
WHERE NOT EXISTS (SELECT * FROM `#prefix#MessageGroups` WHERE `#prefix#MessageGroups`.`msg_name` LIKE 'Support Ticket Created') 
LIMIT 1 ;
INSERT INTO `#prefix#MessageGroups` (`group_id`, `group`, `msg_name`, `enabled`) 
SELECT * FROM ( SELECT '0', 'admin', 'Support Ticket Department Reassigned', 'off') AS tmp
WHERE NOT EXISTS (SELECT * FROM `#prefix#MessageGroups` WHERE `#prefix#MessageGroups`.`msg_name` LIKE 'Support Ticket Department Reassigned') 
LIMIT 1 ;
INSERT INTO `#prefix#MessageGroups` (`group_id`, `group`, `msg_name`, `enabled`) 
SELECT * FROM ( SELECT '0', 'admin', 'Support Ticket Flagged', 'off') AS tmp
WHERE NOT EXISTS (SELECT * FROM `#prefix#MessageGroups` WHERE `#prefix#MessageGroups`.`msg_name` LIKE 'Support Ticket Flagged') 
LIMIT 1 ;
INSERT INTO `#prefix#MessageGroups` (`group_id`, `group`, `msg_name`, `enabled`) 
SELECT * FROM ( SELECT '0', 'admin', 'Support Ticket Response', 'off') AS tmp
WHERE NOT EXISTS (SELECT * FROM `#prefix#MessageGroups` WHERE `#prefix#MessageGroups`.`msg_name` LIKE 'Support Ticket Response') 
LIMIT 1 ;
INSERT INTO `#prefix#MessageGroups` (`group_id`, `group`, `msg_name`, `enabled`) 
SELECT * FROM ( SELECT '0', 'admin', 'Client Login Notification', 'off') AS tmp
WHERE NOT EXISTS (SELECT * FROM `#prefix#MessageGroups` WHERE `#prefix#MessageGroups`.`msg_name` LIKE 'Client Login Notification') 
LIMIT 1 ;
INSERT INTO `#prefix#MessageGroups` (`group_id`, `group`, `msg_name`, `enabled`)
SELECT * FROM ( SELECT '0', 'admin', 'Admin Login Notification', 'off') AS tmp
WHERE NOT EXISTS (SELECT * FROM `#prefix#MessageGroups` WHERE `#prefix#MessageGroups`.`msg_name` LIKE 'Admin Login Notification')
LIMIT 1 ;
INSERT INTO `#prefix#MessageGroups` (`group_id`, `group`, `msg_name`, `enabled`)
SELECT * FROM ( SELECT '0', 'admin', 'Service Suspend Notification', 'off') AS tmp
WHERE NOT EXISTS (SELECT * FROM `#prefix#MessageGroups` WHERE `#prefix#MessageGroups`.`msg_name` LIKE 'Service Suspend Notification')
LIMIT 1 ;
INSERT INTO `#prefix#MessageGroups` (`group_id`, `group`, `msg_name`, `enabled`)
SELECT * FROM ( SELECT '0', 'admin', 'Order Paid Notification', 'off') AS tmp
WHERE NOT EXISTS (SELECT * FROM `#prefix#MessageGroups` WHERE `#prefix#MessageGroups`.`msg_name` LIKE 'Order Paid Notification')
LIMIT 1 ;
INSERT INTO `#prefix#Templates` (`group_id`, `lang`, `message`)
SELECT * FROM ( SELECT (SELECT id FROM `#prefix#MessageGroups` WHERE `#prefix#MessageGroups`.`msg_name` LIKE 'Admin Login Notification'), 'default', 'Notification: {$admin_first_name} {$admin_last_name}  {$admin_username} admin logged in to the website.') AS tmp
WHERE NOT EXISTS (SELECT * FROM `#prefix#Templates` WHERE `#prefix#Templates`.`group_id` = (SELECT id FROM `#prefix#MessageGroups` WHERE `#prefix#MessageGroups`.`msg_name` LIKE 'Admin Login Notification'))
LIMIT 1 ;
INSERT INTO `#prefix#Templates` (`group_id`, `lang`, `message`)
SELECT * FROM ( SELECT (SELECT id FROM `#prefix#MessageGroups` WHERE `#prefix#MessageGroups`.`msg_name` LIKE 'Service Suspend Notification'), 'default', 'Notification: This product/service has been suspended. Product/Service: {$service_product_name} {if $service_domain}Domain: {$service_domain}{/if} Amount: {$service_first_payment_amount} Due Date: {$service_next_due_date} Suspension Reason: {$service_suspension_reason}') AS tmp
WHERE NOT EXISTS (SELECT * FROM `#prefix#Templates` WHERE `#prefix#Templates`.`group_id` = (SELECT id FROM `#prefix#MessageGroups` WHERE `#prefix#MessageGroups`.`msg_name` LIKE 'Service Suspend Notification'))
LIMIT 1 ;
INSERT INTO `#prefix#Templates` (`group_id`, `lang`, `message`)
SELECT * FROM ( SELECT (SELECT id FROM `#prefix#MessageGroups` WHERE `#prefix#MessageGroups`.`msg_name` LIKE 'Order Paid Notification'), 'default', 'The payment for order {$order_number} has been successfully processed.') AS tmp
WHERE NOT EXISTS (SELECT * FROM `#prefix#Templates` WHERE `#prefix#Templates`.`group_id` = (SELECT id FROM `#prefix#MessageGroups` WHERE `#prefix#MessageGroups`.`msg_name` LIKE 'Order Paid Notification'))
LIMIT 1 ;