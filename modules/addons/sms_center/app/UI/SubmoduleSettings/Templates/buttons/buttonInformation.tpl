<a {foreach $htmlAttributes as $aValue} {$aValue@key}="{$aValue}" {/foreach}
{if $rawObject->isRawTitle()}title="{$rawObject->getRawTitle()}"{elseif $rawObject->getTitle()}title="{$MGLANG->T('button', $rawObject->getTitle())}"{/if}
class="{$rawObject->getClasses()}"
{if $rawObject->isEnabledByColumnValue()}{literal} v-if="dataRow.{/literal}{$rawObject->getEnabledColumnName()}{literal} == {/literal} `{$rawObject->getEnabledColumnValue()}`{literal}"{/literal}{/if}
>

{if $rawObject->getIcon()}<i class="{$rawObject->getIcon()}"></i>{/if}
</a>