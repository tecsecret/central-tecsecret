<div class="alert alert--warning alert--faded">
    <div class="alert__body" style="overflow: auto;">
        <b>{$rawObject->getText()}</b> <a href="{$rawObject->getUrl()}" target="_blank">{$rawObject->getUrl()}</a>
    </div>
</div>
