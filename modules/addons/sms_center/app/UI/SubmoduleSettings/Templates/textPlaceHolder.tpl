<div class="form-group">
    <label class="form-label">
        {if $rawObject->isRawTitle()}{$rawObject->getRawTitle()}{elseif $rawObject->getTitle()}{$MGLANG->T($rawObject->getTitle())}{/if}
        {if $rawObject->getDescription()}
            <i data-title="{$MGLANG->T($rawObject->getDescription())}" data-toggle="tooltip" class="i-c-2x zmdi zmdi-help-outline form-tooltip-helper lu-tooltip drop-target drop-element-attached-bottom drop-element-attached-center drop-target-attached-top drop-target-attached-center"></i>
        {/if}
    </label>
    <input class="form-control" type="text"  name="{$rawObject->getName()}"
           value="{if $rawObject->getValue()}{$rawObject->getValue()}{else}{$rawObject->getPlaceholder()}{/if}" {if $rawObject->isDisabled()}disabled="disabled"{/if}
           {foreach $htmlAttributes as $aValue} {$aValue@key}="{$aValue}" {/foreach}>
    <div class="form-feedback form-feedback--icon" hidden="hidden">
    </div>    
</div>
