<a {foreach $htmlAttributes as $aValue} {$aValue@key}="{$aValue}" {/foreach}
    {if $rawObject->isRawTitle()}title="{$rawObject->getRawTitle()}"{elseif $rawObject->getTitle()}title="{$MGLANG->T('button', $rawObject->getTitle())}"{/if}
    class="{$rawObject->getClasses()}">
    {if $rawObject->getIcon()}
        <span class="btn__icon btn__icon">
            <i class="{$rawObject->getIcon()}"></i>
        </span>
    {/if}
    {if $rawObject->isRawTitle()}
        <span class="btn__text">{$rawObject->getRawTitle()}</span>
    {elseif $rawObject->getTitle()}
        <span class="btn__text">{$MGLANG->T('button', $rawObject->getTitle())}</span>
    {/if}
</a>