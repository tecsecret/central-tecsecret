
{if $rawObject->haveInternalAllertMessage()}
    <div class="alert {if $rawObject->getInternalAllertSize() !== ''}alert--{$rawObject->getInternalAllertSize()}{/if} alert-{$rawObject->getInternalAllertMessageType()} alert--faded modal-alert-top">
        <div class="alert__body">
            {if $rawObject->isInternalAllertMessageRaw()|unescape:'html'}{$rawObject->getInternalAllertMessage()}{else}{$MGLANG->T($rawObject->getInternalAllertMessage())|unescape:'html'}{/if}
        </div>
    </div>
{/if}

{if $rawObject->getConfirmMessage()}
        {if $rawObject->isTranslateConfirmMessage()}
            {$MGLANG->T($rawObject->getConfirmMessage())|unescape:'html'}
        {else}
            {$rawObject->getConfirmMessage()|unescape:'html'}
        {/if}
{/if}
    <form id="{$rawObject->getId()}" mgformaction="{$rawObject->getFormType()}" id="{$elementId}">
        <div class="widget m-b-4x m-t-4x widget--lg">
            {if $rawObject->getTitle()}
                <div class="widget__header">
                    <div class="widget__top top">
                        <div class="top__title">{$MGLANG->T($rawObject->getTitle())|unescape:'html'}</div>
                    </div>
                </div>
            {/if}
            <div class=" widget__body">
                <div class="widget__content">
                    {if $rawObject->getSections()}
                        {foreach from=$rawObject->getSections() item=section }
                            {$section->getHtml()}
                        {/foreach}                                
                    {else}
                        {foreach from=$rawObject->getFields() item=field }
                            {$field->getHtml()}
                        {/foreach}
                    {/if}
                </div>
            </div>
            <div class="widget__actions widget__actions--raised p-t-2x p-b-2x">
                {$rawObject->getSubmitHtml()}
                {$rawObject->insertButton('resendTokenButton')}
            </div>
        {if ($isDebug eq true AND (count($MGLANG->getMissingLangs()) != 0))}{literal}
            <div class="modal__actions">
                <div class="alert alert--info">
                    <div class="row">
                        {/literal}{foreach from=$MGLANG->getMissingLangs() key=varible item=value}{literal}
                            <div class="col-md-12"><b>{/literal}{$varible}{literal}</b> = '{/literal}{$value}{literal}';</div>
                        {/literal}{/foreach}{literal}
                    </div>
                </div>
            </div>
        {/literal}{/if}
    </div>
</form>

