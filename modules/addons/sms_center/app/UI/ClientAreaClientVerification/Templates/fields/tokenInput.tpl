    
    <div class="form-group">
        {if $rawObject->isRawTitle()}<p>{$rawObject->getRawTitle()}</p>{elseif $rawObject->getTitle()}<p>{$MGLANG->T($rawObject->getTitle())}</p>{/if}
        {if $rawObject->getDescription()}<p>{$MGLANG->T($rawObject->getDescription())}</p> {/if}
    
        <div class="well m-b-0x m-t-3x">
            <div class="input-group input-group--xlg">
                <i class="input-group__icon zmdi zmdi-lock"></i>
                <input type="text" placeholder="{$rawObject->getPlaceholder()}" name="{$rawObject->getName()}" value="{$rawObject->getValue()}" class="form-control" {if $rawObject->isDisabled()}disabled="disabled"{/if}
           {foreach $htmlAttributes as $aValue} {$aValue@key}="{$aValue}" {/foreach}>
            </div>
        </div>
    </div>