<div class="dataTables_wrapper no-footer">
    <div>
        <table  class="table table--mob-collapsible dataTable no-footer dtr-column" width="100%" role="grid">
            <thead>
            <th class=""   name="test">
                <span class="table__text">{$MGLANG->T('table', 'id')}&nbsp;&nbsp;</span>
            </th>
            <th class=""   name="test">
                <span class="table__text">{$MGLANG->T('table', 'name')}&nbsp;&nbsp;</span>
            </th>
            <th class=""   name="test">
                <span class="table__text">{$MGLANG->T('table', 'number')}&nbsp;&nbsp;</span>
            </th>
            </thead>
            <tbody>
                {foreach from=$rawObject->getElements() key=tplKey item=tplValue}
                    <tr>
                        <td class="mgTableActions">
                            {$tplKey}
                        </td>
                        <td class="mgTableActions">
                            {$tplValue.name}
                        </td>
                        <td class="mgTableActions">
                            {$tplValue.number}
                        </td>
                    </tr>
                {/foreach}
            </tbody>
        </table>
    </div>
</div>