<div class="form-group">
    {if $rawObject->isRawTitle() || $rawObject->getTitle()}
        <label class="control-label">
            {if $rawObject->isRawTitle()}{$rawObject->getRawTitle()}{elseif $rawObject->getTitle()}{$MGLANG->T($rawObject->getTitle())}{/if}
        </label>
        {else}
            <label class="control-label">   
                &nbsp;
            </label>
    {/if}
            <select 
                class="form-control" 
                name="{$rawObject->getName()}"
                {if $rawObject->isDisabled()}disabled="disabled"{/if}
                {if $rawObject->isMultiple()}data-options="removeButton:true; resotreOnBackspace:true; dragAndDrop:true; maxItems: null;" multiple="multiple"{/if}
            >
                {$rawObject->getAvalibleValues()}
                {foreach from=$rawObject->getAvalibleValues() key=opValue item=option}
                    <option value="{$opValue}" {if $opValue==$rawObject->getCustomValue()}selected{/if}>
                        {$MGLANG->T($option)}
                    </option>
                {/foreach}
            </select>

    {if $rawObject->getDescription()}
        <div class="row">
            <div class="col-md-offset-{$rawObject->getLabelWidth()} col-md-{$rawObject->getWidth()}">
                <span class="help-block">
                    {$MGLANG->T($rawObject->getDescription())}
                </span>
            </div>
        </div>
    {/if}
</div>
