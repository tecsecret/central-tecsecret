<div class="col-md-6 p-r-4x">
    {foreach from=$rawObject->getSections() item=section }
        <div >
                <div class="widget" style="margin-left:-5px;">
                    <div class="widget__header">
                        <div class="widget__top top">
                            <div class="top__title">{$MGLANG->T($section->getTitle())}</div>
                        </div>
                    </div>
                    <div class="widget__body">
                        <div class="widget__content">
                            {$section->getHtml()}
                            </div>
                        </div>
                    </div>
                </div>
    {/foreach}
</div>