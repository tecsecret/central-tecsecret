<div class="form-group">
    <label class="form-label">
        {if $rawObject->isRawTitle()}{$rawObject->getRawTitle()}{elseif $rawObject->getTitle()}{$MGLANG->T($rawObject->getTitle())}{/if}
        {if $rawObject->getDescription()}
            <i data-title="{$MGLANG->T($rawObject->getDescription())}" data-toggle="tooltip" class="i-c-2x zmdi zmdi-help-outline form-tooltip-helper lu-tooltip drop-target drop-element-attached-bottom drop-element-attached-center drop-target-attached-top drop-target-attached-center"></i>
        {/if}
    </label>
    <div class="input-group">
        <input class="selectize-control form-control" style="padding-left: 15px;" type="text" placeholder="{$rawObject->getPlaceholder()}" name="{$rawObject->getNameInput()}"
               value="{$rawObject->getValueInput()}" {if $rawObject->isDisabled()}disabled="disabled"{/if}
               {foreach $htmlAttributes as $aValue} {$aValue@key}="{$aValue}" {/foreach}>  
        <select 
            style="width: 130px;"
            class="selectize-control form-control" 
            name="{$rawObject->getNameSelect()}"
            {if $rawObject->isDisabled()}disabled="disabled"{/if}
            {if $rawObject->isMultiple()}data-options="removeButton:true; resotreOnBackspace:true; dragAndDrop:true; maxItems: null;" multiple="multiple"{/if}
            >
            {if $rawObject->getSelectedValue()|is_array}
                {foreach from=$rawObject->getAvalibleValues() key=opValue item=option}
                    <option value="{$opValue}" {if $opValue|in_array:$rawObject->getSelectedValue()}selected{/if}>
                        {$MGLANG->T({$option})}
                    </option>
                {/foreach}            
            {else}
                {foreach from=$rawObject->getAvalibleValues() key=opValue item=option}
                    <option value="{$opValue}" {if $opValue==$rawObject->getSelectedValue()}selected{/if}>
                        {$MGLANG->T({$option})}
                    </option>
                {/foreach}
            {/if}
        </select>
    </div>
    <div class="form-feedback form-feedback--icon" hidden="hidden"></div> 
</div>
