
<div class="row {if $rawObject->getEquals()}row--eq-height{/if} col-lg-12" {if $rawObject->getHidden()} style="visibility: hidden" {/if}>
    {if $rawObject->getSections()}
        {foreach from=$rawObject->getSections() item=section }
            <div class="col-lg-6">
                <div class="widget" style="margin-left:-5px;">
                    <div class="widget__header">
                        <div class="widget__top top">
                            <div class="top__title">{$MGLANG->T($section->getTitle())}</div>
                        </div>
                    </div>
                    <div class="widget__body">
                        <div class="widget__content">
                            {$section->getHtml()}
                            </div>
                        </div>
                    </div>
                </div>
                {/foreach}  
                    {else}
                        <div class="col-md-6 p-r-4x">
                            {foreach from=$rawObject->getFields() item=field }
                                {$field->getHtml()}
                            {/foreach}
                        </div>
                        {/if}
                        </div>

