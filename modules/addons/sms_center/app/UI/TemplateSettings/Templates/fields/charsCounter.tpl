<div class="form-group" >    
    <span class="nav__link-text">{$MGLANG->T('Character count')}
    </span>
    <span class="badge badge--default badge--outline" style="margin-top: -2px;" id ="{$rawObject->getId()}"><b>{$rawObject->getStartedCount()}</b></span>
</div>