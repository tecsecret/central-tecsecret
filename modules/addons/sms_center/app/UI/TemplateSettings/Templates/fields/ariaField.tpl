<div class="alert alert-{$rawObject->getClasses()}">
    <div class="list-group list-group--collapse" id="accordion{$rawObject->getName()}">
        <div class="list-group__item">
            <div class="list-group__top top" data-toggle="lu-collapse" data-target="#collapse{$rawObject->getName()}" data-parent="#accordion{$rawObject->getName()}" aria-expanded="false">
                <span class="collapse-icon"></span>
                <div class="top__title">{$MGLANG->T('ticketRelated')}</div>
            </div>
            <div class="collapse" id="collapse{$rawObject->getName()}">
                <div class="list-group__content">
                    <div class="according-inner">
                        {foreach from=$rawObject->getList() key=k item=v}
                            <div class="vars-row">
                                <span data-var="ticket_id" style="font-weight: 600; padding-right: 5px;">{$k}</span>
                                <span data-var="ticket_id">{$v}</span>
                            </div>
                        {/foreach}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>