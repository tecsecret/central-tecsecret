<div class="list-group__item ">
    <div class="list-group__top top lu-tooltip drop-target" data-toggle="lu-collapse" data-target="#collapse{$rawObject->getName()}" data-parent="#accordion{$rawObject->getName()}" aria-expanded="false">
        <span class="collapse-icon"></span>
        <div class="top__title type-7 ">{$MGLANG->T('ticketRelated')}</div>
    </div>
    <div class="collapse" id="collapse{$rawObject->getName()}">
        <div class="list-group__content">
            <div id="{$rawObject->getVueAria()}">
                <div class="according-inner">
                    {foreach from=$rawObject->getList() key=k item=v}
                        <div class="lu-row form-data-select lu-tooltip drop-target " onclick="addValue('{$rawObject->getParseValue($v)}')">
                            <div class="lu-row" style="margin-left: 20px; width:450px;">
                                <span  style="font-weight: 600;width:170px;" value="{$rawObject->getParseValue($v)}" >{$MGLANG->absoluteT($rawObject->getParseViewValue($v))}</span>
                                <span  style="width:170px;">{$rawObject->getParseValue($v)}</span>
                            </div>
                        </div>
                    {/foreach}
                </div>
            </div>
        </div>
    </div>
</div>
