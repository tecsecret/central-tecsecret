<?php

$_LANG['token']                  = ', Error Token:';
$_LANG['generalError']           = 'Something has gone wrong. Check the logs and contact the administrator.';
$_LANG['generalErrorClientArea'] = 'Something has gone wrong. Contact the administrator.';
$_LANG['permissionsStorage']     = ':storage_path: settings are not sufficient. Please change permissions to the value 777.';
$_LANG['undefinedAction']        = 'Undefined action';
$_LANG['changesHasBeenSaved']    = 'Changes have been saved successfully';
$_LANG['Monthly']                = 'Monthly';
$_LANG['Free Account']           = 'Free Account';
$_LANG['Passwords']              = 'Passwords';
$_LANG['labelAddedSuccesfully']  = 'The label has been added successfully';
$_LANG['Nothing to display']     = 'Nothing to display';
$_LANG['Search']                 = 'Search';
$_LANG['Previous']               = 'Previous';
$_LANG['Next']                   = 'Next';
$_LANG['searchPlacecholder']     = 'Search...';
$_LANG['historyDeletedSuccesfully'] = 'History has been deleted';
$_LANG['historyRowDeletedSuccesfully'] = 'The SMS has been deleted from history.';
$_LANG['options']                = "Manage SMS Center";
$_LANG['configuration']          = 'Templates';
$_LANG['smsHistory']             = 'SMS History';

$_LANG['noDataAvalible']                 = 'No Data Available';
$_LANG['validationErrors']['emptyField'] = 'The field cannot be empty';
$_LANG['bootstrapswitch']['disabled']    = 'Disabled';
$_LANG['bootstrapswitch']['enabled']     = 'Enabled';

$_LANG['Success'] = 'Success';
$_LANG['Message'] = 'Message';
$_LANG['Error'] = 'Error';


/* * ********************************************************************************************************************
 *                                                   ADMIN AREA                                                        *
 * ******************************************************************************************************************** */

$_LANG['addonAA']['datatables']['next']        = 'Next';
$_LANG['addonAA']['datatables']['previous']    = 'Previous';
$_LANG['addonAA']['datatables']['zeroRecords'] = 'Nothing to display';

// -------------------------------------------------> MENU <--------------------------------------------------------- //

$_LANG['addonAA']['pagesLabels']['uiExamples']['category'] = 'Category Menu';
$_LANG['addonAA']['pagesLabels']['label']['LoggerManager'] = 'Logs';


/** Ticket Status* */
$_LANG['supportticketsticketurgencyhigh']   = "High";
$_LANG['supportticketsticketurgencymedium'] = "Medium";
$_LANG['supportticketsticketurgencylow']    = "Low";

$_LANG['addonAA']['Page not found'] = 'Page Not Found';

$_LANG['labelDeletedSuccesfully']    = 'The label has been deleted successfully';
$_LANG['changesSaved']               = 'Changes have been saved successfully';
$_LANG['ItemNotFound']               = 'Item Not Found';
$_LANG['CategoryDeletedSuccesfully'] = 'The category has been deleted successfully';

$_LANG['categroyCannotBeAssignedAsParentToItself'] = 'The category cannot be assigned as a parent to itself';

$_LANG['formValidationError']                                 = 'Form Validation Error';
$_LANG['FormValidators']['thisFieldCannotBeEmpty']            = 'This field cannot be empty.';
$_LANG['FormValidators']['PleaseProvideANumericValue']        = 'Please provide a numeric value';
$_LANG['FormValidators']['PleaseProvideANumericValueBetween'] = 'Please provide a numeric value between :minValue: and :maxValue:';


$_LANG['addonAA']['datatableExamples']['mainContainer']['labelscont']['deleteLabelModalButton']['button']['deleteLabelModalButton'] = 'Delete Label';
$_LANG['addonAA']['datatableExamples']['mainContainer']['labelscont']['editLabelModalButton']['button']['editLabelModalButton']     = 'Edit Label';
$_LANG['addonAA']['datatableExamples']['deleteLabelModal']['modal']['deleteLabelModal']                                             = 'Delete Label';
$_LANG['addonAA']['datatableExamples']['deleteLabelModal']['baseAcceptButton']['title']                                             = 'Delete Label';
$_LANG['addonAA']['datatableExamples']['deleteLabelModal']['baseCancelButton']['title']                                             = 'Cancel';
$_LANG['addonAA']['datatableExamples']['deleteLabelModal']['deleteLabelForm']['confirmLabelRemoval']                                = 'Are you sure that you want to remove the ":title:" label?';




$_LANG['addonAA']['loggerManager']['mainContainer']['loggercont']['table']['id']          = 'ID';
$_LANG['addonAA']['loggerManager']['mainContainer']['loggercont']['table']['message']     = 'Message';
$_LANG['addonAA']['loggerManager']['mainContainer']['loggercont']['table']['ref']         = 'Reference Object';
$_LANG['addonAA']['loggerManager']['mainContainer']['loggercont']['table']['type']        = 'Type';
$_LANG['addonAA']['loggerManager']['mainContainer']['loggercont']['table']['level']       = 'Level';
$_LANG['addonAA']['loggerManager']['mainContainer']['loggercont']['table']['request']     = 'Request';
$_LANG['addonAA']['loggerManager']['mainContainer']['loggercont']['table']['response']    = 'Response';
$_LANG['addonAA']['loggerManager']['mainContainer']['loggercont']['table']['before_vars'] = 'Before Vars';
$_LANG['addonAA']['loggerManager']['mainContainer']['loggercont']['table']['vars']        = 'Vars';
$_LANG['addonAA']['loggerManager']['mainContainer']['loggercont']['table']['date']        = 'Date';

$_LANG['addonAA']['loggerManager']['mainContainer']['loggercont']['deleteLoggerModalButton']['button']['deleteLoggerModalButton'] = 'Delete Log';
$_LANG['addonAA']['loggerManager']['deleteLoggerModal']['modal']['deleteLoggerModal']                                             = 'Delete Log';
$_LANG['addonAA']['loggerManager']['deleteLoggerModal']['deleteLoggerForm']['confirmLabelRemoval']                                = 'Are you sure that you want to remove this log?';
$_LANG['addonAA']['loggerManager']['deleteLoggerModal']['baseAcceptButton']['title']                                              = 'Delete';
$_LANG['addonAA']['loggerManager']['deleteLoggerModal']['baseCancelButton']['title']                                              = 'Cancel';

$_LANG['loggerDeletedSuccesfully']  = 'The log has been deleted successfully';
$_LANG['loggersDeletedSuccesfully'] = 'The logs have been deleted successfully';

$_LANG['addonAA']['loggerManager']['mainContainer']['loggercont']['massDeleteLoggerButton']['button']['massDeleteLoggerButton'] = 'Delete Logs';

$_LANG['addonAA']['loggerManager']['massDeleteLoggerModal']['modal']['massDeleteLoggerModal']          = 'Delete Logs';
$_LANG['addonAA']['loggerManager']['massDeleteLoggerModal']['baseAcceptButton']['title']               = 'Confirm';
$_LANG['addonAA']['loggerManager']['massDeleteLoggerModal']['baseCancelButton']['title']               = 'Cancel';
$_LANG['addonAA']['loggerManager']['massDeleteLoggerModal']['deleteLoggerForm']['confirmLabelRemoval'] = 'Are you sure that you want to remove the selected logs?';

$_LANG['addonAA']['loggerManager']['mainContainer']['loggercont']['deleteAllLoggersButton']['button']['deleteAllLoggersButton'] = 'Delete All Loggers';
$_LANG['addonAA']['loggerManager']['button']['deleteAllLoggersButton']                                                          = 'Delete All Loggers';

$_LANG['addonAA']['logs']['button']['deleteAllLoggersButton']                                                                   = 'Delete All Logs';

$_LANG['addonAA']['logs']['deleteAllLoggersModal']['modal']['deleteAllLoggersModal']             = 'Delete All Logs';
$_LANG['addonAA']['logs']['deleteAllLoggersModal']['deleteAllLoggerForm']['confirmLabelRemoval'] = 'Are you sure that you want to remove all logs?';
$_LANG['addonAA']['logs']['deleteAllLoggersModal']['baseAcceptButton']['title']                  = 'Confirm';
$_LANG['addonAA']['logs']['deleteAllLoggersModal']['baseCancelButton']['title']                  = 'Cancel';

$_LANG['addonAA']['uiExamples']['mainContainer']['buttonsBox']['customActionButton']['button']['customActionButton']         = 'Custom js action example for non-ajax actions, check the console.';
$_LANG['addonAA']['uiExamples']['mainContainer']['buttonsBox']['customAjaxActionButton']['button']['customAjaxActionButton'] = 'Custom js action example for ajax actions, check the console.';
$_LANG['addonAA']['uiExamples']['mainContainer']['buttonsBox']['redirectMe']['button']['redirectMe']                         = 'Test Redirect Button';
$_LANG['addonAA']['uiExamples']['mainContainer']['buttonsBox']['moveToLogsPage']['button']['moveToLogsPage']                 = 'Redirect To Logs';

$_LANG['addonAA']['uiExamples']['mainContainer']['categoryMenuDynamicContent']['title'] = 'Invoices';
$_LANG['addonAA']['uiExamples']['tldTable']['id']                                       = 'ID';
$_LANG['addonAA']['uiExamples']['tldTable']['email']                                    = 'Email';
$_LANG['addonAA']['uiExamples']['tldTable']['type']                                     = 'Type';
$_LANG['addonAA']['uiExamples']['tldTable']['description']                              = 'Description';
$_LANG['addonAA']['uiExamples']['tldTable']['duedate']                                  = 'Due Date';
$_LANG['addonAA']['uiExamples']['tldTable']['paymentmethod']                            = 'Payment Method';

/* * ********************************************************************************************************************
 *                                                    CLIENT AREA                                                      *
 * ******************************************************************************************************************** */

$_LANG['addonCA']['pagesLabels']['label']['home'] = "Owned Passwords";

$_LANG['addonCA']['pageNotFound'] = "Page Not Found";

$_LANG['addonCA']['home']['Name']              = 'Name';
$_LANG['addonCA']['home']['Username']          = 'Username';
$_LANG['addonCA']['home']['Password']          = 'Password';
$_LANG['addonCA']['home']['Shared By']         = 'Shared By';
$_LANG['addonCA']['home']['Actions']           = 'Actions';
$_LANG['addonCA']['home']['Add New']           = 'Add New';
$_LANG['addonCA']['home']['Field is required'] = 'This field is required';
$_LANG['addonCA']['home']['Unassign']          = 'Unassign';

$_LANG['addonCA']['home']['access']['access[name]']['label']                                     = 'Name';
$_LANG['addonCA']['home']['access']['access[categoryId]']['label']                               = 'Category';
$_LANG['addonCA']['home']['access']['access[username]']['label']                                 = 'Username';
$_LANG['addonCA']['home']['access']['access[password]']['label']                                 = 'Password';
$_LANG['addonCA']['home']['access']['access[websiteUrl]']['label']                               = 'Website URL';
$_LANG['addonCA']['home']['access']['access[loginUrl]']['label']                                 = 'Login URL';
$_LANG['addonCA']['home']['access']['access[notfChangePass]']['options']['enable']               = 'Enable';
$_LANG['addonCA']['home']['access']['access[notfChangePass]']['options']['notfChangeInsertPass'] = 'Insert New Password In Email';
$_LANG['addonCA']['home']['access']['access[notfChangePass]']['label']                           = 'Send Password Change Notification';
$_LANG['addonCA']['home']['access']['access[reminderPeriod]']['label']                           = 'Password Reminder';
$_LANG['addonCA']['home']['access']['access']['access[reminderUnit]']['options']['disabled']     = 'Disabled';
$_LANG['addonCA']['home']['access']['access']['access[reminderUnit]']['options']['day']          = 'Day';
$_LANG['addonCA']['home']['access']['access']['access[reminderUnit]']['options']['week']         = 'Week';
$_LANG['addonCA']['home']['access']['access']['access[reminderUnit]']['options']['month']        = 'Month';
$_LANG['addonCA']['home']['access']['access[reminder]']['options']['reminderInsertPass']         = 'Insert Current Password In Email';
$_LANG['addonCA']['home']['access']['access[note]']['label']                                     = 'Note';

$_LANG['addonCA']['home']['Add New Own Password']         = 'Add New Owned Password';
$_LANG['addonCA']['home']['modal']['Add']                 = 'Add';
$_LANG['addonCA']['home']['modal']['close']               = 'Close';
$_LANG['addonCA']['home']['New Password has been added']  = 'The new password has been added successfully';
$_LANG['addonCA']['home']['actionButtons']['Login In']    = 'Log In';
$_LANG['addonCA']['home']['actionButtons']['details']     = 'Details';
$_LANG['addonCA']['home']['actionButtons']['edit']        = 'Edit';
$_LANG['addonCA']['home']['actionButtons']['delete']      = 'Delete';
$_LANG['addonCA']['home']['Category']                     = 'Category';
$_LANG['addonCA']['home']['Edit Own Password']            = 'Edit Owned Password';
$_LANG['addonCA']['home']['Save']                         = 'Save';
$_LANG['addonCA']['home']['Password %s has been saved']   = 'The password %s has been saved successfully';
$_LANG['addonCA']['home']['modal']['Delete Own Password'] = 'Delete Owned Password';


$_LANG['addonCA']['home']['modal']['delete']                   = 'Delete';
$_LANG['addonCA']['home']['Details']                           = 'Details';
$_LANG['addonCA']['home']['close']                             = 'Close';
$_LANG['addonCA']['home']['Own Password  %s has been deleted'] = 'The owned password %s has been deleted successfully';
$_LANG['addonCA']['home']['Website URL']                       = 'Website URL';
$_LANG['addonCA']['home']['Login URL']                         = 'Login URL';
$_LANG['addonCA']['home']['Note']                              = 'Note';


$_LANG['addonCA']['orderDomain']['SearchDomains'] = 'Search Domains';


//Admin Area
$_LANG['addonAA']['pagesLabels']['label']['smsQueue']      = 'SMS Queue';
$_LANG['addonAA']['pagesLabels']['label']['massSms']       = 'Mass SMS';
$_LANG['addonAA']['pagesLabels']['label']['templates']     = 'Templates';
$_LANG['addonAA']['pagesLabels']['label']['configuration'] = 'Configuration';
$_LANG['addonAA']['pagesLabels']['label']['logs']          = 'Logs';
$_LANG['addonAA']['pagesLabels']['label']['api']           = 'API';
$_LANG['addonAA']['pagesLabels']['label']['documentation'] = 'Documentation';
$_LANG['addonAA']['pagesLabels']['label']['smsHistory']    = 'SMS History';


/* * Template Settings Page* */


$_LANG['addonAA']['templates']['mainContainer']['templateSettingsDataTable']['addLangButton']['button']['addLangButton'] = 'Add Template Translation';
$_LANG['addonAA']['templates']['mainContainer']['templateSettingsDataTable']['table']['id']                              = 'ID';
$_LANG['addonAA']['templates']['mainContainer']['templateSettingsDataTable']['table']['lang']                            = 'Language';
$_LANG['addonAA']['templates']['mainContainer']['templateSettingsDataTable']['table']['message']                         = 'Message';

/* FORM */
$_LANG['addonAA']['templates']['addDiscountModal']['modal']['addDiscountModal']                                                          = 'Add Template Translation';
$_LANG['addonAA']['templates']['addDiscountModal']['addDiscountForm']['boxSection']['halfPageSection']['lang']['lang']                   = 'Language';
$_LANG['addonAA']['templates']['addDiscountModal']['addDiscountForm']['boxSection']['halfPageSection']['message']['message']             = 'Message';
$_LANG['addonAA']['templates']['addDiscountModal']['addDiscountForm']['boxSection']['halfPageSection']['ticketRelated']['ticketRelated'] = 'Ticket Related';
$_LANG['addonAA']['templates']['addDiscountModal']['addDiscountForm']['halfPageSection']['support']['affiliate']                         = 'Ticket Related';
$_LANG['addonAA']['templates']['addDiscountModal']['addDiscountForm']['boxSection']['halfPageSection']['clientRelated']['ticketRelated'] = 'Client Related';
$_LANG['addonAA']['templates']['addDiscountModal']['addDiscountForm']['boxSection']['halfPageSection']['other']['ticketRelated']         = 'Others';



$_LANG['addonAA']['templates']['addDiscountModal']['addDiscountForm']['ticketRelated']['ticketRelated'] = 'Ticket Related';
$_LANG['addonAA']['templates']['addDiscountModal']['addDiscountForm']['clientRelated']['ticketRelated'] = 'Client Related';
$_LANG['addonAA']['templates']['addDiscountModal']['addDiscountForm']['other']['ticketRelated']         = 'Others';

$_LANG['addonAA']['templates']['addDiscountModal']['baseAcceptButton']['title'] = 'Save';
$_LANG['addonAA']['templates']['addDiscountModal']['baseCancelButton']['title'] = 'Cancel';

$_LANG['addonAA']['templates']['addDiscountModal']['addDiscountForm']['lang']['lang']       = 'Language';
$_LANG['addonAA']['templates']['addDiscountModal']['addDiscountForm']['message']['message'] = 'Message';

/* * SMS Queue Page* */

$_LANG['addonAA']['smsQueue']['mainContainer']['smsQueuePage']['table']['id']      = 'ID';
$_LANG['addonAA']['smsQueue']['mainContainer']['smsQueuePage']['table']['sms']     = 'SMS';
$_LANG['addonAA']['smsQueue']['mainContainer']['smsQueuePage']['table']['number']  = 'Number';
$_LANG['addonAA']['smsQueue']['mainContainer']['smsQueuePage']['table']['repeats'] = 'Attempts';
$_LANG['addonAA']['smsQueue']['mainContainer']['smsQueuePage']['table']['date']    = 'Date';
$_LANG['addonAA']['smsQueue']['mainContainer']['smsQueuePage']['table']['status']  = 'Status';
$_LANG['addonAA']['smsQueue']['mainContainer']['smsQueuePage']['table']['lastRun'] = 'Last Cron Run';
$_LANG['addonAA']['smsQueue']['button']['massActionQueueStartButton']              = 'Start';
$_LANG['addonAA']['smsQueue']['button']['massActionQueueRefreshButton']            = 'Refresh';
$_LANG['addonAA']['smsQueue']['button']['massActionQueueDeleteButton']             = 'Delete';

/* * Logger Page* */

$_LANG['addonAA']['logs']['mainContainer']['loggercont']['loggerContTitle']      = 'Logs';
$_LANG['addonAA']['logs']['mainContainer']['loggercont']['table']['id']          = 'ID';
$_LANG['addonAA']['logs']['mainContainer']['loggercont']['table']['message']     = 'Message';
$_LANG['addonAA']['logs']['mainContainer']['loggercont']['table']['ref']         = 'Reference Object';
$_LANG['addonAA']['logs']['mainContainer']['loggercont']['table']['type']        = 'Type';
$_LANG['addonAA']['logs']['mainContainer']['loggercont']['table']['level']       = 'Level';
$_LANG['addonAA']['logs']['mainContainer']['loggercont']['table']['request']     = 'Request';
$_LANG['addonAA']['logs']['mainContainer']['loggercont']['table']['response']    = 'Response';
$_LANG['addonAA']['logs']['mainContainer']['loggercont']['table']['before_vars'] = 'Before Vars';
$_LANG['addonAA']['logs']['mainContainer']['loggercont']['table']['vars']        = 'Vars';
$_LANG['addonAA']['logs']['mainContainer']['loggercont']['table']['date']        = 'Date';

/* * SMS History Page* */
$_LANG['addonAA']['smsHistory']['mainContainer']['smsHistoryPage']['smsHistoryPage'] = 'SMS History';
$_LANG['addonAA']['smsHistory']['mainContainer']['smsHistoryPage']['table']['id'] = 'ID';
$_LANG['addonAA']['smsHistory']['mainContainer']['smsHistoryPage']['table']['sms'] = 'SMS';
$_LANG['addonAA']['smsHistory']['mainContainer']['smsHistoryPage']['table']['number'] = 'Number';
$_LANG['addonAA']['smsHistory']['mainContainer']['smsHistoryPage']['table']['repeats'] = 'Repeats';
$_LANG['addonAA']['smsHistory']['mainContainer']['smsHistoryPage']['table']['date'] = 'Date';
$_LANG['addonAA']['smsHistory']['mainContainer']['smsHistoryPage']['table']['client'] = 'Client';
$_LANG['addonAA']['smsHistory']['mainContainer']['smsHistoryPage']['historyDeleteButton']['button']['historyDeleteButton'] = 'Delete';
$_LANG['addonAA']['smsHistory']['mainContainer']['smsHistoryPage']['historyMassDeleteButton']['button']['historyMassDeleteButton'] = 'Delete';
$_LANG['addonAA']['smsHistory']['historyDeleteModal']['modal']['HistoryDeleteModal'] = 'Delete History';
$_LANG['addonAA']['smsHistory']['historyDeleteModal']['historyDeleteForm']['confirmHistoryDeleteForm'] = 'Are you sure you want to delete this row ?';
$_LANG['addonAA']['smsHistory']['historyMassDeleteModal']['historyMassDeleteForm']['confirmHistoryMassDeleteForm'] = 'Are you sure you want to delete SMS history ?';
$_LANG['addonAA']['smsHistory']['historyDeleteModal']['baseAcceptButton']['title'] = 'Delete';
$_LANG['addonAA']['smsHistory']['historyDeleteModal']['baseCancelButton']['title'] = 'Cancel';
$_LANG['addonAA']['smsHistory']['historyMassDeleteModal']['baseAcceptButton']['title'] = 'Delete';
$_LANG['addonAA']['smsHistory']['historyMassDeleteModal']['baseCancelButton']['title'] = 'Cancel';
$_LANG['addonAA']['smsHistory']['historyMassDeleteModal']['modal']['HistoryMassDeleteModal'] = 'Delete SMS History';
/** Template Page * */
$_LANG['addonAA']['templates']['button']['massDisableButton'] = 'Disable';
$_LANG['addonAA']['templates']['button']['massEnableButton']  = 'Enable';

$_LANG['addonAA']['configuration']['button']['massDisableButton'] = 'Disable';

$_LANG['addonAA']['templates']['mainContainer']['templatesPage']['support']['table']['id']       = 'ID';
$_LANG['addonAA']['templates']['mainContainer']['templatesPage']['support']['table']['msg_name'] = 'Template Name';
$_LANG['addonAA']['templates']['mainContainer']['templatesPage']['support']['table']['enabled']  = 'Status';

$_LANG['addonAA']['templates']['mainContainer']['templatesPage']['invoice']['table']['id']       = 'ID';
$_LANG['addonAA']['templates']['mainContainer']['templatesPage']['invoice']['table']['msg_name'] = 'Template Name';
$_LANG['addonAA']['templates']['mainContainer']['templatesPage']['invoice']['table']['enabled']  = 'Status';

$_LANG['addonAA']['templates']['mainContainer']['templatesPage']['product']['table']['id']       = 'ID';
$_LANG['addonAA']['templates']['mainContainer']['templatesPage']['product']['table']['msg_name'] = 'Template Name';
$_LANG['addonAA']['templates']['mainContainer']['templatesPage']['product']['table']['enabled']  = 'Status';

$_LANG['addonAA']['templates']['mainContainer']['templatesPage']['general']['table']['id']       = 'ID';
$_LANG['addonAA']['templates']['mainContainer']['templatesPage']['general']['table']['msg_name'] = 'Template Name';
$_LANG['addonAA']['templates']['mainContainer']['templatesPage']['general']['table']['enabled']  = 'Status';

$_LANG['addonAA']['templates']['mainContainer']['templatesPage']['notification']['table']['id']       = 'ID';
$_LANG['addonAA']['templates']['mainContainer']['templatesPage']['notification']['table']['msg_name'] = 'Template Name';
$_LANG['addonAA']['templates']['mainContainer']['templatesPage']['notification']['table']['enabled']  = 'Status';

$_LANG['addonAA']['templates']['mainContainer']['templatesPage']['domain']['table']['id']       = 'ID';
$_LANG['addonAA']['templates']['mainContainer']['templatesPage']['domain']['table']['msg_name'] = 'Template Name';
$_LANG['addonAA']['templates']['mainContainer']['templatesPage']['domain']['table']['enabled']  = 'Status';

$_LANG['addonAA']['templates']['mainContainer']['templatesPage']['affiliate']['table']['id']       = 'ID';
$_LANG['addonAA']['templates']['mainContainer']['templatesPage']['affiliate']['table']['msg_name'] = 'Template Name';
$_LANG['addonAA']['templates']['mainContainer']['templatesPage']['affiliate']['table']['enabled']  = 'Status';

$_LANG['addonAA']['templates']['mainContainer']['templatesPage']['admin']['table']['id']       = 'ID';
$_LANG['addonAA']['templates']['mainContainer']['templatesPage']['admin']['table']['msg_name'] = 'Template Name';
$_LANG['addonAA']['templates']['mainContainer']['templatesPage']['admin']['table']['enabled']  = 'Status';

$_LANG['addonAA']['templates']['mainContainer']['templatesPage']['user']['table']['id'] = 'ID';
$_LANG['addonAA']['templates']['mainContainer']['templatesPage']['user']['table']['msg_name'] = 'Template Name';
$_LANG['addonAA']['templates']['mainContainer']['templatesPage']['user']['table']['enabled'] = 'Status';

$_LANG['addonAA']['templates']['mainContainer']['templatesPage']['templatesPage'] = 'Templates';


/** Configuration Page * */
$_LANG['addonAA']['pagesLabels']['configuration']['general']        = 'General';
$_LANG['addonAA']['pagesLabels']['configuration']['submodules']     = 'SMS Gateways';
$_LANG['addonAA']['pagesLabels']['configuration']['administrators'] = 'Administrator Notifications';

/* Submodules */
$_LANG['addonAA']['configuration']['mainContainer']['submodulesPage']['table']['name']                                                            = 'SMS Module';
$_LANG['addonAA']['configuration']['mainContainer']['submodulesPage']['table']['description']                                                     = 'Description';
$_LANG['addonAA']['configuration']['mainContainer']['submodulesPage']['table']['status']                                                          = 'Status';
$_LANG['addonAA']['configuration']['mainContainer']['submodulesPage']['simpleSettingsSubmoduleButton']['button']['simpleSettingsSubmoduleButton'] = 'Simple Configuration';

/* Form */

$_LANG['addonAA']['configuration']['simpleSettingsSubmoduleModal']['baseAcceptButton']['title']                                 = 'Save';
$_LANG['addonAA']['configuration']['simpleSettingsSubmoduleModal']['baseCancelButton']['title']                                 = 'Cancel';
$_LANG['addonAA']['configuration']['mainContainer']['submodulesPage']['testConnectionButton']['button']['testConnectionButton'] = 'Check Connection';
$_LANG['addonAA']['configuration']['simpleSettingsSubmoduleModal']['modal']['simpleSettingsSubmoduleModal']                     = 'Default Configuration';

//Abstract
$_LANG['addonAA']['configuration']['simpleSettingsSubmoduleModal']['simpleSettingsSubmoduleForm']['testConnectionInfo']['testConnectionInfo'] = 'Connection Info';
$_LANG['addonAA']['configuration']['simpleSettingsSubmoduleModal']['simpleSettingsSubmoduleForm']['testItem']['testItem']                     = 'Test Item';
$_LANG['addonAA']['configuration']['simpleSettingsSubmoduleModal']['simpleSettingsSubmoduleForm']['testItem2']['testItem2']                   = 'Test Item 2';
$_LANG['addonAA']['configuration']['simpleSettingsSubmoduleModal']['simpleSettingsSubmoduleForm']['testSelect']['testSelect']                 = 'Test Select';

$_LANG['addonAA']['configuration']['addAdvancedSettingsModal']['advancedSettingsSubmoduleForm']['testConnectionInfo']['testConnectionInfo'] = 'Connection Info';
$_LANG['addonAA']['configuration']['addAdvancedSettingsModal']['advancedSettingsSubmoduleForm']['testItem']['testItem']                     = 'Test Item';
$_LANG['addonAA']['configuration']['addAdvancedSettingsModal']['advancedSettingsSubmoduleForm']['testItem2']['testItem2']                   = 'Test Item 2';
$_LANG['addonAA']['configuration']['addAdvancedSettingsModal']['advancedSettingsSubmoduleForm']['testSelect']['testSelect']                 = 'Test Select';

//Test Module
$_LANG['addonAA']['configuration']['simpleSettingsSubmoduleModal']['simpleSettingsSubmoduleForm']['connectionTest']['connectionTest'] = 'Test Connection';
$_LANG['addonAA']['configuration']['simpleSettingsSubmoduleModal']['simpleSettingsSubmoduleForm']['messageTest']['messageTest']       = 'Send Test Message';
$_LANG['addonAA']['configuration']['simpleSettingsSubmoduleModal']['simpleSettingsSubmoduleForm']['countries']['countries']           = 'Countries';
$_LANG['addonAA']['configuration']['simpleSettingsSubmoduleModal']['simpleSettingsSubmoduleForm']['default']['default']               = 'Default';
$_LANG['addonAA']['configuration']['simpleSettingsSubmoduleModal']['advancedSettingsSubmoduleForm']['username']['username']           = 'Username';

$_LANG['addonAA']['configuration']['addAdvancedSettingsModal']['advancedSettingsSubmoduleForm']['connectionTest']['connectionTest']       = 'Test Connection';
$_LANG['addonAA']['configuration']['addAdvancedSettingsModal']['advancedSettingsSubmoduleForm']['messageTest']['messageTest']             = 'Send Test Message';
$_LANG['addonAA']['configuration']['addAdvancedSettingsModal']['advancedSettingsSubmoduleForm']['countries']['countries']                 = 'Countries';
$_LANG['addonAA']['configuration']['addAdvancedSettingsModal']['advancedSettingsSubmoduleForm']['default']['default']                     = 'Default';
//BearSms
$_LANG['addonAA']['configuration']['simpleSettingsSubmoduleModal']['simpleSettingsSubmoduleForm']['username']['username']                 = 'Username';
$_LANG['addonAA']['configuration']['simpleSettingsSubmoduleModal']['simpleSettingsSubmoduleForm']['webservicesToken']['webservicesToken'] = 'Webservices Token';
$_LANG['addonAA']['configuration']['simpleSettingsSubmoduleModal']['simpleSettingsSubmoduleForm']['senderId']['senderId']                 = 'Sender ID';
$_LANG['addonAA']['configuration']['simpleSettingsSubmoduleModal']['simpleSettingsSubmoduleForm']['unicode']['unicode']                   = 'Unicode';
$_LANG['addonAA']['configuration']['simpleSettingsSubmoduleModal']['simpleSettingsSubmoduleForm']['defaultGateway']['defaultGateway']     = 'Default Gateway';

$_LANG['addonAA']['configuration']['addAdvancedSettingsModal']['advancedSettingsSubmoduleForm']['username']['username']                 = 'Username';
$_LANG['addonAA']['configuration']['addAdvancedSettingsModal']['advancedSettingsSubmoduleForm']['webservicesToken']['webservicesToken'] = 'Webservices Token';
$_LANG['addonAA']['configuration']['addAdvancedSettingsModal']['advancedSettingsSubmoduleForm']['senderId']['senderId']                 = 'Sender ID';
$_LANG['addonAA']['configuration']['addAdvancedSettingsModal']['advancedSettingsSubmoduleForm']['unicode']['unicode']                   = 'Unicode';
$_LANG['addonAA']['configuration']['addAdvancedSettingsModal']['advancedSettingsSubmoduleForm']['defaultGateway']['defaultGateway']     = 'Default Gateway';

//BoxisSms
$_LANG['addonAA']['configuration']['simpleSettingsSubmoduleModal']['simpleSettingsSubmoduleForm']['usernameOrEmail']['usernameOrEmail'] = 'API Username / email';
$_LANG['addonAA']['configuration']['simpleSettingsSubmoduleModal']['simpleSettingsSubmoduleForm']['apiKey']['apiKey']                   = 'API Key';
$_LANG['addonAA']['configuration']['simpleSettingsSubmoduleModal']['simpleSettingsSubmoduleForm']['senderName']['senderName']           = 'Sender Name';
$_LANG['addonAA']['configuration']['simpleSettingsSubmoduleModal']['simpleSettingsSubmoduleForm']['email']['email']                     = 'Email';
$_LANG['addonAA']['configuration']['simpleSettingsSubmoduleModal']['simpleSettingsSubmoduleForm']['device_id']['device_id']             = 'Device ID';
$_LANG['addonAA']['configuration']['addAdvancedSettingsModal']['advancedSettingsSubmoduleForm']['device_id']['device_id']               = 'Device ID';
$_LANG['addonAA']['configuration']['addAdvancedSettingsModal']['advancedSettingsSubmoduleForm']['email']['email']                       = 'Email';
$_LANG['addonAA']['configuration']['addAdvancedSettingsModal']['advancedSettingsSubmoduleForm']['usernameOrEmail']['usernameOrEmail']   = 'API Username / email';
$_LANG['addonAA']['configuration']['addAdvancedSettingsModal']['advancedSettingsSubmoduleForm']['apiKey']['apiKey']                     = 'API Key';
$_LANG['addonAA']['configuration']['addAdvancedSettingsModal']['advancedSettingsSubmoduleForm']['senderName']['senderName']             = 'Sender Name';
$_LANG['addonAA']['configuration']['addAdvancedSettingsModal']['advancedSettingsSubmoduleForm']['ssl']['ssl']                           = 'SSL';

//Bandwidth
$_LANG['addonAA']['configuration']['simpleSettingsSubmoduleModal']['simpleSettingsSubmoduleForm']['userId']['userId']                   = 'User ID';
$_LANG['addonAA']['configuration']['simpleSettingsSubmoduleModal']['simpleSettingsSubmoduleForm']['token']['token']                     = 'API Token';
$_LANG['addonAA']['configuration']['simpleSettingsSubmoduleModal']['simpleSettingsSubmoduleForm']['secret']['secret']                   = 'API Secret';
$_LANG['addonAA']['configuration']['simpleSettingsSubmoduleModal']['simpleSettingsSubmoduleForm']['callbackUrl']['callbackUrl']         = 'Callback URL';
$_LANG['addonAA']['configuration']['simpleSettingsSubmoduleModal']['simpleSettingsSubmoduleForm']['callbackMethod']['callbackMethod']   = 'Callback Method';
$_LANG['addonAA']['configuration']['addAdvancedSettingsModal']['advancedSettingsSubmoduleForm']['userId']['userId']                   = 'User ID';
$_LANG['addonAA']['configuration']['addAdvancedSettingsModal']['advancedSettingsSubmoduleForm']['token']['token']                     = 'API Token';
$_LANG['addonAA']['configuration']['addAdvancedSettingsModal']['advancedSettingsSubmoduleForm']['secret']['secret']                   = 'API Secret';
$_LANG['addonAA']['configuration']['addAdvancedSettingsModal']['advancedSettingsSubmoduleForm']['callbackUrl']['callbackUrl']         = 'Callback URL';
$_LANG['addonAA']['configuration']['addAdvancedSettingsModal']['advancedSettingsSubmoduleForm']['callbackMethod']['callbackMethod']   = 'Callback Method';
$_LANG['addonAA']['configuration']['editAdvancedSettingsModal']['editAdvancedSettingsSubmoduleForm']['userId']['userId']                   = 'User ID';
$_LANG['addonAA']['configuration']['editAdvancedSettingsModal']['editAdvancedSettingsSubmoduleForm']['token']['token']                     = 'API Token';
$_LANG['addonAA']['configuration']['editAdvancedSettingsModal']['editAdvancedSettingsSubmoduleForm']['secret']['secret']                   = 'API Secret';
$_LANG['addonAA']['configuration']['editAdvancedSettingsModal']['editAdvancedSettingsSubmoduleForm']['callbackUrl']['callbackUrl']         = 'Callback URL';
$_LANG['addonAA']['configuration']['editAdvancedSettingsModal']['editAdvancedSettingsSubmoduleForm']['callbackMethod']['callbackMethod']   = 'Callback Method';


//BulkSms
$_LANG['addonAA']['configuration']['simpleSettingsSubmoduleModal']['simpleSettingsSubmoduleForm']['password']['password'] = 'Password';
$_LANG['addonAA']['configuration']['simpleSettingsSubmoduleModal']['simpleSettingsSubmoduleForm']['sender']['sender']     = 'Sender';

$_LANG['addonAA']['configuration']['addAdvancedSettingsModal']['advancedSettingsSubmoduleForm']['password']['password'] = 'Password';
$_LANG['addonAA']['configuration']['addAdvancedSettingsModal']['advancedSettingsSubmoduleForm']['sender']['sender']     = 'Sender';

//Clickatell (Old Api)
$_LANG['addonAA']['configuration']['simpleSettingsSubmoduleModal']['simpleSettingsSubmoduleForm']['mobileOriginated']['mobileOriginated'] = 'Mobile Originated';

$_LANG['addonAA']['configuration']['addAdvancedSettingsModal']['advancedSettingsSubmoduleForm']['mobileOriginated']['mobileOriginated'] = 'Mobile Originated';

//Clickatell (New Api)
$_LANG['addonAA']['configuration']['simpleSettingsSubmoduleModal']['simpleSettingsSubmoduleForm']['fromNumber']['fromNumber'] = 'From Number';

$_LANG['addonAA']['configuration']['addAdvancedSettingsModal']['advancedSettingsSubmoduleForm']['fromNumber']['fromNumber'] = 'From Number';

//FastSms
$_LANG['addonAA']['configuration']['simpleSettingsSubmoduleModal']['simpleSettingsSubmoduleForm']['apiToken']['apiToken'] = 'API Token';
$_LANG['addonAA']['configuration']['simpleSettingsSubmoduleModal']['simpleSettingsSubmoduleForm']['smsFrom']['smsFrom']   = 'SMS From';

$_LANG['addonAA']['configuration']['addAdvancedSettingsModal']['advancedSettingsSubmoduleForm']['apiToken']['apiToken'] = 'API Token';
$_LANG['addonAA']['configuration']['addAdvancedSettingsModal']['advancedSettingsSubmoduleForm']['smsFrom']['smsFrom']   = 'SMS From';

//MessageBird

$_LANG['addonAA']['configuration']['simpleSettingsSubmoduleModal']['simpleSettingsSubmoduleForm']['accessKey']['accessKey'] = 'Access Key';
$_LANG['addonAA']['configuration']['simpleSettingsSubmoduleModal']['simpleSettingsSubmoduleForm']['encoding']['encoding']   = 'Encoding';

$_LANG['addonAA']['configuration']['addAdvancedSettingsModal']['advancedSettingsSubmoduleForm']['accessKey']['accessKey'] = 'Access Key';
$_LANG['addonAA']['configuration']['addAdvancedSettingsModal']['advancedSettingsSubmoduleForm']['encoding']['encoding']   = 'Encoding';

//Nexmo
$_LANG['addonAA']['configuration']['simpleSettingsSubmoduleModal']['simpleSettingsSubmoduleForm']['apiSecret']['apiSecret'] = 'API Secret';
$_LANG['addonAA']['configuration']['simpleSettingsSubmoduleModal']['simpleSettingsSubmoduleForm']['ssl']['ssl']             = 'SSL';
$_LANG['addonAA']['configuration']['simpleSettingsSubmoduleModal']['simpleSettingsSubmoduleForm']['from']['from']           = 'From';

$_LANG['addonAA']['configuration']['addAdvancedSettingsModal']['advancedSettingsSubmoduleForm']['apiSecret']['apiSecret'] = 'API Secret';
$_LANG['addonAA']['configuration']['addAdvancedSettingsModal']['advancedSettingsSubmoduleForm']['from']['from']           = 'From';

//SMS Eagle
$_LANG['addonAA']['configuration']['simpleSettingsSubmoduleModal']['simpleSettingsSubmoduleForm']['url']['url']             = 'URL';
$_LANG['addonAA']['configuration']['simpleSettingsSubmoduleModal']['simpleSettingsSubmoduleForm']['ipAddress']['ipAddress'] = 'IP Address';
$_LANG['addonAA']['configuration']['simpleSettingsSubmoduleModal']['simpleSettingsSubmoduleForm']['login']['login']         = 'Login';

$_LANG['addonAA']['configuration']['addAdvancedSettingsModal']['advancedSettingsSubmoduleForm']['url']['url']             = 'URL';
$_LANG['addonAA']['configuration']['addAdvancedSettingsModal']['advancedSettingsSubmoduleForm']['ipAddress']['ipAddress'] = 'IP Address';
$_LANG['addonAA']['configuration']['addAdvancedSettingsModal']['advancedSettingsSubmoduleForm']['login']['login']         = 'Login';

//SMSGlobal
$_LANG['addonAA']['configuration']['simpleSettingsSubmoduleModal']['simpleSettingsSubmoduleForm']['apiUsername']['apiUsername'] = 'API Username';
$_LANG['addonAA']['configuration']['simpleSettingsSubmoduleModal']['simpleSettingsSubmoduleForm']['apiPassword']['apiPassword'] = 'API Password';

$_LANG['addonAA']['configuration']['simpleSettingsSubmoduleModal']['simpleSettingsSubmoduleForm']['apiUsername']['apiUsername'] = 'API Username';
$_LANG['addonAA']['configuration']['simpleSettingsSubmoduleModal']['simpleSettingsSubmoduleForm']['apiPassword']['apiPassword'] = 'API Password';

//Twilio
$_LANG['addonAA']['configuration']['addAdvancedSettingsModal']['advancedSettingsSubmoduleForm']['accountSid']['accountSid'] = 'Account SID';
$_LANG['addonAA']['configuration']['addAdvancedSettingsModal']['advancedSettingsSubmoduleForm']['authToken']['authToken']   = 'AuthToken';

$_LANG['addonAA']['configuration']['simpleSettingsSubmoduleModal']['simpleSettingsSubmoduleForm']['apiId']['apiId'] = 'API ID';

$_LANG['addonAA']['configuration']['addAdvancedSettingsModal']['advancedSettingsSubmoduleForm']['apiId']['apiId'] = 'API ID';


/** General * */
$_LANG['addonAA']['configuration']['mainContainer']['generalPage']['boxDoubleSection']['clientArea']                                                              = 'Client Area';
$_LANG['addonAA']['configuration']['mainContainer']['generalPage']['leftSide']['noWidgetSection']['enableSmsCenter']['enableSmsCenter']                           = 'Enable SMS Center';
$_LANG['addonAA']['configuration']['mainContainer']['generalPage']['leftSide']['noWidgetSection']['disableLogs']['disableLogs']                                   = 'Disable Logs';
$_LANG['addonAA']['configuration']['mainContainer']['generalPage']['rightSide']['noWidgetSection']['enableClientSmsCenter']['enableClientSmsCenter']              = 'Enable Client SMS Activation';
$_LANG['addonAA']['configuration']['mainContainer']['generalPage']['rightSide']['noWidgetSection']['sendTokenMinutes']['sendTokenMinutes']                        = 'Token Resending Attempts Interval';
$_LANG['addonAA']['configuration']['mainContainer']['generalPage']['rightSide']['noWidgetSection']['resendTokenAttempts']['resendTokenAttempts']                  = 'Token Resending Attempts Number';
$_LANG['addonAA']['configuration']['mainContainer']['generalPage']['rightSide']['noWidgetSection']['notBlockWHMCSPages']['notBlockWHMCSPages']                    = 'Do Not Block WHMCS Pages';
$_LANG['addonAA']['configuration']['mainContainer']['generalPage']['rightSide']['clientSmsAction']                                                                = 'Client SMS Activation';
$_LANG['addonAA']['configuration']['mainContainer']['generalPage']['leftSide']['orderSmsActionWidget']                                                            = 'Order SMS Activation';
$_LANG['addonAA']['configuration']['mainContainer']['generalPage']['rightSide']['smsQueueingWidget']                                                              = 'SMS Queuing';
$_LANG['addonAA']['configuration']['mainContainer']['generalPage']['leftSide']['noWidgetSection']['enableOrderSmsAvtion']['enableOrderSmsAvtion']                 = 'Enable Order SMS Activation';
$_LANG['addonAA']['configuration']['mainContainer']['generalPage']['leftSide']['noWidgetSection']['orderSendTokenMinutes']['orderSendTokenMinutes']               = 'Token Resending Attempts Interval';
$_LANG['addonAA']['configuration']['mainContainer']['generalPage']['leftSide']['noWidgetSection']['smsActionForFreeOrders']['smsActionForFreeOrders']             = 'Free Orders SMS Activation';
$_LANG['addonAA']['configuration']['mainContainer']['generalPage']['leftSide']['noWidgetSection']['resendOrderTokenAttempts']['resendOrderTokenAttempts']         = 'Token Resending Attempts Number';
$_LANG['addonAA']['configuration']['mainContainer']['generalPage']['rightSide']['noWidgetSection']['enableSmsQueuing']['enableSmsQueuing']                        = 'Enable SMS Queuing';
$_LANG['addonAA']['configuration']['mainContainer']['generalPage']['rightSide']['noWidgetSection']['messageSendPerCronRun']['messageSendPerCronRun']              = 'Messages Sent Per Cron Run';
$_LANG['addonAA']['configuration']['mainContainer']['generalPage']['rightSide']['noWidgetSection']['smsQueTrySendEachSms']['smsQueTrySendEachSms']                = 'SMS Sending Attempts Interval';
$_LANG['addonAA']['configuration']['mainContainer']['generalPage']['rightSide']['noWidgetSection']['selectWrapper']['sendMessagesBetween']['sendMessagesBetween'] = 'SMS Sending Time Frame';
$_LANG['addonAA']['configuration']['mainContainer']['generalPage']['boxDoubleSection']['noWidgetSection']['sendMessagesAnd']['sendMessagesAnd']                   = '';//disabled
$_LANG['addonAA']['configuration']['mainContainer']['generalPage']['baseSubmitButton']['button']['submit']                                                        = 'Save Changes';
$_LANG['addonAA']['configuration']['mainContainer']['generalPage']['leftSide']['tokenConfiguration']                                                              = 'Token Configuration';
$_LANG['addonAA']['configuration']['mainContainer']['generalPage']['rightSide']['customFields']                                                                   = 'Custom Fields';
$_LANG['addonAA']['configuration']['mainContainer']['generalPage']['leftSide']['noWidgetSection']['enableSmsCustomToken']['enableSmsCustomToken']                 = 'Enable Custom SMS Token';
$_LANG['addonAA']['configuration']['mainContainer']['generalPage']['leftSide']['noWidgetSection']['lengthOfCode']['lengthOfCode']                                 = 'Token Characters Number';
$_LANG['addonAA']['configuration']['mainContainer']['generalPage']['leftSide']['noWidgetSection']['charsAllowedInCode']['charsAllowedInCode']                     = 'Allowed Token Characters';
$_LANG['addonAA']['configuration']['mainContainer']['generalPage']['rightSide']['noWidgetSection']['clientNumbetCustomField']['clientNumbetCustomField']          = 'Client Number Custom Field';
$_LANG['addonAA']['configuration']['mainContainer']['generalPage']['rightSide']['noWidgetSection']['expireValidateAfter']['expireValidateAfter']                  = 'Expire Validate After (Days)';
$_LANG['addonAA']['configuration']['mainContainer']['generalPage']['rightSide']['noWidgetSection']['expireValidateAfter']['description']                          = "Type '0' to disable this option.";
$_LANG['addonAA']['configuration']['mainContainer']['generalPage']['leftSide']['noWidgetSection']['disableSmsHistory']['disableSmsHistory']                       = 'Disable SMS History';
$_LANG['addonAA']['configuration']['mainContainer']['generalPage']['leftSide']['noWidgetSection']['disableSmsHistory']['description']                             = 'If enabled, SMS History will be unavailable.';
$_LANG['addonAA']['configuration']['mainContainer']['generalPage']['leftSide']['noWidgetSection']['disableSmsHistoryClient']['disableSmsHistoryClient']           = 'Disable SMS History In Client Area';
$_LANG['addonAA']['configuration']['mainContainer']['generalPage']['leftSide']['noWidgetSection']['disableSmsHistoryClient']['description']                       = 'If enabled, SMS History in the client area will not be visible.';

$_LANG['addonAA']['configuration']['mainContainer']['generalPage']['rightSide']['noWidgetSection']['sendTokenMinutes']['minutes'] = 'Minutes';
$_LANG['addonAA']['configuration']['mainContainer']['generalPage']['rightSide']['noWidgetSection']['sendTokenMinutes']['hours']   = 'Hours';
$_LANG['addonAA']['configuration']['mainContainer']['generalPage']['rightSide']['noWidgetSection']['sendTokenMinutes']['days']    = 'Days';

$_LANG['addonAA']['configuration']['mainContainer']['generalPage']['rightSide']['noWidgetSection']['smsQueTrySendEachSms']['minutes'] = 'Minutes';
$_LANG['addonAA']['configuration']['mainContainer']['generalPage']['rightSide']['noWidgetSection']['smsQueTrySendEachSms']['hours']   = 'Hours';
$_LANG['addonAA']['configuration']['mainContainer']['generalPage']['rightSide']['noWidgetSection']['smsQueTrySendEachSms']['days']    = 'Days';

$_LANG['addonAA']['configuration']['mainContainer']['generalPage']['leftSide']['noWidgetSection']['orderSendTokenMinutes']['minutes']         = 'Minutes';
$_LANG['addonAA']['configuration']['mainContainer']['generalPage']['leftSide']['noWidgetSection']['orderSendTokenMinutes']['hours']           = 'Hours';
$_LANG['addonAA']['configuration']['mainContainer']['generalPage']['leftSide']['noWidgetSection']['orderSendTokenMinutes']['days']            = 'Days';

$_LANG['addonAA']['configuration']['mainContainer']['generalPage']['rightSide']['noWidgetSection']['selectWrapper']['sendMessagesBetween']['0']  = '12:00 AM (midnight)';
$_LANG['addonAA']['configuration']['mainContainer']['generalPage']['rightSide']['noWidgetSection']['selectWrapper']['sendMessagesBetween']['1'] = '1:00 AM';
$_LANG['addonAA']['configuration']['mainContainer']['generalPage']['rightSide']['noWidgetSection']['selectWrapper']['sendMessagesBetween']['2'] = '2:00 AM';
$_LANG['addonAA']['configuration']['mainContainer']['generalPage']['rightSide']['noWidgetSection']['selectWrapper']['sendMessagesBetween']['3'] = '3:00 AM';
$_LANG['addonAA']['configuration']['mainContainer']['generalPage']['rightSide']['noWidgetSection']['selectWrapper']['sendMessagesBetween']['4'] = '4:00 AM';
$_LANG['addonAA']['configuration']['mainContainer']['generalPage']['rightSide']['noWidgetSection']['selectWrapper']['sendMessagesBetween']['5'] = '5:00 AM';
$_LANG['addonAA']['configuration']['mainContainer']['generalPage']['rightSide']['noWidgetSection']['selectWrapper']['sendMessagesBetween']['6'] = '6:00 AM';
$_LANG['addonAA']['configuration']['mainContainer']['generalPage']['rightSide']['noWidgetSection']['selectWrapper']['sendMessagesBetween']['7'] = '7:00 AM';
$_LANG['addonAA']['configuration']['mainContainer']['generalPage']['rightSide']['noWidgetSection']['selectWrapper']['sendMessagesBetween']['8'] = '8:00 AM';
$_LANG['addonAA']['configuration']['mainContainer']['generalPage']['rightSide']['noWidgetSection']['selectWrapper']['sendMessagesBetween']['9'] = '9:00 AM';
$_LANG['addonAA']['configuration']['mainContainer']['generalPage']['rightSide']['noWidgetSection']['selectWrapper']['sendMessagesBetween']['10'] = '10:00 AM';
$_LANG['addonAA']['configuration']['mainContainer']['generalPage']['rightSide']['noWidgetSection']['selectWrapper']['sendMessagesBetween']['11'] = '11:00 AM';
$_LANG['addonAA']['configuration']['mainContainer']['generalPage']['rightSide']['noWidgetSection']['selectWrapper']['sendMessagesBetween']['12'] = '12:00 AM (noon)';
$_LANG['addonAA']['configuration']['mainContainer']['generalPage']['rightSide']['noWidgetSection']['selectWrapper']['sendMessagesBetween']['13'] = '1:00 PM';
$_LANG['addonAA']['configuration']['mainContainer']['generalPage']['rightSide']['noWidgetSection']['selectWrapper']['sendMessagesBetween']['14'] = '2:00 PM';
$_LANG['addonAA']['configuration']['mainContainer']['generalPage']['rightSide']['noWidgetSection']['selectWrapper']['sendMessagesBetween']['15'] = '3:00 PM';
$_LANG['addonAA']['configuration']['mainContainer']['generalPage']['rightSide']['noWidgetSection']['selectWrapper']['sendMessagesBetween']['16'] = '4:00 PM';
$_LANG['addonAA']['configuration']['mainContainer']['generalPage']['rightSide']['noWidgetSection']['selectWrapper']['sendMessagesBetween']['17'] = '5:00 PM';
$_LANG['addonAA']['configuration']['mainContainer']['generalPage']['rightSide']['noWidgetSection']['selectWrapper']['sendMessagesBetween']['18'] = '6:00 PM';
$_LANG['addonAA']['configuration']['mainContainer']['generalPage']['rightSide']['noWidgetSection']['selectWrapper']['sendMessagesBetween']['19'] = '7:00 PM';
$_LANG['addonAA']['configuration']['mainContainer']['generalPage']['rightSide']['noWidgetSection']['selectWrapper']['sendMessagesBetween']['20'] = '8:00 PM';
$_LANG['addonAA']['configuration']['mainContainer']['generalPage']['rightSide']['noWidgetSection']['selectWrapper']['sendMessagesBetween']['21'] = '9:00 PM';
$_LANG['addonAA']['configuration']['mainContainer']['generalPage']['rightSide']['noWidgetSection']['selectWrapper']['sendMessagesBetween']['22'] = '10:00 PM';
$_LANG['addonAA']['configuration']['mainContainer']['generalPage']['rightSide']['noWidgetSection']['selectWrapper']['sendMessagesBetween']['23'] = '11:00 PM';

$_LANG['addonAA']['configuration']['mainContainer']['generalPage']['rightSide']['noWidgetSection']['selectWrapper']['sendMessagesAnd']['0'] = '12:00 AM (midnight)';
$_LANG['addonAA']['configuration']['mainContainer']['generalPage']['rightSide']['noWidgetSection']['selectWrapper']['sendMessagesAnd']['1'] = '1:00 AM';
$_LANG['addonAA']['configuration']['mainContainer']['generalPage']['rightSide']['noWidgetSection']['selectWrapper']['sendMessagesAnd']['2'] = '2:00 AM';
$_LANG['addonAA']['configuration']['mainContainer']['generalPage']['rightSide']['noWidgetSection']['selectWrapper']['sendMessagesAnd']['3'] = '3:00 AM';
$_LANG['addonAA']['configuration']['mainContainer']['generalPage']['rightSide']['noWidgetSection']['selectWrapper']['sendMessagesAnd']['4'] = '4:00 AM';
$_LANG['addonAA']['configuration']['mainContainer']['generalPage']['rightSide']['noWidgetSection']['selectWrapper']['sendMessagesAnd']['5'] = '5:00 AM';
$_LANG['addonAA']['configuration']['mainContainer']['generalPage']['rightSide']['noWidgetSection']['selectWrapper']['sendMessagesAnd']['6'] = '6:00 AM';
$_LANG['addonAA']['configuration']['mainContainer']['generalPage']['rightSide']['noWidgetSection']['selectWrapper']['sendMessagesAnd']['7'] = '7:00 AM';
$_LANG['addonAA']['configuration']['mainContainer']['generalPage']['rightSide']['noWidgetSection']['selectWrapper']['sendMessagesAnd']['8'] = '8:00 AM';
$_LANG['addonAA']['configuration']['mainContainer']['generalPage']['rightSide']['noWidgetSection']['selectWrapper']['sendMessagesAnd']['9'] = '9:00 AM';
$_LANG['addonAA']['configuration']['mainContainer']['generalPage']['rightSide']['noWidgetSection']['selectWrapper']['sendMessagesAnd']['10'] = '10:00 AM';
$_LANG['addonAA']['configuration']['mainContainer']['generalPage']['rightSide']['noWidgetSection']['selectWrapper']['sendMessagesAnd']['11'] = '11:00 AM';
$_LANG['addonAA']['configuration']['mainContainer']['generalPage']['rightSide']['noWidgetSection']['selectWrapper']['sendMessagesAnd']['12'] = '12:00 AM (noon)';
$_LANG['addonAA']['configuration']['mainContainer']['generalPage']['rightSide']['noWidgetSection']['selectWrapper']['sendMessagesAnd']['13'] = '1:00 PM';
$_LANG['addonAA']['configuration']['mainContainer']['generalPage']['rightSide']['noWidgetSection']['selectWrapper']['sendMessagesAnd']['14'] = '2:00 PM';
$_LANG['addonAA']['configuration']['mainContainer']['generalPage']['rightSide']['noWidgetSection']['selectWrapper']['sendMessagesAnd']['15'] = '3:00 PM';
$_LANG['addonAA']['configuration']['mainContainer']['generalPage']['rightSide']['noWidgetSection']['selectWrapper']['sendMessagesAnd']['16'] = '4:00 PM';
$_LANG['addonAA']['configuration']['mainContainer']['generalPage']['rightSide']['noWidgetSection']['selectWrapper']['sendMessagesAnd']['17'] = '5:00 PM';
$_LANG['addonAA']['configuration']['mainContainer']['generalPage']['rightSide']['noWidgetSection']['selectWrapper']['sendMessagesAnd']['18'] = '6:00 PM';
$_LANG['addonAA']['configuration']['mainContainer']['generalPage']['rightSide']['noWidgetSection']['selectWrapper']['sendMessagesAnd']['19'] = '7:00 PM';
$_LANG['addonAA']['configuration']['mainContainer']['generalPage']['rightSide']['noWidgetSection']['selectWrapper']['sendMessagesAnd']['20'] = '8:00 PM';
$_LANG['addonAA']['configuration']['mainContainer']['generalPage']['rightSide']['noWidgetSection']['selectWrapper']['sendMessagesAnd']['21'] = '9:00 PM';
$_LANG['addonAA']['configuration']['mainContainer']['generalPage']['rightSide']['noWidgetSection']['selectWrapper']['sendMessagesAnd']['22'] = '10:00 PM';
$_LANG['addonAA']['configuration']['mainContainer']['generalPage']['rightSide']['noWidgetSection']['selectWrapper']['sendMessagesAnd']['23'] = '11:00 PM';


$_LANG['addonAA']['configuration']['mainContainer']['generalPage']['rightSide']['noWidgetSection']['countries']['countries'] = 'Countries';



/** Configuration Administrator* */
$_LANG['addonAA']['configuration']['mainContainer']['administratorsPage']['table']['id']                                                                    = 'ID';
$_LANG['addonAA']['configuration']['mainContainer']['administratorsPage']['table']['firstname']                                                             = 'Administrator Name';
$_LANG['addonAA']['configuration']['mainContainer']['administratorsPage']['table']['name']                                                                  = 'Group';
$_LANG['addonAA']['configuration']['mainContainer']['generalPage']['leftSide']['noWidgetSection']['enableAdvancedSetting']['enableAdvancedSetting']         = 'Enable Advanced Settings';

/* * Submodule Advanced Settings* */
$_LANG['addonAA']['pagesLabels']['configuration']['submoduleSettings']                                                            = 'Advanced Settings';
$_LANG['addonAA']['configuration']['mainContainer']['administratorsPage']['addAdvancedSettings']['button']['addAdvancedSettings'] = 'Add Settings';
$_LANG['addonAA']['configuration']['mainContainer']['administratorsPage']['table']['submodule']                                   = 'Submodule';
$_LANG['addonAA']['configuration']['mainContainer']['administratorsPage']['table']['settingName']                                 = 'Settings Name';
$_LANG['addonAA']['configuration']['mainContainer']['administratorsPage']['table']['default']                                     = 'Default';
$_LANG['addonAA']['configuration']['mainContainer']['administratorsPage']['table']['country']                                     = 'Country';


/** TAB LANGS * */
$_LANG['addonAA']['templates']['mainContainer']['templatesPage']['supportTab']      = 'Support Messages';
$_LANG['addonAA']['templates']['mainContainer']['templatesPage']['invoiceTab']      = 'Invoice Messages';
$_LANG['addonAA']['templates']['mainContainer']['templatesPage']['productTab']      = 'Product/Service Messages';
$_LANG['addonAA']['templates']['mainContainer']['templatesPage']['generalTab']      = 'General Messages';
$_LANG['addonAA']['templates']['mainContainer']['templatesPage']['notificationTab'] = 'Notification Messages';
$_LANG['addonAA']['templates']['mainContainer']['templatesPage']['domainTab']       = 'Domain Messages';
$_LANG['addonAA']['templates']['mainContainer']['templatesPage']['affiliateTab']    = 'Affiliate Messages';
$_LANG['addonAA']['templates']['mainContainer']['templatesPage']['adminTab']        = 'Admin Messages';
$_LANG['addonAA']['templates']['mainContainer']['templatesPage']['userTab']         = 'User Messages';


/** ADVANCED SETTINGS NAMES* */
$_LANG['addonAA']['configuration']['mainContainer']['administratorsPage']['BearSms'] = 'BearSMS';




/** Client Area Configuration * */
$_LANG['addonCA']['configuration']['mainContainer']['clientConfiguration']['clientConfiguration'] = 'Configuration';
$_LANG['addonCA']['configuration']['mainContainer']['clientConfiguration']['table']['msg_name']   = 'Template Name';
$_LANG['addonCA']['configuration']['mainContainer']['clientConfiguration']['table']['enabled']    = 'Status';

$_LANG['addonAA']['configuration']['addAdvancedSettingsModal']['advancedSettingsSubmoduleForm']['submoduleSettingName']['submoduleSettingName'] = 'Settings Name';

//$_LANG['addonAA']['logger']['changeStatusTemplate']       = "The template <b>':changeStatusDiscountsName:' (ID: :changeStatusDiscountsId:)</b> has been successfully changed to <b>':changeStatusDiscountsValue:'</b>.";

$_LANG['addonAA']['pagesLabels']['label']['tools']   = 'Tools';
$_LANG['addonAA']['pagesLabels']['tools']['sendSms'] = 'Send SMS';
$_LANG['smsCenter']                                  = 'SMS Center';
$_LANG['addonCA']['smsCenter']                       = 'SMS Center';

$_LANG['addonCA']['configuration']['mainContainer']['clientConfigurationPage']['clientConfigurationPage'] = 'Templates';

$_LANG['addonCA']['configuration']['mainContainer']['clientConfigurationPage']['supportTab']      = 'Support';
$_LANG['addonCA']['configuration']['mainContainer']['clientConfigurationPage']['invoiceTab']      = 'Invoice';
$_LANG['addonCA']['configuration']['mainContainer']['clientConfigurationPage']['productTab']      = 'Products';
$_LANG['addonCA']['configuration']['mainContainer']['clientConfigurationPage']['generalTab']      = 'General';
$_LANG['addonCA']['configuration']['mainContainer']['clientConfigurationPage']['notificationTab'] = 'Notification';
$_LANG['addonCA']['configuration']['mainContainer']['clientConfigurationPage']['domainTab']       = 'Domain';
$_LANG['addonCA']['configuration']['mainContainer']['clientConfigurationPage']['affiliateTab']    = 'Affiliate';
$_LANG['addonCA']['configuration']['mainContainer']['clientConfigurationPage']['adminTab']        = 'Admin';
$_LANG['addonCA']['configuration']['mainContainer']['clientConfigurationPage']['userTab']         = 'User';

$_LANG['addonCA']['configuration']['mainContainer']['clientConfigurationPage']['support']['table']['msg_name'] = 'Template Name';
$_LANG['addonCA']['configuration']['mainContainer']['clientConfigurationPage']['support']['table']['enabled']  = 'Status';

$_LANG['addonCA']['configuration']['mainContainer']['clientConfigurationPage']['invoice']['table']['msg_name'] = 'Template Name';
$_LANG['addonCA']['configuration']['mainContainer']['clientConfigurationPage']['invoice']['table']['enabled']  = 'Status';

$_LANG['addonCA']['configuration']['mainContainer']['clientConfigurationPage']['product']['table']['msg_name'] = 'Template Name';
$_LANG['addonCA']['configuration']['mainContainer']['clientConfigurationPage']['product']['table']['enabled']  = 'Status';

$_LANG['addonCA']['configuration']['mainContainer']['clientConfigurationPage']['general']['table']['msg_name'] = 'Template Name';
$_LANG['addonCA']['configuration']['mainContainer']['clientConfigurationPage']['general']['table']['enabled']  = 'Status';

$_LANG['addonCA']['configuration']['mainContainer']['clientConfigurationPage']['notification']['table']['msg_name'] = 'Template Name';
$_LANG['addonCA']['configuration']['mainContainer']['clientConfigurationPage']['notification']['table']['enabled']  = 'Status';

$_LANG['addonCA']['configuration']['mainContainer']['clientConfigurationPage']['domain']['table']['msg_name'] = 'Template Name';
$_LANG['addonCA']['configuration']['mainContainer']['clientConfigurationPage']['domain']['table']['enabled']  = 'Status';

$_LANG['addonCA']['configuration']['mainContainer']['clientConfigurationPage']['affiliate']['table']['msg_name'] = 'Template Name';
$_LANG['addonCA']['configuration']['mainContainer']['clientConfigurationPage']['affiliate']['table']['enabled']  = 'Status';

$_LANG['addonCA']['configuration']['mainContainer']['clientConfigurationPage']['admin']['table']['msg_name'] = 'Template Name';
$_LANG['addonCA']['configuration']['mainContainer']['clientConfigurationPage']['admin']['table']['enabled']  = 'Status';

$_LANG['addonCA']['configuration']['mainContainer']['clientConfigurationPage']['user']['table']['msg_name'] = 'Template Name';
$_LANG['addonCA']['configuration']['mainContainer']['clientConfigurationPage']['user']['table']['enabled']  = 'Status';


$_LANG['addonAA']['configuration']['addAdvancedSettingsModal']['baseAcceptButton']['title'] = 'Save Changes';
$_LANG['addonAA']['configuration']['addAdvancedSettingsModal']['baseCancelButton']['title'] = 'Cancel';

$_LANG['addonAA']['configurationPage']['testConnectionButton']['errorLogg'] = 'The <b>:submodule:</b> <b>:setting:</b> connection error has occurred:  <b>:error:</b>';

//Configuration // Admin

$_LANG['addonAA']['configuration']['editAdvancedSettingsModal']['modal']['editAdvancedSettingsModal']                                                                    = 'Configure Gateway';
$_LANG['addonAA']['configuration']['editAdvancedSettingsModal']['baseAcceptButton']['title']                                                                             = 'Confirm';
$_LANG['addonAA']['configuration']['editAdvancedSettingsModal']['baseCancelButton']['title']                                                                             = 'Cancel';
$_LANG['addonAA']['configuration']['administratorNotifications']['administratorNotifications']['adminNumber']['adminNumber']                                             = 'Number';
$_LANG['addonAA']['configuration']['administratorNotifications']['administratorNotifications']['automaticSetupFailed']['AutomaticSetupFailed']                           = 'Automatic Setup Failed';
$_LANG['addonAA']['configuration']['administratorNotifications']['administratorNotifications']['automaticSetupSuccessful']['AutomaticSetupSuccessful']                   = 'Automatic Setup Successful';
$_LANG['addonAA']['configuration']['administratorNotifications']['administratorNotifications']['domainRenewalFailed']['DomainRenewalFailed']                             = 'Domain Renewal Failed';
$_LANG['addonAA']['configuration']['administratorNotifications']['administratorNotifications']['domainRenewalSuccessful']['DomainRenewalSuccessful']                     = 'Domain Renewal Successful';
$_LANG['addonAA']['configuration']['administratorNotifications']['administratorNotifications']['newCancellationRequest']['NewCancellationRequest']                       = 'New Cancellation Request';
$_LANG['addonAA']['configuration']['administratorNotifications']['administratorNotifications']['newOrderNotification']['NewOrderNotification']                           = 'New Order Notification';
$_LANG['addonAA']['configuration']['administratorNotifications']['administratorNotifications']['serviceUnsuspensionFailed']['ServiceUnsuspensionFailed']                 = 'Service Unsuspension Failed';
$_LANG['addonAA']['configuration']['administratorNotifications']['administratorNotifications']['serviceUnsuspensionSuccessful']['ServiceUnsuspensionSuccessful']         = 'Service Unsuspension Successful';
$_LANG['addonAA']['configuration']['administratorNotifications']['administratorNotifications']['supportTicketCreated']['SupportTicketCreated']                           = 'Support Ticket Created';
$_LANG['addonAA']['configuration']['administratorNotifications']['administratorNotifications']['supportTicketDepartmentReassigned']['SupportTicketDepartmentReassigned'] = 'Support Ticket Department Reassigned';
$_LANG['addonAA']['configuration']['administratorNotifications']['administratorNotifications']['supportTicketFlagged']['SupportTicketFlagged']                           = 'Support Ticket Flagged';
$_LANG['addonAA']['configuration']['administratorNotifications']['advancedSettingsSubmoduleForm']['supportTicketResponse']['SupportTicketResponse']                      = 'Support Ticket Response';
$_LANG['addonAA']['configuration']['administratorNotifications']['advancedSettingsSubmoduleForm']['aminNumber']['aminNumber']                                            = 'Mobile Number';
$_LANG['addonAA']['configuration']['administratorNotifications']['advancedSettingsSubmoduleForm']['d_code']['d_code']                                                    = 'Code';
$_LANG['addonAA']['configuration']['administratorNotifications']['adminisratorNotifcations']['serviceSuspendNotification']['ServiceSuspendNotification']                 = 'Service Suspend Notification';
$_LANG['addonAA']['configuration']['administratorNotifications']['adminisratorNotifcations']['orderPaidNotification']['OrderPaidNotification']                           = 'Order Paid Notification';
$_LANG['addonAA']['pagesLabels']['configuration']['rules']                                                                                                               = 'Rules';
//Templates variables :
$_LANG['invoicespaid']                                                                                                                                                   = 'Paid';
$_LANG['invoicesunpaid']                                                                                                                                                 = 'Unpaid';
$_LANG['invoicesdraft']                                                                                                                                                  = 'Draft';
$_LANG['invoicescancelled']                                                                                                                                              = 'Canceled';
$_LANG['invoicesrefunded']                                                                                                                                               = 'Refunded';
$_LANG['invoicescollections']                                                                                                                                            = 'Collections';
$_LANG['invoicespaymentpending']                                                                                                                                         = 'Payment Pending';

$_LANG['addonAA']['configuration']['administratorNotifications']['modal']['administratorNotifications']                                                                = 'Configure Administrator Notifications';
//Rules Page
$_LANG['addonAA']['configuration']['administratorNotifications']['adminisratorNotifcations']['adminNumber']['adminNumber']                                             = 'Number';
$_LANG['addonAA']['configuration']['administratorNotifications']['adminisratorNotifcations']['d_code']['d_code']                                                       = 'Code';
$_LANG['addonAA']['configuration']['administratorNotifications']['adminisratorNotifcations']['automaticSetupFailed']['AutomaticSetupFailed']                           = 'Automatic Setup Failed';
$_LANG['addonAA']['configuration']['administratorNotifications']['adminisratorNotifcations']['automaticSetupSuccessful']['AutomaticSetupSuccessful']                   = 'Automatic Setup Successful';
$_LANG['addonAA']['configuration']['administratorNotifications']['adminisratorNotifcations']['domainRenewalFailed']['DomainRenewalFailed']                             = 'Domain Renewal Failed';
$_LANG['addonAA']['configuration']['administratorNotifications']['adminisratorNotifcations']['domainRenewalSuccessful']['DomainRenewalSuccessful']                     = 'Domain Renewal Successful';
$_LANG['addonAA']['configuration']['administratorNotifications']['adminisratorNotifcations']['newCancellationRequest']['NewCancellationRequest']                       = 'New Cancellation Request';
$_LANG['addonAA']['configuration']['administratorNotifications']['adminisratorNotifcations']['newOrderNotification']['NewOrderNotification']                           = 'New Order Notification';
$_LANG['addonAA']['configuration']['administratorNotifications']['adminisratorNotifcations']['addon']['addon']                                                         = 'Addon';
$_LANG['addonAA']['configuration']['administratorNotifications']['adminisratorNotifcations']['product']['product']                                                     = 'Product';
$_LANG['addonAA']['configuration']['administratorNotifications']['adminisratorNotifcations']['domain']['domain']                                                       = 'Domain';
$_LANG['addonAA']['configuration']['administratorNotifications']['adminisratorNotifcations']['serviceUnsuspensionFailed']['ServiceUnsuspensionFailed']                 = 'Service Unsuspension Failed';
$_LANG['addonAA']['configuration']['administratorNotifications']['adminisratorNotifcations']['serviceUnsuspensionSuccessful']['ServiceUnsuspensionSuccessful']         = 'Service Unsuspension Successful';
$_LANG['addonAA']['configuration']['administratorNotifications']['adminisratorNotifcations']['supportTicketCreated']['SupportTicketCreated']                           = 'Support Ticket Created';
$_LANG['addonAA']['configuration']['administratorNotifications']['adminisratorNotifcations']['priorityCreated']['priorityCreated']                                     = 'Priority Created';
$_LANG['addonAA']['configuration']['administratorNotifications']['adminisratorNotifcations']['supportTicketDepartmentReassigned']['SupportTicketDepartmentReassigned'] = 'Support Ticket Department Reassigned';
$_LANG['addonAA']['configuration']['administratorNotifications']['adminisratorNotifcations']['supportTicketFlagged']['SupportTicketFlagged']                           = 'Support Ticket Flagged';
$_LANG['addonAA']['configuration']['administratorNotifications']['adminisratorNotifcations']['priorityFlagged']['priorityFlagged']                                     = 'Priority Flagged';
$_LANG['addonAA']['configuration']['administratorNotifications']['adminisratorNotifcations']['supportTicketResponse']['SupportTicketResponse']                         = 'Support Ticket Response';
$_LANG['addonAA']['configuration']['administratorNotifications']['adminisratorNotifcations']['clientLoginNotification']['ClientLoginNotification']                     = 'Client Login Notification';
$_LANG['addonAA']['configuration']['administratorNotifications']['baseAcceptButton']['title']                                                                          = 'Confirm';
$_LANG['addonAA']['configuration']['administratorNotifications']['baseCancelButton']['title']                                                                          = 'Cancel';

$_LANG['addonAA']['configuration']['mainContainer']['rulesPage']['table']['id']          = 'ID';
$_LANG['addonAA']['configuration']['mainContainer']['rulesPage']['table']['submodule']   = 'Gateway';
$_LANG['addonAA']['configuration']['mainContainer']['rulesPage']['table']['settingName'] = 'Settings';
$_LANG['addonAA']['configuration']['mainContainer']['rulesPage']['table']['countries']   = 'Countries';
$_LANG['addonAA']['configuration']['mainContainer']['rulesPage']['table']['default']     = 'Default';
$_LANG['addonAA']['configuration']['mainContainer']['rulesPage']['table']['status']      = 'Status';

$_LANG['SmsCenter']['generateToken']['client']['default']                                                                          = 'The new generated token is :token:';
//$_LANG['addonCA']['clientVerification']['mainContainer']['clientVerification']['baseSection']['tokenVerify']['tokenVerify']        = 'Please provide your one-time verification token to activate your account';
//$_LANG['addonCA']['clientVerification']['mainContainer']['clientVerification']['baseSubmitButton']['button']['submit']             = 'Submit';
//$_LANG['addonCA']['clientVerification']['mainContainer']['clientVerification']['resendTokenButton']['button']['resendTokenButton'] = 'Resend Token';
$_LANG['clientNotNumber']                                                                                                          = 'You need to add the SMS number first';
$_LANG['resendTokkenAttemptsFailed']                                                                                               = 'The next token can be sent after :time:';
$_LANG['resendTokkenAttemptsFailedNew']                                                                                            = 'The next token can be sent after :time:';
$_LANG['resendTokenAttemptsFailed']                                                                                                = 'The next token can be sent after :time:';
$_LANG['incorectToken']                                                                                                            = 'The entered token is incorrect';
$_LANG['veryfivationSuccessfull']                                                                                                  = 'The token verification has been performed successfully';
$_LANG['dontNeedVeryfiaction']                                                                                                     = 'This account has been verified successfully';
$_LANG['unavailableCountry']                                                                                                       = 'You cannot get the verification token. The country is unavailable.';

$_LANG['addonAA']['smsQueue']['DataTable']['Status']['start']       = 'Start';
$_LANG['addonAA']['smsQueue']['DataTable']['Status']['in_progress'] = 'In Progress';
$_LANG['addonAA']['smsQueue']['DataTable']['Status']['aborted']     = 'Aborted';
$_LANG['addonAA']['smsQueue']['DataTable']['Status']['canceled']    = 'Canceled';
$_LANG['addonAA']['smsQueue']['DataTable']['Status']['finished']    = 'Finished';
$_LANG['addonAA']['smsQueue']['DataTable']['Status']['error']       = 'Error';

$_LANG['addonAA']['smsQueue']['queueStartModal']['modal']['queueStartModal']                = 'Start';
$_LANG['addonAA']['smsQueue']['queueStartModal']['baseAcceptButton']['title']               = 'Confirm';
$_LANG['addonAA']['smsQueue']['queueStartModal']['baseCancelButton']['title']               = 'Cancel';
$_LANG['addonAA']['smsQueue']['queueStartModal']['queueStartForm']['confirmQueueStartForm'] = 'Are you sure that you want to start this task?';

$_LANG['addonAA']['smsQueue']['queueRefreshModal']['modal']['QueueRefreshModal']                  = 'Refresh';
$_LANG['addonAA']['smsQueue']['queueRefreshModal']['baseAcceptButton']['title']                   = 'Confirm';
$_LANG['addonAA']['smsQueue']['queueRefreshModal']['baseCancelButton']['title']                   = 'Cancel';
$_LANG['addonAA']['smsQueue']['queueRefreshModal']['queueRefreshForm']['confirmQueueRefreshForm'] = 'Are you sure that you want to refresh this task?';

$_LANG['addonAA']['smsQueue']['queueDeleteModal']['modal']['QueueDeleteModal']                 = 'Delete';
$_LANG['addonAA']['smsQueue']['queueDeleteModal']['baseAcceptButton']['title']                 = 'Confirm';
$_LANG['addonAA']['smsQueue']['queueDeleteModal']['baseCancelButton']['title']                 = 'Cancel';
$_LANG['addonAA']['smsQueue']['queueDeleteModal']['queueDeleteForm']['confirmQueueDeleteForm'] = 'Are you sure that you want to delete this task?';

//$_LANG['addonCA']['clientVerification']['mainContainer']['orderVerification']['baseSection']['tokenVerify']['tokenVerify']        = 'Please provide one-time verification token to activate your order';
//$_LANG['addonCA']['clientVerification']['mainContainer']['orderVerification']['baseSubmitButton']['button']['submit']             = 'Submit';
//$_LANG['addonCA']['clientVerification']['mainContainer']['orderVerification']['resendTokenButton']['button']['resendTokenButton'] = 'Resend Token';

$_LANG['addonAA']['configuration']['mainContainer']['administratorsPage']['table']['status'] = 'Status';

$_LANG['addonCA']['configuration']['mainContainer']['clientConfigurationPage']['support']['table']['status'] = 'Status';

$_LANG['addonAA']['tools']['mainContainer']['sendSmsPage']['boxDoubleSection']['noWidgetSection']['number']['number'] = 'Number';

$_LANG['addonAA']['tools']['mainContainer']['sendSmsPage']['boxDoubleSection']['noWidgetSection']['rules']['rules']     = 'Country';
$_LANG['addonAA']['tools']['mainContainer']['sendSmsPage']['boxDoubleSection']['noWidgetSection']['message']['message'] = 'Message';
$_LANG['addonAA']['tools']['mainContainer']['sendSmsPage']['baseSubmitButton']['button']['submit']                      = 'Send';
$_LANG['addonAA']['tools']['mainContainer']['sendSmsPage']['boxDoubleSection']['sendSmsWidget']                         = 'Send SMS';
$_LANG['cannotSendSms']                                                                                                 = 'The SMS message cannot be sent';
$_LANG['sendSmsSuccessfully']                                                                                           = 'The SMS message has been sent successfully';
$_LANG['sendSmsSuccessfullyQueued']                                                                                     = 'The message has been added to the queue successfully';

//$_LANG['hookLoger']['logger']['success']['sendManualSms'] = 'The message has been sent manually to the number <b>:param1:</b> in <b>:param2:</b> by <b>:param3:</b>';
//$_LANG['hookLoger']['logger']['error']['sendManualSms']   = 'The message to <b>:param1:</b> by <b>:param3:</b> cannot be sent (:param4:) :errorMessage:';

$_LANG['addonAA']['logs']['button']['massDeleteLoggerButton']                                 = 'Delete';
$_LANG['addonAA']['logs']['massDeleteLoggerModal']['modal']['massDeleteLoggerModal']          = 'Delete Logs';
$_LANG['addonAA']['logs']['massDeleteLoggerModal']['deleteLoggerForm']['confirmLabelRemoval'] = 'Are you sure that you want to delete the selected logs?';
$_LANG['addonAA']['logs']['massDeleteLoggerModal']['baseAcceptButton']['title']               = 'Confirm';
$_LANG['addonAA']['logs']['massDeleteLoggerModal']['baseCancelButton']['title']               = 'Cancel';

$_LANG['addonAA']['smsQueue']['massActionQueueStartModal']['modal']['massActionQueueStartModal'] = 'Start';

$_LANG['addonAA']['smsQueue']['massActionQueueStartModal']['massActionQueueStartForm']['confirmMassActionQueueStartForm'] = 'Are you sure that you want to run the selected tasks?';
$_LANG['addonAA']['smsQueue']['massActionQueueStartModal']['baseAcceptButton']['title']                                   = 'Confirm';
$_LANG['addonAA']['smsQueue']['massActionQueueStartModal']['baseCancelButton']['title']                                   = 'Cancel';

$_LANG['addonAA']['smsQueue']['massActionQueueRefreshModal']['modal']['massActionQueueRefreshModal'] = 'Refresh';
$_LANG['addonAA']['smsQueue']['massActionQueueDeleteModal']['modal']['massActionQueueDeleteModal']   = 'Delete';

$_LANG['addonAA']['smsQueue']['massActionQueueDeleteModal']['massActionQueueDeleteForm']['confirmMassActionQueueDeleteForm'] = 'Are you sure that you want to delete the selected tasks?';
$_LANG['addonAA']['smsQueue']['massActionQueueDeleteModal']['baseAcceptButton']['title']                                     = 'Confirm';
$_LANG['addonAA']['smsQueue']['massActionQueueDeleteModal']['baseCancelButton']['title']                                     = 'Cancel';

$_LANG['addonAA']['smsQueue']['massActionQueueRefreshModal']['massActionQueueRefreshForm']['confirmMassActionQueueRefreshForm'] = 'Are you sure that you want to refresh the status of the selected tasks?';
$_LANG['deleteMassSettingsSuccessfully']                                                                                        = 'Settings deleted successfully';
$_LANG['addonAA']['smsQueue']['massActionQueueRefreshModal']['baseAcceptButton']['title']                                       = 'Confirm';
$_LANG['addonAA']['smsQueue']['massActionQueueRefreshModal']['baseCancelButton']['title']                                       = 'Cancel';
//$_LANG['hookLoger']['logger']['error']['sendSmsFromQueue']                                                                      = 'The process of SMS sending from the task <b>:param1:</b> has failed :errorMessage:';
$_LANG['addonAA']['pagesLabels']['smsQueue']['index']                                                                           = 'Table';
$_LANG['resendTokkenAttempts']                                                                                                  = 'The limit of token reset attempts has been reached';
$_LANG['addonAA']['pagesLabels']['label']['Templates']                                                                          = 'Templates';
$_LANG['addonAA']['pagesLabels']['Templates']['settings']                                                                       = 'Settings';

$_LANG['changesSaveFailed']                                                                        = 'You need to set the countries first';
$_LANG['addonAA']['configuration']['ruleModal']['modal']['ruleModal']                              = 'Edit Rule';
$_LANG['addonAA']['configuration']['ruleModal']['ruleForm']['country']['country']                  = 'Countries';
$_LANG['addonAA']['configuration']['ruleModal']['ruleForm']['defaultGateway']['defaultGateway']    = 'Default Rule';
$_LANG['addonAA']['configuration']['ruleModal']['baseAcceptButton']['title']                       = 'Confirm';
$_LANG['addonAA']['configuration']['ruleModal']['baseCancelButton']['title']                       = 'Cancel';
$_LANG['addonAA']['configuration']['deleteLoggerModal']['modal']['deleteLoggerModal']              = 'Delete';
$_LANG['addonAA']['configuration']['deleteLoggerModal']['deleteLoggerForm']['confirmLabelRemoval'] = 'Are you sure that you want to delete this setting?';
$_LANG['addonAA']['configuration']['deleteLoggerModal']['baseAcceptButton']['title']               = 'Confirm';
$_LANG['addonAA']['configuration']['deleteLoggerModal']['baseCancelButton']['title']               = 'Cancel';

$_LANG['addonAA']['configuration']['editAdvancedSettingsModal']['editAdvancedSettingsSubmoduleForm']['user']['user']       = 'User';
$_LANG['addonAA']['configuration']['addAdvancedSettingsModal']['advancedSettingsSubmoduleForm']['user']['user']            = 'User';
$_LANG['addonAA']['configuration']['editAdvancedSettingsModal']['editAdvancedSettingsSubmoduleForm']['hashkey']['hashkey'] = 'Key';
$_LANG['addonAA']['configuration']['addAdvancedSettingsModal']['modal']['addAdvancedSettingsModal']                        = 'New Settings';
$_LANG['connectionError']                                                                                                  = 'The connection test has failed';
$_LANG['connectionSuccess']                                                                                                = 'The connection test has been performed successfully';

$_LANG['addonAA']['templates']['editLangModal']['modal']['editLangModal']             = 'Edit';
$_LANG['addonAA']['templates']['editLangModal']['editLangForm']['lang']['lang']       = 'Language';
$_LANG['addonAA']['templates']['editLangModal']['editLangForm']['message']['message'] = 'Message';

$_LANG['addonAA']['templates']['editLangModal']['editLangForm']['support']['ticketRelated']   = 'Support Related';
$_LANG['addonAA']['templates']['editLangModal']['editLangForm']['client']['ticketRelated']    = 'Client Related';
$_LANG['addonAA']['templates']['editLangModal']['editLangForm']['affiliate']['ticketRelated'] = 'Affiliate';
$_LANG['addonAA']['templates']['editLangModal']['editLangForm']['other']['ticketRelated']     = 'Other';
$_LANG['addonAA']['templates']['editLangModal']['editLangForm']['invoice']['ticketRelated']   = 'Invoice Related';

$_LANG['addonAA']['templates']['editLangModal']['editLangForm']['halfPageSection']['admin']['ticketRelated'] = 'Administrator';
$_LANG['Admin First Name'] = 'Admin First Name';
$_LANG['Admin Last Name'] = 'Admin Last Name';
$_LANG['Admin Username'] = 'Admin Username';

$_LANG['addonAA']['templates']['editLangModal']['editLangForm']['halfPageSection']['order']['ticketRelated'] = 'Order';
$_LANG['Order Id'] = 'Order ID';
$_LANG['Order Number'] = 'Order Number';
$_LANG['Order Date'] = 'Order Date';
$_LANG['Order Payment Method'] = 'Order Payment Method';
$_LANG['Order Notes'] = 'Order Notes';
$_LANG['Order User'] = 'Order User';
$_LANG['Order Nameservers'] = 'Order Nameservers';
$_LANG['Order Amount'] = 'Order Amount';

$_LANG['addonAA']['templates']['editLangModal']['baseAcceptButton']['title'] = 'Save';
$_LANG['addonAA']['templates']['editLangModal']['baseCancelButton']['title'] = 'Close';

$_LANG['addonAA']['templates']['deleteLangModal']['modal']['deleteLangModal'] = 'Delete Translation';

$_LANG['addonAA']['templates']['deleteLangModal']['deleteLangForm']['confirmLabelRemoval'] = 'Are you sure that you want to delete this template translation?';
$_LANG['addonAA']['templates']['deleteLangModal']['baseAcceptButton']['title']             = 'Confirm';
$_LANG['addonAA']['templates']['deleteLangModal']['baseCancelButton']['title']             = 'Close';

$_LANG['addonAA']['templates']['addDiscountModal']['addDiscountForm']['support']['ticketRelated'] = 'Support Related';
$_LANG['addonAA']['templates']['addDiscountModal']['addDiscountForm']['client']['ticketRelated']  = 'Client Related';
$_LANG['addonAA']['templates']['addDiscountModal']['addDiscountForm']['invoice']['ticketRelated'] = 'Invoice Related';


$_LANG['addonAA']['tools']['mainContainer']['sendSmsPage']['boxDoubleSection']['noWidgetSection']['clients']['clients'] = 'Client';
$_LANG['tokenResendFailed']                                                                                             = 'Cannot resend your verification token';
$_LANG['dontNeedVeryfiactionOrder']                                                                                     = 'This order does not require the token verification';
$_LANG['dontNeedVerifiactionInvoice']                                                                                   = 'This invoice does not require the token verification';

$_LANG['addonCA']['configuration']['mainContainer']['clientConfigurationPage']['invoice']['table']['status']   = 'Status';
$_LANG['addonCA']['configuration']['mainContainer']['clientConfigurationPage']['product']['table']['status']   = 'Status';
$_LANG['addonCA']['configuration']['mainContainer']['clientConfigurationPage']['general']['table']['status']   = 'Status';
$_LANG['addonCA']['configuration']['mainContainer']['clientConfigurationPage']['domain']['table']['status']    = 'Status';
$_LANG['addonCA']['configuration']['mainContainer']['clientConfigurationPage']['affiliate']['table']['status'] = 'Status';

$_LANG['SmsCenter']['generateToken']['order']['default'] = 'The new generated token is :token:';
$_LANG['SmsCenter']['generateToken']['invoice']['default'] = 'The new generated token is :token:';
//MassSms
$_LANG['addonAA']['massSms']['mainContainer']['massSmsPage']['product']['table']['id']      = 'ID';
$_LANG['addonAA']['massSms']['mainContainer']['massSmsPage']['product']['table']['name']    = 'Name';
$_LANG['addonAA']['massSms']['mainContainer']['massSmsPage']['product']['table']['message'] = 'Message';

$_LANG['addonAA']['massSms']['mainContainer']['massSmsPage']['general']['table']['id']      = 'ID';
$_LANG['addonAA']['massSms']['mainContainer']['massSmsPage']['general']['table']['name']    = 'Name';
$_LANG['addonAA']['massSms']['mainContainer']['massSmsPage']['general']['table']['message'] = 'Message';

$_LANG['addonAA']['massSms']['mainContainer']['massSmsPage']['domain']['table']['id']      = 'ID';
$_LANG['addonAA']['massSms']['mainContainer']['massSmsPage']['domain']['table']['name']    = 'Name';
$_LANG['addonAA']['massSms']['mainContainer']['massSmsPage']['domain']['table']['message'] = 'Message';

$_LANG['addonAA']['massSms']['mainContainer']['massSmsPage']['generalTab'] = 'General';
$_LANG['addonAA']['massSms']['mainContainer']['massSmsPage']['productTab'] = 'Product';
$_LANG['addonAA']['massSms']['mainContainer']['massSmsPage']['domainTab']  = 'Domain';

$_LANG['addonAA']['massSms']['mainContainer']['massSmsPage']['massSmsPage'] = 'Templates';
$_LANG['addonAA']['pagesLabels']['massSms']['templates']                    = 'Templates';

$_LANG['addonAA']['pagesLabels']['massSms']['sms'] = 'Mass SMS Configuration';

$_LANG['addonAA']['massSms']['mainContainer']['filterPage']['boxDoubleSection']['clientFilters']                                                 = 'Client Filters';
$_LANG['addonAA']['massSms']['mainContainer']['filterPage']['boxDoubleSection']['clientStatus']                                                  = 'Client Status';
$_LANG['addonAA']['massSms']['mainContainer']['filterPage']['boxDoubleSection']['customFields']                                                  = 'Custom Fields';
$_LANG['addonAA']['massSms']['mainContainer']['filterPage']['boxDoubleSection']['setLanguages']                                                  = 'Language';
$_LANG['addonAA']['massSms']['mainContainer']['filterPage']['boxDoubleSection']['noWidgetSection']['languages']['languages']                     = 'Language';
$_LANG['addonAA']['massSms']['mainContainer']['filterPage']['boxSection']['clientFilters']                                                       = 'Client Filters';
$_LANG['addonAA']['massSms']['mainContainer']['filterPage']['boxDoubleSection']['halfPageSection']['product']['product']                         = 'Product';
$_LANG['addonAA']['massSms']['mainContainer']['filterPage']['boxDoubleSection']['halfPageSection']['productStatus']['productStatus']             = 'Product Status';
$_LANG['addonAA']['massSms']['mainContainer']['filterPage']['boxDoubleSection']['halfPageSection']['assignedServer']['assignedServer']           = 'Assigned Server';
$_LANG['addonAA']['massSms']['mainContainer']['filterPage']['boxDoubleSection']['noWidgetSection']['enabledClientStatus']['enabledClientStatus'] = 'Select Client Status';
$_LANG['addonAA']['massSms']['mainContainer']['filterPage']['boxDoubleSection']['noWidgetSection']['marketingSms']['marketingSms']               = 'Marketing SMS';

$_LANG['addonAA']['massSms']['mainContainer']['filterPage']['boxDoubleSection']['noWidgetSection']['enabledClientFilter']['enabledClientFilter'] = 'Client Filters';

$_LANG['addonAA']['massSms']['mainContainer']['massSmsPage']['addonTab'] = 'Addon';

$_LANG['addonAA']['massSms']['mainContainer']['massSmsPage']['general']['general']['button']['general']                           = 'Add New Template';
$_LANG['addonAA']['massSms']['mainContainer']['massSmsPage']['general']['product']['product']['button']['product']                = 'Add New Template';
$_LANG['addonAA']['massSms']['mainContainer']['massSmsPage']['general']['product']['domain']['domain']['button']['domain']        = 'Add New Template';
$_LANG['addonAA']['massSms']['mainContainer']['massSmsPage']['general']['product']['domain']['addon']['addon']['button']['addon'] = 'Add New Template';

$_LANG['addonAA']['massSms']['mainContainer']['massSmsPage']['general']['product']['table']['id']      = 'ID';
$_LANG['addonAA']['massSms']['mainContainer']['massSmsPage']['general']['product']['table']['name']    = 'Name';
$_LANG['addonAA']['massSms']['mainContainer']['massSmsPage']['general']['product']['table']['message'] = 'Message';

$_LANG['addonAA']['massSms']['mainContainer']['massSmsPage']['general']['product']['domain']['table']['id']      = 'ID';
$_LANG['addonAA']['massSms']['mainContainer']['massSmsPage']['general']['product']['domain']['table']['name']    = 'Name';
$_LANG['addonAA']['massSms']['mainContainer']['massSmsPage']['general']['product']['domain']['table']['message'] = 'Message';

$_LANG['addonAA']['massSms']['mainContainer']['massSmsPage']['general']['product']['domain']['addon']['table']['id']      = 'ID';
$_LANG['addonAA']['massSms']['mainContainer']['massSmsPage']['general']['product']['domain']['addon']['table']['name']    = 'Name';
$_LANG['addonAA']['massSms']['mainContainer']['massSmsPage']['general']['product']['domain']['addon']['table']['message'] = 'Message';

$_LANG['addonAA']['massSms']['addTplModal']['modal']['addTplModal']                                              = 'Add New Template';
$_LANG['addonAA']['massSms']['addTplModal']['addTplForm']['boxSection']['halfPageSection']['name']['name']       = 'Name';
$_LANG['addonAA']['massSms']['addTplModal']['addTplForm']['boxSection']['halfPageSection']['type']['type']       = 'Type';
$_LANG['addonAA']['massSms']['addTplModal']['addTplForm']['boxSection']['halfPageSection']['message']['message'] = 'Message';
$_LANG['addonAA']['massSms']['addTplModal']['baseAcceptButton']['title']                                         = 'Confirm';
$_LANG['addonAA']['massSms']['addTplModal']['baseCancelButton']['title']                                         = 'Close';

$_LANG['addonAA']['massSms']['addTplModal']['addTplForm']['boxSection']['halfPageSection']['product']['ticketRelated'] = 'Product Related';
$_LANG['addonAA']['massSms']['addTplModal']['addTplForm']['boxSection']['halfPageSection']['client']['ticketRelated']  = 'Client Related';
$_LANG['addonAA']['massSms']['addTplModal']['addTplForm']['boxSection']['halfPageSection']['other']['ticketRelated']   = 'Other';
$_LANG['addonAA']['massSms']['addTplModal']['addTplForm']['boxSection']['halfPageSection']['domain']['ticketRelated']  = 'Domain Related';

$_LANG['addonAA']['pagesLabels']['massSms']['smsStepTwo']                                                                        = 'Step Two';
$_LANG['addonAA']['massSms']['mainContainer']['clientFilterPage']['boxSection']['halfPageSection']['setTemplate']['setTemplate'] = 'Template';
$_LANG['addonAA']['massSms']['mainContainer']['clientFilterPage']['boxSection']['stepTwoMassSms']                                = 'Configuration';
$_LANG['addonAA']['massSms']['mainContainer']['clientFilterPage']['stepButton']['button']['stepButton']                          = 'Next Step';
$_LANG['addonAA']['massSms']['mainContainer']['filterPage']['stepButton']['button']['stepButton']                                = 'Next Step';

$_LANG['addonAA']['massSms']['mainContainer']['clientFilterPage']['boxSection']['halfPageSection']['addons']['addons']                 = 'Addon Filter';
$_LANG['addonAA']['massSms']['mainContainer']['clientFilterPage']['boxSection']['halfPageSection']['addonStatus']['addonStatus']       = 'Addon Status Filter';
$_LANG['addonAA']['massSms']['mainContainer']['clientFilterPage']['boxSection']['halfPageSection']['product']['product']               = 'Product';
$_LANG['addonAA']['massSms']['mainContainer']['clientFilterPage']['boxSection']['halfPageSection']['productStatus']['productStatus']   = 'Product Status';
$_LANG['addonAA']['massSms']['mainContainer']['clientFilterPage']['boxSection']['halfPageSection']['assignedServer']['assignedServer'] = 'Assigned Server';

$_LANG['addonAA']['massSms']['mainContainer']['clientFilterPage']['boxSection']['halfPageSection']['domainStatus']['domainStatus'] = 'Domain Status';

$_LANG['addonAA']['pagesLabels']['massSms']['massSmsView'] = 'View And Send';

$_LANG['addonAA']['massSms']['mainContainer']['massSmSView']['boxSection']['halfPageSection']['viewTemplate']['viewTemplate'] = 'Template';

$_LANG['addonAA']['massSms']['mainContainer']['filterPage']['baseSubmitButton']['button']['submit'] = 'Send And Save';

$_LANG['massRunTaskFinished'] = 'The SMS queue has been updated successfully';

$_LANG['messagessAddedtoQueueFailed']                                                          = 'The messages cannot be added to the queue';
$_LANG['messagessAddedtoQueueSuccessfully']                                                    = 'The messages have been added to the queue successfully';
$_LANG['runTaskSuccessfull']                                                                   = 'The SMS queue has been updated successfully';
$_LANG['refreshTaskSuccessfull']                                                               = 'The task has been refreshed successfully';
$_LANG['addonAA']['configuration']['mainContainer']['administratorsPage']['table']['lastname'] = '';

$_LANG['addonAA']['massSms']['mainContainer']['massSmsDataTable']['table']['id']     = 'ID';
$_LANG['addonAA']['massSms']['mainContainer']['massSmsDataTable']['table']['name']   = 'Name';
$_LANG['addonAA']['massSms']['mainContainer']['massSmsDataTable']['table']['number'] = 'Number';

$_LANG['addonAA']['massSms']['mainContainer']['massSmSView']['boxSection']['halfPageSection']['charsCounter-name']['Character count'] = 'Number of Characters';

$_LANG['validationError'] = 'Invalid Data';

$_LANG['addonAA']['templates']['editLangModal']['editLangForm']['domain']['ticketRelated'] = 'Domain Related';

$_LANG['noAvaliableTemplates'] = 'The template cannot be enabled, there is no content inside';


$_LANG['addonAA']['templates']['mainContainer']['labelscont']['moveToSettingPage']['button']['moveToSettingPage'] = 'Edit Template';
$_LANG['addonAA']['templates']['mainContainer']['templateSettingsDataTable']['editLangButton']['button']['editLangButton']     = 'Edit';
$_LANG['addonAA']['templates']['mainContainer']['templateSettingsDataTable']['deleteLangButton']['button']['deleteLangButton'] = 'Delete';


$_LANG['addonAA']['configuration']['simpleSettingsSubmoduleModal']['simpleSettingsSubmoduleForm']['user']['user']         = 'User';
$_LANG['addonAA']['configuration']['simpleSettingsSubmoduleModal']['simpleSettingsSubmoduleForm']['hashkey']['hashkey']   = 'Webservices Token';
$_LANG['addonAA']['configuration']['simpleSettingsSubmoduleModal']['simpleSettingsSubmoduleForm']['senderid']['senderid'] = 'Sender ID';
$_LANG['0']                                                                                                               = 'Disabled';
$_LANG['1']                                                                                                               = 'Enabled';

$_LANG['addonAA']['configuration']['simpleSettingsSubmoduleModal']['simpleSettingsSubmoduleForm']['sender_name']['sender_name'] = 'Sender Name';
$_LANG['addonAA']['configuration']['simpleSettingsSubmoduleModal']['simpleSettingsSubmoduleForm']['user_key']['user_key']       = 'API Key';
$_LANG['addonAA']['configuration']['simpleSettingsSubmoduleModal']['simpleSettingsSubmoduleForm']['apiid']['apiid']             = 'API ID';
$_LANG['addonAA']['configuration']['simpleSettingsSubmoduleModal']['simpleSettingsSubmoduleForm']['mo']['mo']                   = 'Mobile Originated';

$_LANG['addonAA']['configuration']['simpleSettingsSubmoduleModal']['simpleSettingsSubmoduleForm']['originator']['originator'] = 'Sender';
$_LANG['addonAA']['configuration']['simpleSettingsSubmoduleModal']['simpleSettingsSubmoduleForm']['access_key']['access_key'] = 'Access Key';
$_LANG['addonAA']['configuration']['simpleSettingsSubmoduleModal']['simpleSettingsSubmoduleForm']['datacoding']['datacoding'] = 'Encoding';
$_LANG['plain']                                                                                                               = 'Plain';
$_LANG['unicode']                                                                                                             = 'Unicode';

$_LANG['addonAA']['configuration']['simpleSettingsSubmoduleModal']['simpleSettingsSubmoduleForm']['api_key']['api_key']       = 'API Key';
$_LANG['addonAA']['configuration']['simpleSettingsSubmoduleModal']['simpleSettingsSubmoduleForm']['api_secret']['api_secret'] = 'API Secret';
$_LANG['addonAA']['configuration']['simpleSettingsSubmoduleModal']['simpleSettingsSubmoduleForm']['ip']['ip']                 = 'IP Address';

$_LANG['addonAA']['configuration']['editAdvancedSettingsModal']['editAdvancedSettingsSubmoduleForm']['senderid']['senderid'] = 'Sender';
$_LANG['addonAA']['pagesLabels']['tools']['clients']                                                                         = 'Clients';

$_LANG['addonAA']['tools']['mainContainer']['clientStatusPages']['table']['id']     = 'ID';
$_LANG['addonAA']['tools']['mainContainer']['clientStatusPages']['table']['name']   = 'Client';
$_LANG['addonAA']['tools']['mainContainer']['clientStatusPages']['table']['status'] = 'SMS Activation Status';

$_LANG['addonAA']['tools']['mainContainer']['clientStatusPages']['exportUserModalButton']['button']['exportUserModalButton'] = 'The client is not verified';
$_LANG['addonAA']['tools']['exportUserModal']['modal']['exportUserModal']                                                    = 'Verification';
$_LANG['addonAA']['tools']['exportUserModal']['baseAcceptButton']['title']                                                   = 'Confirm';
$_LANG['addonAA']['tools']['exportUserModal']['baseCancelButton']['title']                                                   = 'Cancel';

$_LANG['addonAA']['tools']['exportUserModal']['clientStatusAcceptForm']['confirmVeryficaqtionClient'] = 'Are you sure that you want to verify this client?';

//2FA
$_LANG['noActivedRules']         = 'SMS Center or any its submodule is not active. Please configure the module properly in order to continue.';
$_LANG['activationCodeValidFor'] = 'Activation Code Valid For';
$_LANG['smsTwoFactorAuth']       = 'Two-factor authentication adds an additional layer of security by adding a second step to your login. It takes something you know (e.g. password) and adds a second factor, typically something you have (such as your phone). Since both are required to log in, even if an attacker has your password they cannot access your account. So, two-factor authentication gives you additional security because your password alone no longer allows access to your account.';
$_LANG['smsTwoFactorAuthShort']       = 'SMS Center Two-Factor Authentication allows you to get your security code through the SMS message to log in.';
$_LANG['minutes']                = 'Minutes';
$_LANG['twoFactorAuth']          = 'SMS Center Two-Factor Authentication';
$_LANG['smsValidFor']            = 'SMS Code Valid For';
$_LANG['lengthOfCode']           = 'Token Characters Number';
$_LANG['charsAllowed']           = 'Allowed Token Characters';
$_LANG['recomendedChars']        = "Please remember that SMS gateways have their own characters restrictions. You can safely use uppercase and lowercase letters, and numbers.";
$_LANG['chars']                  = 'Characters';
//2FA CA
$_LANG['successfullyValid']      = 'Your account has been validated successfully';
$_LANG['validNumberOk']          = "Your account has been validated successfully";

//Tasks tooltips
$_LANG['addonAA']['smsQueue']['mainContainer']['smsQueuePage']['queueStartButton']['button']['queueStartButton']     = 'Run Task';
$_LANG['addonAA']['smsQueue']['mainContainer']['smsQueuePage']['queueRefreshButton']['button']['queueRefreshButton'] = 'Refresh Task';
$_LANG['addonAA']['smsQueue']['mainContainer']['smsQueuePage']['queueDeleteButton']['button']['queueDeleteButton']   = 'Delete Task';


$_LANG['addonAA']['massSms']['addTplModal']['addTplForm']['halfPageSection']['message']['message']       = 'Message';
$_LANG['addonAA']['massSms']['addTplModal']['addTplForm']['halfPageSection']['type']['type']             = 'Type';
$_LANG['addonAA']['massSms']['addTplModal']['addTplForm']['halfPageSection']['name']['name']             = 'Name';
$_LANG['addonAA']['massSms']['addTplModal']['addTplForm']['halfPageSection']['client']['ticketRelated']  = 'Client Related';
$_LANG['addonAA']['massSms']['addTplModal']['addTplForm']['halfPageSection']['other']['ticketRelated']   = 'Other';
$_LANG['addonAA']['massSms']['addTplModal']['addTplForm']['halfPageSection']['product']['ticketRelated'] = 'Product Related';
$_LANG['addonAA']['massSms']['addTplModal']['addTplForm']['halfPageSection']['domain']['ticketRelated']  = 'Domain Related';

$_LANG['addonAA']['configuration']['mainContainer']['administratorsPage']['advancedSettingSubmoduleModalButton']['button']['advancedSettingSubmoduleModalButton'] = 'Edit';

$_LANG['addonAA']['massSms']['mainContainer']['massSmsPage']['general']['editTplButton']['button']['editTplButton']     = 'Edit';
$_LANG['addonAA']['massSms']['mainContainer']['massSmsPage']['general']['deleteTplButton']['button']['deleteTplButton'] = 'Delete';
$_LANG['addonAA']['massSms']['deleteLangModal']['modal']                                                                = 'Delete';

$_LANG['addonAA']['massSms']['deleteLangModal']['deleteLangForm']['confirmLabelRemoval'] = 'Are you sure that you want to remove this template?';
$_LANG['addonAA']['massSms']['deleteLangModal']['baseAcceptButton']['title']             = 'Confirm';
$_LANG['addonAA']['massSms']['deleteLangModal']['baseCancelButton']['title']             = 'Cancel';

$_LANG['addonAA']['massSms']['editTplModal']['modal']['editLangModal']    = 'Edit Template';
$_LANG['addonAA']['massSms']['editTplModal']['baseAcceptButton']['title'] = 'Confirm';
$_LANG['addonAA']['massSms']['editTplModal']['baseCancelButton']['title'] = 'Cancel';

$_LANG['addonAA']['massSms']['editTplModal']['modal']['editTplModal']                               = 'Edit';
$_LANG['addonAA']['massSms']['editTplModal']['addTplForm']['halfPageSection']['name']['name']       = 'Name';
$_LANG['addonAA']['massSms']['editTplModal']['addTplForm']['halfPageSection']['type']['type']       = 'Type';
$_LANG['addonAA']['massSms']['editTplModal']['addTplForm']['halfPageSection']['message']['message'] = 'Message';

$_LANG['addonAA']['massSms']['editTplModal']['addTplForm']['halfPageSection']['client']['ticketRelated'] = 'Client';
$_LANG['addonAA']['massSms']['editTplModal']['addTplForm']['halfPageSection']['other']['ticketRelated']  = 'Other';



$_LANG['addonAA']['massSms']['mainContainer']['massSmsPage']['general']['product']['editTplButton']['button']['editTplButton']                        = 'Edit';
$_LANG['addonAA']['massSms']['mainContainer']['massSmsPage']['general']['product']['deleteTplButton']['button']['deleteTplButton']                    = 'Delete';
$_LANG['addonAA']['massSms']['mainContainer']['massSmsPage']['general']['product']['domain']['editTplButton']['button']['editTplButton']              = 'Edit';
$_LANG['addonAA']['massSms']['mainContainer']['massSmsPage']['general']['product']['domain']['deleteTplButton']['button']['deleteTplButton']          = 'Delete';
$_LANG['addonAA']['massSms']['mainContainer']['massSmsPage']['general']['product']['domain']['addon']['editTplButton']['button']['editTplButton']     = 'Edit';
$_LANG['addonAA']['massSms']['mainContainer']['massSmsPage']['general']['product']['domain']['addon']['deleteTplButton']['button']['deleteTplButton'] = 'Delete';

$_LANG['invalidNumberOrClient'] = 'The client or the number is invalid';

$_LANG['addonAA']['tools']['mainContainer']['sendSmsPage']['boxSection']['halfPageSection']['message']['message'] = 'Message';
$_LANG['addonAA']['tools']['mainContainer']['sendSmsPage']['boxSection']['halfPageSection']['clients']['clients'] = 'Client';
$_LANG['addonAA']['tools']['mainContainer']['sendSmsPage']['boxSection']['halfPageSection']['number']['number']   = 'Number';
$_LANG['addonAA']['tools']['mainContainer']['sendSmsPage']['boxSection']['halfPageSection']['rules']['rules']     = 'SMS Gateway';
$_LANG['sendSmsSuccessfully']                                                                                     = 'The message has been sent successfully';
$_LANG['addedtoQueue']                                                                                            = 'The message has been added to the queue successfully';

$_LANG['addonAA']['configuration']['mainContainer']['administratorsPage']['AtomicSMS'] = 'Atomic SMS';

$_LANG['addonAA']['configuration']['addAdvancedSettingsModal']['advancedSettingsSubmoduleForm']['public_key']['public_key']   = 'Public Key';
$_LANG['addonAA']['configuration']['addAdvancedSettingsModal']['advancedSettingsSubmoduleForm']['private_key']['private_key'] = 'Private Key';
$_LANG['addonAA']['configuration']['addAdvancedSettingsModal']['advancedSettingsSubmoduleForm']['use_ssl']['use_ssl']         = 'Use SSL';
$_LANG['addonAA']['configuration']['addAdvancedSettingsModal']['advancedSettingsSubmoduleForm']['debug_mode']['debug_mode']   = 'Debug Mode';
$_LANG['addonAA']['configuration']['addAdvancedSettingsModal']['advancedSettingsSubmoduleForm']['settingName']['settingName'] = 'Settings Name';

$_LANG['addonAA']['configuration']['mainContainer']['rulesPage']['advancedSettingSubmoduleModalButton']['button']['advancedSettingSubmoduleModalButton'] = 'Edit';

$_LANG['addonAA']['configuration']['mainContainer']['labelscont']['moveToSettingPage']['button']['moveToSettingPage']                     = 'Advanced Settings';
$_LANG['addonAA']['configuration']['mainContainer']['administratorsPage']['deleteLoggerModalButton']['button']['deleteLoggerModalButton'] = 'Delete';
$_LANG['addonAA']['configuration']['mainContainer']['administratorsPage']['BulkSms']                                                      = 'BulkSMS';
$_LANG['addonAA']['configuration']['mainContainer']['administratorsPage']['BoxisSms']                                                     = 'BoxisSMS';
$_LANG['addonAA']['configuration']['mainContainer']['administratorsPage']['Clickatell']                                                   = 'Clickatell';
$_LANG['addonAA']['configuration']['mainContainer']['administratorsPage']['ClickatellNew']                                                = 'Clickatell(New API)';
$_LANG['addonAA']['configuration']['mainContainer']['administratorsPage']['FastSms']                                                      = 'Fast SMS';
$_LANG['addonAA']['configuration']['mainContainer']['administratorsPage']['Infobip']                                                      = 'Infobip';
$_LANG['addonAA']['configuration']['mainContainer']['administratorsPage']['MessageBird']                                                  = 'MessageBird';
$_LANG['addonAA']['configuration']['mainContainer']['administratorsPage']['Nexmo']                                                        = 'Nexmo';
$_LANG['addonAA']['configuration']['mainContainer']['administratorsPage']['SmsEagle']                                                     = 'SMSEagle';
$_LANG['addonAA']['configuration']['mainContainer']['administratorsPage']['SmsGlobal']                                                    = 'SMSGlobal';
$_LANG['addonAA']['configuration']['mainContainer']['administratorsPage']['TestModule']                                                   = 'Test Module';
$_LANG['addonAA']['configuration']['mainContainer']['administratorsPage']['Twilio']                                                       = 'Twilio';
$_LANG['addonAA']['configuration']['mainContainer']['administratorsPage']['VoodooSms']                                                    = 'Voodoo SMS';

$_LANG['clientArea']['2FA']['loginCode']    = 'Your login code is :token:';
$_LANG['clientArea']['2FA']['activateCode'] = 'Your activation code is :token:';

$_LANG['addonAA']['configuration']['editAdvancedSettingsModal']['advancedSettingsSubmoduleForm']['addon']['addon']       = 'Contains Addon';
$_LANG['addonAA']['configuration']['editAdvancedSettingsModal']['advancedSettingsSubmoduleForm']['product']['product']   = 'Contains Product';
$_LANG['addonAA']['configuration']['editAdvancedSettingsModal']['advancedSettingsSubmoduleForm']['domain']['domain']     = 'Contains Domain';
$_LANG['addonAA']['configuration']['editAdvancedSettingsModal']['advancedSettingsSubmoduleForm']['priority']['priority'] = 'Has Priority';

$_LANG['addonAA']['templates']['mainContainer']['templatesPage']['support']['moveToSettingPage']['button']['moveToSettingPage']      = 'Move To Setting Page';
$_LANG['addonAA']['templates']['mainContainer']['templatesPage']['invoice']['moveToSettingPage']['button']['moveToSettingPage']      = 'Move To Setting Page';
$_LANG['addonAA']['templates']['mainContainer']['templatesPage']['product']['moveToSettingPage']['button']['moveToSettingPage']      = 'Move To Setting Page';
$_LANG['addonAA']['templates']['mainContainer']['templatesPage']['general']['moveToSettingPage']['button']['moveToSettingPage']      = 'Move To Setting Page';
$_LANG['addonAA']['templates']['mainContainer']['templatesPage']['notification']['moveToSettingPage']['button']['moveToSettingPage'] = 'Move To Setting Page';
$_LANG['addonAA']['templates']['mainContainer']['templatesPage']['domain']['moveToSettingPage']['button']['moveToSettingPage']       = 'Move To Setting Page';
$_LANG['addonAA']['templates']['mainContainer']['templatesPage']['affiliate']['moveToSettingPage']['button']['moveToSettingPage']    = 'Move To Setting Page';
$_LANG['addonAA']['templates']['mainContainer']['templatesPage']['admin']['moveToSettingPage']['button']['moveToSettingPage']        = 'Move To Setting Page';
$_LANG['addonAA']['templates']['mainContainer']['templatesPage']['user']['moveToSettingPage']['button']['moveToSettingPage']        = 'Move To Setting Page';

$_LANG['addonAA']['massSms']['mainContainer']['filterPage']['boxDoubleSection']['noWidgetSection']['enabledClientFilter']['description'] = 'Select the type of subject that your message is about.';
$_LANG['addonAA']['massSms']['mainContainer']['filterPage']['boxDoubleSection']['noWidgetSection']['enabledClientStatus']['description'] = 'Select the particular status(es) of clients that your message will be sent to.';
$_LANG['addonAA']['massSms']['mainContainer']['filterPage']['boxDoubleSection']['noWidgetSection']['marketingSms']['description']        = 'If enabled, the message will be sent only to the clients who have opted in for advertisements and other marketing content.';
$_LANG['addonAA']['massSms']['mainContainer']['filterPage']['boxDoubleSection']['noWidgetSection']['languages']['description']           = 'Select languages from the dropdown menu.';

$_LANG['addonAA']['configuration']['mainContainer']['submodulesPage']['advancedSettingsSubmoduleButton']['button']['advancedSettingsSubmoduleButton'] = 'Advanced Settings';
$_LANG['addonAA']['massSms']['nocarddetails']                                                                                                         = 'No Card Details';

$_LANG['disable'] = 'Disable';
$_LANG['enable']  = 'Enable';

$_LANG['addonAA']['tools']['mainContainer']['sendSmsPage']['boxSection']['halfPageSection']['message']['description'] = 'Provide the content of your message';
$_LANG['addonAA']['tools']['mainContainer']['sendSmsPage']['boxSection']['halfPageSection']['clients']['description'] = 'Select the client to whom the message will be sent';
$_LANG['addonAA']['tools']['mainContainer']['sendSmsPage']['boxSection']['halfPageSection']['number']['description']  = 'Set the number to which the message will be sent';
$_LANG['addonAA']['tools']['mainContainer']['sendSmsPage']['boxSection']['halfPageSection']['rules']['description']   = 'Select the SMS gateway that shall be used to send the message';

$_LANG['addonAA']['logs']['mainContainer']['loggercont']['deleteLoggerModalButton']['button']['deleteLoggerModalButton'] = 'Delete';

$_LANG['addonAA']['logs']['deleteLoggerModal']['modal']['deleteLoggerModal']              = 'Delete';
$_LANG['addonAA']['logs']['deleteLoggerModal']['deleteLoggerForm']['confirmLabelRemoval'] = 'Are you sure that you want to remove this log?';
$_LANG['addonAA']['logs']['deleteLoggerModal']['baseAcceptButton']['title']               = 'Confirm';
$_LANG['addonAA']['logs']['deleteLoggerModal']['baseCancelButton']['title']               = 'Cancel';

$_LANG['tokenResendSuccessfully'] = 'The token has been sent successfully';

$_LANG['addonAA']['configuration']['mainContainer']['generalPage']['leftSide']['generalSetting'] = 'General Settings';
$_LANG['addonAA']['massSms']['mainContainer']['filterPage']['boxDoubleSection']['noWidgetSection']['generalSettings']['description']     = 'Select either general settings or none';
$_LANG['addonAA']['massSms']['mainContainer']['filterPage']['boxSection']['halfPageSection']['name']['name']                             = 'Name';

$_LANG['addonAA']['pagesLabels']['tools']['api'] = 'API';

$_LANG['addonAA']['massSms']['mainContainer']['massSmSView']['saveFiltersButton']['button']['save'] = 'Save Filters';
$_LANG['addonAA']['pagesLabels']['massSms']['settings']                                             = 'Mass SMS Configuration';

$_LANG['addonAA']['massSms']['settingDeleteModal']['modal']['settingDeleteModal'] = 'Delete';
$_LANG['addonAA']['massSms']['settingDeleteModal']['baseAcceptButton']['title']   = 'Confirm';
$_LANG['addonAA']['massSms']['settingDeleteModal']['baseCancelButton']['title']   = 'Cancel';

$_LANG['addonAA']['massSms']['settingDeleteModal']['settingDeleteForm']['confirmQueueDeleteForm']                     = 'Are you sure that you want to delete these settings?';
$_LANG['addonAA']['massSms']['mainContainer']['labelscont']['moveToSettingPage']['button']['moveToSettingPage']       = 'Edit';
$_LANG['addonAA']['massSms']['mainContainer']['smsQueuePage']['settingDeleteButton']['button']['settingDeleteButton'] = 'Delete';
$_LANG['addonAA']['massSms']['mainContainer']['smsQueuePage']['massDeleteSettings']['modal']['massDeleteSettings']    = 'Delete';
$_LANG['addonAA']['massSms']['button']['massDeleteSettings']                                                          = 'Delete';

$_LANG['addonAA']['massSms']['massDeleteModal']['modal']['massDeleteModal']                   = 'Delete';
$_LANG['addonAA']['massSms']['massDeleteModal']['baseAcceptButton']['title']                  = 'Confirm';
$_LANG['addonAA']['massSms']['massDeleteModal']['baseCancelButton']['title']                  = 'Cancel';
$_LANG['addonAA']['massSms']['massDeleteModal']['settingDeleteForm']['confirmDeleteSettings'] = 'Are you sure that you want to delete the selected settings?';

$_LANG['addonAA']['massSms']['mainContainer']['smsQueuePage']['table']['id']                                      = 'Name';
$_LANG['addonAA']['massSms']['mainContainer']['smsQueuePage']['table']['name']                                    = 'Template Name';
$_LANG['addonAA']['massSms']['mainContainer']['smsQueuePage']['table']['settingName']                             = 'Name';
$_LANG['addonAA']['massSms']['mainContainer']['smsQueuePage']['table']['message']                                 = 'Message';
$_LANG['addonAA']['massSms']['mainContainer']['smsQueuePage']['massTemplatesEdit']['button']['massTemplatesEdit'] = 'Edit';

$_LANG['addonAA']['configuration']['mainContainer']['generalPage']['leftSide']['noWidgetSection']['enableSmsCenter']['description']                  = 'If enabled, customers will be able to select the SMS notifications that they would like to receive.';
$_LANG['addonAA']['configuration']['mainContainer']['generalPage']['leftSide']['noWidgetSection']['disableLogs']['description']                      = 'If enabled, Logs will not be visible.';
$_LANG['addonAA']['configuration']['mainContainer']['generalPage']['leftSide']['noWidgetSection']['enableAdvancedSetting']['description']            = 'If enabled, you will be able to set multiple different configurations per gateway.';
$_LANG['addonAA']['configuration']['mainContainer']['generalPage']['rightSide']['noWidgetSection']['enableClientSmsCenter']['description']           = 'If enabled, new clients will be required to confirm the registration using the token delivered via SMS.';
$_LANG['addonAA']['configuration']['mainContainer']['generalPage']['rightSide']['noWidgetSection']['sendTokenMinutes']['description']                = 'Define the minimum interval between attempts at resending tokens.';
$_LANG['addonAA']['configuration']['mainContainer']['generalPage']['rightSide']['noWidgetSection']['resendTokenAttempts']['description']             = 'Define a maximum number of attempts per hour that a client will be allowed to make to resend the activation token.';
$_LANG['addonAA']['configuration']['mainContainer']['generalPage']['rightSide']['noWidgetSection']['notBlockWHMCSPages']['description']              = 'Enable to allow unverified clients to retain access to the client area pages. If disabled, the access will remain blocked until the client is successfully verified.';
$_LANG['addonAA']['configuration']['mainContainer']['generalPage']['leftSide']['noWidgetSection']['enableOrderSmsAvtion']['description']             = 'If enabled, new orders will require the SMS verification.';
$_LANG['addonAA']['configuration']['mainContainer']['generalPage']['leftSide']['noWidgetSection']['orderSendTokenMinutes']['description']            = 'Define the minimum interval between attempts at resending tokens.';
$_LANG['addonAA']['configuration']['mainContainer']['generalPage']['leftSide']['noWidgetSection']['smsActionForFreeOrders']['description']           = 'If enabled, free orders will require the SMS verification.';
$_LANG['addonAA']['configuration']['mainContainer']['generalPage']['leftSide']['noWidgetSection']['resendOrderTokenAttempts']['description']         = 'Define a maximum number of attempts per hour that a client will be allowed to make to resend the activation token.';
$_LANG['addonAA']['configuration']['mainContainer']['generalPage']['rightSide']['noWidgetSection']['enableSmsQueuing']['description']                = 'If enable, new messages will be automatically added to the queue.';
$_LANG['addonAA']['configuration']['mainContainer']['generalPage']['rightSide']['noWidgetSection']['messageSendPerCronRun']['description']           = 'Specify how many messages should be sent in a single cron run.';
$_LANG['addonAA']['configuration']['mainContainer']['generalPage']['boxDoubleSection']['noWidgetSection']['smsQueTrySendEachSms']['description']     = 'Define the minimum interval between attempts at resending tokens.';
$_LANG['addonAA']['configuration']['mainContainer']['generalPage']['leftSide']['noWidgetSection']['enableSmsCustomToken']['description']             = 'If enabled, you will be able to use custom settings to generate tokens.';
$_LANG['addonAA']['configuration']['mainContainer']['generalPage']['leftSide']['noWidgetSection']['lengthOfCode']['description']                     = 'Enter the required number of characters in the token.';
$_LANG['addonAA']['configuration']['mainContainer']['generalPage']['leftSide']['noWidgetSection']['charsAllowedInCode']['description']               = 'Provide the particular characters to be used to generate the token.';
$_LANG['addonAA']['configuration']['mainContainer']['generalPage']['rightSide']['noWidgetSection']['clientNumbetCustomField']['description']         = 'Select a custom field from the dropdown menu.';
$_LANG['addonAA']['massSms']['mainContainer']['smsQueuePage']['redirectSettingsButton']['button']['addIconModalButton']                              = 'Add Settings';
$_LANG['addonAA']['massSms']['mainContainer']['filterPage']['boxDoubleSection']['noWidgetSection']['settingName']['settingName']                     = 'Name';
$_LANG['addonAA']['configuration']['mainContainer']['generalPage']['rightSide']['noWidgetSection']['countries']['description']                       = 'Choose countries that will be checked whether the SMS should be sent with a token.';
$_LANG['addonAA']['massSms']['mainContainer']['filterPage']['boxDoubleSection']['noWidgetSection']['settingName']['setName']                         = 'Provide the name of the settings.';
$_LANG['addonAA']['configuration']['editAdvancedSettingsModal']['advancedSettingsSubmoduleForm']['priorityCreated']['priorityCreated']               = 'Has Priority';
$_LANG['addonAA']['configuration']['editAdvancedSettingsModal']['advancedSettingsSubmoduleForm']['priorityFlagged']['priorityFlagged']               = 'Has Priority';
$_LANG['addonAA']['tools']['button']['massEnableButton']                                                                                             = 'Verified Status';
$_LANG['addonAA']['tools']['button']['massDisableButton']                                                                                            = 'Deactivate';
$_LANG['addonAA']['massSms']['mainContainer']['massSmSView']['sendMassSmsButton']['button']['submit']                                                = 'Send';
$_LANG['addonAA']['massSms']['mainContainer']['labelscont']['moveToSettingPage']['button']['sendMassSmsSettings']                                    = 'Send SMS';
$_LANG['addonAA']['massSms']['mainContainer']['smsQueuePage']['sendBySettings']['button']['sendBySettings']                                          = 'Send By Settings';
$_LANG['massSmsSettingsSaved']                                                                                                                       = 'The settings have been saved successfully';
$_LANG['addonAA']['configuration']['editAdvancedSettingsModal']['editAdvancedSettingsSubmoduleForm']['originator']['originator']                     = 'Sender';
$_LANG['addonAA']['configuration']['editAdvancedSettingsModal']['editAdvancedSettingsSubmoduleForm']['access_key']['access_key']                     = 'Access Key';
$_LANG['addonAA']['configuration']['editAdvancedSettingsModal']['editAdvancedSettingsSubmoduleForm']['datacoding']['datacoding']                     = 'Encoding';
$_LANG['addonAA']['configuration']['editAdvancedSettingsModal']['editAdvancedSettingsSubmoduleForm']['settingName']['settingName']                   = 'Setting Name';
$_LANG['addonAA']['configuration']['addAdvancedSettingsModal']['advancedSettingsSubmoduleForm']['originator']['originator']                          = 'Sender';
$_LANG['addonAA']['configuration']['addAdvancedSettingsModal']['advancedSettingsSubmoduleForm']['access_key']['access_key']                          = 'Access Key';
$_LANG['addonAA']['configuration']['addAdvancedSettingsModal']['advancedSettingsSubmoduleForm']['datacoding']['datacoding']                          = 'Encoding';
$_LANG['addonAA']['configuration']['editAdvancedSettingsModal']['editAdvancedSettingsSubmoduleForm']['public_key']['public_key']                     = 'Public Key';
$_LANG['addonAA']['configuration']['editAdvancedSettingsModal']['editAdvancedSettingsSubmoduleForm']['private_key']['private_key']                   = 'Private Key';
$_LANG['addonAA']['configuration']['editAdvancedSettingsModal']['editAdvancedSettingsSubmoduleForm']['sender']['sender']                             = 'Sender';
$_LANG['addonAA']['configuration']['editAdvancedSettingsModal']['editAdvancedSettingsSubmoduleForm']['use_ssl']['use_ssl']                           = 'Use SSL';
$_LANG['addonAA']['configuration']['editAdvancedSettingsModal']['editAdvancedSettingsSubmoduleForm']['debug_mode']['debug_mode']                     = 'Debug Mode';
$_LANG['addonAA']['configuration']['editAdvancedSettingsModal']['editAdvancedSettingsSubmoduleForm']['unicode']['unicode']                           = 'Unicode';
$_LANG['addonAA']['configuration']['addAdvancedSettingsModal']['advancedSettingsSubmoduleForm']['hashkey']['hashkey']                                = 'Hash Key';
$_LANG['addonAA']['configuration']['addAdvancedSettingsModal']['advancedSettingsSubmoduleForm']['senderid']['senderid']                              = 'Sender';
$_LANG['addonAA']['configuration']['editAdvancedSettingsModal']['editAdvancedSettingsSubmoduleForm']['username']['username']                         = 'Username';
$_LANG['addonAA']['configuration']['editAdvancedSettingsModal']['editAdvancedSettingsSubmoduleForm']['user_key']['user_key']                         = 'User Key';
$_LANG['addonAA']['configuration']['editAdvancedSettingsModal']['editAdvancedSettingsSubmoduleForm']['sender_name']['sender_name']                   = 'Sender Name';
$_LANG['addonAA']['configuration']['addAdvancedSettingsModal']['advancedSettingsSubmoduleForm']['user_key']['user_key']                              = 'User Key';
$_LANG['addonAA']['configuration']['addAdvancedSettingsModal']['advancedSettingsSubmoduleForm']['sender_name']['sender_name']                        = 'Sender Name';
$_LANG['addonAA']['configuration']['addAdvancedSettingsModal']['advancedSettingsSubmoduleForm']['apiid']['apiid']                                    = 'API ID';
$_LANG['addonAA']['configuration']['addAdvancedSettingsModal']['advancedSettingsSubmoduleForm']['mo']['mo']                                          = 'Mobile Originated';
$_LANG['addonAA']['configuration']['editAdvancedSettingsModal']['editAdvancedSettingsSubmoduleForm']['apiid']['apiid']                               = 'API ID';
$_LANG['addonAA']['configuration']['editAdvancedSettingsModal']['editAdvancedSettingsSubmoduleForm']['login']['login']                               = 'Login';
$_LANG['addonAA']['configuration']['editAdvancedSettingsModal']['editAdvancedSettingsSubmoduleForm']['password']['password']                         = 'Password';
$_LANG['addonAA']['configuration']['editAdvancedSettingsModal']['editAdvancedSettingsSubmoduleForm']['mo']['mo']                                     = 'Mobile Originated';
$_LANG['addonAA']['configuration']['editAdvancedSettingsModal']['editAdvancedSettingsSubmoduleForm']['apiToken']['apiToken']                         = 'API Token';
$_LANG['addonAA']['configuration']['editAdvancedSettingsModal']['editAdvancedSettingsSubmoduleForm']['fromNumber']['fromNumber']                     = 'From Number';
$_LANG['addonAA']['configuration']['editAdvancedSettingsModal']['editAdvancedSettingsSubmoduleForm']['from']['from']                                 = 'From';
$_LANG['addonAA']['configuration']['addAdvancedSettingsModal']['advancedSettingsSubmoduleForm']['api_key']['api_key']                                = 'API Key';
$_LANG['addonAA']['configuration']['addAdvancedSettingsModal']['advancedSettingsSubmoduleForm']['api_secret']['api_secret']                          = 'API Secret';
$_LANG['addonAA']['configuration']['editAdvancedSettingsModal']['editAdvancedSettingsSubmoduleForm']['api_key']['api_key']                           = 'API Key';
$_LANG['addonAA']['configuration']['editAdvancedSettingsModal']['editAdvancedSettingsSubmoduleForm']['api_secret']['api_secret']                     = 'API Secret';
$_LANG['addonAA']['configuration']['addAdvancedSettingsModal']['advancedSettingsSubmoduleForm']['ip']['ip']                                          = 'IP';
$_LANG['addonAA']['configuration']['editAdvancedSettingsModal']['editAdvancedSettingsSubmoduleForm']['url']['url']                                   = 'URL';
$_LANG['addonAA']['configuration']['editAdvancedSettingsModal']['editAdvancedSettingsSubmoduleForm']['ip']['ip']                                     = 'IP';
$_LANG['addonAA']['configuration']['addAdvancedSettingsModal']['advancedSettingsSubmoduleForm']['connection']['connection']                          = 'Test Connection';
$_LANG['success']                                                                                                                                    = 'Successful';
$_LANG['message']                                                                                                                                    = 'One of Error Message';
$_LANG['error']                                                                                                                                      = 'Error';
$_LANG['addonAA']['configuration']['addAdvancedSettingsModal']['advancedSettingsSubmoduleForm']['send']['send']                                      = 'Send Test Message';
$_LANG['addonAA']['configuration']['editAdvancedSettingsModal']['editAdvancedSettingsSubmoduleForm']['connection']['connection']                     = 'Test Connection';
$_LANG['addonAA']['configuration']['editAdvancedSettingsModal']['editAdvancedSettingsSubmoduleForm']['send']['send']                                 = 'Send Test Message';
$_LANG['addonAA']['configuration']['editAdvancedSettingsModal']['editAdvancedSettingsSubmoduleForm']['accountSid']['accountSid']                     = 'Account SID';
$_LANG['addonAA']['configuration']['editAdvancedSettingsModal']['editAdvancedSettingsSubmoduleForm']['authToken']['authToken']                       = 'AuthToken';
$_LANG['addonAA']['configuration']['addAdvancedSettingsModal']['advancedSettingsSubmoduleForm']['sender_id']['sender_id']                            = 'Sender ID';
$_LANG['addonAA']['configuration']['editAdvancedSettingsModal']['editAdvancedSettingsSubmoduleForm']['sender_id']['sender_id']                       = 'Sender ID';

$_LANG['addonAA']['configuration']['editAdvancedSettingsModal']['advancedSettingsSubmoduleForm']['escalationRuleNotification']['EscalationRuleNotification']                     = 'Escalation Rule Notification';
$_LANG['addonAA']['configuration']['editAdvancedSettingsModal']['advancedSettingsSubmoduleForm']['manualUpgradeRequired']['ManualUpgradeRequired']                               = 'Manual Upgrade Required';
$_LANG['addonAA']['configuration']['editAdvancedSettingsModal']['advancedSettingsSubmoduleForm']['supportTicketChangeNotification']['SupportTicketChangeNotification']           = 'Support Ticket Change Notification';
$_LANG['addonAA']['configuration']['editAdvancedSettingsModal']['advancedSettingsSubmoduleForm']['adminPasswordResetValidation']['AdminPasswordResetValidation']                 = 'Admin Password Reset Validation';
$_LANG['addonAA']['configuration']['editAdvancedSettingsModal']['advancedSettingsSubmoduleForm']['adminPasswordResetConfirmation']['AdminPasswordResetConfirmation']             = 'Admin Password Reset Confirmation';
$_LANG['addonAA']['configuration']['editAdvancedSettingsModal']['advancedSettingsSubmoduleForm']['paymentReversedNotification']['PaymentReversedNotification']                   = 'Payment Reversed Notification';
$_LANG['addonAA']['configuration']['editAdvancedSettingsModal']['advancedSettingsSubmoduleForm']['quickBooksDesktopFailureNotification']['QuickBooksDesktopFailureNotification'] = 'QuickBooks Desktop Failure Notification';
$_LANG['addonAA']['configuration']['editAdvancedSettingsModal']['advancedSettingsSubmoduleForm']['newTaskNotification']['NewTaskNotification']                                   = 'New Task Notification';
$_LANG['addonAA']['configuration']['editAdvancedSettingsModal']['advancedSettingsSubmoduleForm']['newTimeEntryNotification']['NewTimeEntryNotification']                         = 'New Time Entry Notification';
$_LANG['addonAA']['configuration']['editAdvancedSettingsModal']['advancedSettingsSubmoduleForm']['quoteAcceptedNotification']['QuoteAcceptedNotification']                       = 'Quote Accepted Notification';

$_LANG['addonAA']['templates']['massEnableModal']['modal']['massEnableModal']                                                                          = 'Enable';
$_LANG['addonAA']['templates']['massEnableModal']['massEnableForm']['confirmEnableTemplates']                                                          = 'Are you sure that you want to enable the selected templates?';
$_LANG['addonAA']['templates']['massEnableModal']['baseAcceptButton']['title']                                                                         = 'Confirm';
$_LANG['addonAA']['templates']['massEnableModal']['baseCancelButton']['title']                                                                         = 'Cancel';
$_LANG['addonAA']['templates']['massDisableModal']['modal']['massDisableModal']                                                                        = 'Disable';
$_LANG['addonAA']['templates']['massDisableModal']['massDisableForm']['confirmDisableTemplates']                                                       = 'Are you sure that you want to disable the selected templates?';
$_LANG['addonAA']['templates']['massDisableModal']['baseAcceptButton']['title']                                                                        = 'Confirm';
$_LANG['addonAA']['templates']['massDisableModal']['baseCancelButton']['title']                                                                        = 'Cancel';
$_LANG['massDisabledTemplateSuccessfully']                                                                                                             = 'The status of the selected templates has been changed successfully';
$_LANG['massDisabledTemplateFailed']                                                                                                                   = 'The selected templates cannot be enabled, there is no content inside';
$_LANG['statusClientNotifySaved']                                                                                                                      = 'The status of customer login notifications has been changed successfully';
$_LANG['addonAA']['configuration']['editAdvancedSettingsModal']['advancedSettingsSubmoduleForm']['clientLoginNotification']['ClientLoginNotification'] = 'Client Login Notification';
$_LANG['noSettingsConfigured']                                                                                                                         = 'You need to configure the administrator settings first';
$_LANG['savedAdminNotification']                                                                                                                       = 'The administrator notifications have been changed successfully';
//$_LANG['addonCA']['clientVerification']['mainContainer']['clientVerification']['setSmsNumber']                                                         = 'Your SMS number is not defined. Please <a href="clientarea.php?action=details">edit your account details</a> first.';
//$_LANG['addonCA']['clientVerification']['mainContainer']['orderVerification']['setSmsNumber']                                                          = 'Your SMS number is not defined. Please <a href="clientarea.php?action=details">edit your account details</a> first.';
$_LANG['addonAA']['tools']['button']['massLogInfoEnableButton']                                                                                        = 'Login Notification';
$_LANG['addonAA']['tools']['button']['massLogInfoDisableButton']                                                                                       = 'Disable Notification';
$_LANG['addonAA']['tools']['massLogInfoEnableModal']['modal']['massLogInfoEnableModal']                                                                = 'Enable Notification';
$_LANG['addonAA']['tools']['massLogInfoEnableModal']['massLogInfoEnableForm']['confirmEnableNotification']                                             = 'Are you sure that you want to change the notification status for the selected clients?';
$_LANG['addonAA']['tools']['massLogInfoEnableModal']['baseAcceptButton']['title']                                                                      = 'Confirm';
$_LANG['addonAA']['tools']['massLogInfoEnableModal']['baseCancelButton']['title']                                                                      = 'Cancel';
$_LANG['addonAA']['tools']['massLogInfoDisableModal']['modal']['massLogInfoDisableModal']                                                              = 'Disable Notification';
$_LANG['addonAA']['tools']['massLogInfoDisableModal']['massLogInfoDisableForm']['confirmDisableNotification']                                          = 'Are you sure that you want to disable the login notification for the selected clients?';
$_LANG['addonAA']['tools']['massLogInfoDisableModal']['baseAcceptButton']['title']                                                                     = 'Confirm';
$_LANG['addonAA']['tools']['massLogInfoDisableModal']['baseCancelButton']['title']                                                                     = 'Cancel';
$_LANG['addonAA']['tools']['massLogInfoEnableModal']['massLogInfoEnableForm']['enableStatus']['enableStatus']                                          = 'Login Notification';
$_LANG['addonAA']['tools']['massLogInfoEnableModal']['massLogInfoEnableForm']['enableStatus']['description']                                           = 'If enabled, administrators will receive notifications when selected customers log in to your client area';

$_LANG['addonAA']['tools']['button']['massAcceptSmsButton']                                                                                            = 'Toggle Accept SMS';
$_LANG['addonAA']['tools']['massAcceptSMSModal']['modal']['massAcceptSMSModal']                                                                        = 'Accept SMS';
$_LANG['addonAA']['tools']['massAcceptSMSModal']['massAcceptSMSForm']['confirmAcceptSMS']                                                              = 'Are you sure you want to change accept SMS status ?';
$_LANG['addonAA']['tools']['massAcceptSMSModal']['massAcceptSMSForm']['enableStatus']['enableStatus']                                                  = 'Accept SMS Status:';
$_LANG['addonAA']['tools']['massAcceptSMSModal']['massAcceptSMSForm']['enableStatus']['description']                                                   = 'If enabled, the status for Accept SMS will be changed';
$_LANG['addonAA']['tools']['massAcceptSMSModal']['baseAcceptButton']['title']                                                                          = 'Confirm';
$_LANG['addonAA']['tools']['massAcceptSMSModal']['baseCancelButton']['title']                                                                          = 'Cancel';

$_LANG['addonAA']['tools']['button']['massMarketingSmsButton']                                                                                         = 'Toggle Marketing SMS';
$_LANG['addonAA']['tools']['massMarketingSMSModal']['modal']['massMarketingSMSModal']                                                                  = 'Marketing SMS';
$_LANG['addonAA']['tools']['massMarketingSMSModal']['massMarketingSMSForm']['confirmMarketingSMS']                                                     = 'Are you sure you want to change marketing SMS status ?';
$_LANG['addonAA']['tools']['massMarketingSMSModal']['massMarketingSMSForm']['enableStatus']['enableStatus']                                            = 'Marketing SMS Status:';
$_LANG['addonAA']['tools']['massMarketingSMSModal']['massMarketingSMSForm']['enableStatus']['description']                                             = 'If enabled, the status for Marketing SMS will be changed';
$_LANG['addonAA']['tools']['massMarketingSMSModal']['baseAcceptButton']['title']                                                                       = 'Confirm';
$_LANG['addonAA']['tools']['massMarketingSMSModal']['baseCancelButton']['title']                                                                       = 'Cancel';

$_LANG['addonAA']['configuration']['simpleSettingsSubmoduleModal']['simpleSettingsSubmoduleForm']['public_key']['public_key']                                        = 'Public Key';
$_LANG['addonAA']['configuration']['simpleSettingsSubmoduleModal']['simpleSettingsSubmoduleForm']['private_key']['private_key']                                      = 'Private Key';
$_LANG['addonAA']['configuration']['simpleSettingsSubmoduleModal']['simpleSettingsSubmoduleForm']['use_ssl']['use_ssl']                                              = 'Use SSL';
$_LANG['addonAA']['configuration']['simpleSettingsSubmoduleModal']['simpleSettingsSubmoduleForm']['debug_mode']['debug_mode']                                        = 'Debug Mode';
$_LANG['taskDeletedSuccessfully']                                                                                                                                    = 'The task has been deleted successfully';
$_LANG['massTaskDeletedSuccessfully']                                                                                                                                = 'The selected tasks have been deleted successfully';
$_LANG['addonAA']['configuration']['mainContainer']['generalPage']['boxDoubleSection']['generalSetting']                                                             = 'General Settings';
$_LANG['addonAA']['configuration']['mainContainer']['generalPage']['leftSide']['noWidgetSection']['customOrClientNumber']['customOrClientNumber']                    = 'Use Custom Number';
$_LANG['addonAA']['configuration']['mainContainer']['generalPage']['leftSide']['noWidgetSection']['customOrClientNumber']['description']                             = 'If enabled, the number set in the custom field will be used.';
$_LANG['addonAA']['configuration']['mainContainer']['generalPage']['leftSide']['noWidgetSection']['prefixNumberCode']['prefixNumberCode']                            = 'Code Prefix';
$_LANG['addonAA']['configuration']['mainContainer']['generalPage']['leftSide']['noWidgetSection']['prefixNumberCode']['description']                                 = "The provided code prefix will be automatically added to the client's SMS number with the country code (if not specified).<br>If you choose <b>`none`</b> option you need to provide the SMS number with a country code.";
$_LANG['addonAA']['tools']['mainContainer']['clientStatusPages']['table']['loginNotify']                                                                             = 'Login Notification';
$_LANG['addonAA']['tools']['massDisableStatusModal']['modal']['massDisableStatusModal']                                                                              = 'Client Verification Status';
$_LANG['addonAA']['tools']['massDisableStatusModal']['massEnableForm']['confirmDisableTemplates']                                                                    = 'Are you sure that you want to change the verification status for the selected clients?';
$_LANG['addonAA']['tools']['massDisableStatusModal']['massEnableForm']['enableStatus']['enableStatus']                                                               = 'Status is verified?';
$_LANG['addonAA']['tools']['massDisableStatusModal']['massEnableForm']['enableStatus']['description']                                                                = 'If enabled, the status will be changed to verified';
$_LANG['addonAA']['tools']['massDisableStatusModal']['baseAcceptButton']['title']                                                                                    = 'Confirm';
$_LANG['addonAA']['tools']['massDisableStatusModal']['baseCancelButton']['title']                                                                                    = 'Cancel';
$_LANG['verifiStatusOn']                                                                                                                                             = 'Enable';
$_LANG['verifiStatusOff']                                                                                                                                            = 'Disable';
$_LANG['enableStatusOn']                                                                                                                                             = 'Enable';
$_LANG['enableStatusOff']                                                                                                                                            = 'Disable';
$_LANG['addonAA']['massSms']['addTplModal']['addTplForm']['halfPageSection']['type_tpl']['type_tpl']                                                                 = 'Type';
$_LANG['addonAA']['configuration']['testConnectionModal']['modal']['testConnectionModal']                                                                            = 'Test Connection';
$_LANG['addonAA']['configuration']['testConnectionModal']['testConnectionForm']['settingName']['settingName']                                                        = 'Settings';
$_LANG['addonAA']['configuration']['testConnectionModal']['baseAcceptButton']['title']                                                                               = 'Test Connection';
$_LANG['addonAA']['configuration']['testConnectionModal']['baseCancelButton']['title']                                                                               = 'Cancel';
$_LANG['addonAA']['configuration']['mainContainer']['submodulesPage']['testConnectionModalButton']['button']['testConnectionModalButton']                            = 'Test Connection';
$_LANG['addonAA']['configuration']['mainContainer']['submoduleSettingsPage']['AtomicSMS']                                                                            = 'Atomic SMS';
$_LANG['addonAA']['configuration']['mainContainer']['submoduleSettingsPage']['addAdvancedSettings']['button']['addAdvancedSettings']                                 = 'Add Settings';
$_LANG['addonAA']['configuration']['mainContainer']['submoduleSettingsPage']['table']['submodule']                                                                   = 'SMS Gateway';
$_LANG['addonAA']['configuration']['mainContainer']['submoduleSettingsPage']['table']['settingName']                                                                 = 'Setting Name';
$_LANG['addonAA']['configuration']['mainContainer']['submoduleSettingsPage']['advancedSettingSubmoduleModalButton']['button']['advancedSettingSubmoduleModalButton'] = 'Edit';
$_LANG['addonAA']['configuration']['mainContainer']['submoduleSettingsPage']['deleteSettingsModalButton']['button']['deleteSettingsModalButton']                     = 'Delete';
$_LANG['addonAA']['configuration']['mainContainer']['submoduleSettingsPage']['BearSms']                                                                              = 'Bear SMS';
$_LANG['addonAA']['configuration']['mainContainer']['submoduleSettingsPage']['BoxisSms']                                                                             = 'Boxis SMS';
$_LANG['addonAA']['configuration']['mainContainer']['submoduleSettingsPage']['BulkSms']                                                                              = 'BulkSMS';
$_LANG['addonAA']['configuration']['mainContainer']['submoduleSettingsPage']['Clickatell']                                                                           = 'Clickatell';
$_LANG['addonAA']['configuration']['mainContainer']['submoduleSettingsPage']['ClickatellNew']                                                                        = 'Clickatell New';
$_LANG['addonAA']['configuration']['mainContainer']['submoduleSettingsPage']['FastSms']                                                                              = 'Fast SMS';
$_LANG['addonAA']['configuration']['mainContainer']['submoduleSettingsPage']['Infobip']                                                                              = 'Infobip';
$_LANG['addonAA']['configuration']['mainContainer']['submoduleSettingsPage']['MessageBird']                                                                          = 'MessageBird';
$_LANG['addonAA']['configuration']['mainContainer']['submoduleSettingsPage']['Nexmo']                                                                                = 'Nexmo';
$_LANG['addonAA']['configuration']['mainContainer']['submoduleSettingsPage']['SmsEagle']                                                                             = 'SMSEagle';
$_LANG['addonAA']['configuration']['mainContainer']['submoduleSettingsPage']['SmsGlobal']                                                                            = 'SMSGlobal';
$_LANG['addonAA']['configuration']['mainContainer']['submoduleSettingsPage']['TestModule']                                                                           = 'TestModule';
$_LANG['addonAA']['configuration']['mainContainer']['submoduleSettingsPage']['Twilio']                                                                               = 'Twilio';
$_LANG['addonAA']['configuration']['mainContainer']['submoduleSettingsPage']['VoodooSms']                                                                            = 'VoodooSMS';
$_LANG['addonAA']['configuration']['mainContainer']['submoduleSettingsPage']['Textplode']                                                                            = 'Texplode';
$_LANG['addonAA']['configuration']['mainContainer']['submoduleSettingsPage']['TraiTel']                                                                              = 'TraiTel';
$_LANG['addonAA']['configuration']['mainContainer']['submoduleSettingsPage']['Textlocal']                                                                            = 'Textlocal';
$_LANG['addonAA']['configuration']['mainContainer']['submoduleSettingsPage']['GretorSMS']                                                                            = 'Gretor SMS (HTTP Simple API)';
$_LANG['addonAA']['configuration']['mainContainer']['submoduleSettingsPage']['SMSMV']                                                                                = 'SMS Maldives (SMSMV)';
$_LANG['addonAA']['configuration']['mainContainer']['submoduleSettingsPage']['Mocean']                                                                               = 'Mocean';
$_LANG['addonAA']['configuration']['mainContainer']['submoduleSettingsPage']['AfricasTalking']                                                                       = "Africa's Talking";
$_LANG['addonAA']['configuration']['addAdvancedSettingsModal']['advancedSettingsSubmoduleForm']['api_token']['api_token']                                            = 'Api Token';
$_LANG['addonAA']['configuration']['editAdvancedSettingsModal']['editAdvancedSettingsSubmoduleForm']['api_token']['api_token']                                       = 'Api Token';
$_LANG['addonAA']['configuration']['addAdvancedSettingsModal']['advancedSettingsSubmoduleForm']['application Id']['Application Id']                                  = 'Application ID';
$_LANG['addonAA']['configuration']['editAdvancedSettingsModal']['editAdvancedSettingsSubmoduleForm']['application Id']['Application Id']                             = 'Application ID';
$_LANG['addonAA']['massSms']['mainContainer']['filterPage']['boxDoubleSection']['noWidgetSection']['enabledGroup']['enabledGroup']                                   = 'Select Client Group';
$_LANG['addonAA']['massSms']['mainContainer']['filterPage']['boxDoubleSection']['noWidgetSection']['enabledGroup']['description']                                    = 'Select the particular group(s) of clients that your message will be sent to';
$_LANG['changesSavedFailed']                                                                                                                                         = 'The gateway cannot be enabled, configure the gateway settings first';
$_LANG['addonAA']['massSms']['editTplModal']['addTplForm']['halfPageSection']['type_tpl']['type_tpl']                                                                = 'Type';
$_LANG['addonAA']['massSms']['editTplModal']['addTplForm']['halfPageSection']['domain']['ticketRelated']                                                             = 'Domain';
$_LANG['addonAA']['massSms']['editTplModal']['addTplForm']['halfPageSection']['product']['ticketRelated']                                                            = 'Product';
$_LANG['addonAA']['configuration']['simpleSettingsSubmoduleModal']['simpleSettingsSubmoduleForm']['connection']['connection']                                        = 'Connection';
$_LANG['addonAA']['configuration']['simpleSettingsSubmoduleModal']['simpleSettingsSubmoduleForm']['send']['send']                                                    = 'Send';
$_LANG['langAddedSuccesfully']                                                                                                                                       = 'The template has been added successfully';
$_LANG['langDeletedSuccesfully']                                                                                                                                     = 'The template has been deleted successfully';
$_LANG['addonAA']['templates']['addDiscountModal']['addDiscountForm']['halfPageSection']['invoice']['ticketRelated']                                                 = 'Invoice Related';
$_LANG['addonAA']['templates']['addDiscountModal']['addDiscountForm']['halfPageSection']['domain']['ticketRelated']                                                  = 'Domain';
$_LANG['addonAA']['templates']['addDiscountModal']['addDiscountForm']['halfPageSection']['lang']['lang']                                                             = 'Language';
$_LANG['addonAA']['templates']['addDiscountModal']['addDiscountForm']['halfPageSection']['message']['message']                                                       = 'Message';
$_LANG['addonAA']['templates']['addDiscountModal']['addDiscountForm']['halfPageSection']['client']['ticketRelated']                                                  = 'Client Related';
$_LANG['addonAA']['templates']['addDiscountModal']['addDiscountForm']['halfPageSection']['other']['ticketRelated']                                                   = 'Other';
$_LANG['noUserRequired']                                                                                                                                             = 'There are no users who meet the requirements';
$_LANG['FormValidators']['incorectNumber']                                                                                                                           = 'The number :number: is incorrect';
$_LANG['FormValidators']['requiredField']                                                                                                                            = 'This field is required';
$_LANG['FormValidators']['nameInUsed']                                                                                                                               = 'The name is already used';
$_LANG['settingNameInUsed']                                                                                                                                          = 'The setting name is already used';
$_LANG['addonAA']['configuration']['editAdvancedSettingsModal']['advancedSettingsSubmoduleForm']['adminNumberValid']['adminNumberValid']                             = 'Number Validator';

$_LANG['addonAA']['templates']['addDiscountModal']['addDiscountForm']['halfPageSection']['product']['ticketRelated']                                                 = 'Product';
$_LANG['addonAA']['pagesLabels']['submodules']['submoduleSettings']                                                                                                  = 'Advanced Settings';
$_LANG['addonAA']['tools']['mainContainer']['sendSmsPage']['boxSection']['halfPageSection']['clients']['description']                                                = 'Select the client to whom the message will be sent';
$_LANG['numericValue']                                                                                                                                               = 'Provide numeric value.';
$_LANG['generalSettingsSaveFailed']                                                                                                                                  = 'Settings save failed.';
$_LANG['tokenGenerated']                                                                                                                                             = 'The account needs verification.';
$_LANG['TokenDeleted']                                                                                                                                               = 'The account was verified.';
$_LANG['incorectr2FAToken']                                                                                                                                          = 'Your token is incorrect.';
$_LANG['savedClientNotification']                                                                                                                                    = 'The notification has been saved successfully';
$_LANG['savedClientNotificationFailed']                                                                                                                              = 'The notification cannot be saved';
$_LANG['savedAdminNotificationFailed']                                                                                                                               = 'The administrator notifications cannot be saved';

$_LANG['addonAA']['clientSummer']['sendSms']['success']                                                                                                              = 'SMS has been sent successfully';
$_LANG['addonAA']['clientSummer']['sendSms']['error']                                                                                                                = 'Cannot send SMS, %s';
$_LANG['addonAA']['clientSummer']['sendSms']['toqueue']                                                                                                              = 'The message has been added to the queue successfully';
$_LANG['addonAA']['api']['clientSummer']['sendSms']['success']                                                                                                       = 'SMS has been sent successfully';
$_LANG['addonAA']['api']['clientSummer']['sendSms']['error']                                                                                                         = 'Cannot send SMS, %s';
$_LANG['addonAA']['api']['clientSummer']['sendSms']['toqueue']                                                                                                       = 'The message has been added to the queue successfully';

$_LANG['addonAA']['configuration']['simpleSettingsSubmoduleModal']['simpleSettingsSubmoduleForm']['endpoint']['endpoint']    = 'Endpoint';
$_LANG['api-sp3']                                                                                                            = 'API-sp3';
$_LANG['api']                                                                                                                = 'API';

$_LANG['addonAA']['configuration']['addAdvancedSettingsModal']['advancedSettingsSubmoduleForm']['endpoint']['endpoint']      = 'Endpoint';
$_LANG['addonAA']['configuration']['editAdvancedSettingsModal']['editAdvancedSettingsSubmoduleForm']['endpoint']['endpoint'] = 'Endpoint';


$_LANG['addonAA']['templates']['mainContainer']['templatesPage']['crmTab']                                                   = 'CRM Tab';
$_LANG['addonAA']['templates']['mainContainer']['templatesPage']['crm']['table']['id']                                       = 'ID';
$_LANG['addonAA']['templates']['mainContainer']['templatesPage']['crm']['table']['msg_name']                                 = 'Template Name';
$_LANG['addonAA']['templates']['mainContainer']['templatesPage']['crm']['table']['enabled']                                  = 'Status';
$_LANG['addonAA']['templates']['mainContainer']['templatesPage']['crm']['moveToSettingPage']['button']['moveToSettingPage']  = 'Move To Setting Page';
$_LANG['addonAA']['configuration']['simpleSettingsSubmoduleModal']['simpleSettingsSubmoduleForm']['admin_id']['admin_id']    = 'Administrator ID';
$_LANG['addonAA']['configuration']['mainContainer']['submoduleSettingsPage']['MailSmsTest']                                  = 'Mail SMS Test';
$_LANG['addonAA']['configuration']['editAdvancedSettingsModal']['editAdvancedSettingsSubmoduleForm']['admin_id']['admin_id'] = 'Administrator ID';
$_LANG['addonAA']['configuration']['addAdvancedSettingsModal']['advancedSettingsSubmoduleForm']['admin_id']['admin_id']      = 'Administrator ID';
/* * TEMPLATE NAMES ADMIN AREA* */

$_LANG['addonAA']['Templates']                  = 'Expired Domain Notice';
$_LANG['addonAA']['Domain Registration Confirmation']       = 'Domain Registration Confirmation';
$_LANG['addonAA']['Domain Renewal Confirmation']            = 'Domain Renewal Confirmation';
$_LANG['addonAA']['Domain Transfer Completed']              = 'Domain Transfer Completed';
$_LANG['addonAA']['Domain Transfer Initiated']              = 'Domain Transfer Initiated';
$_LANG['addonAA']['Upcoming Domain Renewal Notice']         = 'Upcoming Domain Renewal Notice';
$_LANG['addonAA']['Domain Transfer Failed']                 = 'Domain Transfer Failed';
$_LANG['addonAA']['Affiliate Monthly Referrals Report']     = 'Affiliate Monthly Referrals Report';
$_LANG['addonAA']['Credit Warning']                         = 'Credit Warning';
$_LANG['addonAA']['Access Details Reset']                   = 'Access Details Reset';
$_LANG['addonAA']['Client Signup Email']                    = 'Client Signup Email';
$_LANG['addonAA']['Automated Password Reset']               = 'Automated Password Reset';
$_LANG['addonAA']['Credit Card Expiring Soon']              = 'Credit Card Expiring Soon';
$_LANG['addonAA']['Access Details Reset']                   = 'Access Details Reset';
$_LANG['addonAA']['Order Confirmation']                     = 'Order Confirmation';
$_LANG['addonAA']['Password Reset Confirmation']            = 'Password Reset Confirmation';
$_LANG['addonAA']['Password Reset Reminder']                = 'Password Reset Reminder';
$_LANG['addonAA']['Password Reset Validation']              = 'Password Reset Validation';
$_LANG['addonAA']['Quote Accepted']                         = 'Quote Accepted';
$_LANG['addonAA']['Quote Accepted Notification']            = 'Quote Accepted Notification';
$_LANG['addonAA']['Customer Information']                   = 'Customer Information';
$_LANG['addonAA']['Quote Delivery with PDF']                = 'Quote Delivery with PDF';
$_LANG['addonAA']['Unsubscribe Confirmation']               = 'Unsubscribe Confirmation';
$_LANG['addonAA']['Client Email Address Verification']      = 'Client Email Address Verification';
$_LANG['addonAA']['Credit Card Invoice Created']            = 'Credit Card Invoice Created';
$_LANG['addonAA']['Credit Card Payment Confirmation']       = 'Credit Card Payment Confirmation';
$_LANG['addonAA']['Credit Card Payment Due']                = 'Credit Card Payment Due';
$_LANG['addonAA']['Credit Card Payment Failed']             = 'Credit Card Payment Failed';
$_LANG['addonAA']['First Invoice Overdue Notice']           = 'First Invoice Overdue Notice';
$_LANG['addonAA']['Invoice Created']                        = 'Invoice Created';
$_LANG['addonAA']['Invoice Payment Confirmation']           = 'Invoice Payment Confirmation';
$_LANG['addonAA']['Invoice Payment Reminder']               = 'Invoice Payment Reminder';
$_LANG['addonAA']['Invoice Refund Confirmation']            = 'Invoice Refund Confirmation';
$_LANG['addonAA']['Second Invoice Overdue Notice']          = 'Second Invoice Overdue Notice';
$_LANG['addonAA']['Third Invoice Overdue Notice']           = 'Third Invoice Overdue Notice';
$_LANG['addonAA']['Direct Debit Payment Failed']            = 'Direct Debit Payment Failed';
$_LANG['addonAA']['Direct Debit Payment Confirmation']      = 'Direct Debit Payment Confirmation';
$_LANG['addonAA']['Direct Debit Payment Pending']           = 'Direct Debit Payment Pending';
$_LANG['addonAA']['Credit Card Payment Pending']            = 'Credit Card Payment Pending';
$_LANG['addonAA']['Invoice Modified']                       = 'Invoice Modified';
$_LANG['addonAA']['Service Unsuspension Notification']      = 'Service Unsuspension Notification';
$_LANG['addonAA']['Cancellation Request Confirmation']      = 'Cancellation Request Confirmation';
$_LANG['addonAA']['Dedicated/VPS Server Welcome Email']     = 'Dedicated/VPS Server Welcome Email';
$_LANG['addonAA']['Hosting Account Welcome Email']          = 'Hosting Account Welcome Email';
$_LANG['addonAA']['Other Product/Service Welcome Email']    = 'Other Product/Service Welcome Email';
$_LANG['addonAA']['Reseller Account Welcome Email']         = 'Reseller Account Welcome Email';
$_LANG['addonAA']['Service Suspension Notification']        = 'Service Suspension Notification';
$_LANG['addonAA']['SHOUTcast Welcome Email']                = 'SHOUTcast Welcome Email';
$_LANG['addonAA']['New Account Information']                = 'New Account Information';
$_LANG['addonAA']['Server Information']                     = 'Server Information';
$_LANG['addonAA']['SpamExperts Welcome Email']              = 'SpamExperts Welcome Email';
$_LANG['addonAA']['Weebly Welcome Email']                   = 'Weebly Welcome Email';
$_LANG['addonAA']['SSL Certificate Configuration Required'] = 'SSL Certificate Configuration Required';
$_LANG['addonAA']['Upgrade Order Cancelled']                = 'Upgrade Order Cancelled';
$_LANG['addonAA']['SiteLock Welcome Email']                 = 'SiteLock Welcome Email';
$_LANG['addonAA']['Default Notification Message']           = 'Default Notification Message';
$_LANG['addonAA']['Clients Only Bounce Message']            = 'Clients Only Bounce Message';
$_LANG['addonAA']['Replies Only Bounce Message']            = 'Replies Only Bounce Message';
$_LANG['addonAA']['Support Ticket Auto Close Notification'] = 'Support Ticket Auto Close Notification';
$_LANG['addonAA']['Closed Ticket Bounce Message']           = 'Closed Ticket Bounce Message';
$_LANG['addonAA']['Support Ticket Feedback Request']        = 'Support Ticket Feedback Request';
$_LANG['addonAA']['Support Ticket Opened']                  = 'Support Ticket Opened';
$_LANG['addonAA']['Support Ticket Opened by Admin']         = 'Support Ticket Opened by Admin';
$_LANG['addonAA']['Support Ticket Reply']                   = 'Support Ticket Reply';

$_LANG['addonAA']['Automatic Setup Failed']               = 'Automatic Setup Failed';
$_LANG['addonAA']['Automatic Setup Successful']           = 'Automatic Setup Successful';
$_LANG['addonAA']['Domain Renewal Failed']                = 'Domain Renewal Failed';
$_LANG['addonAA']['Domain Renewal Successful']            = 'Domain Renewal Successful';
$_LANG['addonAA']['New Cancellation Request']             = 'New Cancellation Request';
$_LANG['addonAA']['New Order Notification']               = 'New Order Notification';
$_LANG['addonAA']['Service Unsuspension Failed']          = 'Service Unsuspension Failed';
$_LANG['addonAA']['Service Unsuspension Successful']      = 'Service Unsuspension Successful';
$_LANG['addonAA']['Support Ticket Created']               = 'Support Ticket Created';
$_LANG['addonAA']['Support Ticket Department Reassigned'] = 'Support Ticket Department Reassigned';
$_LANG['addonAA']['Support Ticket Flagged']               = 'Support Ticket Flagged';
$_LANG['addonAA']['Support Ticket Response']              = 'Support Ticket Response';
$_LANG['addonAA']['Client Login Notification']            = 'Client Login Notification';
$_LANG['addonAA']['configuration']['administratorNotifications']['adminisratorNotifcations']['adminLoginNotification']['AdminLoginNotification'] = 'Admin Login Notification';

/* * TEMPLATE NAMES CLIENT AREA* */

$_LANG['addonCA']['Expired Domain Notice']                  = 'Expired Domain Notice';
$_LANG['addonCA']['Domain Registration Confirmation']       = 'Domain Registration Confirmation';
$_LANG['addonCA']['Domain Renewal Confirmation']            = 'Domain Renewal Confirmation';
$_LANG['addonCA']['Domain Transfer Completed']              = 'Domain Transfer Completed';
$_LANG['addonCA']['Domain Transfer Initiated']              = 'Domain Transfer Initiated';
$_LANG['addonCA']['Upcoming Domain Renewal Notice']         = 'Upcoming Domain Renewal Notice';
$_LANG['addonCA']['Domain Transfer Failed']                 = 'Domain Transfer Failed';
$_LANG['addonCA']['Affiliate Monthly Referrals Report']     = 'Affiliate Monthly Referrals Report';
$_LANG['addonCA']['Credit Warning']                         = 'Credit Warning';
$_LANG['addonCA']['Access Details Reset']                   = 'Access Details Reset';
$_LANG['addonCA']['Client Signup Email']                    = 'Client Signup Email';
$_LANG['addonCA']['Automated Password Reset']               = 'Automated Password Reset';
$_LANG['addonCA']['Credit Card Expiring Soon']              = 'Credit Card Expiring Soon';
$_LANG['addonCA']['Access Details Reset']                   = 'Access Details Reset';
$_LANG['addonCA']['Order Confirmation']                     = 'Order Confirmation';
$_LANG['addonCA']['Password Reset Confirmation']            = 'Password Reset Confirmation';
$_LANG['addonCA']['Password Reset Reminder']                = 'Password Reset Reminder';
$_LANG['addonCA']['Password Reset Validation']              = 'Password Reset Validation';
$_LANG['addonCA']['Quote Accepted']                         = 'Quote Accepted';
$_LANG['addonCA']['Quote Accepted Notification']            = 'Quote Accepted Notification';
$_LANG['addonCA']['Customer Information']                   = 'Customer Information';
$_LANG['addonCA']['Quote Delivery with PDF']                = 'Quote Delivery with PDF';
$_LANG['addonCA']['Unsubscribe Confirmation']               = 'Unsubscribe Confirmation';
$_LANG['addonCA']['Client Email Address Verification']      = 'Client Email Address Verification';
$_LANG['addonCA']['Credit Card Invoice Created']            = 'Credit Card Invoice Created';
$_LANG['addonCA']['Credit Card Payment Confirmation']       = 'Credit Card Payment Confirmation';
$_LANG['addonCA']['Credit Card Payment Due']                = 'Credit Card Payment Due';
$_LANG['addonCA']['Credit Card Payment Failed']             = 'Credit Card Payment Failed';
$_LANG['addonCA']['First Invoice Overdue Notice']           = 'First Invoice Overdue Notice';
$_LANG['addonCA']['Invoice Created']                        = 'Invoice Created';
$_LANG['addonCA']['Invoice Payment Confirmation']           = 'Invoice Payment Confirmation';
$_LANG['addonCA']['Invoice Payment Reminder']               = 'Invoice Payment Reminder';
$_LANG['addonCA']['Invoice Refund Confirmation']            = 'Invoice Refund Confirmation';
$_LANG['addonCA']['Second Invoice Overdue Notice']          = 'Second Invoice Overdue Notice';
$_LANG['addonCA']['Third Invoice Overdue Notice']           = 'Third Invoice Overdue Notice';
$_LANG['addonCA']['Direct Debit Payment Failed']            = 'Direct Debit Payment Failed';
$_LANG['addonCA']['Direct Debit Payment Confirmation']      = 'Direct Debit Payment Confirmation';
$_LANG['addonCA']['Direct Debit Payment Pending']           = 'Direct Debit Payment Pending';
$_LANG['addonCA']['Credit Card Payment Pending']            = 'Credit Card Payment Pending';
$_LANG['addonCA']['Invoice Modified']                       = 'Invoice Modified';
$_LANG['addonCA']['Service Unsuspension Notification']      = 'Service Unsuspension Notification';
$_LANG['addonCA']['Cancellation Request Confirmation']      = 'Cancellation Request Confirmation';
$_LANG['addonCA']['Dedicated/VPS Server Welcome Email']     = 'Dedicated/VPS Server Welcome Email';
$_LANG['addonCA']['Hosting Account Welcome Email']          = 'Hosting Account Welcome Email';
$_LANG['addonCA']['Other Product/Service Welcome Email']    = 'Other Product/Service Welcome Email';
$_LANG['addonCA']['Reseller Account Welcome Email']         = 'Reseller Account Welcome Email';
$_LANG['addonCA']['Service Suspension Notification']        = 'Service Suspension Notification';
$_LANG['addonCA']['SHOUTcast Welcome Email']                = 'SHOUTcast Welcome Email';
$_LANG['addonCA']['New Account Information']                = 'New Account Information';
$_LANG['addonCA']['Server Information']                     = 'Server Information';
$_LANG['addonCA']['SpamExperts Welcome Email']              = 'SpamExperts Welcome Email';
$_LANG['addonCA']['Weebly Welcome Email']                   = 'Weebly Welcome Email';
$_LANG['addonCA']['SSL Certificate Configuration Required'] = 'SSL Certificate Configuration Required';
$_LANG['addonCA']['Upgrade Order Cancelled']                = 'Upgrade Order Cancelled';
$_LANG['addonCA']['SiteLock Welcome Email']                 = 'SiteLock Welcome Email';
$_LANG['addonCA']['Default Notification Message']           = 'Default Notification Message';
$_LANG['addonCA']['Clients Only Bounce Message']            = 'Clients Only Bounce Message';
$_LANG['addonCA']['Replies Only Bounce Message']            = 'Replies Only Bounce Message';
$_LANG['addonCA']['Support Ticket Auto Close Notification'] = 'Support Ticket Auto Close Notification';
$_LANG['addonCA']['Closed Ticket Bounce Message']           = 'Closed Ticket Bounce Message';
$_LANG['addonCA']['Support Ticket Feedback Request']        = 'Support Ticket Feedback Request';
$_LANG['addonCA']['Support Ticket Opened']                  = 'Support Ticket Opened';
$_LANG['addonCA']['Support Ticket Opened by Admin']         = 'Support Ticket Opened by Admin';
$_LANG['addonCA']['Support Ticket Reply']                   = 'Support Ticket Reply';

$_LANG['addonAA']['configuration']['simpleSettingsSubmoduleModal']['simpleSettingsSubmoduleForm']['eapiUrl']['eapiUrl']     = 'Eapi URL';
$_LANG['addonAA']['configuration']['simpleSettingsSubmoduleModal']['simpleSettingsSubmoduleForm']['eapiUrl']['description'] = 'Set EAPI URL from your BulkSMS profile page. If the field is left blank, it will use the default address \'http://bulksms.vsms.net/eapi/\'';

/* refactoring */
/* notifications: */
$_LANG['Client phone number not found'] = 'Client phone number not found';

/* logs */
$_LANG['smsSender']['logger']['sendSms']['sendInvoiceToken']['success'] = 'The token :token: for the invoice :invoiceId: has been sent to the user :userDetails: successfully :gatewayDetails:';
$_LANG['smsSender']['logger']['sendSms']['sendInvoiceToken']['error'] = "The process of sending the verification invoice token :token: for the invoice :invoiceId: to the user :userDetails: failed <b>:gatewayDetails:</b> <b>:errorDetails:</b>";

$_LANG['smsSender']['logger']['sendSms']['resendInvoiceToken']['success']   = 'The token :token: for the invoice :invoiceId: has been resent to the user :userDetails: successfully :gatewayDetails:';
    $_LANG['smsSender']['logger']['sendSms']['resendInvoiceToken']['error'] = "The process of resending the verification invoice token :token: for the invoice :invoiceId: to the user :userDetails: failed <b>:gatewayDetails:</b> <b>:errorDetails:</b>";

$_LANG['smsSender']['logger']['sendSms']['summarySend']['success'] = "SMS has been sent to the user :userDetails: from the summary page successfully :gatewayDetails: :additionalDetails:";
$_LANG['smsSender']['logger']['sendSms']['summarySend']['error']   = "The process of sending SMS to the user :userDetails: from the <b>summary page</b> has failed <b>:gatewayDetails:</b> <b>:errorDetails:</b> <b>:additionalDetails:</b>";

$_LANG['smsSender']['logger']['sendSms']['manualSend']['success'] = "The message has been sent manually to :userDetails: by administrator :administratorDetails: successfully :gatewayDetails:";
$_LANG['smsSender']['logger']['sendSms']['manualSend']['error']   = "The message to :userDetails: cannot be sent by administrator :administratorDetails: <b>:gatewayDetails:</b> <b>:errorDetails:</b>";
$_LANG['smsSender']['logger']['sendSms']['manualSend']['info']    = "SMS has been sent to the user :userDetails: manually";

$_LANG['smsSender']['logger']['sendSms']['queueSend']['success'] = "SMS has been sent from the task :taskId: to :userDetails: successfully :gatewayDetails:";
$_LANG['smsSender']['logger']['sendSms']['queueSend']['error']   = "Cannot send SMS from the task :taskId: to :userDetails: <b>:gatewayDetails:</b> <b>:errorDetails:</b>";

$_LANG['smsSender']['logger']['sendSms']['taskAdded']['success'] = "The message to :userDetails: based on template: <b>:templateDetails:</b> has been added to the queue successfully :gatewayDetails:";;
$_LANG['smsSender']['logger']['sendSms']['taskAdded']['error']   = "The process of adding message to :userDetails: based on template: <b>:templateDetails:</b> to the queue has failed <b>:gatewayDetails:</b>";
$_LANG['smsSender']['logger']['sendSms']['taskAdded']['info']    = "The message to :userDetails: based on template: <b>:templateDetails:</b> has been added to the queue successfully :gatewayDetails:";

$_LANG['smsSender']['logger']['sendSms']['taskDeleted']['info']    = "The task :taskId: has been deleted successfully";

$_LANG['smsSender']['logger']['sendSms']['sendOrderToken']['success'] = "The token :token: for the order :orderId: has been sent to the user :userDetails: successfully :gatewayDetails:";
$_LANG['smsSender']['logger']['sendSms']['sendOrderToken']['error']   = "The process of sending the verification order token :token: for the order :orderId: to the user :userDetails: failed <b>:gatewayDetails:</b> <b>:errorDetails:</b>";

$_LANG['smsSender']['logger']['sendSms']['newToken']['success'] = "The verification account token :token: has been sent to the user :userDetails: successfully :gatewayDetails:";
$_LANG['smsSender']['logger']['sendSms']['newToken']['error']   = "The process of sending the <b>verification account token</b> :token: to the user :userDetails: failed <b>:gatewayDetails:</b> <b>:errorDetails:</b>";

$_LANG['smsSender']['logger']['sendSms']['resendClientToken']['success'] = "The verification account token :token: has been resent to the user :userDetails: successfully :gatewayDetails:";
$_LANG['smsSender']['logger']['sendSms']['resendClientToken']['error']   = "The process of resending the <b>verification account token</b> :token: to the user :userDetails: failed <b>:gatewayDetails:</b> <b>:errorDetails:</b>";

$_LANG['smsSender']['logger']['sendSms']['resendOrderToken']['success'] = "The token :token: for the order :orderId: has been resent to the user :userDetails: successfully :gatewayDetails:";
$_LANG['smsSender']['logger']['sendSms']['resendOrderToken']['error']   = "The process of resending the <b>verification order token</b> :token: for the order :orderId: to the user :userDetails: failed <b>:gatewayDetails:</b> <b>:errorDetails:</b>";

$_LANG['smsSender']['logger']['sendSms']['emailPreSend']['success'] = "The SMS based on template: :templateDetails: has been sent to the user :userDetails: successfully :gatewayDetails:";
$_LANG['smsSender']['logger']['sendSms']['emailPreSend']['info']   = "Cannot send the SMS based on template: <b>:templateDetails:</b> to the user :userDetails: <b>:gatewayDetails:</b> <b>:errorDetails:</b>";

$_LANG['smsSender']['logger']['sendSms']['2FaActivate']['success'] = "The two-factor authentication activation token :token: has been sent to the user :userDetails: successfully :gatewayDetails:";
$_LANG['smsSender']['logger']['sendSms']['2FaActivate']['error']   = "Cannot send the <b>two-factor authentication activation token</b> :token: to the user :userDetails: <b>:gatewayDetails:</b> <b>:errorDetails:</b>";

$_LANG['smsSender']['logger']['sendSms']['2FaLogin']['success'] = "The two-factor authentication token :token: has been sent to the user :userDetails: successfully :gatewayDetails:";
$_LANG['smsSender']['logger']['sendSms']['2FaLogin']['error']   = "Cannot send the <b>two-factor authentication token</b> :token: to the user :userDetails: <b>:gatewayDetails:</b> <b>:errorDetails:</b>";

$_LANG['smsSender']['logger']['sendSms']['localApi']['success'] = "SMS has been sent from localAPI to :userDetails: by :administratorDetails: successfully :gatewayDetails:";
$_LANG['smsSender']['logger']['sendSms']['localApi']['error']   = "Cannot send SMS from <b>localAPI</b> to :userDetails: by :administratorDetails: <b>:gatewayDetails:</b> <b>:errorDetails:</b>";

/*FromQueue*/
//success
$_LANG['smsSender']['logger']['sendSms']['queued_emailPreSend']['info'] = "The message to :userDetails: based on template: :templateDetails: has been added to the queue successfully";
$_LANG['smsSender']['logger']['sendSms']['queued_summarySend']['info']  = "The message to :userDetails: from summary page has been added to the queue successfully";
$_LANG['smsSender']['logger']['sendSms']['queued_manualSend']['info']   = "The custom SMS message to :userDetails: has been added to the queue successfully";
$_LANG['smsSender']['logger']['sendSms']['queued_localApi']['info']     = "The message to :userDetails: from localAPI has been added to the queue successfully";

//errors
/* 3.2.0 features lang */
$_LANG['CaAdminNotAcceptSMS'] = "Administrator did not activate any SMS notifications.";
$_LANG['addonAA']['tools']['mainContainer']['clientStatusPages']['table']['acceptSms'] = 'Accept SMS';
$_LANG['addonAA']['tools']['mainContainer']['clientStatusPages']['table']['marketingSms'] = 'Marketing SMS';
/*end langs*/
$_LANG['addonAA']['templates']['addDiscountModal']['addDiscountForm']['halfPageSection']['charsCounter-name']['Character count'] = 'Number Of Characters';
$_LANG['addonAA']['templates']['editLangModal']['editLangForm']['halfPageSection']['charsCounter-name']['Character count'] = 'Number Of Characters';
$_LANG['addonAA']['templates']['editLangModal']['editLangForm']['halfPageSection']['lang']['lang'] = 'Language';
$_LANG['addonAA']['templates']['editLangModal']['editLangForm']['halfPageSection']['message']['message'] = 'Message';
$_LANG['ticketRelated'] = 'Domain Related';
$_LANG['ticketRelated'] = 'Client Related';
$_LANG['ticketRelated'] = 'Other';
$_LANG['addonAA']['tools']['mainContainer']['sendSmsPage']['boxSection']['halfPageSection']['charsCounter-name']['Character count'] = 'Number Of Characters';
$_LANG['addonAA']['massSms']['mainContainer']['massSmsDataTable']['addClientButton-name']['button']['addClientButton-title'] = 'Add Client';
$_LANG['addonAA']['massSms']['addClientModal-name']['modal']['addClientModal-title'] = 'Add Client';
$_LANG['addonAA']['massSms']['addClientModal-name']['addClientForm-name']['clientsList']['clientsList'] = 'Clients';
$_LANG['addonAA']['massSms']['addClientModal-name']['baseAcceptButton']['title'] = 'Confirm';
$_LANG['addonAA']['massSms']['addClientModal-name']['baseCancelButton']['title'] = 'Cancel';
$_LANG['addonAA']['massSms']['addTplModal']['addTplForm']['halfPageSection']['charsCounter-name']['Character count'] = 'Number Of Characters';
$_LANG['addonAA']['massSms']['editTplModal']['addTplForm']['halfPageSection']['charsCounter-name']['Character count'] = 'Number Of Characters';

$_LANG['addonAA']['massSms']['mainContainer']['massSmsDataTable']['disableClientButton-name']['button']['disableClientButton-title'] = 'Remove';

$_LANG['addonAA']['massSms']['disableClientModal-name']['modal']['disableClientModal-title'] = 'DisableClientModal-title';
$_LANG['addonAA']['massSms']['disableClientModal-name']['disableClientForm-name']['removeCLientMassSms'] = 'Are you sure you want to remove this client?';
$_LANG['addonAA']['massSms']['disableClientModal-name']['baseAcceptButton']['title'] = 'Confirm';
$_LANG['addonAA']['massSms']['disableClientModal-name']['baseCancelButton']['title'] = 'Cancel';
$_LANG['addonAA']['massSms']['Remove %s %s'] = 'Remove %s %s';
$_LANG['addonAA']['massSms']['button']['massActionDisableClientsButton-title'] = 'Remove';

$_LANG['addonAA']['massSms']['massActionDisableClientsModal-name']['modal']['massActionDisableClientsModal-title'] = 'Remove';
$_LANG['addonAA']['massSms']['massActionDisableClientsModal-name']['massActionQueueDeleteForm']['confirmMassActionRemoveClientsForm'] = 'Are you sure you want to remove the selected clients?';
$_LANG['addonAA']['massSms']['massActionDisableClientsModal-name']['baseAcceptButton']['title'] = 'Confirm';
$_LANG['addonAA']['massSms']['massActionDisableClientsModal-name']['baseCancelButton']['title'] = 'Cancel';



/* Merge Fields */


$_LANG['ticketRelated'] = 'Invoice';
$_LANG['Invoice Id'] = 'Invoice ID';
$_LANG['Invoice Num'] = 'Invoice Number';
$_LANG['Invoice Date Created'] = 'Date Created';
$_LANG['Invoice Date Due'] = 'Due Date';
$_LANG['Invoice Date Paid'] = 'Date Paid';
$_LANG['Invoice Items'] = 'Invoice Items';
$_LANG['Invoice Html Contents'] = 'Invoice Items HTML';
$_LANG['Invoice Subtotal'] = 'Sub Total';
$_LANG['Invoice Tax'] = 'Tax';
$_LANG['Invoice Tax Rate'] = 'Tax Rate';
$_LANG['Invoice Tax2'] = 'Tax 2';
$_LANG['Invoice Tax Rate2'] = 'Tax Rate 2';
$_LANG['Invoice Credit'] = 'Credit';
$_LANG['Invoice Total'] = 'Total';
$_LANG['Invoice Amount Paid'] = 'Amount Paid';
$_LANG['Invoice Balance'] = 'Balance';
$_LANG['Invoice Last Payment Amount'] = 'Last Payment Amount';
$_LANG['Invoice Last Payment Transid'] = 'Last Payment Trans ID';
$_LANG['Invoice Payment Method'] = 'Payment Method';
$_LANG['Invoice Payment Link'] = 'Payment Link';
$_LANG['Invoice Subscription Id'] = 'Subscription ID';
$_LANG['Invoice Status'] = 'Status';
$_LANG['Invoice Link'] = 'Invoice Link';
$_LANG['Invoice Previous Balance'] = 'Previous Balance';
$_LANG['Invoice Total Balance Due'] = 'Total Due Invoices Balance';
$_LANG['Invoice Notes'] = 'Notes';
$_LANG['Invoice All Due Total'] = 'All Due Invoices';
$_LANG['Invoice Domains'] = 'Domains';
$_LANG['Invoice Domains Registered'] = 'Domains Registered';
$_LANG['Invoice Domains Transfered'] = 'Domains Transferred';
$_LANG['Invoice Domains Renewed'] = 'Domains Renewed';
$_LANG['Client Id'] = 'ID';
$_LANG['Client Name'] = 'Name';
$_LANG['Client First Name'] = 'First Name';
$_LANG['Client Last Name'] = 'Last Name';
$_LANG['Client Company Name'] = 'Company Name';
$_LANG['Client Email'] = 'Email Address';
$_LANG['Client Address1'] = 'Address 1';
$_LANG['Client Address2'] = 'Address 2';
$_LANG['Client City'] = 'City';
$_LANG['Client State'] = 'State';
$_LANG['Client Postcode'] = 'Postcode';
$_LANG['Client Country'] = 'Country';
$_LANG['Client Phonenumber'] = 'Phone Number';
$_LANG['Client Password'] = 'Password';
$_LANG['Client Signup Date'] = 'Sigup Date';
$_LANG['Client Credit'] = 'Credit Balance';
$_LANG['Client Cc Type'] = 'Card Type';
$_LANG['Client Cc Number'] = 'Card Number';
$_LANG['Client Cc Expiry'] = 'Card Expiry Date';
$_LANG['Client Language'] = 'Language';
$_LANG['Client Status'] = 'Status';
$_LANG['Client Group Id'] = 'Client Group ID';
$_LANG['Client Group Name'] = 'Client Group Name';
$_LANG['Client Gateway Id'] = 'Client Gateway ID';
$_LANG['Unsubscribe Url'] = 'Unsubscribe URL';
$_LANG['Client Due Invoices Balance'] = 'Total Due Invoices Balance';
$_LANG['Client Custom Fields'] = 'Custom Fields';
$_LANG['Client Custom Field Name'] = 'Custom Field Name';
$_LANG['Client Country Code'] = 'Country Code';
$_LANG['Clientname'] = 'Client Name';
$_LANG['Client Hostname'] = 'Hostname';
$_LANG['Client Ip'] = 'IP Address';
$_LANG['Client Email Verification Link'] = 'Email Verification Link';
$_LANG['Company Name'] = 'Company Name';
$_LANG['Company Domain'] = 'Domain';
$_LANG['Company Logo Url'] = 'Logo URL';
$_LANG['WHMCS Url'] = 'WHMCS URL';
$_LANG['WHMCS Link'] = 'WHMCS Link';
$_LANG['Signature'] = 'Signature';
$_LANG['Date'] = 'Date';
$_LANG['Time'] = 'Time';
$_LANG['Pw Reset Url'] = 'PW Reset URL';
$_LANG['Order Details'] = 'Order Details';
$_LANG['WHMCS Admin Url'] = 'WHMCS Admin URL';
$_LANG['WHMCS Admin Link'] = 'WHMCS Admin Link';
$_LANG['Domain Id'] = 'ID';
$_LANG['Domain Order Id'] = 'Order ID';
$_LANG['Domain Reg Date'] = 'Registration Date';
$_LANG['Domain Status'] = 'Status';
$_LANG['Domain Name'] = 'Name';
$_LANG['Domain Sld'] = 'SLD';
$_LANG['Domain Tld'] = 'TLD';
$_LANG['Domain First Payment Amount'] = 'First Payment Amount';
$_LANG['Domain Recurring Amount'] = 'Recurring Amount';
$_LANG['Domain Registrar'] = 'Registrar';
$_LANG['Domain Reg Period'] = 'Reg Period';
$_LANG['Domain Expiry Date'] = 'Expiry Date';
$_LANG['Domain Next Due Date'] = 'Next Due Date';
$_LANG['Domain Days Until Expiry'] = 'Days Until Expiry';
$_LANG['Domain Days Until Nextdue'] = 'Days Until Next Due Date';
$_LANG['Domain Dns Management'] = 'DNS Management';
$_LANG['Domain Email Forwarding'] = 'Email Forwarding';
$_LANG['Domain Id Protection'] = 'ID Protection';
$_LANG['Domain Do Not Renew'] = 'Auto Renew Disabled';
$_LANG['Domain Type'] = 'Type';
$_LANG['Domain Days After Expiry'] = 'Days After Expiry';
$_LANG['ticketRelated'] = 'Affiliate';
$_LANG['Affiliate Total Visits'] = 'No. of Visits';
$_LANG['Affiliate Balance'] = 'Earning Balance';
$_LANG['Affiliate Withdrawn'] = 'Withdrawn Amount';
$_LANG['Affiliate Referrals Table'] = 'Referrals Table';
$_LANG['Affiliate Referral Url'] = 'Referral URL';
$_LANG['ticketRelated'] = 'Product Related';
$_LANG['Service Order Id'] = 'Order ID';
$_LANG['Service Id'] = 'ID';
$_LANG['Service Reg Date'] = 'Registration Date';
$_LANG['Service Product Name'] = 'Product Name';
$_LANG['Service Product'] = 'Product';
$_LANG['Service Product Description'] = 'Product Description';
$_LANG['Service Config Options'] = 'Config. Options';
$_LANG['Service Config Options Html'] = 'Config. Options HTML';
$_LANG['Service Domain'] = 'Domain';
$_LANG['Service Server Name'] = 'Server Name';
$_LANG['Service Server Hostname'] = 'Server Hostname';
$_LANG['Service Server Ip'] = 'Server IP';
$_LANG['Service Dedicated Ip'] = 'Dedicated IP';
$_LANG['Service Assigned Ips'] = 'Assigned IPs';
$_LANG['Service Ns1'] = 'Nameserver 1';
$_LANG['Service Ns2'] = 'Nameserver 2';
$_LANG['Service Ns3'] = 'Nameserver 3';
$_LANG['Service Ns4'] = 'Nameserver 4';
$_LANG['Service Ns1 Ip'] = 'Nameserver 1 IP';
$_LANG['Service Ns2 Ip'] = 'Nameserver 2 IP';
$_LANG['Service Ns3 Ip'] = 'Nameserver 3 IP';
$_LANG['Service Ns4 Ip'] = 'Nameserver 4 IP';
$_LANG['Service Payment Method'] = 'Payment Method';
$_LANG['Service First Payment Amount'] = 'First Payment Amount';
$_LANG['Service Recurring Amount'] = 'Recurring Payment';
$_LANG['Service Billing Cycle'] = 'Billing Cycle';
$_LANG['Service Next Due Date'] = 'Next Due Date';
$_LANG['Service Status'] = 'Status';
$_LANG['Service Username'] = 'Username';
$_LANG['Service Password'] = 'Password';
$_LANG['Service Subscription Id'] = 'Subscription ID';
$_LANG['Service Suspension Reason'] = 'Suspension Reason';
$_LANG['Service Cancellation Type'] = 'Cancellation Type';
$_LANG['Service Custom Fields'] = 'Custom Fields';
$_LANG['Service Custom Field Xyz'] = 'Custom Field XYZ';
$_LANG['Product Name'] = 'Product Name';
$_LANG['Service Cancellation Reason'] = 'Cancellation Reason';

$_LANG['ticketRelated'] = 'Ticket Related';
$_LANG['Ticket Id'] = 'ID';
$_LANG['Ticket Reply Id'] = 'Reply ID';
$_LANG['Ticket Department'] = 'Department';
$_LANG['Ticket Date Opened'] = 'Date Opened';
$_LANG['Ticket Subject'] = 'Subject';
$_LANG['Ticket Message'] = 'Message';
$_LANG['Ticket Status'] = 'Status';
$_LANG['Ticket Priority'] = 'Priority';
$_LANG['Ticket Tid'] = 'Ticket TID';
$_LANG['Ticket Url'] = 'Ticket URL';
$_LANG['Ticket Link'] = 'Ticket Link';
$_LANG['Ticket Auto Close Time'] = 'Auto Close Time';
$_LANG['Ticket Kb Auto Suggestions'] = 'Knowledgebase Auto Suggestions';

$_LANG['Verification Url'] = 'Verification URL';
$_LANG['Reset Password Url'] = 'Reset Password URL';

/*End of merge fields*/

$_LANG['addonAA']['pagesLabels']['templates']['settings'] = 'Settings';
$_LANG['The connection test has failed: %s']                                                                                                  = 'The connection test has failed: %s';
$_LANG["The SMS message cannot be sent"] = "The SMS message cannot be sent. %s";
$_LANG['addonAA']['configuration']['mainContainer']['submoduleSettingsPage']['Bytehand'] = 'BYTEHAND';
$_LANG['addonAA']['configuration']['mainContainer']['submoduleSettingsPage']['Sms77'] = 'sms77';
$_LANG['addonAA']['configuration']['mainContainer']['submoduleSettingsPage']['SmssSms'] = 'SMSS';
$_LANG['addonAA']['configuration']['mainContainer']['submoduleSettingsPage']['SmsApi'] = 'SMSAPI';

$_LANG['addonAA']['templates']['addDiscountModal']['addDiscountForm']['halfPageSection']['support']['ticketRelated'] = 'Ticket Related';
$_LANG['addonAA']['templates']['editLangModal']['editLangForm']['halfPageSection']['support']['ticketRelated'] = 'Ticket Related';
$_LANG['addonAA']['templates']['editLangModal']['editLangForm']['halfPageSection']['client']['ticketRelated'] = 'Client Related';
$_LANG['addonAA']['templates']['editLangModal']['editLangForm']['halfPageSection']['other']['ticketRelated'] = 'Other';
$_LANG['addonAA']['templates']['addDiscountModal']['addDiscountForm']['halfPageSection']['affiliate']['ticketRelated'] = 'Affiliate';
$_LANG['addonAA']['templates']['editLangModal']['editLangForm']['halfPageSection']['affiliate']['ticketRelated'] = 'Affiliate';
$_LANG['addonAA']['templates']['editLangModal']['editLangForm']['halfPageSection']['domain']['ticketRelated'] = 'Domain';
$_LANG['addonAA']['templates']['editLangModal']['editLangForm']['halfPageSection']['user']['ticketRelated'] = 'User Related';

$_LANG['AtomicSMS'] = "Atomic SMS";
$_LANG['BearSms'] = "BearSMS";
$_LANG['BoxisSms'] = "BoxisSMS";
$_LANG['BudgetSMS'] = "BudgetSMS";
$_LANG['BulkSms'] = "BulkSMS";
$_LANG['BurstSMS'] = "BurstSMS";
$_LANG['Bytehand'] = "BYTEHAND";
$_LANG['ClickSend'] = "ClickSend";
$_LANG['Clickatell'] = "Clickatell";
$_LANG['ClickatellNew'] = "Clickatell (New API)";
$_LANG['FastSms'] = "Fast SMS";
$_LANG['Infobip'] = "Infobip";
$_LANG['MailSmsTest'] = "Test Email";
$_LANG['MessageBird'] = "MessageBird";
$_LANG['Nexmo'] = "Nexmo";
$_LANG['Sms77'] = "sms77";
$_LANG['SmsApi'] = "SMSAPI";
$_LANG['SmsEagle'] = "SMSEagle";
$_LANG['SmsGatewayMe'] = "SMS Gateway Me";
$_LANG['SmsGlobal'] = "SMSGlobal";
$_LANG['SmssSms'] = "SMSS";
$_LANG['TestModule'] = "Test Module";
$_LANG['Twilio'] = "Twilio";
$_LANG['VoodooSms'] = "Voodoo SMS";
$_LANG['temptaleNameExists'] = "The template with the given name already exists";

$_LANG['addonCA']['clientVerification']['mainContainer']['centerWrapper']['clientVerification']['clientVerification'] = 'Verify Account with SMS';
$_LANG['addonCA']['clientVerification']['mainContainer']['centerWrapper']['clientVerification']['tokenVerify']['tokenVerify'] = 'Please provide your one-time verification token to activate your account';
$_LANG['addonCA']['clientVerification']['mainContainer']['centerWrapper']['clientVerification']['baseSubmitButton']['button']['submit'] = 'Submit';
$_LANG['addonCA']['clientVerification']['mainContainer']['centerWrapper']['clientVerification']['resendTokenButton']['button']['resendTokenButton'] = 'Resend Token';

$_LANG['addonCA']['clientVerification']['mainContainer']['centerWrapper']['invoiceVerification']['invoiceVerification'] = 'Verify Invoice with SMS';

$_LANG['addonCA']['clientVerification']['mainContainer']['centerWrapper']['orderVerification']['orderVerification'] = 'Verify Order with SMS';
$_LANG['addonCA']['clientVerification']['mainContainer']['centerWrapper']['orderVerification']['tokenVerify']['tokenVerify'] = 'Please provide one-time verification token to activate your order.';
$_LANG['addonCA']['clientVerification']['mainContainer']['centerWrapper']['orderVerification']['baseSubmitButton']['button']['submit'] = 'Submit';
$_LANG['addonCA']['clientVerification']['mainContainer']['centerWrapper']['orderVerification']['resendTokenButton']['button']['resendTokenButton'] = 'Resend Token';

$_LANG['addonCA']['invoiceVerification']['mainContainer']['centerWrapper']['invoiceVerification']['invoiceVerification']                    = 'Verify Invoice with SMS';
$_LANG['addonCA']['clientVerification']['mainContainer']['centerWrapper']['invoiceVerification']['tokenVerify']['tokenVerify']              = 'Please provide your one-time verification token to see your invoice';
$_LANG['addonCA']['clientVerification']['mainContainer']['centerWrapper']['invoiceVerification']['baseSubmitButton']['button']['submit']    = 'Submit';
$_LANG['addonCA']['clientVerification']['mainContainer']['centerWrapper']['invoiceVerification']['resendTokenButton']['button']['resendTokenButton']    = 'Resend Token';

$_LANG['addonCA']['clientVerification']['mainContainer']['centerWrapper']['clientVerification']['setSmsNumber'] = 'Your SMS number is not defined. Please <b><a href="clientarea.php?action=details">edit your account details</a></b> first.';
$_LANG['addonCA']['clientVerification']['mainContainer']['centerWrapper']['orderVerification']['setSmsNumber'] = 'Your SMS number is not defined. Please <b><a href="clientarea.php?action=details">edit your account details</a></b> first.';

$_LANG['addonCA']['clientVerification']['mainContainer']['centerWrapper']['orderVerification']['setSmsNumber'] = 'Your SMS number is not defined. Please <b><a href="clientarea.php?action=details">edit your account details</a></b> first.';

$_LANG['ticket_stats_Open']              = "Open";
$_LANG['ticket_stats_OnHold']            = "On Hold";
$_LANG['ticket_stats_Answered']          = "Answered";
$_LANG['ticket_stats_Closed']            = "Closed";
$_LANG['ticket_stats_Customer-Reply']    = "Customer-Reply";
$_LANG['ticket_stats_InProgress']        = "In Progress";


$_LANG['ticket_priority_Medium'] = "Medium";
$_LANG['ticket_priority_High']   = "High";
$_LANG['ticket_priority_Low']    = "Low";
$_LANG['addonAA']['configuration']['simpleSettingsSubmoduleModal']['simpleSettingsSubmoduleForm']['accountId']['accountId'] = 'Service Name';
$_LANG['firstUseUrl'] = "First click to the link and connect token with yours account:";
$_LANG['addonAA']['configuration']['simpleSettingsSubmoduleModal']['simpleSettingsSubmoduleForm']['marketingSMS']['marketingSMS'] = 'Marketing SMS';

$_LANG['addonAA']['configuration']['addAdvancedSettingsModal']['advancedSettingsSubmoduleForm']['applicationKey']['applicationKey'] = 'Application Key';
$_LANG['addonAA']['configuration']['addAdvancedSettingsModal']['advancedSettingsSubmoduleForm']['applicationSecret']['applicationSecret'] = 'Application Secret';
$_LANG['addonAA']['configuration']['addAdvancedSettingsModal']['advancedSettingsSubmoduleForm']['consumerKey']['consumerKey'] = 'Consumer Key';
$_LANG['Ovh-eu'] = 'Ovh-eu';
$_LANG['Ovh-ca'] = 'Ovh-ca';
$_LANG['Kimsufi-eu'] = 'Kimsufi-eu';
$_LANG['Kimsufi-ca'] = 'Kimsufi-ca';
$_LANG['Soyoustart-eu'] = 'Soyoustart-eu';
$_LANG['Soyoustart-ca'] = 'Soyoustart-ca';
$_LANG['Runabove-ca'] = 'Runabove-ca';
$_LANG['addonAA']['configuration']['addAdvancedSettingsModal']['advancedSettingsSubmoduleForm']['accountId']['accountId'] = 'Service Name';
$_LANG['Sms-iy1679-1'] = 'Sms-iy1679-1';
$_LANG['addonAA']['configuration']['addAdvancedSettingsModal']['advancedSettingsSubmoduleForm']['marketingSMS']['marketingSMS'] = 'Marketing SMS';
$_LANG['addonAA']['configuration']['mainContainer']['submoduleSettingsPage']['OvhSms'] = 'OVH SMS';
$_LANG['addonAA']['configuration']['addAdvancedSettingsModal']['advancedSettingsSubmoduleForm']['from']['description'] = "Specifies the Twilio phone number, short code, or Messaging Service that sends this message. This must be a Twilio phone number that you own, formatted with a '+' and country code.";
$_LANG['addonAA']['configuration']['simpleSettingsSubmoduleModal']['simpleSettingsSubmoduleForm']['accountSid']['accountSid'] = 'Account Sid';
$_LANG['addonAA']['configuration']['simpleSettingsSubmoduleModal']['simpleSettingsSubmoduleForm']['authToken']['authToken'] = 'Auth Token';
$_LANG['addonAA']['configuration']['simpleSettingsSubmoduleModal']['simpleSettingsSubmoduleForm']['from']['description'] = "Specifies the Twilio phone number, short code, or Messaging Service that sends this message. This must be a Twilio phone number that you own, formatted with a '+' and country code.";
$_LANG['addonAA']['configuration']['noneOptions'] = 'None';

//3.4.0 lang
$_LANG['addonAA']['configuration']['simpleSettingsSubmoduleModal']['simpleSettingsSubmoduleForm']['endPoint']['endPoint'] = 'Endpoint';
$_LANG['addonAA']['configuration']['simpleSettingsSubmoduleModal']['simpleSettingsSubmoduleForm']['passname']['passname'] = 'Password';
$_LANG['addonAA']['configuration']['simpleSettingsSubmoduleModal']['simpleSettingsSubmoduleForm']['sender_id']['sender_id'] = 'Sender ID';
$_LANG['addonAA']['configuration']['simpleSettingsSubmoduleModal']['simpleSettingsSubmoduleForm']['content_type']['content_type'] = 'Content Type';
$_LANG['addonAA']['configuration']['simpleSettingsSubmoduleModal']['simpleSettingsSubmoduleForm']['mode']['mode'] = 'Mode';
$_LANG['addonAA']['configuration']['simpleSettingsSubmoduleModal']['simpleSettingsSubmoduleForm']['userName']['userName'] = 'Username';
$_LANG['addonAA']['configuration']['simpleSettingsSubmoduleModal']['simpleSettingsSubmoduleForm']['userPassword']['userPassword'] = 'Password';

$_LANG['addonAA']['configuration']['simpleSettingsSubmoduleModal']['simpleSettingsSubmoduleForm']['type']['type'] = 'Type';
$_LANG['Text'] = 'Text';
$_LANG['Unicode'] = 'Unicode';
$_LANG['addonAA']['configuration']['simpleSettingsSubmoduleModal']['simpleSettingsSubmoduleForm']['maskName']['maskName'] = 'Mask Name';
$_LANG['addonAA']['configuration']['simpleSettingsSubmoduleModal']['simpleSettingsSubmoduleForm']['campaignName']['campaignName'] = 'Campaign Name';
$_LANG['addonAA']['templates']['editLangModal']['editLangForm']['halfPageSection']['invoice']['ticketRelated'] = 'Invoice Related';
$_LANG['addonAA']['templates']['editLangModal']['editLangForm']['halfPageSection']['product']['ticketRelated'] = 'Product Related';
$_LANG['addonAA']['templates']['editLangModal']['editLangForm']['halfPageSection']['invite']['ticketRelated']  = 'Invitation Related';

$_LANG['addonAA']['configuration']['simpleSettingsSubmoduleModal']['simpleSettingsSubmoduleForm']['applicationKey']['applicationKey'] = 'Application Key';
$_LANG['addonAA']['configuration']['simpleSettingsSubmoduleModal']['simpleSettingsSubmoduleForm']['applicationSecret']['applicationSecret'] = 'Application Secret';
$_LANG['addonAA']['configuration']['simpleSettingsSubmoduleModal']['simpleSettingsSubmoduleForm']['consumerKey']['consumerKey'] = 'Consumer Key';

$_LANG['addonAA']['configuration']['mainContainer']['submoduleSettingsPage']['TrioMobileSms'] = 'Trio Mobile';

$_LANG['addonAA']['configuration']['editAdvancedSettingsModal']['editAdvancedSettingsSubmoduleForm']['endPoint']['endPoint'] = 'Endpoint';
$_LANG['addonAA']['configuration']['editAdvancedSettingsModal']['editAdvancedSettingsSubmoduleForm']['passname']['passname'] = 'Password';
$_LANG['addonAA']['configuration']['editAdvancedSettingsModal']['editAdvancedSettingsSubmoduleForm']['content_type']['content_type'] = 'Content Type';
$_LANG['addonAA']['configuration']['editAdvancedSettingsModal']['editAdvancedSettingsSubmoduleForm']['mode']['mode'] = 'Mode';

$_LANG['addonAA']['configuration']['addAdvancedSettingsModal']['advancedSettingsSubmoduleForm']['endPoint']['endPoint'] = 'Endpoint';
$_LANG['addonAA']['configuration']['addAdvancedSettingsModal']['advancedSettingsSubmoduleForm']['passname']['passname'] = 'Password';
$_LANG['addonAA']['configuration']['addAdvancedSettingsModal']['advancedSettingsSubmoduleForm']['content_type']['content_type'] = 'Content Type';
$_LANG['addonAA']['configuration']['addAdvancedSettingsModal']['advancedSettingsSubmoduleForm']['mode']['mode'] = 'Mode';

$_LANG['addonAA']['configuration']['mainContainer']['submoduleSettingsPage']['WinSms'] = 'WinSMS';

$_LANG['addonAA']['configuration']['addAdvancedSettingsModal']['advancedSettingsSubmoduleForm']['userName']['userName'] = 'Username';
$_LANG['addonAA']['configuration']['addAdvancedSettingsModal']['advancedSettingsSubmoduleForm']['userPassword']['userPassword'] = 'Password';
$_LANG['addonAA']['configuration']['editAdvancedSettingsModal']['editAdvancedSettingsSubmoduleForm']['userName']['userName'] = 'Username';
$_LANG['addonAA']['configuration']['editAdvancedSettingsModal']['editAdvancedSettingsSubmoduleForm']['userPassword']['userPassword'] = 'Password';

$_LANG['addonAA']['configuration']['mainContainer']['submoduleSettingsPage']['OnnoRokomSms'] = 'OnnoRokom SMS';

$_LANG['addonAA']['configuration']['addAdvancedSettingsModal']['advancedSettingsSubmoduleForm']['type']['type'] = 'Type';
$_LANG['addonAA']['configuration']['addAdvancedSettingsModal']['advancedSettingsSubmoduleForm']['maskName']['maskName'] = 'Mask Name';
$_LANG['addonAA']['configuration']['addAdvancedSettingsModal']['advancedSettingsSubmoduleForm']['campaignName']['campaignName'] = 'Campaign Name';

$_LANG['addonAA']['configuration']['editAdvancedSettingsModal']['editAdvancedSettingsSubmoduleForm']['type']['type'] = 'Type';
$_LANG['addonAA']['configuration']['editAdvancedSettingsModal']['editAdvancedSettingsSubmoduleForm']['maskName']['maskName'] = 'Mask Name';
$_LANG['addonAA']['configuration']['editAdvancedSettingsModal']['editAdvancedSettingsSubmoduleForm']['campaignName']['campaignName'] = 'Campaign Name';

$_LANG['addonAA']['configuration']['editAdvancedSettingsModal']['editAdvancedSettingsSubmoduleForm']['applicationKey']['applicationKey'] = 'Application Key';
$_LANG['addonAA']['configuration']['editAdvancedSettingsModal']['editAdvancedSettingsSubmoduleForm']['applicationSecret']['applicationSecret'] = 'Application Secret';
$_LANG['addonAA']['configuration']['editAdvancedSettingsModal']['editAdvancedSettingsSubmoduleForm']['consumerKey']['consumerKey'] = 'Consumer Key';
$_LANG['addonAA']['configuration']['editAdvancedSettingsModal']['editAdvancedSettingsSubmoduleForm']['accountId']['accountId'] = 'Account ID';
$_LANG['addonAA']['configuration']['editAdvancedSettingsModal']['editAdvancedSettingsSubmoduleForm']['marketingSMS']['marketingSMS'] = 'Marketing SMS';

$_LANG['addonAA']['configuration']['editAdvancedSettingsModal']['editAdvancedSettingsSubmoduleForm']['maskName']['description'] = 'Mask Name which is allowed to your client panel.';
$_LANG['addonAA']['configuration']['addAdvancedSettingsModal']['advancedSettingsSubmoduleForm']['maskName']['description'] = 'Mask Name which is allowed to your client panel.';
$_LANG['addonAA']['configuration']['simpleSettingsSubmoduleModal']['simpleSettingsSubmoduleForm']['maskName']['description'] = 'Mask Name which is allowed to your client panel.';
$_LANG['WinSms']                = "WinSMS";
$_LANG['OvhSms']                = "OVH SMS";
$_LANG['TrioMobileSms']         = "Trio Mobile";
$_LANG['OnnoRokomSms']          = "OnnoRokom SMS";

$_LANG['SMS could not be sent. Please try again later.']    = "The SMS might not be sent. Please try again later.";
$_LANG['Please note that new activation code could be sent only once every']    = "Please note that new activation code might be sent only once every";
$_LANG['minutes.']    = 'minutes.';
$_LANG['Expiry date of the current code:']    = 'Expiry date of the current code:';
$_LANG['Verification SMS Code']    = 'Verification SMS Code';
$_LANG['Login2FA']    = 'Login';
$_LANG['SMS Center Two-Factor Authentication']    = 'SMS Center Two-Factor Authentication';
$_LANG['An Error Occurred. Please Try Again...']    = 'An Error Occurred. Please Try Again...';
$_LANG['Your mobile number is incorrect.']    = 'Your mobile number is incorrect.';
$_LANG['To begin with SMS Center 2FA simply provide your mobile number.']    = 'To begin with SMS Center 2FA simply provide your mobile number.';
$_LANG['Activate2FA']    = 'Active';
$_LANG['SMS message with activation code could not be sent. Please try again later.']    = 'SMS message with activation code could not be sent. Please try again later.';
$_LANG['Please enter the code we sent you via SMS message.']    = 'Please enter the code we sent you via SMS message.';

$_LANG['addonAA']['configuration']['modalInformation']['modal']['modalInformation'] = 'Information';
$_LANG['addonAA']['configuration']['modalInformation']['formInformation']['gatewayInformationSmsApi'] = '
Before you start configuring the gateway, make sure to choose the correct <b>endpoint</b> depending on the currency used for SMSAPI. <br>
<ul style="display: block; list-style-type: disc; margin-top: 1em; margin-bottom: 1 em; margin-left: 0; margin-right: 0; padding-left: 40px;">
    <li>For <b>EUR</b> currency it is recommended to choose <b>smsapi.com</b></li>
    <li>For <b>PLN</b> currency it is recommended to choose <b>smsapi.pl</b></li>
</ul>
<br>
If you do not have an SMSAPI account yet, you can easily create one:
<ul style="display: block; list-style-type: disc; margin-top: 1em; margin-bottom: 1 em; margin-left: 0; margin-right: 0; padding-left: 40px;">
    <li><a href="https://www.smsapi.com/signup/UNKM">Sign up here</a> for the <b>EUR</b> account settlements</li>
    <li><a href="https://www.smsapi.pl/rejestracja/JTMT">Sign up here</a> for the <b>PLN</b> account settlements</li>
</ul>

For customers referencing ModulesGarden, preferential terms are available at SMSAPI. Please <a href="mailto:t.budzynski@smsapi.pl">email the provider</a> for more information.
';
$_LANG['addonAA']['configuration']['modalInformation']['baseCancelButton']['title'] = 'Close';
$_LANG['addonAA']['configuration']['mainContainer']['submodulesPage']['buttonInformation']['button']['buttonInformation'] = 'Information';

$_LANG['addonAA']['configuration']['simpleSettingsSubmoduleModal']['simpleSettingsSubmoduleForm']['endpoint']['description'] = 'Before you start configuring the gateway, make sure to choose the correct <b>endpoint</b> depending on the currency used for SMSAPI.';

$_LANG['Api.smsapi.com']    = 'smsapi.com';
$_LANG['Api.smsapi.pl']     = 'smsapi.pl';

$_LANG['addonAA']['configuration']['editAdvancedSettingsModal']['editAdvancedSettingsSubmoduleForm']['endpoint']['description'] = 'Before you start configuring the gateway, make sure to choose the correct <b>endpoint</b> depending on the currency used for SMSAPI.';
$_LANG['Disable'] = 'Disable';
$_LANG['Enable'] = 'Enable';
/* additional langs for gateway */
$_LANG['addonAA']['configuration']['simpleSettingsSubmoduleModal']['simpleSettingsSubmoduleForm']['apikey']['apikey'] = 'API Key';
$_LANG['addonAA']['configuration']['simpleSettingsSubmoduleModal']['simpleSettingsSubmoduleForm']['operator']['operator'] = 'Operator';
$_LANG['addonAA']['configuration']['addAdvancedSettingsModal']['advancedSettingsSubmoduleForm']['apikey']['apikey'] = 'API Key';
$_LANG['addonAA']['configuration']['editAdvancedSettingsModal']['editAdvancedSettingsSubmoduleForm']['apikey']['apikey'] = 'API Key';
$_LANG['addonAA']['configuration']['editAdvancedSettingsModal']['editAdvancedSettingsSubmoduleForm']['operator']['operator'] = 'Operator';

$_LANG['addonAA']['configuration']['addAdvancedSettingsModal']['advancedSettingsSubmoduleForm']['operator']['operator'] = 'Operator';
$_LANG['addonAA']['configuration']['simpleSettingsSubmoduleModal']['simpleSettingsSubmoduleForm']['la']['la'] = 'La';
$_LANG['addonAA']['configuration']['addAdvancedSettingsModal']['advancedSettingsSubmoduleForm']['la']['la'] = 'La';
$_LANG['addonAA']['configuration']['editAdvancedSettingsModal']['editAdvancedSettingsSubmoduleForm']['la']['la'] = 'La';

$_LANG['addonAA']['configuration']['mainContainer']['submoduleSettingsPage']['HajanaOneSms'] = 'Hajana One';
$_LANG['addonAA']['configuration']['mainContainer']['submoduleSettingsPage']['MediasatSms'] = 'MEDIA SAT';
$_LANG['addonAA']['configuration']['mainContainer']['submoduleSettingsPage']['SmsPublic'] = 'SMSpubli';
$_LANG['SmsPublic'] = 'SMSpubli';

$_LANG['Windows-1250'] = 'Windows-1250';
$_LANG['Windows-1251'] = 'Windows-1251';
$_LANG['Iso-8859-1'] = 'ISO-8859-1';
$_LANG['Iso-8859-2'] = 'ISO-8859-2';
$_LANG['Iso-8859-3'] = 'ISO-8859-3';
$_LANG['Iso-8859-4'] = 'ISO-8859-4';
$_LANG['Iso-8859-5'] = 'ISO-8859-5';
$_LANG['Iso-8859-7'] = 'ISO-8859-7';
$_LANG['Utf-8'] = 'UTF-8';
$_LANG['addonAA']['configuration']['editAdvancedSettingsModal']['editAdvancedSettingsSubmoduleForm']['encodingChar']['encodingChar'] = 'Characters Encoding';


$_LANG['addonAA']['configuration']['editAdvancedSettingsModal']['editAdvancedSettingsSubmoduleForm']['apiToken']['description'] = 'OAuth token used to authenticate in SMSAPI system.';
$_LANG['addonAA']['configuration']['editAdvancedSettingsModal']['editAdvancedSettingsSubmoduleForm']['sender']['description'] = 'By default, the sender name is set to „INFO”. Only verified names are accepted. The sender name can be set after logging into SMSAPI web panel in the Sender Names section.';
$_LANG['addonAA']['configuration']['editAdvancedSettingsModal']['editAdvancedSettingsSubmoduleForm']['unicode']['description'] = 'When disabled, it prevents from sending messages containing special characters. ERROR: 11 is returned if the message contains any special characters.';
$_LANG['addonAA']['configuration']['editAdvancedSettingsModal']['editAdvancedSettingsSubmoduleForm']['normalize']['normalize'] = 'Normalize Characters';
$_LANG['addonAA']['configuration']['editAdvancedSettingsModal']['editAdvancedSettingsSubmoduleForm']['normalize']['description'] = 'When enabled, special characters in the message are replaced with their equivalents (e.g. ą-a, ć-c, ê-e, ę-e, ñ-n, ý-y)';
$_LANG['addonAA']['configuration']['editAdvancedSettingsModal']['editAdvancedSettingsSubmoduleForm']['fast']['fast'] = 'Fast Priority';
$_LANG['addonAA']['configuration']['editAdvancedSettingsModal']['editAdvancedSettingsSubmoduleForm']['fast']['description'] = 'When enabled, the message is sent with the highest priority which ensures the quickest possible time of delivery. Fast message costs 50% more than a standard one. Attention! Mass and marketing messages cannot be sent with fast parameter. In case of sending the message to more than one recipient in a single request, messages are always sent as standard ones.';
$_LANG['addonAA']['configuration']['editAdvancedSettingsModal']['editAdvancedSettingsSubmoduleForm']['testmode']['testmode'] = 'Test Mode';
$_LANG['addonAA']['configuration']['editAdvancedSettingsModal']['editAdvancedSettingsSubmoduleForm']['testmode']['description'] = 'When enabled, the messages are not sent but the responses are still provided. Test messages are not charged.';


$_LANG['addonAA']['configuration']['addAdvancedSettingsModal']['advancedSettingsSubmoduleForm']['apiToken']['description'] = 'OAuth token used to authenticate in SMSAPI system.';
$_LANG['addonAA']['configuration']['addAdvancedSettingsModal']['advancedSettingsSubmoduleForm']['sender']['description'] = 'By default, the sender name is set to „INFO”. Only verified names are accepted. The sender name can be set after logging into SMSAPI web panel in the Sender Names section.';
$_LANG['addonAA']['configuration']['addAdvancedSettingsModal']['advancedSettingsSubmoduleForm']['unicode']['description'] = 'When disabled, it prevents from sending messages containing special characters. ERROR: 11 is returned if the message contains any special characters.';
$_LANG['addonAA']['configuration']['addAdvancedSettingsModal']['advancedSettingsSubmoduleForm']['normalize']['normalize'] = 'Normalize Characters';
$_LANG['addonAA']['configuration']['addAdvancedSettingsModal']['advancedSettingsSubmoduleForm']['normalize']['description'] = 'When enabled, special characters in the message are replaced with their equivalents (e.g. ą-a, ć-c, ê-e, ę-e, ñ-n, ý-y)';
$_LANG['addonAA']['configuration']['addAdvancedSettingsModal']['advancedSettingsSubmoduleForm']['fast']['fast'] = 'Fast Priority';
$_LANG['addonAA']['configuration']['addAdvancedSettingsModal']['advancedSettingsSubmoduleForm']['fast']['description'] = 'When enabled, the message is sent with the highest priority which ensures the quickest possible time of delivery. Fast message costs 50% more than a standard one. Attention! Mass and marketing messages cannot be sent with fast parameter. In case of sending the message to more than one recipient in a single request, messages are always sent as standard ones.';
$_LANG['addonAA']['configuration']['addAdvancedSettingsModal']['advancedSettingsSubmoduleForm']['testmode']['testmode'] = 'Test Mode';
$_LANG['addonAA']['configuration']['addAdvancedSettingsModal']['advancedSettingsSubmoduleForm']['testmode']['description'] = 'When enabled, the messages are not sent but the responses are still provided. Test messages are not charged.';

$_LANG['addonAA']['configuration']['addAdvancedSettingsModal']['advancedSettingsSubmoduleForm']['endpoint']['description'] = 'Before you start configuring the gateway, make sure to choose the correct <b>endpoint</b> depending on the currency used for SMSAPI.';
$_LANG['addonAA']['configuration']['addAdvancedSettingsModal']['advancedSettingsSubmoduleForm']['encodingChar']['encodingChar'] = 'Characters Encoding';;
$_LANG['addonAA']['configuration']['editAdvancedSettingsModal']['editAdvancedSettingsSubmoduleForm']['encodingChar']['description'] = 'Parameter that describes the text message encoding. It is recommended to use UTF-8 by default. Change it if you encounter problems with encoding characters in your language.';
$_LANG['addonAA']['configuration']['addAdvancedSettingsModal']['advancedSettingsSubmoduleForm']['encodingChar']['description'] = 'Parameter that describes the text message encoding. It is recommended to use UTF-8 by default. Change it if you encounter problems with encoding characters in your language.';


$_LANG['addonAA']['configuration']['simpleSettingsSubmoduleModal']['simpleSettingsSubmoduleForm']['apiToken']['description'] = 'OAuth token used to authenticate in SMSAPI system.';
$_LANG['addonAA']['configuration']['simpleSettingsSubmoduleModal']['simpleSettingsSubmoduleForm']['sender']['description'] = 'By default, the sender name is set to „INFO”. Only verified names are accepted. The sender name can be set after logging into SMSAPI web panel in the Sender Names section.';
$_LANG['addonAA']['configuration']['simpleSettingsSubmoduleModal']['simpleSettingsSubmoduleForm']['unicode']['description'] = 'When disabled, it prevents from sending messages containing special characters. ERROR: 11 is returned if the message contains any special characters.';
$_LANG['addonAA']['configuration']['simpleSettingsSubmoduleModal']['simpleSettingsSubmoduleForm']['normalize']['normalize'] = 'Normalize Characters';
$_LANG['addonAA']['configuration']['simpleSettingsSubmoduleModal']['simpleSettingsSubmoduleForm']['normalize']['description'] = 'When enabled, special characters in the message are replaced with their equivalents (e.g. ą-a, ć-c, ê-e, ę-e, ñ-n, ý-y)';
$_LANG['addonAA']['configuration']['simpleSettingsSubmoduleModal']['simpleSettingsSubmoduleForm']['fast']['fast'] = 'Fast Priority';
$_LANG['addonAA']['configuration']['simpleSettingsSubmoduleModal']['simpleSettingsSubmoduleForm']['fast']['description'] = 'When enabled, the message is sent with the highest priority which ensures the quickest possible time of delivery. Fast message costs 50% more than a standard one. Attention! Mass and marketing messages cannot be sent with fast parameter. In case of sending the message to more than one recipient in a single request, messages are always sent as standard ones.';
$_LANG['addonAA']['configuration']['simpleSettingsSubmoduleModal']['simpleSettingsSubmoduleForm']['testmode']['testmode'] = 'Test Mode';
$_LANG['addonAA']['configuration']['simpleSettingsSubmoduleModal']['simpleSettingsSubmoduleForm']['testmode']['description'] = 'When enabled, the messages are not sent but the responses are still provided. Test messages are not charged.';
$_LANG['addonAA']['configuration']['simpleSettingsSubmoduleModal']['simpleSettingsSubmoduleForm']['encodingChar']['encodingChar'] = 'Characters Encoding';;
$_LANG['addonAA']['configuration']['simpleSettingsSubmoduleModal']['simpleSettingsSubmoduleForm']['encodingChar']['description'] = 'Parameter that describes the text message encoding. It is recommended to use UTF-8 by default. Change it if you encounter problems with encoding characters in your language.';
$_LANG['addonAA']['configuration']['simpleSettingsSubmoduleModal']['simpleSettingsSubmoduleForm']['message_type']['message_type'] = 'Message Type';


$_LANG['2FAnotEnabled']      = 'The 2 Factor Authentication cannot be enabled. Make sure if the module is active and a rule is enabled.';

/**
 * Disable Queue Admin Area
 */

$_LANG['addonAA']['configuration']['mainContainer']['generalPage']['leftSide']['disableQueueTitle'] = 'Skip Queue';
$_LANG['addonAA']['configuration']['mainContainer']['generalPage']['leftSide']['disableQueue']['clientProfileDisableQueue']['clientProfileDisableQueue'] = 'Client Profile';
$_LANG['addonAA']['configuration']['mainContainer']['generalPage']['leftSide']['disableQueue']['clientProfileDisableQueue']['description'] = "If you enable, messages sent from the client's profile will skip the queue.";
$_LANG['addonAA']['configuration']['mainContainer']['submoduleSettingsPage']['MimSms'] = 'MiM SMS-eSMS';
$_LANG['addonAA']['configuration']['mainContainer']['submoduleSettingsPage']['SmsPortal'] = 'SMSPortal';

$_LANG['addonAA']['configuration']['addAdvancedSettingsModal']['advancedSettingsSubmoduleForm']['eapiUrl']['eapiUrl'] = 'Eapi URL';
$_LANG['addonAA']['configuration']['addAdvancedSettingsModal']['advancedSettingsSubmoduleForm']['eapiUrl']['description'] = 'Set EAPI URL from your BulkSMS profile page. If the field is left blank, it will use the default address \'http://bulksms.vsms.net/eapi/\'';
$_LANG['addonAA']['configuration']['editAdvancedSettingsModal']['editAdvancedSettingsSubmoduleForm']['eapiUrl']['eapiUrl'] = 'Eapi URL';
$_LANG['addonAA']['configuration']['editAdvancedSettingsModal']['editAdvancedSettingsSubmoduleForm']['eapiUrl']['description'] = 'Set EAPI URL from your BulkSMS profile page. If the field is left blank, it will use the default address \'http://bulksms.vsms.net/eapi/\'';

$_LANG['addonAA']['configuration']['mainContainer']['submoduleSettingsPage']['BulkSmsRest'] = 'BulkSMS Rest API';
$_LANG['addonAA']['configuration']['editAdvancedSettingsModal']['editAdvancedSettingsSubmoduleForm']['secure']['secure'] = 'Secure';
$_LANG['addonAA']['configuration']['addAdvancedSettingsModal']['advancedSettingsSubmoduleForm']['secure']['secure'] = 'Secure';
$_LANG['addonAA']['configuration']['simpleSettingsSubmoduleModal']['simpleSettingsSubmoduleForm']['secure']['secure'] = 'Secure';
$_LANG['addonAA']['configuration']['mainContainer']['submoduleSettingsPage']['MobiShastra'] = 'Mobishastra';
$_LANG['addonAA']['configuration']['mainContainer']['submoduleSettingsPage']['Jawaly4'] = '4jawaly';

//Invoice Activation
$_LANG['addonAA']['configuration']['mainContainer']['generalPage']['leftSide']['invoiceActivationTitle']                                                                            = 'Invoice Activation';
$_LANG['addonAA']['configuration']['mainContainer']['generalPage']['leftSide']['invoiceActivation']['invoiceWithoutOrderActivation']['InvoiceWithoutOrderActivation']               = 'Invoice Without Order Activation';
$_LANG['addonAA']['configuration']['mainContainer']['generalPage']['leftSide']['invoiceActivation']['invoiceWithoutOrderActivation']['description']                                 = 'If enabled, a client will be able to check an invoice that is not assigned to any order only by the token delivered via SMS.';


$_LANG['invitationClientIdEmpty']   = 'Invitation has no Client ID declared.';
$_LANG['invitationInvitedByEmpty']  = 'Invitation has no Invited By ID declared.';