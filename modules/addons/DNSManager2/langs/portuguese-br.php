<?php

$_LANG['token'] = ', Erro de Token:';
$_LANG['generalError'] = 'Parece que você encontrou um erro inesperado. Entre em contato com a equipe de suporte do ModulesGarden.';

$_LANG['manage_dns'] = 'Gerenciar DNS';
$_LANG['mg_manage_dns_link'] = 'Gerenciar DNS';

$_LANG['addonAA']['datatables']['next'] = 'Próximo';
$_LANG['addonAA']['datatables']['previous'] = 'Próximo';
$_LANG['addonAA']['datatables']['zeroRecords'] = 'Nada para exibir';
$_LANG['addonAA']['datatables']['sEmptyTable'] = "Não há dados disponíveis";
$_LANG['addonAA']['datatables']['sPrevious'] = "Anterior";
$_LANG['addonAA']['datatables']['sNext'] = "Próximo";
$_LANG['addonAA']['datatables']['sZeroRecords'] = "Não há registros a serem exibidos";
$_LANG['addonAA']['datatables']['sInfoEmpty'] = "Nenhum registro correspondente encontrado"; 
$_LANG['addonAA']['datatables']['sSearch'] = "Buscar";
$_LANG['addonAA']['datatables']['sLengthMenu'] = "Mostrar _MENU_ entradas";
$_LANG['addonAA']['datatables']['sInfoFiltered'] = " (filtrada de _MAX_ total entrada)";
$_LANG['addonAA']['datatables']['sInfo'] = "Tem um total de _TOTAL_ entradas para mostrar (_START_ para _END_)";

$_LANG['addonAA']['pagesLabels']['home']['label']           = 'Principal';
$_LANG['addonAA']['pagesLabels']['dashboard']['label']      = 'Dashboard';
$_LANG['addonAA']['pagesLabels']['settings']['servers'] = 'Servidores';
$_LANG['addonAA']['pagesLabels']['zones']['label'] = 'Lista de Zonas';
$_LANG['addonAA']['pagesLabels']['settings']['label'] = 'Configurações';
$_LANG['addonAA']['pagesLabels']['settings']['global'] = 'Configurações Globais';
$_LANG['addonAA']['pagesLabels']['settings']['packages'] = 'Pacotes';
$_LANG['addonAA']['pagesLabels']['settings']['sets'] = 'Conjuntos de Registros DNS';
$_LANG['addonAA']['pagesLabels']['settings']['logs'] = 'Logs';
$_LANG['addonAA']['pagesLabels']['tools']['label'] = 'Ferramentas';
$_LANG['addonAA']['pagesLabels']['tools']['cron'] = "Cron";
$_LANG['addonAA']['pagesLabels']['tools']['migration'] = "Migração";
$_LANG['addonAA']['pagesLabels']['tools']['import'] = "Importar";
$_LANG['addonAA']['pagesLabels']['documentation']['label'] = 'Documentação';
$_LANG['addonAA']['pagesLabels']['tools']['tasks'] = "Tarefas";

$_LANG['addonAA']['confirmation_close'] = "Fechar";
$_LANG['addonAA']['confirmation_confirm'] = "Confirmar";
$_LANG['addonAA']['confirmation_default_title'] = "Confirmação";

$_LANG['addonAA']['dashboard']['servers'] = "Servidores";
$_LANG['addonAA']['dashboard']['zones_used'] = "Zonas Usadas";
$_LANG['addonAA']['dashboard']['email_send'] = "E-mails";
$_LANG['addonAA']['dashboard']['last_week'] = "Semana Passada";
$_LANG['addonAA']['dashboard']['zone_created'] = "Zonas Criadas";
$_LANG['addonAA']['dashboard']['zone_edited'] = "Zonas Alteradas";
$_LANG['addonAA']['dashboard']['zone_removed'] = "Zonas Removidas";
$_LANG['addonAA']['dashboard']['notifications'] = "Notificações";
$_LANG['addonAA']['dashboard']['notification'] = "Notificação";
$_LANG['addonAA']['dashboard']['typeh'] = "Tipo";
$_LANG['addonAA']['dashboard']['type'][0] = "Problema";
$_LANG['addonAA']['dashboard']['type'][1] = "Info";
$_LANG['addonAA']['dashboard']['type'][2] = "Problema";
$_LANG['addonAA']['dashboard']['date'] = "Data";
$_LANG['addonAA']['dashboard']['no_zone_in_use'] = "No zones are currently in use";
$_LANG['addonAA']['dashboard']['are_you_sure_that_you_want_to_hide_this_guide'] = "Are you sure that you want to hide this guide? You can turn it on again in the Global Settings section.";

$_LANG['addonAA']['zones']['add_dns_zone'] = "Add Zona DNS";
$_LANG['addonAA']['zones']['zone_client']  = "Client";
$_LANG['addonAA']['zones']['zone_service'] = "Service";
$_LANG['addonAA']['zones']['zone_name'] = "Zone Name";
$_LANG['addonAA']['zones']['ip'] = "IP Address";
$_LANG['addonAA']['zones']['selectRecordSet'] = "Select Record Set";
$_LANG['addonAA']['zones']['set_dns_record_set'] = "Change Record Set";
$_LANG['addonAA']['zones']['wipe_records'] = "Wipe Records";
$_LANG['addonAA']['zones']['select_record_set'] = "Select Record Set";
$_LANG['addonAA']['zones']['none'] = "Nada";
$_LANG['addonAA']['zones']['table']['name'] = "Nome";
$_LANG['addonAA']['zones']['table']['client'] = "Cliente";
$_LANG['addonAA']['zones']['table']['type'] = "Tipo";
$_LANG['addonAA']['zones']['table']['server'] = "Servidor";
$_LANG['addonAA']['zones']['table']['status'] = "Status";
$_LANG['addonAA']['zones']['edit'] = "Editar";
$_LANG['addonAA']['zones']['synchronize'] = "Sincronizar";
$_LANG['addonAA']['zones']['create'] = "Criar";
$_LANG['addonAA']['zones']['remove'] = "Excluir";
$_LANG['addonAA']['zones']['remove_from_whmcs_only'] = "Remover apenas do WHMCS";
$_LANG['addonAA']['zones']['close'] = "Fechar";
$_LANG['addonAA']['zones']['edit_zone'] = "Editar Zona";
$_LANG['addonAA']['zones']['save_changes'] = "Salvar Alterações";
$_LANG['addonAA']['zones']['zone_already_taken'] = "A zona selecionada já foi tomada";

$_LANG['addonAA']['zones']['infos']['zone_created_successfully'] = "A new zone has been created successfully";
$_LANG['addonAA']['zones']['infos']['set_added_successfully'] = "The selected record has been changed successfully";
$_LANG['addonAA']['zones']['infos']['changes_saved'] = "Changes have been saved successfully";
$_LANG['addonAA']['zones']['infos']['zone_synchronized'] = "The selected zone has been synchronized successfully";
$_LANG['addonAA']['zones']['infos']['zone_created'] = "The selected zone has been created successfully";
$_LANG['addonAA']['zones']['infos']['zone_removed_from_server'] = "The selected zone has been successfully deleted from the server";
$_LANG['addonAA']['zones']['infos']['zone_removed_from_whmcs'] = "The selected zone has been successfully removed from WHMCS";

$_LANG['addonAA']['zones']['remove_record'] = "Remover Registro";
$_LANG['addonAA']['zones']['add_new_record'] = "Add Novo Registro";
$_LANG['addonAA']['zones']['zone_type']['0'] = "-";
$_LANG['addonAA']['zones']['zone_type']['1'] = "Domínio";
$_LANG['addonAA']['zones']['zone_type']['2'] = "Hospedagem";
$_LANG['addonAA']['zones']['zone_type']['3'] = "Addon";

$_LANG['addonAA']['zones']['synchronize_zone_confirm'] = "Are you sure that you want to synchronize this zone?";
$_LANG['addonAA']['zones']['add_zone_confirm'] = "Are you sure that you want to create this zone on the server?";
$_LANG['addonAA']['zones']['remove_zone_confirm'] = "Are you sure that you want to remove this zone from the server?";
$_LANG['addonAA']['zones']['remove_zone_whmcs_confirm'] = "Are you sure that you want to delete this zone from WHMCS? It will remain untouched on the server.";
$_LANG['addonAA']['zones']['remove_zone_record_confirm'] = "Are you sure that you want to remove this record? All unsaved changes will be lost.";
$_LANG['addonAA']['zones']['add_zone_record_confirm'] = "Are you sure that you want to add this record? All unsaved changes will be lost.";
$_LANG['addonAA']['zones']['there_is_no_supported_record_within_this_zone'] = "There is no supported record within this zone";

//===========================================================================================================
//===========================================RECORDS INFO====================================================
//===========================================================================================================
$_LANG['addonAA']['zones']['add_zone'] = "Add Zone";
$_LANG['addonAA']['zones']['record_info']['name'] = "Name of the owner, i.e. name of the node this resource record is related to.";
$_LANG['addonAA']['zones']['name'] = "Nome";
$_LANG['addonAA']['zones']['record_info']['ttl'] = "A 32 bit signed integer that specifies a time interval that the resource record may be cached before the source of the information should again be consulted. Zero values are interpreted to mean that the RR can only be used for a transaction in progress, and should not be cached.";
$_LANG['addonAA']['zones']['ttl'] = "TTL";
$_LANG['addonAA']['zones']['record_type_info']['MX'] = "MX records cause the type A additional section processing for the host specified by the EXCHANGE.";
$_LANG['addonAA']['zones']['record_field_info']['MX']['preference'] = "A 16 bit integer which specifies the preference given to this RR among others at the same owner. Lower values are preferred.";
$_LANG['addonAA']['zones']['record_field_info']['MX']['exchange'] = "A <domain-name> which specifies a host willing to act as a mail exchange for the owner name.";
$_LANG['addonAA']['zones']['record_type_info']['A'] = "Hosts that have multiple Internet addresses will have multiple A records.";
$_LANG['addonAA']['zones']['record_field_info']['A']['address'] = "A 32 bit Internet address.";
$_LANG['addonAA']['zones']['record_type_info']['AAAA'] = "The AAAA resource record type is a record specific to the Internet class that stores a single IPv6 address.";
$_LANG['addonAA']['zones']['record_field_info']['AAAA']['address'] = "A 128 bit IPv6 address is encoded in the data portion of an AAAA resource record in network byte order (high-order byte first).";
$_LANG['addonAA']['zones']['record_type_info']['CNAME'] = "CNAME RRs do not cause any additional section processing, but name servers may choose to restart the query at the canonical name in certain cases.";
$_LANG['addonAA']['zones']['record_field_info']['CNAME']['cname'] = "A <domain-name> which specifies a canonical or primary name for the owner. The owner name is an alias.";
$_LANG['addonAA']['zones']['record_type_info']['AFSDB'] = "An AFSDB-record maps a domain name to the AFS (Andrew File System) database server.";
$_LANG['addonAA']['zones']['record_field_info']['AFSDB']['subtype'] = "A 16 bit integer.";
$_LANG['addonAA']['zones']['record_field_info']['AFSDB']['hostname'] = "A domain name of a host that has a server for the cell named by the owner name of the RR.";
$_LANG['addonAA']['zones']['record_type_info']['DNAME'] = "A DNAME-record is used to map/rename an entire sub-tree of the DNS name space to another domain. It differs from the CNAME-record which maps only a single node of the name space.";
$_LANG['addonAA']['zones']['record_field_info']['DNAME']['target'] = "Target";
$_LANG['addonAA']['zones']['record_type_info']['NS'] = "NS records cause both the usual additional section processing to locate a type A record, and, when used in a referral, a special search of the zone in which they reside for glue information.";
$_LANG['addonAA']['zones']['record_field_info']['NS']['nsdname'] = "A <domain-name> which specifies a host which should be authoritative for the specified class and domain.";
$_LANG['addonAA']['zones']['record_type_info']['RP'] = "An RP-record specifies the mailbox of the person responsible for a host (domain name).";
$_LANG['addonAA']['zones']['record_field_info']['RP']['mbox'] = "A domain name that specifies the mailbox for the responsible person.";
$_LANG['addonAA']['zones']['record_field_info']['RP']['txtdname'] = "A domain name for which TXT RRs exist.";
$_LANG['addonAA']['zones']['record_type_info']['SRV'] = "SRV-records are used to specify the location of a service.";
$_LANG['addonAA']['zones']['record_field_info']['SRV']['priority'] = "The priority of this target host.";
$_LANG['addonAA']['zones']['record_field_info']['SRV']['weight'] = "A server selection mechanism. The weight field specifies a relative weight for entries with the same priority.";
$_LANG['addonAA']['zones']['record_field_info']['SRV']['port'] = "The port on this target host of this service. The range is 0-65535.";
$_LANG['addonAA']['zones']['record_field_info']['SRV']['target'] = "The domain name of the target host.";
$_LANG['addonAA']['zones']['record_type_info']['TXT'] = "TXT-records are used to hold a descriptive text. They are often used to hold general information about a domain name such as who is hosting it, contact person, phone numbers, etc.";
$_LANG['addonAA']['zones']['record_field_info']['TXT']['txtdata'] = "One or more <character-string>s.";
//TODO: dodaÄ‡ opsiy
$_LANG['addonAA']['zones']['record_type_info']['PTR'] = "PTR";
$_LANG['addonAA']['zones']['record_field_info']['PTR']['ptrdname'] = "PTRD name";

$_LANG['addonAA']['zones']['record_type_info']['NAPTR'] = "NAPTR";
$_LANG['addonAA']['zones']['record_field_info']['NAPTR']['order'] = "Order";
$_LANG['addonAA']['zones']['record_field_info']['NAPTR']['preference'] = "Preference";
$_LANG['addonAA']['zones']['record_field_info']['NAPTR']['flags'] = "Flags";
$_LANG['addonAA']['zones']['record_field_info']['NAPTR']['service'] = "Service";
$_LANG['addonAA']['zones']['record_field_info']['NAPTR']['regexp'] = "Regexp";
$_LANG['addonAA']['zones']['record_field_info']['NAPTR']['replacement'] = "Replacement";

$_LANG['addonAA']['zones']['record_type_info']['LOC'] = "LOC";
$_LANG['addonAA']['zones']['record_field_info']['LOC']['version'] = "Version";
$_LANG['addonAA']['zones']['record_field_info']['LOC']['size'] = "Size";
$_LANG['addonAA']['zones']['record_field_info']['LOC']['horiz_pre'] = "Horiz Pre";
$_LANG['addonAA']['zones']['record_field_info']['LOC']['vert_pre'] = "Vert Pre";
$_LANG['addonAA']['zones']['record_field_info']['LOC']['latitude'] = "Latitude";
$_LANG['addonAA']['zones']['record_field_info']['LOC']['longitude'] = "Longitude";
$_LANG['addonAA']['zones']['record_field_info']['LOC']['altitude'] = "Altitude";

$_LANG['addonAA']['zones']['record_type_info']['X25'] = "X25";
$_LANG['addonAA']['zones']['record_field_info']['X25']['psdnaddress'] = "PSDN address";
$_LANG['addonAA']['settings']['add_new_record'] = "Add New Record";

$_LANG['addonAA']['zones']['record_field_info']['CAA']['flag'] = "Flag";
$_LANG['addonAA']['zones']['record_field_info']['CAA']['tag'] = "Tag";
$_LANG['addonAA']['zones']['record_field_info']['CAA']['target'] = "Target";

$_LANG['addonAA']['zones']['record_type_info']['DS'] = "DS";
$_LANG['addonAA']['zones']['record_field_info']['DS']['keytag'] = "Keytag";
$_LANG['addonAA']['zones']['record_field_info']['DS']['algorithm'] = "Algorithm";
$_LANG['addonAA']['zones']['record_field_info']['DS']['digesttype'] = "Digesttype";
$_LANG['addonAA']['zones']['record_field_info']['DS']['digest'] = "Digest";

$_LANG['addonAA']['zones']['record_type_info']['HINFO'] = "HINFO";
$_LANG['addonAA']['zones']['record_field_info']['HINFO']['cpu'] = "Cpu";
$_LANG['addonAA']['zones']['record_field_info']['HINFO']['os'] = "Os";

$_LANG['addonAA']['zones']['record_type_info']['ISDN'] = "ISDN";
$_LANG['addonAA']['zones']['record_field_info']['ISDN']['isdnaddress'] = "ISDN address";
$_LANG['addonAA']['zones']['record_field_info']['ISDN']['sa'] = "Sa";

$_LANG['addonAA']['zones']['record_type_info']['MB'] = "MB";
$_LANG['addonAA']['zones']['record_field_info']['MB']['madname'] = "Madname";

$_LANG['addonAA']['zones']['record_type_info']['MD'] = "MD";
$_LANG['addonAA']['zones']['record_field_info']['MD']['madname'] = "Madname";

$_LANG['addonAA']['zones']['record_type_info']['MF'] = "MF";
$_LANG['addonAA']['zones']['record_field_info']['MF']['madname'] = "Madname";

$_LANG['addonAA']['zones']['record_type_info']['MG'] = "MG";
$_LANG['addonAA']['zones']['record_field_info']['MG']['mgmname'] = "Mgmname";

$_LANG['addonAA']['zones']['record_type_info']['MINFO'] = "MINFO";
$_LANG['addonAA']['zones']['record_field_info']['MINFO']['rmailbx'] = "Rmailbx";
$_LANG['addonAA']['zones']['record_field_info']['MINFO']['emailbx'] = "Emailbx";

$_LANG['addonAA']['zones']['record_type_info']['MR'] = "MR";
$_LANG['addonAA']['zones']['record_field_info']['MR']['newname'] = "Newname";

$_LANG['addonAA']['zones']['record_type_info']['SOA'] = "SOA";
$_LANG['addonAA']['zones']['record_field_info']['SOA']['mname'] = "Mname";
$_LANG['addonAA']['zones']['record_field_info']['SOA']['rname'] = "Rname";
$_LANG['addonAA']['zones']['record_field_info']['SOA']['serial'] = "Serial";
$_LANG['addonAA']['zones']['record_field_info']['SOA']['refresh'] = "Refresh";
$_LANG['addonAA']['zones']['record_field_info']['SOA']['retry'] = "Retry";
$_LANG['addonAA']['zones']['record_field_info']['SOA']['expire'] = "Expire";
$_LANG['addonAA']['zones']['record_field_info']['SOA']['minimum'] = "Minimum";
$_LANG['addonAA']['zones']['record_field_info']['SOA']['subtype'] = "Subtype";
$_LANG['addonAA']['zones']['record_field_info']['SOA']['hostname'] = "Hostname";

$_LANG['addonAA']['zones']['record_type_info']['WKS'] = "WKS";
$_LANG['addonAA']['zones']['record_field_info']['WKS']['address'] = "Address";
$_LANG['addonAA']['zones']['record_field_info']['WKS']['protocol'] = "Protocol";
$_LANG['addonAA']['zones']['record_field_info']['WKS']['bitmap'] = "Bitmap";
//===========================================================================================================
//===========================================/RECORDS INFO===================================================
//===========================================================================================================

$_LANG['addonAA']['settings']['table']['name'] = "Name";
$_LANG['addonAA']['settings']['table']['type'] = "Type";
$_LANG['addonAA']['settings']['table']['default_ip'] = "Default IP Address";
$_LANG['addonAA']['settings']['table']['rdns'] = "rDNS";
$_LANG['addonAA']['settings']['table']['multiple_ptr'] = "Multiple PTR Records";
$_LANG['addonAA']['settings']['table']['status'] = "Status";
$_LANG['addonAA']['settings']['edit'] = "Edit";
$_LANG['addonAA']['settings']['turn_on_server'] = "Turn On";
$_LANG['addonAA']['settings']['turn_off_server'] = "Turn Off";
$_LANG['addonAA']['settings']['delete'] = "Delete";
$_LANG['addonAA']['settings']['enabled'] = "Enabled";
$_LANG['addonAA']['settings']['disabled'] = "Disabled";
$_LANG['addonAA']['settings']['add_server'] = "Add Server";
$_LANG['addonAA']['settings']['close'] = "Close";
$_LANG['addonAA']['settings']['name'] = "Name";
$_LANG['addonAA']['settings']['module'] = "Module";
$_LANG['addonAA']['settings']['select_one'] = "Select One";
$_LANG['addonAA']['settings']['edit_server'] = "Edit Server";
$_LANG['addonAA']['settings']['test_connection'] = "Test Connection";
$_LANG['addonAA']['settings']['save_changes'] = "Save Changes";
$_LANG['addonAA']['settings']['allow_rdns'] = "Allow rDNS Records";
$_LANG['addonAA']['settings']['allow_multiple_ptr'] = "Allow Multiple PTR Records";
$_LANG['addonAA']['settings']['nameservers'] = "Nameservers";
$_LANG['addonAA']['settings']['nameserver'] = "Nameserver";
$_LANG['addonAA']['settings']['ip_address_of_nameserver'] = "IP Address Of Nameserver";
$_LANG['addonAA']['settings']['no_options_available'] = "No Options Available";
$_LANG['addonAA']['settings']['enable_cache'] = "Enable Cache";
$_LANG['addonAA']['settings']['cache_warning'] = "It seems that the :path: is not writable/readable. If you want to use the cache system you have to change permissions of this directory.";
$_LANG['addonAA']['settings']['ns_populate'] = 'Populate Nameservers';
$_LANG['addonAA']['settings']['overwrite_soa'] = 'Overwrite SOA Record';

$_LANG['addonAA']['settings']['templates'] = "Templates";
$_LANG['addonAA']['settings']['client_area'] = "Client Area";
$_LANG['addonAA']['settings']['default_template'] = "Default Template";
$_LANG['addonAA']['settings']['client_can_switch_template'] = "Switch Templates";
$_LANG['addonAA']['settings']['link_to_dns_management'] = "DNS Management Link";
$_LANG['addonAA']['settings']['logs_and_notifications'] = "Logs And Notifications";
$_LANG['addonAA']['settings']['delete_logs_older_than'] = "Delete Logs Older Than";
$_LANG['addonAA']['settings']['delete_notifications_older_than'] = "Delete Notifications Older Than";
$_LANG['addonAA']['settings']['log_successful_actions'] = "Log Successful Actions";
$_LANG['addonAA']['settings']['notifications'] = "Notifications";
$_LANG['addonAA']['settings']['configure_emails_send'] = "Delivered Emails";
$_LANG['addonAA']['settings']['client_notifications'] = "Client Notifications";
$_LANG['addonAA']['settings']['zone_created'] = "Zone Created";
$_LANG['addonAA']['settings']['zone_altered'] = "Zone Altered";
$_LANG['addonAA']['settings']['zone_removed'] = "Zone Removed";
$_LANG['addonAA']['settings']['admin_notifications'] = "Administrator Notifications";
$_LANG['addonAA']['settings']['save'] = "Save";

$_LANG['addonAA']['settings']['days']['1'] = "1 Day";
$_LANG['addonAA']['settings']['days']['2'] = "2 Days";
$_LANG['addonAA']['settings']['days']['3'] = "3 Days";
$_LANG['addonAA']['settings']['days']['4'] = "4 Days";
$_LANG['addonAA']['settings']['days']['5'] = "5 Days";
$_LANG['addonAA']['settings']['days']['6'] = "6 Days";
$_LANG['addonAA']['settings']['days']['7'] = "1 Week";
$_LANG['addonAA']['settings']['days']['14'] = "2 Weeks";
$_LANG['addonAA']['settings']['days']['21'] = "3 Weeks";
$_LANG['addonAA']['settings']['days']['30'] = "1 Month";
$_LANG['addonAA']['settings']['days']['60'] = "2 Months";
$_LANG['addonAA']['settings']['days']['90'] = "3 Months";
$_LANG['addonAA']['settings']['days']['180'] = "6 Months";

$_LANG['addonAA']['settings']['table']['name'] = "Name";
$_LANG['addonAA']['settings']['table']['client'] = "Client";
$_LANG['addonAA']['settings']['table']['action'] = "Action";
$_LANG['addonAA']['settings']['table']['status'] = "Status";
$_LANG['addonAA']['settings']['table']['value'] = "Value";
$_LANG['addonAA']['settings']['table']['date'] = "Date";

$_LANG['addonAA']['settings']['table']['dns_records'] = "DNS Records";
$_LANG['addonAA']['settings']['table']['assigned_to'] = "Assigned";
$_LANG['addonAA']['settings']['add_set'] = "Add Set";
$_LANG['addonAA']['settings']['edit'] = "Edit";
$_LANG['addonAA']['settings']['remove'] = "Remove";
$_LANG['addonAA']['settings']['close'] = "Close";
$_LANG['addonAA']['settings']['add'] = "Add";
$_LANG['addonAA']['settings']['set_edit'] = "Edit Set";
$_LANG['addonAA']['settings']['save_changes'] = "Save Changes";
$_LANG['addonAA']['settings']['name'] = "Name";

$_LANG['addonAA']['settings']['table']['servers'] = "Servers";
$_LANG['addonAA']['settings']['add_package'] = "Add Package";
$_LANG['addonAA']['settings']['domains'] = "Domains";
$_LANG['addonAA']['settings']['hostings'] = "Hostings";
$_LANG['addonAA']['settings']['addons'] = "Addons";
$_LANG['addonAA']['settings']['enabled'] = "Enabled";
$_LANG['addonAA']['settings']['disable'] = "Disable";
$_LANG['addonAA']['settings']['disabled'] = "Disabled";
$_LANG['addonAA']['settings']['enable'] = "Enable";

$_LANG['addonAA']['settings']['general'] = "General";
$_LANG['addonAA']['settings']['items'] = "Items";
$_LANG['addonAA']['settings']['servers'] = "Servers";
$_LANG['addonAA']['settings']['Records'] = "Records";
$_LANG['addonAA']['settings']['own_zones_per_item'] = "Zones Limit";
$_LANG['addonAA']['settings']['enable_rdns'] = "Enable rDNS Records";
$_LANG['addonAA']['settings']['allow_multiple_ptr'] = "Allow Multiple PTR Records";
$_LANG['addonAA']['settings']['zone_creation_automation'] = "Zone Creation Automation";
$_LANG['addonAA']['settings']['new_item'] = "New Item";
$_LANG['addonAA']['settings']['pending_transfer_domain'] = "Pending Domain Transfer";
$_LANG['addonAA']['settings']['pending_registration_domain'] = "Pending Domain Registration";
$_LANG['addonAA']['settings']['domain_with_dns_management'] = "Domain With DNS Management";
$_LANG['addonAA']['settings']['update_existed_zone'] = "Allow Update Of Existing Zone";
$_LANG['addonAA']['settings']['products'] = "Products";
$_LANG['addonAA']['settings']['products_addons'] = "Product Addons";
$_LANG['addonAA']['settings']['type'] = "Type";
$_LANG['addonAA']['settings']['packages'] = "Package";
$_LANG['addonAA']['settings']['lang'] = "Lang";
$_LANG['addonAA']['settings']['multiple_ptr'] = "Multiple PTR Records";
$_LANG['addonAA']['settings']['status'] = "Status";
$_LANG['addonAA']['settings']['rdns'] = "rDNS";
$_LANG['addonAA']['settings']['edit_package'] = "Edit Package";
$_LANG['addonAA']['settings']['restrictions'] = "Restrictions";
$_LANG['addonAA']['settings']['allow_rdns_records'] = "Allow rDNS Records";
$_LANG['addonAA']['settings']['record_limit'] = "Record Limit";
$_LANG['addonAA']['settings']['dns_record_sets'] = "DNS Record Sets";
$_LANG['addonAA']['settings']['default_dns_record_set'] = "Default DNS Record Set";
$_LANG['addonAA']['settings']['none'] = "None";
$_LANG['addonAA']['settings']['client_can_use_sets'] = "Client Can Use Sets";
$_LANG['addonAA']['settings']['allowed_sets'] = "Allowed Sets";
$_LANG['addonAA']['settings']['modify'] = "Modify";
$_LANG['addonAA']['settings']['exclude'] = "Exclude";
$_LANG['addonAA']['settings']['cron_synchronization'] = "Cron Synchronization";
$_LANG['addonAA']['settings']['cron_migrator'] = "Cron Migrator";
$_LANG['addonAA']['settings']['cron_cleaner'] = "Cron Cleaner";
$_LANG['addonAA']['settings']['allow_record_types'] = "Allowed Record Types";
$_LANG['addonAA']['settings']['cron_synchronizator'] = "Cron Synchronization";
$_LANG['addonAA']['settings']['include'] = "Include";
$_LANG['addonAA']['settings']['admins'] = "Administrators";
$_LANG['addonAA']['settings']['delete_all_logs'] = "Delete All Logs";
$_LANG['addonAA']['settings']['already_in_package'] = "Already In Package: ";
$_LANG['addonAA']['settings']['there_is_no_server_added_to_this_package'] = "There are no servers added to this package";
$_LANG['addonAA']['settings']['there_is_no_record_within_this_set'] = "There are no records within this set";
$_LANG['addonAA']['settings']['other'] = "Other";
$_LANG['addonAA']['settings']['disable_tutorial'] = "Hide Guide";
$_LANG['addonAA']['settings']['client_area_status'] = "Client Area Zones Status";
$_LANG['addonAA']['settings']['soa_is_not_ns_status'] = "SOA Warning";
$_LANG['addonAA']['settings']['whois_missing_status'] = "Available Domain";
$_LANG['addonAA']['settings']['is_ok_status'] = "Active";
$_LANG['addonAA']['settings']['status_is_pending'] = "Pending";
$_LANG['addonAA']['settings']['status_content'] = "Status Content";
$_LANG['addonAA']['settings']['soa_is_not_ns_status_text'] = 'The domain {$domain} is not active. In order to activate it, change nameservers for the domain. You can do this either by redelegating the domain at the domain registrar, or you can transfer the domain to us.';
$_LANG['addonAA']['settings']['whois_missing_status_text'] = 'The domain {$domain} is still not registered.';
$_LANG['addonAA']['settings']['is_ok_status_text']         = 'The domain {$domain} is active.';
$_LANG['addonAA']['settings']['is_pending_status_text']    = 'We are still analyzing the {$domain} domain. It can take up to 15 minutes to receive the zone status. You can proceed to create records below.';

$_LANG['addonAA']['tools']['info'] = "Information";
$_LANG['addonAA']['tools']['cron_job'] = "Cron Job";
$_LANG['addonAA']['tools']['recommended_run_time'] = "Suggested Interval";
$_LANG['addonAA']['tools']['each_5_minutes'] = "Every 5 Minutes";
$_LANG['addonAA']['tools']['last_run_time'] = "Last Execution";
$_LANG['addonAA']['tools']['minutes_ago'] = "Minutes Ago";
$_LANG['addonAA']['tools']['run_time_period'] = "Execution Period";
$_LANG['addonAA']['tools']['minutes'] = "Minutes";
$_LANG['addonAA']['tools']['cron_migration'] = "Cron Migration";
$_LANG['addonAA']['tools']['migrate_zones'] = "Migrate Zones";
$_LANG['addonAA']['tools']['rune_each'] = "Run Every";
$_LANG['addonAA']['tools']['zones_per_run'] = "Zones Per Execution";
$_LANG['addonAA']['tools']['cron_importer'] = "Cron Importer";
$_LANG['addonAA']['tools']['import_zones_from_external_server'] = "Import Zones From External Server";
$_LANG['addonAA']['tools']['cron_sync'] = "Cron Sync";
$_LANG['addonAA']['tools']['synchronize_your_zones'] = "Synchronize Zones";
$_LANG['addonAA']['tools']['recheck_zones'] = "Recheck Zones";
$_LANG['addonAA']['tools']['cron_cleaner'] = "Cron Cleaner";
$_LANG['addonAA']['tools']['cron_cleaner_warning'] = "The Cron Cleaner is enabled. Zones will be removed if the Notify Only option is disabled.";
$_LANG['addonAA']['tools']['clear_the_mess'] = "Delete Unused Elements";
$_LANG['addonAA']['tools']['remove_zone_if'] = "Remove Zones:";
$_LANG['addonAA']['tools']['no_longer_match_any_package'] = "Not Included In Package";
$_LANG['addonAA']['tools']['unmodified_longer_than'] = "Not Modified For";
$_LANG['addonAA']['tools']['domain_does_not_exist_whois'] = "Not Found In WHOIS";
$_LANG['addonAA']['tools']['does_not_have_at_least_one'] = "Without Records";
$_LANG['addonAA']['tools']['remove_ptr_record_if'] = "Remove PTR Records:";
$_LANG['addonAA']['tools']['service_is_terminated_canceled'] = "Suspended/Terminated/Canceled Service";
$_LANG['addonAA']['tools']['save'] = "Save Changes";
$_LANG['addonAA']['tools']['every'] = "Every";

$_LANG['addonAA']['tools']['migration'] = "Migration";
$_LANG['addonAA']['tools']['zones_moved_total'] = "Total Number Of Zones Moved";
$_LANG['addonAA']['tools']['progress'] = "Progress";
$_LANG['addonAA']['tools']['there_are_no_migration'] = "There are no migrations yet";
$_LANG['addonAA']['tools']['schedule_migration'] = "Schedule Migration";

$_LANG['addonAA']['tools']['import'] = "Import";
$_LANG['addonAA']['tools']['zones_imported_total'] = "Total Number Of Zones Imported";
$_LANG['addonAA']['tools']['there_are_no_import'] = "There are no imports yet";
$_LANG['addonAA']['tools']['schedule_import'] = "Schedule Import";

$_LANG['addonAA']['tools']['addExportToFile'] = "Export To File";
$_LANG['addonAA']['tools']['add_import'] = "Add Import";
$_LANG['addonAA']['tools']['close'] = "Close";
$_LANG['addonAA']['tools']['add_migration'] = "Add Migration";
$_LANG['addonAA']['tools']['from_server'] = "From Server";
$_LANG['addonAA']['tools']['to_server'] = "To Server";
$_LANG['addonAA']['tools']['add'] = "Add";
$_LANG['addonAA']['tools']['no_server_available'] = "No Server Available";

$_LANG['addonAA']['tools']['infos']['new_task_added'] = "A new task has been added successfully";
$_LANG['addonAA']['tools']['fetching_list'] = "Fetching List";
$_LANG['addonAA']['tools']['remove_import_confirmation'] = "Are you sure that you want to cancel this import? Already imported zones will not be removed from the client accounts.";
$_LANG['addonAA']['tools']['remove_migration_confirmation'] = "Are you sure that you want to cancel this migration? Already migrated zones will not be removed from the target server.";
$_LANG['addonAA']['tools']['cancel'] = "Cancel";
$_LANG['addonAA']['tools']['show_list'] = "Show List";

$_LANG['addonAA']['tools']['zone'] = "Zone";
$_LANG['addonAA']['tools']['ip'] = "IP Address";
$_LANG['addonAA']['tools']['status'] = "Status";
$_LANG['addonAA']['tools']['client'] = "Client";
$_LANG['addonAA']['tools']['there_is_no_zone_to_import'] = "There Are No Zones To Import";
$_LANG['addonAA']['tools']['no_clients_available'] = "No Clients Available";
$_LANG['addonAA']['tools']['import_zone'] = "Import Zone";
$_LANG['addonAA']['tools']['import_zones'] = "Import Zones";
$_LANG['addonAA']['tools']['you_cannot_edit_this_zone_because_it_is_terminated_on_server'] = "You cannot edit this zone because it is terminated on the server";
$_LANG['addonAA']['zones']['you_cannot_edit_this_zone_because_it_is_terminated_on_server'] = "You cannot edit this zone because it is terminated on the server";

$_LANG['addonAA']['settings']['turn_on_server_confirmation'] = "Are you sure that you want to turn on this server?";
$_LANG['addonAA']['settings']['turn_off_server_confirmation'] = "Are you sure that you want to turn off this server";
$_LANG['addonAA']['settings']['delete_server_confirmation'] = "Are you sure that you want to remove this server?";

$_LANG['addonAA']['settings']['disable_package_confirmation'] = "Are you sure that you want to disable this package?";
$_LANG['addonAA']['settings']['enable_package_confirmation'] = "Are you sure that you want to enable this package?";
$_LANG['addonAA']['settings']['remove_package_confirmation'] = "Are you sure that you want to remove this package?";

$_LANG['addonAA']['settings']['remove_set_confirmation_body'] = "Are you sure that you want to remove this set?";

$_LANG['addonAA']['settings']['remove_all_logs_confirmation'] = "Are you sure that you want to remove all logs?";

$_LANG['addonAA']['zones']['cannot_find_class'] = "Cannot find a class";
$_LANG['addonAA']['zones']['errors']['select_one_at_least'] = "You have to select at least one zone";

$_LANG['addonAA']['settings']['infos']['connection_success'] = "The server connection has been established successfully";
$_LANG['addonAA']['settings']['infos']['new_server_added'] = "A new server has been added successfully";
$_LANG['addonAA']['settings']['infos']['changes_saved'] = "Changes have been saved successfully";
$_LANG['addonAA']['settings']['infos']['server_removed'] = "The server has been removed successfully";
$_LANG['addonAA']['settings']['infos']['server_turned_on'] = "The server has been turned on successfully";
$_LANG['addonAA']['settings']['infos']['server_turned_off'] = "The server has been turned off successfully";
$_LANG['addonAA']['settings']['errors']['module_is_unsupported'] = "The module :module: is not supported";

$_LANG['addonAA']['settings']['infos']['changes_saved'] = "Changes have been saved successfully";
$_LANG['addonAA']['settings']['infos']['new_package_added'] = "A new package has been added successfully";
$_LANG['addonAA']['settings']['infos']['package_removed'] = "The selected package has been removed successfully";
$_LANG['addonAA']['settings']['infos']['package_enabled'] = "The selected package has been enabled successfully";
$_LANG['addonAA']['settings']['infos']['package_disabled'] = "The selected package has been disabled successfully";
$_LANG['addonAA']['settings']['infos']['set_added'] = "The selected set has been added successfully";
$_LANG['addonAA']['settings']['infos']['set_removed'] = "The selected set has been removed successfully";
$_LANG['addonAA']['settings']['infos']['all_logs_removed'] = "All logs have been removed successfully ";
$_LANG['addonAA']['settings']['error']['cannot_find_class'] = "The class :class: cannot be found";

$_LANG['addonAA']['tools']['infos']['import_removed'] = "The selected import has been canceled successfully";
$_LANG['addonAA']['tools']['infos']['changes_saved'] = "Changes have been saved successfully";
$_LANG['addonAA']['tools']['infos']['cron_cleaner_warning'] = "The Cron Cleaner is enabled. Zones will be removed if the Notify Only option is disabled.";
$_LANG['addonAA']['zones']['add'] = "Add";

$_LANG['addonAA']['tools']['select_minutes']['0'] = "Disabled";
$_LANG['addonAA']['tools']['select_minutes']['5'] = "5 minutes";
$_LANG['addonAA']['tools']['select_minutes']['10'] = "10 minutes";
$_LANG['addonAA']['tools']['select_minutes']['15'] = "15 minutes";
$_LANG['addonAA']['tools']['select_minutes']['20'] = "20 minutes";
$_LANG['addonAA']['tools']['select_minutes']['40'] = "40 minutes";
$_LANG['addonAA']['tools']['select_minutes']['60'] = "1 hour";
$_LANG['addonAA']['tools']['select_minutes']['120'] = "2 hours";
$_LANG['addonAA']['tools']['select_minutes']['180'] = "3 hours";
$_LANG['addonAA']['tools']['select_minutes']['360'] = "6 hours";
$_LANG['addonAA']['tools']['select_minutes']['720'] = "12 hours";
$_LANG['addonAA']['tools']['select_minutes']['1440'] = "1 day";
$_LANG['addonAA']['tools']['select_minutes']['2880'] = "2 days";
$_LANG['addonAA']['tools']['select_minutes']['4320'] = "3 days";
$_LANG['addonAA']['tools']['select_minutes']['10080'] = "7 days";
$_LANG['addonAA']['tools']['select_minutes']['43200'] = "1 month";

$_LANG['addonAA']['zones']['cancel'] = "Cancel";
$_LANG['addonAA']['settings']['errors']['server_module_cannot_be_empty'] = "The server module cannot be empty";
$_LANG['addonAA']['settings']['errors']['server_name_cannot_be_empty'] = "The server name cannot be empty";

$_LANG['addonAA']['settings']['general'] = "General";
$_LANG['addonAA']['settings']['configuration'] = "Configuration";

$_LANG['addonAA']['settings']['enable_dnssec']  =   'Enable DNSSEC';

$_LANG['addonAA']['tools']['import_now'] = "Import Now";
$_LANG['addonAA']['tools']['add_migration'] = "Add Migration";
$_LANG['addonAA']['tools']['errors']['you_have_to_choose_two_diffrent_servers'] = "You have to choose two different servers";
$_LANG['addonAA']['tools']['infos']['migration_removed'] = "Migration Canceled";
$_LANG['addonAA']['tools']['migrate_zone'] = "Migrate Zone";
$_LANG['addonAA']['tools']['migrate_now'] = "Migrate Now";

$_LANG['addonAA']['tools']['run_now'] = "Run Now";
$_LANG['addonAA']['tools']['task_run_now_confirm'] = "Are you sure that you want to run this task now?";
$_LANG['addonAA']['tools']['remove'] = "Remove";
$_LANG['addonAA']['tools']['task_remove_confirm'] = "Are you sure that you want to remove this task?";
$_LANG['addonAA']['tools']['table']['name'] = "Name";
$_LANG['addonAA']['tools']['table']['lastrun'] = "Last Run";
$_LANG['addonAA']['tools']['table']['nextrun'] = "Next Run";
$_LANG['addonAA']['tools']['table']['created_at'] = "Created On";
$_LANG['addonAA']['tools']['table']['status'] = "Status";
$_LANG['addonAA']['tools']['infos']['task_removed'] = "The selected task has been removed successfully";
$_LANG['addonAA']['tools']['schedule_import_confirmation'] = "Are you sure you that want to schedule this import? It will cause your admin area to be unavailable for a while.";
$_LANG['addonAA']['tools']['schedule_migration_confirmation'] = "Are you sure that you want to schedule this migration? It will cause your admin area to be unavailable for a while.";
$_LANG['addonAA']['tools']['infos']['import_scheduled'] = "A new import has been scheduled successfully";
$_LANG['addonAA']['tools']['infos']['migration_scheduled'] = "A new migration has been scheduled successfully";

$_LANG['addonAA']['zones']['not_esxist'] = "Deleted";
$_LANG['addonAA']['zones']['exist'] = "Created";
$_LANG['addonAA']['tools']['infos']['zone_imported'] = "The selected zone has been imported successfully";
$_LANG['addonAA']['tools']['errors']['something_went_wrong_during_import'] = "Something has gone wrong during the import";
$_LANG['addonAA']['tools']['infos']['zone_migrated'] = "The selected zone has been migrated successfully";
$_LANG['addonAA']['tools']['errors']['something_went_wrong_during_migration'] = "Something has gone wrong during the migration";

$_LANG['addonAA']['dns_submodule_connection_problem'] = "Something has gone wrong while connecting to the zone server. Please try again later.";
$_LANG['addonAA']['dns_submodule_command_error'] = "Something has gone wrong. Server of the zone returns:";
$_LANG['addonAA']['dns_submodule_invalid_parameters'] = "Something has gone wrong. Reason:";
$_LANG['addonAA']['dns_submodule_invalid_response'] = "Something has gone wrong. The response from the zone server cannot be parsed. Please try again later.";
$_LANG['addonAA']['settings']['errors']['this_server_is_already_in_use_by_some_packages'] = "You cannot remove this server because it is already used by some packages.";
$_LANG['addonAA']['settings']['errors']['there_are_zones_on_this_server'] = "There are zones on this server. Please migrate/remove them before removing the server.";
$_LANG['addonAA']['settings']['errors']['you_cannot_delete_server_with_task'] = "You cannot delete a server with an import or migration assigned.";
$_LANG['addonAA']['settings']['errors']['server_cannot_be_empty'] = "This field cannot be empty.";
$_LANG['addonAA']['settings']['errors']['server_already_on_list'] = "This server is already on the list.";

$_LANG['addonAA']['tools']['task_statuses']['start'] = "New";
$_LANG['addonAA']['tools']['task_statuses']['pending'] = "Pending";
$_LANG['addonAA']['tools']['task_statuses']['in_progress'] = "In Progress";
$_LANG['addonAA']['tools']['task_statuses']['aborted'] = "Aborted";
$_LANG['addonAA']['tools']['task_statuses']['canceled'] = "Canceled";
$_LANG['addonAA']['tools']['task_statuses']['finished'] = "Finished";
$_LANG['addonAA']['tools']['task_statuses']['error'] = "Error";
$_LANG['addonAA']['tools']['infos']['task_finished'] = 'The selected task has been finished';

$_LANG['addonAA']['tools']['related_item'] = "Related Item";
$_LANG['addonAA']['tools']['none'] = "None";
$_LANG['addonAA']['tools']['select_one'] = "Select One";

$_LANG['addonAA']['settings']['records_limit'] = "Records Limit";
$_LANG['addonAA']['settings']['total'] = "Total";
$_LANG['addonAA']['settings']['there_is_no_domain_configured_yet'] = "There are no domains configured yet";
$_LANG['addonAA']['settings']['there_is_no_product_configured_yet'] = "There are no products configured yet";
$_LANG['addonAA']['settings']['there_is_no_product_addon_configured_yet'] = "There are no product addons configured yet";
$_LANG['addonAA']['tools']['errors']['something_went_wrong_during_import'] = "Something has gone wrong during the import";
$_LANG['addonAA']['zones']['infos']['record_removed'] = "Record Removed";
$_LANG['addonAA']['tools']['there_is_no_zone_to_migrate'] = "There are no zones to migrate";
$_LANG['addonAA']['tools']['errors']['something_went_wrong_during_import'] = "Something has gone wrong during the import";
$_LANG['addonAA']['tools']['one_of_servers_has_been_removed'] = 'One of the servers has been removed';
$_LANG['addonAA']['tools']['server_has_been_removed'] = 'The selected server has been removed';

$_LANG['addonAA']['tools']['notify_only'] = "Notify Only";
$_LANG['addonAA']['tools']['notify_only_desc'] = "When the Notify Only option is enabled by default, the cron job is disabled. These settings have to be changed manually if you would like to have the zones removed by the Cron Cleaner.";

$_LANG['addonAA']['settings']['rdns_standalone'] = "Standalone rDNS";
$_LANG['addonAA']['settings']['other'] = 'Other';
$_LANG['addonAA']['settings']['allow_to_create_own_zones'] = 'Allow the creation of own zones';
$_LANG['addonAA']['settings']['related_service_ips_only'] = 'Related Service IP Addresses Only';
$_LANG['addonAA']['settings']['custom_subnet_ip'] = 'Custom Subnet IP Address';
$_LANG['addonAA']['settings']['custom_ip'] = 'rDNS Custom IP Address';
$_LANG['addonAA']['zones']['change_related_item'] = 'Change Related Item';
$_LANG['addonAA']['zones']['related_item'] = 'Related Item';
$_LANG['addonAA']['zones']['infos']['success'] = 'Success';
$_LANG['addonAA']['settings']['standalone_rdns_title'] = 'Create PTR records for IPv4 and IPv6 addresses with no related zone';
$_LANG['addonAA']['settings']['related_service_ips_only_title'] = 'Create zones and PTR records with a related IP address only';
$_LANG['addonAA']['settings']['custom_subnet_ip_title'] = 'Create zones and PTR records with an IP address included in a subnet related to the product (if integrated with IP Manager For WHMCS)';
$_LANG['addonAA']['settings']['custom_ip_title'] = 'Create PTR records with a custom IP address';
$_LANG['addonAA']['settings']['owned_domains_only'] = 'Owned Domains Only';
$_LANG['addonAA']['settings']['owned_domains_only_title'] = 'Restrict clients to manually create zones using their own domains only';

$_LANG['addonAA']['settings']['zone_create_custom_ip'] = 'Custom IP Address';
$_LANG['addonAA']['settings']['zone_create_custom_ip_title'] = 'Create zones with a custom IP address';

$_LANG['addonAA']['settings']['select_all'] = "Select All";

//===========================================================================================================
//===========================================CLIENT_AREA=====================================================
//===========================================================================================================
$_LANG['addonCA']['dashboard']['errors']['you_cant_create_this_zone_because_it_is_not_belongs_to_you'] = "Você não pode criar esta zona, porque ela não pertence a você";
$_LANG['addonCA']['dashboard']['you_cannot_edit_this_zone_because_it_is_not_belongs_to_you'] = "Você não pode editar esta zona, porque ela não pertence a você";
$_LANG['addonCA']['dashboard']['you have no zones available'] = "Você não tem zonas disponíveis neste momento";
$_LANG['addonCA']['confirmation_close'] = "Fechar";
$_LANG['addonCA']['confirmation_confirm'] = "Confirmar";
$_LANG['addonCA']['confirmation_default_title'] = "Confirmação";
$_LANG['addonCA']['dashboard']['zones'] = "Zonas";
$_LANG['addonCA']['dashboard']['add_new_zone'] = "Add Nova Zona";
$_LANG['addonCA']['dashboard']['you_have_no_zone_within_this_group'] = "Não há zonas dentro deste grupo";
$_LANG['addonCA']['dashboard']['edit_zone'] = "Editar Zona";
$_LANG['addonCA']['dashboard']['remove-zone-confirm'] = "Tem certeza de que deseja remover esta zona?";
$_LANG['addonCA']['dashboard']['delete_zone'] = "Excluir Zona";
$_LANG['addonCA']['dashboard']['service'] = "Serviço";
$_LANG['addonCA']['dashboard']['domain'] = "Domínio";
$_LANG['addonCA']['dashboard']['addon'] = "Addon";
$_LANG['addonCA']['dashboard']['dns_zones'] = "Zona DNS";
$_LANG['addonCA']['dashboard']['search'] = "Buscar";
$_LANG['addonCA']['dashboard']['add_dns_zone'] = "Add Zona DNS";
$_LANG['addonCA']['dashboard']['zone_name'] = "Nome da Zone";
$_LANG['addonCA']['dashboard']['ip'] = "Endereço IP";
$_LANG['addonCA']['dashboard']['cancel'] = "Cancelar";
$_LANG['addonCA']['dashboard']['add_zone'] = "Add Zona";
$_LANG['addonCA']['dashboard']['errors']['you_cant_add_new_zones_within_this_group'] = "Você não pode adicionar novas zonas dentro deste grupo";
$_LANG['addonCA']['dashboard']['errors']['you_cant_remove_this_zone_because_it_is_not_belongs_to_you'] = "Você não pode remover esta zona porque não pertence a você";
$_LANG['addonCA']['dashboard']['errors']['you_cant_remove_this_record_because_it_is_not_belongs_to_one_of_your_zones'] = "Você não pode remover este registro porque não pertence às suas zonas";
$_LANG['addonCA']['dashboard']['errors']['invalid_zone_id'] = "O ID da zona selecionada é inválido";
$_LANG['addonCA']['dashboard']['errors']['invalid_module_config'] = "Os campos Nome do Módulo ou Dados de Configuração do Módulo estão vazios";
$_LANG['addonCA']['dashboard']['infos']['zone_removed_successfully'] = "A zona selecionada foi removida com sucesso";

$_LANG['addonCA']['dashboard']['add_record'] = "Add Registro";
$_LANG['addonCA']['dashboard']['name'] = "Nome";
$_LANG['addonCA']['dashboard']['type'] = "Tipo";
$_LANG['addonCA']['dashboard']['ttl'] = "TTL";
$_LANG['addonCA']['dashboard']['rdata'] = "RDATA";
$_LANG['addonCA']['dashboard']['actions'] = "Ações";
$_LANG['addonCA']['dashboard']['remove_zone_record_confirm'] = "Tem certeza de que deseja remover este registro? Todas as alterações não salvas serão perdidas.";
$_LANG['addonCA']['dashboard']['remove_record'] = "Remover Registro";
$_LANG['addonCA']['dashboard']['save_changes'] = "Salvar Alterações";
$_LANG['addonCA']['dashboard']['infos']['record_removed_successfully'] = "O registro selecionado foi removido com sucesso";

$_LANG['addonCA']['dashboard']['add_new_record'] = "Add Novo Registro";
$_LANG['addonCA']['dashboard']['select_one'] = "Selecione Um";
$_LANG['addonCA']['dashboard']['infos']['new_record_added_to_zone'] = "Um novo registro foi adicionado com sucesso à zona";
$_LANG['addonCA']['dashboard']['infos']['changes_saved'] = "As alterações foram salvas com sucesso";
$_LANG['addonCA']['dashboard']['dns_manager'] = "Gerenciador de DNS";
$_LANG['addonCA']['dashboard']['errors']['choose_record_type'] = "Escolha um tipo de registro";
$_LANG['addonCA']['dashboard']['errors']['you_cant_add_this_record_because_you_reach_limit'] = "Você não pode adicionar este registro porque você atingiu o limite";
$_LANG['addonCA']['dashboard']['records_limit_reached'] = "Limite de registros alcançado";
$_LANG['addonCA']['dashboard']['create_zone'] = "Criar Zona";
$_LANG['addonCA']['dashboard']['infos']['zone_created_successfully'] = "Uma nova zona foi criada com sucesso";
$_LANG['addonCA']['dashboard']['infos']['zone_updated_successfully'] = "A zona selecionada foi atualizada com sucesso";
$_LANG['addonCA']['dashboard']['you_cannot_edit_this_zone_because_it_is_terminated_on_server'] = "Você não pode editar esta zona porque ela está terminada no servidor";
$_LANG['addonCA']['dashboard']['there_is_no_record_within_this_zone'] = "Não há registros dentro desta zona";
$_LANG['addonCA']['dashboard']['no_ip_available'] = "Nenhum endereço IP disponível";
$_LANG['addonCA']['dashboard']['errors']['you_cannot_add_record_to_zone_that_is_not_yours'] = "Você não pode adicionar um registro à zona que não pertence a você";
$_LANG['addonCA']['dashboard']['errors']['you_cant_use_ip_that_not_belongs_to_you'] = "Você não pode usar um endereço IP que não pertence a você";
$_LANG['addonCA']['dashboard']['errors']['you_have_exceeded_limit'] = "Você excedeu o limite de registros";
$_LANG['addonCA']['dashboard']['errors']['zone_already_in_task'] = "Esta zona já foi agendada em uma tarefa";
$_LANG['addonCA']['dashboard']['zone_already_taken'] = "A zona selecionada já foi tomada";
$_LANG['addonCA']['dashboard']['errors']['this_ip_is_already_taken'] = "Este endereço IP já está sendo usado";
$_LANG['addonCA']['dashboard']['errors']['you_cannot_remove_record_that_is_not_belongs_to_you'] = "Você não pode remover um registro que não pertence a você";
$_LANG['addonCA']['dashboard']['no_matches_found'] = "Nenhuma equivalência encontrada";

$_LANG['addonCA']['dns_submodule_connection_problem'] = "Algo deu errado ao se conectar ao servidor de zona. Por favor, tente novamente mais tarde.";
$_LANG['addonCA']['dns_submodule_command_error'] = "Algo deu errado. O servidor da zona retorna:";
$_LANG['addonCA']['dns_submodule_invalid_parameters'] = "Something has gone wrong. Reason:";
$_LANG['addonCA']['dns_submodule_invalid_response'] = "Something has gone wrong. The response from the zone server cannot be parsed. Please try again later.";
$_LANG['addonCA']['you_cannot_edit_zone_that_is_not_your_own_zone'] = "You cannot edit a zone which does not belong to you";

$_LANG['addonCA']['dashboard']['ip_or_block'] = "IP Address Or Subnet";
$_LANG['addonCA']['dashboard']['errors']['ip_is_not_belongs_to_selected_block'] = "This IP address does not belong to the selected block";
$_LANG['addonCA']['dashboard']['errors']['you_dont_own_this_ip'] = 'You do not own this IP address';
$_LANG['addonCA']['dashboard']['leave_it_blank_if_you_want_to_use_domain_only'] = "Leave it empty if you want to use the domain only";

$_LANG['addonCA']['dashboard']['add_new_rdns'] = "Add novo PTR";
$_LANG['addonCA']['dashboard']['add'] = "Add";
$_LANG['addonCA']['dashboard']['related_item'] = "Item relacionado";
$_LANG['addonCA']['dashboard']['manage_rdns'] = "Gerenciar DNS reverso";
$_LANG['addonCA']['dashboard']['select_zones'] = "Selecionar Zones";
$_LANG['addonCA']['dashboard']['set_dns_record_set'] = "Alterar conjunto de registros";
$_LANG['addonCA']['dashboard']['select_record_set'] = "Select Record Set";
$_LANG['addonCA']['dashboard']['wipe_records'] = "Wipe Records";
$_LANG['addonCA']['dashboard']['errors']['select_one_at_least'] = "You have to select at least one zone";
$_LANG['addonCA']['dashboard']['set_dns_records'] = "Change Record Set";
$_LANG['addonCA']['dashboard']['infos']['set_added_successfully'] = "The 'Set DNS Record' task has been added successfully";
$_LANG['addonCA']['dashboard']['add_ptr'] = "Add PTR";
$_LANG['addonCA']['dashboard']['there_is_no_ptr_record'] = "There is no PTR record";
$_LANG['addonCA']['dashboard']['errors']['no_active_server_related_to_this_item'] = "There is no active server related to this item";
$_LANG['addonCA']['dashboard']['errors']['rnds_disabled'] = "Reverse DNS is disabled on the server related to the selected item";
$_LANG['addonCA']['dashboard']['edit_record'] = "Edit Record";
$_LANG['addonCA']['dashboard']['edit_rdns'] = "Edit Reverse DNS";
$_LANG['addonCA']['dashboard']['errors']['invalid_domain_name'] = "Dados de domínio inválidos (RDATA)";
$_LANG['addonCA']['dashboard']['infos']['success'] = "Success";
$_LANG['addonCA']['dashboard']['errors']['ip_cannot_be_empty'] = "IP address cannot be empty";
$_LANG['addonCA']['dashboard']['default_ip'] = "Default IP Address";
$_LANG['addonCA']['dashboard']['errors']['you_cant_use_domain_that_does_not_belong_to_you'] = "You cannot use a domain which does not belong to you";
$_LANG['addonCA']['dashboard']['zone_signing_keys'] = 'Zone Signing Keys';
$_LANG['addonCA']['dashboard']['key_signing_keys'] = 'Key Signing Key';
$_LANG['addonCA']['dashboard']['delegation_signer_records'] = 'Delegation Signer Records';
$_LANG['addonCA']['dashboard']['dns_key_signing_key_records'] = 'DNS Key Signing Key Records';
$_LANG['addonCA']['dashboard']['dns_zone_signing_key_records'] = 'DNS Zone Signing Key Records';
$_LANG['addonCA']['dashboard']['encryption_method'] = 'Encryption Method';
$_LANG['addonCA']['dashboard']['key_expiration'] = 'Key Expiration';
$_LANG['addonCA']['dashboard']['expiration'] = 'Expiration';
$_LANG['addonCA']['dashboard']['key_tag'] = 'Key Tag';
$_LANG['addonCA']['dashboard']['key_size'] = 'Key Size';
$_LANG['addonCA']['dashboard']['algorithm'] = 'Algorithm';
$_LANG['addonCA']['dashboard']['digest_type'] = 'Digest Type';
$_LANG['addonCA']['dashboard']['digest'] = 'Digest';
$_LANG['addonCA']['dashboard']['flags'] = 'flags';
$_LANG['addonCA']['dashboard']['protocol'] = 'protocol';
$_LANG['addonCA']['dashboard']['public_key'] = 'public key';
$_LANG['addonCA']['dashboard']['dnssec'] = 'DNSSEC';
$_LANG['addonCA']['dashboard']['disable_dnssec'] = 'Disable DNSSEC';
$_LANG['addonCA']['dashboard']['enable_dnssec'] = 'Enable DNSSEC';
$_LANG['addonCA']['dashboard']['rectify_dnssec'] = 'Rectify Zone';
$_LANG['addonCA']['dashboard']['no_dnsseck_keys'] = 'You do not have DNSSEC keys';
$_LANG['addonCA']['dashboard']['bits'] = 'Bits';
$_LANG['addonCA']['dashboard']['dnssec_is_disabled']    =   'O DNSSEC está desativado';
$_LANG['addonCA']['dashboard']['dnssec_keys']    =   'DNSSEC Keys';
$_LANG['addonCA']['dashboard']['dnssec_is_disabled']    =   'O DNSSEC está desativado';
$_LANG['addonCA']['dashboard']['domain_is_not_registered']    =   'O domínio selecionado não está registrado ';
$_LANG['addonCA']['dashboard']['status_active']    =   'Ativo';
$_LANG['addonCA']['dashboard']['status_not_active']    =   'Inativo';
$_LANG['addonCA']['dashboard']['domain_is_pending'] = 'Pendente';

$_LANG['Directory'] = 'O diretório';
$_LANG['is_not_readable'] = 'is not readable';
$_LANG['is_not_writable'] = 'is not writable';
$_LANG['not found'] = 'not found';
$_LANG['addonAA']['settings']['registrars'] = "Registrars";

$_LANG['addonAA']['settings']['Server'] = "The selected server";
$_LANG['addonAA']['settings']['does not support'] = "does not support";
$_LANG['addonAA']['settings']['record'] = "the record";
$_LANG['addonAA']['settings']['records'] = "records";

$_LANG['addonAA']['tools']['auto_check_matches_on_import'] = 'Auto Check Matching Records';
$_LANG['addonAA']['tools']['auto_check_matches_on_import_title'] = 'Mark records matched to the existing hostings/domains automatically. This will speed up the process of manual selecting zones. Do not forget to confirm the import manually.';

$_LANG['addonAA']['settings']['is_not_readable'] = 'is not readable';
$_LANG['addonAA']['settings']['please_set'] = 'Please set';
$_LANG['addonAA']['settings']['to_enable_the_feature'] = 'To enable this feature';
$_LANG['addonAA']['settings']['directory_as'] = 'the directory as';
$_LANG['addonAA']['settings']['writable'] = 'writable';
$_LANG['addonAA']['settings']['readable'] = 'readable';
$_LANG['recordLimitExceded'] = 'You cannot add more records, the limit has been exceeded';
$_LANG['youCannotAddSpecificRecords'] = 'You cannot add more of';
$_LANG['limitExceded'] = 'records, the limit has been exceeded';
$_LANG['addonAA']['settings']['registrarsDescription'] = "The following feature allows you to add the DNS Management option to registrars, where it was not originally supported. This feature uses the WHMCS 'DNS Management' tab in the client area which makes it identical to the default domain DNS management.";

$_LANG['addonAA']['zones']['importZoneFromFile'] = 'Import Zone From File';
$_LANG['addonAA']['zones']['exportZoneToFile'] = 'Export Zone To File';
$_LANG['addonAA']['zones']['please_set'] = 'Please set';
$_LANG['addonAA']['zones']['to_enable_the_feature'] = 'To enable this feature';
$_LANG['addonAA']['zones']['directory_as'] = 'the directory as';
$_LANG['addonAA']['zones']['writable'] = 'writable';
$_LANG['addonAA']['zones']['newFileName'] = 'New File Name';
$_LANG['addonAA']['zones']['choseFileForImport'] = 'Choose File To Import'; 
$_LANG['addonAA']['zones']['importZoneFromFile'] = 'Import Zone From File';
$_LANG['addonAA']['zones']['readable'] = 'readable';
$_LANG['addonAA']['zones']['writable'] = 'writable';

$_LANG['addonAA']['tools']['toFile'] = "Import To File";
$_LANG['addonAA']['tools']['toEnableImportToFIle'] = "In order to enable the import of zones to files, the directory:";
$_LANG['addonAA']['tools']['needsToBeWritable'] = "needs to be writable.";
$_LANG['addonAA']['pagesLabels']['tools']['export'] = "Export";
$_LANG['addonAA']['tools']['zonesExportedTotal'] = "Zones Exported Total";
$_LANG['addonAA']['tools']['addExport'] = "Add Export";
$_LANG['addonAA']['tools']['thereAreNoExports'] = "There are no exports yet";
$_LANG['addonAA']['tools']['removeExportConfirmation'] = "Are you sure that you want to cancel this export? All zones that have been already exported will not be removed from the client accounts.";
$_LANG['addonAA']['tools']['scheduleExportConfirmation'] = "Are you sure that you want to schedule this export? It will cause your admin area to be unavailable for a while.";
$_LANG['addonAA']['tools']['scheduleExport'] = "Schedule Export";
$_LANG['addonAA']['tools']['needsToBeReadable'] = "needs to be readable.";
$_LANG['addonAA']['tools']['destinationServer'] = "Destination Server";
$_LANG['addonAA']['tools']['exportWarning'] = "Warning! This action will rebuild the zones found in the source file on the DNS server. This might result in deleting some of the records.";
$_LANG['addonAA']['tools']['fromFile'] = "Source File";
$_LANG['addonAA']['tools']['infos']['exportRemoved'] = "The selected export has been canceled successfully";
$_LANG['addonAA']['tools']['infos']['exportScheduled'] = "A new export has been scheduled successfully";
$_LANG['addonAA']['tools']['exportZones'] = "Export Zones";
$_LANG['addonAA']['tools']['exportZone'] = "Export Zone";
$_LANG['addonAA']['tools']['exportNow'] = "Export Now";
$_LANG['addonAA']['tools']['export'] = "Export";

$_LANG['addonAA']['tools']['does_not_have_active_service'] = 'Suspended/Terminated/Canceled Service';

$_LANG['mg_sets_dns_link'] = 'DNS Record Sets';
$_LANG['addonAA']['settings']['table']['user'] = "Client";
$_LANG['addonCA']['dashboard']['infos']['set_removed'] = "The selected set has been removed successfully";
$_LANG['addonCA']['dashboard']['dns_manager']['setsdns'] = "DNS Manager";
$_LANG['addonCA']['setsdns']['setsdns'] = 'DNS Record Sets';
$_LANG['addonCA']['setsdns']['add_set'] = 'Add Set';
$_LANG['addonCA']['setsdns']['table']['name'] = 'Name';
$_LANG['addonCA']['setsdns']['table']['dns_records'] = 'DNS Records';
$_LANG['addonCA']['setsdns']['table']['assigned_to'] = 'Assigned';
$_LANG['addonCA']['dashboard']['remove_set_confirmation_body'] = 'Are you sure that you want to remove this set?';
$_LANG['addonCA']['dashboard']['remove'] = 'Remove';
$_LANG['addonCA']['dashboard']['edit'] = 'Edit';
$_LANG['addonCA']['dashboard']['add_set'] = 'Add Set';
$_LANG['addonCA']['dashboard']['wiki'] = 'You may add adequate variables for a domain: {$domain}, {$domainname}, {$domainextension} and IP {$ip} address and then these fields will be automatically changed to the domain/zone IP when this record set is used.';
$_LANG['addonCA']['dashboard']['set_edit'] = 'Edit Set';
$_LANG['addonCA']['dashboard']['close'] = 'Close';
$_LANG['addonCA']['dashboard']['there_is_no_record_within_this_set'] = "There are no records within this set";
$_LANG['addonAA']['setsdns']['setsdns'] = 'Sets DNS';
$_LANG['addonAA']['setsdns']['infos']['set_added'] = "A new set has been added successfully";
$_LANG['addonAA']['setsdns']['infos']['set_removed'] = "The selected set has been removed successfully";
$_LANG['addonAA']['dashboard']['infos']['set_added'] = "A new set has been added successfully";
$_LANG['addonAA']['dashboard']['infos']['set_removed'] = "The selected set has been removed successfully";
$_LANG['addonAA']['setsdns']['set_added'] = "A new set has been added successfully";
$_LANG['addonAA']['setsdns']['set_removed'] = "The selected set has been removed successfully";
$_LANG['addonAA']['dashboard']['set_added'] = "A new set has been added successfully";
$_LANG['addonAA']['dashboard']['set_removed'] = "The selected set has been removed successfully";
$_LANG['addonCA']['dashboard']['def'] = 'Default';
$_LANG['addonCA']['dashboard']['true'] = 'True';
$_LANG['addonCA']['dashboard']['false'] = 'False';

$_LANG['addonAA']['settings']['link_to_dns_sets_management'] = "Client Area Record Sets";
$_LANG['addonAA']['settings']['link_to_dns_sets_management_desc'] = "Allows customers to manage DNS record sets from the client area";

$_LANG['addonAA']['pagesLabels']['tools']['backups'] = "Zone Backups";
$_LANG['addonAA']['pagesLabels']['tools']['backupsList'] = "Zone Backups";
$_LANG['addonAA']['tools']['backupsList'] = "Zone Backups";
$_LANG['addonAA']['tools']['fileName'] = "File Name";
$_LANG['addonAA']['tools']['exportType'] = "Export Type";
$_LANG['addonAA']['tools']['thereAreNoBackupFiles'] = "There are no backup files yet";
$_LANG['addonAA']['tools']['downloadFile'] = "Download File";
$_LANG['addonAA']['tools']['uploadFile'] = "Upload File";
$_LANG['addonAA']['tools']['deleteFile'] = "Delete File";
$_LANG['addonAA']['tools']['errors']['fileDoesNotExist'] = "The :file: file does not exist";
$_LANG['addonAA']['tools']['removeBackupConfirmation'] = "Are you sure that you want to remove this file?";
$_LANG['addonAA']['tools']['infos']['fileRemoved'] = "The selected file has been removed successfully";
$_LANG['addonAA']['tools']['errors']['fileDeleteFailed'] = "The process of removing the :file: file has failed";
$_LANG['addonAA']['tools']['addBackupFile'] = "Upload Backup File";
$_LANG['addonAA']['tools']['chooseFileToUpload'] = "Choose File";
$_LANG['addonAA']['tools']['selectedFileToUpload'] = "Selected File";
$_LANG['addonAA']['tools']['backupType'] = "Backup Type";
$_LANG['addonAA']['tools']['singleZone'] = "Single Zone";
$_LANG['addonAA']['tools']['bulkZones'] = "Bulk Zones";
$_LANG['addonAA']['tools']['upload'] = "Upload";
$_LANG['addonAA']['tools']['selectFile'] = "Select File";
$_LANG['addonAA']['tools']['errors']['fileAlreadyExist'] = "The :file: file already exists";
$_LANG['addonAA']['tools']['errors']['sendingFailed'] = "Upload Failed";
$_LANG['addonAA']['tools']['infos']['uploadSuccesfull'] = "The selected file has been uploaded successfully";
$_LANG['addonAA']['tools']['backupsTasks'] = "Backups Tasks";
$_LANG['addonAA']['tools']['taskType'] = "Task Type";
$_LANG['addonAA']['tools']['taskDescription'] = "Task Description";
$_LANG['addonAA']['tools']['scheduleTask'] = "Schedule Task";
$_LANG['addonAA']['tools']['infos']['taskRemoved'] = "Task Removed";
$_LANG['addonAA']['tools']['zones_procesed_total'] = "Total Zones Processed";
$_LANG['addonCA']['dashboard']['sets_dns'] = "DNS Record Sets";
$_LANG['addonCA']['dashboard']['default_check_desc'] = "If checked, this will be a default set used during the creation of a new DNS zone";
$_LANG['addonAA']['tools']['you_cannot_edit_this_zone_because_it_is_terminated_on_server'] = 'You cannot edit this zone because it is terminated on the server';
$_LANG['addonCA']['setsdns']['dnsSetsTitle'] = "DNS Manager";
$_LANG['addonAA']['zones']['domain'] = "Domain";
$_LANG['addonAA']['zones']['service'] = "Service";
$_LANG['addonAA']['zones']['addon'] = "Addon";
$_LANG['addonAA']['tools']['there_are_no_backup_tasks'] = "There are no backup tasks";
$_LANG['addonAA']['zones']['infos']['zoneSuccessfullyExported'] = "The selected zone has been exported successfully";
$_LANG['addonAA']['zones']['infos']['zoneSuccessfullyImported'] = "The selected zone has been imported successfully";
$_LANG['addonCA']['dashboard']['general'] = "General";
$_LANG['addonCA']['dashboard']['records'] = "Records";
$_LANG['addonAA']['tools']['errors']['invalidContent'] = "Invalid File Content";
$_LANG['addonCA']['dashboard']['selectRecordSet'] = "Select Record Set";
$_LANG['addonCA']['dashboard']['none'] = "None";
$_LANG['addonCA']['dashboard']['defaultSet'] = "Default Set";
$_LANG['addonAA']['tools']['unactiveServices'] = "Inactive Services";
