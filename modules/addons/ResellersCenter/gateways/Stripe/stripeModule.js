const rcStripeModule = {

    complete:{
        cardNumber: false,
        cardExpiry: false,
        cardCvc:    false,
    },
    existingToken: null,
    htmlStorage: {
        inputNumber:    '',
        inputExpiry:    '',
        inputCcv:       '',
    },
    mount(){
        this.rebuildFormInputs();
        this.mountStripeApp();
    },
    umount(){
        this.restoreFormInputs();
    },
    rebuildFormInputs(){
        var inputNumber = $('#inputCardNumber').parent();
        inputNumber.attr('id','inputCardNumberSection');
        this.htmlStorage.inputNumber = inputNumber.contents();
        inputNumber.text('');
        inputNumber.append('<div class="form-control newccinfo cc-number-field" id="inputCardNumber"></div>');
        var inputExpiry = $('#inputCardExpiry').parent();
        inputExpiry.attr('id','inputCardExpirySection');
        this.htmlStorage.inputExpiry = inputExpiry.contents();
        inputExpiry.text('');
        inputExpiry.append('<div class="form-control field input-inline input-inline-100" id="inputCardExpiry"></div>');

        var inputCardCvv = $('#inputCardCvv').parent();
        inputCardCvv.attr('id','inputCardCvvSection');
        this.htmlStorage.inputCcv = inputCardCvv.contents();
        inputCardCvv.text('');
        inputCardCvv.append('<div class="form-control input-inline input-inline-100" id="inputCardCvv"></div>');
    },
    restoreFormInputs(){
        var inputNumber = $('#inputCardNumber').parent();
        inputNumber.attr('id','inputCardNumberSection');
        inputNumber.text('');
        inputNumber.append(this.htmlStorage.inputNumber);

        var inputExpiry = $('#inputCardExpiry').parent();
        inputExpiry.attr('id','inputCardExpirySection');
        inputExpiry.text('');
        inputExpiry.append(this.htmlStorage.inputExpiry );

        var inputCardCvv = $('#inputCardCvv').parent();
        inputCardCvv.attr('id','inputCardCvvSection');
        inputCardCvv.text('');
        inputCardCvv.append(this.htmlStorage.inputCcv);

    },
    mountStripeApp(){

        var self = this;

        card.mount('#inputCardNumber');
        card.addEventListener("change", function(result){
            self.inputEventListener(result,'inputCardNumber', '');
        });

        cardExpiryElements.mount("#inputCardExpiry");
        cardExpiryElements.addEventListener("change", function(result){
            self.inputEventListener(result,'inputCardExpiry','<br>');
        });

        cardCvcElements.mount("#inputCardCvv");
        cardCvcElements.addEventListener("change", function(result){
            self.inputEventListener(result,'inputCardCvv', '<br>');
        });
    },

    inputEventListener(result, id, prepend){

        var input = $('#'+id);

        input.parent().find('.field-error-msg').remove();
        if(result.error)
        {
            input.parent().parent().addClass('has-error');
            input.parent().append(prepend+'<span class="field-error-msg" style="display: inline;">'+result.error.message+'</span>');
        }else{
            input.parent().parent().removeClass('has-error');
        }
    },

    submitForm(form, callbackError){

        var self = this;
        stripe.createPaymentMethod("card", card).then(function (e) {
            if (e.error) {
                callbackError(400, e);
                return false;
            }else{
                form.off('submit', validatePaymentStripe);
                form.append('<input type="hidden" name="stripeToken" value="'+e.paymentMethod.id+'">');
                form.submit();
            }
        });
    },


};