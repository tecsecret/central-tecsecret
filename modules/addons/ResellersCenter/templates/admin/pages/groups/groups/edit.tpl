<div id="groupEditFormModal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">{$MGLANG->T('group','update','title')}</h4>
            </div>
            <div class="modal-body">
                <div class="control-group">
                    <div class="row">
                        <div class="col-md-2">
                            <label class="label-control" for="name">{$MGLANG->T('group','update','form','name','label')}</label>
                        </div>
                        <div class="col-md-10">
                            <input class="form-control" name="name"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success btn-inverse" onclick='RC_ConfigurationGroups.editGroup();'>{$MGLANG->absoluteT('form','button','save')}</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">{$MGLANG->absoluteT('form','button','close')}</button>
            </div>
        </div>
    </div>
</div>