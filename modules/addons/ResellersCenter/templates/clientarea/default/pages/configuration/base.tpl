{if $noGatewayEnabled && !$globalSettings->disableEndClientInvoices}
    <div class="noGatewayEnabled alert alert-danger">
        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only"></span></button>
        <p><strong>{$MGLANG->T('noGatewaysInfo')}</strong></p>
    </div>
{/if}

<div id="RCConfiguration" class="box light">
    <div class="box-title tabbable-line">
        <div class="caption">
            <i class="fa fa-cogs font-red-thunderbird"></i>
            <span class="caption-subject bold font-red-thunderbird uppercase">
                {$MGLANG->T('title')}
            </span>
        </div>
            
        <ul class="nav nav-tabs">
            <li class="active">
                <a id="RCConfigGeneralLi" href="#RCConfigGeneral" data-toggle="tab" >
                    {$MGLANG->T('general', 'title')}
                </a>
            </li>
            
            <li>
                <a id="RCConfigEmailTemplatesLi" href="#RCConfigEmailTemplates" data-toggle="tab" >
                    {$MGLANG->T('emails', 'title')}
                </a>
            </li>

            {if $globalSettings->resellerInvoice eq 'on' && !$globalSettings->disableEndClientInvoices}
                <li>
                    <a href="#RCConfigPayments" data-toggle="tab">
                        {$MGLANG->T('payments', 'title')}
                    </a>
                </li>
            {/if}
        </ul>
        
    </div>
    <div class="box-body" style='min-height: 320px;'>
        <form action=''>
            <div class="tab-content">
                <div class="tab-pane active" id="RCConfigGeneral">
                    <div class="scroller">
                        {include file='general.tpl'}
                    </div>
                </div>
                    
                <div class="tab-pane" id="RCConfigEmailTemplates">
                    <div class="scroller">
                        {include file='emailTemplates/list.tpl'}
                    </div>
                </div>

               {if $globalSettings->resellerInvoice eq 'on' && !$globalSettings->disableEndClientInvoices}
                    <div class="tab-pane" id="RCConfigPayments">
                        <div class="scroller">
                            {include file='payments.tpl'}
                        </div>
                    </div>
                {/if}
            </div>
        </form>
    </div>
</div>
                    
<div class="row">
    <div class="col-md-12 text-center">
        <button class="btn btn-lg btn-success btn-inverse" onclick="ResellersCenter_Configuration.submitConfigForm();">{$MGLANG->T('form','save')}</button>
    </div>
</div>

<script type="text/javascript">
    {include file='controller.js'}
</script>