{literal}
var ResellersCenter_Configuration = 
{
    init: function()
    {
        $(".checkbox-switch").bootstrapSwitch();
        this.uploadLogoHandler();
        this.deleteLogoHandler();
        this.sortPaymentGatewaysHandler();
        
        //Set active tab in settings
        if(window.location.href.indexOf("#RCConfigEmailTemplates") != -1) 
        {
            $("#RCConfigGeneralLi").parent().removeClass("active");
            $("#RCConfigEmailTemplatesLi").parent().addClass("active");
        }
    },
    
    submitConfigForm: function()
    {
        $("#MGPageconfiguration .has-error").removeClass("has-error");
        this.checkIfAnyGatewayIsEnabled();
        
        //Validate
        if(! $.isNumeric($("[name='settings[nextinvoicenumber]']").val()) && $("[name='settings[nextinvoicenumber]']").length){
            $("[name='settings[nextinvoicenumber]']").parents(".form-group").addClass("has-error");
        }
        
        if($("#MGPageconfiguration .has-error").length)
        {
            $('#MGAlerts').alerts('error', "{/literal}{$MGLANG->absoluteT('form', 'validate', 'configuration')}{literal}");
            return;
        }
         
        var form = $("#RCConfiguration form").serialize();
        JSONParser.request("save", form, function(){});
    },
    
    deleteLogoHandler: function()
    {
        $(".deleteLogo").on("click", function(e){
            e.preventDefault();
            JSONParser.request("deleteLogo", {}, function()
            {
                $(".deleteLogo").hide();
                $(".logo-container img").removeAttr("src");
            });
        });
    },
    
    uploadLogoHandler: function()
    {
        $(".logo-container").on("click", function(){
            $("#RCConfiguration form").find("[name='logoFile']").click();   
        });
        
        $("[name='logoFile']").change(function()
        {
            var formdata = new FormData();
            formdata.append("logo", this.files[0]);
            formdata.append("resellerid", this.files[0]);
            
            $.ajax({
                url:'index.php?m=ResellersCenter&mg-page=configuration&mg-action=uploadLogo&json=1',
                data: formdata,
                type:'POST',
                contentType: false,
                processData: false
            }).success(function(result) {
                result = JSONParser.getJSON(result);
                if(!result.data.error)
                {
                    $(".deleteLogo").show();
                    $("[name='settings[logo]']").val(result.data.logo);
                    $(".logo-container img").removeAttr("src").attr("src", result.data.htmllogopath+'?'+Math.random());
                }
                else
                {
                    $('#MGAlerts').alerts('error', result.data.error);
                }
            });
        });
    },
    
    sortPaymentGatewaysHandler: function()
    {
        $("#RCConfigPayments .nav").sortable(
        {
            beforeStop: function(event, ui)
            {
                var order = {};
                $("#RCConfigPayments .nav li a").each(function(index, element){
                    order[index] = $(element).data("gateway");
                });
                
                JSONParser.request("sortPaymentGateways", {order: order}, function()
                {
                    $.each(order, function(value, gateway)
                    {
                        $("[name='gateways["+gateway+"][order]']").val(value);
                    });
                });
            }
        });
        
        $('#RCConfigPayments .nav').sortable({ cancel: '.show-draggable' });
    },
    
    checkIfAnyGatewayIsEnabled: function()
    {
        var isAnyEnabled = false;
        var switches = $("#RCConfigPayments").find("[name$='[enabled]']");
        $.each(switches, function(index, switcher)
        {
            var status = $(switcher).bootstrapSwitch('state');
            if(status)
            {
                isAnyEnabled = true;
            }
        });
        
        if(!isAnyEnabled){
            $(".noGatewayEnabled").show();
        }
        else
        {
            $(".noGatewayEnabled").hide();
        }
    }
}
ResellersCenter_Configuration.init();
{/literal}