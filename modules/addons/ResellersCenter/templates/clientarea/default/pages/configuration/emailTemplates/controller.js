{literal}
var ResellersCenter_EmailTemplates = 
{
    init: function()
    {
        tinymce.init({
            selector: '.tinyMCE',
            height: 500,
            menubar: false,
            plugins: [
              'advlist autolink lists link image charmap print preview anchor',
              'searchreplace visualblocks code fullscreen',
              'insertdatetime media table contextmenu paste code'
            ],   
            setup: function (editor) {
                editor.on('change', function () {
                    editor.save();
                });
            },
            toolbar: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
        });
          
        ResellersCenter_EmailTemplates.removeLanguageHandler();
    },
    
    addLanguageHandler: function()
    {
        var template = $("#editTemplateForm [name='name']").val();
        
        $("#RCEmailTemplatesAddLang [name='language']").select2({
            ajax: 
            {
                url: 'index.php?m=ResellersCenter&mg-page=configuration&mg-action=getAvailableLanguages&json=1&name='+template,
                processResults: function (data) 
                {
                    var result = JSONParser.getJSON(data);
                    var languages = [];
                    $.each(result.data, function(index, value){
                        languages.push({id: value, text: value});
                    });
                    
                    return {results: languages};
                },
                delay: 250
            },
            width: '100%',
            placeholder: "{/literal}{$MGLANG->absoluteT('form','select','placeholder')}{literal}",
        });
        
        //Clear selection
        $("#RCEmailTemplatesAddLang [name='language']").select2("val", "");      
        
        $("#RCEmailTemplatesAddLang").modal("show");
        
    },
    
    addLanguageSubmit: function()
    {
        var language = $("#RCEmailTemplatesAddLang [name='language']").val();
        if(!language) return;
        language = language.toLowerCase();
        var tab = $("#langTab_default").clone();
        tab.attr("id", language);
        tab.removeClass("active");
        
        tab.find(".mce-tinymce").remove();
        tab.find("textarea").removeAttr("id");
        tab.find("[name='templates[default][subject]']").attr("name", "templates["+language+"][subject]");
        tab.find("[name='templates[default][message]']").attr("name", "templates["+language+"][message]");
        tab.find("textarea").css("display", "block");

        $("#RCEmailTemplateEdit").find("ul").append('<li style="display: flex;"><a href="#'+language+'" data-toggle="tab" >'+$("#RCEmailTemplatesAddLang [name='language']").val()+'</a> <button type="button" class="close deleteLanguage" data-language="'+language+'"><span aria-hidden="true">×</span><span class="sr-only"></span></button> </li>');
        $("#editTemplateForm .tab-content").append(tab);
        
        tinymce.init({
            selector: "[name='templates["+language+"][message]'",
            height: 500,
            menubar: false,
            plugins: [
              'advlist autolink lists link image charmap print preview anchor',
              'searchreplace visualblocks code fullscreen',
              'insertdatetime media table contextmenu paste code'
            ],
            setup: function (editor) {
                editor.on('change', function () {
                    editor.save();
                });
            },
            toolbar: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
        });
          
        $("#RCEmailTemplatesAddLang").modal("hide");
        
        //Save new template 
        ResellersCenter_EmailTemplates.saveChanges();
        ResellersCenter_EmailTemplates.removeLanguageHandler();
    },
    
    removeLanguageHandler: function()
    {
        $(".deleteLanguage").unbind("click");
        $(".deleteLanguage").on("click", function()
        {
            var template = $("#editTemplateForm [name='name']").val();
            var tmplang = $(this).data("language");
            
            JSONParser.request("deleteTemplate", {template: template, language: tmplang}, function(){});
            
            //Remove from list
            $("#langTab_"+tmplang).remove();
            $("[data-language='"+tmplang+"']").parent().remove();
        });
    },
    
    saveChanges: function()
    {
        var form = $("#editTemplateForm").serialize();
        JSONParser.request("saveTemplate", form, function(result)
        {
            if(result == 'success') 
            {
                var url = window.location.href;
                url = url.substring(0, url.lastIndexOf("/") + 1);
                window.location.href = url + "index.php?m=ResellersCenter&mg-page=configuration";
            }
        });
    },
}
ResellersCenter_EmailTemplates.init();
{/literal}