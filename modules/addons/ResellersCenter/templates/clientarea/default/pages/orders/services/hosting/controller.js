{literal}
var ResellersCenter_Services_Hosting =
{
    init: function()
    {
        this.loadTable();
    },

    loadTable: function()
    {
        var table = new ResellersCenter_Datatable();
        table.setSource("orders", "getServicesTable");
        table.setSelector("#RCServicesTab table");

        table.addColumn(new ResellersCenter_Datatable_Column("hosting_id"));
        table.addColumn(new ResellersCenter_Datatable_Column("product"));
        table.addColumn(new ResellersCenter_Datatable_Column("domain"));
        table.addColumn(new ResellersCenter_Datatable_Column("client"));
        table.addColumn(new ResellersCenter_Datatable_Column("price"));
        table.addColumn(new ResellersCenter_Datatable_Column("billingcycle"));
        table.addColumn(new ResellersCenter_Datatable_Column("status"));
        table.addColumn(new ResellersCenter_Datatable_Column("nextduedate"));
        table.addColumn(new ResellersCenter_Datatable_Column("actions", false, false));
        table.addSearch(new ResellersCenter_Datatable_Search("#RCServicesTabFilters [name='search']"));

        $("#RCServicesTabFilters select").each(function(key, value)
        {
            var filter = new ResellersCenter_Datatable_Filter(value);
            filter.initSelect2("orders", "getFiltersData");
            table.addFilter(filter);
        });

        var button = new ResellersCenter_Datatable_Button(".openDeleteService", "#RCServicesTabDelete");
        button.addClickAction(this.deleteOpenHandler);
        button.addSuccessAction(this.deleteSubmitHandler);
        table.addButton(button);

        table.init();
    },

    deleteSubmitHandler: function (self)
    {
        var hostingid = $("#RCServicesTabDelete [name='relid']").val();
        JSONParser.request("terminateService", {relid: hostingid, type: 'hosting'}, function(result)
        {
            $("#RCServicesTabDelete").modal("hide");
        });
    },

    deleteOpenHandler: function(self)
    {
        var hostingid = $(self).data("hosting_id");
        $("#RCServicesTabDelete [name='relid']").val(hostingid);
    }
};
ResellersCenter_Services_Hosting.init();
{/literal}