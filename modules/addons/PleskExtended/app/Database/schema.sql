--
-- `#prefix#FunctionsSettings`
--
CREATE TABLE IF NOT EXISTS `#prefix#FunctionsSettings` (
                        `product_id` int(11) NOT NULL,
                        `ftp` varchar(2) NOT NULL,
                        `spamassassin` varchar(2) NOT NULL,
                        `backups` varchar(2) NOT NULL,
                        `emails` varchar(2) NOT NULL,  
                        `emails_forwarders` varchar(2) NOT NULL,
                        `databases` varchar(2) NOT NULL,              
                        `subdomains` varchar(2) NOT NULL,
                        `addon_domains` varchar(2) NOT NULL,
                        `domain_alias` varchar(2) NOT NULL,
                        `dns`	varchar(2) NOT NULL,
                        `plesk_login` varchar(2) NOT NULL,
                        `panel_login_hostname` varchar(255) NOT NULL,
                        `autoinstaller` varchar(255) NOT NULL,
                        `order_assign` varchar(255) NOT NULL,
                        `autoinstall_on_create` varchar(255) NOT NULL,
                        `install_app` varchar(255) NOT NULL,
                        `app_name` varchar(255) NOT NULL,
                        `lang` varchar(255) NOT NULL,
                        `config_option_gid` varchar(255) NOT NULL,
                        `apps_backups` varchar(2) NOT NULL,
                        `client_install_app` varchar(2) NOT NULL,
                        `auto_apps_backups` varchar(2) NOT NULL,
                        `auto_apps_backups_default` varchar(2) NOT NULL,
                        `webmail_login` varchar(2) NOT NULL,
                        `webmail_login_hostname` varchar(255) NOT NULL,
                        `installapps` varchar(2) NOT NULL,
                        `webusers` varchar(2) NOT NULL,
                        `ssl` varchar(2) NOT NULL,
                        `gitRepositories` varchar(2) NOT NULL,
                        `nodeJs` varchar(2) NOT NULL,
                        `php_settings` varchar(2) NOT NULL,
                        `log_rotation` varchar(2) NOT NULL,
                        PRIMARY KEY (`product_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=#charset# DEFAULT COLLATE #collation#;

--
-- `#prefix#Accounts`
--
CREATE TABLE IF NOT EXISTS `#prefix#Accounts` (
                        `customer_id` int(11) NOT NULL,
                        `serverid` int(11) NOT NULL,
                        `username` varchar(250) NOT NULL,
                        `password` text NOT NULL,
                        `hostingId` int(11) UNIQUE,
                        `secret_key` varchar (250),
                        UNIQUE KEY `customer_id` (`customer_id`,`serverid`,`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=#charset#  DEFAULT COLLATE #collation#;

--
-- `#prefix#Apps`
--
CREATE TABLE IF NOT EXISTS `#prefix#Apps` (
                        `domain_id` varchar(255) NOT NULL,
                        `app_name` varchar(255) NOT NULL,
                        `app_version` varchar(50) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=#charset#  DEFAULT COLLATE #collation#;

--
-- `#prefix#ServerSettings`
--
CREATE TABLE IF NOT EXISTS `#prefix#ServerSettings` (
                        `server_id` int(11) NOT NULL,
                        `panel_login_hostname` varchar(250) NOT NULL,
                        `webmail_login_hostname` varchar(250) NOT NULL,
                        UNIQUE KEY `server_id` (server_id)
) ENGINE=InnoDB  DEFAULT CHARSET=#charset# DEFAULT COLLATE #collation#;

--
-- `#prefix#Errors`
--
CREATE TABLE IF NOT EXISTS `#prefix#Errors` (
                        `serviceid` int(11) NOT NULL,
                        `message` TEXT,
                        `logid` int(11)
) ENGINE=InnoDB  DEFAULT CHARSET=#charset# DEFAULT COLLATE #collation#;

--
-- `#prefix#AccountIds`
--
CREATE TABLE IF NOT EXISTS `#prefix#AccountIds` (
                `hostingId`             int(11) PRIMARY KEY NOT NULL,
                `userid`                int(11) NOT NULL,
                `usertype`              varchar(255) NOT NULL,
                `panelexternalid`       varchar(255) NOT NULL

) ENGINE=InnoDB  DEFAULT CHARSET=#charset# DEFAULT COLLATE #collation#;