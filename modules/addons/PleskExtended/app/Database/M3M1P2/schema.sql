--
-- `#prefix#AccountIds`
--
CREATE TABLE IF NOT EXISTS `#prefix#AccountIds` (
                `hostingId`             int(11) PRIMARY KEY NOT NULL,
                `userid`                int(11) NOT NULL,
                `usertype`              varchar(255) NOT NULL,
                `panelexternalid`       varchar(255) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=#charset# DEFAULT COLLATE #collation#;