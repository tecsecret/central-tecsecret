<div class="lu-row">
    {foreach from=$rawObject->getFields() item=field }
        <div class="col-xs-12 col-sm-6 col-md-{$rawObject->getSectionSize()} col-lg-{$rawObject->getSectionSize()}">
            {$field->getHtml()}
        </div>
    {/foreach}
</div>