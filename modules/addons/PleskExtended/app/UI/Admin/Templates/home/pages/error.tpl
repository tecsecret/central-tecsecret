<div class="lu-row">
    <div class="lu-col-md-12">
        <div class="lu-alert lu-alert--danger" role="alert">
          <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
          <span class="sr-only">Error:</span>
          {$rawObject->getErrorMessage()}
        </div>        
    </div>
</div>
