<div class="lu-widget__content">
    <ul class="lu-list lu-list--info">
            {foreach from=$rawObject->getListArray() key=name item=value}
                <li class="lu-list__item">
                    <span class="lu-list__item-title">{if $rawObject->getAbsoluteTranslate()}{$MGLANG->absoluteT($name)}{else}{$MGLANG->T($name)}{/if}</span>
                    {if $rawObject->isBadgeElement($name)}
                    <span class="lu-badge lu-badge--default lu-badge--outline"><b>{$value}</b></span>
                    {else}
                        <span class="lu-list__value">{$value}</span>
                    {/if}
                </li>
            {/foreach}
    </ul>
</div>