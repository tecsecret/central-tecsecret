$( document ).ready(function() {
    
    /* trigg switcher action on load  */
    triggSwitcherAction();
});

function autoInstallerOptions(event)
{
    val         = event.target.value;
    section     = $(event.target).parent().parent();
    switchers   = $(section).find('div').find('.switch');
    
    autoUpdateBackups   = $(switchers).find('input[name="auto_apps_backups"]').parent().parent();
    defaultOnOrder      = $(switchers).find('input[name="auto_apps_backups_default"]').parent().parent();

    if(val === 'installatron')
    {
        $(autoUpdateBackups).show();
        $(defaultOnOrder).show();        
    }
    else
    {
        $(autoUpdateBackups).hide();
        $(defaultOnOrder).hide();             
    } 
}

///**
// * 
// * @description trigg switcher change event action 
// * @returns {undefined}
// */
function triggSwitcherAction()
{
    $('.lu-switch__checkbox').each(function () {
        /* should run event on loaded */
        jQuery(this).trigger('change');
    });
}

function changeFieldStatus(names, event, value)
{
    var self = event.target;

    if(self.checked === value)
    {
        $.each(names, function(key, value){
            $(this).parent().attr('disabled', true)
        });
        
    }else{
        
        $.each(names, function(key, value){
            $(this).parent().attr('disabled', false);
        });
    }
}

function checkSection(vueControler, params, event)
{
    if(!event.currentTarget)
    {
        return;
    }
    var div = event.currentTarget.parentElement.parentElement.parentElement.parentElement;
    inputs = $(div).find('input');
    inputs.each(function(key,input){
        if($(event.currentTarget).find('input')[0].checked == true)
        {
            input.checked = true;
        }
        else
        {
            input.checked = false;
        }
        
    }); 
}

function checkOptionUnderSection(event)
{
    if(!event.currentTarget)
    {
        return;
    }
    
    var allInputs  = $(event.currentTarget.parentElement.parentElement.parentElement).find('input');
    var inputsToCheck = $(event.currentTarget.parentElement.parentElement).find('input')
    var checked = true;
    inputsToCheck.each(function(key,input){
        if(input.checked == false)
        {
            checked = false;
        }
    });

    if(checked == false)
    {
        allInputs[0].checked = false;
    }
    else
    {
        allInputs[0].checked = true;
    }
}
