

function massEditRedirect(targetId, postData, event, namespace, index)
{
    postData = {
        formData: {
            massActionId: collectTableMassActionsData('featuresPage')
        }
    };

    var self = mgPageControler.vueLoader;
    self.showSpinner(event);
    self.refreshUrl();
    self.initRefreshActions(event, targetId);
    self.addUrlComponent('loadData', targetId);
    self.addUrlComponent('namespace', namespace);
    self.addUrlComponent('index', index);
    self.getActionId(event);
    self.addUrlComponent('ajax', '1');

    $.post(self.targetUrl, postData)
            .done(function (data) {
                data = data.data;
                window.location = data.rawData.url
            }, 'json');
    self.refreshUrl();

}


function downloadApps(data, targetId, event)
{
    //window.location.href = data.rawData.url;
    window.open(data.rawData.url, '_blank');

}



function enableDisableAll(event)
{
    var parent = $(event.target).parent().parent().parent().parent().parent().parent().parent().parent();

    var inputs = parent.find('input');

    /* change each */
    inputs.each(function (key, input) {

        if (event.target.checked == true)
        {
            input.checked = true;
        } else
        {
            input.checked = false;
        }
    });
}

function unsetWithMe(event, name)
{   
    /* if switcher value is disabled, disable select all switcher*/
    if (event.target.checked == false)
    {
        $('[name='+name+']')[0].checked = false;
    }else{
        
        /* root element */
        var parent = $(event.target).parent().parent().parent().parent().parent().parent().parent().parent();
        var inputs = parent.find('input');
        
        /* flag true if all switchet is enabled */
        var enabledAll = true;
        /* check all switches to see if they are turned on */
        inputs.each(function (key, input) {
           
           /* if one of switcher is disabled set flag to false*/
           if(input.name != name && input.checked == false)
           {
                enabledAll = false;
           }
        });
        
        /* if all of switchers is enabled set `switch all` switcher to true */
        if(enabledAll == true)
        {
            $('[name='+name+']')[0].checked = true;   
        }
    }
}