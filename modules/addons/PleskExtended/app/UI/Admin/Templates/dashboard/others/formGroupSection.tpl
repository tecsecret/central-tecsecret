{if $rawObject->getSections()}
    {foreach from=$rawObject->getSections() item=section }
        {$section->getHtml()}
    {/foreach}  
{else}
    <div class="form-group">
        {foreach from=$rawObject->getFields() item=field }
            {$field->getHtml()}
        {/foreach}
    </div>
{/if}
