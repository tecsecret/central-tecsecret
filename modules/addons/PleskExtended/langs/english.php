<?php

$_LANG['token']                  = ', Error Token:';
$_LANG['generalError']           = 'Something has gone wrong. Check the logs and contact the administrator.';
$_LANG['generalErrorClientArea'] = 'Something has gone wrong. Contact the administrator.';
$_LANG['permissionsStorage']     = ':storage_path: settings are not sufficient. Please change the permissions to writable.';
$_LANG['undefinedAction']        = 'Undefined Action';
$_LANG['changesHasBeenSaved']    = 'Changes have been saved successfully';
$_LANG['Monthly']                = 'Monthly';
$_LANG['Free Account']           = 'Free Account';
$_LANG['Passwords']              = 'Passwords';
$_LANG['labelAddedSuccesfully']  = 'The label has been added successfully';
$_LANG['Nothing to display']     = 'Nothing to display';
$_LANG['Search']                 = 'Search';
$_LANG['Previous']               = 'Previous';
$_LANG['Next']                   = 'Next';
$_LANG['searchPlacecholder']     = 'Search...';
$_LANG['thisFieldCannotBeEmpty'] = 'This field cannot be empty';
$_LANG['invalidLangCode']        = 'The language code is incorrect';
$_LANG['datatableItemsSelected'] = 'Items Selected';
$_LANG['invalidPleskUrl']        = 'The given URL is incorrect';
$_LANG['Synchronize Client Details'] = 'Synchronize Client Details';
$_LANG['Plesk clients details will be updated']   ='Plesk clients details will be updated';

$_LANG['noDataAvalible']                 = 'No Data Available';
$_LANG['validationErrors']['emptyField'] = 'The field cannot be empty';
$_LANG['bootstrapswitch']['disabled']    = 'Disabled';
$_LANG['bootstrapswitch']['enabled']     = 'Enabled';

$_LANG['addonCA']['pageNotFound']['title']       = 'PAGE NOT FOUND';
$_LANG['addonCA']['pageNotFound']['description'] = 'The provided URL does not exist on this page. If you are sure that an error has occurred here, please contact support.';
$_LANG['addonCA']['pageNotFound']['button']      = 'Return to the product page';

$_LANG['addonCA']['errorPage']['title']       = 'AN ERROR OCCURRED';
$_LANG['addonCA']['errorPage']['description'] = 'An error has occurred. Please contact administrator and pass the details:';
$_LANG['addonCA']['errorPage']['button']      = 'Return to the product page';
$_LANG['addonCA']['errorPage']['error']       = 'ERROR';

$_LANG['addonCA']['errorPage']['errorCode']    = 'Error Code';
$_LANG['addonCA']['errorPage']['errorToken']   = 'Error Token';
$_LANG['addonCA']['errorPage']['errorTime']    = 'Time';
$_LANG['addonCA']['errorPage']['errorMessage'] = 'Message';

$_LANG['addonAA']['pageNotFound']['title']       = 'PAGE NOT FOUND';
$_LANG['addonAA']['pageNotFound']['description'] = 'An error has occurred. Please contact administrator.';
$_LANG['addonAA']['pageNotFound']['button']      = 'Return to the module page';


$_LANG['addonAA']['errorPage']['title']       = 'AN ERROR OCCURRED';
$_LANG['addonAA']['errorPage']['description'] = 'An error has occurred. Please contact administrator and pass the details:';
$_LANG['addonAA']['errorPage']['button']      = 'Return to the module page';
$_LANG['addonAA']['errorPage']['error']       = 'ERROR';

$_LANG['addonAA']['errorPage']['errorCode']    = 'Error Code';
$_LANG['addonAA']['errorPage']['errorToken']   = 'Error Token';
$_LANG['addonAA']['errorPage']['errorTime']    = 'Time';
$_LANG['addonAA']['errorPage']['errorMessage'] = 'Message';

$_LANG['addonAA']['breadcrumbs']['clients'] = 'Clients';
$_LANG['addonAA']['breadcrumbs']['features'] = 'Features';
$_LANG['addonAA']['breadcrumbs']['dashboard'] = 'Dashboard';
$_LANG['addonAA']['breadcrumbs']['configuration'] = 'Configuration';
/* * ********************************************************************************************************************
 *                                                   ERROR CODES                                                        *
 * ******************************************************************************************************************** */
$_LANG['errorCodeMessage']['Uncategorised error occured']         = 'Unexpected Error';
$_LANG['errorCodeMessage']['Database error']                      = 'Database error';
$_LANG['errorCodeMessage']['Provided controller does not exists'] = 'Page not found';
$_LANG['errorCodeMessage']['Invalid Error Code!']                 = 'Unexpected Error';

/* * ********************************************************************************************************************
 *                                                   MODULE REQUIREMENTS                                                *
 * ******************************************************************************************************************** */
$_LANG['unfulfilledRequirement']['In order for the module to work correctly, please remove the following file: :remove_file_requirement:'] = 'In order for the module to work correctly, please remove the following file: :remove_file_requirement:';


/* * ********************************************************************************************************************
 *                                                   ADMIN AREA                                                        *
 * ******************************************************************************************************************** */

$_LANG['addonAA']['pagesLabels']['label']['dashboard']     = 'Dashboard';
$_LANG['addonAA']['pagesLabels']['label']['features']      = 'Features';
$_LANG['addonAA']['pagesLabels']['label']['documentation'] = 'Documentation';
$_LANG['addonAA']['pagesLabels']['label']['Home']          = 'Home';

$_LANG['addonAA']['dashboard']['mainContainer']['productsPage']['productsPage']                             = 'Products';
$_LANG['addonAA']['dashboard']['mainContainer']['productsPage']['table']['id']                              = 'ID';
$_LANG['addonAA']['dashboard']['mainContainer']['productsPage']['table']['groupName']                       = 'Group';
$_LANG['addonAA']['dashboard']['mainContainer']['productsPage']['table']['productName']                     = 'Product';
$_LANG['addonAA']['features']['mainContainer']['featuresPage']['table']['autoinstaller']                    = 'Auto Installer';
$_LANG['addonAA']['dashboard']['mainContainer']['productsPage']['table']['serverType']                      = 'Server Type';
$_LANG['addonAA']['dashboard']['mainContainer']['productsPage']['table']['stats']                           = 'Accounts';
$_LANG['addonAA']['dashboard']['mainContainer']['productsPage']['upgradeButton']['button']['upgradeButton'] = 'Upgrade';
$_LANG['addonAA']['dashboard']['mainContainer']['productsPage']['editProduct']['button']['editProduct']     = 'Edit';
$_LANG['addonAA']['dashboard']['mainContainer']['serversPage']['serversPage']                               = 'Servers';
$_LANG['addonAA']['dashboard']['mainContainer']['serversPage']['table']['id']                               = 'ID';
$_LANG['addonAA']['dashboard']['mainContainer']['serversPage']['table']['name']                             = 'Name';
$_LANG['addonAA']['dashboard']['mainContainer']['serversPage']['table']['accounts_count']                   = 'Accounts';
$_LANG['addonAA']['dashboard']['mainContainer']['serversPage']['table']['type']                             = 'Type';
$_LANG['addonAA']['dashboard']['mainContainer']['serversPage']['editServer']['button']['editServer']        = 'Edit';

$_LANG['addonAA']['features']['mainContainer']['featuresPage']['massCopyButton']['button']['massCopyButton']                   = 'Copy';
$_LANG['addonAA']['features']['mainContainer']['featuresPage']['table']['id']                                                  = 'ID';
$_LANG['addonAA']['features']['mainContainer']['featuresPage']['table']['fullname']                                            = 'Product';
$_LANG['addonAA']['features']['mainContainer']['featuresPage']['table']['enabled_features']                                    = 'Enabled Features';
$_LANG['addonAA']['features']['mainContainer']['featuresPage']['downloadAppsButton']['button']['downloadAppsButton']           = 'Download Applications List';
$_LANG['addonAA']['features']['mainContainer']['featuresPage']['copyButton']['button']['copyButton']                           = 'Copy Configuration';
$_LANG['addonAA']['features']['mainContainer']['featuresPage']['moveToConfigurationPage']['button']['moveToConfigurationPage'] = 'Settings';
$_LANG['addonAA']['dashboard']['mainContainer']['serversPage']['settingsButton']['button']['settingsButton']                   = 'Settings';

$_LANG['addonAA']['dashboard']['settingsModal']['modal']['settingsModal']                       = 'Server Settings';
$_LANG['addonAA']['dashboard']['settingsModal']['settingsForm']['pleskUrl']['pleskUrl']         = 'Log In To Panel';
$_LANG['addonAA']['dashboard']['settingsModal']['settingsForm']['webmailPanel']['webmailPanel'] = 'Log In To Webmail';
$_LANG['addonAA']['dashboard']['settingsModal']['baseAcceptButton']['title']                    = 'Save';
$_LANG['addonAA']['dashboard']['settingsModal']['baseCancelButton']['title']                    = 'Cancel';

$_LANG['serverSettingsUpdated'] = 'The server settings have been updated successfully';

$_LANG['addonAA']['features']['mainContainer']['configurationPage']['featuresSection']['featuresSection']                                                                = 'Features';
$_LANG['addonAA']['features']['mainContainer']['configurationPage']['applicationsSection']['applicationsSettings']                                                       = 'Applications';
$_LANG['addonAA']['features']['mainContainer']['configurationPage']['applicationsSection']['leftSide']['client_install_app']['client_install_app']                       = 'Install Applications';
$_LANG['addonAA']['features']['mainContainer']['configurationPage']['applicationsSection']['leftSide']['client_install_app']['installAppDescription']                    = 'Allow your clients to install one of over 250 applications instantly on their account. This feature requires Softaculous/Installatron to be installed on your Plesk server.';
$_LANG['addonAA']['features']['mainContainer']['configurationPage']['applicationsSection']['leftSide']['autoinstaller']['autoinstaller']                                 = 'Auto Installer Type';
$_LANG['addonAA']['features']['mainContainer']['configurationPage']['applicationsSection']['leftSide']['order_assign']['order_assign']                                   = 'Use Configurable Options';
$_LANG['addonAA']['features']['mainContainer']['configurationPage']['applicationsSection']['leftSide']['app_name']['app_name']                                           = 'Apps / Configurable Options';
$_LANG['addonAA']['features']['mainContainer']['configurationPage']['applicationsSection']['leftSide']['appsConfigOpt']['appsConfigOptDescription']                      = 'Select the applications that will be installed automatically for newly created accounts.';
$_LANG['addonAA']['features']['mainContainer']['configurationPage']['applicationsSection']['leftSide']['autoinstall_on_create']['autoinstall_on_create']                 = 'Auto Install On Creation';
$_LANG['addonAA']['features']['mainContainer']['configurationPage']['applicationsSection']['leftSide']['autoinstaller']['autoinstallerDescription']                      = 'Choose whether applications should be installed by Installatron, Softaculous or a default auto installer.';
$_LANG['addonAA']['features']['mainContainer']['configurationPage']['applicationsSection']['leftSide']['apps_backups']['apps_backups']                                   = 'Application Backups';
$_LANG['addonAA']['features']['mainContainer']['configurationPage']['applicationsSection']['leftSide']['lang']['lang']                                                   = 'Language';
$_LANG['addonAA']['features']['mainContainer']['configurationPage']['applicationsSection']['leftSide']['lang']['auto_apps_backups']['auto_apps_backups']                 = 'Enable Auto Update Backups';
$_LANG['addonAA']['features']['mainContainer']['configurationPage']['applicationsSection']['leftSide']['lang']['auto_apps_backups_default']['auto_apps_backups_default'] = 'Auto Update Backups Enabled By Default On Order';
$_LANG['addonAA']['features']['mainContainer']['configurationPage']['applicationsSection']['leftSide']['lang']['languageDescription']                                    = "Change the language of your application by typing in its two letter code, such as 'en', 'pl', etc.";
$_LANG['addonAA']['features']['mainContainer']['configurationPage']['applicationsSection']['leftSide']['auto_apps_backups']['auto_apps_backups']                         = 'Enable Auto Update Backups';
$_LANG['addonAA']['features']['mainContainer']['configurationPage']['applicationsSection']['leftSide']['auto_apps_backups']['autoUpdateBackups']                         = 'Auto Update Backups';
$_LANG['addonAA']['features']['mainContainer']['configurationPage']['applicationsSection']['leftSide']['auto_apps_backups_default']['auto_apps_backups_default']         = 'Auto Update Backups Enabled By Default On Order';
$_LANG['addonAA']['features']['mainContainer']['configurationPage']['applicationsSection']['leftSide']['auto_apps_backups_default']['defaultOnOrderDescription']         = 'The creation of automatic update backups is enabled by default after the initial provisioning in WHMCS.';
$_LANG['addonAA']['features']['mainContainer']['configurationPage']['applicationsSection']['caTemplateSettings']                                                         = 'Template Settings';
$_LANG['addonAA']['features']['mainContainer']['configurationPage']['applicationsSection']['rightSide']['template']['template']                                          = 'Template';
$_LANG['addonAA']['features']['mainContainer']['configurationPage']['applicationsSection']['rightSide']['template']['templateDescription']                               = 'Template Description';
$_LANG['addonAA']['features']['mainContainer']['configurationPage']['baseSubmitButton']['button']['submit']                                                              = 'Save';
$_LANG['addonAA']['pagesLabels']['features']['configuration']                                                                                                            = ':productName: Configuration';
$_LANG['addonAA']['features']['mainContainer']['configurationPage']['applicationsSection']['leftSide']['app_name']['appNameDescription']                                 = 'Select the applications that will be installed automatically for newly created accounts.';

$_LANG['addonAA']['features']['mainContainer']['configurationPage']['featuresSection']['pleskVersionAlert'] = '<b>Note:</b> This product uses a server in version equal to or higher than 17.8. Listed below functionalities are temporarily unsupported: <ul><li>- Backups</li><li>- Spam Filter (SpamAssassin)</li><li>- Web Users</li></ul>';

$_LANG['addonAA']['features']['copyConfigurationModal']['modal']['copyConfigurationModal']       = 'Copy Configuration';
$_LANG['addonAA']['features']['copyConfigurationModal']['addForm']['fromProduct']['fromProduct'] = 'From';
$_LANG['addonAA']['features']['copyConfigurationModal']['baseAcceptButton']['title']             = 'Save';
$_LANG['addonAA']['features']['copyConfigurationModal']['baseCancelButton']['title']             = 'Cancel';
$_LANG['configurationHasBeenCopied']                                                             = 'The configuration has been copied successfully';
$_LANG['emptySettingsCannotBeCopied']                                                            = 'Empty settings cannot be copied';


$_LANG['addonAA']['dashboard']['upgradeModal']['modal']['upgradeModal']                 = 'Upgrade Product';
$_LANG['addonAA']['dashboard']['upgradeModal']['addForm']['serverGroup']['serverGroup'] = 'Server Group';
$_LANG['addonAA']['dashboard']['upgradeModal']['baseAcceptButton']['title']             = 'Confirm';
$_LANG['addonAA']['dashboard']['upgradeModal']['baseCancelButton']['title']             = 'Cancel';

$_LANG['addonAA']['dashboard']['upgradeModal']['addForm']['upgradeDescription'] = 'You can easily upgrade the current Plesk product to the Plesk Extended one. Simply, choose the new Plesk Extended server group you want to switch to and confirm the upgrade.';

$_LANG['addonAA']['features']['mainContainer']['configurationPage']['applicationsSection']['leftSide']['auto_apps_backups']['autoUpdateBackupsDesc'] = 'Enable clients to toggle automatically created update backups and restore the application from a backup in case its update process fails.';

$_LANG['addonAA']['features']['button']['massCopyButton'] = 'Copy Configuration';
$_LANG['addonAA']['features']['button']['massEditButton'] = 'Edit Settings';
//$_LANG['massFeatureEdit'] = "TEST";

$_LANG['addonAA']['features']['mainContainer']['massConfigurationPage']['featuresSection']['featuresSection']                                                        = 'Features';
$_LANG['addonAA']['features']['mainContainer']['massConfigurationPage']['applicationsSection']['applicationsSettings']                                               = 'Applications';
$_LANG['addonAA']['features']['mainContainer']['massConfigurationPage']['applicationsSection']['leftSide']['client_install_app']['client_install_app']               = 'Install Applications';
$_LANG['addonAA']['features']['mainContainer']['massConfigurationPage']['applicationsSection']['leftSide']['client_install_app']['installAppDescription']            = 'Allow your clients to install one of over 250 applications instantly on their account. This feature requires Softaculous/Installatron to be installed on your Plesk server.';
$_LANG['addonAA']['features']['mainContainer']['massConfigurationPage']['applicationsSection']['leftSide']['autoinstaller']['autoinstaller']                         = 'Auto Installer Type ';
$_LANG['addonAA']['features']['mainContainer']['massConfigurationPage']['applicationsSection']['leftSide']['autoinstaller']['autoinstallerDescription']              = 'Choose whether applications should be installed by Installatron, Softaculous or a default auto installer.';
$_LANG['addonAA']['features']['mainContainer']['massConfigurationPage']['applicationsSection']['leftSide']['autoinstall_on_create']['autoinstall_on_create']         = 'Auto Install On Creation';
$_LANG['addonAA']['features']['mainContainer']['massConfigurationPage']['applicationsSection']['leftSide']['order_assign']['order_assign']                           = 'Use Configurable Options';
$_LANG['addonAA']['features']['mainContainer']['massConfigurationPage']['applicationsSection']['leftSide']['lang']['lang']                                           = 'Language';
$_LANG['addonAA']['features']['mainContainer']['massConfigurationPage']['applicationsSection']['leftSide']['lang']['languageDescription']                            = "Change the language of your application by typing in its two letter code, such as 'en', 'pl', etc.";
$_LANG['addonAA']['features']['mainContainer']['massConfigurationPage']['applicationsSection']['leftSide']['auto_apps_backups']['auto_apps_backups']                 = 'Enable Auto Update Backups';
$_LANG['addonAA']['features']['mainContainer']['massConfigurationPage']['applicationsSection']['leftSide']['auto_apps_backups']['autoUpdateBackupsDesc']             = 'Enable clients to toggle automatically created update backups and restore the application from a backup in case its update process fails.';
$_LANG['addonAA']['features']['mainContainer']['massConfigurationPage']['applicationsSection']['leftSide']['auto_apps_backups_default']['auto_apps_backups_default'] = 'Auto Update Backups Enabled By Default On Order';
$_LANG['addonAA']['features']['mainContainer']['massConfigurationPage']['applicationsSection']['leftSide']['auto_apps_backups_default']['defaultOnOrderDescription'] = 'The creation of automatic update backups is enabled by default after the initial provisioning in WHMCS.';
$_LANG['addonAA']['features']['mainContainer']['massConfigurationPage']['baseSubmitButton']['button']['submit']                                                      = 'Submit';
$_LANG['addonAA']['pagesLabels']['features']['massConfiguration']                                                                                                    = 'Mass Products Configuration';

/* Feature Page */
$_LANG['addonAA']['features']['Email Addresses']              = 'Email Addresses';
$_LANG['addonAA']['features']['Email Forwarders']             = 'Email Forwarders';
$_LANG['addonAA']['features']['Spam Filter (SpamAssassin)']   = 'Spam Filter (SpamAssassin)';
$_LANG['addonAA']['features']['Plesk']                        = 'Plesk';
$_LANG['addonAA']['features']['Webmail']                      = 'Webmail';
$_LANG['addonAA']['features']['Addon Domains']                = 'Addon Domains';
$_LANG['addonAA']['features']['Domain Aliases']               = 'Domain Aliases';
$_LANG['addonAA']['features']['Subdomains']                   = 'Subdomains';
$_LANG['addonAA']['features']['FTP Access']                   = 'FTP Access';
$_LANG['addonAA']['features']['Databases']                    = 'Databases';
$_LANG['addonAA']['features']['DNS Settings']                 = 'DNS Settings';
$_LANG['addonAA']['features']['Applications']                 = 'Applications';
$_LANG['addonAA']['features']['Web Users']                    = 'Web Users';
$_LANG['addonAA']['features']['SSL Certificates']             = 'SSL Certificates';
$_LANG['addonAA']['features']['Backups']                      = 'Backups';
$_LANG['addonAA']['features']['Git Repositories']             = 'Git Repositories';
$_LANG['addonAA']['features']['NodeJs']                       = 'Node.js';
$_LANG['addonAA']['features']['Log Rotation'] = 'Log Rotation';
$_LANG['addonAA']['features']['PHP Settings'] = 'PHP Settings';

$_LANG['addonAA']['pagesLabels']['label']['Features']         = 'Features';
$_LANG['addonAA']['pagesLabels']['Features']['configuration'] = ':productName: Configuration';

$_LANG['addonAA']['features']['Installation App']                                                          = "Installation App";
$_LANG['addonAA']['features']['Configurable options for %s product']                                       = "Configurable options for %s product";
$_LANG['addonAA']['features']['Auto generated by module %s']                                               = "Auto generated by module %s";
$_LANG['addonAA']['features']['mainContainer']['featuresPage']['appConfButton']['button']['appConfButton'] = 'Generate Auto Installer\'s Applications Configurable Options';
$_LANG['addonAA']['features']['appConfModal']['modal']['appConfModal']                                     = 'Generate Configurable Options';
$_LANG['addonAA']['features']['appConfModal']['baseAcceptButton']['title']                                 = 'Confirm';
$_LANG['addonAA']['features']['appConfModal']['baseCancelButton']['title']                                 = 'Cancel';

$_LANG['addonAA']['clients']['modalClientInformation']['formClientInformation']['listLabel']['login']          = "Login";
$_LANG['addonAA']['clients']['modalClientInformation']['formClientInformation']['listLabel']['email']          = "Email";
$_LANG['addonAA']['clients']['modalClientInformation']['formClientInformation']['listLabel']['country']        = "Country";
$_LANG['addonAA']['clients']['modalClientInformation']['formClientInformation']['listLabel']['locale']         = "Locale";
$_LANG['addonAA']['clients']['modalClientInformation']['formClientInformation']['listLabel']['active_domains'] = "Active Domains";
$_LANG['addonAA']['clients']['modalClientInformation']['formClientInformation']['listLabel']['subdomains']     = "Subdomains";
$_LANG['addonAA']['clients']['modalClientInformation']['formClientInformation']['listLabel']['redirects']      = "Redirects";
$_LANG['addonAA']['clients']['modalClientInformation']['formClientInformation']['listLabel']['mail_groups']    = "Mail Groups";

$_LANG['addonAA']['clients']['modalClientInformation']['formClientInformation']['listLabel']['mail_resps']                 = "Mail Resps";
$_LANG['addonAA']['clients']['modalClientInformation']['formClientInformation']['listLabel']['mail_lists']                 = "Mail Lists";
$_LANG['addonAA']['clients']['modalClientInformation']['formClientInformation']['listLabel']['web_users']                  = "Web Users";
$_LANG['addonAA']['clients']['modalClientInformation']['formClientInformation']['listLabel']['data_bases']                 = "Databases";
$_LANG['addonAA']['clients']['modalClientInformation']['formClientInformation']['listLabel']['traffic']                    = "Traffic";
$_LANG['addonAA']['clients']['modalClientInformation']['modal']['modalClientInformation']                                  = "Plesk Client Information";
$_LANG['addonAA']['clients']['modalClientInformation']['baseCancelButton']['title']                                        = "Close";
$_LANG['addonAA']['pagesLabels']['label']['clients']                                                                       = 'Clients';
$_LANG['addonAA']['pagesLabels']['label']['Clients']                                                                       = 'Clients';
$_LANG['addonAA']['clients']['mainContainer']['dataTable']['table']['email']                                               = 'Email';
$_LANG['addonAA']['dashboard']['mainContainer']['serversPage']['loginToWhmName']['button']['loginToWhm']                   = 'Log In To Plesk';
$_LANG['addonAA']['pagesLabels']['label']['Dashboard']                                                                     = 'Dashboard';
$_LANG['addonAA']['features']['mainContainer']['featuresPage']['table']['group']                                           = "Group";
$_LANG['addonAA']['pagesLabels']['Features']['massConfiguration']                                                          = "Bulk Configuration";
$_LANG['addonAA']['features']['appConfModal']['appConfForm']['enableAllSection']['enableAll']['enableAll']                 = "Toggle All";
$_LANG['addonAA']['features']['appConfModal']['appConfForm']['productRelatedCo']                                           = "The selected product already has some Configurable Options related. Before confirming, make sure that the applications list has not been generated before. After confirmation, the link will be deleted.";

$_LANG['addonAA']['clients']['mainContainer']['clientDataTable']['table']['uid'] = 'ID';
$_LANG['addonAA']['clients']['mainContainer']['clientDataTable']['table']['clientName'] = 'Client';
$_LANG['addonAA']['clients']['mainContainer']['clientDataTable']['table']['email'] = 'Email';
$_LANG['addonAA']['clients']['mainContainer']['clientDataTable']['buttonClientInformation']['button']['buttonClientInformation'] = 'More Information';
$_LANG['addonAA']['clients']['mainContainer']['clientDataTable']['table']['customerid']                                          = "Customer ID";
$_LANG['addonAA']['clients']['mainContainer']['clientDataTable']['table']['serverid']                                            = "Server";
$_LANG['addonAA']['features']['mainContainer']['configurationPage']['featuresSection']['pleskResselerAccountAlert']              = "The Reseller Plesk account belongs to the group with limited API restrictions.<br>The below features have been disabled:<br><ul><li>- Default Applications Installer</li><li>- DB types retrieved from server</li><li>- PHP section from the form for the new addon domain.</li><li>- SSL Certificates section.</li></ul>";


