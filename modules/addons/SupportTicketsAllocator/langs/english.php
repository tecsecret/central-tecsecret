<?php
/**********************************************************************
 * Tickets Allocator product developed. (2015-03-17)
 * *
 *
 *  CREATED BY MODULESGARDEN       ->       http://modulesgarden.com
 *  CONTACT                        ->       contact@modulesgarden.com
 *
 *
 * This software is furnished under a license and may be used and copied
 * only  in  accordance  with  the  terms  of such  license and with the
 * inclusion of the above copyright notice.  This software  or any other
 * copies thereof may not be provided or otherwise made available to any
 * other person.  No title to and  ownership of the  software is  hereby
 * transferred.
 *
 *
 **********************************************************************/

$_LANG['generalError']  = "Something Goes Wrong, check logs, contact admin";
$_LANG['token'] ="Token";

$_LANG['pagesTitles']['configurations'] = 'Configuration';
$_LANG['pagesTitles']['departments']    = 'Departments';
$_LANG['pagesTitles']['adminGroups']    = 'Rules';
$_LANG['pagesTitles']['dashboard'] = 'Dashboard';
$_LANG['pagesTitles']['documentation'] = 'Documentation';



$_LANG['pages']['departments']['header']  = 'Departments List';
$_LANG['pages']['departments']['table']['id']  = 'ID';
$_LANG['pages']['departments']['table']['name']  = 'Name';
$_LANG['pages']['departments']['table']['email']  = 'Email';
$_LANG['pages']['departments']['table']['administrators']  = 'Assigned Admin Users';
$_LANG['pages']['departments']['table']['officeHours']  = 'Office Hours';
$_LANG['pages']['departments']['table']['hipChatRoom']  = 'HipChat Room';
$_LANG['pages']['departments']['table']['action']  = 'Action';
$_LANG['pages']['departments']['search']  = 'Search';
$_LANG['pages']['departments']['previous']  = 'Previous';
$_LANG['pages']['departments']['next']  = 'Next';
$_LANG['pages']['departments']['noDepartmentsAvaiable']  = 'No Departments Available';
$_LANG['pages']['departments']['noDepartmentsAvaiableInfo']  = '';
$_LANG['pages']['departments']['modal']['close']  = 'Close';
$_LANG['pages']['departments']['modal']['editLabel']  = 'Setting For ';
$_LANG['pages']['departments']['editOfficeHours']['open']['label'] = 'Open';
$_LANG['pages']['departments']['editOfficeHours']['close']['label'] = 'Close';
$_LANG['pages']['departments']['close']['label'] = 'Close';
$_LANG['pages']['departments']['officeHours'] = "%s - %s";
$_LANG['pages']['departments']['messages']['editSuccess'] = "Setting For %s have been saved successfully";
$_LANG['pages']['departments']['modal']['saveChanges'] ='Save Changes';
$_LANG['pages']['departments']['modal']['Time Range:'] ='Time Range:';
$_LANG['pages']['departments']['editOfficeHours']['open']['placeholder'] = "07:00:00";
$_LANG['pages']['departments']['editOfficeHours']['close']['placeholder'] = "21:00:00";
$_LANG['pages']['departments']['editOfficeHours']['officeHours'] = "Office Hours";

$_LANG['pages']['departments']['editOfficeHours']['departmentReserve']['label'] ="Reserve Department";
$_LANG['pages']['departments']['editOfficeHours']['departmentReserve']['description'] ='Reserve Department for the ticket';
$_LANG['pages']['departments']['editOfficeHours']['hipChatRoom']['label']  = 'HipChat Room';
$_LANG['pages']['departments']['editOfficeHours']['firstAdmin']['label']  = '1st Line';
$_LANG['pages']['departments']['editOfficeHours']['secondAdmin']['label']  = '2nd Line';
$_LANG['pages']['departments']['editOfficeHours']['thirdAdmin']['label']  = '3rd Line';


$_LANG['pages']['departments']['actionButtons']['edit'] ="Edit";

$_LANG['pages']['adminGroups']['header']  = 'Rules List';
$_LANG['pages']['adminGroups']['table']['id']  = 'ID';
$_LANG['pages']['adminGroups']['table']['name']  = 'Name';
$_LANG['pages']['adminGroups']['table']['administrators']  = 'Assigned Admin Users';
$_LANG['pages']['adminGroups']['table']['action']  = 'Actions';
$_LANG['pages']['adminGroups']['search']  = 'Search';
$_LANG['pages']['adminGroups']['previous']  = 'Previous';
$_LANG['pages']['adminGroups']['next']  = 'Next';
$_LANG['pages']['adminGroups']['noAvaiable']  = 'No Rules Available';
$_LANG['pages']['adminGroups']['noAvaiableInfo']  = '';
$_LANG['pages']['adminGroups']['modal']['close']  = 'Close';
$_LANG['pages']['adminGroups']['modal']['editLabel']  = 'Rule:  ';
$_LANG['pages']['adminGroups']['taAdminGroupEdit']['keywords']['label'] = 'Keywords';
$_LANG['pages']['adminGroups']['taAdminGroupEdit']['keywords']['description'] = 'Keywords searched in the ticket';
$_LANG['pages']['adminGroups']['taAdminGroupEdit']['addKkeywords']['label'] = 'Add Keywords';
$_LANG['pages']['adminGroups']['taAdminGroupEdit']['addKkeywords']['description']  = 'Keywords searched in the ticket (comma separator without spaces)';
$_LANG['pages']['adminGroups']['taAdminGroupEdit']['products']['label'] = 'Products';
$_LANG['pages']['adminGroups']['taAdminGroupEdit']['products']['label'] = 'Products';
$_LANG['pages']['adminGroups']['taAdminGroupEdit']['ticketPriorities']['label'] = 'Ticket Priorities';
$_LANG['pages']['adminGroups']['taAdminGroupEdit']['adminMaximumTickets']['label'] = 'Maximum Tickets';
$_LANG['pages']['adminGroups']['taAdminGroupEdit']['adminMaximumTickets']['description'] = 'Maximum number of tickets opened per Admin';
$_LANG['pages']['adminGroups']['messages']['editSuccess'] = "Rule for %s has been edited successfully";
$_LANG['pages']['adminGroups']['modal']['saveChanges'] ='Save Changes';
$_LANG['pages']['adminGroups']['table']['maxTickets'] ="Max Tickets";
$_LANG['pages']['adminGroups']['button']['Add'] ="Add Rule";
$_LANG['pages']['adminGroups']['modal']['addGroup']  = "New Rule";
$_LANG['pages']['adminGroups']['taAdminGroupEdit']['name']['label'] ="Name";
$_LANG['pages']['adminGroups']['taAdminGroupEdit']['name']['description'] ='';
$_LANG['pages']['adminGroups']['taAdminGroupEdit']['admins']['label'] = "Admin Users";
$_LANG['pages']['adminGroups']['taAdminGroupEdit']['admins']['description'] = 'Assigned Admin Users';
$_LANG['pages']['adminGroups']['taAdminGroupAdd'] = $_LANG['pages']['adminGroups']['taAdminGroupEdit'];
$_LANG['pages']['adminGroups']['Add Keywords'] = 'Add Keywords';
$_LANG['pages']['adminGroups']['Rule'] = "Rule";
$_LANG['pages']['adminGroups']['keywordsAddDesc'] ='Keywords searched in the ticket (comma separator without spaces)';
$_LANG['pages']['adminGroups']['ADD']  ='ADD';
$_LANG['pages']['adminGroups']['OR']  ='OR';
$_LANG['pages']['adminGroups']['AND']  ='AND';
$_LANG['pages']['adminGroups']['messages']['addSuccess'] = "Rule for %s has been saved successfully";
$_LANG['pages']['adminGroups']['modal']['deleteLabel'] = "Delete";
$_LANG['pages']['adminGroups']['modal']['deleteGroup']  ="The rule will be deleted. Are you sure you would like to continue?";
$_LANG['pages']['adminGroups']['modal']['delete'] ="Delete";
$_LANG['pages']['adminGroups']['messages']['deleteSuccess'] = "Server %s has been deleted successfully";
$_LANG['pages']['adminGroups']['modal']['help'] = 'Help';
$_LANG['pages']['adminGroups']['modal']['helpTip'] ="Tickets fall in to this rule if this relation has been met: (Keywords OR Product) AND Priority.\r\n
If you leave Ticket Priorities empty, relation will change to: Keywords OR Product.\r\n
There is OR relation between keyword groups.";
$_LANG['pages']['adminGroups']['actionButtons']['edit'] ="Edit";
$_LANG['pages']['adminGroups']['actionButtons']['delete'] ="Delete";
$_LANG['pages']['adminGroups']['key1,key2'] ='key1,key2';
$_LANG['pages']['adminGroups']['errorMessages']['emptyKeywords'] = 'A keyword group was not saved, press \'ADD\' to save it.';

$_LANG['pages']['configurations']['header']['configurations'] ='Configuration';
$_LANG['pages']['configurations']['header']['default']  = 'Default  Rules';
$_LANG['pages']['configurations']['header']['chipChat'] = 'Hip Chat';
$_LANG['pages']['configurations']['header']['tickets'] ='Tickets';
        
$_LANG['pages']['configurations']['button']['save'] ='Save Changes';
$_LANG['pages']['configurations']['configuration']['keywords']['label'] = 'Keywords';
$_LANG['pages']['configurations']['configuration']['keywords']['description'] = 'Keywords list';
$_LANG['pages']['configurations']['Add Keywords']= 'Add Keywords';
$_LANG['pages']['configurations']['Rule']  ='Rule:';
$_LANG['pages']['configurations']['keywordsAddDesc'] = 'Keywords searched in the ticket (comma separator without spaces)';
$_LANG['pages']['configurations']['ADD']  ='ADD';
$_LANG['pages']['configurations']['OR']  ='OR';
$_LANG['pages']['configurations']['AND']  ='AND';
$_LANG['pages']['configurations']['configuration']['products']['label'] = 'Products';
$_LANG['pages']['configurations']['configuration']['products']['label'] = 'Products';
$_LANG['pages']['configurations']['configuration']['ticketPriorities']['label'] = 'Ticket Priorities';
$_LANG['pages']['configurations']['configuration']['adminMaximumTickets']['label'] = 'Maximum Tickets';
$_LANG['pages']['configurations']['configuration']['adminMaximumTickets']['description'] = 'Maximum number of tickets opened per Admin';
$_LANG['pages']['configurations']['configuration']['ticketNoReplyNotificationEmail']['label'] = 'No Reply Email Notification ';
$_LANG['pages']['configurations']['configuration']['ticketNoReplyNotificationEmail']['options']['on']   = 'Tick to enable - email notification';
$_LANG['pages']['configurations']['configuration']['ticketNoReplyNotificationHipChat']['label'] = 'No Reply HipChat Notification ';
$_LANG['pages']['configurations']['configuration']['ticketNoReplyNotificationHipChat']['options']['on'] = 'Tick to enable - HipChat notification';
$_LANG['pages']['configurations']['configuration']['hipChatNotification']['label'] ='Hip Chat Notification';
$_LANG['pages']['configurations']['configuration']['hipChatNotification']['options']['on'] = 'Tick to enable - HipChat notification';
$_LANG['pages']['configurations']['configuration']['hipChatApiToken']['label'] ='HipChat API Token';
$_LANG['pages']['configurations']['configuration']['hipChatApiToken']['description'] ='HipChat API requires an auth_token variable';
$_LANG['pages']['configurations']['configuration']['ticketAssignmentByOnlineAdmins']['label'] = 'Online Admins';
$_LANG['pages']['configurations']['configuration']['ticketAssignmentByOnlineAdmins']['options']['on']  = 'Tick to enable - tickets will be assigned to online admins only';
$_LANG['pages']['configurations']['configuration']['ticketAssignmentByOfficeHours']['label'] = 'Office Hours';
$_LANG['pages']['configurations']['configuration']['ticketAssignmentByOfficeHours']['options']['on']  = 'Tick to enable - assignment by office hours';
$_LANG['pages']['configurations']['messages']['testSuccess'] ='Success';
$_LANG['pages']['configurations']['configuration']['testConnectionHipChat']['label'] = 'Test Connection';
$_LANG['pages']['configurations']['configuration']['ticketNotReplyTime']['label'] = 'No Reply Time';
$_LANG['pages']['configurations']['configuration']['ticketNotReplyTime']['description'] = 'Ticket will be assigned to another admin if the time has passed. (Leave blank for disabled)';
$_LANG['pages']['configurations']['configuration']['noReplyNotificationTime']['label'] = 'No Reply Notification Time';
$_LANG['pages']['configurations']['configuration']['noReplyNotificationTime']['description'] ='Notification will be sent if the time has passed.';
$_LANG['pages']['configurations']['configuration']['localApiUser']['label'] = 'Admin API Used';
$_LANG['pages']['configurations']['configuration']['localApiUser']['description'] ='Choose admin used for API connection. He will be excluded from the list of online admins.';
$_LANG['pages']['configurations']['configuration']['ticketRejection']['label'] ='Ticket Rejection';
$_LANG['pages']['configurations']['configuration']['ticketRejection']['options']['on'] ='Tick to enable - ticket rejection ';
$_LANG['pages']['configurations']['configuration']['departmentNoreplyTime']['label'] = 'Department No Reply Time';
$_LANG['pages']['configurations']['configuration']['departmentNoreplyTime']['description'] = 'Ticket will be assigned to another department if the time has passed. (Leave blank for disabled)';
$_LANG['pages']['configurations']['configuration']['useDepartmentReserve']['label'] ='Reserve Departments';
$_LANG['pages']['configurations']['configuration']['useDepartmentReserve']['options']['on'] = 'Tick to enable - reserve departments';
$_LANG['pages']['configurations']['configuration']['departmentReserve']['label'] = 'Default Reserve Department';
$_LANG['pages']['configurations']['configuration']['departmentReserve']['description'] ='';
$_LANG['pages']['configurations']['header']['cron'] = "Cron Job";
$_LANG['pages']['configurations']['cronURL']='Cron job  configuration  (each 5 minutes suggested): ';
$_LANG['pages']['configurations']['header']['settings']  ='Settings';
$_LANG['pages']['configurations']['header']['notifications'] ='Notifications';
$_LANG['pages']['configurations']['configuration']['noReplyNotificationTime']['info'] ="[min]";
$_LANG['pages']['configurations']['configuration']['ticketNotReplyTime']['info']="[min]";
$_LANG['pages']['configurations']['configuration']['hipChatNotify']['label'] ='HipChat Notify';
$_LANG['pages']['configurations']['configuration']['hipChatNotify']['options']['on']  = 'Tick to enable - HipChat Notify';
$_LANG['pages']['configurations']['configuration']['hipChatMessageColor']['label'] ='HipChat Message Color';
$_LANG['pages']['configurations']['configuration']['hipChatMessageColor']['description']='Background color for message.';
$_LANG['pages']['configurations']['configuration']['hipChatMessageColor']['options']['yellow']="Yellow";
$_LANG['pages']['configurations']['configuration']['hipChatMessageColor']['options']['red']  ="Red";
$_LANG['pages']['configurations']['configuration']['hipChatMessageColor']['options']['green'] ='Green';
$_LANG['pages']['configurations']['configuration']['hipChatMessageColor']['options']['purple'] ='Purple';
$_LANG['pages']['configurations']['configuration']['hipChatMessageColor']['options']['gray'] ='Green';
$_LANG['pages']['configurations']['configuration']['hipChatMessageColor']['options']['random'] ='Random';
$_LANG['pages']['configurations']['configuration']['hipChatMessageColor']['options'][''] ='';
$_LANG['pages']['configurations']['configuration']['hipChatNewTicketNotification']['label'] = 'New Ticket HipChat Notification';
$_LANG['pages']['configurations']['configuration']['hipChatNewTicketNotification']['options']['on'] = 'Tick to enable -  new ticket HipChat notification';
$_LANG['pages']['configurations']['configuration']['hipChatNewTicketPrivateNotification']['label'] = 'New Ticket HipChat Private Notification';
$_LANG['pages']['configurations']['configuration']['hipChatNewTicketPrivateNotification']['options']['on'] = 'Tick to enable -  notify administrator by private message';
$_LANG['pages']['configurations']['configuration']['hipChatNoReplyPrivateNotification']['label'] = 'No Reply HipChat Private Notification';
$_LANG['pages']['configurations']['configuration']['hipChatNoReplyPrivateNotification']['options']['on'] = 'Tick to enable -  notify administrator by private message';
$_LANG['pages']['configurations']['messages']['changedSaved'] = 'Changes Saved';

 $_LANG['pages']['cron']['This is a notice that ticket'] ='This is a notice that a ticket';
 $_LANG['pages']['cron']['is waiting for answer.']='is waiting for an answer.';
 $_LANG['pages']['cron']['Client:']='Client:';
 $_LANG['pages']['cron']['Department:'] ='Department:';
 $_LANG['pages']['cron']['Subject:']='Subject:';
$_LANG['pages']['cron']['Priority:'] ='Priority:';

$_LANG['pages']['dashboard']['header']['summary'] ="Summary";
$_LANG['pages']['dashboard']['openTickets'] = 'Open Tickets: ';
$_LANG['pages']['dashboard']['assignedTickets']  = 'Assigned Tickets: ';
$_LANG['pages']['dashboard']['ticketWaitingAssign']  = 'Ticket Waiting For Assignment: ';
$_LANG['pages']['dashboard']['header']['staff'] ='Staff Online';
$_LANG['pages']['dashboard']['table']['username'] ='Username';
$_LANG['pages']['dashboard']['table']['assignedTickets2'] ='Assigned Tickets';
$_LANG['pages']['dashboard']['table']['groupFree'] ="Group / Free Tickets"; 
$_LANG['pages']['dashboard']['header']['cron'] ="Automation";
$_LANG['pages']['dashboard']['automationInfo'] = 'Last Cron Run Info';
$_LANG['pages']['dashboard']['cronLastRun'] ='Cron Last Run: ';
$_LANG['pages']['dashboard']['cron']['minutes']  ='minutes';
$_LANG['pages']['dashboard']['cron']['hours']  ='hours';
$_LANG['pages']['dashboard']['cron']['seconds']  ='seconds';
$_LANG['pages']['dashboard']['ago']  ='ago';
$_LANG['pages']['dashboard']['checkTimePeriod'] = 'Check Time Period:';
$_LANG['pages']['dashboard']['every'] ='every';
$_LANG['pages']['dashboard']['unassignedTickets'] ='Unassigned Tickets:';
$_LANG['pages']['dashboard']['assignmentEfficiency'] ='Assignment Efficiency';
$_LANG['pages']['dashboard']['unassignedTicketsTip'] ='Tickets which slip through defined rules of assignment.';
$_LANG['pages']['dashboard']['serverTime'] ='Server Time: ';

$_LANG['hook']['rejectTicket']='Reject Ticket';
$_LANG['hook']['ticketrejected'] = 'Ticket Rejected: ';
$_LANG['hook']['cancel'] = 'Cancel';

$_LANG['pagesTitles']['logs'] ="Logs";
$_LANG['pages']['logs']['header']  ="Logs";
$_LANG['pages']['logs']['Delete All Entries'] ="Delete All Entries";
$_LANG['pages']['logs']['modal']['deleteLabel'] ="Delete All Entries";
$_LANG['pages']['logs']['modal']['close'] ="Close";
$_LANG['pages']['logs']['modal']['delete'] ="Delete";
$_LANG['pages']['logs']['modal']['deleteLogs'] ="Are you sure you want to delete all entries from logs list?";
$_LANG['pages']['logs']['search'] ="Search";
$_LANG['pages']['logs']['table']['ticketID'] ="Ticket ID";
$_LANG['pages']['logs']['table']['message'] ="Message";
$_LANG['pages']['logs']['table']['date'] ="Date";
$_LANG['pages']['logs']['table']['Action']="Action";
$_LANG['pages']['logs']['noAvailable'] ='No Logs Available';
$_LANG['pages']['logs']['noAvailableInfo'] ='';
$_LANG['pages']['logs']['previous']="Previous";
$_LANG['pages']['logs']['next']="Next";
$_LANG['pages']['logs']['actionButtons']['delete'] ='Delete';

$_LANG['pages']['logs']['modal']['deleteLabel2'] ="Delete Log";
$_LANG['pages']['logs']['modal']['deleteOne'] ="Are you sure you want to delete this entry from logs list?";
$_LANG['pages']['logs']['messages']['deleteSuccess'] = "Log %s has been deleted successfully";
$_LANG['pages']['logs']['messages']['deleteAllSuccess']= "Logs have been deleted successfully";


