<?php
/**********************************************************************
 * Tickets Allocator product developed. (2015-03-17)
 * *
 *
 *  CREATED BY MODULESGARDEN       ->       http://modulesgarden.com
 *  CONTACT                        ->       contact@modulesgarden.com
 *
 *
 * This software is furnished under a license and may be used and copied
 * only  in  accordance  with  the  terms  of such  license and with the
 * inclusion of the above copyright notice.  This software  or any other
 * copies thereof may not be provided or otherwise made available to any
 * other person.  No title to and  ownership of the  software is  hereby
 * transferred.
 *
 *
 **********************************************************************/

$_LANG['generalError']  = "Something Goes Wrong, check logs, contact admin";
$_LANG['token'] ="Token";

$_LANG['pagesTitles']['configurations'] = 'Configurações';
$_LANG['pagesTitles']['departments']    = 'Departamentos';
$_LANG['pagesTitles']['adminGroups']    = 'Regras';
$_LANG['pagesTitles']['dashboard'] = 'Dashboard';
$_LANG['pagesTitles']['documentation'] = 'Documentação';



$_LANG['pages']['departments']['header']  = 'Lista de Departamentos';
$_LANG['pages']['departments']['table']['id']  = 'ID';
$_LANG['pages']['departments']['table']['name']  = 'Nome';
$_LANG['pages']['departments']['table']['email']  = 'E-mail';
$_LANG['pages']['departments']['table']['administrators']  = 'Usuários de Administrador Atribuídos';
$_LANG['pages']['departments']['table']['officeHours']  = 'Horário de Expediente';
$_LANG['pages']['departments']['table']['hipChatRoom']  = 'Sala HipChat';
$_LANG['pages']['departments']['table']['action']  = 'Ação';
$_LANG['pages']['departments']['search']  = 'Busca';
$_LANG['pages']['departments']['previous']  = 'Anterior';
$_LANG['pages']['departments']['next']  = 'Próxima';
$_LANG['pages']['departments']['noDepartmentsAvaiable']  = 'Não há departamento disponíveis';
$_LANG['pages']['departments']['noDepartmentsAvaiableInfo']  = '';
$_LANG['pages']['departments']['modal']['close']  = 'Fechar';
$_LANG['pages']['departments']['modal']['editLabel']  = 'Configuração para ';
$_LANG['pages']['departments']['editOfficeHours']['open']['label'] = 'Aberto';
$_LANG['pages']['departments']['editOfficeHours']['close']['label'] = 'Fechado';
$_LANG['pages']['departments']['close']['label'] = 'Fechado';
$_LANG['pages']['departments']['officeHours'] = "%s - %s";
$_LANG['pages']['departments']['messages']['editSuccess'] = "Configuração para %s foram salvos com sucesso";
$_LANG['pages']['departments']['modal']['saveChanges'] ='Salvar Alteraçõeses';
$_LANG['pages']['departments']['modal']['Time Range:'] ='Intervalo de Tempo:';
$_LANG['pages']['departments']['editOfficeHours']['open']['placeholder'] = "07:00:00";
$_LANG['pages']['departments']['editOfficeHours']['close']['placeholder'] = "21:00:00";
$_LANG['pages']['departments']['editOfficeHours']['officeHours'] = "Horário de Trabalho";

$_LANG['pages']['departments']['editOfficeHours']['departmentReserve']['label'] ="Departamento de Reserva";
$_LANG['pages']['departments']['editOfficeHours']['departmentReserve']['description'] ='Departamento de Reserva para o ticket';
$_LANG['pages']['departments']['editOfficeHours']['hipChatRoom']['label']  = 'Sala HipChat';
$_LANG['pages']['departments']['editOfficeHours']['firstAdmin']['label']  = '1ª Linha';
$_LANG['pages']['departments']['editOfficeHours']['secondAdmin']['label']  = '2ª Linha';
$_LANG['pages']['departments']['editOfficeHours']['thirdAdmin']['label']  = '3ª Linha';


$_LANG['pages']['departments']['actionButtons']['edit'] ="Editar";

$_LANG['pages']['adminGroups']['header']  = 'Lista de Regras';
$_LANG['pages']['adminGroups']['table']['id']  = 'ID';
$_LANG['pages']['adminGroups']['table']['name']  = 'Nome';
$_LANG['pages']['adminGroups']['table']['administrators']  = 'Usuários Admin Atribuídos';
$_LANG['pages']['adminGroups']['table']['action']  = 'Ações';
$_LANG['pages']['adminGroups']['search']  = 'Buscar';
$_LANG['pages']['adminGroups']['previous']  = 'Anterior';
$_LANG['pages']['adminGroups']['next']  = 'Próximo';
$_LANG['pages']['adminGroups']['noAvaiable']  = 'Não há regras disponíveis';
$_LANG['pages']['adminGroups']['noAvaiableInfo']  = '';
$_LANG['pages']['adminGroups']['modal']['close']  = 'Fechar';
$_LANG['pages']['adminGroups']['modal']['editLabel']  = 'Regra:  ';
$_LANG['pages']['adminGroups']['taAdminGroupEdit']['keywords']['label'] = 'palavras-chaves';
$_LANG['pages']['adminGroups']['taAdminGroupEdit']['keywords']['description'] = 'Palavras-chave pesquisadas no ticket';
$_LANG['pages']['adminGroups']['taAdminGroupEdit']['addKkeywords']['label'] = 'Add Palavra-crave';
$_LANG['pages']['adminGroups']['taAdminGroupEdit']['addKkeywords']['description']  = 'Palavras-chave pesquisadas no ticket (separador de v�rgulas sem espa�os)';
$_LANG['pages']['adminGroups']['taAdminGroupEdit']['products']['label'] = 'Produtos';
$_LANG['pages']['adminGroups']['taAdminGroupEdit']['products']['label'] = 'Produtos';
$_LANG['pages']['adminGroups']['taAdminGroupEdit']['ticketPriorities']['label'] = 'Propriedades do Ticket';
$_LANG['pages']['adminGroups']['taAdminGroupEdit']['adminMaximumTickets']['label'] = 'Máximo de Tickets';
$_LANG['pages']['adminGroups']['taAdminGroupEdit']['adminMaximumTickets']['description'] = 'Número máximo de ticket abertos por Admin';
$_LANG['pages']['adminGroups']['messages']['editSuccess'] = "Regra para %s foi editado com sucesso";
$_LANG['pages']['adminGroups']['modal']['saveChanges'] ='Salvar Alterações';
$_LANG['pages']['adminGroups']['table']['maxTickets'] ="Max Tickets";
$_LANG['pages']['adminGroups']['button']['Add'] ="Add Regra";
$_LANG['pages']['adminGroups']['modal']['addGroup']  = "Nova Regra";
$_LANG['pages']['adminGroups']['taAdminGroupEdit']['name']['label'] ="Nome";
$_LANG['pages']['adminGroups']['taAdminGroupEdit']['name']['description'] ='';
$_LANG['pages']['adminGroups']['taAdminGroupEdit']['admins']['label'] = "Usuários Admin";
$_LANG['pages']['adminGroups']['taAdminGroupEdit']['admins']['description'] = 'Atribuído ao usuário Admin';
$_LANG['pages']['adminGroups']['taAdminGroupAdd'] = $_LANG['pages']['adminGroups']['taAdminGroupEdit'];
$_LANG['pages']['adminGroups']['Add Keywords'] = 'Add palavra-chave';
$_LANG['pages']['adminGroups']['Rule'] = "Regra";
$_LANG['pages']['adminGroups']['keywordsAddDesc'] ='Palavras-chave pesquisadas no ticket (separador de vírgulas sem espaços)';
$_LANG['pages']['adminGroups']['ADD']  ='ADD';
$_LANG['pages']['adminGroups']['OR']  ='OU';
$_LANG['pages']['adminGroups']['AND']  ='E';
$_LANG['pages']['adminGroups']['messages']['addSuccess'] = "Regra para %s foi salva com sucesso";
$_LANG['pages']['adminGroups']['modal']['deleteLabel'] = "Excluir";
$_LANG['pages']['adminGroups']['modal']['deleteGroup']  ="A regra será eliminada. Tem certeza de que gostaria de continuar?";
$_LANG['pages']['adminGroups']['modal']['delete'] ="Excluir";
$_LANG['pages']['adminGroups']['messages']['deleteSuccess'] = "Servidor %s foi excluído com sucesso";
$_LANG['pages']['adminGroups']['modal']['help'] = 'Ajuda';
$_LANG['pages']['adminGroups']['modal']['helpTip'] ="Os Tickets caem nesta regra se essa relações tiver sido atendida: (Palavras-chave OU Produto) E Prioridade.\r\n
Se você deixar Prioridades de Tickets vazias, a relação mudar para: Palavras-chave OU Produto.\r\n
Existe uma relação OU entre grupos de palavras-chave.";
$_LANG['pages']['adminGroups']['actionButtons']['edit'] ="Editar";
$_LANG['pages']['adminGroups']['actionButtons']['delete'] ="Excluir";
$_LANG['pages']['adminGroups']['key1,key2'] ='key1,key2';
$_LANG['pages']['adminGroups']['errorMessages']['emptyKeywords'] = 'Um grupo de palavras-chave não foi salvo, pressione \'ADD\' para salva-lo.';

$_LANG['pages']['configurations']['header']['configurations'] ='Configuração';
$_LANG['pages']['configurations']['header']['default']  = 'Regras Padrão';
$_LANG['pages']['configurations']['header']['chipChat'] = 'Hip Chat';
$_LANG['pages']['configurations']['header']['tickets'] ='Ticket';
        
$_LANG['pages']['configurations']['button']['save'] ='Salvar Alterações';
$_LANG['pages']['configurations']['configuration']['keywords']['label'] = 'Palavas-chaves';
$_LANG['pages']['configurations']['configuration']['keywords']['description'] = 'Lista de palavras-chave';
$_LANG['pages']['configurations']['Add Keywords']= 'Add palavra-chave';
$_LANG['pages']['configurations']['Rule']  ='Regra:';
$_LANG['pages']['configurations']['keywordsAddDesc'] = 'Palavras-chave pesquisadas no ticket (separador de vírgulas sem espaços)';
$_LANG['pages']['configurations']['ADD']  ='ADD';
$_LANG['pages']['configurations']['OR']  ='OU';
$_LANG['pages']['configurations']['AND']  ='E';
$_LANG['pages']['configurations']['configuration']['products']['label'] = 'Produtos';
$_LANG['pages']['configurations']['configuration']['products']['label'] = 'Produtos';
$_LANG['pages']['configurations']['configuration']['ticketPriorities']['label'] = 'Propriedades do Ticket';
$_LANG['pages']['configurations']['configuration']['adminMaximumTickets']['label'] = 'Máximo de Tickets';
$_LANG['pages']['configurations']['configuration']['adminMaximumTickets']['description'] = 'Número máximo de ticket abertos por Admin';
$_LANG['pages']['configurations']['configuration']['ticketNoReplyNotificationEmail']['label'] = 'Notifiçaõ de e-mail sem resposta ';
$_LANG['pages']['configurations']['configuration']['ticketNoReplyNotificationEmail']['options']['on']   = 'Marque para ativar - notificação por e-mail';
$_LANG['pages']['configurations']['configuration']['ticketNoReplyNotificationHipChat']['label'] = 'Nenhuma resposta Notificação HipChat ';
$_LANG['pages']['configurations']['configuration']['ticketNoReplyNotificationHipChat']['options']['on'] = 'Marque para habilitar - notificação HipChat';
$_LANG['pages']['configurations']['configuration']['hipChatNotification']['label'] ='Notificação do HipChat';
$_LANG['pages']['configurations']['configuration']['hipChatNotification']['options']['on'] = 'Tick to enable - HipChat notification';
$_LANG['pages']['configurations']['configuration']['hipChatApiToken']['label'] ='HipChat API Token';
$_LANG['pages']['configurations']['configuration']['hipChatApiToken']['description'] ='HipChat API requires an auth_token variable';
$_LANG['pages']['configurations']['configuration']['ticketAssignmentByOnlineAdmins']['label'] = 'Online Admins';
$_LANG['pages']['configurations']['configuration']['ticketAssignmentByOnlineAdmins']['options']['on']  = 'Tick to enable - tickets will be assigned to online admins only';
$_LANG['pages']['configurations']['configuration']['ticketAssignmentByOfficeHours']['label'] = 'Office Hours';
$_LANG['pages']['configurations']['configuration']['ticketAssignmentByOfficeHours']['options']['on']  = 'Tick to enable - assignment by office hours';
$_LANG['pages']['configurations']['messages']['testSuccess'] ='Success';
$_LANG['pages']['configurations']['configuration']['testConnectionHipChat']['label'] = 'Test Connection';
$_LANG['pages']['configurations']['configuration']['ticketNotReplyTime']['label'] = 'No Reply Time';
$_LANG['pages']['configurations']['configuration']['ticketNotReplyTime']['description'] = 'Ticket will be assigned to another admin if the time has passed. (Leave blank for disabled)';
$_LANG['pages']['configurations']['configuration']['noReplyNotificationTime']['label'] = 'No Reply Notification Time';
$_LANG['pages']['configurations']['configuration']['noReplyNotificationTime']['description'] ='Notification will be sent if the time has passed.';
$_LANG['pages']['configurations']['configuration']['localApiUser']['label'] = 'Admin API Used';
$_LANG['pages']['configurations']['configuration']['localApiUser']['description'] ='Choose admin used for API connection. He will be excluded from the list of online admins.';
$_LANG['pages']['configurations']['configuration']['ticketRejection']['label'] ='Ticket Rejection';
$_LANG['pages']['configurations']['configuration']['ticketRejection']['options']['on'] ='Tick to enable - ticket rejection ';
$_LANG['pages']['configurations']['configuration']['departmentNoreplyTime']['label'] = 'Department No Reply Time';
$_LANG['pages']['configurations']['configuration']['departmentNoreplyTime']['description'] = 'Ticket will be assigned to another department if the time has passed. (Leave blank for disabled)';
$_LANG['pages']['configurations']['configuration']['useDepartmentReserve']['label'] ='Reserve Departments';
$_LANG['pages']['configurations']['configuration']['useDepartmentReserve']['options']['on'] = 'Tick to enable - reserve departments';
$_LANG['pages']['configurations']['configuration']['departmentReserve']['label'] = 'Default Reserve Department';
$_LANG['pages']['configurations']['configuration']['departmentReserve']['description'] ='';
$_LANG['pages']['configurations']['header']['cron'] = "Cron Job";
$_LANG['pages']['configurations']['cronURL']='Cron job  configuration  (each 5 minutes suggested): ';
$_LANG['pages']['configurations']['header']['settings']  ='Settings';
$_LANG['pages']['configurations']['header']['notifications'] ='Notifications';
$_LANG['pages']['configurations']['configuration']['noReplyNotificationTime']['info'] ="[min]";
$_LANG['pages']['configurations']['configuration']['ticketNotReplyTime']['info']="[min]";
$_LANG['pages']['configurations']['configuration']['hipChatNotify']['label'] ='HipChat Notify';
$_LANG['pages']['configurations']['configuration']['hipChatNotify']['options']['on']  = 'Tick to enable - HipChat Notify';
$_LANG['pages']['configurations']['configuration']['hipChatMessageColor']['label'] ='HipChat Message Color';
$_LANG['pages']['configurations']['configuration']['hipChatMessageColor']['description']='Background color for message.';
$_LANG['pages']['configurations']['configuration']['hipChatMessageColor']['options']['yellow']="Yellow";
$_LANG['pages']['configurations']['configuration']['hipChatMessageColor']['options']['red']  ="Red";
$_LANG['pages']['configurations']['configuration']['hipChatMessageColor']['options']['green'] ='Green';
$_LANG['pages']['configurations']['configuration']['hipChatMessageColor']['options']['purple'] ='Purple';
$_LANG['pages']['configurations']['configuration']['hipChatMessageColor']['options']['gray'] ='Green';
$_LANG['pages']['configurations']['configuration']['hipChatMessageColor']['options']['random'] ='Random';
$_LANG['pages']['configurations']['configuration']['hipChatMessageColor']['options'][''] ='';
$_LANG['pages']['configurations']['configuration']['hipChatNewTicketNotification']['label'] = 'New Ticket HipChat Notification';
$_LANG['pages']['configurations']['configuration']['hipChatNewTicketNotification']['options']['on'] = 'Tick to enable -  new ticket HipChat notification';
$_LANG['pages']['configurations']['configuration']['hipChatNewTicketPrivateNotification']['label'] = 'New Ticket HipChat Private Notification';
$_LANG['pages']['configurations']['configuration']['hipChatNewTicketPrivateNotification']['options']['on'] = 'Tick to enable -  notify administrator by private message';
$_LANG['pages']['configurations']['configuration']['hipChatNoReplyPrivateNotification']['label'] = 'No Reply HipChat Private Notification';
$_LANG['pages']['configurations']['configuration']['hipChatNoReplyPrivateNotification']['options']['on'] = 'Tick to enable -  notify administrator by private message';
$_LANG['pages']['configurations']['messages']['changedSaved'] = 'Changes Saved';

 $_LANG['pages']['cron']['This is a notice that ticket'] ='This is a notice that a ticket';
 $_LANG['pages']['cron']['is waiting for answer.']='is waiting for an answer.';
 $_LANG['pages']['cron']['Client:']='Cliente:';
 $_LANG['pages']['cron']['Department:'] ='Departmento:';
 $_LANG['pages']['cron']['Subject:']='Assunto:';
$_LANG['pages']['cron']['Priority:'] ='Prioridade:';

$_LANG['pages']['dashboard']['header']['summary'] ="Sumario";
$_LANG['pages']['dashboard']['openTickets'] = 'Open Tickets: ';
$_LANG['pages']['dashboard']['assignedTickets']  = 'Assigned Tickets: ';
$_LANG['pages']['dashboard']['ticketWaitingAssign']  = 'Ticket Waiting For Assignment: ';
$_LANG['pages']['dashboard']['header']['staff'] ='Funcionário Online';
$_LANG['pages']['dashboard']['table']['username'] ='Nome do Usuário';
$_LANG['pages']['dashboard']['table']['assignedTickets2'] ='Assigned Tickets';
$_LANG['pages']['dashboard']['table']['groupFree'] ="Group / Free Tickets"; 
$_LANG['pages']['dashboard']['header']['cron'] ="Automation";
$_LANG['pages']['dashboard']['automationInfo'] = 'Last Cron Run Info';
$_LANG['pages']['dashboard']['cronLastRun'] ='Cron Last Run: ';
$_LANG['pages']['dashboard']['cron']['minutes']  ='minutos';
$_LANG['pages']['dashboard']['cron']['hours']  ='houras';
$_LANG['pages']['dashboard']['cron']['seconds']  ='segundos';
$_LANG['pages']['dashboard']['ago']  ='ago';
$_LANG['pages']['dashboard']['checkTimePeriod'] = 'Check Time Period:';
$_LANG['pages']['dashboard']['every'] ='every';
$_LANG['pages']['dashboard']['unassignedTickets'] ='Unassigned Tickets:';
$_LANG['pages']['dashboard']['assignmentEfficiency'] ='Assignment Efficiency';
$_LANG['pages']['dashboard']['unassignedTicketsTip'] ='Tickets which slip through defined rules of assignment.';
$_LANG['pages']['dashboard']['serverTime'] ='Hora do Servidor: ';

$_LANG['hook']['rejectTicket']='Reject Ticket';
$_LANG['hook']['ticketrejected'] = 'Ticket Rejected: ';
$_LANG['hook']['cancel'] = 'Cancelar';

$_LANG['pagesTitles']['logs'] ="Logs";
$_LANG['pages']['logs']['header']  ="Logs";
$_LANG['pages']['logs']['Delete All Entries'] ="Delete All Entries";
$_LANG['pages']['logs']['modal']['deleteLabel'] ="Delete All Entries";
$_LANG['pages']['logs']['modal']['close'] ="Close";
$_LANG['pages']['logs']['modal']['delete'] ="Delete";
$_LANG['pages']['logs']['modal']['deleteLogs'] ="Are you sure you want to delete all entries from logs list?";
$_LANG['pages']['logs']['search'] ="Search";
$_LANG['pages']['logs']['table']['ticketID'] ="Ticket ID";
$_LANG['pages']['logs']['table']['message'] ="Message";
$_LANG['pages']['logs']['table']['date'] ="Date";
$_LANG['pages']['logs']['table']['Action']="Action";
$_LANG['pages']['logs']['noAvailable'] ='No Logs Available';
$_LANG['pages']['logs']['noAvailableInfo'] ='';
$_LANG['pages']['logs']['previous']="Previous";
$_LANG['pages']['logs']['next']="Next";
$_LANG['pages']['logs']['actionButtons']['delete'] ='Delete';

$_LANG['pages']['logs']['modal']['deleteLabel2'] ="Delete Log";
$_LANG['pages']['logs']['modal']['deleteOne'] ="Are you sure you want to delete this entry from logs list?";
$_LANG['pages']['logs']['messages']['deleteSuccess'] = "Log %s has been deleted successfully";
$_LANG['pages']['logs']['messages']['deleteAllSuccess']= "Logs have been deleted successfully";


