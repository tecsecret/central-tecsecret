<?php return array (
  'root' => 
  array (
    'pretty_version' => '2.0.0',
    'version' => '2.0.0.0',
    'aliases' => 
    array (
    ),
    'reference' => NULL,
    'name' => 'modulesgarden/paymentgatewaycharges',
  ),
  'versions' => 
  array (
    'adbario/php-dot-notation' => 
    array (
      'pretty_version' => '2.1.0',
      'version' => '2.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '895fe4bb153ac875c61a6fba658ded45405e73a4',
    ),
    'modulesgarden/paymentgatewaycharges' => 
    array (
      'pretty_version' => '2.0.0',
      'version' => '2.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'piwik/ini' => 
    array (
      'pretty_version' => '1.0.8',
      'version' => '1.0.8.0',
      'aliases' => 
      array (
      ),
      'reference' => '663d4fdedd8f344407fd1599e623f41912fe2ebd',
    ),
    'rappasoft/laravel-helpers' => 
    array (
      'pretty_version' => '1.0.2',
      'version' => '1.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c8dfa1e979437528262725ebe99c2e6383b24c16',
    ),
  ),
);
