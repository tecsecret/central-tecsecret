<div class="mg-wrapper body" data-target=".body" data-spy="scroll" data-twttr-rendered="true">


    <div class="page-content" id="MGPage{$currentPageName}">
        <div class="container-fluid">

            {$content}
        </div>
    </div>
    <div id="MGLoader" style="display:none;" >
        <div>
            <img src="{$assetsURL}/img/ajax-loader.gif" alt="Loading ..." />
        </div>
    </div>
</div>
<script type="text/javascript" src="{$assetsURL}/js/vue.min.js"></script>
{*<script type="text/javascript" src="https://unpkg.com/vue"></script>*}
<script type="text/javascript" src="{$assetsURL}/js/mgComponentsByHooks.js"></script>
<script type="text/javascript" src="{$assetsURL}/js/jscolor.min.js"></script>            
<script type="text/javascript" src="{$assetsURL}/js/layers-ui.js"></script>
<script type="text/javascript" src="{$assetsURL}/js/layers-ui-table.js"></script>