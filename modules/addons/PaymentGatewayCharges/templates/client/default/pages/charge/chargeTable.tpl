<div id="mg-charge-table">
    <div class="sub-heading">
        <span class="primary-bg-color">{$MGLANG->T('paymentGatewayCharges')}</span>
    </div>
    <table class="table table-striped">
        <thead>
            <tr>
                <th>{$MGLANG->T('name')}</th>
                <th>{$MGLANG->T('provision')}</th>
                <th>{$MGLANG->T('calculated')}</th>
            </tr>
        </thead>
        <tbody>
            {foreach from=$detail key=keyValue item=value}
                <tr class="text-left">
                    <td>{$value.name}</td>
                    <td>{$value.percentage}</td>
                    <td>{$value.fixed_amount}</td>
                </tr>
            {/foreach}
        </tbody>
    </table>
</div>