{literal}
    <script type="text/javascript">
        var applycredit = document.getElementsByName('applycredit')[0];
        if (applycredit)
        {
            var form        = applycredit.parentElement;
            form.addEventListener("submit", function (event) {
                var startValue = parseFloat(event.target.elements['creditamount'].value);
                if (startValue > {/literal}{$creditamount}{literal}) {
                    if (startValue <= ({/literal}{$creditamount}{literal} + {/literal}{$charge}{literal})) {
                        event.target.elements['creditamount'].value = "" + ((Math.round((startValue - {/literal}{$charge}{literal})*100))/100).toFixed(2);
                    }
                }
            }); 
        }
    </script>
{/literal}