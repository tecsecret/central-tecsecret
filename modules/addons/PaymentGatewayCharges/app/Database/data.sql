INSERT INTO `#prefix#ModuleSettings` (`setting`, `value`) SELECT "isChargesTable", "on" FROM DUAL
WHERE NOT EXISTS (SELECT * FROM `#prefix#ModuleSettings` WHERE `#prefix#ModuleSettings`.`setting` LIKE 'isChargesTable')
LIMIT 1 ;

INSERT INTO `#prefix#ModuleSettings` (`setting`, `value`) SELECT "formatChargeText", ":mainText: (:gatewayName: :price::discount:)"  FROM DUAL
WHERE NOT EXISTS (SELECT * FROM `#prefix#ModuleSettings` WHERE `#prefix#ModuleSettings`.`setting` LIKE 'formatChargeText')
LIMIT 1 ;

INSERT INTO `#prefix#ModuleSettings` (`setting`, `value`) SELECT "chargeTableId", "div#creditCardInputFields"  FROM DUAL
WHERE NOT EXISTS (SELECT * FROM `#prefix#ModuleSettings` WHERE `#prefix#ModuleSettings`.`setting` LIKE 'chargeTableId')
LIMIT 1 ;

INSERT INTO `#prefix#ModuleSettings` (`setting`, `value`) SELECT "chargeTableIdSelect", "none"  FROM DUAL
WHERE NOT EXISTS (SELECT * FROM `#prefix#ModuleSettings` WHERE `#prefix#ModuleSettings`.`setting` LIKE 'chargeTableIdSelect')
LIMIT 1 ;

INSERT INTO `#prefix#ModuleSettings` (`setting`, `value`) SELECT "isShowCharge", "on"  FROM DUAL
WHERE NOT EXISTS (SELECT * FROM `#prefix#ModuleSettings` WHERE `#prefix#ModuleSettings`.`setting` LIKE 'isShowCharge')
LIMIT 1 ;

INSERT INTO `#prefix#other` (`options`) SELECT "funds" FROM DUAL
WHERE NOT EXISTS (SELECT * FROM `#prefix#other` WHERE `#prefix#other`.`options` LIKE 'funds')
LIMIT 1;

INSERT INTO `#prefix#other` (`options`) SELECT "customInvoice" FROM DUAL
WHERE NOT EXISTS (SELECT * FROM `#prefix#other` WHERE `#prefix#other`.`options` LIKE 'customInvoice')
LIMIT 1;

INSERT INTO `#prefix#other` (`options`) SELECT "lateFee" FROM DUAL
WHERE NOT EXISTS (SELECT * FROM `#prefix#other` WHERE `#prefix#other`.`options` LIKE 'lateFee')
LIMIT 1;