--
-- `#prefix#clients`
--
CREATE TABLE IF NOT EXISTS `#prefix#clients` (
    `id`            int(10) unsigned NOT NULL AUTO_INCREMENT,
    `client_id`     int(10) NOT NULL,
    `enabled`       CHAR(4) NOT NULL DEFAULT 'on',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=#charset# DEFAULT COLLATE #collation#;

--
-- `#prefix#steps`
--
CREATE TABLE IF NOT EXISTS `#prefix#steps` (
    `id`                 int(10) unsigned NOT NULL AUTO_INCREMENT,
    `rule_id`            int(10) NOT NULL,
    `min_invoice_amount` VARCHAR(150) NOT NULL,
    `percentage`         float        NOT NULL DEFAULT '0',
    `fixed_amount`       float        NOT NULL DEFAULT '0',
    `enabled`            CHAR(4)      NOT NULL DEFAULT 'on',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=#charset# DEFAULT COLLATE #collation#;

--
-- `#prefix#conditions`
--
CREATE TABLE IF NOT EXISTS `#prefix#conditions` (
    `id`               int(10) unsigned NOT NULL AUTO_INCREMENT,
    `type`             enum('user', 'group', 'other', 'all'),
    `rule_id`          int(10) NOT NULL,
    `client_id`        int(10),
    `group_id`         int(10),
    `currency_id`      int(10),
    `country`          VARCHAR(100),
    PRIMARY KEY (`id`),
    UNIQUE KEY `rule_id` (`currency_id`,`country`,`group_id`,`client_id`,`rule_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=#charset# DEFAULT COLLATE #collation#;

--
-- `#prefix#gateways`
--
CREATE TABLE IF NOT EXISTS `#prefix#gateways` (
    `id`               int(10) unsigned NOT NULL AUTO_INCREMENT,
    `gateway`          TEXT NOT NULL,
    `enabled`          CHAR(4) NOT NULL DEFAULT 'on',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=#charset# DEFAULT COLLATE #collation#;

--
-- `#prefix#gateway_settings`
--
CREATE TABLE IF NOT EXISTS `#prefix#gateway_settings` (
    `id`               int(10) unsigned NOT NULL AUTO_INCREMENT,
    `gateway_id`       int(10)          NOT NULL,
    `setting`          VARCHAR(255)     NOT NULL,
    `value`            VARCHAR(255)     NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=#charset# DEFAULT COLLATE #collation#;

--
-- `#prefix#items`
--
CREATE TABLE IF NOT EXISTS `#prefix#items` (
    `id`               int(10)      unsigned NOT NULL AUTO_INCREMENT,
    `rel_id`           int(10)               NOT NULL,
    `type`             VARCHAR(255)          NOT NULL,
    `enabled`          CHAR(4)               NOT NULL DEFAULT 'on',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=#charset# DEFAULT COLLATE #collation#;

--
-- `#prefix#rules`
--
CREATE TABLE IF NOT EXISTS `#prefix#rules` (
    `id`               int(10)      unsigned NOT NULL AUTO_INCREMENT,
    `gateway_id`       int(10)               NOT NULL,
    `name`             VARCHAR(255)          NOT NULL,
    `percentage`       float                 NOT NULL DEFAULT '0',
    `fixed_amount`     float                 NOT NULL DEFAULT '0',
    `enabled`          CHAR(4)               NOT NULL DEFAULT 'on',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=#charset# DEFAULT COLLATE #collation#;


--
-- `#prefix#DebugLogs
--
CREATE TABLE IF NOT EXISTS `#prefix#DebugLogs` (
    `id`            int(10) unsigned NOT NULL AUTO_INCREMENT,
    `clientID`      int(10) NOT NULL,
    `invoiceID`     int(10) NOT NULL,
    `hook`          VARCHAR(150),
    `action`        VARCHAR(150),
    `invoiceBefore` TEXT NOT NULL,
    `invoiceAfter`  TEXT NOT NULL,
    `settings`  TEXT NOT NULL,
    `conditions`    TEXT NOT NULL,
    `updated_at`    DATETIME DEFAULT null,
    `created_at`    DATETIME DEFAULT null,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=#charset# DEFAULT COLLATE #collation#;


--
-- `#prefix#other`
--
CREATE TABLE IF NOT EXISTS `#prefix#other` (
    `id`            int(10) unsigned NOT NULL AUTO_INCREMENT,
    `options`       VARCHAR(255),
    PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=#charset# DEFAULT COLLATE #collation#;
