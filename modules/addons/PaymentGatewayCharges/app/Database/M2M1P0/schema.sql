--
-- `#prefix#DebugLogs
--
CREATE TABLE IF NOT EXISTS `#prefix#DebugLogs` (
    `id`            int(10) unsigned NOT NULL AUTO_INCREMENT,
    `clientID`      int(10) NOT NULL,
    `invoiceID`     int(10) NOT NULL,
    `hook`          VARCHAR(150),
    `action`        VARCHAR(150),
    `invoiceBefore` TEXT NOT NULL,
    `invoiceAfter`  TEXT NOT NULL,
    `settings`      TEXT NOT NULL,
    `conditions`    TEXT NOT NULL,
    `updated_at`    DATETIME DEFAULT null,
    `created_at`    DATETIME DEFAULT null,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=#charset# DEFAULT COLLATE #collation#;



--
-- `#prefix#other`
--
CREATE TABLE IF NOT EXISTS `#prefix#other` (
    `id`            int(10) unsigned NOT NULL AUTO_INCREMENT,
    `options`       VARCHAR(255),
    PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=#charset# DEFAULT COLLATE #collation#;

ALTER TABLE `#prefix#rules` MODIFY COLUMN `percentage` float;
ALTER TABLE `#prefix#rules` MODIFY COLUMN `fixed_amount` float;

ALTER TABLE `#prefix#steps` MODIFY COLUMN `percentage` float;
ALTER TABLE `#prefix#steps` MODIFY COLUMN `fixed_amount` float;
ALTER TABLE `#prefix#steps` MODIFY COLUMN `min_invoice_amount` float;