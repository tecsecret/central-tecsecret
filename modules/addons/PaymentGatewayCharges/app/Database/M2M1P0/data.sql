INSERT INTO `#prefix#other` (`options`) SELECT "funds" FROM DUAL
WHERE NOT EXISTS (SELECT * FROM `#prefix#other` WHERE `#prefix#other`.`options` LIKE 'funds')
LIMIT 1;

INSERT INTO `#prefix#other` (`options`) SELECT "customInvoice" FROM DUAL
WHERE NOT EXISTS (SELECT * FROM `#prefix#other` WHERE `#prefix#other`.`options` LIKE 'customInvoice')
LIMIT 1;

INSERT INTO `#prefix#other` (`options`) SELECT "lateFee" FROM DUAL
WHERE NOT EXISTS (SELECT * FROM `#prefix#other` WHERE `#prefix#other`.`options` LIKE 'lateFee')
LIMIT 1;
