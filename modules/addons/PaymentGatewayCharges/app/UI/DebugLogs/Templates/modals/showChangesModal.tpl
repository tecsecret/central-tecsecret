{**********************************************************************
* DomainOrdersExtended product developed. (2017-11-16)
* *
*
*  CREATED BY MODULESGARDEN       ->       http://modulesgarden.com
*  CONTACT                        ->       contact@modulesgarden.com
*
*
* This software is furnished under a license and may be used and copied
* only  in  accordance  with  the  terms  of such  license and with the
* inclusion of the above copyright notice.  This software  or any other
* copies thereof may not be provided or otherwise made available to any
* other person.  No title to and  ownership of the  software is  hereby
* transferred.
*
*
**********************************************************************}

{**
* @author Sławomir Miśkowicz <slawomir@modulesgarden.com>
*}

<div class="modal show modal--xlg" id="confirmationModal">
    <div class="modal__dialog">
        <div class="modal__content" id="mgModalContainer">
            <div class="modal__top top">
                <div class="top__title type-6">
                    <span class="text-faded font-weight-normal">
                        {if $rawObject->isRawTitle()}{$rawObject->getRawTitle()}{elseif $rawObject->getTitle()}{$MGLANG->T('modal', $rawObject->getTitle())}{/if}
                    </span>
                </div>
                <div class="top__toolbar">
                    <button class="btn btn--xs btn--danger btn--icon btn--link btn--plain closeModal" data-dismiss="lu-modal" aria-label="Close" @click="closeModal($event)">
                        <i class="btn__icon zmdi zmdi-close"></i>
                    </button>
                </div>
            </div>
            {assign var="sections" value=$rawObject->getForm('changesForm')}

            <div class="modal__nav">
                <ul class="nav nav--md nav--h nav--tabs">
                    <ul class="nav nav--d nav--h nav--tabs">
                        {assign var="fields" value=$sections->getTabData()|array_keys}

                        {foreach from=$fields key=sectionID  item=section }
                            <li class="nav__item {if $fields[0] == $section}is-active{/if}">
                                <a class="nav__link" data-toggle="lu-tab" href="#modalTab{$section}">
                                    <span class="nav__link-text">{$MGLANG->T('modal', $section)}</span>
                                </a>
                            </li>
                        {/foreach}
                    </ul>
                </ul>
            </div>
            <div class="modal__body">
                <div class="tab-content">
                    {assign var="fieldsContent" value=$sections->getTabData()}
                    {foreach from=$fieldsContent key=sectionID  item=section }

                        <div class="tab-pane {if $fields[0] == $sectionID}is-active{/if}" id="modalTab{$sectionID}">
                                {if (is_array($section))}
                                    <table border="0">
                                        <tr>
                                            {foreach from=$section item=row}
                                                <td valign="top"><pre><code>{print_r(json_decode($row), TRUE)}</code></pre></td>
                                            {/foreach}
                                        </tr>
                                    </table>
                                {else}
                                <pre><code>{print_r(json_decode($section), TRUE)}</code></pre>
                                {/if}
                        </div>
                    {/foreach}
                </div>

            </div>
        </div> 
    </div>
</div>            