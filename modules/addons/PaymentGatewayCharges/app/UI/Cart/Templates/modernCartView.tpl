{**********************************************************************
* ModuleFramework product developed. (2017-09-08)
* *
*
*  CREATED BY MODULESGARDEN       ->       http://modulesgarden.com
*  CONTACT                        ->       contact@modulesgarden.com
*
*
* This software is furnished under a license and may be used and copied
* only  in  accordance  with  the  terms  of such  license and with the
* inclusion of the above copyright notice.  This software  or any other
* copies thereof may not be provided or otherwise made available to any
* other person.  No title to and  ownership of the  software is  hereby
* transferred.
*
*
**********************************************************************}

{**
* @author Mateusz Pawłowski <mateusz.pa@modulesgarden.com>
*}

<tr class='subtotal removedTotal'>
    <td class='text-right '>
        {$MGLANG->absoluteT('addonCA','charge','Total')}: &nbsp;
    </td>
    <td class='text-center'>
        {$rawObject->getTotal()}
    </td>
</tr>
{if $rawObject->getGatewayCharge()}
    <tr class='subtotal removedTotal'>
        <td class='text-right'>
            {if $rawObject->isDiscount()}
                {$MGLANG->absoluteT('addonCA','charge','discountCheckout')}: &nbsp;
            {else}
                {$MGLANG->absoluteT('addonCA','charge','chargeCheckout')}: &nbsp;
            {/if}
        </td>
        <td class='text-center'>
            {$rawObject->getGatewayCharge()}
        </td>
    </tr>
{/if}
<tr class='total'>
    <td class='text-right'>
        {$rawObject->getDueDateLang()}: &nbsp;
    </td>
    <td class='text-center'>
        {$rawObject->getTotalDueDate()}
    </td>
</tr>


