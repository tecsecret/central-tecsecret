var initConditionTypeRemember = null;

function initConditionTypeModal(data, targetId, event) {
    if (targetId) {
        initConditionTypeRemember = targetId;
    }
    mgPageControler.vueLoader.makeCustomActiom(updateConditionFields, {targetId: initConditionTypeRemember});
}

function viewConditionFields($type, elementName) {
    if ($type == "user") {
        if (elementName == "client_id") {
            return true;
        } else {
            return false;
        }
    } else if ($type == "group") {
        if (elementName == "group_id" || elementName == "currency_id" || elementName == "country") {
            return true;
        } else {
            return false;
        }
    } else if ($type == "other") {
        if (elementName == "currency_id" || elementName == "country") {
            return true;
        } else {
            return false;
        }
    }
    return false;
}

function updateConditionFields(vieObject, params, event) {
    var typeChose = '';
    var element = null;
    if (params.targetId === 'addConditionButton') {
        element = $("#addConditionForm");
        typeChose = element.find("select[name=type]").find(":selected").val();
    } else if (params.targetId === 'editConditionButton') {
        element = $("#editConditionForm");
        typeChose = element.find("select[name=type]").find(":selected").val();
    }

    element.children(".form-group").each(function () {
        if ($(this).find("select").attr("name") !== "type") {
            $(this).css("display", "none");
            if (viewConditionFields(typeChose, $(this).find("select").attr("name"))) {
                $(this).css("display", "");
            }
        }
    });
}