<?php

$_LANG['token']                  = ', Error Token:';
$_LANG['generalError']           = 'Something has gone wrong. Check the logs and contact the administrator.';
$_LANG['generalErrorClientArea'] = 'Something has gone wrong. Contact the administrator.';
$_LANG['permissionsStorage']     = ':storage_path: settings are not sufficient. Please change permissions to value 777.';
$_LANG['undefinedAction']        = 'Undefined Action';
$_LANG['changesHasBeenSaved']    = 'Changes have been saved successfully';
$_LANG['Monthly']                = 'Monthly';
$_LANG['Free Account']           = 'Free Account';
$_LANG['Passwords']              = 'Passwords';
$_LANG['labelAddedSuccesfully']  = 'The label has been added successfully';
$_LANG['Nothing to display']     = 'Nothing to display';
$_LANG['Search']                 = 'Search';
$_LANG['Previous']               = 'Previous';
$_LANG['Next']                   = 'Next';
$_LANG['searchPlacecholder']     = 'Search...';

$_LANG['noDataAvalible']                 = 'No Data Available';
$_LANG['validationErrors']['emptyField'] = 'Field cannot be empty';
$_LANG['bootstrapswitch']['disabled']    = 'Disabled';
$_LANG['bootstrapswitch']['enabled']     = 'Enabled';

/* * ********************************************************************************************************************
 *                                                   ADMIN AREA                                                        *
 * ******************************************************************************************************************** */

$_LANG['addonAA']['datatables']['next']        = 'Next';
$_LANG['addonAA']['datatables']['previous']    = 'Previous';
$_LANG['addonAA']['datatables']['zeroRecords'] = 'Nothing to display';

// -------------------------------------------------> MENU <--------------------------------------------------------- //
$_LANG['addonAA']['pagesLabels']['label']['home']                             = 'Home';
$_LANG['addonAA']['pagesLabels']['label']['Home']                             = 'Home';
$_LANG['addonAA']['pagesLabels']['gateways']['index'] = 'List';

$_LANG['addonAA']['pagesLabels']['label']['gateways'] = 'Gateways';
$_LANG['addonAA']['gateways']['mainContainer']['gateways']['table']['value'] = 'Title';
$_LANG['addonAA']['gateways']['mainContainer']['gateways']['table']['enabled'] = 'Status';
$_LANG['addonAA']['gateways']['mainContainer']['gateways']['showRules']['button']['redirectButton'] = 'Show Rules';
$_LANG['addonAA']['gateways']['mainContainer']['gateways']['showSettings']['button']['redirectButton'] = 'Settings';

$_LANG['addonAA']['gateways']['mainContainer']['gateways']['editGatewaySettings']['button']['editGatewaySettings'] = 'Settings';
$_LANG['addonAA']['gateways']['editGatewaySettingsModal']['modal']['editGatewaySettingsModal'] = 'Settings';
$_LANG['addonAA']['gateways']['editGatewaySettingsModal']['baseForm']['enabled']['enabled'] = 'Status';
$_LANG['addonAA']['gateways']['editGatewaySettingsModal']['baseForm']['isTax']['isTax']  = 'Apply Tax To Charge';
$_LANG['addonAA']['gateways']['editGatewaySettingsModal']['baseForm']['showInTable']['showInTable']  = 'Show In Charges Table';
$_LANG['addonAA']['gateways']['editGatewaySettingsModal']['baseForm']['showInTable']['description']  = 'If enabled, the charges and discounts configured in the rules of this gateway will be displayed in the table at the checkout.';
$_LANG['addonAA']['gateways']['editGatewaySettingsModal']['baseForm']['paymentType']['paymentType'] = 'Billing Type';
$_LANG['addonAA']['gateways']['editGatewaySettingsModal']['baseForm']['paymentType']['description'] = 'Select the calculation method of charges and discounts specified for this gateway. View the documentation to learn the difference between the Standard and Alternative billing type.';
$_LANG['addonAA']['gateways']['editGatewaySettingsModal']['baseForm']['isTax']['description'] = 'If enabled, the tax will be applied to the charge.';
$_LANG['addonAA']['gateways']['editGatewaySettingsModal']['baseForm']['afterTaxing']['afterTaxing'] = 'Charge After Taxing';
$_LANG['addonAA']['gateways']['editGatewaySettingsModal']['baseForm']['afterTaxing']['description'] = 'If enabled, the charge will be applied to an item after it is taxed.';
$_LANG['addonAA']['gateways']['editGatewaySettingsModal']['baseAcceptButton']['title'] = 'Confirm';
$_LANG['addonAA']['gateways']['editGatewaySettingsModal']['baseCancelButton']['title'] = 'Cancel';

$_LANG['addonAA']['gateways']['standard'] = 'Standard';
$_LANG['addonAA']['gateways']['paypal']   = 'Alternative (e.g. PayPal model)';
$_LANG['addonAA']['gateways']['paypalV2']   = 'PayPalV2';

$_LANG['addonAA']['gateways']['allerdySelected'] = 'The option has been already selected';
$_LANG['addonAA']['gateways']['existEmpty'] = 'There is a conflict with an existing condition. Please make sure the new condition does not overlap with others.';
$_LANG['addonAA']['gateways']['existSelected'] = 'There is a conflict with an existing condition. Please make sure the new condition does not overlap with others.';

$_LANG['addonAA']['gateways']['choseCountry']   = '-- Choose Country --';
$_LANG['addonAA']['gateways']['choseClient']    = '-- Choose Client --';
$_LANG['addonAA']['gateways']['choseGroup']     = '-- Choose Clients Group --';
$_LANG['addonAA']['gateways']['choseCurrency']  = '-- Choose Currency --';

$_LANG['addonAA']['gateways']['user']  = 'User';
$_LANG['addonAA']['gateways']['group'] = 'Group';
$_LANG['addonAA']['gateways']['other'] = 'All';
$_LANG['addonAA']['gateways']['all']   = 'All';

$_LANG['InvoiceItemTextCharge'] = 'Payment Gateway Charge';
$_LANG['InvoiceItemTextDiscount'] = 'Payment Gateway Discount';

$_LANG['addonAA']['pagesLabels']['gateways']['rules'] = ':gatewayRulesTitle: Rules';
$_LANG['addonAA']['gateways']['mainContainer']['rules']['table']['name'] = 'Name';
$_LANG['addonAA']['gateways']['mainContainer']['rules']['table']['percentage'] = 'Percentage';
$_LANG['addonAA']['gateways']['mainContainer']['rules']['table']['fixed_amount'] = 'Fixed Amount';
$_LANG['addonAA']['gateways']['mainContainer']['rules']['table']['enabled'] = 'Status';

$_LANG['addonAA']['gateways']['mainContainer']['rules']['addRuleModalButton']['button']['addRuleModalButton'] = 'Add Rule';
$_LANG['addonAA']['gateways']['addRuleModal']['modal']['addRuleModal'] = 'Add Rule';
$_LANG['addonAA']['gateways']['addRuleModal']['addRuleForm']['enabled']['enabled']  = 'Status';
$_LANG['addonAA']['gateways']['addRuleModal']['addRuleForm']['name']['name'] = 'Name';
$_LANG['addonAA']['gateways']['addRuleModal']['addRuleForm']['percentage']['percentage'] = 'Percentage';
$_LANG['addonAA']['gateways']['addRuleModal']['addRuleForm']['percentage']['description'] = '';
$_LANG['addonAA']['gateways']['addRuleModal']['addRuleForm']['fixed_amount']['fixed_amount'] = 'Fixed Amount';
$_LANG['addonAA']['gateways']['addRuleModal']['addRuleForm']['fixed_amount']['description'] = '';
$_LANG['addonAA']['gateways']['addRuleModal']['baseAcceptButton']['title'] = 'Confirm';
$_LANG['addonAA']['gateways']['addRuleModal']['baseCancelButton']['title'] = 'Cancel';

$_LANG['addonAA']['gateways']['mainContainer']['rules']['editRuleModalButton']['button']['editRuleModalButton'] = 'Edit Rule';
$_LANG['addonAA']['gateways']['editRuleModal']['modal']['editRuleModal'] = 'Edit Rule';
$_LANG['addonAA']['gateways']['editRuleModal']['editLabelForm']['enabled']['enabled'] = 'Status';
$_LANG['addonAA']['gateways']['editRuleModal']['editLabelForm']['name']['name'] = 'Name';
$_LANG['addonAA']['gateways']['editRuleModal']['editLabelForm']['percentage']['percentage'] = 'Percentage';
$_LANG['addonAA']['gateways']['editRuleModal']['editLabelForm']['percentage']['description'] = '';
$_LANG['addonAA']['gateways']['editRuleModal']['editLabelForm']['fixed_amount']['fixed_amount'] = 'Fixed Amount';
$_LANG['addonAA']['gateways']['editRuleModal']['editLabelForm']['fixed_amount']['description'] = '';
$_LANG['addonAA']['gateways']['editRuleModal']['baseAcceptButton']['title'] = 'Confirm';
$_LANG['addonAA']['gateways']['editRuleModal']['baseCancelButton']['title'] = 'Cancel';

$_LANG['addonAA']['gateways']['mainContainer']['rules']['showSteps']['button']['showSteps'] = 'Show Steps';
$_LANG['addonAA']['gateways']['mainContainer']['rules']['showConditions']['button']['showConditions'] = 'Show Conditions';

$_LANG['addonAA']['gateways']['alreadyExist'] = 'The value already exists.';

$_LANG['addonAA']['gateways']['mainContainer']['rules']['deleteRuleButton']['button']['deleteRuleButton'] = 'Remove Rule';
$_LANG['addonAA']['gateways']['deleteRuleModal']['modal']['deleteRuleModal'] = 'Remove Rule';
$_LANG['addonAA']['gateways']['deleteRuleModal']['deleteRuleForm']['confirmRuleRemove'] = "Are you sure that you want to remove the ':name:' rule?";
$_LANG['addonAA']['gateways']['deleteRuleModal']['baseAcceptButton']['title'] = 'Confirm';
$_LANG['addonAA']['gateways']['deleteRuleModal']['baseCancelButton']['title'] = 'Cancel';
$_LANG['ruleDeletedSuccesfully'] = 'The rule has been deleted successfully';


$_LANG['addonAA']['pagesLabels']['rules']['conditions']  = ':rulesTitle: Conditions';

$_LANG['addonAA']['gateways']['mainContainer']['conditionsPage']['table']['type']     = 'Condition Type';
$_LANG['addonAA']['gateways']['mainContainer']['conditionsPage']['table']['fullName']     = 'Client';
$_LANG['addonAA']['gateways']['mainContainer']['conditionsPage']['table']['groupname'] = 'Client Group Name';
$_LANG['addonAA']['gateways']['mainContainer']['conditionsPage']['table']['code']      = 'Currency Code';
$_LANG['addonAA']['gateways']['mainContainer']['conditionsPage']['table']['country']   = 'Country';

$_LANG['addonAA']['gateways']['mainContainer']['conditionsPage']['backToRules']['button']['backToRules'] = 'Back To Rules';
$_LANG['addonAA']['gateways']['mainContainer']['conditionsPage']['addConditionButton']['button']['addConditionButton'] = 'Add Condition';
$_LANG['addonAA']['gateways']['addConditionModal']['modal']['addConditionModal'] = 'Add Condition';
$_LANG['addonAA']['gateways']['addConditionModal']['addConditionForm']['type']['type'] = 'Condition Type';
$_LANG['addonAA']['gateways']['addConditionModal']['addConditionForm']['client_id']['client_id'] = 'Client';
$_LANG['addonAA']['gateways']['addConditionModal']['addConditionForm']['group_id']['group_id'] = 'Clients Group';
$_LANG['addonAA']['gateways']['addConditionModal']['addConditionForm']['currency_id']['currency_id'] = 'Currency';
$_LANG['addonAA']['gateways']['addConditionModal']['addConditionForm']['country']['country'] = 'Country';
$_LANG['addonAA']['gateways']['addConditionModal']['baseAcceptButton']['title'] = 'Confirm';
$_LANG['addonAA']['gateways']['addConditionModal']['baseCancelButton']['title'] = 'Cancel';

$_LANG['addonAA']['gateways']['mainContainer']['conditionsPage']['editConditionButton']['button']['editConditionButton'] = 'Edit Condition';
$_LANG['addonAA']['gateways']['editConditionModal']['modal']['editConditionModal'] = 'Edit Condition';
$_LANG['addonAA']['gateways']['editConditionModal']['editConditionForm']['type']['type'] = 'Condition Type';
$_LANG['addonAA']['gateways']['editConditionModal']['editConditionForm']['client_id']['client_id'] = 'Client';
$_LANG['addonAA']['gateways']['editConditionModal']['editConditionForm']['group_id']['group_id'] = 'Clients Group';
$_LANG['addonAA']['gateways']['editConditionModal']['editConditionForm']['currency_id']['currency_id'] = 'Currency';
$_LANG['addonAA']['gateways']['editConditionModal']['editConditionForm']['country']['country'] = 'Country';
$_LANG['addonAA']['gateways']['editConditionModal']['baseAcceptButton']['title'] = 'Confirm';
$_LANG['addonAA']['gateways']['editConditionModal']['baseCancelButton']['title'] = 'Cancel';


$_LANG['addonAA']['gateways']['mainContainer']['conditionsPage']['deleteConditionButton']['button']['deleteConditionButton'] = 'Remove Condition';
$_LANG['addonAA']['gateways']['deleteConditionModal']['modal']['deleteConditionModal']  = 'Remove Condition';
$_LANG['addonAA']['gateways']['deleteConditionModal']['deleteConditionForm']['confirmStepRemove'] = 'Are you sure that you want to remove this condition?';
$_LANG['addonAA']['gateways']['deleteConditionModal']['baseAcceptButton']['title'] = 'Confirm';
$_LANG['addonAA']['gateways']['deleteConditionModal']['baseCancelButton']['title'] = 'Cancel';
$_LANG['conditionDeletedSuccesfully'] = 'The condition has been deleted successfully';

$_LANG['addonAA']['pagesLabels']['rules']['steps']   = ':rulesTitle: Steps';


$_LANG['addonAA']['gateways']['mainContainer']['steps']['table']['id'] = 'Id';
$_LANG['addonAA']['gateways']['mainContainer']['steps']['table']['min_invoice_amount'] = 'Minimum Invoice Amount';
$_LANG['addonAA']['gateways']['mainContainer']['steps']['table']['percentage'] = 'Percentage';
$_LANG['addonAA']['gateways']['mainContainer']['steps']['table']['fixed_amount'] = 'Fixed Amount';
$_LANG['addonAA']['gateways']['mainContainer']['steps']['table']['enabled'] = 'Status';

$_LANG['addonAA']['gateways']['mainContainer']['steps']['backToRules']['button']['backToRules'] = 'Back To Rules';

$_LANG['addonAA']['gateways']['mainContainer']['steps']['addStepsButton']['button']['addStepsButton'] = 'Add Step';
$_LANG['addonAA']['gateways']['addStepsModal']['modal']['addStepsModal'] = 'Add Step';
$_LANG['addonAA']['gateways']['addStepsModal']['addStepsForm']['enabled']['enabled'] = 'Status';
$_LANG['addonAA']['gateways']['addStepsModal']['addStepsForm']['min_invoice_amount']['min_invoice_amount'] = 'Minimum Invoice Amount';
$_LANG['addonAA']['gateways']['addStepsModal']['addStepsForm']['percentage']['percentage'] = 'Percentage';
$_LANG['addonAA']['gateways']['addStepsModal']['addStepsForm']['fixed_amount']['fixed_amount'] = 'Fixed Amount';
$_LANG['addonAA']['gateways']['addStepsModal']['baseAcceptButton']['title'] = 'Confirm';
$_LANG['addonAA']['gateways']['addStepsModal']['baseCancelButton']['title'] = 'Cancel';

$_LANG['stepDeletedSuccesfully'] = 'The step has been deleted successfully';

$_LANG['addonAA']['gateways']['mainContainer']['steps']['editStepsButton']['button']['editStepsButton'] = 'Edit Step';
$_LANG['addonAA']['gateways']['editStepsModal']['modal']['editStepsModal'] = 'Edit Step';
$_LANG['addonAA']['gateways']['editStepsModal']['editStepsForm']['enabled']['enabled'] = 'Status';
$_LANG['addonAA']['gateways']['editStepsModal']['editStepsForm']['min_invoice_amount']['min_invoice_amount'] = 'Minimum Invoice Amount';
$_LANG['addonAA']['gateways']['editStepsModal']['editStepsForm']['percentage']['percentage'] = 'Percentage';
$_LANG['addonAA']['gateways']['editStepsModal']['editStepsForm']['fixed_amount']['fixed_amount'] = 'Fixed Amount';
$_LANG['addonAA']['gateways']['editStepsModal']['baseAcceptButton']['title'] = 'Confirm';
$_LANG['addonAA']['gateways']['editStepsModal']['baseCancelButton']['title'] = 'Cancel';

$_LANG['addonAA']['gateways']['mainContainer']['steps']['deleteStepButton']['button']['deleteStepButton'] = 'Remove Step';

$_LANG['addonAA']['gateways']['deleteStepModal']['modal']['deleteStepModal'] = 'Remove Step';
$_LANG['addonAA']['gateways']['deleteStepModal']['deleteStepForm']['confirmStepRemove'] = 'Are you sure that you want to remove this step?';
$_LANG['addonAA']['gateways']['deleteStepModal']['baseAcceptButton']['title'] = 'Confirm';
$_LANG['addonAA']['gateways']['deleteStepModal']['baseCancelButton']['title'] = 'Cancel';

$_LANG['addonAA']['pagesLabels']['label']['items']  = 'Items';
$_LANG['addonAA']['pagesLabels']['items']['addons'] = 'Addons';


$_LANG['addonAA']['items']['mainContainer']['itemsPage']['Tlds'] = 'TLDs';
$_LANG['addonAA']['items']['mainContainer']['itemsPage']['Addons'] = 'Addons';
$_LANG['addonAA']['items']['mainContainer']['itemsPage']['Products'] = 'Products';

$_LANG['addonAA']['items']['mainContainer']['itemsPage']['addonsPage']['table']['name'] = 'Name';
$_LANG['addonAA']['items']['mainContainer']['itemsPage']['addonsPage']['table']['description'] = 'Description';
$_LANG['addonAA']['items']['mainContainer']['itemsPage']['addonsPage']['table']['billingcycle'] = 'Pay Type';
$_LANG['addonAA']['items']['mainContainer']['itemsPage']['addonsPage']['table']['enabled'] = 'Chargeable';
$_LANG['addonAA']['items']['mainContainer']['itemsPage']['addonsPage']['addonChargeableMassAction']['button']['addonChargeableMassAction'] = 'Set Chargeable';
$_LANG['addonAA']['items']['addonModal']['modal']['addonModal']  = 'Set Chargeable';
$_LANG['addonAA']['items']['addonModal']['addonForm']['enabled']['enabled']  = 'Chargeable Status';
$_LANG['addonAA']['items']['addonModal']['baseAcceptButton']['title'] = 'Confirm';
$_LANG['addonAA']['items']['addonModal']['baseCancelButton']['title'] = 'Cancel';

$_LANG['addonChangesSaved'] = 'The addon chargeable status has been changed successfully';

$_LANG['addonAA']['pagesLabels']['items']['products'] = 'Product Items';
$_LANG['addonAA']['items']['mainContainer']['itemsPage']['productsPage']['table']['name'] = 'Name';
$_LANG['addonAA']['items']['mainContainer']['itemsPage']['productsPage']['table']['type'] = 'Type';
$_LANG['addonAA']['items']['mainContainer']['itemsPage']['productsPage']['table']['paytype'] = 'Pay Type';
$_LANG['addonAA']['items']['mainContainer']['itemsPage']['productsPage']['table']['autosetup'] = 'Auto Setup';
$_LANG['addonAA']['items']['mainContainer']['itemsPage']['productsPage']['table']['enabled'] = 'Chargeable';

$_LANG['addonAA']['items']['hostingaccount'] = 'Hosting Account';
$_LANG['addonAA']['items']['reselleraccount'] = 'Reseller Account';
$_LANG['addonAA']['items']['server'] = 'Dedicated/VPS Server';
$_LANG['addonAA']['items']['other'] = 'Other';

$_LANG['addonAA']['items']['free'] = 'Free';
$_LANG['addonAA']['items']['onetime'] = 'One Time';
$_LANG['addonAA']['items']['recurring'] = 'Recurring';

$_LANG['addonAA']['items']['order'] = 'Instantly After Order';
$_LANG['addonAA']['items']['off'] = 'Off';
$_LANG['addonAA']['items']['payment'] = 'After First Payment';
$_LANG['addonAA']['items']['on'] = 'After Accepting Pending Order';

$_LANG['addonAA']['items']['mainContainer']['itemsPage']['productsPage']['productChargeableMassAction']['button']['productChargeableMassAction'] = 'Set Chargeable';
$_LANG['addonAA']['items']['productModal']['modal']['productModal']  = 'Set Chargeable';
$_LANG['addonAA']['items']['productModal']['productForm']['enabled']['enabled']  = 'Chargeable Status';
$_LANG['addonAA']['items']['productModal']['baseAcceptButton']['title'] = 'Confirm';
$_LANG['addonAA']['items']['productModal']['baseCancelButton']['title'] = 'Cancel';

$_LANG['productChangesSaved'] = 'The product chargeable status has been changed successfully';

$_LANG['addonAA']['pagesLabels']['items']['domains'] = 'TLDs';
$_LANG['addonAA']['items']['mainContainer']['itemsPage']['tldsPage']['table']['extension'] = 'TLD';
$_LANG['addonAA']['items']['mainContainer']['itemsPage']['tldsPage']['table']['enabled'] = 'Chargeable';
$_LANG['addonAA']['items']['mainContainer']['itemsPage']['tldsPage']['table']['autoreg']   = 'Auto Registration';

$_LANG['addonAA']['items']['mainContainer']['itemsPage']['tldsPage']['domainChargeableMassAction']['button']['domainChargeableMassAction'] = 'Set Chargeable';
$_LANG['addonAA']['items']['domainModal']['modal']['domainModal']  = 'Set Chargeable';
$_LANG['addonAA']['items']['domainModal']['domainForm']['enabled']['enabled']  = 'Chargeable Status';
$_LANG['addonAA']['items']['domainModal']['baseAcceptButton']['title'] = 'Confirm';
$_LANG['addonAA']['items']['domainModal']['baseCancelButton']['title'] = 'Cancel';

$_LANG['tldChangesSaved'] = 'The TLD chargeable status has been changed successfully';

$_LANG['addonAA']['pagesLabels']['label']['clients'] = 'Clients';
$_LANG['addonAA']['clients']['mainContainer']['clients']['table']['fullname'] = 'Name';
$_LANG['addonAA']['clients']['mainContainer']['clients']['table']['lastname'] = 'Last Name';
$_LANG['addonAA']['clients']['mainContainer']['clients']['table']['email'] = 'Email';
$_LANG['addonAA']['clients']['mainContainer']['clients']['table']['companyname'] = 'Company Name';
$_LANG['addonAA']['clients']['mainContainer']['clients']['table']['enabled'] = 'Chargeable';
$_LANG['addonAA']['clients']['mainContainer']['clients']['table']['id'] = 'Id';

$_LANG['addonAA']['clients']['mainContainer']['clients']['setChargeableMassAction']['button']['setChargeableMassAction'] = 'Set Chargeable';
$_LANG['addonAA']['clients']['default']['description']  = 'Toggle the chargeable status of the selected clients. If enabled, the charges or discounts will be applied to the clients according to the configured billing rules.';
$_LANG['addonAA']['items']['default']['description']  = 'Toggle the chargeable status of the selected items. If enabled, the charges or discounts will be applied to the items according to the configured billing rules.';
$_LANG['addonAA']['clients']['setChargeableModals']['modal']['setChargeableModals'] = 'Set Chargeable';
$_LANG['addonAA']['clients']['setChargeableModals']['setChargeableForm']['enabled']['enabled'] = 'Chargeable Status';
$_LANG['addonAA']['clients']['setChargeableModals']['baseAcceptButton']['title'] = 'Confirm';
$_LANG['addonAA']['clients']['setChargeableModals']['baseCancelButton']['title'] = 'Cancel';

$_LANG['changesClientSaved'] = 'The client chargeable status has been changed successfully';

$_LANG['addonAA']['Page not found'] = 'Page not found';

$_LANG['labelDeletedSuccesfully']                  = 'The label has been deleted successfully';
$_LANG['changesSaved']                             = 'Changes have been saved successfully';
$_LANG['ItemNotFound']                             = 'Item Not Found';
$_LANG['CategoryDeletedSuccesfully']               = 'The category has been deleted successfully';
$_LANG['categroyCannotBeAssignedAsParentToItself'] = 'Category cannot be assigned as a parent to itself';

$_LANG['formValidationError']                                 = 'Form Validation Error';

$_LANG['addonAA']['pagesLabels']['label']['LoggerManager'] = 'Logs';
$_LANG['addonAA']['loggerManager']['mainContainer']['loggercont']['table']['id']          = 'ID';
$_LANG['addonAA']['loggerManager']['mainContainer']['loggercont']['table']['message']     = 'Message';
$_LANG['addonAA']['loggerManager']['mainContainer']['loggercont']['table']['ref']         = 'Reference Object';
$_LANG['addonAA']['loggerManager']['mainContainer']['loggercont']['table']['type']        = 'Type';
$_LANG['addonAA']['loggerManager']['mainContainer']['loggercont']['table']['level']       = 'Level';
$_LANG['addonAA']['loggerManager']['mainContainer']['loggercont']['table']['request']     = 'Request';
$_LANG['addonAA']['loggerManager']['mainContainer']['loggercont']['table']['response']    = 'Response';
$_LANG['addonAA']['loggerManager']['mainContainer']['loggercont']['table']['before_vars'] = 'Before Vars';
$_LANG['addonAA']['loggerManager']['mainContainer']['loggercont']['table']['vars']        = 'Vars';
$_LANG['addonAA']['loggerManager']['mainContainer']['loggercont']['table']['date']        = 'Date';

$_LANG['addonAA']['loggerManager']['mainContainer']['loggercont']['deleteLoggerModalButton']['button']['deleteLoggerModalButton'] = 'Delete Log';
$_LANG['addonAA']['loggerManager']['deleteLoggerModal']['modal']['deleteLoggerModal']                                             = 'Delete Log';
$_LANG['addonAA']['loggerManager']['deleteLoggerModal']['deleteLoggerForm']['confirmLabelRemoval']                                = 'Are you sure that you want to remove the selected log?';
$_LANG['addonAA']['loggerManager']['deleteLoggerModal']['baseAcceptButton']['title']                                              = 'Delete';
$_LANG['addonAA']['loggerManager']['deleteLoggerModal']['baseCancelButton']['title']                                              = 'Cancel';

$_LANG['loggerDeletedSuccesfully']  = 'The log has been deleted successfully';
$_LANG['loggersDeletedSuccesfully'] = 'The logs have been deleted successfully';

$_LANG['addonAA']['loggerManager']['mainContainer']['loggercont']['massDeleteLoggerButton']['button']['massDeleteLoggerButton'] = 'Delete Logs';

$_LANG['addonAA']['loggerManager']['massDeleteLoggerModal']['modal']['massDeleteLoggerModal']          = 'Delete Logs';
$_LANG['addonAA']['loggerManager']['massDeleteLoggerModal']['baseAcceptButton']['title']               = 'Confirm';
$_LANG['addonAA']['loggerManager']['massDeleteLoggerModal']['baseCancelButton']['title']               = 'Cancel';
$_LANG['addonAA']['loggerManager']['massDeleteLoggerModal']['deleteLoggerForm']['confirmLabelRemoval'] = 'Are you sure that you want to remove the selected logs?';

$_LANG['addonAA']['loggerManager']['mainContainer']['loggercont']['deleteAllLoggersButton']['button']['deleteAllLoggersButton'] = 'Delete All Logs';
$_LANG['addonAA']['loggerManager']['button']['deleteAllLoggersButton']                                                          = 'Delete All Logs';

$_LANG['addonAA']['loggerManager']['button']['massDeleteLoggerButton'] = 'Delete Logs';

$_LANG['addonAA']['loggerManager']['deleteAllLoggersModal']['modal']['deleteAllLoggersModal']             = 'Delete All Logs';
$_LANG['addonAA']['loggerManager']['deleteAllLoggersModal']['deleteAllLoggerForm']['confirmLabelRemoval'] = 'Are you sure that you want to remove all logs?';
$_LANG['addonAA']['loggerManager']['deleteAllLoggersModal']['baseAcceptButton']['title']                  = 'Confirm';
$_LANG['addonAA']['loggerManager']['deleteAllLoggersModal']['baseCancelButton']['title']                  = 'Cancel';

$_LANG['updateChargeLog'] = "The invoice with the ID :invoiceId: has been updated according to the :gatewayName: billing rules.";


$_LANG['addonAA']['pagesLabels']['label']['documentation3'] = 'Documentation';

$_LANG['addonAA']['pagesLabels']['label']['settings'] = 'Settings';

$_LANG['addonAA']['settings']['mainContainer']['baseStandaloneFormExtSections']['firstSection']['leftPage']['isShowCharge']['isShowCharge'] = 'Show Charge';
$_LANG['addonAA']['settings']['mainContainer']['baseStandaloneFormExtSections']['firstSection']['leftPage']['formatChargeText']['formatChargeText'] = 'Charge Format';
$_LANG['addonAA']['settings']['mainContainer']['baseStandaloneFormExtSections']['firstSection']['rightPage']['isChargesTable']['isChargesTable'] = 'Show Charges Table';
$_LANG['addonAA']['settings']['mainContainer']['baseStandaloneFormExtSections']['firstSection']['rightPage']['chargeTableId']['chargeTableId'] = 'Charges Table Tag';
$_LANG['addonAA']['settings']['mainContainer']['baseStandaloneFormExtSections']['firstSection']['rightPage']['chargeTableIdSelect']['chargeTableIdSelect'] = 'Select Template';
$_LANG['addonAA']['settings']['mainContainer']['baseStandaloneFormExtSections']['baseSubmitButton']['button']['submit'] = 'Save';

$_LANG['addonAA']['settings']['choseTemplate'] = 'Use "Charges Table Tag"';
$_LANG['addonAA']['settings']['standardTemplate'] = 'Standard';
$_LANG['addonAA']['settings']['boxesTemplate'] = 'Boxes';
$_LANG['addonAA']['settings']['modernTemplate'] = 'Modern';
$_LANG['addonAA']['settings']['cloudSliderTemplate'] = 'Cloud Slider';
$_LANG['addonAA']['settings']['premiumComparisonTemplate'] = 'Premium Comparison';
$_LANG['addonAA']['settings']['pureComparisonTemplate'] = 'Pure Comparison';
$_LANG['addonAA']['settings']['supremeComparisonTemplate'] = 'Supreme Comparison';
$_LANG['addonAA']['settings']['universalSliderTemplate'] = 'Universal Slider';

$_LANG['addonAA']['settings']['mainContainer']['baseStandaloneFormExtSections']['firstSection']['leftPage']['isShowCharge']['description'] = 'If enabled, the set charge or discount will be displayed in the client area.';
$_LANG['addonAA']['settings']['mainContainer']['baseStandaloneFormExtSections']['firstSection']['leftPage']['formatChargeText']['description'] = 'Provide the form of a description displayed on the invoice updated with a charge or a discount.';
$_LANG['addonAA']['settings']['mainContainer']['baseStandaloneFormExtSections']['firstSection']['rightPage']['isChargesTable']['description'] = 'If enabled, the table with all specified charges and discounts is displayed in the client area.';
$_LANG['addonAA']['settings']['mainContainer']['baseStandaloneFormExtSections']['firstSection']['rightPage']['chargeTableIdSelect']['description'] = 'Select the used order template. If a custom template is used, choose the "Charges Table Tag" option.';
$_LANG['addonAA']['settings']['mainContainer']['baseStandaloneFormExtSections']['firstSection']['rightPage']['chargeTableId']['description'] = 'Provide a unique div tag element of a template after which the charges table will be displayed.';

$_LANG['addonAA']['settings']['mainContainer']['baseStandaloneFormExtSections']['firstSection']['rightPage']['disableLogs']['disableLogs'] = 'Disable Logs';
$_LANG['addonAA']['settings']['mainContainer']['baseStandaloneFormExtSections']['firstSection']['rightPage']['disableLogs']['description'] = 'If this option is enabled, logs are not saved in the database. Remember that logs provide valuable information about the module\'s operation.';

/* * ********************************************************************************************************************
 *                                                    CLIENT AREA                                                      *
 * ******************************************************************************************************************** */

$_LANG['addonCA']['pagesLabels']['label']['home'] = "Owned Passwords";

$_LANG['addonCA']['pageNotFound'] = "Page Not Found";

$_LANG['addonCA']['charge']['chargeCheckout'] = 'Gateway Charge';
$_LANG['addonCA']['charge']['discountCheckout'] = 'Gateway Discount';

$_LANG['addonCA']['charge']['name'] = 'Name';
$_LANG['addonCA']['charge']['provision'] = 'Provision';
$_LANG['addonCA']['charge']['calculated'] = 'Calculated';
$_LANG['addonCA']['charge']['paymentGatewayCharges'] = 'Payment Gateway Charges';
$_LANG['addonCA']['charge']['Total'] = 'Total';




/* --------------------------------------------- 2.1.0 --------------------------------------------- */
$_LANG['formValidationError']                                    = 'Form Validation Error';
$_LANG['FormValidators']['thisFieldCannotBeEmpty']               = 'This field cannot be empty';
$_LANG['FormValidators']['PleaseProvideANumericValue']           = 'Please provide a numeric value';
$_LANG['FormValidators']['PleaseProvideANumericValueBetween']    = 'Please provide a numeric value between :minValue: and :maxValue:';
$_LANG['FormValidators']['PleaseProvideAPriceEqualOrHigherThan'] = 'Please provide a numeric value equal to :eqValue: or higher than :minValue:';
$_LANG['FormValidators']['PleaseProvideAPriceEqualOrHigher']     = 'Please provide a numeric value equal or higher than :minValue:';


$_LANG['addonAA']['gateways']['editGatewaySettingsModal']['baseForm']['afterPromoCode']['afterPromoCode'] = 'Charge After Applying Promo Code';
$_LANG['addonAA']['gateways']['editGatewaySettingsModal']['baseForm']['afterPromoCode']['description'] = 'If enabled, the charge will be calculated after the promo code has been applied.';



/* --------------------------------------------- Debug Logs --------------------------------------------- */
$_LANG['addonAA']['debugLogs']['mainContainer']['debugLogs']['table']['id'] = 'ID';
$_LANG['addonAA']['debugLogs']['mainContainer']['debugLogs']['table']['clientID'] = 'Client ID';
$_LANG['addonAA']['debugLogs']['mainContainer']['debugLogs']['table']['invoiceID'] = 'Invoice ID';
$_LANG['addonAA']['debugLogs']['mainContainer']['debugLogs']['table']['hook'] = 'Hook';
$_LANG['addonAA']['debugLogs']['mainContainer']['debugLogs']['table']['action'] = 'Action';
$_LANG['addonAA']['debugLogs']['mainContainer']['debugLogs']['table']['invoiceBefore'] = 'Invoice Before Action';
$_LANG['addonAA']['debugLogs']['mainContainer']['debugLogs']['table']['invoiceAfter'] = 'Invoice After Action';
$_LANG['addonAA']['debugLogs']['mainContainer']['debugLogs']['table']['conditions'] = 'Conditions';
$_LANG['addonAA']['debugLogs']['mainContainer']['debugLogs']['table']['settings'] = 'Settings';
$_LANG['addonAA']['debugLogs']['mainContainer']['debugLogs']['table']['created_at'] = 'Date';

$_LANG['addonAA']['debugLogs']['mainContainer']['debugLogs']['showChangesButton']['button']['showChangesButton'] = 'Show Changes';


$_LANG['addonAA']['pagesLabels']['label']['DebugLogs'] = 'Debug Logs';

$_LANG['addonAA']['debugLogs']['showChangesModal']['modal']['baseEditModal'] = "Debug Logs";

$_LANG['addonAA']['debugLogs']['showChangesModal']['baseCancelButton']['title'] = "Close";

$_LANG['addonAA']['debugLogs']['showChangesModal']['modal']['invoiceBefore'] = 'Invoice Before Action';
$_LANG['addonAA']['debugLogs']['showChangesModal']['modal']['invoiceAfter'] = 'Invoice After Action';
$_LANG['addonAA']['debugLogs']['showChangesModal']['modal']['conditions'] = 'Conditions';
$_LANG['addonAA']['debugLogs']['showChangesModal']['modal']['settings'] = 'Settings';
$_LANG['addonAA']['debugLogs']['showChangesModal']['modal']['diff'] = 'Diff Invoices';


/* --------------------------------------------- Items -> Other --------------------------------------------- */
$_LANG['addonAA']['items']['mainContainer']['itemsPage']['Other'] = 'Other';
$_LANG['addonAA']['items']['mainContainer']['itemsPage']['otherPage']['table']['options'] = 'Options';
$_LANG['addonAA']['items']['mainContainer']['itemsPage']['otherPage']['table']['enabled'] = 'Chargeable';
$_LANG['addonAA']['items']['funds'] = 'Adding Funds To Credit Deposit';
$_LANG['addonAA']['items']['customInvoice'] = 'Custom Invoice Items';
$_LANG['addonAA']['items']['lateFee'] = 'Late Fee Invoice Items';
$_LANG['addonAA']['items']['mainContainer']['itemsPage']['otherPage']['otherChargeableMassAction']['button']['otherChargeableMassAction'] = 'Set Chargeable';
$_LANG['otherChangesSaved'] = 'The selected option chargeable status has been changed successfully';

$_LANG['addonAA']['items']['otherModal']['modal']['otherModal'] = 'Set Chargeable';
$_LANG['addonAA']['items']['otherModal']['otherForm']['enabled']['enabled'] = 'Chargeable Status';
$_LANG['addonAA']['items']['otherModal']['baseAcceptButton']['title'] = 'Confirm';
$_LANG['addonAA']['items']['otherModal']['baseCancelButton']['title'] = 'Cancel';
$_LANG['addonAA']['items']['otherModal']['description']  = 'Toggle the chargeable status for the elements of the selected options. If enabled, the charges or discounts will be applied to the elements according to the configured billing rules.';

$_LANG['addonAA']['debugLogs']['mainContainer']['debugLogs']['cleanLogsButton']['button']['cleanLogsButton'] = 'Truncate Logs';
$_LANG['addonAA']['debugLogs']['cleanLogsModal']['cleanAllForm']['confirmTruncate'] = "Are you sure that you want to truncate the debug logs table?";
$_LANG['addonAA']['debugLogs']['cleanLogsModal']['modal']['baseEditModal'] = 'Truncate Logs Table';
$_LANG['addonAA']['debugLogs']['cleanLogsModal']['baseAcceptButton']['title'] = 'Confirm';
$_LANG['addonAA']['debugLogs']['cleanLogsModal']['baseCancelButton']['title'] = 'Cancel';




