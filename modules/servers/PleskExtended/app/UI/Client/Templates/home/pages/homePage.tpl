{assign var="assetsUrl" value=$rawObject->getAssetsUrl()}
{if $rawObject->getEnabledFeatures()}
    <div class="lu-h4 lu-m-b-3x lu-m-t-3x">{$MGLANG->absoluteT('addonCA','homePage','manageHeader')}</div>
    <div class="lu-tiles lu-row row--eq-height">
        {foreach from=$rawObject->getEnabledFeatures() key=setting item=controller}
            <div class="lu-col-sm-20p">
                <a class="lu-tile lu-tile--btn" href="{$rawObject->getRedirectUrl($controller)}">
                    <div class="lu-i-c-6x">
                        <img src="{$assetsUrl}/img/plesk/icon-{$controller}.png" alt="">
                    </div>
                    <div class="lu-tile__title">{$MGLANG->absoluteT('addonCA' , 'homeIcons' ,$controller)}</div>
                </a>
            </div>
        {/foreach}
    </div>
{/if}
{if $rawObject->getRedirectLinks()}
    <div class="lu-h4 lu-m-b-3x lu-m-t-2x">{$MGLANG->absoluteT('addonCA','homePage','oneclickHeader')}</div>
    <div class="lu-tiles lu-row lu-row--eq-height">
        {foreach from=$rawObject->getRedirectLinks() key=where item=link}
            {assign var="redirectUrl" value=$rawObject->getRedirectUrlOneClick($where)}
            <div class="lu-col-sm-20p">
                <a class="lu-tile lu-tile--btn" href="#" @mouseDown.middle="makeCustomAction('oneClickLoginRedirect',['{$redirectUrl}'],$event)" @click="makeCustomAction('oneClickLoginRedirect',['{$redirectUrl}'],$event)">
                    <div class="lu-i-c-6x">
                        <img src="{$assetsUrl}/img/plesk/icon-{$where|ucfirst}.png" alt="">
                    </div>
                    <div class="lu-tile__title">{$MGLANG->absoluteT('addonCA' , 'homeIcons' ,$where)}</div>
                </a>
            </div>
        {/foreach}

    </div>
{/if}