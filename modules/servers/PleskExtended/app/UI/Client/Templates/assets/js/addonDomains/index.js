

function fillWithMe(inputs,event)
{
    var text = $(event.target).val();
    $.each(inputs,function(key,input){
        if(input.name == 'directory')
        {
            var domain = $('select[name="domains"]').val();
            $(input).val(text + '.' + domain);
        }
        else
        {
            $(input).val(text);
        }
        
    });
    
    $('input[name="dir"]').val('/public_html/' + text);
}
