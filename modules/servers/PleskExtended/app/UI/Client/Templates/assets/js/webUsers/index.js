/* Generate PW */
function generateRandomPassword() {
    var length = 12,
            charset = "abcdefghijklnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()",
            password = "";
    var input = $('input[name="password"]');
    for (var i = 0, n = charset.length; i < length; ++i) {
        password += charset.charAt(Math.floor(Math.random() * n));
    }

    input.val(password);
    input.parent().css('border-bottom','2px solid #449d44');
}

/* calculate PW strange */
function calculatePasswordStr()
{
    var input = $('input[name="password"]');
    var result = zxcvbn(input.val());
    var score = parseInt(result.score);
    var score_to_color = ['#c9302c', '#d9534f', '#ec971f', '#5cb85c', '#449d44'];
    input.parent().css('border-bottom','2px solid' + score_to_color[score]);
}