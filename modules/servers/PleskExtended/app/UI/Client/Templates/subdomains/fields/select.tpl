    <select 
        class="lu-form-control" 
        name="{$rawObject->getName()}"
        {if $rawObject->isDisabled()}disabled="disabled"{/if}
        {if $rawObject->isMultiple()}data-options="removeButton:true; resotreOnBackspace:true; dragAndDrop:true; maxItems: null;" multiple="multiple"{/if}
        {foreach $htmlAttributes as $aValue} {$aValue@key}="{$aValue}" {/foreach}
        >
        {if $rawObject->getValue()|is_array}
            {foreach from=$rawObject->getAvailableValues() key=opValue item=option}
                <option value="{$opValue}" {if $opValue|in_array:$rawObject->getValue()}selected{/if}>
                    {$option}
                </option>
            {/foreach}            
        {else}
            {foreach from=$rawObject->getAvailableValues() key=opValue item=option}
                <option value="{$opValue}" {if $opValue===$rawObject->getValue()}selected{/if}>
                    {$option}
                </option>
            {/foreach}
        {/if}
    </select>