<input class="{$rawObject->getClasses()}" type="text" placeholder="{$rawObject->getPlaceholder()}" name="{$rawObject->getName()}"
       value="{$rawObject->getValue()}" {if $rawObject->isDisabled()}disabled="disabled"{/if}
       {foreach $htmlAttributes as $aValue} {$aValue@key}="{$aValue}" {/foreach}>