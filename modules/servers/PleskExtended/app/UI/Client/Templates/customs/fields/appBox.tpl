<div class="lu-col-sm-20p" style="margin-right:12px;margin-left:-12px">
    <a class="lu-tile lu-tile--btn" href="{$rawObject->getRawUrlFixed($rawObject->getSid(),$rawObject->getVersion())}">
        <div class="lu-i-c-6x">
            <img 
                src="{$rawObject->getImage()}" alt="">
        </div>
        <div class="lu-tile__title">{$rawObject->getAppName()} {$rawObject->getVersion()}</div>
    </a>
</div>
