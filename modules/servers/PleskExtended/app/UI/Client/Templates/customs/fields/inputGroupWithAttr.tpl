<div class="lu-form-group" {foreach $htmlAttributes as $aValue} {$aValue@key}="{$aValue}" {/foreach}>
    <label class="lu-form-label">
        {if $rawObject->isRawTitle()}{$rawObject->getRawTitle()}{elseif $rawObject->getTitle()}{$MGLANG->T($rawObject->getTitle())}{/if}
    </label>
        <div class="lu-input-group">
            {foreach from=$rawObject->getFields() item=field}
                {$field->getHtml()}
            {/foreach}
        </div>
    <div class="lu-form-feedback lu-form-feedback--icon" hidden="hidden">
    </div>    
</div>