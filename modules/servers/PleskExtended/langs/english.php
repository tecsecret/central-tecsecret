<?php

$_LANG['token']                                    = ', Error Token:';
$_LANG['generalError']                             = 'An unexpected error has occurred. Check the logs and contact the support team.';
$_LANG['generalErrorClientArea']                   = 'An unexpected error has occurred. Contact the administrator.';
$_LANG['permissionsStorage']                       = ':storage_path: settings are not sufficient. Please change permissions to the value 777.';
$_LANG['undefinedAction']                          = 'Undefined action';
$_LANG['changesHasBeenSaved']                      = 'Changes have been saved successfully';
$_LANG['Monthly']                                  = 'Monthly';
$_LANG['Free Account']                             = 'Free Account';
$_LANG['Passwords']                                = 'Passwords';
$_LANG['labelAddedSuccesfully']                    = 'The label has been added successfully';
$_LANG['Nothing to display']                       = 'Nothing to display';
$_LANG['Search']                                   = 'Search';
$_LANG['Previous']                                 = 'Previous';
$_LANG['Next']                                     = 'Next';
$_LANG['searchPlacecholder']                       = 'Search...';
$_LANG['FormValidators']['invalidDatabaseName']               = 'The name can contain alphanumeric, dot, dash, and underscore symbols only.';
$_LANG['FormValidators']['invalidSpamSensivity']              = 'The provided value is invalid for a sensitivity parameter';
$_LANG['FormValidators']['thisFieldCannotBeEmpty']            = 'This field cannot be empty';
$_LANG['FormValidators']['PleaseProvideANumericValueBetween'] = 'Please provide a number between 0 and 999';
$_LANG['FormValidators']['invalidDomain']                     = 'The domain is invalid';
$_LANG['FormValidators']['invalidEmail']                      = 'The email is invalid';
$_LANG['FormValidators']['invalidEmailAddress']               = "Provided email address is incorrect.";
$_LANG['Synchronize Client Details']                          = 'Synchronize Client Details';
$_LANG['Plesk clients details will be updated']               = 'Plesk clients details will be updated';

$_LANG['noDataAvalible']                 = 'No Data Available';
$_LANG['datatableItemsSelected']         = 'Items Selected';
$_LANG['validationErrors']['emptyField'] = 'This field is required';
$_LANG['bootstrapswitch']['disabled']    = 'Disabled';
$_LANG['bootstrapswitch']['enabled']     = 'Enabled';

$_LANG['addonCA']['breadcrumbs']['PleskExtended'] = 'PleskExtended';

$_LANG['addonCA']['pageNotFound']['title']       = 'PAGE NOT FOUND';
$_LANG['addonCA']['pageNotFound']['description'] = 'The provided URL does not exist on this page. If you are sure that an error has occurred here, please contact support.';
$_LANG['addonCA']['pageNotFound']['button']      = 'Return to the product page';

$_LANG['addonCA']['errorPage']['title']       = 'AN ERROR OCCURRED';
$_LANG['addonCA']['errorPage']['description'] = 'An error has occurred. Please contact administrator and pass the details:';
$_LANG['addonCA']['errorPage']['button']      = 'Return to the product page';
$_LANG['addonCA']['errorPage']['error']       = 'ERROR';

$_LANG['addonCA']['errorPage']['errorCode']    = 'Error Code';
$_LANG['addonCA']['errorPage']['errorToken']   = 'Error Token';
$_LANG['addonCA']['errorPage']['errorTime']    = 'Time';
$_LANG['addonCA']['errorPage']['errorMessage'] = 'Message';

$_LANG['addonAA']['pageNotFound']['title']       = 'PAGE NOT FOUND';
$_LANG['addonAA']['pageNotFound']['description'] = 'An error has occurred. Please contact administrator.';
$_LANG['addonAA']['pageNotFound']['button']      = 'Return to the module page';


$_LANG['addonAA']['errorPage']['title']       = 'AN ERROR OCCURRED';
$_LANG['addonAA']['errorPage']['description'] = 'An error has occurred. Please contact administrator and pass the details:';
$_LANG['addonAA']['errorPage']['button']      = 'Return to the module page';
$_LANG['addonAA']['errorPage']['error']       = 'ERROR';

$_LANG['addonAA']['errorPage']['errorCode']    = 'Error Code';
$_LANG['addonAA']['errorPage']['errorToken']   = 'Error Token';
$_LANG['addonAA']['errorPage']['errorTime']    = 'Time';
$_LANG['addonAA']['errorPage']['errorMessage'] = 'Message';

/* * ********************************************************************************************************************
 *                                                   ERROR CODES                                                        *
 * ******************************************************************************************************************** */
$_LANG['errorCodeMessage']['Uncategorised error occured']         = 'Unexpected Error';
$_LANG['errorCodeMessage']['Database error']                      = 'Database error';
$_LANG['errorCodeMessage']['Provided controller does not exists'] = 'Page not found';
$_LANG['errorCodeMessage']['Invalid Error Code!']                 = 'Unexpected Error';

/* * ********************************************************************************************************************
 *                                                   MODULE REQUIREMENTS                                                *
 * ******************************************************************************************************************** */
$_LANG['unfulfilledRequirement']['In order for the module to work correctly, please remove the following file: :remove_file_requirement:'] = 'In order for the module to work correctly, please remove the following file: :remove_file_requirement:';

/* * ********************************************************************************************************************
 *                                                   ADMIN AREA                                                        *
 * ******************************************************************************************************************** */

$_LANG['addonAA']['datatables']['next']        = 'Next';
$_LANG['addonAA']['datatables']['previous']    = 'Previous';
$_LANG['addonAA']['datatables']['zeroRecords'] = 'Nothing to display';

// -------------------------------------------------> MENU <--------------------------------------------------------- //

$_LANG['addonCA']['homePage']['manageHeader']              = 'Manage Account';
$_LANG['addonCA']['sidebarMenu']['mg-provisioning-module'] = 'Manage Account';
$_LANG['addonCA']['sidebarMenu']['addonDomains']           = 'Addon Domains';
$_LANG['addonCA']['sidebarMenu']['applications']           = 'Applications';
$_LANG['addonCA']['sidebarMenu']['backups']                = 'Backups';
$_LANG['addonCA']['sidebarMenu']['databases']              = 'Databases';
$_LANG['addonCA']['sidebarMenu']['domainAliases']          = 'Domain Aliases';
$_LANG['addonCA']['sidebarMenu']['dnsSettings']            = 'DNS Settings';
$_LANG['addonCA']['sidebarMenu']['ftp']                    = 'FTP Access';
$_LANG['addonCA']['sidebarMenu']['gitRepositories']        = 'Git Repositories';
$_LANG['addonCA']['sidebarMenu']['emails']                 = 'Email Addresses';
$_LANG['addonCA']['sidebarMenu']['emailForwarders']        = 'Email Forwarders';
$_LANG['addonCA']['sidebarMenu']['nodeJs']                 = 'Node.js';
$_LANG['addonCA']['sidebarMenu']['spamFilter']             = 'Spam Filter';
$_LANG['addonCA']['sidebarMenu']['subdomains']             = 'Subdomains';
$_LANG['addonCA']['sidebarMenu']['ssl']                    = 'SSL Certificates';
$_LANG['addonCA']['sidebarMenu']['webUsers']               = 'Web Users';
$_LANG['addonCA']['sidebarMenu']['phpSettings']            = 'PHP Settings';
$_LANG['addonCA']['sidebarMenu']['logRotation']            = 'Log Rotation';

$_LANG['addonCA']['sidebarMenu']['one-click-login'] = 'One Click Login';
$_LANG['addonCA']['sidebarMenu']['plesk']           = 'Plesk';
$_LANG['addonCA']['sidebarMenu']['webmail']         = 'Webmail';

$_LANG['addonCA']['homeIcons']['AddonDomains']    = 'Addon Domains';
$_LANG['addonCA']['homeIcons']['Applications']    = 'Applications';
$_LANG['addonCA']['homeIcons']['Backups']         = 'Backups';
$_LANG['addonCA']['homeIcons']['Databases']       = 'Databases';
$_LANG['addonCA']['homeIcons']['DnsSettings']     = 'DNS Settings';
$_LANG['addonCA']['homeIcons']['DomainAliases']   = 'Domain Aliases';
$_LANG['addonCA']['homeIcons']['EmailForwarders'] = 'Email Forwarders';
$_LANG['addonCA']['homeIcons']['Emails']          = 'Email Addresses';
$_LANG['addonCA']['homeIcons']['Ftp']             = 'FTP Access';
$_LANG['addonCA']['homeIcons']['GitRepositories'] = 'Git Repositories';
$_LANG['addonCA']['homeIcons']['NodeJs']          = 'Node.js';
$_LANG['addonCA']['homeIcons']['SpamFilter']      = 'Spam Filter';
$_LANG['addonCA']['homeIcons']['Subdomains']      = 'Subdomains';
$_LANG['addonCA']['homeIcons']['Ssl']             = 'SSL Certificates';
$_LANG['addonCA']['homeIcons']['WebUsers']        = 'Web Users';
$_LANG['addonCA']['homeIcons']['PhpSettings']     = 'PHP Settings';
$_LANG['addonCA']['homeIcons']['LogRotation']     = 'Log Rotation';

$_LANG['addonCA']['homePage']['oneclickHeader'] = 'One Click Login';
$_LANG['addonCA']['homeIcons']['plesk']         = 'Plesk';
$_LANG['addonCA']['homeIcons']['webmail']       = 'Webmail';




// ---------------------------------------------------- Applications  ---------------------------------------------------------


$_LANG['addonCA']['applications']['mainContainer']['applications']['ApplicationsPageTitle']                     = 'Applications';
$_LANG['addonCA']['applications']['mainContainer']['applications']['ApplicationsPageDescription']               = "Manage your applications here. View the already set up applications along with backups, and install new ones.";
$_LANG['addonCA']['applications']['mainContainer']['applicationsPage']['ApplicationsTab']                       = 'Applications';
$_LANG['addonCA']['applications']['mainContainer']['applicationsPage']['applicationsNewPageTab']                = 'Install New';
$_LANG['addonCA']['applications']['mainContainer']['applicationsPage']['applicationsTable']['table']['name']    = 'Name';
$_LANG['addonCA']['applications']['mainContainer']['applicationsPage']['applicationsTable']['table']['time']    = 'Installation Time';
$_LANG['addonCA']['applications']['mainContainer']['applicationsPage']['applicationsTable']['table']['version'] = 'Version';

$_LANG['addonCA']['applications']['mainContainer']['applicationsPage']['backupsTable']['restoreBackupButton']['button']['restoreBackupButton'] = 'Restore';
$_LANG['addonCA']['applications']['restoreBackupAppModal']['modal']['restoreBackupAppModal']                                                   = 'Restore Backup';
$_LANG['addonCA']['applications']['restoreBackupAppModal']['restoreBackupAppForm']['confirmAppBackup']                                         = 'The selected backup will be restored.';
$_LANG['addonCA']['applications']['restoreBackupAppModal']['baseAcceptButton']['title']                                                        = 'Confirm';
$_LANG['addonCA']['applications']['restoreBackupAppModal']['baseCancelButton']['title']                                                        = 'Cancel';

$_LANG['addonCA']['applications']['mainContainer']['applicationsPage']['BackupsTab']                       = 'Backups';
$_LANG['addonCA']['applications']['mainContainer']['applicationsPage']['backupsTable']['table']['name']    = 'Name';
$_LANG['addonCA']['applications']['mainContainer']['applicationsPage']['backupsTable']['table']['version'] = 'Version';

$_LANG['addonCA']['applications']['mainContainer']['applicationsPage']['applicationsTable']['deleteButton']['button']['deleteButton']        = 'Delete';
$_LANG['addonCA']['applications']['mainContainer']['applicationsPage']['applicationsTable']['backupButton']['button']['backupButton']        = 'Create Backup';
$_LANG['addonCA']['applications']['mainContainer']['applicationsPage']['backupsTable']['deleteBackupButton']['button']['deleteBackupButton'] = 'Delete';

$_LANG['addonCA']['applications']['deleteModal']['modal']['deleteModal']                   = 'Delete Application';
$_LANG['addonCA']['applications']['deleteModal']['deleteForm']['confirmApplicationDelete'] = 'Are you sure that you want to delete this application?';
$_LANG['addonCA']['applications']['deleteModal']['baseAcceptButton']['title']              = 'Confirm';
$_LANG['addonCA']['applications']['deleteModal']['baseCancelButton']['title']              = 'Cancel';

$_LANG['addonCA']['applications']['backupAppModal']['modal']['backupAppModal']           = 'Create Backup';
$_LANG['addonCA']['applications']['backupAppModal']['backupAppForm']['confirmAppBackup'] = 'Are you sure that you want to create a backup for this application?';
$_LANG['addonCA']['applications']['backupAppModal']['baseAcceptButton']['title']         = 'Create';
$_LANG['addonCA']['applications']['backupAppModal']['baseCancelButton']['title']         = 'Cancel';

$_LANG['addonCA']['applications']['deleteBackupAppModal']['modal']['deleteBackupAppModal']              = 'Delete Backup';
$_LANG['addonCA']['applications']['deleteBackupAppModal']['deleteBackupAppForm']['confirmDeleteBackup'] = 'Are you sure that you want to delete this backup?';
$_LANG['addonCA']['applications']['deleteBackupAppModal']['baseAcceptButton']['title']                  = 'Confirm';
$_LANG['addonCA']['applications']['deleteBackupAppModal']['baseCancelButton']['title']                  = 'Cancel';


$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['directory']['directoryDescription']         = 'The directory is relative to your domain and should not exist. e.g. To install at http://mydomain/dir/ just type dir. To install only in http://mydomain/ leave this empty.';
$_LANG['addonCA']['applications']['mainContainer']['applicationsPage']['applicationsNewPage']['appSection']['applications']['applications']         = 'APPLICATIONS';
$_LANG['addonCA']['applications']['mainContainer']['applicationsPage']['applicationsNewPage']['appSection']['admanager']['admanager']               = 'AD MANAGEMENT';
$_LANG['addonCA']['applications']['mainContainer']['applicationsPage']['applicationsNewPage']['appSection']['blogs']['blogs']                       = 'BLOGS';
$_LANG['addonCA']['applications']['mainContainer']['applicationsPage']['applicationsNewPage']['appSection']['calendars']['calendars']               = 'CALENDARS';
$_LANG['addonCA']['applications']['mainContainer']['applicationsPage']['applicationsNewPage']['appSection']['cms']['cms']                           = 'CMS / PORTALS';
$_LANG['addonCA']['applications']['mainContainer']['applicationsPage']['applicationsNewPage']['appSection']['customersupport']['customersupport']   = 'CUSTOMER SUPPORT';
$_LANG['addonCA']['applications']['mainContainer']['applicationsPage']['applicationsNewPage']['appSection']['dbtools']['dbtools']                   = 'DB TOOLS';
$_LANG['addonCA']['applications']['mainContainer']['applicationsPage']['applicationsNewPage']['appSection']['ecommerce']['ecommerce']               = 'E-COMMERCE';
$_LANG['addonCA']['applications']['mainContainer']['applicationsPage']['applicationsNewPage']['appSection']['educational']['educational']           = 'EDUCATIONAL';
$_LANG['addonCA']['applications']['mainContainer']['applicationsPage']['applicationsNewPage']['appSection']['erp']['erp']                           = 'ERP';
$_LANG['addonCA']['applications']['mainContainer']['applicationsPage']['applicationsNewPage']['appSection']['files']['files']                       = 'FILE MANAGEMENT';
$_LANG['addonCA']['applications']['mainContainer']['applicationsPage']['applicationsNewPage']['appSection']['forums']['forums']                     = 'FORUMS';
$_LANG['addonCA']['applications']['mainContainer']['applicationsPage']['applicationsNewPage']['appSection']['frameworks']['frameworks']             = 'FRAMEWORKS';
$_LANG['addonCA']['applications']['mainContainer']['applicationsPage']['applicationsNewPage']['appSection']['galleries']['galleries']               = 'IMAGE GALLERIES';
$_LANG['addonCA']['applications']['mainContainer']['applicationsPage']['applicationsNewPage']['appSection']['igalleries']['igalleries']             = 'IMAGE GALLERIES';
$_LANG['addonCA']['applications']['mainContainer']['applicationsPage']['applicationsNewPage']['appSection']['games']['games']                       = 'GAMING';
$_LANG['addonCA']['applications']['mainContainer']['applicationsPage']['applicationsNewPage']['appSection']['guestbooks']['guestbooks']             = 'GUEST BOOKS';
$_LANG['addonCA']['applications']['mainContainer']['applicationsPage']['applicationsNewPage']['appSection']['libraries']['libraries']               = 'LIBRARIES';
$_LANG['addonCA']['applications']['mainContainer']['applicationsPage']['applicationsNewPage']['appSection']['mail']['mail']                         = 'MAILS';
$_LANG['addonCA']['applications']['mainContainer']['applicationsPage']['applicationsNewPage']['appSection']['microblogs']['microblogs']             = 'MICRO BLOGS';
$_LANG['addonCA']['applications']['mainContainer']['applicationsPage']['applicationsNewPage']['appSection']['music']['music']                       = 'MUSIC';
$_LANG['addonCA']['applications']['mainContainer']['applicationsPage']['applicationsNewPage']['appSection']['others']['others']                     = 'OTHERS';
$_LANG['addonCA']['applications']['mainContainer']['applicationsPage']['applicationsNewPage']['appSection']['polls']['polls']                       = 'POLLS AND ANALYTICS';
$_LANG['addonCA']['applications']['mainContainer']['applicationsPage']['applicationsNewPage']['appSection']['projectman']['projectman']             = 'PROJECT MANAGEMENT';
$_LANG['addonCA']['applications']['mainContainer']['applicationsPage']['applicationsNewPage']['appSection']['rss']['rss']                           = 'RSS';
$_LANG['addonCA']['applications']['mainContainer']['applicationsPage']['applicationsNewPage']['appSection']['socialnetworking']['socialnetworking'] = 'SOCIAL NETWORKING';
$_LANG['addonCA']['applications']['mainContainer']['applicationsPage']['applicationsNewPage']['appSection']['video']['video']                       = 'VIDEO';
$_LANG['addonCA']['applications']['mainContainer']['applicationsPage']['applicationsNewPage']['appSection']['wikis']['wikis']                       = 'WIKIS';

$_LANG['addonCA']['applications']['mainContainer']['applicationsPage']['applicationsNewPage']['appSection']['photos_and_Files']['Photos and Files']               = 'PHOTOS AND FILES';
$_LANG['addonCA']['applications']['mainContainer']['applicationsPage']['applicationsNewPage']['appSection']['content_Management']['Content Management']           = 'CONTENT MANAGEMENT';
$_LANG['addonCA']['applications']['mainContainer']['applicationsPage']['applicationsNewPage']['appSection']['surveys_and_Statistics']['Surveys and Statistics']   = 'SURVEYS AND STATISTICS';
$_LANG['addonCA']['applications']['mainContainer']['applicationsPage']['applicationsNewPage']['appSection']['e-Commerce_and_Business']['e-Commerce and Business'] = 'E-COMMERCE AND BUSINESS';
$_LANG['addonCA']['applications']['mainContainer']['applicationsPage']['applicationsNewPage']['appSection']['eCommerce_and_Business']['ECommerce and Business']   = 'E-COMMERCE AND BUSINESS';
$_LANG['addonCA']['applications']['mainContainer']['applicationsPage']['applicationsNewPage']['appSection']['miscellaneous']['Miscellaneous']                     = 'MISCELLANEOUS';
$_LANG['addonCA']['applications']['mainContainer']['applicationsPage']['applicationsNewPage']['appSection']['community_Building']['Community Building']           = 'COMMUNITY BUILDING';

$_LANG['addonCA']['applications']['installSectionTitle']                                                                                                                                           = 'Install New Application';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['baseSubmitButton']['button']['submit']                                                                       = 'Install';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['domain']['domain']                                                                         = 'Domain';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['directory']['directory']                                                                   = 'Directory';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['database']['database']                                                                     = 'Database Name';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['domain']['domainDescription']                                                              = 'Please choose the domain to install the software.';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['directory']['dirDescription']                                                              = "The directory is relative to your domain and should not exist. For example, to install the software at http://mydomain/dir/ just type in \'dir\'. To install it at http://mydomain/ only, leave the field empty.";
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['database']['dbNameDesc']                                                                   = 'Type in the name of the database to be created for the installation.';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['admin_name']['admin_name']                                                                 = 'Admin Name';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['softdb']['softdb']                                                                         = 'Database Name';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['db_user']['db_user']                                                                       = 'Database User';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['db_pass']['db_pass']                                                                       = 'Database Password';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['dbprefix']['dbprefix']                                                                     = 'Database Prefix';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['autoup']['autoup']                                                                         = 'Auto Update';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['autoup_backup']['autoup_backup']                                                           = 'Auto Update Backup';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['language']['language']                                                                     = 'Language';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['login']['login']                                                                           = 'Admin Login';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['passwd']['passwd']                                                                         = 'Admin Password';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['email']['email']                                                                           = 'Admin Email';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['sitetitle']['sitetitle']                                                                   = 'Site Title';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['content']['content']                                                                       = 'Site Content';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['site_desc']['site_desc']                                                                   = 'Site Description';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['admin_username']['admin_username']                                                         = 'Admin Username';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['admin_pass']['admin_pass']                                                                 = 'Admin Password';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['admin_email']['admin_email']                                                               = 'Admin Email';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['site_name']['site_name']                                                                   = 'Site Name';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['multisite']['multisite']                                                                   = 'Multisite';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['loginizer']['loginizer']                                                                   = 'Lognizer';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['wpclef']['wpclef']                                                                         = 'WordPress Clef';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['admin_realname']['admin_realname']                                                         = 'Admin Name';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['sample_data']['sample_data']                                                               = 'Sample Data';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['site_shortname']['site_shortname']                                                         = 'Site Short Name';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['admin_fname']['admin_fname']                                                               = 'Admin First Name';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['admin_lname']['admin_lname']                                                               = 'Admin Last Name';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['admin_password']['admin_password']                                                         = 'Admin Password';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['content_install']['content_install']                                                       = 'Content';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['site_email']['site_email']                                                                 = 'Company Email';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['license_key']['license_key']                                                               = 'License Key';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['admin_language']['admin_language']                                                         = 'Admin Language';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['end_language']['end_language']                                                             = 'Site Language';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['cron hour']['cron hour']                                                                   = 'Cron (hour)';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['cron day']['cron day']                                                                     = 'Cron (day)';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['cron month']['cron month']                                                                 = 'Cron (month)';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['cron weekday']['cron weekday']                                                             = 'Cron (weekday)';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['admin_nickname']['admin_nickname']                                                         = 'Nickname';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['db_prefix']['db_prefix']                                                                   = 'Table Prefix';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['real_name']['real_name']                                                                   = 'Real Name';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['admin_folder']['admin_folder']                                                             = 'Admin Folder';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['sn']['sn']                                                                                 = 'Site Name';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['cron min']['cron min']                                                                     = 'Cron (min)';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['site_profile']['site_profile']                                                             = 'Site Profile';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['fancyurls']['fancyurls']                                                                   = 'Fancy URLs';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['select_version']['select_version']                                                         = 'Select Version';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['store_name']['store_name']                                                                 = 'Store Name';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['site_dom']['site_dom']                                                                     = 'Site Domain';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['site_subtitle']['site_subtitle']                                                           = 'Site Subtitle';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['store_desc']['store_desc']                                                                 = 'Store Description';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['store_owner']['store_owner']                                                               = 'Store Owner';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['store_address']['store_address']                                                           = 'Store Address';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['db_host']['db_host']                                                                       = 'Database Host';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['company_name']['company_name']                                                             = 'Company Name';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['country_code']['country_code']                                                             = 'Country Code';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['store_time_zone']['store_time_zone']                                                       = 'Store Time Zone';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['store_title']['store_title']                                                               = 'Store Title';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['project_name']['project_name']                                                             = 'Project Name';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['fullname']['fullname']                                                                     = 'Full Name';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['user_username']['user_username']                                                           = 'Username';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['user_pass']['user_pass']                                                                   = 'User Password';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['smtp_port']['smtp_port']                                                                   = 'SMTP Port';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['noreply_email']['noreply_email']                                                           = 'No-reply Email';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['comp_name']['comp_name']                                                                   = 'Company Name';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['birth_date']['birth_date']                                                                 = 'Birth Date';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['gender']['gender']                                                                         = 'Gender';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['wiki_name']['wiki_name']                                                                   = 'Wiki Name';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['app_ref']['app_ref']                                                                       = 'Application Reference';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['conn_prefix']['conn_prefix']                                                               = 'Connection Type Prefix';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['htpassword_cmd']['htpassword_cmd']                                                         = 'htpassword Command';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['svn_cmd']['svn_cmd']                                                                       = 'SVN Command';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['svnadmin_cmd']['svnadmin_cmd']                                                             = 'Svnadmin Command';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['svn_repos_loc']['svn_repos_loc']                                                           = 'SVN Repository Location';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['board_email']['board_email']                                                               = 'Board Email';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['department']['department']                                                                 = 'Department';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['visual_verification_type']['visual_verification_type']                                     = 'Visual Verification Type';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['bbname']['bbname']                                                                         = 'Board Name';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['utf8']['utf8']                                                                             = 'Use UTF-8 Character Set';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['opening']['opening']                                                                       = 'Opening Message';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['first_forum']['first_forum']                                                               = 'First Forum Name';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['imap']['imap']                                                                             = 'Server Address';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['forum_webtag']['forum_webtag']                                                             = 'Default Forum Webtag';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['mail_name']['mail_name']                                                                   = 'Mailing List Name';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['imap_port']['imap_port']                                                                   = 'IMAP Port';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['imap_tls']['imap_tls']                                                                     = 'TLS/SSL';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['smtp']['smtp']                                                                             = 'Server Address';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['smtp_tls']['smtp_tls']                                                                     = 'TLS/SSL';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['bug_email']['bug_email']                                                                   = 'Bug Reports Email';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['feedback_email']['feedback_email']                                                         = 'Email Address For The Contact Form (Feedback)';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['mail_desc']['mail_desc']                                                                   = 'Mailing List Description';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['policy_desc']['policy_desc']                                                               = 'Privacy Policy';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['physical_desc']['physical_desc']                                                           = 'Physical Address';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['list_pass']['list_pass']                                                                   = 'Mailing List Password';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['wiki_mail']['wiki_mail']                                                                   = 'Wiki Email';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['system_name']['system_name']                                                               = 'System Name';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['organisation_name']['organisation_name']                                                   = 'Organisation Name';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['organisation_initials']['organisation_initials']                                           = 'Organisation Initials';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['primary_assessment_scale']['primary_assessment_scale']                                     = 'Primary Assessment Scale';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['title']['title']                                                                           = 'Title';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['sname']['sname']                                                                           = 'Surname';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['fname']['fname']                                                                           = 'First Name';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['city_add']['city_add']                                                                     = 'City';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['short_site_name']['short_site_name']                                                       = 'Short Site Name';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['out_host_server']['out_host_server']                                                       = 'Server Address';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['out_m_port']['out_m_port']                                                                 = 'SMTP Port';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['email_name']['email_name']                                                                 = 'Email Name';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['full_Version']['Full_Version']                                                             = 'Full Version';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['locale']['locale']                                                                         = 'Interface Language';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['databaseName']['databaseName']                                                             = 'Database Name';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['databaseLogin']['databaseLogin']                                                           = 'Database Login';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['databasePassword']['databasePassword']                                                     = 'Database Password';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['databasePrefix']['databasePrefix']                                                         = 'Database Prefix';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['wpboldgrid']['wpboldgrid']                                                                 = 'WP BoldGrid';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['overwriteExisting']['overwriteExisting']                                                   = 'Overwrite Any Conflicting Files';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['overwriteExisting']['overwriteDescription']                                                = 'If there are any files already located in the installation destination folder that conflict with the new ones, check this option to overwrite them.';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['autoup']['autoupDescription']                                                              = 'If enabled, the application installation will be automatically updated to the latest version when it is available.';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['admin_login']['admin_login']                                                               = 'Admin Login';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['admin_display_name']['admin_display_name']                                                 = 'Admin Display Name';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['admin_given_name']['admin_given_name']                                                     = 'Admin Given Name';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['admin_surname']['admin_surname']                                                           = 'Admin Surname';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['context_id']['context_id']                                                                 = 'Context ID';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['admin_tz_default']['admin_tz_default']                                                     = 'Default Time Zone';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['notify_email']['notify_email']                                                             = 'Notification Email';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['bugreport_email']['bugreport_email']                                                       = 'Bug Reports Email';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['site_title']['site_title']                                                                 = 'Website Title';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['php_path']['php_path']                                                                     = 'PHP Path';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['mogrify_path']['mogrify_path']                                                             = 'Mogrify Path';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['convert_path']['convert_path']                                                             = 'Convert Path';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['composite_path']['composite_path']                                                         = 'Composite Path';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['send_usage_statistics']['send_usage_statistics']                                           = 'Send Usage Statistics';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['check_for_updates']['check_for_updates']                                                   = 'Check For Updates';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['company_domain']['company_domain']                                                         = 'Company Domain';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['other_company_domain']['other_company_domain']                                             = 'Other Company Domain';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['user_limit']['user_limit']                                                                 = 'User Limit';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['blog_name']['blog_name']                                                                   = 'Blog Name';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['admin_first_name']['admin_first_name']                                                     = 'Admin First Name';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['admin_last_name']['admin_last_name']                                                       = 'Admin Last Name';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['organization_name']['organization_name']                                                   = 'Organization Name';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['admin_forwarding_number']['admin_forwarding_number']                                       = 'Admin Forwarding Number';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['admin_ext_number']['admin_ext_number']                                                     = 'Admin Extension Number';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['admin_login']['admin_login']                                                               = 'Admin Login';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['site_id']['site_id']                                                                       = 'State ID';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['admin_last_name']['admin_last_name']                                                       = 'Admin Last Name';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['firstname']['firstname']                                                                   = 'First Name';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['lastname']['lastname']                                                                     = 'Last Name';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['admin_firstname']['admin_firstname']                                                       = 'Admin First Name';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['admin_lastname']['admin_lastname']                                                         = 'Admin Last Name';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['admin_nick']['admin_nick']                                                                 = 'Admin Nick';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['admin_domain']['admin_domain']                                                             = 'Admin Domain';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['admin_locale']['admin_locale']                                                             = 'Admin Locale';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['default_ctrl']['default_ctrl']                                                             = 'Default Controler';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['allow_cross_posting']['allow_cross_posting']                                               = 'Allow Cross Posting';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['default_post_status']['default_post_status']                                               = 'Default Post Status';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['use_preview']['use_preview']                                                               = 'Preview';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['use_post_url']['use_post_url']                                                             = 'Use Post URL';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['report_abuse']['report_abuse']                                                             = 'Use Report Abuse';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['app_name']['app_name']                                                                     = 'Application Name';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['app_shortname']['app_shortname']                                                           = 'Application Shortname';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['use_balanceTags']['use_balanceTags']                                                       = 'Use Balance Tags';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['use_xhtmlvalidation_for_comments']['use_xhtmlvalidation_for_comments']                     = 'Use XHTML Validation For Comments';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['use_strict']['use_strict']                                                                 = 'Use XHTML Strict';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['comments_use_autobr']['comments_use_autobr']                                               = 'Comments Use Auto BR';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['comments_allow_css_tweaks']['comments_allow_css_tweaks']                                   = 'Comments Allow CSS Tweaks';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['debug']['debug']                                                                           = 'Debug';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['log_app_errors']['log_app_errors']                                                         = 'Log Application Errors';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['demo_mode']['demo_mode']                                                                   = 'Demo Mode';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['require_name_email']['require_name_email']                                                 = 'Require Name And Email';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['minimum_comment_interval']['minimum_comment_interval']                                     = 'Minimum Comment Interval ';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['antispam_on_message_form']['antispam_on_message_form']                                     = 'Antispam On Message Form';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['timeout_online_user']['timeout_online_user']                                               = 'Timeout Online User';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['public_access_to_media']['public_access_to_media']                                         = 'Public Access To Media';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['show_errors']['show_errors']                                                               = 'Show MySQL Errors';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['halt_on_error']['halt_on_error']                                                           = 'Halt On MySQL Errors';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['cookie_expires']['cookie_expires']                                                         = 'Cookie Expires';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['cookie_expired']['cookie_expired']                                                         = 'Cookie Expired';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['upload_maxmaxkb']['upload_maxmaxkb']                                                       = 'Upload max KB';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['maintenance_mode']['maintenance_mode']                                                     = 'Maintenance Mode';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['admin_p_name']['admin_p_name']                                                             = 'Admin Name';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['url']['url']                                                                               = 'URL';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['domain_admin_login']['domain_admin_login']                                                 = 'Domain Admin Login';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['domain_admin_password']['domain_admin_password']                                           = 'Domain Admin Password';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['timezone']['timezone']                                                                     = 'Time Zone';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['postmaster_name']['postmaster_name']                                                       = 'Postmaster Name';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['imap_folder_name_draft']['imap_folder_name_draft']                                         = 'IMAP Folder Name Draft';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['imap_folder_name_outbox']['imap_folder_name_outbox']                                       = 'IMAP Folder Name Outbox';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['imap_folder_name_trash']['imap_folder_name_trash']                                         = 'IMAP Folder Name Trash';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['imap_folder_name_spam']['imap_folder_name_spam']                                           = 'IMAP Folder Name Spam';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['imap_host']['imap_host']                                                                   = 'IMAP Host';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['imap_host_port']['imap_host_port']                                                         = 'IMAP Host Port';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['imap_login_long']['imap_login_long']                                                       = 'IMAP Login Long';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['smtp_host']['smtp_host']                                                                   = 'SMTP Host';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['smtp_host_port']['smtp_host_port']                                                         = 'SMTP Host Port';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['smtp_host_port']['smtp_host_port']                                                         = 'SMTP Host Port';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['install_sample_data']['install_sample_data']                                               = 'Install Sample Data';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['full_site_name']['full_site_name']                                                         = 'Full Site Name';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['summary']['summary']                                                                       = 'Summary';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['site_title']['site_title']                                                                 = 'Site Title';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['aDMIN_NAME']['ADMIN_NAME']                                                                 = 'Admin Name';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['aDMIN_PASSWORD']['ADMIN_PASSWORD']                                                         = 'Admin Password';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['tITLE']['TITLE']                                                                           = 'Title';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['lOCALE']['LOCALE']                                                                         = 'Locale';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['companyName']['CompanyName']                                                               = 'Company Name';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['adminLogin']['AdminLogin']                                                                 = 'Admin Login';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['adminPassword']['AdminPassword']                                                           = 'Admin Password';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['fullName']['FullName']                                                                     = 'Full Name';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['maxUsers']['MaxUsers']                                                                     = 'Max Users';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['customer_login']['customer_login']                                                         = 'Customer Login';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['customer_password']['customer_password']                                                   = 'Customer Password';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['organization_name']['organization_name']                                                   = 'Organization Name';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['customer_email']['customer_email']                                                         = 'Customer Email';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['sHOP_EMAIL']['SHOP_EMAIL']                                                                 = 'Shop Email';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['sHOP_NAME']['SHOP_NAME']                                                                   = 'Shop Name';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['admin_p_name']['admin_p_name']                                                             = 'Admin Name';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['site_url']['site_url']                                                                     = 'Site URL';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['adminUsername']['adminUsername']                                                           = 'Admin Username';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['organizationName']['organizationName']                                                     = 'Organization Name';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['adminFirstName']['adminFirstName']                                                         = 'Admin First Name';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['adminLastName']['adminLastName']                                                           = 'Admin Last Name';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['adminEmail']['adminEmail']                                                                 = 'Admin Email';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['licenseSeatsRequested']['licenseSeatsRequested']                                           = 'License Seats Requested';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['mobolizedSiteUrl']['mobolizedSiteUrl']                                                     = 'Mobolized Site URL';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['gateway_username']['gateway_username']                                                     = 'Gateway Username';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['gateway_password']['gateway_password']                                                     = 'Gateway Password';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['customer_name']['customer_name']                                                           = 'Customer Name';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['customer_license']['customer_license']                                                     = 'Customer License';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['customer_id']['customer_id']                                                               = 'Customer ID';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['admin_firstname']['admin_firstname']                                                       = 'Admin First Name';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['admin_lastname']['admin_lastname']                                                         = 'Admin Last Name';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['blog_title']['blog_title']                                                                 = 'Blog Title';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['admin_user_name']['admin_user_name']                                                       = 'Admin User Name';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['name']['name']                                                                             = 'Name';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['demodata']['demodata']                                                                     = 'Demo Data';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['user_name']['user_name']                                                                   = 'User Name';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['user_password']['user_password']                                                           = 'User Password';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['base_dir']['base_dir']                                                                     = 'Base Directory';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['icon_path']['icon_path']                                                                   = 'Icon Path';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['template']['template']                                                                     = 'Template';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['show_dir_size']['show_dir_size']                                                           = 'Show Directory Size';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['search_enabled']['search_enabled']                                                         = 'Search Enabled';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['use_login_system']['use_login_system']                                                     = 'Use Login System';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['must_login_to_download']['must_login_to_download']                                         = 'Must Login To Download';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['days_new']['days_new']                                                                     = 'Days New';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['thumbnail_height']['thumbnail_height']                                                     = 'Thumbnail Limit';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['bandwidth_limit']['bandwidth_limit']                                                       = 'Bandwidth Limit';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['anti_leech']['anti_leech']                                                                 = 'Anti Leech';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['archive']['archive']                                                                       = 'Archive';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['parse_htaccess']['parse_htaccess']                                                         = 'Parse .htaccess';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['md5_show']['md5_show']                                                                     = 'MD5 Show';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['storename']['storename']                                                                   = 'Store Name';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['storeemail']['storeemail']                                                                 = 'Store Email';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['storelogin']['storelogin']                                                                 = 'Store Login';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['storepass']['storepass']                                                                   = 'Store Pass';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['mail_backend']['mail_backend']                                                             = 'Mail Backend';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['mail_host']['mail_host']                                                                   = 'Mail Host';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['mail_port']['mail_port']                                                                   = 'Mail Port';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['mail_user']['mail_user']                                                                   = 'Mail User';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['mail_password']['mail_password']                                                           = 'Mail Password';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['admin_allow_hosts']['admin_allow_hosts']                                                   = 'Admin Allow Hosts';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['domain_name']['domain_name']                                                               = 'Domain Name';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['admin']['admin']                                                                           = 'Admin';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['default_sender_name']['default_sender_name']                                               = 'Default Sender Name';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['install_sample_data']['install_sample_data']                                               = 'Install Sample Data';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['timezone']['timezone']                                                                     = 'Time Zone';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['currency']['currency']                                                                     = 'Currency';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['admin_first_name']['admin_first_name']                                                     = 'Admin First Name';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['skip_demo_data']['skip_demo_data']                                                         = 'Skip Demo Data';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['customer_manager_login']['customer_manager_login']                                         = 'Customer Manager Login';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['customer_manager_password']['customer_manager_password']                                   = 'Customer Manager Password';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['customer_manager_email']['customer_manager_email']                                         = 'Customer Manager Email';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['admin_display_name']['admin_display_name']                                                 = 'Admin Display Name';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['admin_given_name']['admin_given_name']                                                     = 'Admin Given Name';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['admin_surname']['admin_surname']                                                           = 'Admin Surname';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['information_text']['information_text']                                                     = 'Information Text';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['context_name']['context_name']                                                             = 'Context Name';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['admin_language_default']['admin_language_default']                                         = 'Default Admin Language';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['admin_tz_default']['admin_tz_default']                                                     = 'Default Admin Time Zone';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['environment_name']['environment_name']                                                     = 'Environment Name';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['admin_email_address']['admin_email_address']                                               = 'Admin Email Address';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['first_name']['first_name']                                                                 = 'First Name';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['middle_name']['middle_name']                                                               = 'Middle Name';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['last_name']['last_name']                                                                   = 'Last Name';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['company']['company']                                                                       = 'Company';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['address_line1']['address_line1']                                                           = 'Address';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['address_line2']['address_line2']                                                           = 'Address';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['city']['city']                                                                             = 'City';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['province']['province']                                                                     = 'Province';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['usr_country']['usr_country']                                                               = 'User Country';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['zip']['zip']                                                                               = 'ZIP Code';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['phone1']['phone1']                                                                         = 'Phone';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['phone2']['phone2']                                                                         = 'Phone';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['fax']['fax']                                                                               = 'Fax';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['mail1']['mail1']                                                                           = 'Email';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['emailformat']['emailformat']                                                               = 'Email Format';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['username']['username']                                                                     = 'Username';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['password']['password']                                                                     = 'Password';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['errormsg']['errormsg']                                                                     = 'Error Message';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['contact_email']['contact_email']                                                           = 'Contact Email';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['pASSWORD']['PASSWORD']                                                                     = 'Password';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['domain_password']['domain_password']                                                       = 'Domain Password';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['cu_first_name']['cu_first_name']                                                           = 'Customer First Name';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['cu_last_name']['cu_last_name']                                                             = 'Customer Last Name';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['cu_company']['cu_company']                                                                 = 'Customer Company';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['cu_address_1']['cu_address_1']                                                             = 'Customer Address';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['cu_address_2']['cu_address_2']                                                             = 'Customer Address';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['cu_city']['cu_city']                                                                       = 'Customer City';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['cu_province']['cu_province']                                                               = 'Customer Province';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['cu_zipcode']['cu_zipcode']                                                                 = 'Customer ZIP Code';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['cu_country']['cu_country']                                                                 = 'Customer Country';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['cu_email']['cu_email']                                                                     = 'Customer Email';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['cu_phone']['cu_phone']                                                                     = 'Customer Phone';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['storage_addon']['storage_addon']                                                           = 'Storage Addon';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['mailbox_addon']['mailbox_addon']                                                           = 'Mailbox Addon';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['bandwidth_addon']['bandwidth_addon']                                                       = 'Bandwidth Addon';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['additional_email']['additional_email']                                                     = 'Additional Email';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['subscription_id']['subscription_id']                                                       = 'Subscription ID';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['plan_name']['plan_name']                                                                   = 'Plan Name';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['customer_name']['customer_name']                                                           = 'Customer Name';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['customer_street']['customer_street']                                                       = 'Customer Street';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['customer_city']['customer_city']                                                           = 'Customer City';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['customer_country']['customer_country']                                                     = 'Customer Country';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['customer_zipcode']['customer_zipcode']                                                     = 'Customer ZIP Code';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['admin_mobile_number']['admin_mobile_number']                                               = 'Admin Mobile Number';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['groupAccountName']['GroupAccountName']                                                     = 'Group Account Name';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['groupAccountPwd']['GroupAccountPwd']                                                       = 'Group Account Password';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['groupemail']['groupemail']                                                                 = 'Group Email';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['lastname']['lastname']                                                                     = 'Last Name';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['pack']['pack']                                                                             = 'Pack';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['mailprog']['mailprog']                                                                     = 'Mailer Program';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['lang']['lang']                                                                             = 'Language';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['system_email']['system_email']                                                             = 'System Email';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['admin']['admin']                                                                           = 'Admin';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['admin_fullname']['admin_fullname']                                                         = 'Admin Full Name';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['site_slogan']['site_slogan']                                                               = 'Site Slogan';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['admin_passwd']['admin_passwd']                                                             = 'Admin Password';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['setupadmin_login']['setupadmin_login']                                                     = 'Setup Admin Login';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['setupadmin_passwd']['setupadmin_passwd']                                                   = 'Setup Admin Password';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['headeradmin_login']['headeradmin_login']                                                   = 'Header Admin Login';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['headeradmin_passwd']['headeradmin_passwd']                                                 = 'Header Admin Password';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['mail_server']['mail_server']                                                               = 'Mail Server';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['mail_suffix']['mail_suffix']                                                               = 'Mail Suffix';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['smtp_server']['smtp_server']                                                               = 'SMTP Server';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['provisioned_domains']['provisioned_domains']                                               = 'Provisioned Domains';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['blog_descr']['blog_descr']                                                                 = 'Blog Description';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['aDMINEMAIL']['ADMINEMAIL']                                                                 = 'Admin Email';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['sHOPNAME']['SHOPNAME']                                                                     = 'Shop Name';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['aDMINPASSWORD']['ADMINPASSWORD']                                                           = 'Admin Password';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['cache_enable']['cache_enable']                                                             = 'Enable Cache';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['cache_lifetime']['cache_lifetime']                                                         = 'Cache Lifetime';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['cache_page_index']['cache_page_index']                                                     = 'Cache Page Index';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['cache_page_categories']['cache_page_categories']                                           = 'Cache Page Categories';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['cache_page_top']['cache_page_top']                                                         = 'Cache Page Top';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['cache_page_rss']['cache_page_rss']                                                         = 'Cache Page RSS';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['captcha_enable']['captcha_enable']                                                         = 'Enable Captcha';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['captcha_enable_comments']['captcha_enable_comments']                                       = 'Captcha Enable Comments';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['captcha_enable_upload']['captcha_enable_upload']                                           = 'Captcha Enable Upload';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['captcha_enable_registration']['captcha_enable_registration']                               = 'Captcha Enable Registration';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['captcha_enable_postcards']['captcha_enable_postcards']                                     = 'Captcha Enable Postcards';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['captcha_chars']['captcha_chars']                                                           = 'Captcha Chars';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['captcha_length']['captcha_length']                                                         = 'Captcha Length';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['captcha_wordfile']['captcha_wordfile']                                                     = 'Captcha Wordfile';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['captcha_width']['captcha_width']                                                           = 'Captcha Width';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['captcha_height']['captcha_height']                                                         = 'Captcha Height';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['captcha_text_color']['captcha_text_color']                                                 = 'Captcha Text Color';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['captcha_text_size']['captcha_text_size']                                                   = 'Captcha Text Size';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['captcha_text_transparency']['captcha_text_transparency']                                   = 'Captcha Text Transparency';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['captcha_filter_text']['captcha_filter_text']                                               = 'Captcha Filter Text';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['captcha_filter_bg']['captcha_filter_bg']                                                   = 'Captcha Filter Background';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['reseller_login']['reseller_login']                                                         = 'Reseller Login';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['reseller_password']['reseller_password']                                                   = 'Reseller Password';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['reseller_email']['reseller_email']                                                         = 'Reseller Email';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['default_skin']['default_skin']                                                             = 'Default Skin';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['check_authorization']['check_authorization']                                               = 'Check Authorization';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['error_reporting']['error_reporting']                                                       = 'Error Reporting';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['fix_png']['fix_png']                                                                       = 'Fix PNG Images';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['max_filesize']['max_filesize']                                                             = 'Max File Size';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['log_access']['log_access']                                                                 = 'Log Access';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['log_error']['log_error']                                                                   = 'Log Error';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['log_length_days']['log_length_days']                                                       = 'Log Length (Days)';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['check_consumption']['check_consumption']                                                   = 'Check Consumption';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['max_consumption_ipaddress_datatransfer']['max_consumption_ipaddress_datatransfer']         = 'Max Consumption IP Address Data Transfer';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['max_consumption_ftpserver_datatransfer']['max_consumption_ftpserver_datatransfer']         = 'Max Consumption FTP server Data Transfer';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['max_consumption_ipaddress_executiontime']['max_consumption_ipaddress_executiontime']       = 'Max Consumption IP Address Execution Time';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['max_consumption_ftpserver_executiontime']['max_consumption_ftpserver_executiontime']       = 'Max Consumption FTP server Execution Time';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['max_consumption_ipaddress_nr_of_ftpservers']['max_consumption_ipaddress_nr_of_ftpservers'] = 'Max Consumption IP Address Number Of FTP Servers';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['check_homedirectory']['check_homedirectory']                                               = 'Check Home Directory';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['functionuse_newdir']['functionuse_newdir']                                                 = 'Create New Directory';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['functionuse_newfile']['functionuse_newfile']                                               = 'Create New File';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['functionuse_upload']['functionuse_upload']                                                 = 'Upload';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['functionuse_jupload']['functionuse_jupload']                                               = 'Java Upload';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['functionuse_swfupload']['functionuse_swfupload']                                           = 'Flash Upload';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['functionuse_easyWebsite']['functionuse_easyWebsite']                                       = 'Easy Website';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['functionuse_bookmark']['functionuse_bookmark']                                             = 'Bookmark Page';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['functionuse_install']['functionuse_install']                                               = 'Install Functions';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['functionuse_advanced']['functionuse_advanced']                                             = 'Advanced Functions';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['functionuse_copy']['functionuse_copy']                                                     = 'Copy Directories And Files';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['functionuse_move']['functionuse_move']                                                     = 'Move Directories And Files';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['functionuse_delete']['functionuse_delete']                                                 = 'Delete Directories And Files';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['functionuse_rename']['functionuse_rename']                                                 = 'Rename';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['functionuse_chmod']['functionuse_chmod']                                                   = 'Chmod';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['functionuse_downloadzip']['functionuse_downloadzip']                                       = 'Zip-And-Download';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['functionuse_unzip']['functionuse_unzip']                                                   = 'Unzip';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['functionuse_zip']['functionuse_zip']                                                       = 'Zip-And-Save, Zip-And-Email';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['functionuse_calculatesize']['functionuse_calculatesize']                                   = 'Calculate Size';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['functionuse_findstring']['functionuse_findstring']                                         = 'Find String';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['functionuse_downloadfile']['functionuse_downloadfile']                                     = 'Download File';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['functionuse_view']['functionuse_view']                                                     = 'View File';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['functionuse_edit']['functionuse_edit']                                                     = 'Edit File';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['functionuse_update']['functionuse_update']                                                 = 'Update File';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['functionuse_open']['functionuse_open']                                                     = 'Open File';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['moderator_name']['moderator_name']                                                         = 'Moderator Name';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['moderator_password']['moderator_password']                                                 = 'Moderator Password';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['user_first_name']['user_first_name']                                                       = 'User First Name';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['user_last_name']['user_last_name']                                                         = 'User Last Name';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['work_phone']['work_phone']                                                                 = 'Work Phone';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['user_language']['user_language']                                                           = 'User Language';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['_userLoginId']['_userLoginId']                                                             = 'User Login ID';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['_userPassword']['_userPassword']                                                           = 'User Password';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['_administratorNameUji']['_administratorNameUji']                                           = 'Administrator Name Uji';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['_administratorNameNa']['_administratorNameNa']                                             = 'Administrator Name Na';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['_companyName']['_companyName']                                                             = 'Company Name';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['_companyNameKana']['_companyNameKana']                                                     = 'Company Name Kana';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['_representativeNameUji']['_representativeNameUji']                                         = 'Representative Name Uji';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['_representativeNameNa']['_representativeNameNa']                                           = 'Representative Name Na';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['_mailAddress']['_mailAddress']                                                             = 'Mail Address';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['_postNo']['_postNo']                                                                       = 'Post Number';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['_address']['_address']                                                                     = 'Address';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['_telNo']['_telNo']                                                                         = 'Phone Number';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['_faxNo']['_faxNo']                                                                         = 'Fax Number';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['_URL']['_URL']                                                                             = 'URL';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['_companyRemarks']['_companyRemarks']                                                       = 'Company Remarks';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['contractCapacity']['contractCapacity']                                                     = 'Contract Capacity';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['_companyLoginId']['_companyLoginId']                                                       = 'Company Login ID';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['_customerNo']['_customerNo']                                                               = 'Customer Number';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['_ordNo']['_ordNo']                                                                         = 'Order Number';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['mobile_domain']['mobile_domain']                                                           = 'Mobile Domain';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['mobile_domain_prefix']['mobile_domain_prefix']                                             = 'Mobile Domain Prefix';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['source_domain']['source_domain']                                                           = 'Source Domain';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['mail']['mail']                                                                             = 'Email';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['name']['name']                                                                             = 'Name';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['surname']['surname']                                                                       = 'Surname';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['patronymic']['patronymic']                                                                 = 'Patronymic';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['isMale']['isMale']                                                                         = 'Is Male';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['licenceCount']['licenceCount']                                                             = 'License Count';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['phone']['phone']                                                                           = 'Phone';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['document_type']['document_type']                                                           = 'Document Type';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['document_number']['document_number']                                                       = 'Document Number';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['username']['username']                                                                     = 'Username';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['business_phone']['business_phone']                                                         = 'Business Phone';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['zip_code']['zip_code']                                                                     = 'ZIP Code';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['business_name']['business_name']                                                           = 'Business Name';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['contact_name']['contact_name']                                                             = 'Contact Name';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['last_name']['last_name']                                                                   = 'Last Name';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['mural_plan1']['mural_plan1']                                                               = 'Mural Plan 1';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['mural_plan2']['mural_plan2']                                                               = 'Mural Plan 2';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['mural_plan3']['mural_plan3']                                                               = 'Mural Plan 3';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['mural_plan4']['mural_plan4']                                                               = 'Mural Plan 4';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['mural_plan5']['mural_plan5']                                                               = 'Mural Plan 5';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['mural_plan6']['mural_plan6']                                                               = 'Mural Plan 6';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['plan']['plan']                                                                             = 'Plan';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['customer_email']['customer_email']                                                         = 'Customer Email';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['customer_first_name']['customer_first_name']                                               = 'Customer First Name';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['customer_last_name']['customer_last_name']                                                 = 'Customer Last Name';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['customer_phone']['customer_phone']                                                         = 'Customer Phone';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['customer_username']['customer_username']                                                   = 'Customer Username';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['customer_password']['customer_password']                                                   = 'Customer Password';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['customer_emailCrawlComplete']['customer_emailCrawlComplete']                               = 'Crawl Complete';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['customer_emailOverview']['customer_emailOverview']                                         = 'Email Overview';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['customer_sendWelcomeEmail']['customer_sendWelcomeEmail']                                   = 'Send Welcome Email';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['mobile-domain-prefix']['mobile-domain-prefix']                                             = 'Mobile Domain Prefix';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['domain_name']['domain_name']                                                               = 'Domain Name';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['storage_name']['storage_name']                                                             = 'Storage Name';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['email2']['email2']                                                                         = 'Email 2';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['first_name']['first_name']                                                                 = 'First Name';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['last_name']['last_name']                                                                   = 'Last Name';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['user_email']['user_email']                                                                 = 'User Email';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['user_password']['user_password']                                                           = 'User Password';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['customer_state']['customer_state']                                                         = 'Customer State';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['customer_phone_work']['customer_phone_work']                                               = 'Customer Phone Work';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['customer_phone_mobile']['customer_phone_mobile']                                           = 'Customer Phone Mobile';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['customer_admin_name']['customer_admin_name']                                               = 'Customer Admin Name';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['customer_admin_email_address']['customer_admin_email_address']                             = 'Customer Admin Email Address';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['customer_application_status']['customer_application_status']                               = 'Customer Application Status';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['subdomain']['subdomain']                                                                   = 'Subdomain';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['plan']['plan']                                                                             = 'Plan';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['user_name']['user_name']                                                                   = 'User Name';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['orgname']['orgname']                                                                       = 'Organization Name';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['purse_length']['purse_length']                                                             = 'Purse Length';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['abbr_length']['abbr_length']                                                               = 'Abbreviation Length';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['user_display_name']['user_display_name']                                                   = 'User Display Name';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['street']['street']                                                                         = 'Street';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['state']['state']                                                                           = 'State';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['country']['country']                                                                       = 'Country';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['postal_code']['postal_code']                                                               = 'Postal Code';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['telephone_business1']['telephone_business1']                                               = 'Business Phone';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['domainEmail']['domainEmail']                                                               = 'Domain Email';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['domains']['domains']                                                                       = 'Domains';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['application_status']['application_status']                                                 = 'Application Status';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['realmName']['RealmName']                                                                   = 'Real Name';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['email']['Email']                                                                           = 'Email';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['aDMIN_LOGIN']['ADMIN_LOGIN']                                                               = 'Admin Login';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['adminPassword']['adminPassword']                                                           = 'Admin Password';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['default_sender_name']['default_sender_name']                                               = 'Default Sender Name';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['install_sample_data']['install_sample_data']                                               = 'Install Sample Data';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['timezone']['timezone']                                                                     = 'Time Zone';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['fIRSTNAME']['FIRSTNAME']                                                                   = 'First Name';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['lASTNAME']['LASTNAME']                                                                     = 'Last Name';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['userName']['UserName']                                                                     = 'Username';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['passWord']['PassWord']                                                                     = 'Password';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['firstName']['FirstName']                                                                   = 'First Name';
$_LANG['addonCA']['applications']['mainContainer']['applicationsNewInstallFormPage']['installSection']['lastName']['LastName']                                                                     = 'Last Name';


$_LANG['admanager']                                                                                                                                                                                = 'AD Management';
$_LANG['blogs']                                                                                                                                                                                    = 'Blogs';
$_LANG['calendars']                                                                                                                                                                                = 'Calendars';
$_LANG['cms']                                                                                                                                                                                      = 'CMS';
$_LANG['customersupport']                                                                                                                                                                          = 'Customer Support';
$_LANG['ecommerce']                                                                                                                                                                                = 'e-Commerce';
$_LANG['educational']                                                                                                                                                                              = 'Educational';
$_LANG['files']                                                                                                                                                                                     = 'Files';
$_LANG['forums']                                                                                                                                                                                    = 'Forums';
$_LANG['games']                                                                                                                                                                                     = 'Games';
$_LANG['guestbooks']                                                                                                                                                                                = 'Guest Books';
$_LANG['igalleries']                                                                                                                                                                                = 'Image Galleries';
$_LANG['libraries']                                                                                                                                                                                 = 'Libraries';
$_LANG['mail']                                                                                                                                                                                      = 'Mail';
$_LANG['music']                                                                                                                                                                                     = 'Music';
$_LANG['others']                                                                                                                                                                                    = 'Others';
$_LANG['polls']                                                                                                                                                                                     = 'Polls';
$_LANG['projectman']                                                                                                                                                                                = 'Project Management';
$_LANG['wikis']                                                                                                                                                                                     = 'Wikis';
$_LANG['dbtools'] = 'DB Tools';
$_LANG['erp'] = 'ERP';
$_LANG['frameworks'] = 'Frameworks';
$_LANG['galleries'] = 'Galleries';
$_LANG['microblogs'] = 'Micro Blogs';
$_LANG['rss'] = 'RSS';
$_LANG['socialnetworking'] = 'Social Networking';
$_LANG['video'] = 'Video';


//$_LANG['AD Management']                                                                                                 = 'AD Management';
//$_LANG['Blogs']                                                                                                         = 'Blogs';
//$_LANG['CMS']                                                                                                           = 'CMS';
//$_LANG['Calendars']                                                                                                     = 'Calendars';
//$_LANG['Customer Support']                                                                                              = 'Customer Support';
//$_LANG['DB Tools']                                                                                                      = 'DB Tools';
//$_LANG['ERP']                                                                                                           = 'ERP';
//$_LANG['Educational']                                                                                                   = 'Educational';
//$_LANG['Files']                                                                                                         = 'Files';
//$_LANG['Forums']                                                                                                        = 'Forums';
//$_LANG['Games']                                                                                                         = 'Games';
//$_LANG['Guest Books']                                                                                                   = 'Guest Books';
//$_LANG['Image Galleries']                                                                                               = 'Image Galleries';
//$_LANG['Libraries']                                                                                                     = 'Libraries';
//$_LANG['Mail']                                                                                                          = 'Mail';
//$_LANG['Micro Blogs']                                                                                                   = 'Micro Blogs';
//$_LANG['Music']                                                                                                         = 'Music';
//$_LANG['Others']                                                                                                        = 'Others';
//$_LANG['Polls']                                                                                                         = 'Polls';
//$_LANG['Project Management']                                                                                            = 'Project Management';
//$_LANG['RSS']                                                                                                           = 'RSS';
//$_LANG['Social Networking']                                                                                             = 'Social Networking';
//$_LANG['Wikis']                                                                                                         = 'Wikis';
//$_LANG['e-Commerce']                                                                                                    = 'e-Commerce';
//$_LANG['Dbtools']                                                                                                       = 'DB Tools';
//$_LANG['Erp']                                                                                                           = 'ERP';
//$_LANG['Frameworks']                                                                                                    = 'Frameworks';
//$_LANG['Microblogs']                                                                                                    = 'Micro Blogs';
//$_LANG['Rss']                                                                                                           = 'RSS';
//$_LANG['Socialnetworking']                                                                                              = 'Social Networking';
//$_LANG['Video']                                                                                                         = 'Video';

//$_LANG['addonCA']['applications']['aD_Management']['AD Management']                                                                                     = 'AD Management';
//$_LANG['addonCA']['applications']['image_Galleries']['Image Galleries']                                                                                 = 'Image Galleries';
//$_LANG['addonCA']['applications']['e-Commerce']['e-Commerce']                                                                                           = 'E-Commerce';
//$_LANG['addonCA']['applications']['cMS']['CMS']                                                                                                         = 'CMS';
//$_LANG['addonCA']['applications']['calendars']['Calendars']                                                                                             = 'Calendars';
//$_LANG['addonCA']['applications']['customer_Support']['Customer Support']                                                                               = 'Customer Support';
//$_LANG['addonCA']['applications']['dB_Tools']['DB Tools']                                                                                               = 'DB Tools';
//$_LANG['addonCA']['applications']['eRP']['ERP']                                                                                                         = 'ERP';
//$_LANG['addonCA']['applications']['wikis']['Wikis']                                                                                                     = 'Wikis';
//$_LANG['addonCA']['applications']['video']['Video']                                                                                                     = 'Video';
//$_LANG['addonCA']['applications']['social_Networking']['Social Networking']                                                                             = 'Social Networking';
//$_LANG['addonCA']['applications']['rSS']['RSS']                                                                                                         = 'RSS';
//$_LANG['addonCA']['applications']['project_Management']['Project Management']                                                                           = 'Project Management';
//$_LANG['addonCA']['applications']['polls']['Polls']                                                                                                     = 'Polls';
//$_LANG['addonCA']['applications']['others']['Others']                                                                                                   = 'Others';
//$_LANG['addonCA']['applications']['music']['Music']                                                                                                     = 'Music';
//$_LANG['addonCA']['applications']['micro_Blogs']['Micro Blogs']                                                                                         = 'Micro Blogs';
//$_LANG['addonCA']['applications']['mail']['Mail']                                                                                                       = 'Mail';
//$_LANG['addonCA']['applications']['libraries']['Libraries']                                                                                             = 'Libraries';
//$_LANG['addonCA']['applications']['blogs']['Blogs']                                                                                                     = 'Blogs';
//$_LANG['addonCA']['applications']['educational']['Educational']                                                                                         = 'Educational';
//$_LANG['addonCA']['applications']['guest_Books']['Guest Books']                                                                                         = 'Guest Books';
//$_LANG['addonCA']['applications']['games']['Games']                                                                                                     = 'Games';
//$_LANG['addonCA']['applications']['frameworks']['Frameworks']                                                                                           = 'Frameworks';
//$_LANG['addonCA']['applications']['forums']['Forums']                                                                                                   = 'Forums';
//$_LANG['addonCA']['applications']['files']['Files']                                                                                                     = 'Files';

$_LANG['addonCA']['applications']['admanager']['admanager']                                                             = 'AD MANAGEMNT';
$_LANG['addonCA']['applications']['games']['games']                                                                     = 'GAMES';
$_LANG['addonCA']['applications']['wikis']['wikis']                                                                     = 'WIKIS';
$_LANG['addonCA']['applications']['calendars']['calendars']                                                             = 'CALENDERS';
$_LANG['addonCA']['applications']['cms']['cms']                                                                         = 'CMS';
$_LANG['addonCA']['applications']['customersupport']['customersupport']                                                 = 'CUSTOMER SUPPORT';
$_LANG['addonCA']['applications']['dbtools']['dbtools']                                                                 = 'DB TOOLS';
$_LANG['addonCA']['applications']['ecommerce']['ecommerce']                                                             = 'E-COMMERCE';
$_LANG['addonCA']['applications']['educational']['educational']                                                         = 'EDUCATIONAL';
$_LANG['addonCA']['applications']['erp']['erp']                                                                         = 'ERP';
$_LANG['addonCA']['applications']['files']['files']                                                                     = 'FILES';
$_LANG['addonCA']['applications']['forums']['forums']                                                                   = 'FORUMS';
$_LANG['addonCA']['applications']['frameworks']['frameworks']                                                           = 'FRAMEWORKS';
$_LANG['addonCA']['applications']['galleries']['galleries']                                                             = 'GALLERIES';
$_LANG['addonCA']['applications']['blogs']['blogs']                                                                     = 'BLOGS';
$_LANG['addonCA']['applications']['guestbooks']['guestbooks']                                                           = 'GUEST BOOKS';
$_LANG['addonCA']['applications']['video']['video']                                                                     = 'VIDEO';
$_LANG['addonCA']['applications']['socialnetworking']['socialnetworking']                                               = 'SOCIAL NETWORKING';
$_LANG['addonCA']['applications']['rss']['rss']                                                                         = 'RSS';
$_LANG['addonCA']['applications']['projectman']['projectman']                                                           = 'PROJECT MANAGEMNET';
$_LANG['addonCA']['applications']['polls']['polls']                                                                     = 'POLLS';
$_LANG['addonCA']['applications']['others']['others']                                                                   = 'OTHERS';
$_LANG['addonCA']['applications']['music']['music']                                                                     = 'MUSIC';
$_LANG['addonCA']['applications']['microblogs']['microblogs']                                                           = 'MICRO BLOGS';
$_LANG['addonCA']['applications']['mail']['mail']                                                                       = 'MAIL';
$_LANG['addonCA']['applications']['libraries']['libraries']                                                             = 'LIBRARIES';


$_LANG['addonCA']['applications']['mainContainer']['applicationsPage']['applicationsNewPage']['appSection']['dbtools']['Dbtools']                                                                   = 'DB Tools';
$_LANG['addonCA']['applications']['mainContainer']['applicationsPage']['applicationsNewPage']['appSection']['erp']['Erp']                                                                           = 'ERP';
$_LANG['addonCA']['applications']['mainContainer']['applicationsPage']['applicationsNewPage']['appSection']['frameworks']['Frameworks']                                                             = 'Frameworks';
$_LANG['addonCA']['applications']['mainContainer']['applicationsPage']['applicationsNewPage']['appSection']['microblogs']['Microblogs']                                                             = 'Micro Blogs';
$_LANG['addonCA']['applications']['mainContainer']['applicationsPage']['applicationsNewPage']['appSection']['rss']['Rss']                                                                           = 'RSS';
$_LANG['addonCA']['applications']['mainContainer']['applicationsPage']['applicationsNewPage']['appSection']['socialnetworking']['Socialnetworking']                                                 = 'Social Networking';
$_LANG['addonCA']['applications']['mainContainer']['applicationsPage']['applicationsNewPage']['appSection']['video']['Video']                                                                       = 'Video';


$_LANG['Community Building'] = 'Community Building';
$_LANG['Content Management'] = 'Content Management';
$_LANG['ECommerce and Business'] = 'ECommerce and Business';
$_LANG['Miscellaneous'] = 'Miscellaneous';
$_LANG['Photos and Files'] = 'Photos and Files';
$_LANG['Surveys and Statistics'] = 'Surveys and Statistics';
$_LANG['addonCA']['applications']['eCommerce_and_Business']['ECommerce and Business'] = 'ECommerce and Business';



$_LANG['applicationHasBeenDeleted']   = 'The selected application has been deleted successfully';
$_LANG['applicationHasBeenInstalled'] = 'The application has been installed successfully';
$_LANG['backupHasBeenRestored']       = 'The selected backup has been restored successfully';


//---------------------------------------------------------- NodeJs ------------------------------------------------------------

$_LANG['addonCA']['nodeJs']['mainContainer']['nodeJs']['NodeJsPageDescription']                                                                     = 'You can enable Node.js support on the server and on a particular domain in order to run Node.js applications on a web site.';
$_LANG['addonCA']['nodeJs']['availableVersionsNodeJsModal']['modal']['availableVersionsNodeJsModal']                                                = 'Setting Node.js Versions';
$_LANG['addonCA']['nodeJs']['availableVersionsNodeJsModal']['baseAcceptButton']['title']                                                            = 'Save Changes';
$_LANG['addonCA']['nodeJs']['availableVersionsNodeJsModal']['baseCancelButton']['title']                                                            = 'Cancel';
$_LANG['addonCA']['nodeJs']['mainContainer']['nodeJs']['NodeJsPageTitle']                                                                           = 'Node.js';
$_LANG['addonCA']['nodeJs']['mainContainer']['nodeJsVersionsList']['table']['domain']                                                               = 'Domain';
$_LANG['addonCA']['nodeJs']['mainContainer']['nodeJsVersionsList']['table']['version']                                                              = 'Version';
$_LANG['addonCA']['nodeJs']['mainContainer']['nodeJsVersionsList']['availableVersionsNodeJsButton']['button']['availableVersionsNodeJsButton']      = 'Set Version';
$_LANG['addonCA']['nodeJs']['availableVersionsNodeJsModal']['availableVersionsNodeJsForm']['enabled']['enabled']                                    = 'Enabled';
$_LANG['addonCA']['nodeJs']['availableVersionsNodeJsModal']['availableVersionsNodeJsForm']['version']['version']                                    = 'Version';

//----------------------------------------------------- GIT Repositories ---------------------------------------------------

$_LANG['addonCA']['gitRepositories']['mainContainer']['gitRepositories']['GitRepositoriesPageDescription']                                   = 'You can manage Git repositories and automatically deploy web sites from such repositories to a target public directory.';
$_LANG['addonCA']['gitRepositories']['mainContainer']['gitRepositories']['GitRepositoriesPageTitle']                                         = 'Git Repositories';
$_LANG['addonCA']['gitRepositories']['mainContainer']['repositoriesTable']['addButton']['button']['addButton']                               = 'Create Repository';
$_LANG['addonCA']['gitRepositories']['deleteModal']['deleteForm']['confirmGitRepositoryDelete']                                              = 'Are you sure that you want to delete this repository?';
$_LANG['addonCA']['gitRepositories']['addModal']['modal']['addModal']                                                                        = 'Create Repository';
$_LANG['addonCA']['gitRepositories']['addModal']['addForm']['domain']['domain']                                                              = 'domain';
$_LANG['addonCA']['gitRepositories']['addModal']['addForm']['name']['name']                                                                  = 'Name';
$_LANG['addonCA']['gitRepositories']['addModal']['addForm']['domain']['domain']                                                              = 'Domain';
$_LANG['addonCA']['gitRepositories']['addModal']['addForm']['deployment-path']['deployment-path']                                            = 'Deployment Path';
$_LANG['addonCA']['gitRepositories']['addModal']['addForm']['deployment-mode']['deployment-mode']                                            = 'Deployment Mode';
$_LANG['addonCA']['gitRepositories']['addModal']['addForm']['actions']['actions']                                                            = 'Actions';
$_LANG['addonCA']['gitRepositories']['addModal']['baseAcceptButton']['title']                                                                = 'Create';
$_LANG['addonCA']['gitRepositories']['addModal']['baseCancelButton']['title']                                                                = 'Cancel';
$_LANG['addonCA']['gitRepositories']['mainContainer']['repositoriesTable']['editButton']['button']['Edit']                                   = 'Edit';
$_LANG['addonCA']['gitRepositories']['editModal']['editForm']['domain']['domain']                                                            = 'Domain';
$_LANG['addonCA']['gitRepositories']['editModal']['editForm']['name']['name']                                                                = 'Name';
$_LANG['addonCA']['gitRepositories']['editModal']['editForm']['url']['url']                                                                  = 'URL';
$_LANG['addonCA']['gitRepositories']['mainContainer']['repositoriesTable']['table']['deploymentpath']                                        = 'Deployment Path';
$_LANG['addonCA']['gitRepositories']['mainContainer']['repositoriesTable']['table']['deploymentmode']                                        = 'Deployment Mode';
$_LANG['addonCA']['gitRepositories']['mainContainer']['repositoriesTable']['table']['url']                                                   = 'URL';
$_LANG['addonCA']['gitRepositories']['editModal']['editForm']['newName']['newName']                                                          = 'Name';
$_LANG['addonCA']['gitRepositories']['editModal']['editForm']['deployment-path']['deployment-path']                                          = 'Deployment Path';
$_LANG['addonCA']['gitRepositories']['editModal']['editForm']['deployment-mode']['deployment-mode']                                          = 'Deployment Mode';
$_LANG['addonCA']['gitRepositories']['gitRepositories']['deployment-mode']['auto']                                                           = 'Auto';
$_LANG['addonCA']['gitRepositories']['gitRepositories']['deployment-mode']['manual']                                                         = 'Manual';
$_LANG['addonCA']['gitRepositories']['gitRepositories']['deployment-mode']['none']                                                           = 'None';
$_LANG['addonCA']['gitRepositories']['mainContainer']['domainsTable']['Auto']                                                                = 'Auto';
$_LANG['addonCA']['gitRepositories']['mainContainer']['domainsTable']['None']                                                                = 'None';
$_LANG['addonCA']['gitRepositories']['mainContainer']['domainsTable']['Manual']                                                              = 'Manual';
$_LANG['addonCA']['nodeJs']['mainContainer']['domainsTable']['None']                                                                         = 'None';
$_LANG['addonCA']['gitRepositories']['editModal']['editForm']['actions']['actions']                                                          = 'Actions';
$_LANG['addonCA']['gitRepositories']['editModal']['modal']['Edit Repository']                                                                = 'Edit Repository';
$_LANG['addonCA']['gitRepositories']['editModal']['baseAcceptButton']['title']                                                               = 'Save';
$_LANG['addonCA']['gitRepositories']['editModal']['baseCancelButton']['title']                                                               = 'Cancel';
$_LANG['addonCA']['gitRepositories']['mainContainer']['repositoriesTable']['deleteButton']['button']['Delete Repository']                    = 'Delete Repository';
$_LANG['addonCA']['gitRepositories']['deleteModal']['modal']['deleteModal']                                                                  = 'Delete Git Repository';
$_LANG['addonCA']['gitRepositories']['deleteModal']['baseAcceptButton']['title']                                                             = 'Confirm';
$_LANG['addonCA']['gitRepositories']['deleteModal']['baseCancelButton']['title']                                                             = 'Cancel';
$_LANG['addonCA']['gitRepositories']['mainContainer']['repositoriesTable']['deployButton']['button']['deployButton']                         = 'Deploy Repository';
$_LANG['addonCA']['gitRepositories']['deployModal']['baseAcceptButton']['title']                                                             = 'Confirm';
$_LANG['addonCA']['gitRepositories']['deployModal']['baseCancelButton']['title']                                                             = 'Cancel';
$_LANG['addonCA']['gitRepositories']['deployModal']['baseForm']['confirmGitRepositoryDeploy']                                                = 'Are you sure that you want to deploy this repository?';
$_LANG['addonCA']['gitRepositories']['deployModal']['modal']['Deploy Git Repository']                                                        = 'Deploy Git Repository';
$_LANG['addonCA']['gitRepositories']['mainContainer']['repositoriesTable']['table']['name']                                                  = 'Name';
$_LANG['addonCA']['gitRepositories']['mainContainer']['repositoriesTable']['table']['domain']                                                = 'Domain';
$_LANG['addonCA']['gitRepositories']['addModal']['addForm']['actions']['gitRepositoriesAction']                                              = 'Specify a list of additional actions which can be carried out each time the files are deployed to the website. Use shell commands delimited by “;” symbol that should be applied with an escape character: “>”.';
$_LANG['addonCA']['gitRepositories']['editModal']['editForm']['actions']['gitRepositoriesAction']                                            = 'Specify a list of additional actions which can be carried out each time the files are deployed to the website. Use shell commands delimited by “;” symbol that should be applied with an escape character: “>”.';
$_LANG['gitRepositoryHasBeenCreated']                                                                                                        = 'The git repository has been created successfully';
$_LANG['gitRepositoryEditSuccess']                                                                                                           = 'The selected repository has been updated successfully';
$_LANG['gitRepositoryHasBeenDeleted']                                                                                                        = 'The selected repository has been deleted successfully';
$_LANG['gitRepositoryHasBeenDeployed']                                                                                                       = 'The selected repository has been deployed successfully';
$_LANG['FormValidators']['invalidChars']                                                                                                     = 'No special characters can be used in this field';


// ---------------------------------------------------- FTP Access  ---------------------------------------------------------

$_LANG['addonCA']['ftp']['mainContainer']['ftp']['FtpPageTitle']                                                                             = 'FTP Access';
$_LANG['addonCA']['ftp']['mainContainer']['ftp']['FtpPageDescription']                                                                       = "Add and configure FTP accounts here. FTP allows you to manage the files that are associated with your website through an FTP client such as FileZilla.";
$_LANG['addonCA']['ftp']['mainContainer']['accountTable']['table']['id']                                                                     = 'Login';
$_LANG['addonCA']['ftp']['mainContainer']['accountTable']['table']['name']                                                                   = 'Name';
$_LANG['addonCA']['ftp']['mainContainer']['accountTable']['table']['home']                                                                   = 'Path';
$_LANG['addonCA']['ftp']['mainContainer']['accountTable']['deleteButton']['button']['deleteButton']                                          = 'Delete';
$_LANG['addonCA']['ftp']['mainContainer']['accountTable']['editButton']['button']['editButton']                                              = 'Edit Path';
$_LANG['addonCA']['ftp']['mainContainer']['accountTable']['addButton']['button']['addButton']                                                = 'Create Account';

$_LANG['addonCA']['ftp']['deleteModal']['modal']['deleteModal']                                                                              = 'Delete Account';
$_LANG['addonCA']['ftp']['deleteModal']['deleteForm']['confirmFtpDelete']                                                                    = 'Are you sure that you want to delete this account?';
$_LANG['addonCA']['ftp']['deleteModal']['baseAcceptButton']['title']                                                                         = 'Confirm';
$_LANG['addonCA']['ftp']['deleteModal']['baseCancelButton']['title']                                                                         = 'Cancel';

$_LANG['addonCA']['ftp']['addModal']['modal']['addModal']                                                                                    = 'Create Account';
$_LANG['addonCA']['ftp']['addModal']['addForm']['loginGroup']['loginGroup']                                                                  = 'Account Name';
$_LANG['addonCA']['ftp']['addModal']['addForm']['passwordSection']['password']['password']                                                   = 'Password';
$_LANG['addonCA']['ftp']['addModal']['addForm']['directorySection']['directorySection']                                                      = 'Directory';
$_LANG['addonCA']['ftp']['addModal']['addForm']['quotaUnlimited']['quotaUnlimited']                                                          = 'Unlimited Quota';
$_LANG['addonCA']['ftp']['addModal']['addForm']['quota']['quota']                                                                            = 'Quota (MB)';
$_LANG['addonCA']['ftp']['addModal']['addForm']['passwordSection']['password']['passDesc']                                                   = 'Enter a strong password with lowercase and uppercase letters, numbers and symbols or, alternatively, generate a random password. The color below the field indicates the current level of password security.';
$_LANG['addonCA']['ftp']['addModal']['addForm']['passwordSection']['password']['generatePasswordButton']['button']['generatePasswordButton'] = 'Generate';
$_LANG['addonCA']['ftp']['addModal']['baseAcceptButton']['title']                                                                            = 'Create';
$_LANG['addonCA']['ftp']['addModal']['baseCancelButton']['title']                                                                            = 'Cancel';

$_LANG['addonCA']['ftp']['mainContainer']['accountTable']['changePasswordButton']['button']['changePasswordButton']                             = 'Change Password';
$_LANG['addonCA']['ftp']['changePasswordModal']['changePasswordForm']['login']['login']                                                         = 'Account Name';
$_LANG['addonCA']['ftp']['changePasswordModal']['modal']['changePasswordModal']                                                                 = 'Change Password';
$_LANG['addonCA']['ftp']['changePasswordModal']['changePasswordForm']['password']['password']                                                   = 'Password';
$_LANG['addonCA']['ftp']['changePasswordModal']['changePasswordForm']['password']['passDesc']                                                   = 'Enter a strong password with lowercase and uppercase letters, numbers and symbols or, alternatively, generate a random password. The color below the field indicates the current level of password security.';
$_LANG['addonCA']['ftp']['changePasswordModal']['changePasswordForm']['password']['generatePasswordButton']['button']['generatePasswordButton'] = 'Generate';
$_LANG['addonCA']['ftp']['changePasswordModal']['baseAcceptButton']['title']                                                                    = 'Save';
$_LANG['addonCA']['ftp']['changePasswordModal']['baseCancelButton']['title']                                                                    = 'Cancel';

$_LANG['addonCA']['ftp']['editModal']['modal']['editModal']                                                             = 'Change Directory';
$_LANG['addonCA']['ftp']['editModal']['editForm']['login']['login']                                                     = 'Account Name';
$_LANG['addonCA']['ftp']['editModal']['editForm']['directorySection']['directorySection']                               = 'Directory';
$_LANG['addonCA']['ftp']['editModal']['editForm']['directorySection']['directoryDesc']                                  = 'The new home directory will be created if one does not exist.';
$_LANG['addonCA']['ftp']['editModal']['baseAcceptButton']['title']                                                      = 'Save';
$_LANG['addonCA']['ftp']['editModal']['baseCancelButton']['title']                                                      = 'Cancel';

$_LANG['addonCA']['ftp']['massDeleteModal']['modal']['massDeleteModal']                                                 = 'Delete Accounts';
$_LANG['addonCA']['ftp']['massDeleteModal']['deleteForm']['confirmMassFtpDelete']                                       = 'Are you sure that you want to delete the selected accounts?';
$_LANG['addonCA']['ftp']['massDeleteModal']['baseAcceptButton']['title']                                                = 'Confirm';
$_LANG['addonCA']['ftp']['massDeleteModal']['baseCancelButton']['title']                                                = 'Cancel';

$_LANG['ftpAccountUpdated']                                                 = 'The selected account has been updated successfully';
$_LANG['ftpAccountHasBeenDeleted']                                          = 'The selected account has been deleted successfully';
$_LANG['ftpAccountHasBeenCreated']                                          = 'The account has been created successfully';
$_LANG['ftpAccountsHaveBeenDeleted']                                        = 'The selected accounts have been deleted successfully';
$_LANG['pleaseProvideQuota']                                                = 'Please provide the correct value of quota';



// ---------------------------------------------------- Emails  ---------------------------------------------------------


$_LANG['addonCA']['emails']['mainContainer']['emails']['EmailsPageTitle']                                                                       = 'Email Addresses';
$_LANG['addonCA']['emails']['mainContainer']['emails']['EmailsPageDescription']                                                                 = 'Manage email accounts associated with your domains here. Create new email accounts and handle the existing ones.';
$_LANG['addonCA']['emails']['mainContainer']['accountTable']['table']['name']                                                                   = 'Account';
$_LANG['addonCA']['emails']['mainContainer']['accountTable']['table']['quota']                                                                  = 'Quota';
$_LANG['addonCA']['emails']['mainContainer']['accountTable']['addButton']['button']['addButton']                                                = 'Create Email Account';
$_LANG['addonCA']['emails']['mainContainer']['accountTable']['deleteButton']['button']['deleteButton']                                          = 'Delete';
$_LANG['addonCA']['emails']['mainContainer']['accountTable']['changePasswordButton']['button']['changePasswordButton']                          = 'Change Password';
$_LANG['addonCA']['emails']['addModal']['modal']['addModal']                                                                                    = 'Create Email Account';
$_LANG['addonCA']['emails']['addModal']['addForm']['loginGroup']['loginGroup']                                                                  = 'Email';
$_LANG['addonCA']['emails']['addModal']['addForm']['passwordSection']['password']['password']                                                   = 'Password';
$_LANG['addonCA']['emails']['addModal']['addForm']['passwordSection']['password']['passDesc']                                                   = 'Enter a strong password with lowercase and uppercase letters, numbers and symbols or, alternatively, generate a random password. The color below the field indicates the current level of password security.';
$_LANG['addonCA']['emails']['addModal']['addForm']['passwordSection']['password']['generatePasswordButton']['button']['generatePasswordButton'] = 'Generate';
$_LANG['addonCA']['emails']['addModal']['addForm']['directory']['directory']                                                                    = 'Directory';
$_LANG['addonCA']['emails']['addModal']['addForm']['quota']['quota']                                                                            = 'Quota (MB)';
$_LANG['addonCA']['emails']['addModal']['addForm']['quotaUnlimited']['quotaUnlimited']                                                          = 'Default Quota';
$_LANG['addonCA']['emails']['addModal']['addForm']['pleskAccess']['pleskAccess']                                                                = 'Use As Login To Plesk';
$_LANG['addonCA']['emails']['addModal']['addForm']['description']['description']                                                                = 'Description';
$_LANG['addonCA']['emails']['addModal']['addForm']['description']['descArea']                                                                   = 'The description is visible to everyone who has access to this email account.';
$_LANG['addonCA']['emails']['addModal']['baseAcceptButton']['title']                                                                            = 'Create';
$_LANG['addonCA']['emails']['addModal']['baseCancelButton']['title']                                                                            = 'Cancel';

$_LANG['addonCA']['emails']['deleteModal']['modal']['deleteModal']              = 'Delete Email Account';
$_LANG['addonCA']['emails']['deleteModal']['deleteForm']['confirmEmailsDelete'] = 'Are you sure that you want to delete this email account?';
$_LANG['addonCA']['emails']['deleteModal']['baseAcceptButton']['title']         = 'Confirm';
$_LANG['addonCA']['emails']['deleteModal']['baseCancelButton']['title']         = 'Cancel';


$_LANG['addonCA']['emails']['changePasswordModal']['modal']['changePasswordModal']                                                                 = 'Change Password';
$_LANG['addonCA']['emails']['changePasswordModal']['changePasswordForm']['nameAccount']['nameAccount']                                             = 'Account';
$_LANG['addonCA']['emails']['changePasswordModal']['changePasswordForm']['password']['password']                                                   = 'Password';
$_LANG['addonCA']['emails']['changePasswordModal']['changePasswordForm']['password']['passDesc']                                                   = 'Enter a strong password with lowercase and uppercase letters, numbers and symbols or, alternatively, generate a random password. The color below the field indicates the current level of password security.';
$_LANG['addonCA']['emails']['changePasswordModal']['changePasswordForm']['password']['generatePasswordButton']['button']['generatePasswordButton'] = 'Generate';
$_LANG['addonCA']['emails']['changePasswordModal']['baseAcceptButton']['title']                                                                    = 'Save';
$_LANG['addonCA']['emails']['changePasswordModal']['baseCancelButton']['title']                                                                    = 'Cancel';


$_LANG['emailAccountHasBeenCreated'] = 'The email account has been created successfully';
$_LANG['emailAccountHasBeenDeleted'] = 'The selected email account has been deleted successfully';
$_LANG['emailAccountHasBeenUpdated'] = 'The selected email account has been updated successfully';
$_LANG['quotaExceeded']              = 'The provided quota exceeds the set limits';


// ---------------------------------------------------- Email Forwarders  ---------------------------------------------------------

$_LANG['addonCA']['emailForwarders']['mainContainer']['emailForwarders']['EmailForwardersPageTitle']                    = 'Email Forwarders';
$_LANG['addonCA']['emailForwarders']['mainContainer']['emailForwarders']['EmailForwardersPageDescription']              = 'Send a copy of any incoming email from one address to another. For example, messages delivered to joe@example.com forward to joseph@example.com so that you have only one account to check.';
$_LANG['addonCA']['emailForwarders']['mainContainer']['emailForwardersTable']['table']['name']                          = 'Account';
$_LANG['addonCA']['emailForwarders']['mainContainer']['emailForwardersTable']['table']['destination']                   = 'Destination';
$_LANG['addonCA']['emailForwarders']['mainContainer']['emailForwardersTable']['addButton']['button']['addButton']       = 'Create Email Forwarder';
$_LANG['addonCA']['emailForwarders']['mainContainer']['emailForwardersTable']['deleteButton']['button']['deleteButton'] = 'Delete';

$_LANG['addonCA']['emailForwarders']['deleteModal']['modal']['deleteModal']                       = 'Delete Email Forwarder';
$_LANG['addonCA']['emailForwarders']['deleteModal']['deleteForm']['confirmEmailForwardersDelete'] = 'Are you sure that you want to delete this email forwarder?';
$_LANG['addonCA']['emailForwarders']['deleteModal']['baseAcceptButton']['title']                  = 'Confirm';
$_LANG['addonCA']['emailForwarders']['deleteModal']['baseCancelButton']['title']                  = 'Cancel';

$_LANG['addonCA']['emailForwarders']['addModal']['modal']['addModal']                     = 'Create Email Forwarder';
$_LANG['addonCA']['emailForwarders']['addModal']['addForm']['loginGroup']['loginGroup']   = 'Address To Forward';
$_LANG['addonCA']['emailForwarders']['addModal']['addForm']['destination']['destination'] = 'Destination';
$_LANG['addonCA']['emailForwarders']['addModal']['baseAcceptButton']['title']             = 'Create';
$_LANG['addonCA']['emailForwarders']['addModal']['baseCancelButton']['title']             = 'Cancel';


$_LANG['emailForwarderHasBeenDeleted'] = 'The selected email forwarder has been deleted successfully';
$_LANG['emailForwarderHasBeenCreated'] = 'The email forwarder has been created successfully';


// ---------------------------------------------------- Addon Domains  ---------------------------------------------------------

$_LANG['addonCA']['addonDomains']['mainContainer']['addonDomains']['AddonDomainsPageTitle']                  = 'Addon Domains';
$_LANG['addonCA']['addonDomains']['mainContainer']['addonDomains']['AddonDomainsPageDescription']            = "An addon domain is an additional domain that is stored as a subdomain of your main site. Use addon domains to host additional domains on your account without registering a new domain name.";
$_LANG['addonCA']['addonDomains']['mainContainer']['domainsTable']['table']['name']                          = 'Name';
$_LANG['addonCA']['addonDomains']['mainContainer']['domainsTable']['table']['ip']                            = 'IP Address';
$_LANG['addonCA']['addonDomains']['mainContainer']['domainsTable']['table']['createDate']                    = 'Creation Date';
$_LANG['addonCA']['addonDomains']['mainContainer']['domainsTable']['table']['diskUsage']                     = 'Disk Usage';
$_LANG['addonCA']['addonDomains']['mainContainer']['domainsTable']['table']['path']                          = 'Path';
$_LANG['addonCA']['addonDomains']['mainContainer']['domainsTable']['noHosting']                              = 'No Hosting';
$_LANG['addonCA']['addonDomains']['mainContainer']['domainsTable']['forwarding']                             = 'Forwarding';
$_LANG['addonCA']['addonDomains']['mainContainer']['domainsTable']['addButton']['button']['addButton']       = 'Create Addon Domain';
$_LANG['addonCA']['addonDomains']['mainContainer']['domainsTable']['editButton']['button']['editButton']     = 'Edit';
$_LANG['addonCA']['addonDomains']['mainContainer']['domainsTable']['deleteButton']['button']['deleteButton'] = 'Delete';

$_LANG['addonCA']['addonDomains']['deleteModal']['modal']['deleteModal']           = 'Delete Addon Domain';
$_LANG['addonCA']['addonDomains']['deleteModal']['deleteForm']['confirmFtpDelete'] = 'Are you sure that you want to delete this addon domain?';
$_LANG['addonCA']['addonDomains']['deleteModal']['baseAcceptButton']['title']      = 'Confirm';
$_LANG['addonCA']['addonDomains']['deleteModal']['baseCancelButton']['title']      = 'Cancel';

$_LANG['addonCA']['addonDomains']['massDeleteModal']['modal']['massDeleteModal']                   = 'Delete Addon Domains';
$_LANG['addonCA']['addonDomains']['massDeleteModal']['deleteForm']['confirmMassAddonDomainDelete'] = 'Are you sure that you want to delete the selected addon domains?';
$_LANG['addonCA']['addonDomains']['massDeleteModal']['baseAcceptButton']['title']                  = 'Confirm';
$_LANG['addonCA']['addonDomains']['massDeleteModal']['baseCancelButton']['title']                  = 'Cancel';

$_LANG['addonCA']['addonDomains']['create']['hostingNone'] = 'None';
$_LANG['addonCA']['addonDomains']['create']['forwarding']  = 'Forwarding';
$_LANG['addonCA']['addonDomains']['create']['hostingVRT']  = 'Website Hosting';

$_LANG['addonCA']['addonDomains']['create']['permanently'] = 'Moved permanently (code 301)';
$_LANG['addonCA']['addonDomains']['create']['temporarily'] = 'Moved temporarily (code 302)';
$_LANG['addonCA']['addonDomains']['create']['frame']       = 'Frame forwarding';

$_LANG['addonCA']['addonDomains']['status']['active']                                                                   = 'Active';
$_LANG['addonCA']['addonDomains']['status']['suspendByAdmin']                                                           = 'Suspended'; // by Administrator
$_LANG['addonCA']['addonDomains']['status']['disabledByAdmin']                                                          = 'Disabled'; // by the Administrator
$_LANG['addonCA']['addonDomains']['status']['disabledByReseller']                                                       = 'Disabled'; // by Reseller
$_LANG['addonCA']['addonDomains']['status']['disabledByCustomer']                                                       = 'Disabled'; // by a Customer

$_LANG['addonCA']['addonDomains']['turn-off-action']['active']                                                          = 'Active';
$_LANG['addonCA']['addonDomains']['turn-off-action']['suspended']                                                       = 'Suspended';
$_LANG['addonCA']['addonDomains']['turn-off-action']['disabled']                                                        = 'Disabled';

$_LANG['addonCA']['addonDomains']['addModal']['modal']['addModal']                                                      = 'Create Addon Domain';
$_LANG['addonCA']['addonDomains']['addModal']['generalTabTitle']                                                        = 'General';
$_LANG['addonCA']['addonDomains']['addModal']['additionalTabTitle']                                                     = 'Other';
$_LANG['addonCA']['addonDomains']['addForm']['generalTab']['basicFieldsSection']['status']['status']                    = 'Status';
$_LANG['addonCA']['addonDomains']['addModal']['generalTab']['basicFieldsSection']['domain']['domain']                   = 'Domain';
$_LANG['addonCA']['addonDomains']['addModal']['generalTab']['basicFieldsSection']['hostingType']['hostingType']         = 'Hosting Type';
$_LANG['addonCA']['addonDomains']['addModal']['generalTab']['pathGroup']['pathGroup']                                   = 'Document Root';
$_LANG['addonCA']['addonDomains']['addModal']['additionalTab']['additionalFields']['phpHandler']['phpHandler']          = 'PHP Handler';
$_LANG['addonCA']['addonDomains']['addModal']['baseAcceptButton']['title']                                              = 'Create';
$_LANG['addonCA']['addonDomains']['addModal']['baseCancelButton']['title']                                              = 'Cancel';

$_LANG['addonCA']['addonDomains']['addModal']['generalTab']['forwardingAddressSection']['destinationAddress']['destinationAddress'] = 'Destination Address';
$_LANG['addonCA']['addonDomains']['addModal']['generalTab']['forwardingTypeSection']['forwardingType']['forwardingType']            = 'Forwarding Type';

$_LANG['addonCA']['addonDomains']['editForm']['generalTab']['basicFieldsSection']['status']['status']                                = 'Status';
$_LANG['addonCA']['addonDomains']['editForm']['generalTab']['basicFieldsSection']['turn-off-action']['turn-off-action']              = 'Status';
$_LANG['addonCA']['addonDomains']['editModal']['modal']['editModal']                                                                 = 'Edit Addon Domain';
$_LANG['addonCA']['addonDomains']['editModal']['generalTab']['forwardingAddressSection']['destinationAddress']['destinationAddress'] = 'Destination Address';
$_LANG['addonCA']['addonDomains']['editModal']['generalTab']['forwardingTypeSection']['forwardingType']['forwardingType']            = 'Forwarding Type';
$_LANG['addonCA']['addonDomains']['editModal']['additionalTab']['additionalFields']['phpHandler']['phpHandler']                      = 'PHP Handler';
$_LANG['addonCA']['addonDomains']['editModal']['baseAcceptButton']['title']                                                          = 'Save';
$_LANG['addonCA']['addonDomains']['editModal']['baseCancelButton']['title']                                                          = 'Cancel';

$_LANG['provideDocumentRootPath']     = 'The document root field is required. Please provide a path.';
$_LANG['provideDestinationAddress']   = 'The destination address field is required. Please provide an address.';
$_LANG['addonDomainHasBeenDeleted']   = 'The selected addon domain has been deleted successfully';
$_LANG['addonDomainHasBeenUpdated']   = 'The selected addon domain has been updated successfully';
$_LANG['addonDomainHasBeenCreated']   = 'The addon domain has been created successfully';
$_LANG['addonDomainsHaveBeenDeleted'] = 'The selected addon domains have been deleted successfully';

// ---------------------------------------------------- Backups  ---------------------------------------------------------

$_LANG['addonCA']['backups']['mainContainer']['backups']['BackupsPageTitle']                                = 'Backups';
$_LANG['addonCA']['backups']['mainContainer']['backups']['BackupsPageDescription']                          = "Download a zipped copy of your entire site or a part of your site that you can save to your computer. When you backup your website, you have an extra copy of your information in case something happens to your host.";
$_LANG['addonCA']['backups']['mainContainer']['backupsTable']['table']['id']                                = 'Backup Name';
$_LANG['addonCA']['backups']['mainContainer']['backupsTable']['table']['creationDate']                      = 'Creation Date';
$_LANG['addonCA']['backups']['mainContainer']['backupsTable']['table']['size']                              = 'Size';
$_LANG['addonCA']['backups']['mainContainer']['backupsTable']['table']['description']                       = 'Description';
$_LANG['addonCA']['backups']['mainContainer']['backupsTable']['deleteButton']['button']['deleteButton']     = 'Delete';
$_LANG['addonCA']['backups']['mainContainer']['backupsTable']['downloadButton']['button']['downloadButton'] = 'Download';
$_LANG['addonCA']['backups']['mainContainer']['backupsTable']['addButton']['button']['addButton']           = 'Create Backup';
$_LANG['addonCA']['backups']['deleteModal']['modal']['deleteModal']                                         = 'Delete Backup';
$_LANG['addonCA']['backups']['deleteModal']['deleteForm']['confirmBackupDelete']                            = 'Are you sure that you want to delete this backup?';
$_LANG['addonCA']['backups']['deleteModal']['baseAcceptButton']['title']                                    = 'Confirm';
$_LANG['addonCA']['backups']['deleteModal']['baseCancelButton']['title']                                    = 'Cancel';

$_LANG['addonCA']['backups']['addModal']['modal']['addModal']                     = 'Create Backup';
$_LANG['addonCA']['backups']['addModal']['addForm']['prefix']['prefix']           = 'Prefix';
$_LANG['addonCA']['backups']['addModal']['addForm']['description']['description'] = 'Description';
$_LANG['addonCA']['backups']['addModal']['addForm']['splitSize']['splitSize']     = 'Split Size (KB)';
$_LANG['addonCA']['backups']['addModal']['addForm']['type']['type']               = 'Type';
$_LANG['addonCA']['backups']['addModal']['addForm']['content']['content']         = 'Content';
$_LANG['addonCA']['backups']['addModal']['typeSelect']['local']                   = 'Local';
$_LANG['addonCA']['backups']['addModal']['typeSelect']['remote']                  = 'Remote';
$_LANG['addonCA']['backups']['addModal']['contentSelect']['all']                  = 'All';
$_LANG['addonCA']['backups']['addModal']['contentSelect']['onlyConfiguration']    = 'Domain Configuration';
$_LANG['addonCA']['backups']['addModal']['contentSelect']['onlyMail']             = 'Mail Configuration and Content';
$_LANG['addonCA']['backups']['addModal']['contentSelect']['onlyHosting']          = 'User Files and Databases';
$_LANG['addonCA']['backups']['addModal']['baseAcceptButton']['title']             = 'Create';
$_LANG['addonCA']['backups']['addModal']['baseCancelButton']['title']             = 'Cancel';

$_LANG['addonCA']['backups']['massDeleteModal']['modal']['massDeleteModal']              = 'Delete Backups';
$_LANG['addonCA']['backups']['massDeleteModal']['deleteForm']['confirmMassBackupDelete'] = 'Are you sure that you want to delete the selected backups?';
$_LANG['addonCA']['backups']['massDeleteModal']['baseAcceptButton']['title']             = 'Confirm';
$_LANG['addonCA']['backups']['massDeleteModal']['baseCancelButton']['title']             = 'Cancel';

$_LANG['addonCA']['backups']['addModal']['backupCreateTime'] = 'The creation of a backup can take a while, hence it may not be available on the backup list immediately.';

$_LANG['addonCA']['backups']['mainContainer']['backupsTable']['buttonBase']['storageSettingsButton']['button']['storageSettingsButton'] = 'FTP Storage Settings';
$_LANG['addonCA']['backups']['storageSettingsModal']['modal']['storageSettingsModal']                                                   = 'FTP Storage Settings';
$_LANG['addonCA']['backups']['storageSettingsModal']['storageSettingsForm']['host']['host']                                             = 'FTP Server Hostname or IP';
$_LANG['addonCA']['backups']['storageSettingsModal']['storageSettingsForm']['directory']['directory']                                   = 'Directory For Backup Files Storage';
$_LANG['addonCA']['backups']['storageSettingsModal']['storageSettingsForm']['username']['username']                                     = 'FTP Username';
$_LANG['addonCA']['backups']['storageSettingsModal']['storageSettingsForm']['password']['password']                                     = 'FTP Password';
$_LANG['addonCA']['backups']['storageSettingsModal']['storageSettingsForm']['passiveMode']['passiveMode']                               = 'Passive Mode';
$_LANG['addonCA']['backups']['storageSettingsModal']['baseAcceptButton']['title']                                                       = 'Save';
$_LANG['addonCA']['backups']['storageSettingsModal']['baseCancelButton']['title']                                                       = 'Cancel';


$_LANG['backupHasBeenDeleted']         = 'The selected backup has been deleted successfully';
$_LANG['backupHasBeenCreated']         = 'The backup has been created successfully';
$_LANG['backupsHaveBeenDeleted']       = 'The selected backups have been deleted successfully';
$_LANG['storageSettingsHaveBeenSaved'] = 'The storage settings have been saved successfully';


// ---------------------------------------------------- Databases  ---------------------------------------------------------

$_LANG['addonCA']['databases']['mainContainer']['databases']['DatabasesPageTitle']                                                           = 'Databases';
$_LANG['addonCA']['databases']['mainContainer']['databases']['DatabasesPageDescription']                                                     = 'Manage large amounts of information over the web easily. MySQL databases are necessary to run many web-based applications, such as bulletin boards, content management systems, and online shopping carts.';
$_LANG['addonCA']['databases']['mainContainer']['databasesPage']['databasesTable']['table']['name']                                          = 'Database';
$_LANG['addonCA']['databases']['mainContainer']['databasesPage']['databasesTable']['addButton']['button']['addButton']                       = 'Create Database';
$_LANG['addonCA']['databases']['mainContainer']['databasesPage']['databasesTableTitle']                                                      = 'Databases';
$_LANG['addonCA']['databases']['mainContainer']['databasesPage']['usersTableTitle']                                                          = 'Users';
$_LANG['addonCA']['databases']['mainContainer']['databasesPage']['databasesTable']['deleteButton']['button']['deleteButton']                 = 'Delete';
$_LANG['addonCA']['databases']['mainContainer']['databasesPage']['databasesTable']['deleteDatabaseButton']['button']['deleteDatabaseButton'] = 'Delete';
$_LANG['addonCA']['databases']['mainContainer']['databasesPage']['usersTable']['addUserButton']['button']['addUserButton']                   = 'Create Database User';
$_LANG['addonCA']['databases']['mainContainer']['databasesPage']['usersTable']['updateUserButton']['button']['updateUserButton']             = 'Edit';

$_LANG['addonCA']['databases']['mainContainer']['databasesPage']['usersTable']['addButton']['button']['addButton']       = 'Create Database User';
$_LANG['addonCA']['databases']['mainContainer']['databasesPage']['usersTable']['table']['login']                         = 'Name';
$_LANG['addonCA']['databases']['mainContainer']['databasesPage']['usersTable']['table']['name']                          = 'Database';
$_LANG['addonCA']['databases']['mainContainer']['databasesPage']['usersTable']['deleteButton']['button']['deleteButton'] = 'Delete';

$_LANG['addonCA']['databases']['massDeleteModal']['modal']['massDeleteModal']        = 'Delete Users';
$_LANG['addonCA']['databases']['massDeleteModal']['deleteForm']['confirmMassDelete'] = 'Are you sure that you want to delete the selected users?';
$_LANG['addonCA']['databases']['massDeleteModal']['baseAcceptButton']['title']       = 'Confirm';
$_LANG['addonCA']['databases']['massDeleteModal']['baseCancelButton']['title']       = 'Cancel';

$_LANG['addonCA']['databases']['deleteModal']['modal']['deleteModal']            = 'Delete User';
$_LANG['addonCA']['databases']['deleteModal']['deleteForm']['confirmUserDelete'] = 'Are you sure that you want to delete this user?';
$_LANG['addonCA']['databases']['deleteModal']['baseAcceptButton']['title']       = 'Confirm';
$_LANG['addonCA']['databases']['deleteModal']['baseCancelButton']['title']       = 'Cancel';

$_LANG['addonCA']['databases']['addModal']['modal']['addModal']         = 'Create Database';
$_LANG['addonCA']['databases']['addModal']['addForm']['name']['name']   = 'Name';
$_LANG['addonCA']['databases']['addModal']['addForm']['type']['type']   = 'Type';
$_LANG['addonCA']['databases']['databasesType']['mysql']                = 'MySQL';
$_LANG['addonCA']['databases']['databasesType']['mssql']                = 'MSSQL';
$_LANG['addonCA']['databases']['databasesType']['postgresql']           = 'PostgreSQL';
$_LANG['addonCA']['databases']['addModal']['baseAcceptButton']['title'] = 'Create';
$_LANG['addonCA']['databases']['addModal']['baseCancelButton']['title'] = 'Cancel';

$_LANG['addonCA']['databases']['addModal']['addForm']['username']['username']                                                   = 'Username';
$_LANG['addonCA']['databases']['addModal']['addForm']['password']['password']                                                   = 'Password';
$_LANG['addonCA']['databases']['addModal']['addForm']['password']['passDesc']                                                   = 'Enter a strong password with lowercase and uppercase letters, numbers and symbols or, alternatively, generate a random password. The color below the field indicates the current level of password security.';
$_LANG['addonCA']['databases']['addModal']['addForm']['password']['generatePasswordButton']['button']['generatePasswordButton'] = 'Generate';
$_LANG['addonCA']['databases']['addModal']['addForm']['role']['role']                                                           = 'Role';

$_LANG['addonCA']['databases']['deleteDatabaseModal']['modal']['deleteDatabaseModal']                = 'Delete Database';
$_LANG['addonCA']['databases']['deleteDatabaseModal']['deleteDatabaseForm']['confirmDatabaseDelete'] = 'Are you sure that you want to delete this database?';
$_LANG['addonCA']['databases']['deleteDatabaseModal']['baseAcceptButton']['title']                   = 'Confirm';
$_LANG['addonCA']['databases']['deleteDatabaseModal']['baseCancelButton']['title']                   = 'Cancel';

$_LANG['addonCA']['databases']['massDeleteDatabaseModal']['modal']['massDeleteDatabaseModal']        = 'Delete Databases';
$_LANG['addonCA']['databases']['massDeleteDatabaseModal']['deleteDatabaseForm']['confirmMassDelete'] = 'Are you sure that you want to delete the selected databases?';
$_LANG['addonCA']['databases']['massDeleteDatabaseModal']['baseAcceptButton']['title']               = 'Confirm';
$_LANG['addonCA']['databases']['massDeleteDatabaseModal']['baseCancelButton']['title']               = 'Cancel';

$_LANG['addonCA']['databases']['addUserModal']['modal']['addUserModal']                 = 'Create Database User';
$_LANG['addonCA']['databases']['addUserModal']['addUserForm']['name']['name']           = 'Name';
$_LANG['addonCA']['databases']['addUserModal']['addUserForm']['databases']['databases'] = 'Database';
$_LANG['addonCA']['databases']['addUserModal']['baseAcceptButton']['title']             = 'Create';
$_LANG['addonCA']['databases']['addUserModal']['baseCancelButton']['title']             = 'Cancel';

$_LANG['addonCA']['databases']['addUserModal']['addUserForm']['password']['password']                                                   = 'Password';
$_LANG['addonCA']['databases']['addUserModal']['addUserForm']['password']['passDesc']                                                   = 'Enter a strong password with lowercase and uppercase letters, numbers and symbols or, alternatively, generate a random password. The color below the field indicates the current level of password security.';
$_LANG['addonCA']['databases']['addUserModal']['addUserForm']['password']['generatePasswordButton']['button']['generatePasswordButton'] = 'Generate';



$_LANG['addonCA']['databases']['updateUserModal']['modal']['updateUserModal']                                                                 = 'Edit Database User';
$_LANG['addonCA']['databases']['updateUserModal']['updateUserForm']['userName']['userName']                                                   = 'User';
$_LANG['addonCA']['databases']['updateUserModal']['updateUserForm']['password']['password']                                                   = 'New Password';
$_LANG['addonCA']['databases']['updateUserModal']['updateUserForm']['password']['passDesc']                                                   = 'Enter a strong password with lowercase and uppercase letters, numbers and symbols or, alternatively, generate a random password. The color below the field indicates the current level of password security.';
$_LANG['addonCA']['databases']['updateUserModal']['updateUserForm']['password']['generatePasswordButton']['button']['generatePasswordButton'] = 'Generate';
$_LANG['addonCA']['databases']['updateUserModal']['updateUserForm']['defaultUser']['defaultUser']                                             = "Use this user's credentials by default to access phpMyAdmin";
$_LANG['addonCA']['databases']['updateUserModal']['baseAcceptButton']['title']                                                                = 'Save';
$_LANG['addonCA']['databases']['updateUserModal']['baseCancelButton']['title']                                                                = 'Cancel';


$_LANG['addonCA']['databases']['databases']['roleType']['readWrite']          = 'Read and Write';
$_LANG['addonCA']['databases']['databases']['roleType']['readOnly']           = 'Read Only';
$_LANG['addonCA']['databases']['databases']['roleType']['writeOnly']          = 'Write Only';
$_LANG['addonCA']['databases']['addUserModal']['addUserForm']['role']['role'] = 'Role';



$_LANG['databaseHasBeenCreated']        = 'The database has been created successfully';
$_LANG['databaseHasBeenDeleted']        = 'The selected database has been deleted successfully';
$_LANG['databasesHaveBeenDeleted']      = 'The selected databases have been deleted successfully';
$_LANG['databaseUserHasBeenCreated']    = 'The database user has been created successfully';
$_LANG['databaseUserHasBeenDeleted']    = 'The selected database user has been deleted successfully';
$_LANG['databasesUsersHaveBeenDeleted'] = 'The selected database users have been deleted successfully';
$_LANG['databasesUserHasBeenUpdated']   = 'The selected database user has been updated successfully';
$_LANG['passwordIsRequired']            = 'The password is required';

// ---------------------------------------------------- Domain Aliases  ---------------------------------------------------------

$_LANG['addonCA']['domainAliases']['mainContainer']['domainAliases']['DomainAliasesPageTitle']                = 'Domain Aliases';
$_LANG['addonCA']['domainAliases']['mainContainer']['domainAliases']['DomainAliasesPageDescription']          = 'Domain aliases make your website available from another domain name. For example, you can make www.example.net and www.example.org show content from www.example.com.';
$_LANG['addonCA']['domainAliases']['mainContainer']['domainsTable']['table']['name']                          = 'Domain';
$_LANG['addonCA']['domainAliases']['mainContainer']['domainsTable']['addButton']['button']['addButton']       = 'Create Domain Alias';
$_LANG['addonCA']['domainAliases']['mainContainer']['domainsTable']['deleteButton']['button']['deleteButton'] = 'Delete';
$_LANG['addonCA']['domainAliases']['mainContainer']['domainsTable']['editButton']['button']['editButton']     = 'Settings';
$_LANG['addonCA']['domainAliases']['mainContainer']['domainsTable']['renameButton']['button']['renameButton'] = 'Rename';
$_LANG['addonCA']['domainAliases']['deleteModal']['modal']['deleteModal']                                     = 'Delete Domain Alias';
$_LANG['addonCA']['domainAliases']['deleteModal']['deleteForm']['confirmAliasDelete']                         = 'Are you sure that you want to delete this domain alias?';
$_LANG['addonCA']['domainAliases']['deleteModal']['baseAcceptButton']['title']                                = 'Confirm';
$_LANG['addonCA']['domainAliases']['deleteModal']['baseCancelButton']['title']                                = 'Cancel';
$_LANG['addonCA']['domainAliases']['mainContainer']['domainsTable']['table']['destination']                   = 'Destination';


$_LANG['addonCA']['domainAliases']['massDeleteModal']['modal']['massDeleteModal']             = 'Delete Domain Aliases';
$_LANG['addonCA']['domainAliases']['massDeleteModal']['deleteForm']['confirmMassAliasDelete'] = 'Are you sure that you want to delete the selected domain aliases?';
$_LANG['addonCA']['domainAliases']['massDeleteModal']['baseAcceptButton']['title']            = 'Confirm';
$_LANG['addonCA']['domainAliases']['massDeleteModal']['baseCancelButton']['title']            = 'Cancel';


$_LANG['addonCA']['domainAliases']['addModal']['modal']['addModal']                     = 'Create Domain Alias';
$_LANG['addonCA']['domainAliases']['addModal']['addForm']['name']['name']               = 'Alias Name';
$_LANG['addonCA']['domainAliases']['addModal']['addForm']['domains']['domains']         = 'Redirect To';
$_LANG['addonCA']['domainAliases']['addModal']['addForm']['manageDns']['manageDns']     = 'Synchronize DNS Zone With Primary Domain';
$_LANG['addonCA']['domainAliases']['addModal']['addForm']['mailService']['mailService'] = 'Mail Service';
$_LANG['addonCA']['domainAliases']['addModal']['addForm']['webService']['webService']   = 'Web Service';
$_LANG['addonCA']['domainAliases']['addModal']['addForm']['tomcat']['tomcat']           = 'Java Application Service (Tomcat)';
$_LANG['addonCA']['domainAliases']['addModal']['baseAcceptButton']['title']             = 'Create';
$_LANG['addonCA']['domainAliases']['addModal']['baseCancelButton']['title']             = 'Cancel';

$_LANG['addonCA']['domainAliases']['editModal']['modal']['editModal']                     = 'Settings';
$_LANG['addonCA']['domainAliases']['editModal']['editForm']['webService']['webService']   = 'Web Service';
$_LANG['addonCA']['domainAliases']['editModal']['editForm']['mailService']['mailService'] = 'Mail Service';
$_LANG['addonCA']['domainAliases']['editModal']['editForm']['tomcat']['tomcat']           = 'Java Application Service (Tomcat)';
$_LANG['addonCA']['domainAliases']['editModal']['editForm']['manageDns']['manageDns']     = 'Synchronize DNS Zone With Primary Domain';
$_LANG['addonCA']['domainAliases']['editModal']['baseAcceptButton']['title']              = 'Save';
$_LANG['addonCA']['domainAliases']['editModal']['baseCancelButton']['title']              = 'Cancel';

$_LANG['addonCA']['domainAliases']['renameModal']['modal']['renameModal']             = 'Rename Domain Alias';
$_LANG['addonCA']['domainAliases']['renameModal']['renameForm']['name']['name']       = 'Name';
$_LANG['addonCA']['domainAliases']['renameModal']['renameForm']['newName']['newName'] = 'New Name';
$_LANG['addonCA']['domainAliases']['renameModal']['baseAcceptButton']['title']        = 'Save';
$_LANG['addonCA']['domainAliases']['renameModal']['baseCancelButton']['title']        = 'Cancel';

$_LANG['domainAliasHasBeenCreated']    = 'The domain alias has been created successfully';
$_LANG['domainAliasHasBeenRenamed']    = 'The selected domain alias has been renamed successfully';
$_LANG['domainAliasHasBeenUpdated']    = 'The selected domain alias has been updated successfully';
$_LANG['domainAliasHasBeenDeleted']    = 'The selected domain alias has been deleted successfully';
$_LANG['domainAliasesHaveBeenDeleted'] = 'The selected domain aliases have been deleted successfully';


// ---------------------------------------------------- DNS Settings  ---------------------------------------------------------

$_LANG['addonCA']['dnsSettings']['mainContainer']['dnsSettings']['DnsSettingsPageTitle']                  = 'DNS Settings';
$_LANG['addonCA']['dnsSettings']['mainContainer']['dnsSettings']['DnsSettingsPageDescription']            = 'You can add and manage DNS records here. DNS records are the data elements that define the structure and content of the domain name space. All DNS operations are ultimately formulated in terms of DNS records.';
$_LANG['addonCA']['dnsSettings']['mainContainer']['domainsTable']['table']['name']                        = 'Domain';
$_LANG['addonCA']['dnsSettings']['mainContainer']['domainsTable']['dnsRedirect']['button']['dnsRedirect'] = 'Edit';

$_LANG['addonCA']['dnsSettings']['mainContainer']['dnsTable']['table']['host']                          = 'Host';
$_LANG['addonCA']['dnsSettings']['mainContainer']['dnsTable']['table']['type']                          = 'Type';
$_LANG['addonCA']['dnsSettings']['mainContainer']['dnsTable']['table']['value']                         = 'Value';
$_LANG['addonCA']['dnsSettings']['mainContainer']['dnsTable']['editButton']['button']['editButton']     = 'Edit';
$_LANG['addonCA']['dnsSettings']['mainContainer']['dnsTable']['deleteButton']['button']['deleteButton'] = 'Delete';

$_LANG['addonCA']['dnsSettings']['mainContainer']['dnsTable']['dnsTableTitle']       = 'DNS Records - ';
$_LANG['addonCA']['dnsSettings']['deleteModal']['modal']['deleteModal']              = 'Delete Record';
$_LANG['addonCA']['dnsSettings']['deleteModal']['deleteForm']['confirmRecordDelete'] = 'Are you sure that you want to delete this record?';
$_LANG['addonCA']['dnsSettings']['deleteModal']['baseAcceptButton']['title']         = 'Confirm';
$_LANG['addonCA']['dnsSettings']['deleteModal']['baseCancelButton']['title']         = 'Cancel';

$_LANG['addonCA']['dnsSettings']['mainContainer']['dnsTable']['addButton']['button']['addButton'] = 'Create DNS Record';
$_LANG['addonCA']['dnsSettings']['addModal']['modal']['addModal']                                 = 'Create DNS Record';
$_LANG['addonCA']['dnsSettings']['addModal']['addForm']['selectSection']['type']['type']          = 'Type';
$_LANG['addonCA']['dnsSettings']['addModal']['addForm']['selectSection']['type']['typeDesc']      = 'A (Address)</br>AAAA (IPv6 address)</br>NS (Authoritative name server)</br></br>CNAME (Canonical name for a DNS alias)</br></br>MX (Mail Exchanger)</br>PTR (Domain name pointer)</br>TXT (Text string)</br>DS (Delegation signer)</br>SRV (Service)';
$_LANG['addonCA']['dnsSettings']['addModal']['addForm']['domainGroup']['domainGroup']             = 'Host';
$_LANG['addonCA']['dnsSettings']['addModal']['addForm']['value']['value']                         = 'Value';
$_LANG['addonCA']['dnsSettings']['addModal']['addForm']['value']['valueDesc']                     = 'A - ex. 123.123.123.123</br></br>SRV - ex. SIP</br></br>AAAA - ex. 2002:7b7b:7b7b::1</br></br>DS - ex. 60485 5 1 2BB183AF5F22588179A53B0A<br>98631FAD1A292118';
$_LANG['addonCA']['dnsSettings']['addModal']['baseAcceptButton']['title']                         = 'Create';
$_LANG['addonCA']['dnsSettings']['addModal']['baseCancelButton']['title']                         = 'Cancel';


$_LANG['addonCA']['dnsSettings']['editModal']['modal']['editModal']                     = 'Edit DNS Record';
$_LANG['addonCA']['dnsSettings']['editModal']['editForm']['domainGroup']['domainGroup'] = 'Host';
$_LANG['addonCA']['dnsSettings']['editModal']['editForm']['value']['value']             = 'Value';
$_LANG['addonCA']['dnsSettings']['editModal']['editForm']['value']['valueDesc']         = 'A - ex. 123.123.123.123</br></br>SRV - ex. SIP</br></br>AAAA - ex. 2002:7b7b:7b7b::1</br></br>DS - ex. 60485 5 1 2BB183AF5F22588179A53B0A<br>98631FAD1A292118';
$_LANG['addonCA']['dnsSettings']['editModal']['editForm']['typeHidden']['typeDesc']      = 'A (Address)</br>AAAA (IPv6 address)</br>NS (Authoritative name server)</br></br>CNAME (Canonical name for a DNS alias)</br></br>MX (Mail Exchanger)</br>PTR (Domain name pointer)</br>TXT (Text string)</br>DS (Delegation signer)</br>SRV (Service)';
$_LANG['addonCA']['dnsSettings']['editModal']['editForm']['typeHidden']['typeHidden']   = 'Type';
$_LANG['addonCA']['dnsSettings']['editModal']['baseAcceptButton']['title']              = 'Save';
$_LANG['addonCA']['dnsSettings']['editModal']['baseCancelButton']['title']              = 'Cancel';

$_LANG['addonCA']['dnsSettings']['massDeleteModal']['modal']['massDeleteModal']                   = 'Delete Records';
$_LANG['addonCA']['dnsSettings']['massDeleteModal']['massDeleteForm']['confirmMassRecordsDelete'] = 'Are you sure that you want to delete the selected records?';
$_LANG['addonCA']['dnsSettings']['massDeleteModal']['baseAcceptButton']['title']                  = 'Confirm';
$_LANG['addonCA']['dnsSettings']['massDeleteModal']['baseCancelButton']['title']                  = 'Cancel';

$_LANG['addonCA']['dnsSettings']['addModal']['addForm']['domain']['domain']     = 'Domain';
$_LANG['addonCA']['dnsSettings']['addModal']['addForm']['mask']['mask']         = 'Mask';
$_LANG['addonCA']['dnsSettings']['addModal']['addForm']['priority']['priority'] = 'Priority';
$_LANG['addonCA']['dnsSettings']['addModal']['addForm']['service']['service']   = 'Service';
$_LANG['addonCA']['dnsSettings']['addModal']['addForm']['protocol']['protocol'] = 'Protocol';
$_LANG['addonCA']['dnsSettings']['addModal']['addForm']['port']['port']         = 'Port';
$_LANG['addonCA']['dnsSettings']['addModal']['addForm']['weight']['weight']     = 'Weight';

$_LANG['addonCA']['dnsSettings']['editModal']['editForm']['domain']['domain']     = 'Domain';
$_LANG['addonCA']['dnsSettings']['editModal']['editForm']['mask']['mask']         = 'Mask';
$_LANG['addonCA']['dnsSettings']['editModal']['editForm']['service']['service']   = 'Service';
$_LANG['addonCA']['dnsSettings']['editModal']['editForm']['priority']['priority'] = 'Priority';
$_LANG['addonCA']['dnsSettings']['editModal']['editForm']['protocol']['protocol'] = 'Protocol';
$_LANG['addonCA']['dnsSettings']['editModal']['editForm']['port']['port']         = 'Port';
$_LANG['addonCA']['dnsSettings']['editModal']['editForm']['weight']['weight']     = 'Weight';

$_LANG['dnsRecordsHasBeenDeleted'] = 'The selected records have been deleted successfully';
$_LANG['dnsRecordHasBeenDeleted']  = 'The selected record has been deleted successfully';
$_LANG['dnsRecordHasBeenCreated']  = 'The record has been created successfully';
$_LANG['dnsRecordHasBeenUpdated']  = 'The selected record has been updated successfully';


// ---------------------------------------------------- Spam Filter  ---------------------------------------------------------


$_LANG['addonCA']['spamFilter']['mainContainer']['spamFilter']['SpamFilterPageTitle']                 = 'Spam Filter';
$_LANG['addonCA']['spamFilter']['mainContainer']['spamFilter']['SpamFilterPageDescription']           = 'Spam filtering service detects spam messages sent to the email address. You can adjust spam filter settings to delete spam messages, mark them as spam by changing their subject, or move them to a special Spam folder accessible by mail clients over IMAP.';
$_LANG['addonCA']['spamFilter']['mainContainer']['emailsTable']['table']['id']                        = 'Email';
$_LANG['addonCA']['spamFilter']['mainContainer']['emailsTable']['editButton']['button']['EditButton'] = 'Edit';

$_LANG['addonCA']['spamFilter']['editModal']['modal']['EditModal']                       = 'Edit Spam Filter Settings';
$_LANG['addonCA']['spamFilter']['editModal']['editForm']['switchFilter']['switchFilter'] = 'Enable Spam Filtering';
$_LANG['addonCA']['spamFilter']['editModal']['editForm']['action']['action']             = 'Action';
$_LANG['addonCA']['spamFilter']['editModal']['editForm']['spamMessage']['spamMessage']   = 'Spam Message';
$_LANG['addonCA']['spamFilter']['editModal']['editForm']['sensitivity']['sensitivity']   = 'Sensitivity';
$_LANG['addonCA']['spamFilter']['editModal']['editForm']['whiteList']['whiteList']       = 'White List';
$_LANG['addonCA']['spamFilter']['editModal']['editForm']['blackList']['blackList']       = 'Black List';
$_LANG['addonCA']['spamFilter']['editModal']['baseAcceptButton']['title']                = 'Save';
$_LANG['addonCA']['spamFilter']['editModal']['baseCancelButton']['title']                = 'Cancel';

$_LANG['addonCA']['spamFilter']['spamAction']['Delete'] = 'Delete all spam messages';
$_LANG['addonCA']['spamFilter']['spamAction']['Move']   = 'Move spam to the spam folder';
$_LANG['addonCA']['spamFilter']['spamAction']['Text']   = 'Mark messages as spam by adding the following text to the message subject';

$_LANG['addonCA']['spamFilter']['editModal']['editForm']['action']['actionDescription']           = 'Specify what to do with messages classified as spam.';
$_LANG['addonCA']['spamFilter']['editModal']['editForm']['spamMessage']['spamMessageDescription'] = 'Leave this field blank if you do not want to add any text. Type _SCORE_ if you want to include the score in the message subject.';
$_LANG['addonCA']['spamFilter']['editModal']['editForm']['sensitivity']['sensivityDescription']   = 'Enter the number of points a message must score to qualify as spam - the lower the number, the more messages are classified as spam. The value is set to 7 by default, but you can adjust the sensitivity to ensure customers do not receive spam, and the valid messages are not marked as spam by SpamAssassin at the same time.';
$_LANG['addonCA']['spamFilter']['editModal']['editForm']['whiteList']['whiteListDescription']     = 'Provide the email addresses that should not be checked by the spam filter, one address per line. You can use an asterisk (*) as a substitute for a number of letters, e.g. specifying *@spammers.net will block the entire email domain spammers.net. You can also use a question mark (?) as a substitute for a single letter, e.g. user?@spammers.net.';
$_LANG['addonCA']['spamFilter']['editModal']['editForm']['blackList']['blackListDescription']     = 'Provide the email addresses that should always be blocked, one address per line. You can use an asterisk (*) as a substitute for a number of letters, e.g. specifying *@spammers.net will block the entire email domain spammers.net. You can also use a question mark (?) as a substitute for a single letter, e.g. user?@spammers.net.';

$_LANG['emailDoesNotBelongToYou'] = 'The selected email does not belong to you';


// ---------------------------------------------------- Web Users  ---------------------------------------------------------

$_LANG['addonCA']['webUsers']['mainContainer']['webUsers']['WebUsersPageTitle']                        = 'Web Users';
$_LANG['addonCA']['webUsers']['mainContainer']['webUsers']['WebUsersPageDescription']                  = "View the list of users having personal web pages on your websites, and manage their accounts. Web user accounts are limited user accounts which cannot be used to log in to Plesk. They can only access a directory under one of your sites and host their sites there. Web Users' sites can own addresses like www.example.com/~username.";
$_LANG['addonCA']['webUsers']['mainContainer']['usersTable']['table']['login']                         = 'Name';
$_LANG['addonCA']['webUsers']['mainContainer']['usersTable']['table']['php']                           = 'PHP';
$_LANG['addonCA']['webUsers']['mainContainer']['usersTable']['table']['cgi']                           = 'Cgi';
$_LANG['addonCA']['webUsers']['mainContainer']['usersTable']['table']['fastcgi']                       = 'Fastcgi';
$_LANG['addonCA']['webUsers']['mainContainer']['usersTable']['addButton']['button']['addButton']       = 'Create Web User';
$_LANG['addonCA']['webUsers']['mainContainer']['usersTable']['editButton']['button']['editButton']     = 'Edit';
$_LANG['addonCA']['webUsers']['mainContainer']['usersTable']['deleteButton']['button']['deleteButton'] = 'Delete';

$_LANG['addonCA']['webUsers']['deleteModal']['modal']['deleteModal']                = 'Delete User';
$_LANG['addonCA']['webUsers']['deleteModal']['deleteForm']['confirmWebUsersDelete'] = 'Are you sure that you want to delete this web user?';
$_LANG['addonCA']['webUsers']['deleteModal']['baseAcceptButton']['title']           = 'Confirm';
$_LANG['addonCA']['webUsers']['deleteModal']['baseCancelButton']['title']           = 'Cancel';

$_LANG['addonCA']['webUsers']['editModal']['modal']['editModal']                                                                 = 'Edit Web User';
$_LANG['addonCA']['webUsers']['editModal']['editForm']['login']['login']                                                         = 'Login';
$_LANG['addonCA']['webUsers']['editModal']['editForm']['password']['password']                                                   = 'New Password';
$_LANG['addonCA']['webUsers']['editModal']['editForm']['password']['passDesc']                                                   = 'Enter a strong password with lowercase and uppercase letters, numbers and symbols or, alternatively, generate a random password. The color below the field indicates the current level of password security.';
$_LANG['addonCA']['webUsers']['editModal']['editForm']['password']['generatePasswordButton']['button']['generatePasswordButton'] = 'Generate';
$_LANG['addonCA']['webUsers']['editModal']['editForm']['php']['php']                                                             = 'PHP Support';
$_LANG['addonCA']['webUsers']['editModal']['editForm']['cgi']['cgi']                                                             = 'CGI Support';
$_LANG['addonCA']['webUsers']['editModal']['editForm']['fastcgi']['fastcgi']                                                     = 'FastCGI Support';
$_LANG['addonCA']['webUsers']['editModal']['baseAcceptButton']['title']                                                          = 'Save';
$_LANG['addonCA']['webUsers']['editModal']['baseCancelButton']['title']                                                          = 'Cancel';

$_LANG['addonCA']['webUsers']['addModal']['modal']['addModal']                                                                 = 'Create Web User';
$_LANG['addonCA']['webUsers']['addModal']['addForm']['login']['login']                                                         = 'Login';
$_LANG['addonCA']['webUsers']['addModal']['addForm']['password']['password']                                                   = 'Password';
$_LANG['addonCA']['webUsers']['addModal']['addForm']['password']['passDesc']                                                   = 'Enter a strong password with lowercase and uppercase letters, numbers and symbols or, alternatively, generate a random password. The color below the field indicates the current level of password security.';
$_LANG['addonCA']['webUsers']['addModal']['addForm']['password']['generatePasswordButton']['button']['generatePasswordButton'] = 'Generate';
$_LANG['addonCA']['webUsers']['addModal']['addForm']['php']['php']                                                             = 'PHP Support';
$_LANG['addonCA']['webUsers']['addModal']['addForm']['cgi']['cgi']                                                             = 'CGI Support';
$_LANG['addonCA']['webUsers']['addModal']['addForm']['fastcgi']['fastcgi']                                                     = 'FastCGI Support';
$_LANG['addonCA']['webUsers']['addModal']['baseAcceptButton']['title']                                                         = 'Create';
$_LANG['addonCA']['webUsers']['addModal']['baseCancelButton']['title']                                                         = 'Cancel';

$_LANG['webuserHasBeenCreated'] = 'The web user has been created successfully';
$_LANG['webuserHasBeenUpdated'] = 'The selected web user has been updated successfully';
$_LANG['webuserHasBeenDeleted'] = 'The selected web user has been deleted successfully';


// ---------------------------------------------------- SSl Certificates  ---------------------------------------------------------

$_LANG['addonCA']['ssl']['mainContainer']['ssl']['SslPageTitle']                                    = 'SSL Certificates';
$_LANG['addonCA']['ssl']['mainContainer']['ssl']['SslPageDescription']                              = 'The SSL/TLS Manager will allow you to generate SSL certificates, certificate signing requests, and private keys. All these elements are crucial for securing your website. SSL allows you to secure pages on your site so that all sensitive information such as logins, credit card numbers, etc. are sent in encrypted format instead of a plain text. It is important to secure your site’s login areas, shopping areas, and other pages where confidential information may be sent over the web.';
$_LANG['addonCA']['ssl']['mainContainer']['sslTable']['generateButton']['button']['generateButton'] = 'Generate Certificate';
$_LANG['addonCA']['ssl']['mainContainer']['sslTable']['uploadButton']['button']['uploadButton']     = 'Upload Certificate';

$_LANG['addonCA']['ssl']['generateModal']['modal']['generateModal']    = 'Generate Certificate';
$_LANG['addonCA']['ssl']['generateModal']['baseAcceptButton']['title'] = 'Create';
$_LANG['addonCA']['ssl']['generateModal']['baseCancelButton']['title'] = 'Cancel';

$_LANG['addonCA']['ssl']['mainContainer']['sslTable']['table']['id']                            = 'Name';
$_LANG['addonCA']['ssl']['mainContainer']['sslTable']['deleteButton']['button']['deleteButton'] = 'Delete';

$_LANG['addonCA']['ssl']['deleteModal']['modal']['deleteModal']                   = 'Delete Certificate';
$_LANG['addonCA']['ssl']['deleteModal']['deleteForm']['confirmDeleteCertificate'] = 'Are you sure that you want to delete this certificate?';
$_LANG['addonCA']['ssl']['deleteModal']['baseAcceptButton']['title']              = 'Confirm';
$_LANG['addonCA']['ssl']['deleteModal']['baseCancelButton']['title']              = 'Cancel';

$_LANG['addonCA']['ssl']['massDeleteModal']['modal']['massDeleteModal']                   = 'Delete Certificates';
$_LANG['addonCA']['ssl']['massDeleteModal']['deleteForm']['confirmMassDeleteCertificate'] = 'Are you sure that you want to delete the selected certificates?';
$_LANG['addonCA']['ssl']['massDeleteModal']['baseAcceptButton']['title']                  = 'Confirm';
$_LANG['addonCA']['ssl']['massDeleteModal']['baseCancelButton']['title']                  = 'Cancel';

$_LANG['addonCA']['ssl']['generateModal']['generateForm']['name']['name']                 = 'Name';
$_LANG['addonCA']['ssl']['generateModal']['generateForm']['domains']['domains']           = 'Domain';
$_LANG['addonCA']['ssl']['generateModal']['generateForm']['bits']['bits']                 = 'Bits';
$_LANG['addonCA']['ssl']['generateModal']['generateForm']['country']['country']           = 'Country';
$_LANG['addonCA']['ssl']['generateModal']['generateForm']['state']['state']               = 'State';
$_LANG['addonCA']['ssl']['generateModal']['generateForm']['city']['city']                 = 'Location (City)';
$_LANG['addonCA']['ssl']['generateModal']['generateForm']['organization']['organization'] = 'Organization Name (Company)';
$_LANG['addonCA']['ssl']['generateModal']['generateForm']['department']['department']     = 'Department';
$_LANG['addonCA']['ssl']['generateModal']['generateForm']['email']['email']               = 'Email';
$_LANG['addonCA']['ssl']['generateModal']['generateForm']['pvt']['pvt']                   = 'Private Key (*.key)';

$_LANG['addonCA']['ssl']['uploadModal']['modal']['uploadModal']                         = 'Upload Certificate';
$_LANG['addonCA']['ssl']['uploadModal']['uploadForm']['name']['name']                   = 'Name';
$_LANG['addonCA']['ssl']['uploadModal']['uploadForm']['domains']['domains']             = 'Domain';
$_LANG['addonCA']['ssl']['uploadModal']['uploadForm']['csrKey']['csrKey']               = 'CSR Key';
$_LANG['addonCA']['ssl']['uploadModal']['uploadForm']['privateKey']['privateKey']       = 'Private Key (*.key)';
$_LANG['addonCA']['ssl']['uploadModal']['uploadForm']['certificate']['certificate']     = 'Certificate (*.crt)';
$_LANG['addonCA']['ssl']['uploadModal']['uploadForm']['caCertificate']['caCertificate'] = 'CA Certificate (*-ca.crt)';
$_LANG['addonCA']['ssl']['uploadModal']['baseAcceptButton']['title']                    = 'Upload';
$_LANG['addonCA']['ssl']['uploadModal']['baseCancelButton']['title']                    = 'Cancel';

$_LANG['addonCA']['ssl']['generateModal']['generateForm']['pvtSection']['pvtType']['pvtType'] = 'Private Key';
$_LANG['addonCA']['ssl']['pvtCustom']                                                         = 'Custom';
$_LANG['addonCA']['ssl']['pvtGenerate']                                                       = 'Generate';
$_LANG['addonCA']['ssl']['generateModal']['pvtGenerate']                                      = 'Private Key (*.key)';
$_LANG['addonCA']['ssl']['generateModal']['generateForm']['pvtSection']['pvt']['pvt']         = 'Private Key (*.key)';
$_LANG['addonCA']['ssl']['generateModal']['pvtGenerate']                                      = 'Generate';
$_LANG['addonCA']['ssl']['generateModal']['pvtCustom']                                        = 'Custom';


$_LANG['certificatesHaveBeenDeleted'] = 'The selected certificates have been deleted successfully';
$_LANG['certificateHasBeenDeleted']   = 'The selected certificate has been deleted successfully';
$_LANG['certificateHasBeenGenerated'] = 'The certificate has been generated successfully';
$_LANG['certificateHasBeenUploaded']  = 'The certificate has been uploaded successfully';


// ---------------------------------------------------- Subdomains  ---------------------------------------------------------

$_LANG['addonCA']['subdomains']['mainContainer']['subdomains']['SubdomainsPageTitle']       = 'Subdomains';
$_LANG['addonCA']['subdomains']['mainContainer']['subdomains']['SubdomainsPageDescription'] = 'A subdomain is a subsection of your website which can function as a new website, but without a new domain name. Use subdomains to create memorable URLs for different content areas of your site. For example, you can create a subdomain for your blog that is accessible through blog.example.com and www.example.com/blog.';

$_LANG['addonCA']['subdomains']['mainContainer']['domainsTable']['table']['name']                          = 'Name';
$_LANG['addonCA']['subdomains']['mainContainer']['domainsTable']['addButton']['button']['addButton']       = 'Create Subdomain';
$_LANG['addonCA']['subdomains']['mainContainer']['domainsTable']['deleteButton']['button']['deleteButton'] = 'Delete';

$_LANG['addonCA']['subdomains']['deleteModal']['modal']['deleteModal']                 = 'Delete Subdomain';
$_LANG['addonCA']['subdomains']['deleteModal']['deleteForm']['confirmSubdomainDelete'] = 'Are you sure that you want to delete this subdomain?';
$_LANG['addonCA']['subdomains']['deleteModal']['baseAcceptButton']['title']            = 'Confirm';
$_LANG['addonCA']['subdomains']['deleteModal']['baseCancelButton']['title']            = 'Cancel';

$_LANG['addonCA']['subdomains']['addModal']['modal']['addModal']                           = 'Create Subdomain';
$_LANG['addonCA']['subdomains']['addModal']['addForm']['nameGroup']['nameGroup']           = 'Name';
$_LANG['addonCA']['subdomains']['addModal']['addForm']['directoryGroup']['directoryGroup'] = 'Document Root';
$_LANG['addonCA']['subdomains']['addModal']['addForm']['secure']['secure']                 = 'Secure With SSL/TLS Certificate';
$_LANG['addonCA']['subdomains']['addModal']['baseAcceptButton']['title']                   = 'Create';
$_LANG['addonCA']['subdomains']['addModal']['baseCancelButton']['title']                   = 'Cancel';

$_LANG['addonCA']['subdomains']['massDeleteModal']['modal']['massDeleteModal']                 = 'Delete Subdomains';
$_LANG['addonCA']['subdomains']['massDeleteModal']['deleteForm']['confirmMassSubdomainDelete'] = 'Are you sure that you want to delete the selected subdomains?';
$_LANG['addonCA']['subdomains']['massDeleteModal']['baseAcceptButton']['title']                = 'Confirm';
$_LANG['addonCA']['subdomains']['massDeleteModal']['baseCancelButton']['title']                = 'Cancel';


$_LANG['subdomainHasBeenDeleted']   = 'The selected subdomain has been deleted successfully';
$_LANG['subdomainHasBeenCreated']   = 'The subdomain has been created successfully';
$_LANG['subdomainsHaveBeenDeleted'] = 'The selected subdomains have been deleted successfully';

// ---------------------------------------------------- ???  ---------------------------------------------------------

$_LANG['addonCA']['ssl']['mainContainer']['sslTable']['datatableDropdownActions']         = 'More Actions';
$_LANG['addonCA']['backups']['mainContainer']['backupsTable']['datatableDropdownActions'] = 'More Actions';

$_LANG['permissionsStorage'] = ':storage_path: settings are not sufficient. Please change permissions to 755 or 777.';

$_LANG['unlimited']                       = "Unlimited";

$_LANG['addonCA']['applications']['deleteModal']['deleteForm']['remove_db']['remove_db']           = 'Remove Database';
$_LANG['addonCA']['applications']['deleteModal']['deleteForm']['remove_db']['description']         = 'Toggle to remove the database of the installation.';
$_LANG['addonCA']['applications']['deleteModal']['deleteForm']['remove_dir']['remove_dir']         = 'Remove Directory';
$_LANG['addonCA']['applications']['deleteModal']['deleteForm']['remove_dir']['description']        = 'Toggle to remove the directory where the script is installed.';
$_LANG['addonCA']['applications']['deleteModal']['deleteForm']['remove_datadir']['remove_datadir'] = 'Remove Data Directory';
$_LANG['addonCA']['applications']['deleteModal']['deleteForm']['remove_datadir']['description']    = 'Toggle to remove the data directory where the script is installed.';

$_LANG['addonCA']['ssl']['mainContainer']['sslTable']['buttonBase']['Additional Actions']                     = "More Actions";
$_LANG['addonCA']['ssl']['mainContainer']['sslTable']['buttonBase']['More Actions']                           = 'More Actions';
$_LANG['addonCA']['ssl']['mainContainer']['sslTable']['buttonBase']['uploadButton']['button']['uploadButton'] = 'Upload Certificate';

/* apps group format alpgabetic */
//$_LANG['addonCA']['applications']['admanager']                                                                          = 'AD MANAGEMENT';
//$_LANG['addonCA']['applications']['blogs']                                                                              = 'Blogs';
//$_LANG['addonCA']['applications']['calendars']                                                                          = 'Calendars';
//$_LANG['addonCA']['applications']['cms']                                                                                = 'CMS';
//$_LANG['addonCA']['applications']['customersupport']                                                                    = 'Customer Support';
//$_LANG['addonCA']['applications']['ecommerce']                                                                          = 'e-Commerce';
//$_LANG['addonCA']['applications']['educational']                                                                        = 'Educational';
//$_LANG['addonCA']['applications']['files']                                                                              = 'Files';
//$_LANG['addonCA']['applications']['forums']                                                                             = 'Forums';
//$_LANG['addonCA']['applications']['galleries']                                                                          = 'Image Galleries';
//$_LANG['addonCA']['applications']['games']                                                                              = 'Games';
//$_LANG['addonCA']['applications']['guestbooks']                                                                         = 'Guest Books';
//$_LANG['addonCA']['applications']['libraries']                                                                          = 'Libraries';
//$_LANG['addonCA']['applications']['mail']                                                                               = 'Mail';
//$_LANG['addonCA']['applications']['music']                                                                              = 'Music';
//$_LANG['addonCA']['applications']['others']                                                                             = 'Others';
//$_LANG['addonCA']['applications']['polls']                                                                              = 'Polls';
//$_LANG['addonCA']['applications']['projectman']                                                                         = 'Project Management';
//$_LANG['addonCA']['applications']['wikis']                                                                              = 'Wikis';
//$_LANG['addonCA']['applications']['socialnetworking']                                                                   = 'Social Networking';
//$_LANG['addonCA']['applications']['erp']                                                                                = 'ERP';
//$_LANG['addonCA']['applications']['frameworks']                                                                         = 'Frameworks';
//$_LANG['addonCA']['applications']['video']                                                                              = 'Video';
//$_LANG['addonCA']['applications']['dbtools']                                                                            = 'DB Tools';
//$_LANG['addonCA']['applications']['microblogs']                                                                         = 'Micro Blogs';
//$_LANG['addonCA']['applications']['rss']                                                                                = 'RSS';

$_LANG['addonCA']['applications']['e-Commerce and Business'] = 'E-COMMERCE AND BUSINESS';
$_LANG['addonCA']['applications']['Photos and Files']        = 'PHOTOS AND FILES';
$_LANG['addonCA']['applications']['Content Management']      = 'CONTENT MANAGEMENT';
$_LANG['addonCA']['applications']['Community Building']      = 'COMMUNITY BUILDING';
$_LANG['addonCA']['applications']['Miscellaneous']           = 'MISCALLANEOUS';
$_LANG['addonCA']['applications']['Surveys and Statistics']  = 'SURVEYS AND STATISTICS';

$_LANG['noMailbox'] = "No mailbox";

$_LANG['addonCA']['backups']['mainContainer']['backupsTable']['buttonBase']['More Actions']       = "More Actions";
$_LANG['addonCA']['backups']['mainContainer']['backupsTable']['buttonBase']['Additional Actions'] = "More Actions";

$_LANG['addonCA']['addonDomains']['addForm']['generalTab']['basicFieldsSection']['domain']['domain']                               = 'Domain';
$_LANG['addonCA']['addonDomains']['addForm']['generalTab']['basicFieldsSection']['hostingType']['hostingType']                     = 'Hosting Type';
$_LANG['addonCA']['addonDomains']['addForm']['generalTab']['forwardingAddressSection']['destinationAddress']['destinationAddress'] = 'Destination Address';
$_LANG['addonCA']['addonDomains']['addForm']['generalTab']['forwardingTypeSection']['forwardingType']['forwardingType']            = 'Forwarding Type';
$_LANG['addonCA']['addonDomains']['addForm']['generalTab']['pathGroup']['pathGroup']                                               = 'Document Root';
$_LANG['addonCA']['addonDomains']['addForm']['additionalTab']['additionalFields']['phpHandler']['phpHandler']                      = 'PHP';
$_LANG['addonCA']['ssl']['mainContainer']['sslTable']['table']['name']                                                             = 'Name';

$_LANG['addonCA']['ssl']['namePlaceHolder']                                           = 'Your certificate name';
$_LANG['addonCA']['ssl']['domainsPlaceHolder']                                        = '-----BEGIN RSA PRIVATE KEY-----   MIIJKQIBAAKCAgEA3W29+ID6194bH6ejLrIC4hb2Ugo8v6ZC+Mrck2dNYMNPjccO...   
-----END RSA PRIVATE KEY-----';
$_LANG['addonCA']['ssl']['certificatePlaceHolder']                                    = 'Certificate:
    Data:
        Version: 3 (0x2)
        Serial Number: 1 (0x1)
        Signature Algorithm: sha1WithRSAEncryption
		...';
$_LANG['addonCA']['ssl']['caCertificatePlaceHolder']                                  = '-----BEGIN CERTIFICATE-----
MIIGOTCCBCGgAwIBAgIJAOE/vJd8EB24MA0GCSqGSIb3DQEBBQUAMIGyMQsw...
-----END CERTIFICATE-----';
$_LANG['addonCA']['ssl']['uploadModal']['uploadForm']['name']['description']                    = 'Type in the uploaded certificate name.';
$_LANG['addonCA']['ssl']['uploadModal']['uploadForm']['domains']['description']                 = 'Select domain to which the uploaded certificate will be assigned.';
$_LANG['addonCA']['ssl']['uploadModal']['uploadForm']['privateKey']['description']              = 'Paste the certificate Private Key.';
$_LANG['addonCA']['ssl']['uploadModal']['uploadForm']['certificate']['description']             = 'Paste the certificate (.crt type).';
$_LANG['addonCA']['ssl']['uploadModal']['uploadForm']['caCertificate']['description']           = 'Paste the CA certificate (-ca.crt type).';
$_LANG['addonCA']['ssl']['generateModal']['generateForm']['pvtSection']['pvt']['description']   = 'Paste the certificate Private Key';
$_LANG['addonCA']['ssl']['generateModal']['placeHolder']                                        = '-----BEGIN RSA PRIVATE KEY-----   
MIIJKQIBAAKCAgEA3W29+ID6194bH6ejLrIC4hb2Ugo8v6ZC+Mrck2dNYMNPjccO...   
-----END RSA PRIVATE KEY-----';

$_LANG['addonCA']['addonDomains']['editForm']['generalTab']['basicFieldsSection']['domain']['domain']                               = 'Domain';
$_LANG['addonCA']['addonDomains']['editForm']['generalTab']['basicFieldsSection']['hostingType']['hostingType']                     = 'Hosting Type';
$_LANG['addonCA']['addonDomains']['editForm']['generalTab']['forwardingAddressSection']['destinationAddress']['destinationAddress'] = 'Destination Address';
$_LANG['addonCA']['addonDomains']['editForm']['generalTab']['forwardingTypeSection']['forwardingType']['forwardingType']            = 'Forwarding Type';
$_LANG['addonCA']['addonDomains']['editForm']['generalTab']['pathGroup']['pathGroup']                                               = 'Document Root';
$_LANG['addonCA']['addonDomains']['editForm']['additionalTab']['additionalFields']['phpHandler']['phpHandler']                      = 'PHP';
$_LANG['addonCA']['addonDomains']['editModal']['tab']['generalTab']                                                                 = 'General';
$_LANG['addonCA']['addonDomains']['editModal']['tab']['additionalTab']                                                              = 'Other';
$_LANG['addonCA']['addonDomains']['editModal']['generalTabTitle']                                                                   = 'General';
$_LANG['addonCA']['addonDomains']['editModal']['additionalTabTitle']                                                                = 'Other';
$_LANG['addonCA']['addonDomains']['addModal']['tab']['generalTab']                                                                  = 'General';
$_LANG['addonCA']['addonDomains']['addModal']['tab']['additionalTab']                                                               = 'Other';
$_LANG['addonCA']['applications']['mainContainer']['applicationsPage']['applicationsNewPage']['applicationsNewPageTab']             = '';

$_LANG['addonCA']['domainAliases']['addModal']['addForm']['redirect']['redirect']       = 'Redirect With HTTP 301 Code';
$_LANG['addonCA']['domainAliases']['addModal']['addForm']['redirect']['description']    = "When visitors open the alias' URL, they will be redirected to the URL of the primary domain. Plesk uses the search engine-friendly HTTP 301 redirection, which means the search engine ranking will not be split between the URLs.";
$_LANG['addonCA']['domainAliases']['editModal']['editForm']['redirect']['redirect']     = 'Redirect With HTTP 301 Code';
$_LANG['addonCA']['domainAliases']['editModal']['editForm']['redirect']['description']  = "When visitors open the alias' URL, they will be redirected to the URL of the primary domain. Plesk uses the search engine-friendly HTTP 301 redirection, which means the search engine ranking will not be split between the URLs.";
$_LANG['addonCA']['ftp']['addModal']['addForm']['loginGroup']['description']            = 'You can use lowercase alphanumeric, dash, and underscore symbols in the username. The username should start with a lowercase alphabetic character and should be between 1 and 16 characters in length.';
$_LANG['addonCA']['emailForwarders']['addModal']['addForm']['baseSection']['destination']['description'] = "Enter destination emails to add to the created email forwarder. Use space, comma with or without space or a semicolon to separate them.";
$_LANG['addonCA']['emailForwarders']['addModal']['addForm']['baseSection']['destination']['destination'] = 'Destination';

$_LANG['CONFIG_SERVICE_PLAN_NAME']                      = 'Service Plan Name';
$_LANG['CONFIG_WHICH_IP_ADDRESSES']                     = 'Which IP addresses to use?';
$_LANG['CONFIG_RESELLER_PLAN_NAME']                     = 'Reseller Plan Name';
$_LANG['CONFIG_POWER_USER_MODE']                        = 'Use Power User view';
$_LANG['CONFIG_POWER_USER_MODE_DESCRIPTION']            = '(Parallels Plesk 12+ only)';
$_LANG['CONFIG_DEFAULT_PHP_VERSION']                    = 'Default PHP Version';

$_LANG['Log Into Control Panel']                                                    = 'Log In To Control Panel';
$_LANG['Separate Plesk User']                                                       = 'Separate Plesk User';
$_LANG['Enable if you want to create web hosting account on a separate Plesk User'] = 'Create a web hosting account on a separate Plesk user';

// ---------------------------------------------------- PHP Settings  ---------------------------------------------------------

$_LANG['addonCA']['phpSettings']['mainContainer']['phpSettings']['phpSettingsTable']                                            = 'Available Domains';
$_LANG['addonCA']['phpSettings']['mainContainer']['phpSettings']['PhpSettingsPageTitle']                                        = 'PHP Settings';
$_LANG['addonCA']['phpSettings']['mainContainer']['phpSettings']['PhpSettingsPageDescription']                                  = 'This page displays the PHP configuration for the website. These settings are specific to the website and do not affect other websites on the subscription. You can change the PHP configuration if the hosting provider grants you the corresponding permission. Custom PHP configuration can be used, for example, to limit the consumption of system resources by PHP scripts, or meet the requirements of a certain web app. When you set the value of a parameter to Default, PHP uses the parameter\'s value from the server-wide PHP configuration.';

$_LANG['addonCA']['phpSettings']['mainContainer']['phpSettings']['table']['name']                                               = 'Domain';
$_LANG['addonCA']['phpSettings']['mainContainer']['phpSettings']['table']['versionPhp']                                         = 'PHP';
$_LANG['addonCA']['phpSettings']['mainContainer']['phpSettings']['table']['php_handler_id']                                     = 'PHP';
$_LANG['addonCA']['phpSettings']['mainContainer']['phpSettings']['phpSettingsEditButton']['button']['phpSettingsEditButton']    = 'Edit';

$_LANG['addonCA']['phpSettings']['PHP']['']                                                                             = 'None';

$_LANG['addonCA']['phpSettings']['phpSettingsModal']['modal']['phpSettingsModal']                                               = 'Settings';
$_LANG['addonCA']['phpSettings']['phpSettingsModal']['baseAcceptButton']['title']                                               = 'Save Changes';
$_LANG['addonCA']['phpSettings']['phpSettingsModal']['baseCancelButton']['title']                                               = 'Cancel';

//$_LANG['addonCA']['phpSettings']['phpSettingsModal']['modal']['phpSettingsModal']['mainTab'] = 'General';
//$_LANG['addonCA']['phpSettings']['phpSettingsModal']['mainTab'] = 'General';
//$_LANG['addonCA']['phpSettings']['phpSettingsForm']['mainTab'] = 'General';

$_LANG['addonCA']['phpSettings']['phpSettingsForm']['mainTab']['base']['enabled']['enabled'] = 'Enabled';

$_LANG['addonCA']['phpSettings']['phpSettingsForm']['mainTab']['generalSection']['rowSection']['columnOne']['php_handler_id']['php_handler_id']             = 'Version';
$_LANG['addonCA']['phpSettings']['phpSettingsForm']['mainTab']['generalSection']['rowSection']['columnOne']['memory_limit']['memory_limit']                 = 'Memory Limit';
$_LANG['addonCA']['phpSettings']['phpSettingsForm']['mainTab']['generalSection']['rowSection']['columnOne']['max_execution_time']['max_execution_time']     = 'Max Execution Time';
$_LANG['addonCA']['phpSettings']['phpSettingsForm']['mainTab']['generalSection']['rowSection']['columnOne']['max_input_time']['max_input_time']             = 'Max Input Time';
$_LANG['addonCA']['phpSettings']['phpSettingsForm']['mainTab']['generalSection']['rowSection']['columnOne']['post_max_size']['post_max_size']               = 'Max Post Size';
$_LANG['addonCA']['phpSettings']['phpSettingsForm']['mainTab']['generalSection']['rowSection']['columnOne']['upload_max_filesize']['upload_max_filesize']   = 'Max Upload File Size';
$_LANG['addonCA']['phpSettings']['phpSettingsForm']['mainTab']['generalSection']['rowSection']['columnOne']['opcache.enable']['opcache.enable']             = 'OP Cache';
$_LANG['addonCA']['phpSettings']['phpSettingsForm']['mainTab']['generalSection']['rowSection']['columnOne']['disable_functions']['disable_functions']       = 'Disable Functions';
$_LANG['addonCA']['phpSettings']['phpSettingsForm']['mainTab']['generalSection']['rowSection']['columnOne']['file_uploads']['file_uploads']                 = 'File Uploads';

$_LANG['addonCA']['phpSettings']['phpSettingsForm']['mainTab']['generalSection']['rowSection']['columnTwo']['include_path']['include_path']                 = 'Include Path';
$_LANG['addonCA']['phpSettings']['phpSettingsForm']['mainTab']['generalSection']['rowSection']['columnTwo']['session.save_path']['session.save_path']       = 'Session Save Path';
$_LANG['addonCA']['phpSettings']['phpSettingsForm']['mainTab']['generalSection']['rowSection']['columnTwo']['mail.force_extra_parameters']['mail.force_extra_parameters'] = 'Mail Extra Parameters (force)';
$_LANG['addonCA']['phpSettings']['phpSettingsForm']['mainTab']['generalSection']['rowSection']['columnTwo']['open_basedir']['open_basedir']                 = 'Open Base Directory';
$_LANG['addonCA']['phpSettings']['phpSettingsForm']['mainTab']['generalSection']['rowSection']['columnTwo']['error_reporting']['error_reporting']           = 'Error Reporting';
$_LANG['addonCA']['phpSettings']['phpSettingsForm']['mainTab']['generalSection']['rowSection']['columnTwo']['display_errors']['display_errors']             = 'Display Errors';
$_LANG['addonCA']['phpSettings']['phpSettingsForm']['mainTab']['generalSection']['rowSection']['columnTwo']['log_errors']['log_errors']                     = 'Log Errors';
$_LANG['addonCA']['phpSettings']['phpSettingsForm']['mainTab']['generalSection']['rowSection']['columnTwo']['allow_url_fopen']['allow_url_fopen']           = 'Allow URL File Open';
$_LANG['addonCA']['phpSettings']['phpSettingsForm']['mainTab']['generalSection']['rowSection']['columnTwo']['short_open_tag']['short_open_tag']             = 'Short Open Tag';
$_LANG['addonCA']['phpSettings']['phpSettingsForm']['mainTab']['generalSection']['rowSection']['columnTwo']['include_path']['includePathDesc']              = 'The list of directories where scripts look for files (similar to system\'s PATH variable). To separate directories, use a colon (:) on Linux and a semicolon (;) on Windows.';

$_LANG['addonCA']['phpSettings']['phpSettingsForm']['fpmTab']['fpmSection']['pm']['pm']                                         = 'PM';
$_LANG['addonCA']['phpSettings']['phpSettingsForm']['fpmTab']['fpmSection']['pm.max_children']['pm.max_children']               = 'Max Children';
$_LANG['addonCA']['phpSettings']['phpSettingsForm']['fpmTab']['fpmSection']['pm.max_requests']['pm.max_requests']               = 'Max Requests';
$_LANG['addonCA']['phpSettings']['phpSettingsForm']['fpmTab']['fpmSection']['pm.start_servers']['pm.start_servers']             = 'Start Servers';
$_LANG['addonCA']['phpSettings']['phpSettingsForm']['fpmTab']['fpmSection']['pm.min_spare_servers']['pm.min_spare_servers']     = 'Min Spare Servers';
$_LANG['addonCA']['phpSettings']['phpSettingsForm']['fpmTab']['fpmSection']['pm.max_spare_servers']['pm.max_spare_servers']     = 'Max Spare Servers';

$_LANG['addonCA']['phpSettings']['phpSettingsForm']['fpmTab']['fpmAddSection']['pm.start_servers']['pm.start_servers']          = 'Start Servers';
$_LANG['addonCA']['phpSettings']['phpSettingsForm']['fpmTab']['fpmAddSection']['pm.min_spare_servers']['pm.min_spare_servers']  = 'Min Spare Servers';
$_LANG['addonCA']['phpSettings']['phpSettingsForm']['fpmTab']['fpmAddSection']['pm.max_spare_servers']['pm.max_spare_servers']  = 'Max Spare Servers';

$_LANG['addonCA']['phpSettings']['phpSettingsForm']['directivesTab']['directivesSection']['additional-directives']['additional-directives']     = 'Additional Directives';
$_LANG['addonCA']['phpSettings']['phpSettingsForm']['directivesTab']['directivesSection']['additional-directives']['additionaDirectivesDesc']   = 'Specifies any PHP directive for a website. Use the same syntax as in the php.ini file to set directives.</br>For example:</br>error_log=/tmp/my_file.log';

# Descriptions
$_LANG['addonCA']['phpSettings']['phpSettingsForm']['mainTab']['generalSection']['rowSection']['columnOne']['memory_limit']['memoryLimit']                                  = 'The maximum amount of memory in bytes a script is allowed to allocate.';
$_LANG['addonCA']['phpSettings']['phpSettingsForm']['mainTab']['generalSection']['rowSection']['columnOne']['max_execution_time']['maxExecTime']                            = 'The maximum time in seconds a script is allowed to run before it is terminated.';
$_LANG['addonCA']['phpSettings']['phpSettingsForm']['mainTab']['generalSection']['rowSection']['columnOne']['max_input_time']['maxInputTime']                               = 'The maximum time in seconds a script is allowed to parse input data.';
$_LANG['addonCA']['phpSettings']['phpSettingsForm']['mainTab']['generalSection']['rowSection']['columnOne']['post_max_size']['postMaxSize']                                 = 'The maximum size in bytes of data that can be posted with the POST method. Typically, should be larger than upload_max_filesize and smaller than memory_limit.';
$_LANG['addonCA']['phpSettings']['phpSettingsForm']['mainTab']['generalSection']['rowSection']['columnOne']['upload_max_filesize']['uploadMaxFilesize']                     = 'The maximum size in bytes of an uploaded file.';
$_LANG['addonCA']['phpSettings']['phpSettingsForm']['mainTab']['generalSection']['rowSection']['columnOne']['opcache.enable']['opcacheEnable']                              = 'Enables the opcode cache. When disabled, code is not optimised or cached. The setting opcache.enable can not be enabled at runtime through ini_set(), it can only be disabled. Trying to enable it in a script will generate a warning.';
$_LANG['addonCA']['phpSettings']['phpSettingsForm']['mainTab']['generalSection']['rowSection']['columnOne']['disable_functions']['disableFunctions']                        = 'This directive allows you to disable certain functions. It takes on a comma-delimited list of function names.';
$_LANG['addonCA']['phpSettings']['phpSettingsForm']['mainTab']['generalSection']['rowSection']['columnOne']['file_uploads']['fileUploads']                                  = 'Allows uploading files over HTTP.';
$_LANG['addonCA']['phpSettings']['phpSettingsForm']['mainTab']['generalSection']['rowSection']['columnTwo']['session.save_path']['sessionSavePath']                         = 'The directory to store PHP session files.';
$_LANG['addonCA']['phpSettings']['phpSettingsForm']['mainTab']['generalSection']['rowSection']['columnTwo']['mail.force_extra_parameters']['mailForceExtraParameters']      = 'Additional parameters for the mail() function.';
$_LANG['addonCA']['phpSettings']['phpSettingsForm']['mainTab']['generalSection']['rowSection']['columnTwo']['open_basedir']['openBasedir']                                  = 'Files in the specified directories can be accessed by PHP scripts. To separate directories, use a colon (:) on Linux and a semicolon (;) on Windows.';
$_LANG['addonCA']['phpSettings']['phpSettingsForm']['mainTab']['generalSection']['rowSection']['columnTwo']['error_reporting']['errorReporting']                            = 'The error reporting level.';
$_LANG['addonCA']['phpSettings']['phpSettingsForm']['mainTab']['generalSection']['rowSection']['columnTwo']['display_errors']['displayErrors']                              = 'Determines whether errors should be displayed as a part of the output.';
$_LANG['addonCA']['phpSettings']['phpSettingsForm']['mainTab']['generalSection']['rowSection']['columnTwo']['log_errors']['logErrors']                                      = 'Enable the logging of PHP errors.';
$_LANG['addonCA']['phpSettings']['phpSettingsForm']['mainTab']['generalSection']['rowSection']['columnTwo']['allow_url_fopen']['allowUrlFopen']                             = 'Allows PHP file functions to retrieve data from remote locations over FTP or HTTP.';
$_LANG['addonCA']['phpSettings']['phpSettingsForm']['mainTab']['generalSection']['rowSection']['columnTwo']['short_open_tag']['shortOpenTag']                               = 'Allows the short form (<? ?>) of the PHP\'s open tag.';



$_LANG['addonCA']['phpSettings']['phpSettingsForm']['fpmTab']['fpmSection']['pm']['pmGeneral']                                                                              = 'static - the number of child processes is fixed (pm.max_children).<br><br>ondemand - the processes spawn on demand (when requested, as opposed to dynamic, where pm.start_servers are started when the service is started.<br><br>dynamic - the number of child processes is set dynamically based on the following directives: pm.max_children, pm.start_servers, pm.min_spare_servers, pm.max_spare_servers.';
$_LANG['addonCA']['phpSettings']['phpSettingsForm']['fpmTab']['fpmSection']['pm.max_children']['pmMaxChildren']                                                             = 'The number of child processes to be created when pm is set to static and the maximum number of child processes to be created when pm is set to dynamic.';
$_LANG['addonCA']['phpSettings']['phpSettingsForm']['fpmTab']['fpmSection']['pm.max_requests']['pmMaxRequests']                                                             = 'The number of requests each child process should execute before respawning. This can be useful to work around memory leaks in 3rd party libraries. For endless request processing specify \'0\'.';
$_LANG['addonCA']['phpSettings']['phpSettingsForm']['fpmTab']['fpmAddSection']['pm.start_servers']['pmStartServers']                                                        = 'The number of child processes created on startup. Used only when pm is set to dynamic. Default Value: min_spare_servers + (max_spare_servers - min_spare_servers) / 2.';
$_LANG['addonCA']['phpSettings']['phpSettingsForm']['fpmTab']['fpmAddSection']['pm.min_spare_servers']['pmMinSpareServers']                                                 = 'The desired minimum number of idle server processes. Used only when pm is set to dynamic. Also mandatory in this case.';
$_LANG['addonCA']['phpSettings']['phpSettingsForm']['fpmTab']['fpmAddSection']['pm.max_spare_servers']['pmMaxSpareServers']                                                 = 'The desired maximum number of idle server processes. Used only when pm is set to dynamic. Also mandatory in this case.';

$_LANG['phpSettingsUpdated']                                                                                                                                                = 'PHP Settings have been updated successfully';

$_LANG['addonCA']['phpSettings']['phpSettingsModal']['tab']['mainTab'] = 'General';
$_LANG['addonCA']['phpSettings']['phpSettingsModal']['tab']['directivesTab'] = 'Directives';
$_LANG['addonCA']['phpSettings']['phpSettingsModal']['tab']['fpmTab'] = 'FPM';

// ---------------------------------------------------- Log Rotation  ---------------------------------------------------------

$_LANG['addonCA']['logRotation']['mainContainer']['logRotation']['logRotationTable']                                    = 'Available Domains';
$_LANG['addonCA']['logRotation']['mainContainer']['logRotation']['LogRotationPageTitle']                                = 'Log Rotation';
$_LANG['addonCA']['logRotation']['mainContainer']['logRotation']['LogRotationPageDescription']                          = 'Section responsible for logs management with option to configure log rotation conditions by size or by time. Please keep in mind that custom logs cannot be rotated.';

$_LANG['addonCA']['logRotation']['mainContainer']['logRotation']['table']['name']                                                      = 'Domain';
$_LANG['addonCA']['logRotation']['mainContainer']['logRotation']['logRotationEditButton']['button']['logRotationEditButton']           = 'Edit Settings';
$_LANG['addonCA']['logRotation']['mainContainer']['logRotation']['logRotationMassEditButton']['button']['logRotationMassEditButton']   = 'Edit Selected';

$_LANG['addonCA']['logRotation']['logRotationModal']['logRotationForm']['enabled']['enabled']                           = 'Log Rotation';
$_LANG['addonCA']['logRotation']['logRotationModal']['logRotationForm']['log-compress']['log-compress']                 = 'Compress Log Files';
$_LANG['addonCA']['logRotation']['logRotationModal']['logRotationForm']['log-condition']['log-condition']               = 'Log Rotation Condition';
$_LANG['addonCA']['logRotation']['logRotationModal']['logRotationForm']['log-max-num-files']['log-max-num-files']       = 'Maximum Number Of Log Files';
$_LANG['addonCA']['logRotation']['logRotationModal']['logRotationForm']['log-bytime']['log-bytime']                     = 'By Time';
$_LANG['addonCA']['logRotation']['logRotationModal']['logRotationForm']['log-bysize']['log-bysize']                     = 'By Size (KB)';
$_LANG['addonCA']['logRotation']['logRotationModal']['logRotationForm']['log-email']['log-email']                       = 'Email Address';

$_LANG['addonCA']['logRotation']['logRotationModal']['modal']['logRotationModal']                                       = 'Settings';
$_LANG['addonCA']['logRotation']['logRotationModal']['baseAcceptButton']['title']                                       = 'Save Changes';
$_LANG['addonCA']['logRotation']['logRotationModal']['baseCancelButton']['title']                                       = 'Cancel';

$_LANG['addonCA']['logRotation']['massEditModal']['enabled']['enabled']                                                 = 'Log Rotation';
$_LANG['addonCA']['logRotation']['massEditModal']['log-compress']['log-compress']                                       = 'Compress Log Files';
$_LANG['addonCA']['logRotation']['massEditModal']['log-condition']['log-condition']                                     = 'Log Rotation Condition';
$_LANG['addonCA']['logRotation']['massEditModal']['log-max-num-files']['log-max-num-files']                             = 'Maximum Number Of Log Files';
$_LANG['addonCA']['logRotation']['massEditModal']['log-bytime']['log-bytime']                                           = 'By Time';
$_LANG['addonCA']['logRotation']['massEditModal']['log-bysize']['log-bysize']                                           = 'By Size (KB)';
$_LANG['addonCA']['logRotation']['massEditModal']['log-email']['log-email']                                             = 'Email Address';

$_LANG['addonCA']['logRotation']['massEditModal']['modal']['massEditModal']                                             = 'Settings';
$_LANG['addonCA']['logRotation']['massEditModal']['baseAcceptButton']['title']                                          = 'Save Changes';
$_LANG['addonCA']['logRotation']['massEditModal']['baseCancelButton']['title']                                          = 'Cancel';
$_LANG['addonCA']['logRotation']['massEditModal']['logRotationForm']['enabled']['enabled']                              = 'Enabled';

$_LANG['addonCA']['logRotation']['massEditModal']['massEditForm']['base']['enableSections']['enabled']['enabled']                           = 'Log Rotation';
$_LANG['addonCA']['logRotation']['massEditModal']['massEditForm']['base']['enableSections']['log-max-num-files']['log-max-num-files']       = 'Maximum Number Of Log Files';
$_LANG['addonCA']['logRotation']['massEditModal']['massEditForm']['base']['compressSections']['log-compress']['log-compress']               = 'Compress Log Files';
$_LANG['addonCA']['logRotation']['massEditModal']['massEditForm']['base']['compressSections']['log-email']['log-email']                     = 'Email Address';
$_LANG['addonCA']['logRotation']['massEditModal']['massEditForm']['baseSection']['log-condition']['log-condition']                          = 'Log Rotation Condition';
$_LANG['addonCA']['logRotation']['massEditModal']['massEditForm']['columnCondition']['columnTime']['log-bytime']['log-bytime']              = 'By Time';
$_LANG['addonCA']['logRotation']['massEditModal']['massEditForm']['columnCondition']['columnSize']['log-bysize']['log-bysize']              = 'By Size (KB)';

$_LANG['addonCA']['logRotation']['logRotationModal']['logRotationForm']['log-email']['logSendMail']                         = 'After Plesk retrieves data from log files of web and FTP servers, email the log files to this address';
$_LANG['addonCA']['logRotation']['massEditModal']['massEditForm']['base']['compressSections']['log-email']['logSendMail']   = 'After Plesk retrieves data from log files of web and FTP servers, email the log files to this address';

$_LANG['logRotationHasBeenEdited']                                                                                      = 'All log rotation settings has been updated succesfully';
$_LANG['logRotationHasBeenUpdated']                                                                                     = 'Log rotation settings have been updated successfully';
$_LANG['logRotationHasBeenDisabled']                                                                                    = 'Log rotation has been successfully disabled';
$_LANG['addonCA']['logRotation']['mainContainer']['logRotation']['table']['log']                                        = 'Status';

// ---------------------------------------------------- *  ---------------------------------------------------------
