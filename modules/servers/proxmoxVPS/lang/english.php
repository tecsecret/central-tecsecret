<?php
/* * ********************************************************************
 *  ProxmoxVPS  Product developed. (2013-11-18)
 * *
 *
 *  CREATED BY MODULESGARDEN       ->       http://modulesgarden.com
 *  CONTACT                        ->       contact@modulesgarden.com
 *
 *
 * This software is furnished under a license and may be used and copied
 * only  in  accordance  with  the  terms  of such  license and with the
 * inclusion of the above copyright notice.  This software  or any other
 * copies thereof may not be provided or otherwise made available to any
 * other person.  No title to and  ownership of the  software is  hereby
 * transferred.
 *
 *
 * ******************************************************************** */

/**
 * @author Grzegorz Draganik <grzegorz@modulesgarden.com>
 * @author Paweł Kopeć <pawelk@modulesgarden.com>
 * 
 * Proxmox v 1. 1
 * New translated added:
 * $_LANG['vps']['backup_actions'] 
 * $_LANG['vps']['backup_error_maxjobs'] 
 * 
 * Proxmox v 1. 2
 * New translated added:
 * unlimited, IP_Addresses,
 * Backups_Files_Limit, Network_Rate
 */
$_LANG['generalError']  = "Somethings Goes Wrong, check logs, contact admin";
$_LANG['general']['back']		   = 'Back To Service';
$_LANG['general']['graph_of']	   = 'of';
$_LANG['general']['graph_used']       = 'Used';
$_LANG['general']['usage']		   = 'Usage';
$_LANG['general']['graph_free']   	   = 'Free';
$_LANG['general']['console_info']	   = 'Console requires java software. You need to accept the \'security risk\' popup to run it.';
$_LANG['general']['save']             = 'Save Changes';
$_LANG['general']['add']              = 'Add';
$_LANG['general']['cancel']           = 'Cancel';
$_LANG['general']['confirm']          = 'Are you sure?';
$_LANG['general']['total']            = 'Total';
$_LANG['general']['working']          = 'Working...';
$_LANG['general']['none']              = 'None';
$_LANG['vps']['management']		   = 'Management';
$_LANG['vps']['delete']		   = 'Delete';
$_LANG['vps']['rebuild']		   = 'Reinstall';
$_LANG['vps']['shutdown']		   = 'Shutdown';
$_LANG['vps']['reinstall']		   = 'Reinstall';
$_LANG['vps']['stop']	          = 'Stop';
$_LANG['vps']['reboot']		   = 'Reboot';
$_LANG['vps']['console']	 	   = 'VNC Console';
$_LANG['vps']['control_panel']	   = 'Control Panel';
$_LANG['vps']['details']		   = 'Details';
$_LANG['vps']['status']		   = 'Status';
$_LANG['vps']['boot']		   = 'Boot';
$_LANG['vps']['type']	          = 'Type';
$_LANG['vps']['template']		   = 'Template';
$_LANG['vps']['hostname']		   = 'Hostname';
$_LANG['vps']['change_host_btn']      = 'Change';
$_LANG['vps']['change_hostname']      = 'Change Hostname';
$_LANG['vps']['main_ip']		   = 'Main IP Address';
$_LANG['vps']['ip_addresses']         = 'IP Addresses';
$_LANG['vps']['root_password']        = 'Root Password';
$_LANG['vps']['root_pass_show']       = 'Show';
$_LANG['vps']['root_change_password'] = 'Change Password';
$_LANG['vps']['root_pass_change']	   = 'Change';
$_LANG['vps']['bandwidth']		   = 'Bandwidth';
$_LANG['vps']['memory']		   = 'Memory';
$_LANG['vps']['hdd']			   = 'HDD';
$_LANG['vps']['graphs']		   = 'Graphs';
$_LANG['vps']['graph_cpu']	          = 'CPU Usage %';
$_LANG['vps']['graph_mem']	          = 'Memory Usage';
$_LANG['vps']['graph_net']	          = 'Network Traffic';
$_LANG['vps']['graph_disk']	          = 'Disk IO';
$_LANG['vps']['burst']	          = 'Burst';
$_LANG['vps']['dedicated']	          = 'Dedicated';
$_LANG['vps']['cpu_units']	          = 'CPU Units';
$_LANG['vps']['graph_no_cpu']   	   = 'Traffic graph is not available yet';
$_LANG['vps']['graph_no_mem']	   = 'Memory graph is not available yet';
$_LANG['vps']['graph_no_net']	   = 'Network graph is not available yet';
$_LANG['vps']['graph_no_disk']	   = 'Disk graph is not available yet';
$_LANG['vps']['hour']                 = "Hour";
$_LANG['vps']['day']                  = "Day";
$_LANG['vps']['week']                 = "Week";
$_LANG['vps']['month']                = "Month";
$_LANG['vps']['year']                 = "Year";
$_LANG['vps']['rebuild_template']     = 'Select Template:';
$_LANG['vps']['rebuild_cd_rom_iso']   = 'Select CD-ROM ISO:';
$_LANG['vps']['rebuild_boot_order']   = 'Boot Order:';
$_LANG['vps']['rebuild_boot_1']	   = 'Boot Device 1';
$_LANG['vps']['rebuild_boot_2']	   = 'Boot Device 2';
$_LANG['vps']['rebuild_boot_3']	   = 'Boot Device 3';
$_LANG['vps']['backup']		   = 'Backups'; 
$_LANG['vps']['backup_jobs']	   = 'Backup Jobs';
$_LANG['vps']['no_backup_files']      = 'Empty';
$_LANG['vps']['backup_id']            = 'ID';
$_LANG['vps']['backup_start_time']    = 'Start Time';
$_LANG['vps']['backup_day_of_week']   = 'Day of Week';
$_LANG['vps']['backup_compress']      = 'Compression Type';
$_LANG['vps']['backup_mode']          = 'Backup Mode';
$_LANG['vps']['backup_new']           = 'New Backup';
$_LANG['vps']['backup_new_job']       = 'New Job';
$_LANG['vps']['no_backup_job']        = 'Empty';
$_LANG['vps']['backup_file']          = 'File';
$_LANG['vps']['backupDate']          = 'Date';
$_LANG['vps']['backup_format']        = 'Format';
$_LANG['vps']['backup_size']          = 'Size';
$_LANG['vps']['backup_actions']       = 'Actions';
$_LANG['vps']['backup_error_size']    = 'Maximum size for a backup set has been exceeded. Please remove the old backup files';
$_LANG['vps']['backup_error_maxfiles']= 'Maximum files for a backup set has been exceeded. Please remove the old backup files';
$_LANG['vps']['backup_error_maxjobs'] = 'Maximum Jobs for a backup set has been exceeded. Please remove the old backup jobs';
$_LANG['vps']['new_backup']		   = 'New Backup';
$_LANG['vps']['backup_restore_info']  = 'Are you sure you want to restore this VM? This will permanently erase the current VM data';
$_LANG['vps']['backup_reload']	   = 'Backup list has been reloaded';

$_LANG['vps']['week_array']           = array(
                                                "mon" => "Monday",
                                                "tue" => "Tuesday",
                                                "wed" => "Wednesday",
                                                "thu" => "Thursday",
                                                "fri" => "Friday",
                                                "sat" => "Saturday",
                                                "sun" => "Sunday"
                                          );

$_LANG['vps']['rebuild_no_templates'] = 'No templates available';
$_LANG['vps']['console_pass']	   = 'Password:';
$_LANG['vps']['console_session']      = 'Session Expires:';
$_LANG['vps_ajax']['no_server']	   = 'Setup the server first';
$_LANG['vps_ajax']['no_access']	   = 'No access to that action';
$_LANG['vps_ajax']['unknown']	   = 'Unknown error';
$_LANG['vps']['dns_manager_reverse']  = 'Manage Reverse DNS';
$_LANG['vps']['dns_manager_manage']   = 'Manage DNS';
$_LANG['vps']['reinstallmsg']	   = 'Are you sure to reinstall?';
$_LANG['vps']['name']	          = 'Name';
$_LANG['vps']['cpus']	          = 'CPUs';
$_LANG['vps']['uptime']	          = 'Uptime';
$_LANG['vps']['cpu_usage']	          = 'CPU Usage';
$_LANG['vps']['memory_usage']	   = 'Memory Usage';
$_LANG['vps']['disc_usage']	          = 'Disk Usage';
$_LANG['vps']['ip_addresses']	   = 'IP Addresses';
$_LANG['vps']['mac_addresses']	   = 'MAC Addresses';
$_LANG['vps']['network']	          = 'Network Device Configuration';
$_LANG['vps']['ip_address']	          = 'IP Address';
$_LANG['vps']['netmask']    	   = 'MAC';
$_LANG['vps']['dns']     	          = 'DNS';
$_LANG['vps']['ports_slaves']	   = 'Ports / Slaves';
$_LANG['vps']['ip_address']	          = 'IP Address';
$_LANG['vps']['subnet_mask']	   = 'Subnet Mask';
$_LANG['vps']['gateway']	          = 'Gateway';
$_LANG['vps']['network_device']	   = 'Network Device';
$_LANG['vps_ajax']['no_template']	   = 'Select the template before reinstallation';
$_LANG['vps_ajax']['template_empty']  = 'No templates available for reinstallation';
$_LANG['vps']['delete']		   = 'Delete';
$_LANG['vps']['update']		   = 'Update';
$_LANG['vps']['restore']		   = 'Restore';
// v 1.2
$_LANG['vps']['unlimited']		   = 'Unlimited';
$_LANG['vps']['IP_Addresses']         = 'IP Addresses';
$_LANG['vps']['Backups_Files_Limit']  = 'Backups Files Limit';
$_LANG['vps']['Network_Rate']         = 'Network Rate';

// v 1.3
$_LANG['vps']['node']                 = 'Node';
$_LANG['vps']['task_history']         = 'Task History';
$_LANG['vps']['start_time']           = 'Start Time';
$_LANG['vps']['end_time']             = 'End Time';
$_LANG['vps']['description']          = 'Description';
$_LANG['vps']['nothing_to_display']   = 'Nothing to display';
$_LANG['vps']['vlan_tag']             = 'VLAN Tag';
$_LANG['vps']['msg_backup_routing']             = 'Your routing backup limit is %d. When you exceed this limit, the last backup will be replaced with a new one.';

//v 1.4

$_LANG['button.console']                = 'VNC Console';
$_LANG['button.reinstall']              = 'Reinstall';
$_LANG['button.backup']                 = 'Backup';
$_LANG['button.backup_jobs']            = 'Backup Jobs';
$_LANG['button.graphs']                 = 'Graphs';
$_LANG['button.task_history']           = 'Task History';
$_LANG['button_spice']                  = 'SPICE Console';


$_LANG['button_snapshots']	            = 'Snapshots';
$_LANG['snapshots']['header']		            = 'Snapshots';
$_LANG['snapshots']['name']		            = 'Name';
$_LANG['snapshots']['ram']		            = 'RAM';
$_LANG['snapshots']['date_status']		            = 'Date/Status';
$_LANG['snapshots']['description']		            = 'Description';
$_LANG['snapshots']['action']	= 'Actions';
$_LANG['snapshots']['empty']	= 'No snapshots available';

 $_LANG['snapshots']['ram_on']	= 'Yes';
 $_LANG['snapshots']['ram_off']	= 'No';
 $_LANG['snapshots']['activated']	= 'Activated';
 $_LANG['snapshots']['deleted']	= 'Snapshot has been deleted';
 $_LANG['snapshots']['updated']	= 'Snapshot has been updated';
 $_LANG['snapshots']['delete']	= 'Are you sure you want to delete this snapshot?';
 $_LANG['snapshots']['take_snapshot']	= 'Take Snapshot';
 $_LANG['snapshots']['rollback']	= ' Rollback';
 $_LANG['snapshots']['rollback_alert']	= 'Are you sure you want to roll back this snapshot?';
  $_LANG['snapshots']['deleteMsg']	= 'Are you sure you want to delete this snapshot?';
 $_LANG['snapshots']['rollbacked']	= 'Snapshot has been rolled back';
 $_LANG['snapshots']['close'] = 'Close';
 
$_LANG['snapshots_form']['name']		            = 'Name:';
$_LANG['snapshots_form']['incram']		            = 'Include RAM: ';
$_LANG['snapshots_form']['description']		            = 'Description: ';
$_LANG['snapshots']['created']	= 'Snapshot has been created';
$_LANG['snapshots']['err_alphabetical']	= 'Only alphabetical characters are allowed.';

$_LANG['button_network']	= 'Network';
$_LANG['network']['header']		            = 'Network';
$_LANG['network']['type']		            = 'Type';
$_LANG['network']['name']		            = 'IP Address / Name';
$_LANG['network']['bridge']		            = 'Bridge';
$_LANG['network']['mac']		            = 'Mac Address';
$_LANG['network']['empty']	= 'No networks available';


$_LANG['network']['type_ip']	= 'IP Address';
$_LANG['network']['type_device']	= 'Network Device';

$_LANG['network']['new']	= 'New Private Network';
$_LANG['network']['added']	= 'Private network has been added';
$_LANG['network']['action']	= 'Action';

$_LANG['network']['delete']	= 'Are you sure you want to delete this network?';
$_LANG['network']['deleted']	= 'Private network has been deleted';
$_LANG['network']['tag']	= 'Tag';


$_LANG['ajax']['unknown']	= 'Unknown error'; // access denied
$_LANG['ajax']['vm_booted']  = 'Container has been booted';
$_LANG['ajax']['vm_rebooted']  = 'Container has been rebooted';
$_LANG['ajax']['vm_stopped']  = 'Container has been stopped';
$_LANG['ajax']['vm_shutdown']  = 'Container has been shut down';
$_LANG['ajax']['vm_reinstalled']  = 'Container has been reinstalled';
$_LANG['ajax']['backup_job_created']  = 'Backup job has been successfully created';
$_LANG['ajax']['backup_job_updated']  = 'Backup job has been successfully updated';
$_LANG['ajax']['backup_job_deleted']  = 'Backup job has been successfully  deleted';
$_LANG['ajax']['backup_creating']  = 'Creating backup in progress...';
$_LANG['ajax']['backup_deleted']  = 'Backup file has been deleted';
$_LANG['ajax']['backup_restored']  = 'VM has been restored';
$_LANG['ajax']['hostname_changed']  = 'Hostname has been changed';
$_LANG['ajax']['hostname_failed']  = 'Hostname change failed';

$_LANG['ajax']['vm_running']  = 'VM is already running';
$_LANG['ajax']['vm_stopped']  = 'VM is already stopped';
$_LANG['ajax']['vm_no_running']  = 'VM not running';

$_LANG['button_firewall']  = 'Firewall';
$_LANG['firewall']['header']		            = 'Firewall';
$_LANG['firewall']['enable']		            = 'Enable';
$_LANG['firewall']['type']		            = 'Type';
$_LANG['firewall']['action']		            = 'Action';
$_LANG['firewall']['source']		            = 'Source';

$_LANG['firewall']['macro']		            = 'Macro';
$_LANG['firewall']['interface']		            = 'Interface';
$_LANG['firewall']['destination']		            = 'Destination';
$_LANG['firewall']['protocol']		            = 'Protocol';
$_LANG['firewall']['dest_port']		            = 'Dest. Port';
$_LANG['firewall']['source_port']		            = 'Source Port';
$_LANG['firewall']['comment']		            = 'Comment';
$_LANG['firewall']['actions']		            = 'Actions';
$_LANG['firewall']['empty']	= 'No rules available';

$_LANG['firewall']['add']		            = 'Add';
$_LANG['firewall']['add_rule']		            = 'Add: Rule';
$_LANG['firewall']['edit_rule']		            = 'Edit: Rule';
$_LANG['firewall']['close']		            = 'Close';

$_LANG['firewall']['direction']	= 'Direction';
$_LANG['firewall']['delete']	= 'Are you sure you want to delete this rule?';
$_LANG['firewall']['directions']	 =  array(
                                                "in" => "in",
                                                "out" => "out"
                                   );
$_LANG['firewall']['action_select'] =  array(
                                               "ACCEPT" => "ACCEPT",
                                               "DROP"   => "DROP",
                                               "REJECT" => "REJECT",
                                   );

$_LANG['firewall']['protocols'] =  array(
                                         "tcp" => "6  Transmission Control Protocol",
                                         "udp"   => "17 User Datagram Protocol",
                                         "icmp" => "1 Internet Control Message Protocol",
                                         "igmp" => "2 Internet Group Management",
                                         "ggp" => "3 gateway-gateway protocol",
                                         "ipencap" => "4 IP encapsulated in IP",
                                         "st" => "5 ST datagram mode",
                                         "egp" => "8 exterior gateway protocol",
                                         "igp" => "9 any private interior gateway (Cisco)",
                                         "pup" => "12 PARC universal packet protocol",
                                         "hmp" => "20 host monitoring protocol",
                                         "xns-idp" => "22 Xerox NS IDP",
                                         "rdp" => "27 \"reliable datagram\" protocol",
                                         "iso-tp4" => "29 ISO Transport Protocol class 4 [RFC905]",
                                         "dccp" => "33 Datagram Congestion Control Prot. ",
                                         "xtp" => "33 Xpress Transfer Protocol",
                                         "ddp" => "37 Datagram Delivery Protocol",
                                         "idpr-cmtp" => "38 IDPR Control Message Transport",
                                         "ipv6" => "41 Internet Protocol, version 6",
                                         "ipv6-route" => "43 Routing Header for IPv6",
                                         "ipv6-frag" => "44 Fragment Header for IPv6",
                                         "idrp" => "45 Inter-Domain outing Protocol",
                                         "rsvp" => "46 Reservation Protocol",
                                         "gre" => "47 General Routing Encapsulation",
                                         "esp" => "50 Encap Security Payload [RFC2406]",
                                         "ah" => "51 Authentication Header [RFC2402]",
                                         "skip" => "57 SKIP",
                                         "ipv6-icmp" => "58 ICMP for IPv6",
                                         "ipv6v-nonxt" => "59 No Next Header for IPv6",
                                         "ipv6-opts" => "60 Destination Options for IPv6",
                                         "vmtp" => "81 Versatile Message Transport",
                                         "eigrp" => "88 Enhanced Interior Routing Protocol (Cisco)",
                                         "ospf" => "89 Open Shortest Path First IGP",
                                         "ax.25" => "93 AX.25 frames",
                                         "ipip" => "94 Ip-within-IP Encapsulation Protocol",
                                         "etherip" => "97 IP-within-IP Encapsulation [RFC3378]",
                                         "pim" => "103 Protocol Independent Muulticast",
                                         "ipvomp" => "108 IP Payload Compression Protocol",
                                         "vrrp" => "112 Virtual Router Redundancy Protocol",
                                         "l2tp" => "115 Layer Two Tunneling Protocol [RFC2661]",
                                         "isis" => "124 IS-IS over IPv4",
                                         "sctp" => "132 Stream Control Transmission Protocol",
                                         "fc" => "133 Fibre Channel",
                                         "mobility-header" => "135 Mobility Support for Ipv6 [RFC3775]",
                                         "udplite" => "136 UDP-Lite [RFC3828]",
                                         "mpls-in-ip" => "136 UDP-Lite [RFC3828]",
                                         "hip" => "139 Host Identity Protocol",
                                         "shim6" => "139 Shim6 Protocol [RFC5533]",
                                         "wesp" => "141 Wrapped Encapsulating Security Payload",
                                         "rohc" => "142 Robust Header Compression",
                                   );
$_LANG['firewall']['rule_created']	= 'Rule has been successfully created';
$_LANG['firewall']['rule_updated']	= 'Rule has been successfully updated';
$_LANG['firewall']['rule_delted']	= 'Rule has been successfully removed';
$_LANG['general']['edit']          = 'Edit';
$_LANG['general']['ok']          = 'OK';
$_LANG['firewall']['add_group']	= 'Insert: Security Group';
$_LANG['firewall']['edit_group']	= 'Edit: Security Group';
$_LANG['firewall']['security_group']	= 'Security Group';
$_LANG['firewall']['venet']	= 'venet';

$_LANG['reinstall']['iso_changed']	= 'ISO image and boot order have been changed';
$_LANG['reinstall']['vm_reinstalled']	= 'VM has been reinstalled';
$_LANG['vps']['unlimited']			= 'Unlimited';
$_LANG['button_novnc'] = "noVNC"; 
$_LANG['button_disksManagement'] = "Disks"; 


$_LANG['disks']['header'] = 'Disks';
$_LANG['disks']['name']	  = 'Name';
$_LANG['disks']['bus']	  = 'Bus / Device';
$_LANG['disks']['format']		            = 'Format';
$_LANG['disks']['backup']		            = 'Backup';
$_LANG['disks']['size']		            = 'Size';
$_LANG['disks']['disk-1']		            = 'Disk 1';
$_LANG['disks']['disk-2']		            = 'Disk 2';
$_LANG['disks']['disk-3']		            = 'Disk 3';
$_LANG['disks']['disk-4']		            = 'Disk 4';
$_LANG['disks']['disk-5']		            = 'Disk 5';
$_LANG['disks']['disk-6']		            = 'Disk 6';
$_LANG['disks']['disk-7']		            = 'Disk 7';
$_LANG['disks']['disk-8']		            = 'Disk 8';
$_LANG['disks']['actons']		            = 'Actions';
$_LANG['disks']['empty']		            = 'No Disks';
$_LANG['disks']['delete']		            = 'Are you sure you want to delete this hard disk?';
$_LANG['disks']['added']	= 'Hard disk has been added';
$_LANG['disks']['deleted']	= 'Hard disk has been deleted';
$_LANG['disks']['edited']	= 'Hard disk has been edited';
$_LANG['disks']['actions']	= 'Actions';
$_LANG['disks']['add']	= 'Add Hard Disk';
$_LANG['disks']['size_gb']		            = 'Size (GB)';
$_LANG['general']['close']           = 'Close';
$_LANG['disks']['formats'] =array(
                                 'raw'   => 'Raw disk image (raw)',
                                 'qcow2' => 'QEMU image format (qcow2)', 
                                 'vmdk'  => 'VM image format (vmdk)'
                           );

$_LANG['disks']['add_new_hhd'] ='Add: Hard Disk';
$_LANG['disks']['errorAddDisk'] ='You are not able to set %s GB of the disk size. Disk size available: %s GB';
$_LANG['disks']['errorEditDisk'] ='Downgrading the disk size is restricted';
 $_LANG['disks']['backup_yes']	= 'Yes';
 $_LANG['disks']['backup_no']	= 'No';
 

$_LANG['disks']['edit_hhd'] ='Edit: Hard Disk';
$_LANG['home']['AdditionalTools']	   = 'Additional Tools';
$_LANG['button.management']                = 'Management';
$_LANG['panel']['manageVM']                = 'Manage VM';
$_LANG['panel']['additionalTools']         = 'Additional Tools';
$_LANG['token'] =' Token ';
$_LANG['reinstall']['header'] ='Reinstall';
$_LANG['reinstall']['description'] ='Select the template for reinstallation. If you continue, all data located on the virtual machine will be lost!';
$_LANG['reinstall']['noTemplate']='Provide the template for reinstallation';
$_LANG['reinstall']['password']='Password:';
$_LANG['reinstall']['template']='OS Template:';
$_LANG['backup']['empty']		            = 'No Backups';
$_LANG['backup']['add_new']		            = 'Create: Backup';
$_LANG['backup']['delete']		            = 'Are you sure you want to delete this file?';

$_LANG['jobs']['empty']		            = 'No Backup Jobs';
$_LANG['jobs']['emailNotification']		            = 'Email Notification';
$_LANG['jobs']['add_new']		            = 'Add: Backup Job';
$_LANG['jobs']['mailto']		            = 'Email Notification';
$_LANG['jobs']['mailtoDes']		            = 'Enable the email notification';
$_LANG['jobs']['delete']		            = 'Are you sure you want to delete this job?';
$_LANG['jobs']['edit']		            = 'Edit: Backup Job';
$_LANG['jobs']['compressNone'] ='None';
$_LANG['jobs']['labelDelete']           = 'Delete Job: ';
$_LANG['jobs']['description']           ="Instead of creating backups manually, set up a job, which will automatically create backups for you. Created backups will be available under the 'Backups' page.";


$_LANG['home']['disc'] = 'CD/DVD Disc Image File';
$_LANG['home']['bootOrder'] = 'Boot Order';
$_LANG['home']['boot']['0'] = 'None';
$_LANG['home']['boot']['d'] = 'CD-ROM';
$_LANG['home']['boot']['n'] = 'Network';
$_LANG['home']['boot']['c'] = 'Disk';
$_LANG['home']['boot_order_1']           = 'Device 1:';
$_LANG['home']['boot_order_2']           = 'Device 2:';
$_LANG['home']['boot_order_3']           = 'Device 3:';

$_LANG['CAPages']['taskHistory']['search'] ='Search';
$_LANG['CAPages']['taskHistory']['noAvailable']='No Available';
$_LANG['CAPages']['taskHistory']['noAvailableInfo']= "";
$_LANG['CAPages']['taskHistory']['previous']='Previous';
$_LANG['CAPages']['taskHistory']['next'] ='Next';

$_LANG['backup']['labelDelete']           = 'Delete Backup: ';
$_LANG['backup']['labelRestore']          = 'Restore Backup: ';
$_LANG['backup']['description']          = 'Secure your system by creating backups. In case of any failure, you can restore your VPS to one of the previously created backups. ';

$_LANG['graph']['description'] ="Your VPS usage over time can be viewed here. If some graphs are close to their maximum value, consider a VPS upgrade.";
$_LANG['task']['description'] ="Any action set to be executed on the server along with its status is displayed here.";

$_LANG['network']['description'] ='Brief information about network interfaces used by your VPS.';
$_LANG['firewall']['description'] ='Define firewall rules or enable the security group for your VPS.';
$_LANG['network']['labelDelete']           = 'Delete Network: ';
$_LANG['snapshots']['labelDelete']           = 'Delete Snapshot: ';
$_LANG['snapshots']['labelRollback']           = 'Rollback: ';
$_LANG['firewall']['labelDelete']           = 'Delete: ';
$_LANG['disks']['labelDelete']           = 'Delete: ';
$_LANG['home']['swap']	   = 'SWAP';
$_LANG['task']['error_details'] = 'Error Details';
$_LANG['snapshots']['descriptionLong'] = "A snapshot includes the content of the virtual machine memory, virtual machine settings, and the state of all the virtual disks. When you roll back to a snapshot, you restore the memory, virtual disks and all settings of the virtual machine to the state they were in when you took the snapshot.";
$_LANG['snapshots']['update'] = 'Update:';
$_LANG['snapshots']['updated'] = 'Snapshot has been updated';

$_LANG['snapshots']['status'] = 'Status';
$_LANG['home']['description'] = 'View server details and resource usage.';
$_LANG['disks']['description'] ='View and manage hard disk within defined limits. Any additional disk can be removed at will.';

$_LANG['CAPages']['home']['Boot Order has been successfully changed'] ='Boot order has been successfully changed';
$_LANG['CAPages']['home']['Edit: Boot Order'] ='Edit: Boot Order';
$_LANG['CAPages']['home']['Edit: CD/DVD'] ='Edit: CD/DVD';
$_LANG['CAPages']['home']['Image File'] ='Image File';
$_LANG['CAPages']['home']['ISO Image has been successfully changed']='ISO Image has been successfully changed';
$_LANG['CAPages']['reinstall']['Reinstall task currently in progress...']='Task reinstallation currently in progress...';
$_LANG['CAPages']['reinstall']['Password is too short']='Password is too short';
$_LANG['CAPages']['reinstall']['Provide OS Template.']='Provide OS Template.';
$_LANG['Creating VM in progress. Please try again later.']='VM creation in progress. Please try again later.';
$_LANG["Do you really want to boot?"]="Do you really want to boot?";
$_LANG["Do you really want to reboot?"]="Do you really want to reboot?";
$_LANG["Do you really want to stop?"]="Do you really want to stop?";
$_LANG["Do you really want to shutdown?"]="Do you really want to shut down?";
$_LANG["Login to Proxmox Host failed"]="Node connection failed. Please contact support";
