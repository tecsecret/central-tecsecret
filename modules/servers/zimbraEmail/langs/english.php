<?php

$_LANG['token']                  = ', Error Token:';
$_LANG['generalError']           = 'Something has gone wrong. Check the logs and contact the administrator.';
$_LANG['generalErrorClientArea'] = 'Something has gone wrong. Contact the administrator.';
$_LANG['permissionsStorage']     = ':storage_path: settings are not sufficient. Please set up permissions to the \'storage\' folder as writable.';
$_LANG['undefinedAction']        = 'Undefined Action';
$_LANG['changesHasBeenSaved']    = 'Changes have been saved successfully';
$_LANG['Monthly']                = 'Monthly';
$_LANG['Free Account']           = 'Free Account';
$_LANG['Passwords']              = 'Passwords';
$_LANG['Nothing to display']     = 'Nothing to display';
$_LANG['Search']                 = 'Search';
$_LANG['Previous']               = 'Previous';
$_LANG['Next']                   = 'Next';
$_LANG['searchPlacecholder']     = 'Search...';

$_LANG['noDataAvalible']                 = 'No Data Available';
$_LANG['datatableItemsSelected']         = 'Items Selected';
$_LANG['validationErrors']['emptyField'] = 'This field cannot be empty';
$_LANG['bootstrapswitch']['disabled']    = 'Disabled';
$_LANG['bootstrapswitch']['enabled']     = 'Enabled';

$_LANG['addonCA']['pageNotFound']['title'] = 'No page has been found';
$_LANG['addonCA']['pageNotFound']['description'] = 'The provided URL does not exist on this page. If you are sure that an error occurred here, please contact support.';
$_LANG['addonCA']['pageNotFound']['button'] = 'Return to the previous page';

$_LANG['addonCA']['errorPage']['title'] = 'AN ERROR OCCURRED';
$_LANG['addonCA']['errorPage']['description'] = 'An error occurred. Please contact the administrator and pass the details:';
$_LANG['addonCA']['errorPage']['button'] = 'Return to the previous page';
$_LANG['addonCA']['errorPage']['error'] = 'ERROR';

$_LANG['addonCA']['errorPage']['errorCode'] = 'Error Code';
$_LANG['addonCA']['errorPage']['errorToken'] = 'Error Token';
$_LANG['addonCA']['errorPage']['errorTime'] = 'Time';
$_LANG['addonCA']['errorPage']['errorMessage'] = 'Message';

$_LANG['addonAA']['pageNotFound']['title'] = 'PAGE NOT FOUND';
$_LANG['addonAA']['pageNotFound']['description'] = 'An error occurred. Please contact the administrator.';
$_LANG['addonAA']['pageNotFound']['button'] = 'Return to the previous page';

$_LANG['addonAA']['errorPage']['title'] = 'AN ERROR OCCURRED';
$_LANG['addonAA']['errorPage']['description'] = 'An error occurred. Please contact the administrator and pass the details:';
$_LANG['addonAA']['errorPage']['button'] = 'Return to the previous page';
$_LANG['addonAA']['errorPage']['error'] = 'ERROR';

$_LANG['addonAA']['errorPage']['errorCode'] = 'Error Code';
$_LANG['addonAA']['errorPage']['errorToken'] = 'Error Token';
$_LANG['addonAA']['errorPage']['errorTime'] = 'Time';
$_LANG['addonAA']['errorPage']['errorMessage'] = 'Message';

/* * ********************************************************************************************************************
 *                                                   ERROR CODES                                                        *
 * ******************************************************************************************************************** */
$_LANG['errorCodeMessage']['Uncategorised error occured'] = 'Unexpected Error';
$_LANG['errorCodeMessage']['Database error'] = 'Database error';
$_LANG['errorCodeMessage']['Provided controller does not exists'] = 'Page not found';
$_LANG['errorCodeMessage']['Invalid Error Code!'] = 'Unexpected Error';

/* * ********************************************************************************************************************
 *                                                   MODULE REQUIREMENTS                                                *
 * ******************************************************************************************************************** */
$_LANG['unfulfilledRequirement']['In order for the module to work correctly, please remove the following file: :remove_file_requirement:'] = 'In order for the module to work correctly, please remove the following file: :remove_file_requirement:';

/* * ********************************************************************************************************************
 *                                                   ADMIN AREA                                                         *
 * ******************************************************************************************************************** */

$_LANG['addonAA']['datatables']['next']                                                                        = 'Next';
$_LANG['addonAA']['datatables']['previous']                                                                    = 'Previous';
$_LANG['addonAA']['datatables']['zeroRecords']                                                                 = 'Nothing to display';
$_LANG['formValidationError']                                                                                  = 'A form validation error has occurred';
$_LANG['FormValidators']['thisFieldCannotBeEmpty']                                                             = 'This field cannot be empty';
$_LANG['FormValidators']['incorrectEmailAddress']                                                              = 'The provided email is not valid';
$_LANG['FormValidators']['incorrectEmailAddresses']                                                            = 'Provided emails are invalid: :emails: ';
$_LANG['FormValidators']['PleaseProvideANumericValue']                                                         = 'Please provide a numeric value';
$_LANG['FormValidators']['PleaseProvideANumericValueBetween']                                                  = 'Please provide a numeric value between :minValue: and :maxValue:';

// -------------------------------------------------> MENU <--------------------------------------------------------- //
$_LANG['changesSaved']                             = 'Changes Have Been Saved Successfully';
$_LANG['ItemNotFound']                             = 'Item Not Found';
$_LANG['formValidationError']                                 = 'Form Validation Error';
$_LANG['FormValidators']['thisFieldCannotBeEmpty']            = 'This Field Cannot Be Empty';
$_LANG['FormValidators']['PleaseProvideANumericValue']        = 'Please Provide A Numeric Value';
$_LANG['FormValidators']['PleaseProvideANumericValueBetween'] = 'Please Provide A Numeric Value Between :minValue: AND :maxValue:';

/* * ********************************************************************************************************************
 *                                                    CLIENT AREA                                                      *
 * ******************************************************************************************************************** */


/* * ********************************************************************************************************************
 *                                                    ZIMBRA MODULE                                                    *
 * ******************************************************************************************************************** */



$_LANG['errorCodeMessage']['Class does not exist'] = 'Class does not exist';
$_LANG['addonAA']['pagesLabels']['label']['home'] = 'Home';
$_LANG['addonAA']['pagesLabels']['label']['LoggerManager'] = 'LoggerManager';
$_LANG['addonAA']['pagesLabels']['label']['documentation'] = 'Documentation';

$_LANG['mainContainer']['configForm']['configurableOptions']['configurableOptions'] = 'Configurable Options';


$_LANG['mainContainer']['configForm']['zimbraSettings']['zimbraSettings'] = 'Zimbra Settings';
$_LANG['mainContainer']['configForm']['zimbraSettings']['leftSide']['acc_limit']['acc_limit'] = 'Default Email Accounts Limit';
$_LANG['mainContainer']['configForm']['zimbraSettings']['leftSide']['acc_limit']['description'] = 'Type -1 for unlimited (used when the configurable option is not provided).';

$_LANG['mainContainer']['configForm']['zimbraSettings']['leftSide']['alias_limit']['alias_limit'] = 'Default Email Aliases Limit';
$_LANG['mainContainer']['configForm']['zimbraSettings']['leftSide']['alias_limit']['description'] = 'Type -1 for unlimited (used when the configurable option is not provided).';

$_LANG['mainContainer']['configForm']['zimbraSettings']['leftSide']['cos_name']['cos_name'] = 'Class Of Service Name';
$_LANG['mainContainer']['configForm']['zimbraSettings']['leftSide']['cos_name']['description'] = 'Choose the correct class of the service.';

$_LANG['mainContainer']['configForm']['zimbraSettings']['leftSide']['login_link']['login_link'] = 'Webmail Login Link';
$_LANG['mainContainer']['configForm']['zimbraSettings']['leftSide']['login_link']['description'] = 'Leave empty if you want a URL to be generated from a server.';

$_LANG['mainContainer']['configForm']['zimbraSettings']['leftSide']['filterAccountsByCOS']['filterAccountsByCOS'] = 'Filter Accounts By Class Of Service';
$_LANG['mainContainer']['configForm']['zimbraSettings']['leftSide']['filterAccountsByCOS']['description'] = 'If enabled, accounts will be filtered by a class of the service type.';

$_LANG['mainContainer']['configForm']['zimbraSettings']['rightSide']['acc_size']['acc_size'] = 'Default Email Account Size (MB)';
$_LANG['mainContainer']['configForm']['zimbraSettings']['rightSide']['acc_size']['description'] = 'Type -1 for unlimited (used when the configurable option is not provided).';

$_LANG['mainContainer']['configForm']['zimbraSettings']['rightSide']['domain_alias_limit']['domain_alias_limit'] = 'Default Domain Aliases Limit';
$_LANG['mainContainer']['configForm']['zimbraSettings']['rightSide']['domain_alias_limit']['description'] = 'Type -1 for unlimited (used when the configurable option is not provided).';

$_LANG['mainContainer']['configForm']['zimbraSettings']['rightSide']['dist_list_limit']['dist_list_limit'] = 'Default Distribution Lists Limit';
$_LANG['mainContainer']['configForm']['zimbraSettings']['rightSide']['dist_list_limit']['description'] = 'Type -1 for unlimited (used when the configurable option is not provided).';

$_LANG['mainContainer']['configForm']['zimbraSettings']['rightSide']['domainMaxSize']['domainMaxSize'] = 'Domain Max Size (MB)';
$_LANG['mainContainer']['configForm']['zimbraSettings']['rightSide']['domainMaxSize']['description'] = 'It is a sum off all accounts within a domain. Type -1 for unlimited. Note that when the limit is exceeded, the possibility to add new mailboxes is blocked.';

$_LANG['mainContainer']['configForm']['essentialFeatures']['essentialFeatures'] = 'Essential Features';

$_LANG['mainContainer']['configForm']['essentialFeatures']['essentialFeaturesLeft']['zimbraFeatureMailEnabled']['zimbraFeatureMailEnabled'] = 'E-Mail';
$_LANG['mainContainer']['configForm']['essentialFeatures']['essentialFeaturesLeft']['zimbraFeatureMailEnabled']['description'] = 'Description';

$_LANG['mainContainer']['configForm']['essentialFeatures']['essentialFeaturesLeft']['zimbraFeatureCalendarEnabled']['zimbraFeatureCalendarEnabled'] = 'Contacts';
$_LANG['mainContainer']['configForm']['essentialFeatures']['essentialFeaturesLeft']['zimbraFeatureCalendarEnabled']['description'] = 'Description';

$_LANG['mainContainer']['configForm']['essentialFeatures']['essentialFeaturesLeft']['zimbraFeatureBriefcasesEnabled']['zimbraFeatureBriefcasesEnabled'] = 'Calendar';
$_LANG['mainContainer']['configForm']['essentialFeatures']['essentialFeaturesLeft']['zimbraFeatureBriefcasesEnabled']['description'] = 'Description';

$_LANG['mainContainer']['configForm']['essentialFeatures']['essentialFeaturesRight']['zimbraFeatureContactsEnabled']['zimbraFeatureContactsEnabled'] = 'Tasks';
$_LANG['mainContainer']['configForm']['essentialFeatures']['essentialFeaturesRight']['zimbraFeatureContactsEnabled']['description'] = 'Description';

$_LANG['mainContainer']['configForm']['essentialFeatures']['essentialFeaturesRight']['zimbraFeatureTasksEnabled']['zimbraFeatureTasksEnabled'] = 'Briefcase';
$_LANG['mainContainer']['configForm']['essentialFeatures']['essentialFeaturesRight']['zimbraFeatureTasksEnabled']['description'] = 'Description';

$_LANG['mainContainer']['configForm']['essentialFeatures']['essentialFeaturesRight']['zimbraFeatureOptionsEnabled']['zimbraFeatureOptionsEnabled'] = 'Preferences';
$_LANG['mainContainer']['configForm']['essentialFeatures']['essentialFeaturesRight']['zimbraFeatureOptionsEnabled']['description'] = 'Description';


$_LANG['mainContainer']['configForm']['generalFeatures']['generalFeatures'] = 'General Features';

$_LANG['mainContainer']['configForm']['generalFeatures']['generalFeaturesLeft']['zimbraFeatureTaggingEnabled']['zimbraFeatureTaggingEnabled'] = 'Tagging';
$_LANG['mainContainer']['configForm']['generalFeatures']['generalFeaturesRight']['zimbraFeatureSharingEnabled']['zimbraFeatureSharingEnabled'] = 'Sharing';
$_LANG['mainContainer']['configForm']['generalFeatures']['generalFeaturesLeft']['zimbraFeatureChangePasswordEnabled']['zimbraFeatureChangePasswordEnabled'] = 'Change Password';
$_LANG['mainContainer']['configForm']['generalFeatures']['generalFeaturesRight']['zimbraFeatureSkinChangeEnabled']['zimbraFeatureSkinChangeEnabled'] = 'Skin Change';
$_LANG['mainContainer']['configForm']['generalFeatures']['generalFeaturesLeft']['zimbraFeatureManageZimlets']['zimbraFeatureManageZimlets'] = 'Zimlets Management';
$_LANG['mainContainer']['configForm']['generalFeatures']['generalFeaturesRight']['zimbraFeatureHtmlComposeEnabled']['zimbraFeatureHtmlComposeEnabled'] = 'HTML Compose';
$_LANG['mainContainer']['configForm']['generalFeatures']['generalFeaturesLeft']['zimbraFeatureGalEnabled']['zimbraFeatureGalEnabled'] = 'Global Address List Access';
$_LANG['mainContainer']['configForm']['generalFeatures']['generalFeaturesRight']['zimbraFeatureMAPIConnectorEnabled']['zimbraFeatureMAPIConnectorEnabled'] = 'MAPI (Microsoft Outlook) Connector';
$_LANG['mainContainer']['configForm']['generalFeatures']['generalFeaturesLeft']['zimbraFeatureEwsEnabled']['zimbraFeatureEwsEnabled'] = 'EWS Client';
$_LANG['mainContainer']['configForm']['generalFeatures']['generalFeaturesRight']['zimbraFeatureTouchClientEnabled']['zimbraFeatureTouchClientEnabled'] = 'Mobile Client';
$_LANG['mainContainer']['configForm']['generalFeatures']['generalFeaturesLeft']['zimbraFeatureWebClientOfflineAccessEnabled']['zimbraFeatureWebClientOfflineAccessEnabled'] = 'Web Client Offline Access';
$_LANG['mainContainer']['configForm']['generalFeatures']['generalFeaturesRight']['zimbraFeatureGalAutoCompleteEnabled']['zimbraFeatureGalAutoCompleteEnabled'] = 'Global Access List Autocomplete';
$_LANG['mainContainer']['configForm']['generalFeatures']['generalFeaturesLeft']['zimbraFeatureImportFolderEnabled']['zimbraFeatureImportFolderEnabled'] = 'Import';
$_LANG['mainContainer']['configForm']['generalFeatures']['generalFeaturesRight']['zimbraFeatureExportFolderEnabled']['zimbraFeatureExportFolderEnabled'] = 'Export';
$_LANG['mainContainer']['configForm']['generalFeatures']['generalFeaturesLeft']['zimbraDumpsterEnabled']['zimbraDumpsterEnabled'] = 'Dumpster';
$_LANG['mainContainer']['configForm']['generalFeatures']['generalFeaturesRight']['zimbraDumpsterPurgeEnabled']['zimbraDumpsterPurgeEnabled'] = 'Dumpster Purge';


$_LANG['mainContainer']['configForm']['mailServiceFeatures']['mailServiceFeatures'] = 'Mail Service Features';
$_LANG['mainContainer']['configForm']['mailServiceFeatures']['left']['zimbraFeatureMailPriorityEnabled']['zimbraFeatureMailPriorityEnabled'] = 'Mail Priority';
$_LANG['mainContainer']['configForm']['mailServiceFeatures']['left']['zimbraImapEnabled']['zimbraImapEnabled'] = 'IMAP Access';
$_LANG['mainContainer']['configForm']['mailServiceFeatures']['left']['zimbraFeatureImapDataSourceEnabled']['zimbraFeatureImapDataSourceEnabled'] = 'IMAP Data Source';
$_LANG['mainContainer']['configForm']['mailServiceFeatures']['left']['zimbraFeatureMailSendLaterEnabled']['zimbraFeatureMailSendLaterEnabled'] = 'Send Mail Later';
$_LANG['mainContainer']['configForm']['mailServiceFeatures']['left']['zimbraFeatureFiltersEnabled']['zimbraFeatureFiltersEnabled'] = 'Mail Filter';
$_LANG['mainContainer']['configForm']['mailServiceFeatures']['left']['zimbraFeatureNewMailNotificationEnabled']['zimbraFeatureNewMailNotificationEnabled'] = 'New Mail Notification';
$_LANG['mainContainer']['configForm']['mailServiceFeatures']['left']['zimbraFeatureReadReceiptsEnabled']['zimbraFeatureReadReceiptsEnabled'] = 'Read Receipts';
$_LANG['mainContainer']['configForm']['mailServiceFeatures']['right']['zimbraFeatureFlaggingEnabled']['zimbraFeatureFlaggingEnabled'] = 'Flagging';
$_LANG['mainContainer']['configForm']['mailServiceFeatures']['right']['zimbraPop3Enabled']['zimbraPop3Enabled'] = 'POP3 Access';
$_LANG['mainContainer']['configForm']['mailServiceFeatures']['right']['zimbraFeaturePop3DataSourceEnabled']['zimbraFeaturePop3DataSourceEnabled'] = 'POP3 Data Source';
$_LANG['mainContainer']['configForm']['mailServiceFeatures']['right']['zimbraFeatureConversationsEnabled']['zimbraFeatureConversationsEnabled'] = 'Conversations';
$_LANG['mainContainer']['configForm']['mailServiceFeatures']['right']['zimbraFeatureOutOfOfficeReplyEnabled']['zimbraFeatureOutOfOfficeReplyEnabled'] = 'Out Of Office Reply';
$_LANG['mainContainer']['configForm']['mailServiceFeatures']['right']['zimbraFeatureIdentitiesEnabled']['zimbraFeatureIdentitiesEnabled'] = 'Identities';

$_LANG['mainContainer']['configForm']['contactFeatures']['contactFeatures'] = 'Contact Features';

$_LANG['mainContainer']['configForm']['contactFeatures']['left']['zimbraFeatureDistributionListFolderEnabled']['zimbraFeatureDistributionListFolderEnabled'] = 'Distribution List Folder';

$_LANG['mainContainer']['configForm']['calendarFeatures']['calendarFeatures'] = 'Calendar Features';

$_LANG['mainContainer']['configForm']['calendarFeatures']['left']['zimbraFeatureGroupCalendarEnabled']['zimbraFeatureGroupCalendarEnabled'] = 'Group Calendar';
$_LANG['mainContainer']['configForm']['calendarFeatures']['right']['zimbraFeatureCalendarReminderDeviceEmailEnabled']['zimbraFeatureCalendarReminderDeviceEmailEnabled'] = 'Reminders';

$_LANG['mainContainer']['configForm']['searchFeatures']['searchFeatures'] = 'Search Features';

$_LANG['mainContainer']['configForm']['searchFeatures']['left']['zimbraFeatureAdvancedSearchEnabled']['zimbraFeatureAdvancedSearchEnabled'] = 'Advanced Search';
$_LANG['mainContainer']['configForm']['searchFeatures']['left']['zimbraFeatureInitialSearchPreferenceEnabled']['zimbraFeatureInitialSearchPreferenceEnabled'] = 'Initial Search Preferences';
$_LANG['mainContainer']['configForm']['searchFeatures']['right']['zimbraFeatureSavedSearchesEnabled']['zimbraFeatureSavedSearchesEnabled'] = 'Saved Searches';
$_LANG['mainContainer']['configForm']['searchFeatures']['right']['zimbraFeaturePeopleSearchEnabled']['zimbraFeaturePeopleSearchEnabled'] = 'People Search';

$_LANG['mainContainer']['configForm']['mimeFeatures']['mimeFeatures'] = 'S/MIME Features';

$_LANG['mainContainer']['configForm']['mimeFeatures']['left']['zimbraFeatureSMIMEEnabled']['zimbraFeatureSMIMEEnabled'] = 'Enable S/MIME';

$_LANG['mainContainer']['configForm']['classOfServiceFeatures']['classOfServiceFeatures'] = 'Class Of Service For Mailbox Accounts';

$_LANG['errorCodeMessage']['Class does not exist'] = 'errorCodeMessage Class does not exist';
$_LANG['addonAA']['configuration']['product']['zimbra']['cos']['Use Custom Settings'] = 'Use Custom Settings';
$_LANG['addonAA']['configuration']['product']['zimbra']['cos']['Allow clients to choose Class Of Service'] = 'Allow clients to choose a class of the service';
$_LANG['addonAA']['configuration']['product']['zimbra']['cos']['Allow clients to choose Class Of Service Quota Per Account'] = 'Allow clients to choose a class of the service quota per account';

$_LANG['Average Size Accounts'] = 'Average Size Accounts';
$_LANG['Extreme Size Accounts'] = 'Extreme Size Accounts';
$_LANG['Large Size Accounts'] = 'Large Size Accounts';
$_LANG['Mini Size Accounts'] = 'Mini Size Accounts';
$_LANG['Small Size Accounts'] = 'Small Size Accounts';
$_LANG['default'] = 'default';
$_LANG['defaultExternal'] = 'defaultExternal';
$_LANG['Enter to limit an accounts number of %s with quota %s MB or set -1 to unlimited'] = 'Enter to limit the accounts number of %s with quota %s MB or set -1 for unlimited.';

/**
 * Config Options
 *
 */
$_LANG['COS %s with quota %s MB'] = 'COS %s with quota %s MB';
$_LANG['COS %s MB'] = 'COS %s MB';

$_LANG['mainContainer']['configForm']['configurableOptions']['createCOBaseModalButton']['button']['createCOBaseModalButton'] = 'Create Configurable Options';
$_LANG['createCOConfirmModal']['modal']['createCOConfirmModal'] = 'Create Configurable Options';
$_LANG['createCOConfirmModal']['baseAcceptButton']['title'] = 'Create';
$_LANG['createCOConfirmModal']['baseCancelButton']['title'] = 'Cancel';

$_LANG['mainContainer']['configForm']['clientAreaFeatures']['clientAreaFeatures'] = 'Client Area Pages';
$_LANG['mainContainer']['configForm']['clientAreaFeatures']['clientAreaFeaturesLeft']['ca_emailAccountPage']['ca_emailAccountPage'] = 'Email Accounts';
$_LANG['mainContainer']['configForm']['clientAreaFeatures']['clientAreaFeaturesLeft']['ca_distributionListPage']['ca_distributionListPage'] = 'Distribution Lists';
$_LANG['mainContainer']['configForm']['clientAreaFeatures']['clientAreaFeaturesLeft']['ca_goToWebmailPage']['ca_goToWebmailPage'] = 'Go To Webmail';
$_LANG['mainContainer']['configForm']['clientAreaFeatures']['clientAreaFeaturesRight']['ca_emailAliasesPage']['ca_emailAliasesPage'] = 'Email Aliases';
$_LANG['mainContainer']['configForm']['clientAreaFeatures']['clientAreaFeaturesRight']['ca_domainAliasesPage']['ca_domainAliasesPage'] = 'Domain Aliases';
$_LANG['mainContainer']['configForm']['clientAreaFeatures']['clientAreaFeaturesRight']['ca_logInToMailboxButton']['ca_logInToMailboxButton'] = 'Log In To Mailbox';



/**
 *
 * CLIENT AREA
 *
 */

$_LANG['addonCA']['homePage']['manageHeader'] = 'Zimbra Email Management';
$_LANG['addonCA']['homeIcons']['emailAccount'] = 'Email Accounts';
$_LANG['addonCA']['homeIcons']['emailAlias'] = 'Email Aliases';
$_LANG['addonCA']['homeIcons']['distributionList'] = 'Distribution Lists';
$_LANG['addonCA']['homeIcons']['domainAlias'] = 'Domain Aliases';
$_LANG['addonCA']['homeIcons']['goWebmail'] = 'Go To Webmail';
$_LANG['management']        = 'Management';
$_LANG['emailAccount']      = 'Email Accounts';
$_LANG['emailAlias'] = 'Email Aliases';
$_LANG['distributionList'] = 'Distribution Lists';
$_LANG['domainAlias'] = 'Domain Aliases';
$_LANG['goWebmail'] = 'Go To Webmail';

$_LANG['addonCA']['emailAccount']['mainContainer']['emailAccount']['emailAccountPageTitle'] = 'Email Accounts Management';
$_LANG['addonCA']['emailAccount']['mainContainer']['emailAccount']['emailAccountPageDescription'] = 'In this area you can manage email accounts associated with your domains.';

$_LANG['addonCA']['breadcrumbs']['MG Demo'] = 'addonCA breadcrumbs MG Demo';
$_LANG['addonCA']['emailAccount']['mainContainer']['accounts']['addAccountButton']['button']['addAccountButton'] = 'Add Mailbox';
$_LANG['addonCA']['emailAccount']['mainContainer']['accounts']['table']['mailbox'] = 'Mailbox';
$_LANG['addonCA']['emailAccount']['mainContainer']['accounts']['table']['date_created'] = 'Date Created';
$_LANG['addonCA']['emailAccount']['mainContainer']['accounts']['table']['last_login'] = 'Last Login';
$_LANG['addonCA']['emailAccount']['mainContainer']['accounts']['table']['quota'] = 'Quota (MB)';
$_LANG['addonCA']['emailAccount']['mainContainer']['accounts']['table']['status'] = 'Status';
$_LANG['addonCA']['emailAccount']['mainContainer']['accounts']['editAccountButton']['button']['editAccountButton'] = 'Edit';
$_LANG['addonCA']['emailAccount']['mainContainer']['accounts']['deleteAccountButton']['button']['deleteAccountButton'] = 'Delete';

$_LANG['addonCA']['emailAccount']['addAccountModal']['modal']['addAccountModal'] = 'Add Email Account';
$_LANG['addonCA']['emailAccount']['addAccountModal']['generalSection'] = 'General';
$_LANG['addonCA']['emailAccount']['addAccountModal']['additionalSection'] = 'Additional Information';
$_LANG['addonCA']['emailAccount']['addAccountForm']['generalSection']['generated_row_section_0']['generated_0_0']['firstname']['firstname'] = 'First Name';
$_LANG['addonCA']['emailAccount']['addAccountForm']['generalSection']['generated_row_section_0']['generated_0_1']['lastname']['lastname'] = 'Last Name';
$_LANG['addonCA']['emailAccount']['addAccountForm']['generalSection']['usernameGroup']['usernameGroup'] = 'Username *';
$_LANG['addonCA']['emailAccount']['addAccountForm']['generalSection']['generated_row_section_1']['generated_1_0']['display_name']['display_name'] = 'Display Name';
$_LANG['addonCA']['emailAccount']['addAccountForm']['generalSection']['generated_row_section_1']['generated_1_1']['status']['status'] = 'Status';
$_LANG['addonCA']['emailAccount']['addAccountForm']['generalSection']['generated_row_section_2']['generated_2_0']['password']['password'] = 'Password *';
$_LANG['addonCA']['emailAccount']['addAccountForm']['generalSection']['generated_row_section_2']['generated_2_1']['repeat_password']['repeat_password'] = 'Repeat Password *';
$_LANG['addonCA']['emailAccount']['addAccountForm']['additionalSection']['generated_row_section_0']['generated_0_0']['company']['company'] = 'Company';
$_LANG['addonCA']['emailAccount']['addAccountForm']['additionalSection']['generated_row_section_0']['generated_0_1']['title']['title'] = 'Title';
$_LANG['addonCA']['emailAccount']['addAccountForm']['additionalSection']['phone']['phone'] = 'Phone';
$_LANG['addonCA']['emailAccount']['addAccountForm']['additionalSection']['generated_row_section_1']['generated_1_0']['home_phone']['home_phone'] = 'Home Phone';
$_LANG['addonCA']['emailAccount']['addAccountForm']['additionalSection']['generated_row_section_1']['generated_1_1']['mobile_phone']['mobile_phone'] = 'Mobile Phone';
$_LANG['addonCA']['emailAccount']['addAccountForm']['additionalSection']['generated_row_section_2']['generated_2_0']['fax']['fax'] = 'Fax';
$_LANG['addonCA']['emailAccount']['addAccountForm']['additionalSection']['generated_row_section_2']['generated_2_1']['pager']['pager'] = 'Pager';
$_LANG['addonCA']['emailAccount']['addAccountForm']['additionalSection']['generated_row_section_3']['generated_3_0']['country']['country'] = 'Country';
$_LANG['addonCA']['emailAccount']['addAccountForm']['additionalSection']['generated_row_section_3']['generated_3_1']['state']['state'] = 'State';
$_LANG['addonCA']['emailAccount']['addAccountForm']['additionalSection']['generated_row_section_4']['generated_4_0']['city']['city'] = 'City';
$_LANG['addonCA']['emailAccount']['addAccountForm']['additionalSection']['generated_row_section_4']['generated_4_1']['street']['street'] = 'Street';
$_LANG['addonCA']['emailAccount']['addAccountForm']['additionalSection']['post_code']['post_code'] = 'Postal Code';
$_LANG['addonCA']['emailAccount']['addAccountModal']['baseAcceptButton']['title'] = 'Confirm';
$_LANG['addonCA']['emailAccount']['addAccountModal']['baseCancelButton']['title'] = 'Cancel';

$_LANG['zimbra']['account']['status']['active'] = 'Active';
$_LANG['zimbra']['account']['status']['locked'] = 'Locked';
$_LANG['zimbra']['account']['status']['maintenance'] = 'Maintenance';
$_LANG['zimbra']['account']['status']['closed'] = 'Closed';
$_LANG['zimbra']['account']['status']['lockout'] = 'Lockout';
$_LANG['zimbra']['account']['status']['pending'] = 'Pending';

$_LANG['FormValidators']['passwordsIsNotTheSame'] = 'Passwords must be the same';

$_LANG['emailAccountHasBeenAdded'] = 'The email account has been created successfully';
$_LANG['emailAccountHasBeenUpdated'] = 'The email account has been updated successfully';
$_LANG['emailAccountHasBeenDeleted'] = 'The email account has been deleted successfully';

$_LANG['addonCA']['emailAccount']['editAccountModal']['editGeneralSection'] = 'General';
$_LANG['addonCA']['emailAccount']['editAccountModal']['editAdditionalSection'] = 'Additional Information';
$_LANG['addonCA']['emailAccount']['editAccountModal']['modal']['editAccountModal'] = 'Edit Account';
$_LANG['addonCA']['emailAccount']['editAccountForm']['editGeneralSection']['generated_row_section_0']['generated_0_0']['firstname']['firstname'] = 'First Name';
$_LANG['addonCA']['emailAccount']['editAccountForm']['editGeneralSection']['generated_row_section_0']['generated_0_1']['lastname']['lastname'] = 'Last Name';
$_LANG['addonCA']['emailAccount']['editAccountForm']['editGeneralSection']['usernameGroup']['usernameGroup'] = 'Username';
$_LANG['addonCA']['emailAccount']['editAccountForm']['editGeneralSection']['generated_row_section_1']['generated_1_0']['display_name']['display_name'] = 'Display Name';
$_LANG['addonCA']['emailAccount']['editAccountForm']['editGeneralSection']['generated_row_section_1']['generated_1_1']['status']['status'] = 'Status';
$_LANG['addonCA']['emailAccount']['editAccountForm']['editAdditionalSection']['generated_row_section_0']['generated_0_0']['company']['company'] = 'Company';
$_LANG['addonCA']['emailAccount']['editAccountForm']['editAdditionalSection']['generated_row_section_0']['generated_0_1']['title']['title'] = 'Title';
$_LANG['addonCA']['emailAccount']['editAccountForm']['editAdditionalSection']['phone']['phone'] = 'Phone';
$_LANG['addonCA']['emailAccount']['editAccountForm']['editAdditionalSection']['generated_row_section_1']['generated_1_0']['home_phone']['home_phone'] = 'Home Phone';
$_LANG['addonCA']['emailAccount']['editAccountForm']['editAdditionalSection']['generated_row_section_1']['generated_1_1']['mobile_phone']['mobile_phone'] = 'Mobile Phone';
$_LANG['addonCA']['emailAccount']['editAccountForm']['editAdditionalSection']['generated_row_section_2']['generated_2_0']['fax']['fax'] = 'Fax';
$_LANG['addonCA']['emailAccount']['editAccountForm']['editAdditionalSection']['generated_row_section_2']['generated_2_1']['pager']['pager'] = 'Pager';
$_LANG['addonCA']['emailAccount']['editAccountForm']['editAdditionalSection']['generated_row_section_3']['generated_3_0']['country']['country'] = 'Country';
$_LANG['addonCA']['emailAccount']['editAccountForm']['editAdditionalSection']['generated_row_section_3']['generated_3_1']['state']['state'] = 'State';
$_LANG['addonCA']['emailAccount']['editAccountForm']['editAdditionalSection']['generated_row_section_4']['generated_4_0']['city']['city'] = 'City';
$_LANG['addonCA']['emailAccount']['editAccountForm']['editAdditionalSection']['generated_row_section_4']['generated_4_1']['street']['street'] = 'Street';
$_LANG['addonCA']['emailAccount']['editAccountForm']['editAdditionalSection']['post_code']['post_code'] = 'Postal Code';
$_LANG['addonCA']['emailAccount']['editAccountModal']['baseAcceptButton']['title'] = 'Confirm';
$_LANG['addonCA']['emailAccount']['editAccountModal']['baseCancelButton']['title'] = 'Cancel';

$_LANG['addonCA']['emailAccount']['deleteAccountModal']['modal']['deleteAccountModal'] = 'Delete Email Account';
$_LANG['addonCA']['emailAccount']['deleteAccountModal']['deleteAccountForm']['confirmRemoveAccount'] = 'Are you sure that you want to delete this email account?';
$_LANG['addonCA']['emailAccount']['deleteAccountModal']['baseAcceptButton']['title'] = 'Confirm';
$_LANG['addonCA']['emailAccount']['deleteAccountModal']['baseCancelButton']['title'] = 'Cancel';


$_LANG['addonCA']['emailAlias']['mainContainer']['emailAlias']['emailAliasPageTitle'] = 'Email Aliases Management';
$_LANG['addonCA']['emailAlias']['mainContainer']['emailAlias']['emailAliasPageDescription'] = 'Email aliases allow using another address for the same email account. For example you can create an alias for possible spam messages, so you can remove it later to stop receiving such emails.';
$_LANG['addonCA']['emailAlias']['mainContainer']['emailAliases']['addEmailAliasButton']['button']['addEmailAliasButton'] = 'Add Email Alias';
$_LANG['addonCA']['emailAlias']['mainContainer']['emailAliases']['table']['account'] = 'Account';
$_LANG['addonCA']['emailAlias']['mainContainer']['emailAliases']['table']['email_alias'] = 'Email Alias';
$_LANG['addonCA']['emailAlias']['mainContainer']['emailAliases']['deleteEmailAliasButton']['button']['deleteEmailAliasButton'] = 'Delete';

$_LANG['addonCA']['emailAlias']['addEmailAliasModal']['modal']['addEmailAliasModal'] = 'Add Email Alias';
$_LANG['addonCA']['emailAlias']['addEmailAliasModal']['addEmailAliasForm']['usernameGroup']['usernameGroup'] = 'Email Alias';
$_LANG['addonCA']['emailAlias']['addEmailAliasModal']['addEmailAliasForm']['mailbox']['mailbox'] = 'Account';
$_LANG['addonCA']['emailAlias']['addEmailAliasModal']['baseAcceptButton']['title'] = 'Confirm';
$_LANG['addonCA']['emailAlias']['addEmailAliasModal']['baseCancelButton']['title'] = 'Cancel';

$_LANG['emailAliasHasBeenCreated'] = 'The email alias has been created successfully';
$_LANG['emailAliasHasBeenDeleted'] = 'The email alias has been deleted successfully';

$_LANG['addonCA']['emailAlias']['deleteEmailAliasModal']['modal']['deleteEmailAliasModal'] = 'Delete Email Alias';
$_LANG['addonCA']['emailAlias']['deleteEmailAliasModal']['deleteAccountForm']['confirmDeleteAccountAlias'] = 'Are you sure that you want to delete this email alias?';
$_LANG['addonCA']['emailAlias']['deleteEmailAliasModal']['baseAcceptButton']['title'] = 'Confirm';
$_LANG['addonCA']['emailAlias']['deleteEmailAliasModal']['baseCancelButton']['title'] = 'Cancel';

$_LANG['addonCA']['domainAlias']['mainContainer']['domainAlias']['domainAliasPageTitle'] = 'Domain Aliases Management';
$_LANG['addonCA']['domainAlias']['mainContainer']['domainAlias']['domainAliasPageDescription'] = 'Domain aliases allow using another address for the same domain. For example you can create an alias for possible special clients.';
$_LANG['addonCA']['domainAlias']['mainContainer']['lists']['addDomainAliasButton']['button']['addDomainAliasButton'] = 'Add Domain Alias';
$_LANG['addonCA']['domainAlias']['mainContainer']['lists']['table']['name'] = 'Domain Alias';
$_LANG['addonCA']['domainAlias']['mainContainer']['lists']['table']['description'] = 'Description';
$_LANG['addonCA']['domainAlias']['mainContainer']['lists']['deleteDomainAliasButton']['button']['deleteDomainAliasButton'] = 'Delete';

$_LANG['addonCA']['domainAlias']['addDomainAliasModals']['modal']['addDomainAliasModals'] = 'Add Domain Alias';
$_LANG['addonCA']['domainAlias']['addDomainAliasModals']['addDomainAliasForm']['aliasGroup']['aliasGroup'] = 'Domain Alias';
$_LANG['addonCA']['domainAlias']['addDomainAliasModals']['addDomainAliasForm']['description']['description'] = 'Description';
$_LANG['addonCA']['domainAlias']['addDomainAliasModals']['baseAcceptButton']['title'] = 'Confirm';
$_LANG['addonCA']['domainAlias']['addDomainAliasModals']['baseCancelButton']['title'] = 'Cancel';

$_LANG['addonCA']['domainAlias']['deleteDomainAliasModal']['modal']['deleteDomainAliasModal'] = 'Delete Domain Alias';
$_LANG['addonCA']['domainAlias']['deleteDomainAliasModal']['deleteDomainAliasForm']['confirmRemoveDomainAlias'] = 'Are you sure that you want to delete this domain alias?';
$_LANG['addonCA']['domainAlias']['deleteDomainAliasModal']['baseAcceptButton']['title'] = 'Confirm';
$_LANG['addonCA']['domainAlias']['deleteDomainAliasModal']['baseCancelButton']['title'] = 'Cancel';

$_LANG['domainAliasHasBeenCreated'] = 'The domain alias has been created successfully';
$_LANG['domainAliasHasBeenDeleted'] = 'The domain alias has been deleted successfully';

$_LANG['addonCA']['distributionList']['mainContainer']['distributionList']['distributionListPageTitle'] = 'Distribution Lists Management';
$_LANG['addonCA']['distributionList']['mainContainer']['distributionList']['distributionListPageDescription'] = 'A distribution list, also known as a mailing list, is a group of email addresses contained in a list with a common email address. When users send to a distribution list, they are sending the message to everyone whose address is included in the list.';
$_LANG['addonCA']['distributionList']['mainContainer']['lists']['addListButton']['button']['addListButton'] = 'Add Distribution List';
$_LANG['addonCA']['distributionList']['mainContainer']['lists']['table']['email'] = 'Email';
$_LANG['addonCA']['distributionList']['mainContainer']['lists']['table']['name'] = 'Display Name';
$_LANG['addonCA']['distributionList']['mainContainer']['lists']['table']['status'] = 'Status';
$_LANG['addonCA']['distributionList']['mainContainer']['lists']['editListButton']['button']['editListButton'] = 'Edit';
$_LANG['addonCA']['distributionList']['mainContainer']['lists']['deleteListButton']['button']['deleteListButton'] = 'Delete';

$_LANG['addonCA']['distributionList']['addListForm']['addMembersDistribution']['usernameGroup']['usernameGroup'] = 'Email Address';
$_LANG['addonCA']['distributionList']['addListForm']['addMembersDistribution']['displayName']['displayName'] = 'Display Name';
$_LANG['addonCA']['distributionList']['addListForm']['addMembersDistribution']['description']['description'] = 'Description';
$_LANG['addonCA']['distributionList']['addListForm']['addMembersDistribution']['memberList']['memberList'] = 'Members List';
$_LANG['addonCA']['distributionList']['addListForm']['addMembersDistribution']['customMember']['customMember'] = 'Add Member';
$_LANG['addonCA']['distributionList']['addListForm']['addPropertiesDistribution']['receiveMail']['receiveMail'] = 'Can Receive Mail';
$_LANG['addonCA']['distributionList']['addListForm']['addPropertiesDistribution']['hideGal']['hideGal'] = 'Hide In GAL';
$_LANG['addonCA']['distributionList']['addListForm']['addPropertiesDistribution']['dynamicGroup']['dynamicGroup'] = 'Dynamic Group';
$_LANG['addonCA']['distributionList']['addListForm']['addPropertiesDistribution']['subscriptionRequest']['subscriptionRequest'] = 'Subscription Request';
$_LANG['addonCA']['distributionList']['addListForm']['addPropertiesDistribution']['unsubscriptionRequest']['unsubscriptionRequest'] = 'Unsubscription Request';
$_LANG['addonCA']['distributionList']['addListForm']['addPropertiesDistribution']['sharesNotify']['sharesNotify'] = 'Notify About Shares';
$_LANG['addonCA']['distributionList']['addListForm']['addAliasesDistribution']['emailAliases']['emailAliases'] = 'Email Aliases';
$_LANG['addonCA']['distributionList']['addListForm']['addAliasesDistribution']['newAlias']['newAlias'] = 'Add Email Alias';
$_LANG['addonCA']['distributionList']['addListForm']['addOwnersDistribution']['owners']['owners'] = 'Owners';
$_LANG['addonCA']['distributionList']['addListForm']['addOwnersDistribution']['newOwner']['newOwner'] = 'Add Owner';
$_LANG['addonCA']['distributionList']['addListForm']['addPreferencesDistribution']['replyEmail']['replyEmail'] = 'Set Reply To Field';
$_LANG['addonCA']['distributionList']['addListForm']['addPreferencesDistribution']['replyDisplayName']['replyDisplayName'] = '\'Reply To\' Display Name';
$_LANG['addonCA']['distributionList']['addListForm']['addPreferencesDistribution']['replyEmailAddress']['replyEmailAddress'] = '\'Reply To\' Address';
$_LANG['addonCA']['distributionList']['addListModal']['modal']['addListModal'] = 'Add Distribution List';
$_LANG['addonCA']['distributionList']['addListModal']['baseAcceptButton']['title'] = 'Confirm';
$_LANG['addonCA']['distributionList']['addListModal']['baseCancelButton']['title'] = 'Cancel';

$_LANG['domainAliasHasBeenCreated'] = 'The domain alias has been created successfully';
$_LANG['domainAliasHasBeenDeleted'] = 'The domain alias has been deleted successfully';

$_LANG['addonCA']['distributionList']['addListModal']['addMembersDistribution']     = 'Members';
$_LANG['addonCA']['distributionList']['addListModal']['addPropertiesDistribution']  = 'Properties';
$_LANG['addonCA']['distributionList']['addListModal']['addAliasesDistribution']     = 'Email Aliases';
$_LANG['addonCA']['distributionList']['addListModal']['addOwnersDistribution']      = 'Owners';
$_LANG['addonCA']['distributionList']['addListModal']['addPreferencesDistribution'] = 'Preferences';

$_LANG['distributionListHasBeenAdded']       = 'The distribution list has been created successfully';
$_LANG['distributionListHasBeenDeleted']     = 'The distribution list has been deleted successfully';
$_LANG['distributionListHasBeenUpdated']     = 'The distribution list has been updated successfully';

$_LANG['addonCA']['distributionList']['editListForm']['editMembersDistribution']['usernameGroup']['usernameGroup'] = 'Email Address';
$_LANG['addonCA']['distributionList']['editListForm']['editMembersDistribution']['displayName']['displayName'] = 'Display Name';
$_LANG['addonCA']['distributionList']['editListForm']['editMembersDistribution']['description']['description'] = 'Description';
$_LANG['addonCA']['distributionList']['editListForm']['editMembersDistribution']['memberList']['memberList'] = 'Members List';
$_LANG['addonCA']['distributionList']['editListForm']['editMembersDistribution']['customMember']['customMember'] = 'Custom Members';
$_LANG['addonCA']['distributionList']['editListForm']['editMembersDistribution']['emailAliasesActually']['emailAliasesActually'] = 'Email Aliases';
$_LANG['addonCA']['distributionList']['editListForm']['editMembersDistribution']['memberListActually']['memberListActually'] = 'Members List';
$_LANG['addonCA']['distributionList']['editListForm']['editMembersDistribution']['ownersActually']['ownersActually'] = 'Owners';
$_LANG['addonCA']['distributionList']['editListForm']['addPropertiesDistribution']['receiveMail']['receiveMail'] = 'Can Receive Mail';
$_LANG['addonCA']['distributionList']['editListForm']['addPropertiesDistribution']['hideGal']['hideGal'] = 'Hide In GAL';
$_LANG['addonCA']['distributionList']['editListForm']['addPropertiesDistribution']['dynamicGroup']['dynamicGroup'] = 'Dynamic Group';
$_LANG['addonCA']['distributionList']['editListForm']['addPropertiesDistribution']['subscriptionRequest']['subscriptionRequest'] = 'Subscription Request';
$_LANG['addonCA']['distributionList']['editListForm']['addPropertiesDistribution']['unsubscriptionRequest']['unsubscriptionRequest'] = 'Unsubscription Request';
$_LANG['addonCA']['distributionList']['editListForm']['addPropertiesDistribution']['sharesNotify']['sharesNotify'] = 'Notify About Shares';
$_LANG['addonCA']['distributionList']['editListForm']['addAliasesDistribution']['emailAliases']['emailAliases'] = 'Email Aliases';
$_LANG['addonCA']['distributionList']['editListForm']['addAliasesDistribution']['newAlias']['newAlias'] = 'Add Email Alias';
$_LANG['addonCA']['distributionList']['editListForm']['addOwnersDistribution']['owners']['owners'] = 'Owners';
$_LANG['addonCA']['distributionList']['editListForm']['addOwnersDistribution']['newOwner']['newOwner'] = 'Add Owner';
$_LANG['addonCA']['distributionList']['editListForm']['addPreferencesDistribution']['replyEmail']['replyEmail'] = 'Set Reply To Field';
$_LANG['addonCA']['distributionList']['editListForm']['addPreferencesDistribution']['replyDisplayName']['replyDisplayName'] = '\'Reply To\' Display Name';
$_LANG['addonCA']['distributionList']['editListForm']['addPreferencesDistribution']['replyEmailAddress']['replyEmailAddress'] = '\'Reply To\' Address';
$_LANG['addonCA']['distributionList']['editListModal']['modal']['editListModal'] = 'Edit Distribution List';
$_LANG['addonCA']['distributionList']['editListModal']['editMembersDistribution'] = 'Members';
$_LANG['addonCA']['distributionList']['editListModal']['addPropertiesDistribution'] = 'Properties';
$_LANG['addonCA']['distributionList']['editListModal']['addAliasesDistribution'] = 'Email Aliases';
$_LANG['addonCA']['distributionList']['editListModal']['addOwnersDistribution'] = 'Owners';
$_LANG['addonCA']['distributionList']['editListModal']['addPreferencesDistribution'] = 'Preferences';
$_LANG['addonCA']['distributionList']['editListModal']['baseAcceptButton']['title'] = 'Confirm';
$_LANG['addonCA']['distributionList']['editListModal']['baseCancelButton']['title'] = 'Cancel';
$_LANG['addonCA']['distributionList']['deleteListModal']['modal']['deleteListModal'] = 'Delete Distribution List';
$_LANG['addonCA']['distributionList']['deleteListModal']['deleteAccountForm']['confirmRemoveList'] = 'Are you sure that you want to delete this distribution list?';
$_LANG['addonCA']['distributionList']['deleteListModal']['baseAcceptButton']['title'] = 'Confirm';
$_LANG['addonCA']['distributionList']['deleteListModal']['baseCancelButton']['title'] = 'Cancel';

/***
 * Mass Actions
 */
$_LANG['addonCA']['emailAccount']['mainContainer']['accounts']['massChangeStatusButton']['button']['massChangeStatusButton'] = 'Change Status';
$_LANG['addonCA']['emailAccount']['mainContainer']['accounts']['massDeleteAccountButton']['button']['massDeleteAccountButton'] = 'Delete';

$_LANG['addonCA']['emailAccount']['massChangeStatusModal']['modal']['massChangeStatusModal'] = 'Change Status';
$_LANG['addonCA']['emailAccount']['massChangeStatusModal']['deleteAccountForm']['status']['status'] = 'Status';
$_LANG['addonCA']['emailAccount']['massChangeStatusModal']['baseAcceptButton']['title'] = 'Confirm';
$_LANG['addonCA']['emailAccount']['massChangeStatusModal']['baseCancelButton']['title'] = 'Cancel';

$_LANG['massEmailAccountHasBeenDeleted']        = 'The email accounts have been deleted successfully';
$_LANG['massEmailAccountStatusHasBeenUpdated']  = 'The email accounts have been updated successfully';
$_LANG['emailAccountStatusHasBeenUpdated']      = 'The email account has been updated successfully';

$_LANG['addonCA']['emailAccount']['massDeleteAccountModal']['modal']['massDeleteAccountModal'] = 'Delete Email Accounts';
$_LANG['addonCA']['emailAccount']['massDeleteAccountModal']['massDeleteAccountForm']['massDeleteAccountFormConfirm'] = 'Are you sure that you want to delete the selected email accounts?';
$_LANG['addonCA']['emailAccount']['massDeleteAccountModal']['baseAcceptButton']['title'] = 'Confirm';
$_LANG['addonCA']['emailAccount']['massDeleteAccountModal']['baseCancelButton']['title'] = 'Cancel';

$_LANG['addonCA']['emailAlias']['mainContainer']['emailAliases']['massDeleteEmailAliasButton']['button']['massDeleteEmailAliasButton'] = 'Delete';

$_LANG['addonCA']['emailAlias']['massDeleteEmailAliasModal']['modal']['massDeleteEmailAliasModal'] = 'Delete Email Aliases';
$_LANG['addonCA']['emailAlias']['massDeleteEmailAliasModal']['deleteAccountForm']['massDeleteEmailAliasModalConfirm'] = 'Are you sure that you want to delete the selected email aliases?';
$_LANG['addonCA']['emailAlias']['massDeleteEmailAliasModal']['baseAcceptButton']['title'] = 'Confirm';
$_LANG['addonCA']['emailAlias']['massDeleteEmailAliasModal']['baseCancelButton']['title'] = 'Cancel';

$_LANG['massEmailAliasHasBeenDeleted'] = 'The email aliases have been deleted successfully';

$_LANG['addonCA']['distributionList']['mainContainer']['lists']['massDeleteListButton']['button']['massDeleteListButton'] = 'Delete';

$_LANG['addonCA']['distributionList']['massDeleteListModal']['modal']['massDeleteListModal'] = 'Delete Distribution Lists';
$_LANG['addonCA']['distributionList']['massDeleteListModal']['massDeleteListForm']['MassDeleteListFormConfirm'] = 'Are you sure that you want to delete the selected distribution lists?';
$_LANG['addonCA']['distributionList']['massDeleteListModal']['baseAcceptButton']['title'] = 'Confirm';
$_LANG['addonCA']['distributionList']['massDeleteListModal']['baseCancelButton']['title'] = 'Cancel';

$_LANG['massDistributionListHasBeenDeleted']        = 'The distribution lists have been deleted successfully';

$_LANG['addonCA']['domainAlias']['mainContainer']['lists']['massDeleteDomainAliasButton']['button']['massDeleteDomainAliasButton'] = 'Delete';

$_LANG['addonCA']['domainAlias']['massDeleteDomainAliasModal']['modal']['massDeleteDomainAliasModal'] = 'Delete Domain Aliases';
$_LANG['addonCA']['domainAlias']['massDeleteDomainAliasModal']['massDeleteDomainAliasForm']['confirmMassDeleteDomainAlias'] = 'Are you sure that you want to delete the selected domain aliases?';
$_LANG['addonCA']['domainAlias']['massDeleteDomainAliasModal']['baseAcceptButton']['title'] = 'Confirm';
$_LANG['addonCA']['domainAlias']['massDeleteDomainAliasModal']['baseCancelButton']['title'] = 'Cancel';

$_LANG['massDomainAliasHasBeenDeleted']        = 'The domain aliases have been deleted successfully';
$_LANG['addonCA']['emailAccount']['massChangeStatusModal']['massChangeStatusForm']['status']['status'] = 'Status';
$_LANG['addonCA']['emailAccount']['mainContainer']['accounts']['changeStatusButton']['button']['changeStatusButton'] = 'Change Status';
$_LANG['addonCA']['emailAccount']['changeStatusModal']['modal']['changeStatusModal'] = 'Change Status';
$_LANG['addonCA']['emailAccount']['changeStatusModal']['changeStatusForm']['status']['status'] = 'Status';
$_LANG['addonCA']['emailAccount']['changeStatusModal']['baseAcceptButton']['title'] = 'Confirm';
$_LANG['addonCA']['emailAccount']['changeStatusModal']['baseCancelButton']['title'] = 'Cancel';

$_LANG['zimbra']['account']['neverLogged'] = 'Never logged in';


/**
 *
 *
 * New Langs
 */

$_LANG['error']['invalidServer'] = 'You need to select a server group from the dropdown menu first and save the product configuration.';
$_LANG['error']['extensionRequired'] = 'PHP extension `:extension:` is required.';
$_LANG['restrictions']['error']['somethingWentWrong'] = 'Something has gone wrong.';
$_LANG['restrictions']['error']['extensionRequired'] = 'PHP extension `:extension:` is required.';

$_LANG['addonCA']['emailAccount']['addAccountForm']['generalSection']['generated_row_section_2']['generated_2_0']['password']['description'] = 'A password must contain at least 8 characters';
$_LANG['FormValidators']['passwordCharsLengthError']                                              = 'A password must contain at least 8 characters';
$_LANG['addonCA']['domainAlias']['addDomainAliasModals']['addDomainAliasForm']['description']['descriptionDomainList'] = 'Please provide a description of a new alias.';

$_LANG['configurableOptionsCreate'] = 'The configurable options have been created successfully.';
$_LANG['configurableOptionsUpdate'] = 'The configurable options have been updated successfully.';


$_LANG['passwordChangedSuccessfully'] = 'The password has been changed successfully';
$_LANG['addonCA']['emailAccount']['mainContainer']['accounts']['changePassword']['button']['changePassword'] = 'Change Password';

$_LANG['addonCA']['emailAccount']['changePasswordModal']['modal']['changePasswordModal'] = 'Change Password';
$_LANG['addonCA']['emailAccount']['changePasswordModal']['changePasswordForm']['password']['password'] = 'Password *';
$_LANG['addonCA']['emailAccount']['changePasswordModal']['changePasswordForm']['password']['description'] = 'A password must contain at least 8 characters';
$_LANG['addonCA']['emailAccount']['changePasswordModal']['changePasswordForm']['repeat_password']['repeat_password'] = 'Repeat Password *';
$_LANG['addonCA']['emailAccount']['changePasswordModal']['baseAcceptButton']['title'] = 'Confirm';
$_LANG['addonCA']['emailAccount']['changePasswordModal']['baseCancelButton']['title'] = 'Cancel';

$_LANG['Automatically accept'] = 'Automatically accept';
$_LANG['Require list owner approval'] = 'Require list owner approval';
$_LANG['Automatically reject'] = 'Automatically reject';
$_LANG['mail@example.com'] = 'mail@example.com';
$_LANG['phoneNumberPlaceholder'] = '201 555 0123';
$_LANG['zimbra']['account']['status']['disabled']  = 'Disabled';
$_LANG['zimbra']['account']['status']['enabled'] = 'Enabled';

$_LANG['addonCA']['emailAccount']['mainContainer']['accounts']['actions']['More Actions'] = 'More Actions';
$_LANG['addonCA']['emailAccount']['accounts']['actions']['More Actions'] = 'More Actions';
$_LANG['addonCA']['emailAccount']['mainContainer']['accounts']['actions']['Additional Actions'] = 'Additional Actions';
$_LANG['addonCA']['emailAccount']['accounts']['actions']['Additional Actions'] = 'Additional Actions';
$_LANG['addonCA']['emailAccount']['mainContainer']['accounts']['actions']['changeStatusButton']['button']['changeStatusButton'] = 'Change Status';
$_LANG['addonCA']['emailAccount']['mainContainer']['accounts']['actions']['changePassword']['button']['changePassword'] = 'Change Password';
$_LANG['addonCA']['emailAccount']['mainContainer']['accounts']['actions']['editAccountButton']['button']['editAccountButton'] = 'Edit Account';
$_LANG['addonCA']['emailAccount']['mainContainer']['accounts']['actions']['deleteAccountButton']['button']['deleteAccountButton'] = 'Delete Account';
$_LANG['addonCA']['emailAccount']['accounts']['actions']['changeStatusButton']['button']['changeStatusButton'] = 'Change Status';
$_LANG['addonCA']['emailAccount']['accounts']['actions']['changePassword']['button']['changePassword'] = 'Change Password';
$_LANG['addonCA']['emailAccount']['accounts']['actions']['editAccountButton']['button']['editAccountButton'] = 'Edit Account';
$_LANG['addonCA']['emailAccount']['accounts']['actions']['deleteAccountButton']['button']['deleteAccountButton'] = 'Delete Account';
$_LANG['configurableOptionsWhmcsInfo']  = 'Below you can choose which configurable options will be generated for this product. Please note that these options are divided into two parts separated by a | sign, where the part on the left indicates the sent variable and the part on the right the friendly name displayed to customers. After generating these options you can edit the friendly part on the right, but not the variable part on the left. More information about configurable options and their editing can be found :configurableOptionsNameUrl:.';

$_LANG['addonCA']['emailAccount']['addAccountForm']['generalSection']['cosId']['cosId'] = 'Quota';
$_LANG['addonCA']['emailAccount']['editAccountForm']['editGeneralSection']['cosId']['cosId'] = 'Quota';
$_LANG['addonCA']['emailAccount']['accounts']['actions']['duttonDropdownItem']['button']['dropdownButton'] = 'Log In To Mailbox';
$_LANG['addonCA']['emailAccount']['mainContainer']['accounts']['actions']['duttonDropdownItem']['button']['dropdownButton'] = 'Dropdown Button';

$_LANG['logInToWebmail'] = 'Go To Webmail';
$_LANG['addonCA']['breadcrumbs']['Zimbra Email'] = '';