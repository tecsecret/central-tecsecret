# CyberPanel WHMCS Module Instructions

Using WHMCS module for CyberPanel, you can sell or automate basic functions of CyberPanel for your customers. You can install this module by following WHMCS server module installation steps. Here are they again:-

1. Navigate to the WHMCS public directory. 
2. Download WHMCS module at modules/servers.
3. Unzip files of CyberPanel module in cyberpanel folder. You may create this folder manually.

# Functions of this module are:

- Create new website or user account
- Terminate website
- Suspend or un-suspend website
- Change package 
- Change user password
- Auto-login for Customer or Admin user