{if $smarty.get.update != ''}
	{if $smarty.get.update == 'success'}
		<div class="alert alert-success">{$lang.success_changeip}</div>
	{elseif $smarty.get.update == 'fail'}
		<div class="alert alert-failure">{$lang.error_changeip}</div>
	{/if}
{elseif $smarty.get.refresh != ''}
	{if $smarty.get.refresh == 'success'}
		<div class="alert alert-success">{$lang.success_synckey}</div>
	{elseif $smarty.get.refresh == 'fail'}
		<div class="alert alert-failure">{$lang.error_synckey}</div>
	{/if}
{/if}
<h2>{$lang.header_synckey}</h2>
<form action="clientarea.php?action=productdetails&id={$id}" method="POST">
<p align="left">{$lang.warn_synckey}</p>
<input type="hidden" name="lic_action" value="refresh">
<center><input type="submit" name="submitres" value="{$lang.refreshlicense}" class="btn" /></center></form>
{if $clientopts.allowchangeip != 'false'}
<hr></hr>
<h2>{$lang.header_changeip}</h2>
<table border="0" width="100%" cellpadding="3" cellspacing="0">
	<form action="clientarea.php?action=productdetails&id={$id}" method="POST">
	<tr>
		<td valign="top" align="right" width="70%"><b>{$lang.newip}: </b><input type='text' name='newip' /> </td>
		<td valign="top" align="left" width="30%"><input type="submit" name="submit" value="{$lang.changeip}" class="btn" /></td>
	</tr>
	</form>
</table>
{/if}