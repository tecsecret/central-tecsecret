<?php

// Last Updated : 18/10/2019
// Version : 2.0.8

// This is supported since WHMCS 6.0+
use WHMCS\Database\Capsule;

// Disable warning messages - in PHP 5.4
//error_reporting(E_ALL & ~E_STRICT & ~E_NOTICE);
/* function died1(){
	print_r(error_get_last());
}
register_shutdown_function('died1');
 */
include_once('virtualizor_cloud_account_conf.php');
include_once('functions.php');
include_once(dirname(__FILE__).'/sdk/admin.php');
include_once(dirname(__FILE__).'/sdk/enduser.php');

function virtualizor_cloud_account_ConfigOptions() {
	
	# Should return an array of the module options for each product - Minimum of 24
    $configarray = array(
	 "Type" => array( "Type" => "text", "Size" => "25", "Description" => "Enter virtualizations allowed : openvz, xen, xenhvm, kvm, xcp, xcphvm, proxk, proxl, vzk, vzo"),
	 "Number of VMs" => array( "Type" => "text", "Size" => "25", "Description" => "Zero or empty for unlimited"),
	 "Number of Users" => array( "Type" => "text", "Size" => "25", "Description" => "Zero or empty for unlimited"),
	 "Max Disk Space" => array( "Type" => "text", "Size" => "25", "Description" => "GB"),
	 "Max RAM" => array( "Type" => "text", "Size" => "25", "Description" => "MB"),
	 "Max Burst / Swap" => array( "Type" => "text", "Size" => "25", "Description" => "MB"), 
	 "Max Bandwidth" => array( "Type" => "text", "Size" => "25", "Description" => "GB (Zero or empty for unlimited)"),
	 "Default CPU Weight" => array( "Type" => "text", "Size" => "25", "Description" => ""),
	 "Max Cores" => array( "Type" => "text", "Size" => "25", "Description" => "Max Cores / VM"),
	 "Default CPU %" => array( "Type" => "text", "Size" => "25", "Description" => ""),
	 "Max IPv4" => array( "Type" => "text", "Size" => "25", "Description" => "Number of IPs"),
	 "Max IPv6" => array( "Type" => "text", "Size" => "25", "Description" => "Number of IPv6 allowed"),
	 "Regions Allowed" => array( "Type" => "text", "Size" => "25", "Description" => "Enter comma seperated names of <b>Reseller Region Name</b>"),
	 "Media Groups" => array( "Type" => "text", "Size" => "25", "Description" => "Enter comma seperated names of Media Groups. Leave empty for all Media allowance"),
	 "Max IPv6 Subnets" => array( "Type" => "text", "Size" => "25", "Description" => "Number of IPv6 Subnets allowed"),
	 "Total Cores Allowed" => array( "Type" => "text", "Size" => "25", "Description" => "Total Number of Cores Allowed"),
	 "Network Speed" => array( "Type" => "text", "Size" => "25", "Description" => "KBytes/sec (Zero or empty for unlimited)"),
	 "Upload Speed" => array( "Type" => "text", "Size" => "25", "Description" => "KBytes/sec (Zero or empty for unlimited)"),
	);
	
	return $configarray;
}

function virtualizor_cloud_account_CreateAccount($params) {

	global $virtcloud_acc;

    # ** The variables listed below are passed into all module functions **
	
	$loglevel = (int) @$_REQUEST['loglevel'];
	
	if(!empty($virtcloud_acc['loglevel'])){
		$loglevel = $virtcloud_acc['loglevel'];
	}
	
    $serviceid = $params["serviceid"]; # Unique ID of the product/service in the WHMCS Database
    $pid = $params["pid"]; # Product/Service ID
    $producttype = $params["producttype"]; # Product Type: hostingaccount, reselleraccount, server or other
    $domain = $params["domain"];
	$username = $params["username"];
	$password = $params["password"];
    $clientsdetails = $params["clientsdetails"]; # Array of clients details - firstname, lastname, email, country, etc...
    $customfields = $params["customfields"]; # Array of custom field values for the product
    $configoptions = $params["configoptions"]; # Array of configurable option values for the product
	
	if(!empty($params["customfields"]['uid'])){
		return 'The User exists';
	}

    # Additional variables if the product/service is linked to a server
    $server = $params["server"]; # True if linked to a server
    $serverid = $params["serverid"];
    $serverip = $params["serverip"];
    $serverusername = $params["serverusername"];
    $serverpassword = $params["serverpassword"];
    $serveraccesshash = $params["serveraccesshash"];
    $serversecure = $params["serversecure"]; # If set, SSL Mode is enabled in the server config
	
	// Virts allowed
	$virts = cexplode(',', $params['configoption1']);
	foreach($virts as $k => $v){
		$post['allowed_virts'][] = $v;
	}
	
	// User Details
	$post['newemail'] = $params['clientsdetails']['email'];
	$post['newpass'] = $params["password"];
	$post['priority'] = 2;
	
	// Number of VMs
	$post['num_vs'] = (empty($params['configoptions'][vc_fn('num_vs')]) ? $params['configoption2'] : $params['configoptions'][vc_fn('num_vs')]);
	
	// Number of Users
	$post['num_users'] = (empty($params['configoptions'][vc_fn('num_users')]) ? $params['configoption3'] : $params['configoptions'][vc_fn('num_users')]);
	
	// Max Disk Space
	$post['space'] = (empty($params['configoptions'][vc_fn('space')]) ? $params['configoption4'] : $params['configoptions'][vc_fn('space')]);
	
	// Max Ram
	$post['ram'] = (empty($params['configoptions'][vc_fn('ram')]) ? $params['configoption5'] : $params['configoptions'][vc_fn('ram')]);
	
	// Max Burst
	$post['burst'] = (empty($params['configoptions'][vc_fn('burst')]) ? $params['configoption6'] : $params['configoptions'][vc_fn('burst')]);
	
	// Max Bandwidth
	$post['bandwidth'] = (empty($params['configoptions'][vc_fn('bandwidth')]) ? $params['configoption7'] : $params['configoptions'][vc_fn('bandwidth')]);
	
	// Max Bandwidth
	$post['network_speed'] = (empty($params['configoptions'][vc_fn('network_speed')]) ? $params['configoption17'] : $params['configoptions'][vc_fn('network_speed')]);
	
	// Max Bandwidth
	$post['upload_speed'] = (empty($params['configoptions'][vc_fn('upload_speed')]) ? $params['configoption18'] : $params['configoptions'][vc_fn('upload_speed')]);
	
	// Default CPU Weight
	$post['cpu'] = (empty($params['configoptions'][vc_fn('cpu')]) ? $params['configoption8'] : $params['configoptions'][vc_fn('cpu')]);
	
	// Max Cores
	$post['cores'] = (empty($params['configoptions'][vc_fn('cores')]) ? $params['configoption9'] : $params['configoptions'][vc_fn('cores')]);
	
	// Default CPU %
	$post['cpu_percent'] = (empty($params['configoptions'][vc_fn('cpu_percent')]) ? $params['configoption10'] : $params['configoptions'][vc_fn('cpu_percent')]);
	
	// Total Number of Cores
	$post['num_cores'] = (empty($params['configoptions'][vc_fn('num_cores')]) ? $params['configoption16'] : $params['configoptions'][vc_fn('num_cores')]);
	
	// Number of IPv4
	$post['num_ipv4'] = (empty($params['configoptions'][vc_fn('num_ipv4')]) ? $params['configoption11'] : $params['configoptions'][vc_fn('num_ipv4')]);
	
	// Number of IPv6
	$post['num_ipv6'] = (empty($params['configoptions'][vc_fn('num_ipv6')]) ? $params['configoption12'] : $params['configoptions'][vc_fn('num_ipv6')]);
	
	// Number of IPv6 Subnet
	$post['num_ipv6_subnet'] = (empty($params['configoptions'][vc_fn('num_ipv6_subnet')]) ? $params['configoption15'] : $params['configoptions'][vc_fn('num_ipv6_subnet')]);
	
	/////////////////
	// Get the Data
	/////////////////
	$data = VirtCloud_Account_Curl::call($params["serverip"], $params["serverusername"], $params["serverpassword"], 'index.php?act=adduser');
	
	if(empty($data)){
		return 'Could not load the server data.'.VirtCloud_Account_Curl::error($params["serverip"]);
	}
		
	if($loglevel > 2) logActivity('Virt Cloud Account Data Loaded : '.var_export($data, 1));
	
	// Server Groups
	$sgs = (empty($params['configoptions'][vc_fn('sgs')]) ? $params['configoption13'] : $params['configoptions'][vc_fn('sgs')]);
	$sgs = cexplode(',', $sgs);
	
	if($loglevel > 2) logActivity('Virt Cloud Account Data SGS : '.var_export($sgs, 1));
	
	// Find the server groups
	foreach($sgs as $k => $v){
		
		if(empty($v)){ // cexplode can return empty stuff
			continue;
		}
		
		unset($found);
		
		foreach($data['servergroups'] as $sk => $sv){
			
			if($sv['sg_reseller_name'] == $v){
				
				$found = $sk;
				
			}
			
		}
		
		if(!isset($found)){
			return 'Could not find the Region - '.$v.'. Please correct the <b>Product / Service</b> with the right region name.';
		}
		
		$post['sgs'][$found] = $found;
		
	}
	
	// Media Groups
	$mgs = (empty($params['configoptions'][vc_fn('mgs')]) ? $params['configoption14'] : $params['configoptions'][vc_fn('mgs')]);
	$mgs = cexplode(',', $mgs);
	
	if($loglevel > 2) logActivity('Virt Cloud Account Data MGS : '.var_export($mgs, 1));
	
	// Find the media groups
	foreach($mgs as $k => $v){
		
		if(empty($v)){ // cexplode can return empty stuff
			continue;
		}
		
		$found = 0;
		
		foreach($data['mgs'] as $sk => $sv){
			
			if($sv['mg_name'] == $v){
				
				$found = $sk;
				
			}
			
		}
		
		if(empty($found)){
			return 'Could not find the Media Group - '.$v.'. Please correct the <b>Product / Service</b> with the right Media Group name.';
		}
		
		$post['mgs'][$found] = $found;
		
	}
	
	$post['adduser'] = 1;
	
	if($loglevel > 0) logActivity('Virtualizor Cloud Account Params : '.var_export($post, 1));
	
	//return "Debug";
	
	$ret = VirtCloud_Account_Curl::call($params["serverip"], $params["serverusername"], $params["serverpassword"], 'index.php?act=adduser&addkey=1', $post);
	
	if($loglevel > 1) logActivity('Return Values : '.var_export($ret, 1));
	
	// Was the VPS Inserted
	if(!empty($ret['done'])){
		
		// uid of virtualizor
		$res = Capsule::table('tblcustomfields')
			->select('id')
			->where('relid', $pid)
			->where('fieldname', 'uid')
			->get();
		
		// We will check if there is an entry if not we will insert it.
		$sel_res = Capsule::table('tblcustomfieldsvalues')
			->select('relid')
			->where('relid', $serviceid)
			->where('fieldid', $res[0]->id)
			->get();
		
		if($loglevel > 0) logActivity('Did we found anything : '.var_export($sel_res, 1));
		
		// We will insert it if not found anything
		if(empty($sel_res[0]->relid)){	
			Capsule::table('tblcustomfieldsvalues')->insert(array(
															array('value' => $ret['done'],
															'relid' => $serviceid,
															'fieldid' => $res[0]->id)
														));
		}else{			
			 $update_res = Capsule::table('tblcustomfieldsvalues')
				->where('relid', $serviceid)
				->where('fieldid', $res[0]->id)
				->update(
					array(
						'value' => $ret['done'],
					)
				);
		}
		
		// API key of virtualizor user
		$res = Capsule::table('tblcustomfields')
			->select('id')
			->where('relid', $pid)
			->where('fieldname', 'ca_apikey')
			->get();
			
		$sel_res = Capsule::table('tblcustomfieldsvalues')
			->select('relid')
			->where('relid', $serviceid)
			->where('fieldid', $res[0]->id)
			->get();
		
		if($loglevel > 0) logActivity('Did we found anything for APIKEY : '.var_export($sel_res, 1));
		
		// We will insert it if not found anything
		if(empty($sel_res[0]->relid)){
			Capsule::table('tblcustomfieldsvalues')->insert(array(
															array('value' => $ret['apikeys']['apikey'],
															'relid' => $serviceid,
															'fieldid' => $res[0]->id)
														));
		}else{		
			$update_res = Capsule::table('tblcustomfieldsvalues')
				->where('relid', $serviceid)
				->where('fieldid', $res[0]->id)
				->update(
					array(
						'value' => $ret['apikeys']['apikey'],
					)
				);
		}
		
		// API pass of virtualizor user
		$res = Capsule::table('tblcustomfields')
			->select('id')
			->where('relid', $pid)
			->where('fieldname', 'ca_apipass')
			->get();
			
		$sel_res = Capsule::table('tblcustomfieldsvalues')
			->select('relid')
			->where('relid', $serviceid)
			->where('fieldid', $res[0]->id)
			->get();
		
		if($loglevel > 0) logActivity('Did we found anything for APIPASS : '.var_export($sel_res, 1));
		
		// We will insert it if not found anything
		if(empty($sel_res[0]->relid)){
			Capsule::table('tblcustomfieldsvalues')->insert(array(
															array('value' => $ret['apikeys']['apipass'],
															'relid' => $serviceid,
															'fieldid' => $res[0]->id)
														));
		}else{
			$update_res = Capsule::table('tblcustomfieldsvalues')
				->where('relid', $serviceid)
				->where('fieldid', $res[0]->id)
				->update(
					array(
						'value' => $ret['apikeys']['apipass'],
					)
				);
		}
		
		// Change the Username to the email
		$update_res = Capsule::table('tblhosting')
				->where('id', $serviceid)
				->update(
					array(
						'username' => $params['clientsdetails']['email'],
					)
				);
		
		// Did it start ?
		if(!empty($ret['done'])){		
			return 'success';	
		}else{
			return 'Errors : '.implode('<br>', $ret['error']);
		}
		
	} else {
		return 'Errors : '.implode('<br>', $ret['error']);
	}
	
}


function virtualizor_cloud_account_TerminateAccount($params) {

	global $virtcloud_acc;
	
	// Should delete the VMs as well ?
	$delete_vms = (!empty($virtcloud_acc['dont_delete_vms']) ? '&dont_delete_vms=1' : '');

	$data = VirtCloud_Account_Curl::call($params["serverip"], $params["serverusername"], $params["serverpassword"], 'index.php?act=users&delete='.$params['customfields']['uid'].$delete_vms);
			
	if(empty($data)){
		return 'Could not load the server data.'.VirtCloud_Account_Curl::error($params["serverip"]);
	}
	
	if($loglevel > 1) logActivity('Terminate Return Values : '.var_export($data, 1));	
	
	// If the VPS has been deleted
    if(!empty($data['done'])){
	
		// vpsid of virtualizor
		$query = Capsule::table('tblcustomfields')
			->select('id')
			->where('relid', $params["pid"])
			->where('fieldname', 'uid')
			->get();
		$res = (array) $query[0];
		
		$query = Capsule::table('tblcustomfieldsvalues')
			->where('relid', $params["serviceid"])
			->where('fieldid', $res[id])
			->update(array('value'=>''));
		
		$result = "success";
	} else {
		$result = "There was some error deleting the account";
	}
	
	return $result;
}

function virtualizor_cloud_account_SuspendAccount($params) {
	
	$data = VirtCloud_Account_Curl::call($params["serverip"], $params["serverusername"], $params["serverpassword"], 'index.php?act=users&suspend='.$params['customfields']['uid']);
			
	if(empty($data)){
		return 'Could not load the server data.'.VirtCloud_Account_Curl::error($params["serverip"]);
	}
	
	if($loglevel > 1) logActivity('Suspend Return Values : '.var_export($data, 1));	
	
    	if(!empty($data['done'])) {
		
		$result = "success";
		
	} else {
		
		$result = "There was some error suspending the Cloud Account";
	}
	
	return $result;
}

function virtualizor_cloud_account_UnsuspendAccount($params) {

	$data = VirtCloud_Account_Curl::call($params["serverip"], $params["serverusername"], $params["serverpassword"], 'index.php?act=users&unsuspend='.$params['customfields']['uid']);
			
	if(empty($data)){
		return 'Could not load the server data.'.VirtCloud_Account_Curl::error($params["serverip"]);
	}
	
	if($loglevel > 1) logActivity('Unsuspend Return Values : '.var_export($data, 1));	
	
   	if(!empty($data['done'])) {
		
		$result = "success";
		
	} else {
		
		$result = "There was some error Unsuspending the Cloud Account";
	}
	
	return $result;
}

/*function virtualizor_cloud_account_ChangePassword($params) {

	logActivity('ChangePassword : '.var_export($params, 1));
	
	$post['changepass'] = 1;
	$post['newpass'] = $params['password'];
	$post['conf'] = $params['password'];

	$data = VirtCloud_Account_Curl::call($params["serverip"], $params["serverusername"], $params["serverpassword"], 'index.php?act=userpassword', $post);
	
	logActivity('ChangePassword : '.var_export($data, 1));
			
	if(empty($data)){
		return 'Could not load the server data.'.VirtCloud_Account_Curl::error($params["serverip"]);
	}

    if ($data['done']) {
		$result = "success";
	} else {
		$result = "There was some error changing the password";
	}
	
	return $result;
}*/

// Updates the USER
function virtualizor_cloud_account_ChangePackage($params) {

	global $virtcloud_acc;

    # ** The variables listed below are passed into all module functions **
	
	$loglevel = (int) @$_REQUEST['loglevel'];
	
	if(!empty($virtcloud_acc['loglevel'])){
		$loglevel = $virtcloud_acc['loglevel'];
	}

	// Virts allowed
	$virts = cexplode(',', $params['configoption1']);
	foreach($virts as $k => $v){
		$post[$v] = 'on';
	}
	
	
	$post['newemail'] = $params['clientsdetails']['email'];
	$post['dnsplan_id'] = 0;
	$post['priority'] = 2;
	
	// Number of VMs
	$post['num_vs'] = (empty($params['configoptions'][vc_fn('num_vs')]) ? $params['configoption2'] : $params['configoptions'][vc_fn('num_vs')]);
	
	// Number of Users
	$post['num_users'] = (empty($params['configoptions'][vc_fn('num_users')]) ? $params['configoption3'] : $params['configoptions'][vc_fn('num_users')]);
	
	// Max Disk Space
	$post['space'] = (empty($params['configoptions'][vc_fn('space')]) ? $params['configoption4'] : $params['configoptions'][vc_fn('space')]);
	
	// Max Ram
	$post['ram'] = (empty($params['configoptions'][vc_fn('ram')]) ? $params['configoption5'] : $params['configoptions'][vc_fn('ram')]);
	
	// Max Burst
	$post['burst'] = (empty($params['configoptions'][vc_fn('burst')]) ? $params['configoption6'] : $params['configoptions'][vc_fn('burst')]);
	
	// Max Bandwidth
	$post['bandwidth'] = (empty($params['configoptions'][vc_fn('bandwidth')]) ? $params['configoption7'] : $params['configoptions'][vc_fn('bandwidth')]);
	
	// Max Bandwidth
	$post['network_speed'] = (empty($params['configoptions'][vc_fn('network_speed')]) ? $params['configoption17'] : $params['configoptions'][vc_fn('network_speed')]);
	
	// Max Bandwidth
	$post['upload_speed'] = (empty($params['configoptions'][vc_fn('upload_speed')]) ? $params['configoption18'] : $params['configoptions'][vc_fn('upload_speed')]);
	
	// Default CPU Weight
	$post['cpu'] = (empty($params['configoptions'][vc_fn('cpu')]) ? $params['configoption8'] : $params['configoptions'][vc_fn('cpu')]);
	
	// Max Cores
	$post['cores'] = (empty($params['configoptions'][vc_fn('cores')]) ? $params['configoption9'] : $params['configoptions'][vc_fn('cores')]);
	
	// Default CPU %
	$post['cpu_percent'] = (empty($params['configoptions'][vc_fn('cpu_percent')]) ? $params['configoption10'] : $params['configoptions'][vc_fn('cpu_percent')]);
	
	// Total Number of Cores
	$post['num_cores'] = (empty($params['configoptions'][vc_fn('num_cores')]) ? $params['configoption16'] : $params['configoptions'][vc_fn('num_cores')]);
	
	// Number of IPv4
	$post['num_ipv4'] = (empty($params['configoptions'][vc_fn('num_ipv4')]) ? $params['configoption11'] : $params['configoptions'][vc_fn('num_ipv4')]);
	
	// Number of IPv6
	$post['num_ipv6'] = (empty($params['configoptions'][vc_fn('num_ipv6')]) ? $params['configoption12'] : $params['configoptions'][vc_fn('num_ipv6')]);
	
	// Number of IPv6 Subnet
	$post['num_ipv6_subnet'] = (empty($params['configoptions'][vc_fn('num_ipv6_subnet')]) ? $params['configoption15'] : $params['configoptions'][vc_fn('num_ipv6_subnet')]);
	
	/////////////////
	// Get the Data
	/////////////////

	$data = VirtCloud_Account_Curl::call($params["serverip"], $params["serverusername"], $params["serverpassword"], 'index.php?act=edituser&uid='.$params['customfields']['uid']);
			
	if(empty($data)){
		return 'Could not load the server data.'.VirtCloud_Account_Curl::error($params["serverip"]);
	}
		
	if($loglevel > 1) logActivity('Virt Cloud Account ChanePackage Orig Values : '.var_export($data, 1));
	
	// Server Groups
	$sgs = (empty($params['configoptions'][vc_fn('sgs')]) ? $params['configoption13'] : $params['configoptions'][vc_fn('sgs')]);
	$sgs = cexplode(',', $sgs);
	
	if($loglevel > 2) logActivity('Virt Cloud Account Data ChangePackage SGS : '.var_export($sgs, 1));
	
	// Find the server groups
	foreach($sgs as $k => $v){
		
		if(empty($v)){ // cexplode can return empty stuff
			continue;
		}
		
		$found = 0;
		
		foreach($data['servergroups'] as $sk => $sv){
			
			if($sv['sg_reseller_name'] == $v){
				
				$found = $sk;
				
			}
			
		}
		
		if(empty($found)){
			return 'Could not find the Region - '.$v.'. Please correct the <b>Product / Service</b> with the right region name.';
		}
		
		$post['sgs'][$sk] = $sk;
		
	}
	
	// Media Groups
	$mgs = (empty($params['configoptions'][vc_fn('mgs')]) ? $params['configoption14'] : $params['configoptions'][vc_fn('mgs')]);
	$mgs = cexplode(',', $mgs);
	
	if($loglevel > 2) logActivity('Virt Cloud Account ChangePackage Data MGS : '.var_export($mgs, 1));
	
	// Find the media groups
	foreach($mgs as $k => $v){
		
		if(empty($v)){ // cexplode can return empty stuff
			continue;
		}
		
		$found = 0;
		
		foreach($data['mgs'] as $sk => $sv){
			
			if($sv['mg_name'] == $v){
				
				$found = $sk;
				
			}
			
		}
		
		if(empty($found)){
			return 'Could not find the Media Group - '.$v.'. Please correct the <b>Product / Service</b> with the right Media Group name.';
		}
		
		$post['mgs'][$sk] = $sk;
		
	}
	
	$post['edituser'] = 1;
	
	if($loglevel > 0) logActivity('Virtualizor Cloud Account ChangePackage Params : '.var_export($post, 1));
	
	//return "Debug";
	
	$ret = VirtCloud_Account_Curl::call($params["serverip"], $params["serverusername"], $params["serverpassword"], 'index.php?act=edituser&uid='.$params['customfields']['uid'], $post);
		
	if($loglevel > 1) logActivity('Virtualizor Cloud Account ChangePackage Return Values : '.var_export($ret, 1));
	
	// Was the Action Successful
	if(!empty($ret['done'])){
		$result = "success";
	} else {
		return 'Errors : '.implode('<br>', $ret['error']);
	}
	
	return $result;
}

function virtualizor_cloud_account_AdminLink($params) {
	$code = '<a href="https://'.$params["serverip"].':4085/index.php?act=login" target="_blank">Virtualizor Panel</a>';
	return $code;
}

function virtualizor_cloud_account_LoginLink($params) {
	echo "<a href=\"https://".$params["serverip"].":4083/\" target=\"_blank\" style=\"color:#cc0000\">Login to Virtualizor</a>";
}
/* 
function virtualizor_cloud_account_AdminServicesTabFields($params) {
	
	if(!empty($_GET['vapi_mode'])){
		ob_end_clean();
	}
	
	$code = virtualizor_cloud_account_newUI($params, 'clientsservices.php?vapi_mode=1&userid='.$params['userid'], '../modules/servers'); 
	
	$fieldsarray = array(
	 'VPS Information' => '<div style="width:100%" id="tab1"></div>'.$code,
	);
	
	return $fieldsarray;

}
 */
 
function virtualizor_cloud_account_TestConnection($params) {
	
	$ret = VirtCloud_Account_Curl::call($params["serverip"], $params["serverusername"], $params["serverpassword"], 'index.php?act=adduser');
	
	//logActivity('Virtualizor Cloud Account ChangePackage Return Values : '.var_export($ret, 1));
	
	if(empty($ret)){
		return array('error' => 'FAILED: Could not connect to Server.');
	}else{
		return array('success' => true);
	}
}
 
function virtualizor_cloud_account_ClientArea($params) {
	
	global $virt_action_display, $virt_errors, $virt_resp, $virtualizor_conf, $whmcsmysql;
	return virtualizor_cloud_account_newUI($params);
	//return "<a href=\"https://".$params["serverip"].":4083/\" target=\"_blank\" style=\"color:#cc0000\">Login to Cloud Account</a>";
}

function virtualizor_cloud_account_newUI($params, $url_prefix = 'clientarea.php?action=productdetails', $modules_url = 'modules/servers'){
	//print_r($params);
	global $virt_action_display, $virt_errors, $virt_resp, $virtualizor_conf, $whmcsmysql;
	
	// Is the VPS there ?
	if(empty($params['customfields']['uid'])){
		return 'User is not available';
	}
	
	if($_REQUEST['whmcs_add_api_form'] && !empty($_REQUEST['key']) && !empty($_REQUEST['pass'])){
		$newapikey = '';
		$newapipass = '';
		$resp = array();
		
		$newapikey = trim($_REQUEST['key']);
		$newapipass = trim($_REQUEST['pass']);
		
		if(!(preg_match('/^[a-zA-Z0-9]+$/', $newapikey) && preg_match('/^[a-zA-Z0-9]+$/', $newapipass))){
			$resp['error'] = 1;
			$resp['msg'] = '{{whmcs_api_inv_chars}}';
		}else{
			
			$params['customfields']['ca_apikey'] = $newapikey;
			$params['customfields']['ca_apipass'] = $newapipass;
			
			unset($_GET['give']);
			$_GET['api'] = 'json';
			$_GET['act'] = 'listvs';
			$_GET['random'] = $_REQUEST['random'];
			$_GET['SET_REMOTE_IP'] = $_SERVER['REMOTE_ADDR'];
			$res = VirtCloud_Account_Curl::action($params, http_build_query($_GET), array());
			
			if($res['uid'] != '-1' && $params['customfields']['uid'] == $res['uid']){
				$pid = $params["pid"];
				$serviceid = $params["serviceid"];
				
				// API key of virtualizor user
				$query = Capsule::table('tblcustomfields')
					->select('id')
					->where('relid', $pid)
					->where('fieldname', 'ca_apikey')
					->get();
				$res = (array) $query[0];
				
				$query = Capsule::table('tblcustomfieldsvalues')
					->where('relid', $serviceid)
					->where('fieldid', $res[id])
					->update(
						array('value'=>$newapikey)
					);
				
				// API pass of virtualizor user
				$query = Capsule::table('tblcustomfields')
					->select('id')
					->where('relid', $pid)
					->where('fieldname', 'ca_apipass')
					->get();
				$res = (array) $query[0];
				
				$query = Capsule::table('tblcustomfieldsvalues')
					->where('relid', $serviceid)
					->where('fieldid', $res[id])
					->update(
						array('value'=>$newapipass)
					);
				
				$resp['done'] = 1;
			}else{
				$resp['error'] = 1;
				$resp['msg'] = '{{whmcs_api_not_match}}';
			}
		}
		
		// Parse the languages
		vload_lang($params['clientsdetails']['language']);
		$resp['msg'] = vparse_lang($resp['msg']);
		
		echo json_encode($resp);
		die();
	}
	
	// New method of Virtualizor Module
	if(isset($_GET['give'])){
	
		//error_reporting(-1);
		//&suid='.$params['customfields']['uid'].'
		$var['APP'] = 'Virtualizor'; // NOT USED
		$var['site_name'] = 'WHMCS';
		$var['API'] = $url_prefix.'&id='.$params['serviceid'].'&api=json&';
		$var['giver'] = $url_prefix.'&id='.$params['serviceid'].'&';
		$var['url'] = $url_prefix.'&id='.$params['serviceid'].'&';
		$var['version'] = '2.0.7';
		$var['logo'] = '';
		$var['theme'] = $modules_url.'/virtualizor_cloud_account/ui/';
		$var['theme_path'] = dirname(__FILE__).'/ui/';
		$var['images'] = $var['theme'].'images/';
		$var['virt_dev_license'] = ' ';
		$var['virt_pirated_license'] = ' ';
		
		if($_GET['give'] == 'index.html'){
			
			// We are zipping if possible
			if(function_exists('ob_gzhandler')){
				ob_start('ob_gzhandler');
			}
	
			// Read the file
			$data = file_get_contents($var['theme_path'].'index.html');
			
			$filetime = filemtime($var['theme_path'].'index.html');
			
		}
	
		if($_GET['give'] == 'combined.js'){
		
			// Read the file
			$data = '';
			$jspath = $var['theme_path'].'js2/';
			$files = array('jquery.min.js',
							'jquery.dataTables.min.js',
							'jquery.bpopup.min.js',
							'jquery.tablesorter.min.js',
							'jquery.flot.min.js',
							'jquery.flot.pie.min.js',
							'jquery.flot.stack.min.js',
							'jquery.flot.time.min.js',
							'jquery.flot.tooltip.min.js',
							'jquery.flot.symbol.min.js',
							'jquery.flot.axislabels.js',
							'jquery.flot.selection.min.js',
							'jquery.flot.resize.min.js',
							'jquery.scrollbar.min.js',
							'tiptip.js',
							'chosen.jquery.min.js',
							'bootstrap.min.js',
							'virtualizor.js',
							'haproxy.js',
						);
			
			foreach($files as $k => $v){
				//echo $k.'<br>';
				$data .= file_get_contents($jspath.'/'.$v)."\n\n";
			}
			
			// We are zipping if possible
			if(function_exists('ob_gzhandler')){
				ob_start('ob_gzhandler');
			}
			
			// Type javascript
			header("Content-type: text/javascript; charset: UTF-8");
	
			// Set a zero Mtime
			$filetime = filemtime($var['theme_path'].'/js2/virtualizor.js');
			
		}
	
		if($_GET['give'] == 'style.css'){
		
			// Read the file
			$data = '';
			$jspath = $var['theme_path'].'css2/';
			$files = array('bootstrap.min.css',
							'font-awesome.min.css',
							'jquery.dataTables.css',
							'chosen.min.css',
							'jquery.scrollbar.css',
							'style.css',
							'billing.css',
							'select2.css',
			);
			
			foreach($files as $k => $v){
				//echo $k.'<br>';
				$data .= file_get_contents($jspath.'/'.$v)."\n\n";
			}
			
			// Type CSS
			header("Content-type: text/css; charset: UTF-8");
			
			// We are zipping if possible
			if(function_exists('ob_gzhandler')){
				ob_start('ob_gzhandler');
			}
			
		}
		
		foreach($var as $k => $v){			
			$data = str_replace('[['.$k.']]', $v, $data);
		}
	
		// Parse the languages
		vload_lang($params['clientsdetails']['language']);
		echo vparse_lang($data);
		
		die();
		exit(0);
		
	}
	
	if($_REQUEST['api'] == 'json'){
		
		// Overwrite certain variables
		$_GET['svs'] = $_REQUEST['svs'];
		$_GET['SET_REMOTE_IP'] = $_SERVER['REMOTE_ADDR'];
		
		$res = VirtCloud_Account_Curl::action($params, http_build_query($_GET), $_POST, $_GET['svs']);
		
		//$res['uid'] = $params['customfields']['uid'];
		
		echo json_encode($res);
		die();
		exit(0);
	}
	
	if($_GET['b'] == 'novnc' || (!empty($_REQUEST['novnc'])) && $_REQUEST['act'] == 'vnc'){
	
		$data = VirtCloud_Account_Curl::action($params, 'act=vnc&novnc=1', array(), $_REQUEST['svs']);
		
		// Find the servers hostname
		$query = Capsule::table('tblservers')
					->select('hostname')
					->where('id', $params['serverid'])
					->get();
		
		$server_details = (array) $query[0];
		$params['serverhostname'] = $server_details['hostname'];
		
		// fetch the novnc file
		$modules_url_vnc = $modules_url.'/virtualizor_cloud_account';
		$novnc_viewer = file_get_contents($modules_url_vnc.'/novnc/novnc.html');
		
		$novnc_password = $data['info']['password']; 
		$vpsid = $_REQUEST['svs'];
		$novnc_serverip = empty($params['serverhostname']) ? $params['serverip'] : $params['serverhostname'];
		$proto = 'http';
		$port = 4081;
		$virt_port = 4082;
		$websockify = 'websockify';
		if(!empty($_SERVER['HTTPS']) || @$_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https'){
			$proto = 'https';
			$port = 4083;
			$virt_port = 4083;
			$websockify = 'novnc/';
			$novnc_serverip = empty($params['serverhostname']) ? $params['serverip'] : $params['serverhostname'];
		}
		
		if($data['info']['virt'] == 'xcp'){
			$vpsid .= '-'.$data['info']['password'];
		}
		
		echo $novnc_viewer = vlang_vars_name($novnc_viewer, array('HOST' => $novnc_serverip,
															'PORT' => $port,
															'VIRTPORT' => $virt_port,
															'PROTO' => $proto,
															'WEBSOCKET' => $websockify,
															'TOKEN' => $vpsid,
															'PASSWORD' => $novnc_password,
															'MODULE_URL' => $modules_url_vnc));
															
													
		die();
	}
	
	// Java VNC
	if($_REQUEST['act'] == 'vnc' && !empty($_REQUEST['launch'])){
	
		$response = VirtCloud_Account_Curl::action($params, 'act=vnc&launch=1&giveapplet=1', '', true);
		
		if(empty($response)){
			return false;
		}
		
		// Is the applet code in the API Response ?
		if(!empty($response['info']['applet'])){
			
			$applet = $response['info']['applet'];
			
		}else{
	
			$virttype = preg_match('/xcp/is', $params['configoption1']) ? 'xcp' : strtolower($params['configoption1']);
		
			// NonXCP
			if($virttype != 'xcp'){
				
				if(!empty($response['info']['port']) && !empty($response['info']['ip']) && !empty($response['info']['password'])){				
					$applet = '<APPLET ARCHIVE="https://s2.softaculous.com/a/virtualizor/files/VncViewer.jar" CODE="com.tigervnc.vncviewer.VncViewer" WIDTH="1" HEIGHT="1">
						<PARAM NAME="HOST" VALUE="'.$response['info']['ip'].'">
						<PARAM NAME="PORT" VALUE="'.$response['info']['port'].'">
						<PARAM NAME="PASSWORD" VALUE="'.$response['info']['password'].'">
						<PARAM NAME="Open New Window" VALUE="yes">
					</APPLET>';	
				}
			
			// XCP
			}else{
				
				if(!empty($response['info']['port']) && !empty($response['info']['ip'])){
					$applet = '<APPLET ARCHIVE="https://s2.softaculous.com/a/virtualizor/files/TightVncViewer.jar" CODE="com.tightvnc.vncviewer.VncViewer" WIDTH="1" HEIGHT="1">
						<PARAM NAME="SOCKETFACTORY" value="com.tightvnc.vncviewer.SshTunneledSocketFactory">
						<PARAM NAME="SSHHOST" value="'.$response['info']['ip'].'">
						<PARAM NAME="HOST" value="localhost">
						<PARAM NAME="PORT" value="'.$response['info']['port'].'">
						<PARAM NAME="Open New Window" VALUE="yes">
					</APPLET>';
				}
				
			}
		
		}
		
		echo $applet;
		
		die();
	
	}
	
	if(!empty($virtualizor_conf['client_ui']['direct_login'])){
		return "<center><a href=\"https://".$params["serverip"].":4083/\" target=\"_blank\">Login to Virtualizor</a></center>";
	}

	$code .= '<script type="text/javascript">
		
function iResize(){
	try{
		document.getElementById("virtualizor_manager").style.height = 
		document.getElementById("virtualizor_manager").contentWindow.document.body.offsetHeight + "px";
	}catch(e){ };
}

setInterval("iResize()", 1000);
$(".main-content").removeClass("col-md-9").addClass("col-md-12");
$(document).ready(function(){
	
	var divID = "tab1";
	if (!document.getElementById(divID)) {
        divID = "domain";
    }
	
	var myDiv = document.createElement("div");
	myDiv.id = "virtualizor_load_div";
	myDiv.innerHTML = \'<center style="padding:10px; background-color: #FAFBD9;">Loading Panel options ...</center><br /><br /><br />\';
	document.getElementById(divID).appendChild(myDiv);
	
	var iframe = document.createElement("iframe");
	iframe.id = "virtualizor_manager";
	iframe.width = "100%";
	iframe.style.display = "none";
	iframe.style.border = "none";
	iframe.scrolling = "no";
	iframe.src = "'.$url_prefix.'&id='.$params['serviceid'].'&give=index.html#act=listvs";
	document.getElementById(divID).appendChild(iframe);
	
	$("#virtualizor_manager").load(function(){
		$("#virtualizor_load_div").hide();
		$(this).show();
		iResize();
	});
	
	$(".moduleoutput").each(function(){
		this.style.display = "none";
	});
	
});

</script>';

	return $code;
		
}

/*function virtualizor_cloud_account_AdminCustomButtonArray() {
	# This function can define additional functions your module supports, the example here is a reboot button and then the reboot function is defined below
    $buttonarray = array(
	 "Start VPS" => "admin_start",
	 "Reboot VPS" => "admin_reboot",
 	 "Stop VPS"=> "admin_stop",
	 "Poweroff VPS"=> "admin_poweroff"
	);
	return $buttonarray;
}*/

class VirtCloud_Account_Curl {
	
	public static function make_api_call($ip, $pass, $path, $data = array(), $post = array(), $cookies = array()){
		
		global $virtualizor_conf, $whmcsmysql;
		
		$key = generateRandStr(8);
		$apikey = make_apikey($key, $pass);
		
		$url = 'https://'.$ip.':4085/'.$path;	
		$url .= (strstr($url, '?') ? '' : '?');	
		$url .= '&api=serialize&apikey='.rawurlencode($apikey).'&skip_callback=1';
		
		// Pass some data if there
		if(!empty($data)){
			$url .= '&apidata='.rawurlencode(base64_encode(serialize($data)));
		}
	
		if($virtualizor_conf['loglevel'] > 0){
			logActivity('URL : '. $url);
		}
		
		// Set the curl parameters.
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
			
		// Time OUT
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 3);
		
		// Turn off the server and peer verification (TrustManager Concept).
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
			
		// UserAgent
		curl_setopt($ch, CURLOPT_USERAGENT, 'Softaculous');
		
		// Cookies
		if(!empty($cookies)){
			curl_setopt($ch, CURLOPT_COOKIESESSION, true);
			curl_setopt($ch, CURLOPT_COOKIE, http_build_query($cookies, '', '; '));
		}
		
		if(!empty($post)){
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post));
		}
		
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		
		// Get response from the server.
		$resp = curl_exec($ch);
		
		if(empty($resp)){
			$GLOBALS['virt_curl_err'] = curl_error($ch);
		}
			
		curl_close($ch);
		
		// The following line is a method to test
		//if(preg_match('/sync/is', $url)) echo $resp;
		
		if(empty($resp)){
			return false;
		}
		
		// As a security prevention measure - Though this cannot happen
		$resp = str_replace($pass, '12345678901234567890123456789012', $resp);
		
		$r = _unserialize($resp);
		
		if(empty($r)){
			return false;
		}
		
		return $r;
	}	

	public static function e_make_api_call($ip, $userkey, $pass, $vid, $path, $post = array()){
		
		$v = new Virtualizor_Enduser_Cloud_Account_API($ip, $userkey, $pass);
		
		return $v->call($path.'&svs='.$vid, $post);
	}	
	
	public static function action($params, $action, $post = array(), $svs){
		
		global $virt_verify, $virt_errors;
		
		// Make the call
		$response = VirtCloud_Account_Curl::e_make_api_call($params["serverip"], $params['customfields']['ca_apikey'], $params['customfields']['ca_apipass'], $svs, 'index.php?'.$action, $post);

		if(empty($response)){
			$virt_errors[] = 'The action could not be completed as no response was received.';
			return false;
		}
		
		return $response;
	
	} // function virt_curl_action ends	
	
	public static function error($ip = ''){
		
		$err = '';
		
		if(!empty($GLOBALS['virt_curl_err'])){
			$err .= ' Curl Error: '.$GLOBALS['virt_curl_err'];
		}
		
		if(!empty($ip)){
			$err .= ' (Server IP : '.$ip.')';
		}
		
		return $err;
	}
	
	public static function call($ip, $userkey, $pass, $path, $post = array(), $cookies = array()){

		$v = new Virtualizor_Admin_Cloud_Account_API($ip, $userkey, $pass);
		
		return $v->call($path, array(), $post, $cookies);
		
	}

} // class VirtCloud_Account_Curl ends

?>